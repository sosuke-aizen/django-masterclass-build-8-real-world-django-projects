1
00:00:00,150 --> 00:00:04,440
So in this lecture, let's actually go ahead and add some styling to our app.

2
00:00:04,830 --> 00:00:09,870
So the very first step in adding the styling is that we will get the bootstrap citizen link.

3
00:00:09,930 --> 00:00:15,420
So let's go ahead and search for bootstrap for CVN.

4
00:00:16,200 --> 00:00:20,280
And that should actually give you the option to get the CBN link.

5
00:00:20,310 --> 00:00:23,820
So I will go ahead and click on the first option.

6
00:00:24,390 --> 00:00:28,340
And now this thing right here is nothing but the Syrian link for bootstrap foot.

7
00:00:28,980 --> 00:00:30,870
So now once you have this link with you.

8
00:00:31,110 --> 00:00:36,130
Now let's go ahead and just piece this link into your next.

9
00:00:36,340 --> 00:00:38,040
Each Geminids head tag.

10
00:00:38,400 --> 00:00:41,580
So I will go back and this thing up over here.

11
00:00:42,480 --> 00:00:47,520
So as soon as you add the bootstrap link and hit refresh and rather than hitting refresh.

12
00:00:47,580 --> 00:00:49,720
Always make sure to visit this you are again.

13
00:00:49,860 --> 00:00:53,010
So as you can see now, this thing looks a lot better.

14
00:00:53,250 --> 00:00:56,190
However, we are not going to just leave it like that.

15
00:00:56,530 --> 00:01:00,180
Instead, we are actually going to go ahead and format this in a proper manner.

16
00:01:00,840 --> 00:01:06,420
So if you don't want to follow this lecture of styling these things, then you can just go ahead, skip

17
00:01:06,420 --> 00:01:06,750
that.

18
00:01:07,110 --> 00:01:13,440
I will provide the entire source code of this particular project at the end of the section so you can

19
00:01:13,440 --> 00:01:14,430
download from there.

20
00:01:14,880 --> 00:01:18,840
However, if you are interested in designing this, let's go ahead and continue.

21
00:01:19,230 --> 00:01:23,610
So the very first thing which we want to do here is that you want to go inside the body tag.

22
00:01:24,120 --> 00:01:28,950
And right here, just make sure that you don't delete these forms and everything like that.

23
00:01:29,350 --> 00:01:31,230
Well, just make sure to keep them as it is.

24
00:01:31,410 --> 00:01:36,360
Instead, we are going to just go ahead and cut them up and pasted inside these tanks.

25
00:01:36,750 --> 00:01:40,400
So, first of all, with Bootstrap, you always go ahead and create a container.

26
00:01:40,560 --> 00:01:45,350
So creative class and name it as container.

27
00:01:46,110 --> 00:01:48,100
And inside the container, we will have a room.

28
00:01:48,210 --> 00:01:49,530
So I'll create another div.

29
00:01:49,800 --> 00:01:51,400
So the class is going to be raw.

30
00:01:51,930 --> 00:01:58,680
And within this room, I will have another div and the class of this div is going to be called Dash

31
00:01:58,680 --> 00:02:04,710
MDD as well, meaning that it will actually go ahead, create a column and spend all the twelve columns

32
00:02:05,190 --> 00:02:06,390
which are actually present.

33
00:02:06,540 --> 00:02:12,410
So I will go ahead and kind of minimize this file explorer menu to give us more horizontal space here.

34
00:02:13,080 --> 00:02:20,430
So now once we have this specific column which spans twelve columns, I will go ahead and take this

35
00:02:20,430 --> 00:02:21,230
entire form.

36
00:02:21,810 --> 00:02:25,800
Cut it right away and piece it right in here.

37
00:02:26,280 --> 00:02:30,630
So when I piece this, this should look good.

38
00:02:30,750 --> 00:02:34,680
So as you can see, it actually aligned a little bit to the right.

39
00:02:35,250 --> 00:02:40,640
And now once we have pleased this thing inside this particular room.

40
00:02:40,740 --> 00:02:44,650
So the role of this particular form ends here, here.

41
00:02:44,910 --> 00:02:47,990
After this, we now need to go ahead and create another room.

42
00:02:48,150 --> 00:02:54,420
So even before creating another rule, let's actually go ahead and style the form which we have.

43
00:02:54,900 --> 00:02:58,470
So in order to style up the form, there are a few things which you need to do.

44
00:02:58,890 --> 00:03:05,310
So the first thing which you need to do is that to this particular form, you need to add something

45
00:03:05,310 --> 00:03:06,990
which is called as a form group.

46
00:03:07,320 --> 00:03:12,180
So right after the form tag, let's just go ahead and say div class equals.

47
00:03:12,810 --> 00:03:16,740
That's going to be form Dasch group.

48
00:03:17,280 --> 00:03:19,160
And this is actually going to be a rule.

49
00:03:19,740 --> 00:03:26,910
And now this rule or this div is now going to end right before the form tag ends.

50
00:03:27,420 --> 00:03:31,800
So I will go ahead and make this thing as an ending div tag.

51
00:03:32,250 --> 00:03:34,380
So this div will end right here.

52
00:03:34,620 --> 00:03:38,070
Now let's say we also want to add a label up over here.

53
00:03:38,460 --> 00:03:38,850
Oh it.

54
00:03:38,850 --> 00:03:41,310
See something like select food to add.

55
00:03:41,460 --> 00:03:43,410
So let's create a label up over here.

56
00:03:43,440 --> 00:03:44,820
Right before the select box.

57
00:03:45,210 --> 00:03:47,880
So I will go ahead and say something like label.

58
00:03:48,120 --> 00:03:49,710
So that will create a label tag.

59
00:03:50,220 --> 00:03:55,770
And here I won't have the fourth option, but instead I will see plus equals.

60
00:03:55,860 --> 00:03:56,610
Let's see.

61
00:03:57,540 --> 00:03:59,490
Call Dash MDI, dash two.

62
00:04:00,030 --> 00:04:05,820
So that will actually go ahead and create label which spans two columns.

63
00:04:06,270 --> 00:04:09,270
Now inside this label, I will add some text.

64
00:04:09,300 --> 00:04:17,329
So let's see this label CS select food to add.

65
00:04:18,180 --> 00:04:23,070
So now if I see this and hit refresh, it will see select food to add.

66
00:04:23,640 --> 00:04:27,790
And now as you can see, the button over here actually looks OK.

67
00:04:27,930 --> 00:04:29,340
So let's fix that as well.

68
00:04:29,430 --> 00:04:34,650
So now after adding this label, let's go ahead and also add a class to this thing as well.

69
00:04:35,100 --> 00:04:40,470
So now as we have used a column here, we'll also make use of a column over here as well.

70
00:04:40,490 --> 00:04:42,190
So I will see class.

71
00:04:42,660 --> 00:04:49,770
And in here I will see called Dash M.D. that sake's, which means that this particular select field

72
00:04:50,070 --> 00:04:52,470
will actually span six columns.

73
00:04:52,860 --> 00:04:57,240
And also, let's assign it a class called less form Dasch Control.

74
00:04:57,840 --> 00:04:59,600
So now let's get back and.

75
00:04:59,940 --> 00:05:00,210
Fresh.

76
00:05:00,300 --> 00:05:03,840
And as you can see now, this thing actually spans six columns.

77
00:05:04,500 --> 00:05:06,660
Now let's get back up over here again.

78
00:05:07,080 --> 00:05:13,410
And now over here, let's add a final touch to this particular submit button.

79
00:05:13,770 --> 00:05:15,630
So here the button type is submit.

80
00:05:15,720 --> 00:05:18,930
However, we also want to add a class to this as well.

81
00:05:18,990 --> 00:05:19,890
So I will see.

82
00:05:20,490 --> 00:05:24,270
Class is going to be equal to B.T. and which stands for a button.

83
00:05:24,660 --> 00:05:27,300
And let's say if you want to use a green button here.

84
00:05:27,330 --> 00:05:30,570
So I will see Beatty and dash success.

85
00:05:30,810 --> 00:05:34,140
So once you go ahead, add that up and hit refresh.

86
00:05:34,260 --> 00:05:37,290
As you can see, you have the add button up over here.

87
00:05:37,740 --> 00:05:39,980
Now, this thing actually looks a little bit bigger.

88
00:05:40,050 --> 00:05:43,950
That's because I'm actually using a small window here.

89
00:05:44,010 --> 00:05:47,730
However, if you expand this, this should look much more better.

90
00:05:47,910 --> 00:05:50,280
So let me just go ahead and zoom out a little bit.

91
00:05:51,060 --> 00:05:55,140
So, yeah, as I have zoomed out, as you can see, this is what it looks like.

92
00:05:55,680 --> 00:06:01,380
And you can also go ahead and make this text look a little bit bolder as well.

93
00:06:01,410 --> 00:06:08,580
So I will add a B Digo here for Bold and I will just get the text from here.

94
00:06:09,270 --> 00:06:10,320
Piece it up over here.

95
00:06:10,800 --> 00:06:12,960
So now if I go ahead, hit refresh.

96
00:06:13,470 --> 00:06:15,660
As you can see, this looks a lot better.

97
00:06:16,380 --> 00:06:19,050
So that's it for this specific lecture.

98
00:06:19,260 --> 00:06:25,440
However, in the upcoming lecture, we will go ahead and we will design the table for this specific

99
00:06:25,620 --> 00:06:26,130
items.

100
00:06:26,460 --> 00:06:28,200
So thank you very much for watching.

101
00:06:28,290 --> 00:06:30,030
And I'll see you guys next time.

102
00:06:30,330 --> 00:06:30,860
Thank you.

