1
00:00:00,120 --> 00:00:06,480
So in this lecture, we will be creating a view to sort of displayed these food items to our user so

2
00:00:06,480 --> 00:00:11,850
that the user can actually select these items and sort of add them to his consumption.

3
00:00:12,360 --> 00:00:16,560
So I've actually added two more items in here, which is brown bread and oats.

4
00:00:16,950 --> 00:00:21,090
So now let's go ahead and start building the view to display those items.

5
00:00:21,210 --> 00:00:24,440
So what I will do here is that open up visual studio code.

6
00:00:24,990 --> 00:00:30,870
And we need to go into the View store B y file and create a brand new view to display those items.

7
00:00:31,260 --> 00:00:36,960
So in order to actually go ahead and display those items of the will obviously be displaying them on

8
00:00:37,050 --> 00:00:39,510
our home page, which is going to be the next page.

9
00:00:39,900 --> 00:00:42,630
And I will create a view and name it as index.

10
00:00:42,720 --> 00:00:46,590
So Def Index, this is going to accept a request.

11
00:00:46,920 --> 00:00:52,830
And now the job of this view is to go ahead and get all the items which are actually present in the

12
00:00:52,890 --> 00:00:53,910
food model.

13
00:00:54,780 --> 00:00:57,960
So I will say food equals food.

14
00:00:58,110 --> 00:01:01,070
Daunte objects dot all.

15
00:01:01,230 --> 00:01:05,190
And this will actually get all the objects which are present in the food model.

16
00:01:05,489 --> 00:01:09,090
So we also need to make sure that we import the food model as well.

17
00:01:09,330 --> 00:01:15,480
So I will say from darte models import, that's going to be food.

18
00:01:16,260 --> 00:01:21,810
So once we have imported the model and once we have got all the objects from that model, we now need

19
00:01:21,810 --> 00:01:25,890
to go ahead and pass these objects to some sort of a template.

20
00:01:26,250 --> 00:01:28,830
So let's go ahead and say return rindo.

21
00:01:29,190 --> 00:01:34,200
And then here you need to pass on the request and then pass in the template name here.

22
00:01:34,350 --> 00:01:37,360
So as of now, we don't actually have any sort of template.

23
00:01:37,790 --> 00:01:43,410
However, we are going to create a template and next on each Gemal and henceforth I will directly go

24
00:01:43,410 --> 00:01:45,750
ahead and mentioned the part of that template.

25
00:01:46,230 --> 00:01:49,260
So this template is actually going to be present in my app.

26
00:01:49,350 --> 00:01:52,720
So I will see my app slash in next door ECMO.

27
00:01:53,520 --> 00:01:59,130
And now the next thing which we need to do is that to this index, not each Gemal template, you now

28
00:01:59,130 --> 00:02:03,330
need to go ahead and pass in the object list, which is food.

29
00:02:04,260 --> 00:02:12,450
And as this food actually contains multiple food objects, it's actually better if we name it as foods

30
00:02:12,480 --> 00:02:13,590
instead of just food.

31
00:02:14,160 --> 00:02:14,410
Okay.

32
00:02:14,490 --> 00:02:18,660
So now once we have these foods, let's actually pass them as context here.

33
00:02:18,690 --> 00:02:20,160
So I will see foods.

34
00:02:21,470 --> 00:02:23,180
Golden Foods.

35
00:02:23,780 --> 00:02:27,530
OK, so once we are done with this, we are done with creating The View.

36
00:02:27,830 --> 00:02:32,250
Now our job is to go ahead and set up the template, which is indexed on each Jammal.

37
00:02:32,480 --> 00:02:39,200
So now let's go to the my app and let's create a new folder or a directory and call it desk templates.

38
00:02:39,890 --> 00:02:45,440
And in this templates directory, let's actually go ahead and create a new directory called As My App.

39
00:02:46,130 --> 00:02:48,080
And now and this, my app directory.

40
00:02:48,140 --> 00:02:50,930
Let's create a new file and call it as index.

41
00:02:51,090 --> 00:02:52,170
Don't each shipment.

42
00:02:52,690 --> 00:02:52,960
Okay.

43
00:02:53,000 --> 00:02:58,400
So now once we have this and next chart each Gemal file, let's actually go ahead and add a few basic

44
00:02:58,400 --> 00:02:58,970
tags here.

45
00:02:59,480 --> 00:03:01,970
So first of all, I'll add the age Schimel tag.

46
00:03:02,300 --> 00:03:07,700
Then I will add the hash tag and then I'll add the body tag as well.

47
00:03:08,240 --> 00:03:11,870
So once we have the head and the body and now we just want to go ahead.

48
00:03:11,930 --> 00:03:17,870
And first of all, we want to list out all the food items which are actually passed from this index

49
00:03:17,870 --> 00:03:18,070
view.

50
00:03:18,350 --> 00:03:21,110
So those food items are actually present in foods.

51
00:03:21,500 --> 00:03:25,280
So let's loop through foods and get all of those items.

52
00:03:25,670 --> 00:03:29,240
So in order to loop through those items, I will use a for loop.

53
00:03:29,390 --> 00:03:32,540
So I will save for food in.

54
00:03:32,810 --> 00:03:34,190
That's going to be foods.

55
00:03:35,750 --> 00:03:37,910
And here I will end the fall.

56
00:03:38,030 --> 00:03:40,470
So I will say and fall here.

57
00:03:41,360 --> 00:03:46,480
And now in order to access the name of the food, I will simply say food Dutney.

58
00:03:47,570 --> 00:03:54,230
So now we just want to make sure if this indexed, each GMO is able to display the names of the food

59
00:03:54,260 --> 00:03:56,240
which are actually present in our list.

60
00:03:56,840 --> 00:04:02,750
So now in order to make sure that the view is functional, we also need to go ahead and complete one

61
00:04:02,750 --> 00:04:03,350
last step.

62
00:04:03,380 --> 00:04:07,040
And that step is to go ahead and link this view with whether you are the pattern.

63
00:04:07,580 --> 00:04:11,620
So in order to link this view with the you are the pattern you need to go into, you are A start.

64
00:04:11,620 --> 00:04:12,460
B, you are a file.

65
00:04:12,780 --> 00:04:16,339
And in here, first of all, you just need to go ahead and import views.

66
00:04:16,519 --> 00:04:21,529
So you need to see from my app input views.

67
00:04:22,010 --> 00:04:25,760
And once you have the views, let's go ahead and create a path for The View.

68
00:04:26,000 --> 00:04:30,470
So I would say path and let's say I want to display this on the homepage.

69
00:04:30,590 --> 00:04:34,640
So I will leave this empty and then I will see you start index.

70
00:04:34,690 --> 00:04:35,650
Come on, let's see.

71
00:04:35,690 --> 00:04:37,820
Name of the view is index.

72
00:04:37,910 --> 00:04:39,230
Give a comma at the end.

73
00:04:39,290 --> 00:04:40,670
And we should be good to go.

74
00:04:41,270 --> 00:04:44,630
So now let me make sure that the server is up and running.

75
00:04:45,290 --> 00:04:49,010
And now let's actually go to the homepage.

76
00:04:49,310 --> 00:04:55,780
So as you can see, as we go to the homepage, we are able to take a look at all of the names of the

77
00:04:55,790 --> 00:04:56,630
food items.

78
00:04:57,290 --> 00:05:00,080
So now they are actually displayed in one single line.

79
00:05:00,320 --> 00:05:05,480
And if you actually want to display them in different lines, you can simply add a brief tag over here.

80
00:05:06,320 --> 00:05:09,920
So once you do that and if you go back and hit refresh.

81
00:05:09,980 --> 00:05:13,100
As you can see, all of the three items are added up over here.

82
00:05:13,670 --> 00:05:15,530
So now this is all fine and good.

83
00:05:16,090 --> 00:05:18,470
You were actually able to display those food items.

84
00:05:18,500 --> 00:05:25,880
But we actually want to make our user be able to select these items and add them up to their own consumption

85
00:05:25,910 --> 00:05:26,960
or daily consumption.

86
00:05:27,140 --> 00:05:33,110
So in order to do that, we actually need to go ahead and create a form and sort of put all of these

87
00:05:33,170 --> 00:05:36,770
items inside the forms select box.

88
00:05:37,160 --> 00:05:40,310
So in the upcoming lecture, we are going to learn exactly the same.

89
00:05:40,430 --> 00:05:43,490
That is how to please these items inside a select box.

90
00:05:43,820 --> 00:05:45,680
So the user can actually select them.

91
00:05:46,190 --> 00:05:48,020
So thank you very much for watching.

92
00:05:48,140 --> 00:05:49,940
And I'll see you guys next time.

93
00:05:50,210 --> 00:05:50,760
Thank you.

