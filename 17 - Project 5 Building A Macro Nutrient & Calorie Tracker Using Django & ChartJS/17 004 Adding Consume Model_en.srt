1
00:00:00,570 --> 00:00:03,690
So now let's go ahead and in this particular lecture.

2
00:00:03,920 --> 00:00:07,770
Let's create a model which will save the users consumption list.

3
00:00:08,340 --> 00:00:14,730
So I hope that this thing actually makes sense, that we currently have a model which is actually just

4
00:00:14,730 --> 00:00:16,140
for storing the food items.

5
00:00:16,590 --> 00:00:22,110
However, you need to understand that the food items which are actually present inside this food model

6
00:00:22,590 --> 00:00:25,080
are actually the items which are present in our app.

7
00:00:25,170 --> 00:00:26,970
That is those are the database items.

8
00:00:27,000 --> 00:00:30,090
And those are not the items which are consumed by the user.

9
00:00:30,690 --> 00:00:38,340
So now the floor works in a way that out of these food items, the user is going to select one of the

10
00:00:38,340 --> 00:00:38,880
items.

11
00:00:39,120 --> 00:00:42,600
And that item is actually going to be consumed by the user.

12
00:00:42,750 --> 00:00:48,330
And henceforth, that item out of all the items which are present in the food model has to be added

13
00:00:48,330 --> 00:00:49,980
to the user's consumption list.

14
00:00:50,670 --> 00:00:58,710
So now our job is to go ahead and just create a new model which will store the items which are consumed

15
00:00:58,800 --> 00:00:59,850
by user.

16
00:01:00,900 --> 00:01:03,420
So let's go ahead and create that model up over here.

17
00:01:03,870 --> 00:01:09,810
So I will say a class, uh, let's name this consumption list model as, let's see, consume.

18
00:01:10,710 --> 00:01:13,980
So obviously, this will be model start model.

19
00:01:14,340 --> 00:01:21,510
And now inside this particular consume model, we need to save the food which is consumed by the user.

20
00:01:22,050 --> 00:01:24,960
So here I will create a field called Les Food.

21
00:01:25,110 --> 00:01:27,430
And those who consumed.

22
00:01:27,990 --> 00:01:31,740
And this is not actually going to be some sort of a text or anything like that.

23
00:01:31,830 --> 00:01:37,900
Instead, here, we actually want to save the item out of this food model, which we have here.

24
00:01:38,070 --> 00:01:44,340
And henceforth, what we will do here is that we will make use of something which is called as a foreign

25
00:01:44,340 --> 00:01:44,640
key.

26
00:01:44,910 --> 00:01:51,240
So a foreign key is nothing, but it's actually the primary key of the model, which we want to attach

27
00:01:51,240 --> 00:01:51,660
it to.

28
00:01:51,810 --> 00:01:57,900
So in case of food consumed, as we actually want to save the item which is already present inside the

29
00:01:57,900 --> 00:02:01,030
food table or inside the food module.

30
00:02:01,440 --> 00:02:06,360
We now need to associate this model with this model using a foreign key.

31
00:02:07,050 --> 00:02:13,040
So here, in order to add a foreign key would say modern store, foreign key.

32
00:02:13,890 --> 00:02:17,720
And then here you also need to mention for which particular model.

33
00:02:18,150 --> 00:02:19,770
You're adding a foreign key here.

34
00:02:20,040 --> 00:02:23,430
And also make sure that the key here is in capitals.

35
00:02:23,730 --> 00:02:29,700
So here, as we are using our mentioning the foreign key for the model, which is food, you also need

36
00:02:29,700 --> 00:02:31,530
to mention the name of that model here.

37
00:02:32,280 --> 00:02:38,580
So what this dates is that here we are using a foreign key, which is going to be nothing but primary

38
00:02:38,590 --> 00:02:40,920
key of the food model, which we have here.

39
00:02:41,760 --> 00:02:48,330
So the thing which began store here in food consumed is nothing but the object which is present in the

40
00:02:48,330 --> 00:02:48,990
food model.

41
00:02:49,950 --> 00:02:54,420
And the next parameter, which you need to mention here, is on underscore delete.

42
00:02:55,380 --> 00:02:57,450
And that's going to be equal to models start.

43
00:02:59,970 --> 00:03:00,590
Kaskade.

44
00:03:01,280 --> 00:03:01,460
OK.

45
00:03:01,710 --> 00:03:04,270
So once you are done with this, you are pretty much good to go.

46
00:03:04,670 --> 00:03:09,040
However, there's one more thing which you need to store here in the cans you model.

47
00:03:09,250 --> 00:03:15,400
Now, we have actually saved the food which is consumed, which is all fine and good.

48
00:03:15,490 --> 00:03:22,150
If our application was to be used by a single user, however, in our case, our application is going

49
00:03:22,150 --> 00:03:24,830
to be used by hundreds of users and hence forth.

50
00:03:25,480 --> 00:03:29,080
We also need to mention the user who consumes the food.

51
00:03:29,320 --> 00:03:35,890
So this Kansi model is going to have the data about the food which was consumed, as well as the user

52
00:03:35,920 --> 00:03:38,080
who consumed that food item.

53
00:03:38,530 --> 00:03:48,040
So here, in order to go ahead and also use the user or add user, oh, yo, we now need to go ahead

54
00:03:48,070 --> 00:03:51,000
and add another foreign key in the name of user.

55
00:03:51,430 --> 00:04:02,140
So I will say user equals model, start a foreign key and then you you actually need to add the user

56
00:04:02,140 --> 00:04:02,530
model.

57
00:04:02,920 --> 00:04:06,720
So the user model is actually an in built model which is present in Django.

58
00:04:06,820 --> 00:04:08,260
And you do need to import it.

59
00:04:08,740 --> 00:04:11,080
So first of all, let's import the user model.

60
00:04:11,140 --> 00:04:17,240
So I'll say from Django Tarkan Crib Dart or DART models.

61
00:04:17,399 --> 00:04:18,920
Import user.

62
00:04:19,000 --> 00:04:21,010
So this will actually import the user model.

63
00:04:21,490 --> 00:04:28,420
And now we are associating the user model with the consume model here so that we can find out what food

64
00:04:28,510 --> 00:04:29,590
the user consumed.

65
00:04:30,250 --> 00:04:33,760
So now in here we are going to do something of a similar sort.

66
00:04:33,850 --> 00:04:39,160
So we'll also have undelete equals models start Kaskade.

67
00:04:40,340 --> 00:04:45,050
OK, so now once this thing is done, we are pretty much done with the cans you model.

68
00:04:45,170 --> 00:04:50,510
Now the cans you model might actually sound a little bit different or a little bit weird.

69
00:04:50,600 --> 00:04:53,000
That's because we have not used foreign oil yet.

70
00:04:53,420 --> 00:04:59,420
But always remember that the foreign key is actually used to make connections or associate two models

71
00:04:59,420 --> 00:04:59,870
together.

72
00:05:00,500 --> 00:05:07,290
So we actually want this consumer model to be associated with user because we want to find out what

73
00:05:07,290 --> 00:05:09,320
the user is consuming something.

74
00:05:09,590 --> 00:05:15,950
And we have also associated the Kansi model with the food consumed or the food model as well, so as

75
00:05:15,950 --> 00:05:18,490
to know what sort of food is the user consuming.

76
00:05:18,950 --> 00:05:26,900
So you can see that the consume model is sort of like a middleman between the food model and the user

77
00:05:26,900 --> 00:05:27,260
model.

78
00:05:27,590 --> 00:05:32,600
So the job of this model is to go ahead and give you an idea about which user consumed what.

79
00:05:33,680 --> 00:05:38,690
Okay, so now once we have created this model, our next job is to go ahead and make migration's.

80
00:05:38,870 --> 00:05:43,580
So I guess I have misspelled foreign key here.

81
00:05:44,660 --> 00:05:46,310
So extremely sorry for that.

82
00:05:47,840 --> 00:05:48,770
That should be.

83
00:05:51,450 --> 00:05:52,800
That should be foreign key.

84
00:05:53,430 --> 00:05:56,030
And let me place this up over here as well.

85
00:05:56,420 --> 00:05:57,930
Okay, so that would work just fine.

86
00:05:58,680 --> 00:06:00,540
Okay, so now let's make the migration.

87
00:06:00,540 --> 00:06:04,470
So I will say Python three managed to be PVR.

88
00:06:06,020 --> 00:06:08,060
MC Migration's.

89
00:06:11,420 --> 00:06:14,950
And Python three man has stopped by my.

90
00:06:15,890 --> 00:06:18,050
Okay, so now wants to migrations are made.

91
00:06:18,590 --> 00:06:21,290
Now let's actually go ahead and run the server.

92
00:06:21,470 --> 00:06:23,680
So Python three managed Stoppie.

93
00:06:23,730 --> 00:06:26,260
Why run so well?

94
00:06:27,050 --> 00:06:30,350
And now I can actually go to the admin panel.

95
00:06:31,350 --> 00:06:32,670
So slash admen.

96
00:06:35,580 --> 00:06:41,220
And then here, currently, you won't be able to see the model here because we need to register that

97
00:06:41,220 --> 00:06:43,600
model up over here in Admon Dot Pi.

98
00:06:44,160 --> 00:06:55,500
So I will import the current ZEW model here and just see admen, the outside dog register.

99
00:06:55,740 --> 00:06:57,140
That's going to be consumed.

100
00:06:58,200 --> 00:06:58,410
OK.

101
00:06:58,560 --> 00:07:03,210
So now if we go back and refresh, as you can see, we will have the consumer model here.

102
00:07:03,270 --> 00:07:09,210
If I open this thing up and if I click on ADD Consume, as you can see, it will give me an option to

103
00:07:09,210 --> 00:07:10,050
select a user.

104
00:07:10,260 --> 00:07:14,220
So currently, as our site only has one user and that is the super user.

105
00:07:14,280 --> 00:07:16,140
It will give me an option to select that.

106
00:07:16,620 --> 00:07:22,200
And now I can select any food which is consumed so I can see that the super user consume brown bread.

107
00:07:22,920 --> 00:07:26,370
So if I click on Save, that will actually create this consume object.

108
00:07:26,880 --> 00:07:33,390
So if I open this thing up, it will say that the user, which is super user, consume this item.

109
00:07:34,350 --> 00:07:37,050
And I can now go ahead and create another consume.

110
00:07:37,680 --> 00:07:40,380
I can select the user and add another food item.

111
00:07:40,830 --> 00:07:46,800
So if I actually had multiple users here, I can select another user and I can select any other user

112
00:07:46,800 --> 00:07:47,160
item.

113
00:07:47,370 --> 00:07:53,040
So now if you go to the consume table, you'll be able to find all the items which are consumed by a

114
00:07:53,040 --> 00:07:53,940
specific user.

115
00:07:54,180 --> 00:07:57,060
And now making use of these consume objects.

116
00:07:57,480 --> 00:08:03,120
Now we have access to all the food items which are consumed by any specific user.

117
00:08:04,530 --> 00:08:11,580
So in the upcoming lecture, what we will do is that Aspey have a functionality or heel to sort of go

118
00:08:11,580 --> 00:08:15,660
ahead and select any items or make the user consume an item.

119
00:08:16,110 --> 00:08:20,170
We'll actually develop this functionality in our Django application.

120
00:08:20,250 --> 00:08:23,340
So currently, if you take a look at an application.

121
00:08:23,400 --> 00:08:25,480
So if you would go to the home page.

122
00:08:26,430 --> 00:08:29,700
So currently the select box actually does nothing.

123
00:08:30,210 --> 00:08:33,809
So here in the upcoming lecture, we will add an add button here.

124
00:08:33,990 --> 00:08:40,110
And as soon as the user clicks the add button, this food item is going to be added to his daily consumption

125
00:08:40,110 --> 00:08:40,530
list.

126
00:08:41,309 --> 00:08:43,120
So thank you very much for watching.

127
00:08:43,260 --> 00:08:45,150
And I'll see you guys next time.

128
00:08:45,390 --> 00:08:45,900
Thank you.

