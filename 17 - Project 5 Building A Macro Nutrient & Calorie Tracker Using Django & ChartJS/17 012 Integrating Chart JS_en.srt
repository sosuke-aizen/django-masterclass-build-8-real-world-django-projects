1
00:00:00,180 --> 00:00:03,870
In this lecture, let's go ahead and start adding chart over here.

2
00:00:04,110 --> 00:00:10,090
So in order to add a chart over here, we will be using each our script library class chart.

3
00:00:10,300 --> 00:00:10,810
Yes.

4
00:00:11,280 --> 00:00:19,290
So simply go to Google or simply type in charges and your you are toolbar and the first language pops

5
00:00:19,290 --> 00:00:22,830
up is going to be the official link of the charges library.

6
00:00:23,340 --> 00:00:28,800
So if you go in there, it will actually introduce you to what exactly Chantey says.

7
00:00:28,890 --> 00:00:34,200
So it's a simple yet flexible show us script charting for designers and developers.

8
00:00:34,590 --> 00:00:39,660
So while creating Web applications, many of times you would actually need to create charts.

9
00:00:40,110 --> 00:00:46,350
Now, creating chances are actually quite tricky because it involves making use of some graphics, making

10
00:00:46,350 --> 00:00:48,420
use of the canvas, so on and so forth.

11
00:00:48,810 --> 00:00:54,990
Therefore, charges has actually made it quite simple by providing us with their own library, which

12
00:00:54,990 --> 00:00:56,610
we can use to create charts.

13
00:00:57,030 --> 00:01:04,170
So now in order to go ahead and create a nutrition chart here, we will be making use of this library,

14
00:01:04,200 --> 00:01:05,810
which is the charges library.

15
00:01:06,270 --> 00:01:11,010
So now in order to go ahead and get started with charges, I will click on this option.

16
00:01:11,890 --> 00:01:17,130
I would seize and get started and now it will actually give me this page.

17
00:01:17,220 --> 00:01:20,660
And here it will actually tell you the installation steps.

18
00:01:21,090 --> 00:01:26,540
So you could either download charges or you could use something which is called as a N.

19
00:01:26,640 --> 00:01:29,670
So Swedien stands for Content Delivery Network.

20
00:01:30,060 --> 00:01:34,800
And this will actually give you a link which you can use in your project.

21
00:01:35,250 --> 00:01:40,800
So simply go ahead, click on this link and it will actually open up a new page.

22
00:01:41,220 --> 00:01:46,680
And once you go to this particular Web page, you should be able to find these links right here.

23
00:01:46,830 --> 00:01:52,860
So in order to actually get this link, I will simply go ahead and I will say something like copy each

24
00:01:52,860 --> 00:01:53,340
Gemal.

25
00:01:53,850 --> 00:01:59,940
So now once you have this each Gemal with you, you simply need to go ahead and go here and you need

26
00:01:59,940 --> 00:02:04,980
to add this particular script tag right over here and the head tag.

27
00:02:05,550 --> 00:02:09,720
So now once you add that you now have access to the chance she is library.

28
00:02:09,960 --> 00:02:16,020
And now in order to actually add some charge to your site, you could go ahead and you could copy this

29
00:02:16,020 --> 00:02:17,910
entire code, which we have over here.

30
00:02:18,060 --> 00:02:20,310
Now, this code is actually for a bar chart.

31
00:02:20,700 --> 00:02:24,720
So as of now, I'm simply going to go ahead, copy this particular line.

32
00:02:24,840 --> 00:02:26,940
So this is actually just a simple chart.

33
00:02:27,540 --> 00:02:34,320
So if I go ahead and let's actually piece this over here in this particular column.

34
00:02:36,800 --> 00:02:39,740
And once you piece that thing in, let's save it.

35
00:02:40,040 --> 00:02:43,550
Go back to our Web page and see how it looks like.

36
00:02:44,030 --> 00:02:45,980
So now if I go ahead and hit refresh.

37
00:02:46,100 --> 00:02:49,100
As you can see, we don't have any sort of Charton here.

38
00:02:49,620 --> 00:02:55,460
That's because this particular thing only adds a chart, which is going to be an empty chart.

39
00:02:55,820 --> 00:03:02,690
So this right here is actually just a blank canvas with an I.D., my chart having this with and this

40
00:03:02,690 --> 00:03:03,050
height.

41
00:03:03,530 --> 00:03:08,300
Now, in order to actually fill up this chart, you would need to go ahead and write in some JavaScript.

42
00:03:08,570 --> 00:03:12,530
So here we have some ready-Made JavaScript, which we can use.

43
00:03:12,770 --> 00:03:15,170
So I'm going to simply copy this for now.

44
00:03:15,980 --> 00:03:24,710
Go back over here to our code and go right below where the script ends and paste this good.

45
00:03:25,250 --> 00:03:26,000
Right over here.

46
00:03:26,180 --> 00:03:29,030
Just make sure that you are able to distinguish the chart code.

47
00:03:29,420 --> 00:03:31,490
But the other challenge, script code, which you have.

48
00:03:31,610 --> 00:03:33,020
So now if you get back.

49
00:03:33,080 --> 00:03:39,290
And if you hit refresh, as you can see, and now you have this type of bar chart right up over here.

50
00:03:39,650 --> 00:03:43,220
Now, this is a bar chart, however, to visualize the macronutrients.

51
00:03:43,700 --> 00:03:46,910
Let's say we want a pie chart or a doughnut chart.

52
00:03:47,390 --> 00:03:50,570
So in order to get a doughnut, you can simply go ahead.

53
00:03:50,960 --> 00:03:52,310
You can scroll on a little bit.

54
00:03:52,850 --> 00:03:55,460
And here you will have the option for donut and pie.

55
00:03:56,030 --> 00:04:01,520
So if you click on that, as you can see now, you actually have this thing as a doughnut.

56
00:04:02,360 --> 00:04:07,760
So in order to create a donut chart or a pie chart, you would use this kind of code.

57
00:04:08,270 --> 00:04:14,420
So the only change which you need to make to any bar graph to change its type is that you change this

58
00:04:14,420 --> 00:04:16,490
type variable, which you have in your code.

59
00:04:17,029 --> 00:04:20,209
So now as we want a doughnut, I will simply copy that.

60
00:04:20,630 --> 00:04:26,970
And here, if you would call it here and change the type two donut.

61
00:04:27,260 --> 00:04:28,070
Save the code.

62
00:04:28,400 --> 00:04:29,180
Get back here.

63
00:04:30,140 --> 00:04:34,550
And now, if you hit refresh, as you can see, you will have this nice looking doughnut chart.

64
00:04:35,120 --> 00:04:40,730
Now, the problem, though, here is that if you actually look at the data here, this data is nothing

65
00:04:40,760 --> 00:04:44,330
but the random data which we have up over here.

66
00:04:44,780 --> 00:04:48,740
So now we need to understand how this chargee is good works.

67
00:04:49,130 --> 00:04:51,680
So let's go ahead and understand each and every line.

68
00:04:52,040 --> 00:04:54,230
So let's take a look at the first line.

69
00:04:54,770 --> 00:04:58,200
So this line is nothing, but this line is to get the context.

70
00:04:58,340 --> 00:05:04,130
That is to get this particular canvas element right here so that JavaScript would be able to go ahead

71
00:05:04,160 --> 00:05:05,630
and draw something upon it.

72
00:05:06,410 --> 00:05:08,300
And that's what is actually done here.

73
00:05:08,330 --> 00:05:12,560
We have got the context of the chart using its I.D., which is my chart.

74
00:05:12,980 --> 00:05:17,030
So we have used the get element by idee method to get my chart.

75
00:05:17,390 --> 00:05:20,600
And then we have got the context of that particular chart.

76
00:05:21,080 --> 00:05:25,010
Now, once we have this context, that is once you have the access to the can.

77
00:05:25,640 --> 00:05:28,850
Now you can actually go ahead and draw a actual chart.

78
00:05:29,090 --> 00:05:32,300
So here, as you can see, we have created a new chart object.

79
00:05:32,810 --> 00:05:34,490
We have passed in the context here.

80
00:05:35,000 --> 00:05:39,080
And after the context, you need to specify the attributes of the chart.

81
00:05:39,350 --> 00:05:43,160
So the attributes of the chart are nothing but the kind of the chart.

82
00:05:43,340 --> 00:05:46,910
So in this case, we are using the chart of the type Donat henceforth.

83
00:05:47,240 --> 00:05:48,470
We have Donat right here.

84
00:05:48,950 --> 00:05:53,750
And after specifying the type, you would also need to specify the data as well.

85
00:05:53,780 --> 00:05:57,170
Because without the data, your chart is pretty much useless.

86
00:05:57,590 --> 00:06:02,480
The only reason why we use chart is to represent or visualize data or information.

87
00:06:02,840 --> 00:06:07,850
Hence what you do need to go ahead and mentioned the labels and the data as well.

88
00:06:08,180 --> 00:06:09,830
So that's what's actually done here.

89
00:06:10,280 --> 00:06:14,330
Here we have labels, which is red, blue, yellow, green, purple and orange.

90
00:06:14,540 --> 00:06:18,130
And as you can see, these labels are listed up or here.

91
00:06:18,740 --> 00:06:24,660
So now in Akis, we actually want to go ahead and sort of get our very own label.

92
00:06:24,690 --> 00:06:26,960
So let's go ahead and replace these labels.

93
00:06:27,410 --> 00:06:32,270
So I would say carbs, protein and fats.

94
00:06:32,900 --> 00:06:37,550
So we only want three labels, so I will get rid of the rest of the labels right here.

95
00:06:37,820 --> 00:06:39,830
So now if I go ahead, hit refresh.

96
00:06:40,250 --> 00:06:43,010
As you can see now, we have only three labels here.

97
00:06:43,880 --> 00:06:48,790
Now, if you scroll down a little bit, as you can see, you have something we just call as a dataset

98
00:06:48,820 --> 00:06:49,080
here.

99
00:06:49,280 --> 00:06:52,910
And the dataset is nothing, but it actually represents your data.

100
00:06:53,150 --> 00:06:58,540
So the first parameter in the dataset is, again, label how it will be are not concerned with that

101
00:06:58,550 --> 00:07:01,010
because it does not affect our charts so much.

102
00:07:01,400 --> 00:07:05,930
So now let's move on to the next part, which is one of the most important parts right here, which

103
00:07:05,930 --> 00:07:07,940
is nothing but the data.

104
00:07:08,150 --> 00:07:09,770
So now let's talk about this data.

105
00:07:09,860 --> 00:07:14,510
So the first value in this particular data entry is 12.

106
00:07:14,630 --> 00:07:19,190
So if you hover over this, as you can see, it says that carbs are twelve.

107
00:07:19,430 --> 00:07:20,390
And that's because.

108
00:07:21,790 --> 00:07:25,480
Carbs value correspond to this value, which we have here.

109
00:07:26,110 --> 00:07:31,120
So by that logic, 19 should be for protein and three should be for fats.

110
00:07:31,630 --> 00:07:32,710
So let's check that.

111
00:07:32,920 --> 00:07:34,990
So, yeah, 19 is for protein.

112
00:07:35,380 --> 00:07:36,610
And three years for fats.

113
00:07:36,970 --> 00:07:39,190
And the rest of the things are undefined.

114
00:07:39,580 --> 00:07:43,110
So that means we no longer need these three values.

115
00:07:44,140 --> 00:07:46,600
And now these values alone should work.

116
00:07:46,720 --> 00:07:47,920
So if I hit refresh.

117
00:07:48,010 --> 00:07:50,680
As you can see, we have these three values right here.

118
00:07:51,490 --> 00:07:56,140
Now, the problem with this is that we don't want these values right here.

119
00:07:56,380 --> 00:08:03,160
Instead, we actually want to represent the carbohydrates, proteins and fats values in terms of the

120
00:08:03,160 --> 00:08:03,940
percentage.

121
00:08:04,390 --> 00:08:10,810
So let's say, for example, I want to calculate the percentage of carbohydrates consumed, the percentage

122
00:08:10,810 --> 00:08:13,720
of fats and the percentage of protein consumed.

123
00:08:14,200 --> 00:08:15,340
I now need to go ahead.

124
00:08:15,430 --> 00:08:20,050
And first of all, calculate the individual percentages.

125
00:08:20,440 --> 00:08:22,360
So now let's go ahead and do that.

126
00:08:22,480 --> 00:08:28,480
So in order to do that, I can go right up over here and I can create a couple of variables in here.

127
00:08:28,690 --> 00:08:34,179
So, first of all, I need to go ahead and calculate the sum of cops, proteins and fats.

128
00:08:34,600 --> 00:08:38,200
So for that, I'll create a variable and I'll call it a total.

129
00:08:38,350 --> 00:08:40,210
So I'll save the total equals.

130
00:08:40,720 --> 00:08:46,270
And now, in order to get the individual values of cops, proteins and fats, we already have those

131
00:08:46,270 --> 00:08:47,290
values up over here.

132
00:08:48,160 --> 00:08:51,130
So now it's only the matter of summing them up.

133
00:08:51,220 --> 00:08:56,950
So I will say carbs plus protein plus fats.

134
00:08:57,910 --> 00:09:03,940
So once we have these values now, I can actually go ahead and calculate the percentage amount of each

135
00:09:03,970 --> 00:09:04,510
one of them.

136
00:09:05,050 --> 00:09:06,890
So I will say, well, let's see.

137
00:09:07,010 --> 00:09:10,540
Carbs P as the variable name for carbs percentage.

138
00:09:11,050 --> 00:09:14,950
Now, in order to calculate the percentage, it's actually quite simple.

139
00:09:15,370 --> 00:09:18,490
I could simply do carbs divided by total.

140
00:09:18,670 --> 00:09:20,530
So that will give me the average.

141
00:09:20,620 --> 00:09:24,110
And to get the percentage, I will simply multiply it by 100.

142
00:09:24,760 --> 00:09:27,880
However, this will actually give me a floating point value.

143
00:09:28,210 --> 00:09:32,770
And henceforth I will enclose this entire thing in the math.

144
00:09:32,890 --> 00:09:34,710
Dortch around function.

145
00:09:34,780 --> 00:09:36,430
All math don't round method.

146
00:09:37,000 --> 00:09:43,240
So now this will actually give me the percentage of carbohydrates in a as an integer value.

147
00:09:43,660 --> 00:09:46,570
So I will do the same thing for protein and fats as well.

148
00:09:47,140 --> 00:09:50,080
So this is actually going to be protein.

149
00:09:51,070 --> 00:09:53,320
This is going to be fats.

150
00:09:54,010 --> 00:09:56,230
And now let's change this thing as well.

151
00:09:56,950 --> 00:09:58,770
So that's going to be protein.

152
00:09:59,440 --> 00:10:01,630
That's going to be France.

153
00:10:02,290 --> 00:10:02,520
Okay.

154
00:10:02,650 --> 00:10:08,590
So now this will actually use the values for carbs, percentage, protein percentage and fat percentage.

155
00:10:09,010 --> 00:10:13,870
Now, the only thing which we need to do is that now, instead of having these random values in here,

156
00:10:14,410 --> 00:10:21,960
I need to replace this with carbs, B, protein B and the final one as fat B.

157
00:10:22,420 --> 00:10:27,070
OK, so now once that thing is done, we are now pretty much good to go.

158
00:10:27,490 --> 00:10:33,490
Now, if I switch back and hit refresh, as you can see now, the content has been modified.

159
00:10:33,880 --> 00:10:40,540
So it actually gave me a total breakdown that the carbohydrates are 65 percent, protein is 23 percent

160
00:10:40,600 --> 00:10:42,310
and fats are twelve percent.

161
00:10:42,730 --> 00:10:46,930
So you can also cross verify this by actually comparing these values right here.

162
00:10:46,960 --> 00:10:48,100
So if you take a look.

163
00:10:48,400 --> 00:10:49,660
Carbs are the most.

164
00:10:50,080 --> 00:10:53,110
Then we have protein and then we have the least number of fats.

165
00:10:53,620 --> 00:10:58,060
And if you calculate this, hopefully this should come out to be correct.

166
00:10:58,780 --> 00:11:04,660
Now, the next question is, why do we have these background, color and body color values?

167
00:11:05,170 --> 00:11:10,120
So these values are the color values for carbohydrates, proteins and fats.

168
00:11:10,390 --> 00:11:12,910
That means these three values actually go well.

169
00:11:13,060 --> 00:11:18,880
These three data, and you can actually get rid of these values, which we have here.

170
00:11:19,150 --> 00:11:22,870
So I will get rid of them and I'll get rid of them as well.

171
00:11:23,980 --> 00:11:27,760
So now if I saved them, they should still be working fine.

172
00:11:28,060 --> 00:11:33,730
And one more thing which you could do here is that as these lines no longer make sense, I can simply

173
00:11:33,730 --> 00:11:36,730
go ahead and get rid of these options right here.

174
00:11:37,750 --> 00:11:39,560
So once I go ahead, do that.

175
00:11:39,760 --> 00:11:42,070
Oh, we should be having a clean looking chat.

176
00:11:42,640 --> 00:11:48,640
Now, if you want to also display the percentage of carbs, proteins and fats up over here, you can

177
00:11:48,640 --> 00:11:49,570
do that as well.

178
00:11:49,990 --> 00:11:55,600
So now inside the labels, instead of just displaying the carbs, I can see this.

179
00:11:55,600 --> 00:12:03,700
Plus display the cops percentage as well and append a nice percentage sign in front of it.

180
00:12:04,360 --> 00:12:10,510
So if I do percentage over here, that refresh, as you can see, it will see carbs 65 percent.

181
00:12:11,110 --> 00:12:14,650
I can also have some space in here so that it looks better.

182
00:12:14,920 --> 00:12:18,200
And as you can see, it will now display the comps value here.

183
00:12:18,550 --> 00:12:20,890
So I can do the same thing with protein as well.

184
00:12:21,570 --> 00:12:26,070
I can simply copy this, paste it up over here.

185
00:12:26,250 --> 00:12:27,480
Replace it with.

186
00:12:28,900 --> 00:12:30,190
Protein percentage.

187
00:12:30,250 --> 00:12:33,000
And do the same thing with fats as well.

188
00:12:35,770 --> 00:12:41,330
OK, so now once we are done with this, as you can see, those values are now represented up over here.

189
00:12:41,780 --> 00:12:47,210
So now the final thing which I want to discuss is how to change the colors over here.

190
00:12:48,050 --> 00:12:55,460
So the color values represented over here are actually IGB, a color values, and these values can actually

191
00:12:55,460 --> 00:12:56,180
be changed.

192
00:12:56,390 --> 00:12:59,780
And this thing, which we have right here is actually the obesity.

193
00:13:00,170 --> 00:13:05,870
So if you actually want to make it transparent, you can set this thing closer to zero.

194
00:13:05,990 --> 00:13:09,590
And if you want to make this thing solid, you can set it equal to one.

195
00:13:09,620 --> 00:13:13,340
So let's go ahead and let's try setting these values to one.

196
00:13:13,790 --> 00:13:15,170
And let's see what happens.

197
00:13:15,230 --> 00:13:20,870
So if I do that and if I had to refresh, as you can see, this is what our chart looks like.

198
00:13:21,260 --> 00:13:27,380
So if you want to replace these cholo values with some other color values, you can simply go ahead

199
00:13:27,440 --> 00:13:31,570
and search for IGB color.

200
00:13:33,060 --> 00:13:33,600
Pyka.

201
00:13:35,730 --> 00:13:37,580
Or IGB Akallo values.

202
00:13:37,740 --> 00:13:44,310
And you should be able to go ahead and find some tool which will actually help you to pick some IGB

203
00:13:44,340 --> 00:13:44,840
colors.

204
00:13:45,390 --> 00:13:48,150
So there are plenty of tools available which you can use.

205
00:13:48,660 --> 00:13:51,630
You could use any one of them to pick your own color.

206
00:13:51,900 --> 00:13:54,210
And you can change the color of your chart.

207
00:13:54,450 --> 00:13:59,370
So as of now, I'll keep these values as it is because it looks pretty decent.

208
00:13:59,610 --> 00:14:05,060
So now once we are done with this, we are pretty much done designing the chart and our calorie counter

209
00:14:05,100 --> 00:14:06,600
application is almost ready.

210
00:14:07,020 --> 00:14:10,980
However, there is one small functionality which we need to add up over here.

211
00:14:11,460 --> 00:14:15,570
And that functionality is the functionality to be able to delete an item.

212
00:14:15,750 --> 00:14:20,310
So now we have the ability to go ahead and add some item in here.

213
00:14:20,700 --> 00:14:26,760
However, now we also need to know how to delete certain items from this particular table.

214
00:14:27,240 --> 00:14:30,450
So we are going to learn about that in the upcoming lecture.

215
00:14:30,540 --> 00:14:32,400
So thank you very much for watching.

216
00:14:32,490 --> 00:14:34,350
And I'll see you guys next time.

217
00:14:34,770 --> 00:14:35,340
Thank you.

