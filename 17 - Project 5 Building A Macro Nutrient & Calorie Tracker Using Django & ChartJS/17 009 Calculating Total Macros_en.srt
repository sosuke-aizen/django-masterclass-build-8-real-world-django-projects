1
00:00:00,210 --> 00:00:06,689
So now, once we actually have all of these statistics displayed on our Web page, the next thing is

2
00:00:06,689 --> 00:00:12,690
to go ahead and calculate the total number of carbohydrates, proteins, fats and calories consumed

3
00:00:12,690 --> 00:00:15,270
by a user in a specific day.

4
00:00:15,720 --> 00:00:21,840
So currently we have all of these values here, but we now need to somehow go ahead and add them up.

5
00:00:22,410 --> 00:00:25,740
And there are actually multiple ways to add these values.

6
00:00:25,770 --> 00:00:32,670
One way is that you can actually go ahead, go through each of the database item or the Django objects

7
00:00:32,940 --> 00:00:35,190
and get these individual values here.

8
00:00:35,340 --> 00:00:39,480
And you can straightaway go ahead and calculate its total.

9
00:00:39,720 --> 00:00:41,660
However, there is one another way.

10
00:00:41,700 --> 00:00:45,900
And that's much easier way to calculate the total of these values.

11
00:00:45,990 --> 00:00:48,300
And that's by using some JavaScript.

12
00:00:48,420 --> 00:00:54,480
So what we can do here is that as all of these values are actually rendered on a Web page, which is

13
00:00:54,510 --> 00:01:01,470
the HMO APJ, we can make use of some JavaScript to kind of go ahead, get access to the document object

14
00:01:01,470 --> 00:01:05,670
model and get individual items or individual values from here.

15
00:01:06,030 --> 00:01:09,750
And we can add them up and calculate the total number of macros.

16
00:01:10,230 --> 00:01:12,210
So now let's go ahead and learn.

17
00:01:12,420 --> 00:01:13,920
How exactly can we do that?

18
00:01:14,340 --> 00:01:19,020
So as of now, all the values which we have here are actually present in the stable.

19
00:01:19,380 --> 00:01:24,930
And if you might recall, we have actually assigned an I.D. to this specific table.

20
00:01:25,350 --> 00:01:31,620
So if you actually have a look at the table here, we have the table I.D. for this specific table.

21
00:01:31,740 --> 00:01:38,790
So now making use of the same I.D., we can get access to the document object model and we can get access

22
00:01:38,790 --> 00:01:41,250
to the table using the document object model.

23
00:01:41,640 --> 00:01:43,770
So let's go ahead and learn how to do that.

24
00:01:44,700 --> 00:01:50,370
So as we are doing this in JavaScript, what we will do is that we will go right where our body tag

25
00:01:50,400 --> 00:01:51,600
ends, which is.

26
00:01:51,870 --> 00:01:53,250
And this particular line.

27
00:01:53,370 --> 00:01:59,250
And then here will go ahead and create a script tag, which is actually going to contain JavaScript.

28
00:01:59,610 --> 00:02:02,170
So I will have the script opening and closing tag.

29
00:02:02,610 --> 00:02:04,770
And let's start writing the JavaScript.

30
00:02:04,780 --> 00:02:05,100
Good.

31
00:02:05,430 --> 00:02:10,080
So our first job is to go ahead and get access to this table using its I.D..

32
00:02:10,410 --> 00:02:14,540
So in order to do that, I will create a variable class table.

33
00:02:14,820 --> 00:02:17,010
I will say this equals document.

34
00:02:17,310 --> 00:02:20,400
Dot can't element by I.D..

35
00:02:20,580 --> 00:02:23,780
And we actually want an element whose I.D. is table.

36
00:02:24,240 --> 00:02:26,150
So I will pass and table up or here.

37
00:02:26,750 --> 00:02:26,980
Okay.

38
00:02:27,030 --> 00:02:33,240
So now once you have these stable now you need to get the values of these following items.

39
00:02:33,250 --> 00:02:35,400
So you actually have a role here.

40
00:02:35,430 --> 00:02:41,280
And then you have these individual columns in here for the carbohydrates, proteins, fats and calories.

41
00:02:41,640 --> 00:02:48,180
So now what we wish to do is that we wish to loop through all of these values one by one, and we wish

42
00:02:48,180 --> 00:02:52,530
to add them up and henceforth in order to go ahead and add them up.

43
00:02:52,800 --> 00:02:54,780
Let's go ahead and create some variables.

44
00:02:55,110 --> 00:03:00,300
So I will say that let's say cops equals zero.

45
00:03:00,600 --> 00:03:03,150
So let's force initialize all of them to zero.

46
00:03:04,110 --> 00:03:06,840
Protein equals CEDO.

47
00:03:08,190 --> 00:03:13,230
Farts equals zero and calories equals zero.

48
00:03:13,680 --> 00:03:15,960
So now once we have these variables.

49
00:03:16,350 --> 00:03:20,550
Now let's learn how to go ahead and get access to these individual values.

50
00:03:20,910 --> 00:03:22,800
So as these values.

51
00:03:22,890 --> 00:03:30,000
So if you take a look at this value here, you actually need to go ahead and loop through the rules

52
00:03:30,030 --> 00:03:31,950
which are actually present inside the stable.

53
00:03:32,010 --> 00:03:33,760
So this thing right here is one row.

54
00:03:33,820 --> 00:03:35,430
This thing right here is the second row.

55
00:03:35,460 --> 00:03:36,720
This thing is the third row.

56
00:03:37,050 --> 00:03:38,310
So on and so forth.

57
00:03:38,670 --> 00:03:43,320
So in order to loop through all of those rows, let's create a for loop here.

58
00:03:43,650 --> 00:03:44,760
So I will say four.

59
00:03:45,090 --> 00:03:51,600
So remember that this is a chow script syntax and not the Python or Django or the chinja syntax.

60
00:03:51,930 --> 00:03:53,610
So we'll create a variable.

61
00:03:54,000 --> 00:03:58,500
So let's start the count from I equals one.

62
00:03:58,560 --> 00:04:05,010
And that's because if you talk about the zero through the zero through is nothing but this header roll

63
00:04:05,010 --> 00:04:05,550
right here.

64
00:04:05,600 --> 00:04:08,010
And we are not actually interested in that.

65
00:04:08,490 --> 00:04:10,350
So now let's start with one.

66
00:04:11,410 --> 00:04:17,769
And now if you actually go ahead and initialize the value to one, the count will start from that value.

67
00:04:18,070 --> 00:04:24,760
And now we want to loop unless and until we actually end the number of rows which are present in the

68
00:04:24,760 --> 00:04:25,090
table.

69
00:04:25,150 --> 00:04:32,740
So I will say, while I as less than and now in order to get the total number of rows inside our table,

70
00:04:32,890 --> 00:04:37,270
I would do table, which will actually give me access to this table right here.

71
00:04:37,300 --> 00:04:39,720
So I will see table dot rules.

72
00:04:40,450 --> 00:04:43,130
So this will actually give me access to the table rules.

73
00:04:43,300 --> 00:04:48,250
And now in order to calculate its length, I will do dot length.

74
00:04:48,850 --> 00:04:51,820
So this will actually give me the length of the stable.

75
00:04:51,940 --> 00:04:59,020
However, we want to go ahead and stop at one less than that because we are excluding this particular

76
00:04:59,020 --> 00:04:59,700
rule right there.

77
00:05:00,250 --> 00:05:05,020
So I will see this minus one and I plus plus.

78
00:05:05,320 --> 00:05:06,640
So once we have this loop.

79
00:05:07,060 --> 00:05:11,800
Now let's try to go ahead and actually print out the table rules and let's see what do we get.

80
00:05:12,220 --> 00:05:15,650
So when I do console dot log.

81
00:05:16,750 --> 00:05:20,290
And now let's actually print out the entire rule over here.

82
00:05:20,320 --> 00:05:22,570
So I will see table dot.

83
00:05:23,290 --> 00:05:25,440
Let's see rows.

84
00:05:25,600 --> 00:05:28,750
And let's actually pass in the index value for foru as I.

85
00:05:28,840 --> 00:05:32,410
So this will print out every rule which is present inside the table.

86
00:05:32,860 --> 00:05:41,530
So now in order to get access to these rules, I will go ahead and I will open up the developer tools.

87
00:05:41,650 --> 00:05:44,170
So now if you're on Chrome, you can simply right.

88
00:05:44,200 --> 00:05:46,060
Click and click on Inspect Element.

89
00:05:46,540 --> 00:05:51,940
And when you go ahead and do that, you can actually go to the console, which is the JavaScript console.

90
00:05:52,090 --> 00:05:57,130
So now if I go in the console and hit refresh, as you can see, these are the different rules which

91
00:05:57,130 --> 00:05:57,550
I get.

92
00:05:58,060 --> 00:06:02,260
So I get one, two, three and four rows which are present up over here.

93
00:06:02,470 --> 00:06:08,470
However, if you actually take a look at the number of rows which we have here, we actually have five

94
00:06:08,470 --> 00:06:08,890
rows.

95
00:06:09,160 --> 00:06:13,540
However, as we have done minus one, it will only display four rows.

96
00:06:13,930 --> 00:06:19,750
And the reason why we actually have only four rows here, because we are now going to go ahead and add

97
00:06:19,810 --> 00:06:24,700
another two over here at the bottom, which is actually going to be the root to display the total.

98
00:06:25,240 --> 00:06:28,720
So that's why we have kept this thing at minus one.

99
00:06:28,930 --> 00:06:34,990
However, if you go ahead and change it to this and if you hit refresh, as you can see, you will get

100
00:06:34,990 --> 00:06:36,430
the five rows over here.

101
00:06:36,640 --> 00:06:40,270
So you can actually see that we also have the values in here as well.

102
00:06:40,720 --> 00:06:45,460
So now our job is to go ahead and get the values from these.

103
00:06:45,820 --> 00:06:48,760
So now that getting the values is actually quite simple.

104
00:06:49,660 --> 00:06:51,330
So once you enter this room.

105
00:06:51,790 --> 00:06:53,980
So if you actually take a look at this.

106
00:06:54,550 --> 00:06:58,840
These are nothing but the rows and the columns are actually called the cells.

107
00:06:58,870 --> 00:07:04,600
So if you want to go ahead and access some cell values, you can go ahead and do that as well.

108
00:07:04,660 --> 00:07:10,120
So you can do dot cell and in here you can pass in some index value.

109
00:07:10,150 --> 00:07:13,960
So let's say I want to access the index value of the cell one.

110
00:07:14,290 --> 00:07:15,290
I can go ahead.

111
00:07:15,310 --> 00:07:15,970
Do that.

112
00:07:16,120 --> 00:07:18,790
And now if I had to refresh, I get an error.

113
00:07:18,840 --> 00:07:20,680
That's because there should be cells.

114
00:07:21,130 --> 00:07:27,820
So if I go ahead and refresh again, as you can see now, we got the values inside Cell one, which

115
00:07:27,820 --> 00:07:30,190
is the column for cops.

116
00:07:30,610 --> 00:07:32,680
So this thing right here is cell number zero.

117
00:07:32,740 --> 00:07:36,010
And this thing right here of other cops is cell number one.

118
00:07:36,130 --> 00:07:39,280
So as you can see, we have all the Cobb values in here.

119
00:07:39,910 --> 00:07:45,850
So now once you have the Cobb values, you can now go ahead and actually save those values.

120
00:07:45,880 --> 00:07:50,230
But if you notice all year, you also have this TDE tag as well.

121
00:07:50,710 --> 00:07:56,950
So now you need to go ahead and get rid of this T.D. tag and instead only get the inner portion or the

122
00:07:56,950 --> 00:08:01,930
inner value, which is in this case are these various carbohydrate values.

123
00:08:02,290 --> 00:08:05,530
So in order to get that, you again, go ahead and do.

124
00:08:05,650 --> 00:08:08,740
Don't know each Gemal.

125
00:08:08,950 --> 00:08:10,990
And that should actually fix your problem.

126
00:08:11,500 --> 00:08:19,090
And now if you refresh, as you can see, you only now have these individual values and the tags are

127
00:08:19,090 --> 00:08:20,550
no longer present over there.

128
00:08:21,220 --> 00:08:27,820
So now once you have the core values here, you can also go ahead and round up these values as well.

129
00:08:28,390 --> 00:08:36,130
So now I can go back over here and let's now simply go ahead and sum up all of these values together.

130
00:08:36,760 --> 00:08:44,530
So right inside the for loop, in order to sum up all those values, I would see KOBS plus equals.

131
00:08:45,040 --> 00:08:51,670
And now, instead of actually just displaying these values in the console, I can now get access to

132
00:08:51,670 --> 00:08:52,840
these values over here.

133
00:08:52,990 --> 00:08:54,820
So I will simply go ahead.

134
00:08:55,980 --> 00:08:58,830
Copy this portion and pasted up over here.

135
00:08:59,490 --> 00:09:03,570
So this will now go ahead and kind of save all of those values in here.

136
00:09:03,690 --> 00:09:08,730
However, if you do so and now if you try to just log in the value of cops.

137
00:09:08,820 --> 00:09:10,020
Let's see what you get.

138
00:09:10,140 --> 00:09:16,200
So if I would do console, don't log and if I would pass in the value of KOBS here, let's see what

139
00:09:16,200 --> 00:09:17,520
value we would get.

140
00:09:17,550 --> 00:09:22,170
So if I hit refresh, as you can see, you will get this garbage value here.

141
00:09:22,290 --> 00:09:27,660
And the reason for that is because only getting the value itself is not sufficient.

142
00:09:27,720 --> 00:09:32,820
But you also need to go ahead and can what this particular value and to float.

143
00:09:33,210 --> 00:09:38,190
So in order to do that, we have a method in JavaScript which is called ask pass float.

144
00:09:38,550 --> 00:09:43,350
So I will type and pass float here and to this particular pass float.

145
00:09:43,830 --> 00:09:46,950
I will pass and the value which we get.

146
00:09:47,130 --> 00:09:50,520
So I will simply cut this pasted right in there.

147
00:09:51,630 --> 00:09:55,590
And now once we are done with this, let's now check the value for cops.

148
00:09:55,740 --> 00:10:01,110
Now, if I had to refresh, as you can see, we actually got the value of Gob's over here, which is

149
00:10:01,200 --> 00:10:02,940
one ninety five point eight.

150
00:10:03,120 --> 00:10:07,310
So now once you do that, you now know that you have the value for cops.

151
00:10:07,800 --> 00:10:09,280
Now, similar to this.

152
00:10:09,300 --> 00:10:14,610
You also need to get the value for the protein, for the fat and the calories as well.

153
00:10:14,940 --> 00:10:17,430
So getting their values is actually quite simple.

154
00:10:17,520 --> 00:10:19,050
So let's go ahead and do that.

155
00:10:19,500 --> 00:10:21,640
So I will now get the value of protein.

156
00:10:21,660 --> 00:10:24,540
So I will see protein plus equals.

157
00:10:24,960 --> 00:10:27,960
And all of this good is going to be exactly the same.

158
00:10:28,080 --> 00:10:34,860
But now, instead of actually having the cell value of one as protein is present in cell number two,

159
00:10:35,400 --> 00:10:38,670
I will simply do sell off to up over here.

160
00:10:39,480 --> 00:10:42,090
So now this will actually give me the value of protein.

161
00:10:42,180 --> 00:10:45,270
So let's actually check if the value is correct.

162
00:10:45,780 --> 00:10:48,930
So I will simply pass in the value of protein here.

163
00:10:50,380 --> 00:10:51,290
Hit refresh.

164
00:10:51,380 --> 00:10:55,880
And as you can see, we have the value of protein that is the total protein consumed.

165
00:10:56,270 --> 00:10:58,340
And you can also cross-check these values.

166
00:10:58,340 --> 00:11:01,540
So 10 plus 10, 20, 30.

167
00:11:01,790 --> 00:11:05,260
And then we have six and two, which is around eight.

168
00:11:05,420 --> 00:11:09,230
So the result comes out to be thirty nine point six, which seems correct.

169
00:11:09,740 --> 00:11:12,080
So that means we are getting accurate values in here.

170
00:11:12,560 --> 00:11:15,080
And now let's do the same thing for fans as well.

171
00:11:15,410 --> 00:11:20,000
So I would say Fantz plus equals the same thing.

172
00:11:20,030 --> 00:11:23,690
But now, instead of cell number one, I will see cell number three.

173
00:11:24,260 --> 00:11:26,870
And let's do the same thing for calories as well.

174
00:11:27,020 --> 00:11:30,200
So calories plus equals the same thing.

175
00:11:30,860 --> 00:11:33,410
But change the cell to four.

176
00:11:34,460 --> 00:11:41,510
Okay, so now once you have all of these values, you can also confirm if they are correct by simply

177
00:11:41,870 --> 00:11:43,370
logging them into the console.

178
00:11:43,760 --> 00:11:45,560
So let's check for facts.

179
00:11:47,280 --> 00:11:49,710
And I guess that's actually accurate.

180
00:11:49,980 --> 00:11:52,290
And you can do the same check for calories as well.

181
00:11:52,650 --> 00:11:54,210
But I'm not going to do that.

182
00:11:54,660 --> 00:12:01,110
Okay, so now once you have these values in here, our next job is to go ahead and actually assign all

183
00:12:01,110 --> 00:12:06,780
of these values to the bottom of the table right here in the last row of your table.

184
00:12:07,200 --> 00:12:10,890
So currently, the relaxed rule does not display anything at all.

185
00:12:11,010 --> 00:12:16,500
However, now we need to go ahead and have something at the bottom to display the total.

186
00:12:16,650 --> 00:12:23,280
So our job is to go ahead and add an additional set of rule at the very bottom.

187
00:12:23,460 --> 00:12:27,250
So in order to do that, what I will do here is that I'll go ahead.

188
00:12:27,420 --> 00:12:32,490
And when the end four ends, I will create another rule here.

189
00:12:32,700 --> 00:12:39,330
And within this rule, just as we actually have these particular cells, I will go ahead and create

190
00:12:39,390 --> 00:12:39,990
another cell.

191
00:12:40,020 --> 00:12:40,560
So we're here.

192
00:12:41,250 --> 00:12:43,620
So I will see Didi's.

193
00:12:44,160 --> 00:12:46,590
And here I will assign some idea to it.

194
00:12:46,860 --> 00:12:49,740
So the idea for now is going to be empty.

195
00:12:49,950 --> 00:12:54,870
And I'm going to go ahead and call this thing as, let's say name.

196
00:12:55,260 --> 00:12:57,510
And let's see, we see something like.

197
00:12:57,600 --> 00:13:01,370
So as this is going to display total, I will name the force through total.

198
00:13:01,680 --> 00:13:03,930
So I will see total here.

199
00:13:04,230 --> 00:13:08,100
And if you go back, hit refresh, as you can see, it's a total.

200
00:13:08,370 --> 00:13:09,840
That means it's working well.

201
00:13:10,350 --> 00:13:17,040
So now for the rest of the cells here, let's actually assign some I.D. away here so that we can get

202
00:13:17,070 --> 00:13:18,420
access to these elements.

203
00:13:19,260 --> 00:13:25,770
So now I will go ahead and in here again, create the same thing which we have here.

204
00:13:26,460 --> 00:13:32,190
So I will simply copy that pasted up over here and I will give it an I.D..

205
00:13:32,640 --> 00:13:37,380
Let's see as this cell has to display the total number of carbohydrates.

206
00:13:37,980 --> 00:13:38,790
I will see.

207
00:13:39,480 --> 00:13:43,650
This is going to be two two cups.

208
00:13:45,320 --> 00:13:47,600
And I will keep this thing as empty.

209
00:13:50,290 --> 00:13:59,380
Then I will do the same thing for protein, fat and calories, so I wouldn't see total protein, total

210
00:13:59,410 --> 00:14:03,070
France and total calories.

211
00:14:04,000 --> 00:14:09,490
Okay, so now once we have this, if you hit refresh, as you can see, you don't have any values yet.

212
00:14:09,880 --> 00:14:14,500
However, the values are actually present up over here in these variables.

213
00:14:14,860 --> 00:14:21,730
So now, in order to assign those values to these particular ideas, what you need to do is that you

214
00:14:21,730 --> 00:14:24,100
again need to go ahead and write in some JavaScript.

215
00:14:24,520 --> 00:14:32,110
So here, first of all, you actually need to go ahead and get all of these particular cells over here.

216
00:14:32,440 --> 00:14:34,240
So let's get them one by one.

217
00:14:34,270 --> 00:14:35,630
So I will see document.

218
00:14:35,800 --> 00:14:39,120
Don't get element by I.D..

219
00:14:39,640 --> 00:14:41,430
Let's get the total carbs first.

220
00:14:41,450 --> 00:14:44,310
So I will see total carbs.

221
00:14:44,590 --> 00:14:46,630
And now I want to set.

222
00:14:46,750 --> 00:14:47,920
It's in the HDMI.

223
00:14:47,920 --> 00:14:52,840
I'll do the actual carbs value, which is nothing but this value which we have here.

224
00:14:53,080 --> 00:14:59,200
So I will say this equals carbs and let's do the same thing for other fields as well.

225
00:14:59,470 --> 00:15:03,040
So this is going to be total protein.

226
00:15:03,220 --> 00:15:07,600
This is going to be for fats, calories.

227
00:15:07,990 --> 00:15:12,830
This is going to be protein, fats, calories.

228
00:15:13,210 --> 00:15:13,480
OK.

229
00:15:13,540 --> 00:15:16,300
So now once we are done with this, let's actually take a look.

230
00:15:16,330 --> 00:15:16,970
If it works.

231
00:15:17,000 --> 00:15:18,270
So when I hit refresh.

232
00:15:18,760 --> 00:15:22,180
As you can see, we actually get an any in value here.

233
00:15:22,270 --> 00:15:29,140
And the reason why we have these n n values in here is because we now need to go ahead and end this

234
00:15:29,140 --> 00:15:30,040
particular loop.

235
00:15:30,660 --> 00:15:32,350
One rule behind.

236
00:15:32,410 --> 00:15:34,120
So I will do this minus one.

237
00:15:34,390 --> 00:15:36,250
And that should actually fix our issue.

238
00:15:36,400 --> 00:15:42,160
So now, if I had to refresh, as you can see now, it will display the total number of cops, proteins,

239
00:15:42,160 --> 00:15:44,170
fats and calories up over here.

240
00:15:44,950 --> 00:15:50,790
And one interesting thing about this is that if you now go ahead and let's see if I add oats.

241
00:15:50,890 --> 00:15:54,040
So as you can see, these are the current total statistics.

242
00:15:54,460 --> 00:16:00,220
However, if I add another cup of oats, these values actually have changed up over here.

243
00:16:00,820 --> 00:16:09,190
However, as you can see, we have added one value here and that value actually broke up the entire

244
00:16:09,190 --> 00:16:09,670
table.

245
00:16:09,700 --> 00:16:13,780
That's because it added this long decimal value in here.

246
00:16:14,320 --> 00:16:19,660
So in order to get rid of this exact issue, there's one more thing which you need to do, and that

247
00:16:19,660 --> 00:16:23,500
thing is you actually need to round up all the values.

248
00:16:24,070 --> 00:16:32,680
So if you go back to your code, as you can see, once you get these values from here, we forgot to

249
00:16:32,680 --> 00:16:35,560
actually go ahead and round up all of these values.

250
00:16:36,070 --> 00:16:41,410
So now, in order to round up these values, you can do something like KOBS equals.

251
00:16:42,670 --> 00:16:45,110
Map, dot round.

252
00:16:46,330 --> 00:16:47,390
Let's do cops.

253
00:16:47,470 --> 00:16:52,070
So this will round up the Cobb values and store it back into the Cobb speed able.

254
00:16:52,600 --> 00:16:56,080
And now let's do the same thing for protein and fance as well.

255
00:16:56,350 --> 00:17:02,440
So I would simply do protein equals math dart round.

256
00:17:02,710 --> 00:17:05,079
That's going to be protein.

257
00:17:05,530 --> 00:17:08,460
Let's do the same thing for fats as well.

258
00:17:08,470 --> 00:17:13,329
So fats are going to be math darte around.

259
00:17:15,170 --> 00:17:21,550
Fats, and you don't actually have to go ahead and do this for calories, but just for the sake of safety,

260
00:17:21,550 --> 00:17:22,720
let's do this as well.

261
00:17:23,140 --> 00:17:29,810
So calories equals mass dart round.

262
00:17:30,580 --> 00:17:32,680
That's actually going to be calories.

263
00:17:33,380 --> 00:17:33,700
OK.

264
00:17:33,790 --> 00:17:37,360
So once we do that, let's head back and hit refresh.

265
00:17:39,350 --> 00:17:41,010
Let's visit the space again.

266
00:17:41,120 --> 00:17:45,800
And as you can see now, we no longer have that particular issue or here.

267
00:17:45,830 --> 00:17:48,050
And now the values are actually rounded up.

268
00:17:48,680 --> 00:17:54,530
So now if I try to add, Ron, by the way, here at the bottom of these stats will change.

269
00:17:54,620 --> 00:17:59,060
So if I click on ADD, as you can see, these statistics actually changed.

270
00:17:59,750 --> 00:18:06,560
So that means we have successfully calculated the total up over here and everything is working fine.

271
00:18:06,710 --> 00:18:12,620
However, now our next job is to go ahead and add a nice looking graph over here, which is going to

272
00:18:12,620 --> 00:18:17,810
be a dynamic graph which will display the macro nutrient breakdown in our diet.

273
00:18:18,170 --> 00:18:20,690
So we are going to do that in the upcoming lecture.

274
00:18:20,870 --> 00:18:22,670
So thank you very much for watching.

275
00:18:22,760 --> 00:18:24,560
And I'll see you guys next time.

276
00:18:24,920 --> 00:18:25,370
Thank you.

