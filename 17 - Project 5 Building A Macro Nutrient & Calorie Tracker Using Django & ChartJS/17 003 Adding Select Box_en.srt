1
00:00:00,180 --> 00:00:05,800
OK, so now let's go ahead and sort of put all of these items inside a select box.

2
00:00:05,939 --> 00:00:11,760
So in order to create a select box to first of all, you need to go ahead and set up a form inside this

3
00:00:11,760 --> 00:00:13,260
particular each Schimel body.

4
00:00:13,680 --> 00:00:19,470
So let's type in the form tag and the action of this is going to be just empty.

5
00:00:19,530 --> 00:00:20,100
As of now.

6
00:00:20,670 --> 00:00:24,470
And now, as we are creating a form, you also need to include these.

7
00:00:24,480 --> 00:00:24,770
Yes.

8
00:00:24,780 --> 00:00:25,800
RF token as well.

9
00:00:26,130 --> 00:00:29,250
So I will see you see us out of underscore token.

10
00:00:29,850 --> 00:00:30,720
And after that.

11
00:00:31,200 --> 00:00:37,350
Now, what you need to do is that you now need to go ahead and create a select box in here.

12
00:00:37,860 --> 00:00:42,720
So in order to create a select box, select box can be created using a select tag.

13
00:00:42,840 --> 00:00:44,610
So this is the select right here.

14
00:00:45,150 --> 00:00:49,860
And as of now, we won't be having any name I.D. for this particular select box.

15
00:00:50,250 --> 00:00:55,800
So once you have the select box and once you go back here and hit refresh, as you can see, you will

16
00:00:55,800 --> 00:01:00,900
get an empty select box right up over here, which does not contain any of the option.

17
00:01:01,530 --> 00:01:07,740
So now what we want to do is that we want to be able to add options away here inside this particular

18
00:01:07,740 --> 00:01:10,440
select box in case of options.

19
00:01:10,590 --> 00:01:16,230
We want these items to be the options for the select box and henceforth in order to do that.

20
00:01:16,860 --> 00:01:24,210
What we will do is that will type in the option tag here and each option here is going to have a value.

21
00:01:24,870 --> 00:01:30,690
Now, in place of values, we actually want to display the food name up over here.

22
00:01:30,780 --> 00:01:33,210
So how can we exactly go ahead and do that?

23
00:01:33,810 --> 00:01:38,850
So in order to do that, first of all, you actually need to look through each food item.

24
00:01:39,210 --> 00:01:45,000
And in order to loop through the item, we need to go ahead and use the for loop, which we have here.

25
00:01:45,510 --> 00:01:52,740
So I will simply go ahead, copy this, go right before the option, because obviously we want to look

26
00:01:52,740 --> 00:01:54,900
through each item for every single option.

27
00:01:55,710 --> 00:01:59,580
And I will make sure to end this right after the option tag ends.

28
00:02:00,340 --> 00:02:07,470
And now, once this option is inside the loop, we can now have access to each food item inside the

29
00:02:07,470 --> 00:02:08,310
option value.

30
00:02:09,030 --> 00:02:11,800
So I will see d'Hondt name here.

31
00:02:12,450 --> 00:02:15,180
And I will also display the name of boy here as well.

32
00:02:15,210 --> 00:02:16,430
So I will see food.

33
00:02:16,480 --> 00:02:17,470
Don't name.

34
00:02:18,000 --> 00:02:20,250
So now, once you go ahead, save that.

35
00:02:20,430 --> 00:02:22,110
Once you go back and hit refresh.

36
00:02:22,650 --> 00:02:28,500
As you can see now, you have these items up over here and you can select any one of these items which

37
00:02:28,500 --> 00:02:28,950
you want.

38
00:02:29,700 --> 00:02:35,910
So that means now all these items which be had here are now shifted up over here.

39
00:02:36,360 --> 00:02:39,540
And therefore, we can actually get rid of this code right here.

40
00:02:40,050 --> 00:02:41,490
So that's all fine and good.

41
00:02:42,210 --> 00:02:49,770
Now, the next job, which we need to do here is that now you want the user to be able to select these

42
00:02:49,830 --> 00:02:50,530
options.

43
00:02:51,150 --> 00:02:55,620
And once the user selects this option and let's say we have a add button in here.

44
00:02:55,860 --> 00:03:01,080
So whenever the user actually clicks that add button, what we essentially want to do is that you want

45
00:03:01,080 --> 00:03:05,550
to add that particular item to the user's consumption list.

46
00:03:06,180 --> 00:03:13,350
So the problem here is that let's say if your application is going to be used by multiple users, let's

47
00:03:13,350 --> 00:03:16,080
say hundred users, if not a thousand users.

48
00:03:17,340 --> 00:03:23,670
So the challenge here is that whatever food item which will be adding to the consumption list or the

49
00:03:23,670 --> 00:03:29,400
list of the foods which the user has consumed, we need to maintain a different list for each user.

50
00:03:30,390 --> 00:03:32,650
So how exactly are we going to do that?

51
00:03:33,090 --> 00:03:39,810
So in order to do that, what we want to do is that we first want to go ahead and associate each food

52
00:03:39,840 --> 00:03:41,340
item with each user.

53
00:03:42,000 --> 00:03:45,090
So let's say, for example, if user one is log Ben.

54
00:03:45,270 --> 00:03:48,840
So if user one is logged in and if the user one select eggs.

55
00:03:48,990 --> 00:03:56,460
And if you click on ADD, then we need to make sure that exa only added to user ones account and not

56
00:03:56,460 --> 00:03:57,630
user to account.

57
00:03:58,020 --> 00:04:00,630
And now let's see the user to actually log in.

58
00:04:01,020 --> 00:04:04,860
And if he checks his consumption list, his list should actually be empty.

59
00:04:05,910 --> 00:04:11,850
However, if the user to select oats and if he clicks on the add button, this item should be only added

60
00:04:11,850 --> 00:04:13,770
to user two's consumption list.

61
00:04:14,250 --> 00:04:18,269
So this is the core challenge or the core problem of this entire project.

62
00:04:18,779 --> 00:04:25,260
That is how to associate a user with each item and how to actually add these items to their list.

63
00:04:25,410 --> 00:04:30,720
So in the upcoming lecture, we will actually go ahead and create a different model which will save

64
00:04:30,810 --> 00:04:32,280
the food consumed.

65
00:04:32,400 --> 00:04:36,030
And it will also save by which user the food is consumed.

66
00:04:36,090 --> 00:04:39,510
So thank you very much for watching and I'll see you guys next time.

67
00:04:39,810 --> 00:04:40,350
Thank you.

