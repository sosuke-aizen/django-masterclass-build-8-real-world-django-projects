1
00:00:00,240 --> 00:00:05,520
Now, let's go ahead and learn how to add a delete button over here to the table so that we should be

2
00:00:05,520 --> 00:00:08,039
able to delete any items from our list.

3
00:00:08,220 --> 00:00:13,950
So in order to create a new functionality, first of all, you obviously need to go ahead, go into

4
00:00:13,950 --> 00:00:18,510
the views that you file and then you'll you need to go ahead and create a view.

5
00:00:18,720 --> 00:00:21,100
So let's go ahead and create a view for delete.

6
00:00:21,270 --> 00:00:22,290
So I'll see def.

7
00:00:23,780 --> 00:00:28,740
Delete and the school consume, or you can also see delete and the school item.

8
00:00:29,100 --> 00:00:34,210
However, as we want to delete the consumed item, I will name it as a delete and Dusko consume.

9
00:00:34,770 --> 00:00:42,930
And now to this particular delete consume method or the delete consume view you first need to pass in

10
00:00:42,930 --> 00:00:44,220
the request, as usual.

11
00:00:44,610 --> 00:00:47,970
And also you would need the idea of the item which you want to delete.

12
00:00:48,300 --> 00:00:50,790
So I will pass in the idea up over here as well.

13
00:00:51,400 --> 00:00:51,620
Okay.

14
00:00:51,690 --> 00:00:57,060
So now once you have the idea, first of all, you need to get that particular item which is consumed.

15
00:00:57,480 --> 00:01:01,350
So I will see if that item and two consumed food.

16
00:01:01,470 --> 00:01:04,530
So I will say consumed food equals consumed.

17
00:01:04,540 --> 00:01:09,210
Don't object, start, get and get an item.

18
00:01:09,240 --> 00:01:13,560
Whose I.D. is equal to the idea which we have passed up over here.

19
00:01:14,400 --> 00:01:21,450
So now once we do that, then we actually need to go ahead and check if the request method is post.

20
00:01:21,810 --> 00:01:24,210
So what happens when you click the delete button?

21
00:01:24,330 --> 00:01:29,910
Is that when you click the delete button, a post request is going to be submitted and the post request

22
00:01:29,910 --> 00:01:30,560
is submitted.

23
00:01:30,690 --> 00:01:33,510
We now need to go ahead and delete that particular item.

24
00:01:33,930 --> 00:01:39,870
So I will see if request dart method as equal equal to post.

25
00:01:40,080 --> 00:01:44,760
In that case, simply go ahead and delete the consumed food item.

26
00:01:44,820 --> 00:01:47,430
So I will say consumed and the school food.

27
00:01:47,530 --> 00:01:49,140
Don't delete.

28
00:01:49,500 --> 00:01:55,410
And that's actually simply going to go ahead and delete that particular item or object from our database.

29
00:01:55,830 --> 00:02:01,290
And then once the delete operation is complete, I will simply need to go ahead and return to the main

30
00:02:01,290 --> 00:02:02,640
page, which is the homepage.

31
00:02:02,730 --> 00:02:07,860
So return redirects to the home page, which is slash.

32
00:02:08,610 --> 00:02:11,280
So once this thing is done, we are pretty much good to go.

33
00:02:11,520 --> 00:02:14,730
Now we also need to handle the case for Elsa as well.

34
00:02:15,180 --> 00:02:18,580
So what of this if statement is not executed?

35
00:02:18,630 --> 00:02:21,990
That is what if the request method is not equal to post.

36
00:02:22,440 --> 00:02:28,140
So in that particular case, we want to go ahead and render a delete template.

37
00:02:28,470 --> 00:02:34,380
So we also need to go ahead and create a page for the user to confirm the deletion operation.

38
00:02:34,830 --> 00:02:37,260
So I will see a return rando.

39
00:02:37,970 --> 00:02:42,390
And to this you simply need to pass in the request and then passing the delete template.

40
00:02:42,480 --> 00:02:44,850
So currently we don't have that template.

41
00:02:45,240 --> 00:02:50,060
So let's go ahead and see my app slash delete dot.

42
00:02:50,130 --> 00:02:50,900
Each Gemal.

43
00:02:51,240 --> 00:02:54,630
And now let's go ahead and actually create that template.

44
00:02:55,320 --> 00:02:57,090
So I will go back to my.

45
00:02:58,450 --> 00:03:01,780
My app Inside the Templates create a new file.

46
00:03:02,080 --> 00:03:04,240
Call it as delete dot HMO.

47
00:03:05,260 --> 00:03:08,320
And now once I have this file, I simply need to go ahead.

48
00:03:09,280 --> 00:03:10,870
Have the head and the body tag.

49
00:03:11,380 --> 00:03:15,310
You obviously don't need this bunch of code right here and now.

50
00:03:15,490 --> 00:03:19,640
I can simply go ahead and create a form for confirmation of delete.

51
00:03:19,810 --> 00:03:20,890
So I'll see form.

52
00:03:21,580 --> 00:03:27,070
The method of this thing is obviously going to be will to post.

53
00:03:27,730 --> 00:03:30,400
And then I will add a C, a sort of token.

54
00:03:31,330 --> 00:03:36,160
And now I'm going to simply go ahead and place a simple text here with CS.

55
00:03:36,970 --> 00:03:42,460
Are you sure you want to delete the item?

56
00:03:42,730 --> 00:03:45,970
And finally, I will go ahead and add a submit button in here.

57
00:03:46,000 --> 00:03:48,670
So I will see input type equals submit.

58
00:03:48,970 --> 00:03:52,130
You obviously don't want a name or anything like that.

59
00:03:52,150 --> 00:03:52,600
Do it.

60
00:03:53,260 --> 00:03:57,370
So we are trying to make this page as simple as possible.

61
00:03:58,060 --> 00:04:03,700
So now once you have the view and once The View is able to render this template, we are almost good

62
00:04:03,700 --> 00:04:04,150
to go.

63
00:04:04,330 --> 00:04:10,150
However, now we want to make that view reachable using are you are insert p y file.

64
00:04:10,600 --> 00:04:15,070
So let's head onto the U also p y file and simply add a view for delete.

65
00:04:15,160 --> 00:04:16,390
So I will see path.

66
00:04:17,050 --> 00:04:20,170
And in here I will see delete slash.

67
00:04:20,740 --> 00:04:22,780
So this is going to be the path for delete.

68
00:04:23,140 --> 00:04:28,360
However, along with this path, you would also need to pass in the idea of the item which you want

69
00:04:28,360 --> 00:04:28,870
to delete.

70
00:04:29,320 --> 00:04:32,500
So I will see and call an I.D..

71
00:04:33,130 --> 00:04:36,520
So that will actually pass in the I.D. and give a slash after that.

72
00:04:37,000 --> 00:04:42,670
So this you all should invoke the view which is view, start, delete, underscore, consume.

73
00:04:43,090 --> 00:04:43,600
So delete.

74
00:04:43,630 --> 00:04:48,670
And the school consume is actually the name of the view and the name of this you are is going to be,

75
00:04:48,670 --> 00:04:50,230
let's see, delete.

76
00:04:50,710 --> 00:04:53,440
So once you are done with this, you are pretty much good to go.

77
00:04:54,070 --> 00:04:59,080
Now the only question is when exactly you want to invoke this particular delete view.

78
00:04:59,560 --> 00:05:02,120
So right now there's absolutely no button here.

79
00:05:02,130 --> 00:05:04,970
You would actually need to go ahead and add another column.

80
00:05:05,470 --> 00:05:12,280
So in order to be able to add a column, I will go ahead and I can add that up over here, which is

81
00:05:12,280 --> 00:05:13,030
another column.

82
00:05:13,900 --> 00:05:14,590
Let's see.

83
00:05:14,620 --> 00:05:16,990
This is going to be named as a remove item.

84
00:05:17,320 --> 00:05:23,980
So if I see if that hit refresh, as you can see, you have this remove item column over here.

85
00:05:24,700 --> 00:05:27,370
And now I can also add a button up over there.

86
00:05:27,760 --> 00:05:32,830
So right after the calories, I can go ahead and add a T.D. tag.

87
00:05:33,280 --> 00:05:36,670
And to this daily tag, I simply need to go ahead and pass in.

88
00:05:36,910 --> 00:05:38,200
And each ref tag.

89
00:05:38,620 --> 00:05:40,840
So I will type in e each graph.

90
00:05:40,990 --> 00:05:44,500
So let's say this is going to be empty as of now.

91
00:05:44,680 --> 00:05:47,050
So let's close this tag and let's say this thing.

92
00:05:47,050 --> 00:05:49,150
Say something like remove.

93
00:05:49,450 --> 00:05:51,550
So if I go ahead, hit refresh.

94
00:05:51,640 --> 00:05:55,570
As you can see, we will have these removed buttons over here.

95
00:05:55,750 --> 00:05:57,970
And this is actually a link and not a button.

96
00:05:58,390 --> 00:06:05,890
So in order to make it look like a button, I can go ahead and add class equals BGN Beatty and Dash

97
00:06:06,280 --> 00:06:06,940
Danger.

98
00:06:08,230 --> 00:06:10,630
So now if I see this hit refresh.

99
00:06:10,720 --> 00:06:13,780
As you can see now, this thing looks more like a button.

100
00:06:14,380 --> 00:06:16,120
So don't worry about the formatting.

101
00:06:16,180 --> 00:06:18,170
We will actually go ahead and fix that later.

102
00:06:18,370 --> 00:06:23,800
However, now if I click on the delete button or the remove button, as you can see, nothing happens

103
00:06:23,830 --> 00:06:27,970
because the easy graph of this thing is still empty.

104
00:06:28,420 --> 00:06:30,160
So now let's go ahead and fix that.

105
00:06:30,610 --> 00:06:39,040
So upon clicking the delete button, what should be done is that we should be able to invoke this view.

106
00:06:39,130 --> 00:06:41,720
And this view is actually connected to this.

107
00:06:41,740 --> 00:06:42,640
You are right here.

108
00:06:43,240 --> 00:06:45,370
And the name of this you are is delete.

109
00:06:45,850 --> 00:06:52,210
Now, this delete you URL actually accepts an idea as well, which is nothing but the idea of the item.

110
00:06:53,110 --> 00:06:57,040
So we need to possin that to delete button.

111
00:06:57,490 --> 00:07:04,200
So now in order to pass that thing in, let's go ahead and let's write in some template syntax.

112
00:07:04,660 --> 00:07:07,690
So here will be passing a U all name of that.

113
00:07:07,690 --> 00:07:07,990
You are.

114
00:07:08,170 --> 00:07:11,440
Is delete and along with the delete button.

115
00:07:11,590 --> 00:07:14,350
I also want to pass in the idea of the food item.

116
00:07:14,890 --> 00:07:19,680
So the idea of the food item is nothing but C dot I.D..

117
00:07:20,110 --> 00:07:23,360
So this will actually give me an access to the idea.

118
00:07:23,860 --> 00:07:30,460
So now if I see this, go back over here, just visit the yuan again.

119
00:07:31,180 --> 00:07:33,940
And now let's say if I want to remove this brown bread.

120
00:07:34,000 --> 00:07:36,370
So if I click on remove, it will ask me.

121
00:07:36,430 --> 00:07:38,110
Are you sure you want to delete the item?

122
00:07:38,380 --> 00:07:39,760
So now if I click submit.

123
00:07:40,810 --> 00:07:41,170
Okay.

124
00:07:41,200 --> 00:07:44,890
We got an error here and it is a redirect is not defined.

125
00:07:45,490 --> 00:07:50,620
Now, as earlier mentioned, whenever such error occurs, simply don't freak out.

126
00:07:51,010 --> 00:07:53,770
This is just simply because you might have missed something.

127
00:07:54,250 --> 00:07:57,520
So it will actually give you the exception location as well.

128
00:07:57,870 --> 00:08:02,430
So it seems that the exception is at line number 25 and you start P y.

129
00:08:03,210 --> 00:08:10,740
So now if I go back over here to line number twenty five, as you can see, this redirect is what's

130
00:08:10,740 --> 00:08:11,640
causing an error.

131
00:08:12,030 --> 00:08:18,180
And error says that redirect is not defined, which means that we have forgotten to import redirect.

132
00:08:18,360 --> 00:08:21,510
So I will simply import that up over here.

133
00:08:22,010 --> 00:08:22,310
Okay.

134
00:08:22,470 --> 00:08:26,650
So now let's actually visit the homepage.

135
00:08:26,970 --> 00:08:29,490
And as you can see, the brown bread has been removed.

136
00:08:30,000 --> 00:08:32,340
So now let's try removing the oats from here.

137
00:08:32,460 --> 00:08:39,059
So if I click on remove it, submit, as you can see, oats are removed so you can keep on removing

138
00:08:39,059 --> 00:08:40,020
these items here.

139
00:08:40,320 --> 00:08:43,740
And obviously, these statistics are going to update.

140
00:08:44,100 --> 00:08:46,620
So currently we have 18 hundred calories.

141
00:08:47,640 --> 00:08:49,360
Eighteen hundred and forty six calories.

142
00:08:49,380 --> 00:08:56,010
So if I click remove if you scroll down, as you can see, the calories has been reduced and so are

143
00:08:56,010 --> 00:08:57,360
the other values as well.

144
00:08:57,810 --> 00:09:00,120
And the chart will update accordingly as well.

145
00:09:00,600 --> 00:09:03,810
So now let's also learn how to fix this issue.

146
00:09:03,930 --> 00:09:07,830
That is why exactly do we have this table issue right here?

147
00:09:08,070 --> 00:09:11,940
So we will go ahead and fix this formatting issue in the upcoming lecture.

148
00:09:11,970 --> 00:09:16,290
However, currently, functionality is working just absolutely fine.

149
00:09:16,890 --> 00:09:23,220
So you can go ahead and add another item in here that will actually update the macros, that will actually

150
00:09:23,220 --> 00:09:24,090
update the chart.

151
00:09:24,150 --> 00:09:26,910
That will also update this as well.

152
00:09:27,240 --> 00:09:29,010
And everything would work just fine.

153
00:09:29,640 --> 00:09:31,410
So thank you very much for watching.

154
00:09:31,530 --> 00:09:33,270
And I'll see you guys next time.

155
00:09:33,600 --> 00:09:34,140
Thank you.

