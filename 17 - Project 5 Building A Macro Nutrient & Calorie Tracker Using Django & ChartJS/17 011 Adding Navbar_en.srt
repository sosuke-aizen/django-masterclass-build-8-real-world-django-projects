1
00:00:00,300 --> 00:00:04,650
So in this lecture, let's actually go ahead and add a chart up over here.

2
00:00:05,280 --> 00:00:11,310
But even before that, let's first go ahead and add a navigation bar at the top as well, which will

3
00:00:11,310 --> 00:00:13,800
display the name of our application.

4
00:00:14,160 --> 00:00:16,590
So adding in a bar is actually quite simple.

5
00:00:16,680 --> 00:00:20,220
You just need to go inside the container and inside the container.

6
00:00:20,370 --> 00:00:21,850
You can add enough.

7
00:00:22,350 --> 00:00:23,640
So I will go ahead.

8
00:00:23,940 --> 00:00:33,720
And in order to create a navigation bar, as usual, I'll create a div class rule, then a div class

9
00:00:33,750 --> 00:00:36,140
call dash M.D. Dash twelve.

10
00:00:36,690 --> 00:00:40,740
And in here, let's add a enough time in order to create the navigation bar.

11
00:00:41,130 --> 00:00:43,890
Let's add its glass to NAV Bar.

12
00:00:44,700 --> 00:00:49,900
Now Bardash, let's say if you want a Dacko one, I will see Narrabundah stock.

13
00:00:50,430 --> 00:00:52,560
The background color should be primary.

14
00:00:53,340 --> 00:00:57,750
So once you have this and now you can actually go ahead and and have the name here.

15
00:00:58,290 --> 00:00:58,740
So I will.

16
00:00:58,740 --> 00:01:02,910
Seaspan class equals nav bar brand.

17
00:01:03,000 --> 00:01:05,010
So this is actually going to be the brand name.

18
00:01:05,129 --> 00:01:13,410
And now let's name the application as Callery radical or micro tracker, any name which you want.

19
00:01:13,680 --> 00:01:19,020
And so now if I go back and visit this, you all, as you can see, well, we have our name at the top

20
00:01:19,080 --> 00:01:19,800
up over here.

21
00:01:20,550 --> 00:01:23,520
So once we have that, we are pretty much good to go.

22
00:01:23,790 --> 00:01:29,340
Now let's learn how to go ahead and add a chart here, which will display the macro nutrient breakdown.

23
00:01:29,640 --> 00:01:35,610
So here we actually want to create a chart which will display the macro nutrient breakdown and that

24
00:01:35,610 --> 00:01:37,530
macro nutrient breakdown as nothing.

25
00:01:37,530 --> 00:01:45,330
But it's actually going to be the percentage of carbohydrates, proteins and found in your diet.

26
00:01:45,480 --> 00:01:50,730
So even before we start creating a chart, let's actually make a nice looking container for our chant.

27
00:01:51,180 --> 00:01:56,490
So in order to do that, I will head back to the index chart, each Jamul page.

28
00:01:56,850 --> 00:02:03,270
And as you can see right at the bottom, we actually have this particular five columns which we have

29
00:02:03,270 --> 00:02:04,740
left for our chart.

30
00:02:05,100 --> 00:02:07,080
So in here I will create a div.

31
00:02:07,290 --> 00:02:08,350
So I will see Dave.

32
00:02:09,090 --> 00:02:11,490
Let's name this class as nothing.

33
00:02:11,910 --> 00:02:13,740
And let's just have some heading here.

34
00:02:14,160 --> 00:02:15,880
So I will create each full time.

35
00:02:15,930 --> 00:02:19,740
And I will see today's Hreik down.

36
00:02:21,060 --> 00:02:28,230
And right after this after this tight ends, I will create a card here, which is going to be a card

37
00:02:28,240 --> 00:02:38,610
header, so I will see div class as God header so as to give it a card like look and let us set the

38
00:02:38,730 --> 00:02:40,990
class as text white.

39
00:02:41,040 --> 00:02:42,970
So this will actually set the text to white.

40
00:02:43,380 --> 00:02:46,620
And let's add the background of this card header to blue.

41
00:02:46,650 --> 00:02:49,530
So I will see dash primary.

42
00:02:50,130 --> 00:02:54,130
So now in there I can have another heading which is H four.

43
00:02:54,270 --> 00:02:59,580
And I can see macro nutrients break down.

44
00:03:00,480 --> 00:03:03,900
So apologies if the spelling is not correct.

45
00:03:04,080 --> 00:03:07,260
And now once we have that, let's see how it looks.

46
00:03:07,770 --> 00:03:12,570
So if I had refresh, as you can see, it is today's breakdown and micronutrient breakdown.

47
00:03:13,380 --> 00:03:19,040
So now once we have this, let's actually go ahead and create a column here so as to hold our chart

48
00:03:20,250 --> 00:03:27,650
so I can see div class equals call Dash M.D. Dasht well so has to spend the entire twelve columns.

49
00:03:28,050 --> 00:03:32,040
And in here we will create a chart using Chachi as library.

50
00:03:32,160 --> 00:03:35,070
So we will continue adding this chart in the next lecture.

51
00:03:35,130 --> 00:03:39,090
So thank you very much for watching and I'll see you guys next time.

52
00:03:39,390 --> 00:03:39,990
Thank you.

