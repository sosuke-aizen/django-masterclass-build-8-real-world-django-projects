1
00:00:00,240 --> 00:00:06,360
So now let's go ahead and list out all the items which are consumed by the user on the index page,

2
00:00:06,810 --> 00:00:09,090
so doing that is actually quite simple.

3
00:00:09,120 --> 00:00:16,379
The only thing which you need to do is that you just need to go ahead and in here in the old spot or

4
00:00:16,379 --> 00:00:19,320
here, along with displaying the list of food items.

5
00:00:19,740 --> 00:00:24,330
Now, we also want to go ahead and display the list of consumed foods as well.

6
00:00:24,750 --> 00:00:30,480
So now we know that all the consumed foods are actually present in the You model.

7
00:00:30,600 --> 00:00:33,270
So I will simply go ahead and see consume.

8
00:00:34,840 --> 00:00:36,160
Don't objects.

9
00:00:36,550 --> 00:00:39,940
And instead of getting all the objects, I will filter the objects.

10
00:00:39,970 --> 00:00:41,320
So I will say dot filter.

11
00:00:42,430 --> 00:00:45,020
And we actually want to filter them by the user.

12
00:00:45,280 --> 00:00:50,530
So we only want to get the items which are consumed by the currently logged in user.

13
00:00:50,590 --> 00:00:55,840
So for that, I will see where a user is going to be equal to the current user.

14
00:00:56,020 --> 00:00:59,500
And to get the current user, I will simply do a request.

15
00:00:59,680 --> 00:01:01,210
Don't use it.

16
00:01:02,690 --> 00:01:02,880
OK.

17
00:01:03,220 --> 00:01:08,560
So that will actually give us all the objects which are consumed by the current user.

18
00:01:08,710 --> 00:01:11,710
So I will save those objects and do something called Lah's.

19
00:01:12,310 --> 00:01:13,080
Let's see.

20
00:01:13,330 --> 00:01:15,670
Consumed and the school.

21
00:01:16,650 --> 00:01:17,100
Food.

22
00:01:17,660 --> 00:01:22,200
OK, so once I go ahead and save that apple, here we are pretty much good to go.

23
00:01:22,650 --> 00:01:28,300
So now the only thing which we need to do is that we now need to pass those objects to the next.

24
00:01:28,500 --> 00:01:33,210
Each Gemal template so that we will be able to display them up on the index speech.

25
00:01:33,660 --> 00:01:39,960
So in order to pass them in, I will simply go ahead and write after passing the food object.

26
00:01:40,440 --> 00:01:48,990
I will go ahead and pass and consumed and the school food and is consumed and the school food.

27
00:01:50,510 --> 00:01:54,870
OK, so once we have passed this thing in now we just need to go inside the index.

28
00:01:54,870 --> 00:01:58,170
Not each Schimel and in here, right outside the farm.

29
00:01:58,320 --> 00:02:04,680
I can just go ahead and use it for a loop to loop through all of the items inside consumed food so I

30
00:02:04,680 --> 00:02:05,850
can use it for a loop.

31
00:02:06,000 --> 00:02:10,860
So I will see something like four, let's call this object or a single object.

32
00:02:10,930 --> 00:02:11,460
I see.

33
00:02:11,550 --> 00:02:15,660
So Forese in that's going to be consumed.

34
00:02:16,190 --> 00:02:17,280
And the school food.

35
00:02:18,030 --> 00:02:19,250
Now let's end four.

36
00:02:19,740 --> 00:02:20,730
So I will see.

37
00:02:22,860 --> 00:02:30,900
And for, oh, here and now, in order to display all of those items, I can simply go ahead and I can

38
00:02:30,900 --> 00:02:32,460
see something like see?

39
00:02:32,730 --> 00:02:35,490
So see, here is actually the food item object.

40
00:02:36,060 --> 00:02:41,310
So I can simply go ahead and type in, see and let's see what do we get right up over here.

41
00:02:41,580 --> 00:02:44,050
So now let me just go ahead refresh.

42
00:02:45,000 --> 00:02:49,050
So as you can see, I will get consume object three up over here.

43
00:02:49,860 --> 00:02:56,220
So this consumer object three is actually a consumer object, which means that if you go to the model,

44
00:02:56,220 --> 00:02:57,330
start p y file.

45
00:02:57,390 --> 00:02:58,940
So this is the consumer object.

46
00:02:58,980 --> 00:02:59,200
Okay.

47
00:02:59,210 --> 00:03:00,390
Here you are talking about.

48
00:03:00,750 --> 00:03:07,380
So from this consumer object, you want to access the food, consume objects so you can see that it's

49
00:03:07,440 --> 00:03:09,480
an object stored inside an object.

50
00:03:09,960 --> 00:03:16,560
So here I can see something like C dot food and the school consumed.

51
00:03:16,920 --> 00:03:19,200
So if I do that, let's see, what do we get?

52
00:03:19,500 --> 00:03:24,120
So if I had refresh, I will get that particular object, which is brown bread.

53
00:03:24,630 --> 00:03:31,170
Now, if I actually want to go ahead and access the calories or carbohydrates from this, I can simply

54
00:03:31,170 --> 00:03:32,780
do dort cops.

55
00:03:32,910 --> 00:03:38,370
And now if I go ahead and hit refresh, that will actually give me the carbs inside the brown bread.

56
00:03:38,830 --> 00:03:44,100
However, now in this case, we actually want all the properties of the food item.

57
00:03:44,280 --> 00:03:50,280
So the properties of the food item are nothing but these properties, which is name carbs, proteins,

58
00:03:50,340 --> 00:03:51,570
fats and calories.

59
00:03:52,020 --> 00:03:57,690
So in order to get all of those items, what you need to do is that you need to go to the index store

60
00:03:57,720 --> 00:04:01,290
each Gemal and first of all, let's get the name.

61
00:04:01,440 --> 00:04:03,430
So I will see this start name.

62
00:04:04,110 --> 00:04:08,490
And in front of this, I will give an arrow to separate the other properties.

63
00:04:08,880 --> 00:04:10,570
So now I'll just go ahead.

64
00:04:10,590 --> 00:04:11,310
Copy that.

65
00:04:12,030 --> 00:04:18,880
Piece it and see that this is going to be the cobs.

66
00:04:19,709 --> 00:04:23,490
Then let's paste the other thing for.

67
00:04:25,520 --> 00:04:26,240
Protein.

68
00:04:27,260 --> 00:04:30,290
And now let's do one more for fats.

69
00:04:30,530 --> 00:04:32,360
So I will see France.

70
00:04:32,930 --> 00:04:35,140
And let's do one more over here.

71
00:04:35,510 --> 00:04:38,570
And this is going to be four calories.

72
00:04:39,290 --> 00:04:39,570
OK.

73
00:04:40,280 --> 00:04:45,110
So now once we have this, I will give a breakdown here to separate all of those items.

74
00:04:45,290 --> 00:04:47,420
So I will see slash bill.

75
00:04:48,400 --> 00:04:48,630
OK.

76
00:04:48,740 --> 00:04:51,680
So this is how the format would actually look like.

77
00:04:51,740 --> 00:04:57,620
So now, instead of just displaying the object, we have displayed the properties of the objects as

78
00:04:57,620 --> 00:04:57,890
well.

79
00:04:57,920 --> 00:04:59,280
Which is the name carbs.

80
00:04:59,390 --> 00:05:00,350
Protein content.

81
00:05:00,350 --> 00:05:01,820
Fats and calories.

82
00:05:02,360 --> 00:05:09,530
So now if we go back and hit refresh, as you can see, it will give me that.

83
00:05:09,530 --> 00:05:11,510
The first food item is brown bread.

84
00:05:12,080 --> 00:05:15,440
It has 14 carbs, two point seven grams of proteins.

85
00:05:15,560 --> 00:05:18,620
Zero point eight grams of fat and 73 calories.

86
00:05:19,010 --> 00:05:21,200
So currently we only have one item added.

87
00:05:21,560 --> 00:05:24,500
So now let's add another item, which is, let's see, oats.

88
00:05:24,680 --> 00:05:32,540
So now if I click on ADD, so it says we have an error over here and the error is food consumed reference

89
00:05:32,540 --> 00:05:33,530
before assignment.

90
00:05:33,650 --> 00:05:39,850
So now whenever you get such kind of arrows, so don't get discouraged by them, it's a usual occurrence.

91
00:05:40,370 --> 00:05:45,800
The only thing which you need to do after getting such error is that you need to go ahead and understand

92
00:05:45,800 --> 00:05:46,940
the meaning of this error.

93
00:05:47,480 --> 00:05:53,030
So this particular thing, C is that the local variable which we have used and the name of the local

94
00:05:53,030 --> 00:05:54,410
variable is stated as well.

95
00:05:54,830 --> 00:05:58,600
That means we have some issue with the consumed food variable.

96
00:05:59,030 --> 00:06:05,090
Now the issue is that Shango sees that we have used a reference, this variable, even before assigning

97
00:06:05,090 --> 00:06:05,270
it.

98
00:06:05,810 --> 00:06:13,190
And it will also state that this error exception location is this particular thing, which is nothing

99
00:06:13,190 --> 00:06:18,300
but the view start B by a file and it will even state the line number as well, which is 18.

100
00:06:18,890 --> 00:06:22,550
So now if you go back over here and if you go to Vieux.

101
00:06:24,840 --> 00:06:31,500
And if you go to lane number 18, as you can see, it will give us an error about this food consumed.

102
00:06:31,830 --> 00:06:38,790
So technically, what happens is that in the end spot, if the L'ESPRIT is executed, that means if

103
00:06:38,790 --> 00:06:42,750
the request method is not post, then this will execute fine.

104
00:06:42,850 --> 00:06:47,360
However, what happens when you actually have a post request?

105
00:06:47,400 --> 00:06:53,520
So in case of a post requests, the consumed food variable is not actually defined.

106
00:06:53,940 --> 00:06:59,220
And even if that's not defined, we have still passed the consumed food variable up over here, which

107
00:06:59,220 --> 00:06:59,700
is strong.

108
00:07:00,420 --> 00:07:06,570
So now in order to fix that, the only thing which you need to do is that you actually need to get the

109
00:07:06,570 --> 00:07:09,660
food consumed object or here as well.

110
00:07:09,960 --> 00:07:17,850
So in order to get that, you could either go ahead, copy this and pasted up or here or else what you

111
00:07:17,850 --> 00:07:22,800
can do is that you can actually go ahead and get it out of this political or else block.

112
00:07:23,130 --> 00:07:27,150
So I guess getting it out of the else block would be a better option.

113
00:07:27,210 --> 00:07:30,870
So I will go ahead and just get it out of the else block.

114
00:07:31,500 --> 00:07:33,150
And now let's see if it works.

115
00:07:33,360 --> 00:07:37,320
So now if I go back, let's go back to the next page.

116
00:07:37,380 --> 00:07:38,850
So it will display OT's.

117
00:07:39,210 --> 00:07:43,290
Now, let's see if this will actually work with the post request.

118
00:07:43,710 --> 00:07:46,260
So now let's select some other item, which is egg.

119
00:07:46,410 --> 00:07:50,160
So if I click on ADD, as you can see, eggs are added up over here.

120
00:07:50,760 --> 00:07:52,860
So now I will select oats again.

121
00:07:52,980 --> 00:07:56,280
Click on ADD and now oats will be added up or here as well.

122
00:07:57,150 --> 00:07:58,620
So that's it for this lecture.

123
00:07:58,860 --> 00:08:03,960
And I hope you guys be able to understand how this thing is working right now.

124
00:08:04,470 --> 00:08:10,500
So if you select some item and click on ADD, it will simply add that item to the list and that list

125
00:08:10,530 --> 00:08:12,270
is going to be displayed up over here.

126
00:08:12,660 --> 00:08:17,580
Now, in the upcoming lecture, let's actually work on some formatting over here, because currently

127
00:08:18,030 --> 00:08:25,200
this application in Norway looks like a macro analysis or macro nutrient analysis application.

128
00:08:25,590 --> 00:08:31,350
So in the upcoming lecture, we'll try to make it a little bit better by adding some bootstrap styling.

129
00:08:31,500 --> 00:08:33,179
So thank you very much for watching.

130
00:08:33,240 --> 00:08:35,070
And I'll see you guys next time.

131
00:08:35,340 --> 00:08:35,909
Thank you.

