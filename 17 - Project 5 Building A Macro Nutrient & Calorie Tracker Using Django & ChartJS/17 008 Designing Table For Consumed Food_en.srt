1
00:00:00,330 --> 00:00:03,780
Now, let's go ahead and create a table to please these items.

2
00:00:03,960 --> 00:00:07,190
So in order to create a table, let's hold back to be a scope.

3
00:00:07,650 --> 00:00:14,460
And in you, if you take a look at the bootstrap container and the bootstrap rule, which we have here.

4
00:00:14,940 --> 00:00:16,650
So this rule actually ends here.

5
00:00:17,400 --> 00:00:18,540
Which is right over here.

6
00:00:18,570 --> 00:00:19,740
And the container ends here.

7
00:00:20,250 --> 00:00:23,490
So within this container now, we can actually create another room.

8
00:00:23,670 --> 00:00:25,290
So I will create another row here.

9
00:00:25,760 --> 00:00:26,370
So let's see.

10
00:00:26,370 --> 00:00:28,200
This class equals rule.

11
00:00:28,740 --> 00:00:30,020
Let's end this live here.

12
00:00:30,540 --> 00:00:36,780
And now inside this particular div, we actually want to go ahead and create a people which only spans

13
00:00:36,840 --> 00:00:38,700
60 percent of the left hand side.

14
00:00:38,760 --> 00:00:41,700
So we want to display the table in this portion right over here.

15
00:00:42,180 --> 00:00:47,940
And as bootstrap columns actually span twelve columns, we only want this to spend six columns.

16
00:00:48,030 --> 00:00:50,430
So let's go ahead and create a column here.

17
00:00:50,610 --> 00:00:55,410
So I would say div plus equals let's say call Dash MDD six.

18
00:00:55,470 --> 00:00:56,880
So this spans six columns.

19
00:00:57,420 --> 00:01:00,930
And in here, first of all, let's go ahead and add a simple heading.

20
00:01:02,160 --> 00:01:03,850
So I will create another day for you.

21
00:01:04,090 --> 00:01:04,840
So, Dev.

22
00:01:05,340 --> 00:01:10,260
And in here, I will add a heading which will see something like today's consumption.

23
00:01:10,320 --> 00:01:14,730
So let's use the H for tag and see something like today's

24
00:01:17,250 --> 00:01:18,150
consumption.

25
00:01:19,410 --> 00:01:24,270
So now if I go back and hit refresh, as you can see, there's the heading right there.

26
00:01:24,870 --> 00:01:31,680
So now once we have this particular heading inside this div, let's actually go ahead and create a table

27
00:01:31,710 --> 00:01:32,730
using the table tag.

28
00:01:32,850 --> 00:01:34,040
So I'll see table.

29
00:01:34,560 --> 00:01:35,130
And in here.

30
00:01:35,320 --> 00:01:38,970
Oh, let's go ahead and add a class to this.

31
00:01:39,210 --> 00:01:41,940
So I will see table class is equal to table.

32
00:01:42,430 --> 00:01:45,290
And that's going to be table dash stripe.

33
00:01:45,660 --> 00:01:51,530
So table striped is actually a special class and bootstrap which will create a striped table for us.

34
00:01:51,960 --> 00:01:58,440
And also, if you want to assign a color to this particular table, you see Table Dasch primary, which

35
00:01:58,440 --> 00:02:01,440
will actually assign a bluish kind of color to your table.

36
00:02:02,130 --> 00:02:04,470
Now to this specific table.

37
00:02:04,830 --> 00:02:08,039
We are also going to add something which is called as an I.D..

38
00:02:08,160 --> 00:02:10,979
And the idea for the stable, it's going to be table itself.

39
00:02:11,430 --> 00:02:18,360
Now, the reason why we are adding this I.D. or here is to actually go ahead and calculate the total

40
00:02:18,360 --> 00:02:21,120
carbohydrates, proteins and fats out of this table.

41
00:02:21,600 --> 00:02:25,040
So you will understand the significance of this I.D..

42
00:02:25,360 --> 00:02:27,520
Well, when we go ahead and implement that part.

43
00:02:27,650 --> 00:02:29,610
So as of now, you can ignore that.

44
00:02:29,940 --> 00:02:32,750
However, make sure that you add that idee up or that.

45
00:02:33,480 --> 00:02:33,690
OK.

46
00:02:33,810 --> 00:02:38,970
So now once you have the table, the first thing which you need to do is that you need to have a big

47
00:02:39,090 --> 00:02:42,810
role on the top, which would actually display the different column names.

48
00:02:43,200 --> 00:02:46,980
So you have the food item column, which will display the food name.

49
00:02:47,340 --> 00:02:50,940
Then you have the carbohydrates, proteins, fats and calories.

50
00:02:51,300 --> 00:02:55,830
So currently, if you take a look at our table, there's absolutely nothing here.

51
00:02:56,340 --> 00:02:58,740
So let's add the Roseau here.

52
00:02:59,160 --> 00:03:00,000
So I will see.

53
00:03:00,810 --> 00:03:02,520
And to this particular table rule.

54
00:03:02,970 --> 00:03:05,040
Let's go ahead and add some headers.

55
00:03:05,130 --> 00:03:07,140
So I will see it for headers.

56
00:03:07,620 --> 00:03:11,280
And the first header is going to be food item.

57
00:03:11,940 --> 00:03:13,170
So if I saved that.

58
00:03:14,080 --> 00:03:17,540
As you can see, food item is added up or here now.

59
00:03:17,680 --> 00:03:23,380
Along with that, we also want a couple of other headers as well, which is carbs, proteins and fats.

60
00:03:23,830 --> 00:03:27,020
So I will make sure to add them up or here, Sue.

61
00:03:27,220 --> 00:03:29,170
I will go ahead and replace them one by one.

62
00:03:29,560 --> 00:03:32,240
So this is going to be carbs.

63
00:03:32,890 --> 00:03:37,680
And as this is actually in grams, I will add GM and the brackets, too.

64
00:03:37,930 --> 00:03:40,420
Now let's do protein.

65
00:03:40,690 --> 00:03:42,340
This is also in grams.

66
00:03:42,460 --> 00:03:43,840
So I will type in GM.

67
00:03:44,780 --> 00:03:48,010
Let's do fats also in grams.

68
00:03:48,490 --> 00:03:50,800
And the final item is calories.

69
00:03:52,090 --> 00:03:55,150
So calories is in thousand calories.

70
00:03:55,420 --> 00:03:55,860
Carl.

71
00:03:56,080 --> 00:03:58,330
So I will add cakehole up over here.

72
00:03:59,140 --> 00:03:59,410
Okay.

73
00:03:59,440 --> 00:04:02,890
So once we have these, let's actually take a look at our table.

74
00:04:03,070 --> 00:04:04,870
So the table looks absolutely fine.

75
00:04:05,350 --> 00:04:10,660
Now, once we have this particular table rule now, we need to create rules for actual data.

76
00:04:11,200 --> 00:04:13,240
So here I will create another table rule.

77
00:04:13,660 --> 00:04:20,350
And in here, what we want to do is that we want to actually go ahead and display the columns for the

78
00:04:20,350 --> 00:04:24,510
food, consume the carbohydrates, the proteins and the fat contents.

79
00:04:25,030 --> 00:04:30,730
So inside this now, we actually want to loop through all the food items.

80
00:04:30,820 --> 00:04:34,480
So in order to loop through items, you're are going to make use of this for loop.

81
00:04:34,930 --> 00:04:37,710
So I will simply go ahead and inside the stable room.

82
00:04:38,080 --> 00:04:44,290
I will paste the false statement here and I will also copy this and for a spell.

83
00:04:44,890 --> 00:04:47,590
So this end force should be over here.

84
00:04:48,430 --> 00:04:53,290
So this is what it looks like.

85
00:04:53,350 --> 00:04:54,400
So you have the four.

86
00:04:54,760 --> 00:04:58,600
And then this fall will actually have these stable rules.

87
00:04:58,840 --> 00:05:01,050
So this will actually create multiple table rows.

88
00:05:01,540 --> 00:05:05,560
And in each table room, we wondered, despite speed, the statistics for each item.

89
00:05:05,830 --> 00:05:08,610
So now, in order to add each item, I'll CTD.

90
00:05:08,860 --> 00:05:14,320
And in this particular TDE tag, it will first display the names of all the foods.

91
00:05:14,410 --> 00:05:19,120
So in here to get the names of the foods, we got the names of the foods using this.

92
00:05:19,210 --> 00:05:22,480
So I will simply copy that pasted up over here.

93
00:05:23,320 --> 00:05:27,970
So now if I see this and if I go back over here, it refresh.

94
00:05:28,030 --> 00:05:30,730
It will actually display all the names of the food items.

95
00:05:30,910 --> 00:05:36,790
Now I can go ahead and add a couple more Didi's tags here to display the other statistics.

96
00:05:36,820 --> 00:05:41,110
So I will simply copy this pasted multiple number of times.

97
00:05:41,590 --> 00:05:44,860
So the second statistic which we want to display are the comps.

98
00:05:44,950 --> 00:05:46,810
So I will select that piece.

99
00:05:46,840 --> 00:05:49,240
The boy here, paste it up over here.

100
00:05:49,990 --> 00:05:51,640
Then we have protein.

101
00:05:52,150 --> 00:05:54,340
So I will replace this with protein.

102
00:05:54,580 --> 00:05:56,110
This is going to be for France.

103
00:05:56,800 --> 00:06:00,340
So France and this one is going to be four calories.

104
00:06:00,370 --> 00:06:03,010
So I will see calories.

105
00:06:03,340 --> 00:06:10,540
Now, if I go back and hit refresh and I guess we forgot to spell protein, so I always get confused

106
00:06:10,540 --> 00:06:11,800
with the spelling of protein.

107
00:06:12,250 --> 00:06:13,540
So let me just fix that.

108
00:06:13,750 --> 00:06:16,960
I'm not sure if this is the correct spelling or the previous one was.

109
00:06:17,200 --> 00:06:19,930
However, this should actually work irrespective of that.

110
00:06:20,470 --> 00:06:20,660
Okay.

111
00:06:20,730 --> 00:06:23,620
So as you can see, we have our table right here.

112
00:06:23,740 --> 00:06:26,680
And we also have all the other quantities as well.

113
00:06:26,890 --> 00:06:32,770
So one more thing which you could do here is that to this particular table header, which actually displays

114
00:06:32,920 --> 00:06:34,870
these, you can actually add a class.

115
00:06:34,930 --> 00:06:40,990
So I will say class is going to be equal to MBG Dasch primary.

116
00:06:41,020 --> 00:06:45,310
So it will actually give it a primary color or background as the primary color.

117
00:06:45,850 --> 00:06:51,250
So if I see this and it refresh, as you can see, it will give it a bluish kind of color.

118
00:06:52,420 --> 00:06:57,010
And then I will also see TECs Dash White.

119
00:06:57,850 --> 00:07:00,520
So that will actually make this text too white.

120
00:07:00,580 --> 00:07:02,560
So as you can see, it looks a lot better.

121
00:07:02,800 --> 00:07:08,380
And one more thing which you need to do here is that if you take a look and this div class over here.

122
00:07:08,530 --> 00:07:12,040
So this is actually a column which spans six columns.

123
00:07:12,070 --> 00:07:15,010
So this will actually end here.

124
00:07:15,670 --> 00:07:20,440
So you also need to go ahead and create another day for you to spend the other six columns.

125
00:07:20,470 --> 00:07:26,440
So I will see div class equals two called Dash M.D, dash five.

126
00:07:26,560 --> 00:07:29,830
And I will leave an offset of one.

127
00:07:30,100 --> 00:07:31,810
So this will actually create another div.

128
00:07:31,930 --> 00:07:35,230
And if I had refresh, as you can see, you won't be able to see that.

129
00:07:35,280 --> 00:07:35,950
But we're here.

130
00:07:36,070 --> 00:07:40,330
However, when we designed this part, you will be able to notice the difference here.

131
00:07:40,690 --> 00:07:46,840
So now what you can do is that you can actually get rid of this code from here so that your design looks

132
00:07:46,900 --> 00:07:47,530
much cleaner.

133
00:07:47,860 --> 00:07:52,000
So as you can see, this looks a lot better as compared to the previous one.

134
00:07:52,150 --> 00:07:53,590
So that's it for this lecture.

135
00:07:53,770 --> 00:07:59,050
And in the next lecture, what we will do is that we will learn how to calculate the total number of

136
00:07:59,050 --> 00:08:04,750
carbs, proteins, fats and calories consumed by the user in a specific day.

137
00:08:05,290 --> 00:08:08,230
And we are going to display that up over here at the bottom.

138
00:08:08,590 --> 00:08:10,230
So thank you very much for watching.

139
00:08:10,300 --> 00:08:12,100
And I'll see you guys next time.

140
00:08:12,400 --> 00:08:12,970
Thank you.

