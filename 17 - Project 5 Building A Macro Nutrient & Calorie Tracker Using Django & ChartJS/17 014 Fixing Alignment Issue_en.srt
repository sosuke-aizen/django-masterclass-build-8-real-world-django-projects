1
00:00:00,210 --> 00:00:04,680
Okay, so here's a actually an update on this particular formatting issue, which we had.

2
00:00:05,160 --> 00:00:11,430
So currently I'm actually recording my entire text on screen and I've no longer zoomed out this particular

3
00:00:11,430 --> 00:00:12,820
browser window henceforth.

4
00:00:12,900 --> 00:00:15,570
This is what it currently looks right now.

5
00:00:16,110 --> 00:00:22,200
However, we still seem to have an issue here where this table is not entirely colored by this particular

6
00:00:22,200 --> 00:00:22,780
blue color.

7
00:00:23,370 --> 00:00:29,460
And the way to fix that is you could simply go ahead and you will notice that this thing is currently

8
00:00:29,460 --> 00:00:34,700
spanning only six columns and this space seems to be insufficient.

9
00:00:35,160 --> 00:00:37,440
So you simply need to replace this with seven.

10
00:00:37,530 --> 00:00:43,590
However, if you replace this with seven and if you refresh, that chart will be shifted down right

11
00:00:43,590 --> 00:00:43,800
here.

12
00:00:44,220 --> 00:00:45,900
And this thing is actually a problem.

13
00:00:46,320 --> 00:00:51,300
And in order to fix that, what you could simply do is that you could go back to your code.

14
00:00:51,930 --> 00:00:52,500
And in here.

15
00:00:53,690 --> 00:00:58,940
As you can see, we have added the offset of one to the shot, so simply delete that.

16
00:00:59,540 --> 00:01:02,510
And if you do that, it will actually fix your issue.

17
00:01:03,050 --> 00:01:08,000
One more thing which you could do here is that in order to make this shot consume less space, you could

18
00:01:08,060 --> 00:01:15,260
either replace this button with some icon or you could simply add a X button in here.

19
00:01:15,770 --> 00:01:17,510
So now if you hit refresh.

20
00:01:17,540 --> 00:01:23,450
As you can see, this layout actually looks a little bit better as compared to the previous one.

21
00:01:23,630 --> 00:01:25,850
And this will actually fix the issue.

22
00:01:26,390 --> 00:01:28,580
So that's it for this particular lecture.

23
00:01:28,610 --> 00:01:33,380
I hope you enjoyed this particular new section on Calorie Tracker.

24
00:01:33,590 --> 00:01:39,970
And I hope you learned a couple of things like how to go ahead and use JavaScript, calculate or update

25
00:01:39,990 --> 00:01:41,180
the real time values.

26
00:01:41,270 --> 00:01:42,260
How to use charge.

27
00:01:42,380 --> 00:01:42,600
Yes.

28
00:01:43,040 --> 00:01:44,270
So on and so forth.

29
00:01:44,330 --> 00:01:45,800
So thanks again for watching.

