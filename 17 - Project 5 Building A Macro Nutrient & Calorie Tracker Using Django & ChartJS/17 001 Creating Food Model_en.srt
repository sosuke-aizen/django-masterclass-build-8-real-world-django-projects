1
00:00:00,690 --> 00:00:06,570
Hello and welcome to this brand new section in which we will be building a calorie counter application.

2
00:00:06,689 --> 00:00:13,890
So this is going to be sort of a food tracking application wherein you can go ahead and you can add

3
00:00:13,980 --> 00:00:21,180
food items to your list and it will actually count the micronutrients of the diet which you had.

4
00:00:21,600 --> 00:00:27,630
So let's say, for example, if you had eggs for breakfast so you can select a particular item from

5
00:00:27,660 --> 00:00:30,270
a given list, which is going to be a food item.

6
00:00:30,360 --> 00:00:32,549
And once you have selected that item.

7
00:00:32,640 --> 00:00:38,280
And when you add it to your diet, it will actually go ahead and update all the macro nutrients like

8
00:00:38,280 --> 00:00:43,110
the carbohydrates, proteins, fats, and it will also count the calories as well.

9
00:00:43,590 --> 00:00:47,610
So this is what food will be building for this particular section.

10
00:00:47,670 --> 00:00:53,070
So what you need to do here is that you first need to go ahead and set up a basic Shango project.

11
00:00:53,580 --> 00:00:57,870
So I'm not repeating these steps because we have done it a thousand times.

12
00:00:58,170 --> 00:01:00,570
That is, we have created a Witchel environment.

13
00:01:00,660 --> 00:01:06,240
Then we go ahead create a site which is called As My Site, which is going to be on Jangle Project.

14
00:01:06,670 --> 00:01:11,610
And in that Jango project, we will also create an app which is called As My Site.

15
00:01:12,480 --> 00:01:14,850
So this is the basic setup which you need to do.

16
00:01:15,270 --> 00:01:16,330
So just go ahead.

17
00:01:16,350 --> 00:01:20,160
Create a folder called Last Calorie Counter or calorie count.

18
00:01:20,910 --> 00:01:23,280
Create a virtual environment inside of it.

19
00:01:23,400 --> 00:01:24,480
Install jangle.

20
00:01:24,570 --> 00:01:25,860
Create a new project.

21
00:01:25,860 --> 00:01:31,830
Call last my site, c.D, into that project and then create a new app which is called Last My App.

22
00:01:32,310 --> 00:01:32,570
Okay.

23
00:01:32,610 --> 00:01:39,720
So once you follow these steps, the next step is to go ahead, open up your folder in any code editor.

24
00:01:39,810 --> 00:01:41,790
So it will be using visual studio code.

25
00:01:42,210 --> 00:01:48,570
And then you're, as you always do with any app, whenever you create a new app and jangle, you do

26
00:01:48,570 --> 00:01:52,540
need to add that app over here inside the installed apps.

27
00:01:52,890 --> 00:01:55,920
So the app which we have here is my app.

28
00:01:55,980 --> 00:01:57,920
So simply add that up over here.

29
00:01:58,690 --> 00:02:01,440
Okay, so now once we have added the my app.

30
00:02:01,560 --> 00:02:05,340
Now let's actually go ahead and jump in to building the project.

31
00:02:05,820 --> 00:02:11,850
So the very first thing which we need to do here is that as if you actually want to build a functionality,

32
00:02:11,850 --> 00:02:16,230
that the user can select food items from a given list.

33
00:02:16,350 --> 00:02:20,280
So first of all, we actually need to have a database of food items.

34
00:02:20,760 --> 00:02:26,130
And those food items should also have values for their micronutrients as well.

35
00:02:26,730 --> 00:02:27,630
So let's go ahead.

36
00:02:27,630 --> 00:02:32,430
And first of all, design a model which will actually hold those food items.

37
00:02:32,970 --> 00:02:35,710
So our first job is to go ahead and create a model.

38
00:02:36,630 --> 00:02:40,230
And this model is going to be used to add food items.

39
00:02:40,350 --> 00:02:42,240
So let's name this model as food.

40
00:02:42,360 --> 00:02:44,220
So I wouldn't say class food.

41
00:02:44,910 --> 00:02:47,460
This is going to be model start model.

42
00:02:47,640 --> 00:02:51,630
And now let's think about the properties which this food item would have.

43
00:02:51,840 --> 00:02:54,180
So obviously, you will have a name.

44
00:02:54,300 --> 00:02:56,760
So I'll see name equals models.

45
00:02:56,820 --> 00:03:00,090
Dot Coffield, the map.

46
00:03:00,400 --> 00:03:04,670
This land is going to be, let's say, 100.

47
00:03:05,070 --> 00:03:10,890
So apart from the name, we also want to add the macronutrients of a particular food item.

48
00:03:11,370 --> 00:03:15,510
So the macronutrients are carbohydrates, proteins and fats.

49
00:03:15,810 --> 00:03:17,290
So let's add them up over here.

50
00:03:17,310 --> 00:03:19,490
So I will see Gob's equals.

51
00:03:19,950 --> 00:03:25,500
And as the carbohydrates are, the micronutrients which we are going to be measuring are going to be

52
00:03:25,500 --> 00:03:26,760
in terms of grams.

53
00:03:26,820 --> 00:03:31,230
So they can be ten point two grams, five point eight grams, so on and so forth.

54
00:03:31,650 --> 00:03:35,460
So for this, the field which we'll be using is going to be the fluid field.

55
00:03:35,880 --> 00:03:46,590
So I will see models don't load field and let's do the same thing with protein and fats as well.

56
00:03:46,620 --> 00:03:47,490
So I will see.

57
00:03:49,990 --> 00:03:54,490
Protein equals models don't float, feelies fell.

58
00:03:55,060 --> 00:03:57,940
And finally, we'll add fats as well.

59
00:03:57,950 --> 00:04:01,930
So fats are going to be models don't load feed.

60
00:04:03,040 --> 00:04:08,530
Okay, so once we have carbohydrates, proteins and fats, let's also go ahead and add calories as well.

61
00:04:08,980 --> 00:04:14,800
So I see calories equals models start.

62
00:04:14,980 --> 00:04:17,620
That's going to be, let's say, endesa feel.

63
00:04:21,480 --> 00:04:26,490
And that's because usually you don't measure calories in terms of a decimal value.

64
00:04:26,970 --> 00:04:27,170
Okay.

65
00:04:27,240 --> 00:04:31,200
So once you have this model set up, let's actually go ahead and make migration's.

66
00:04:31,290 --> 00:04:42,720
So I will simply go to the terminal and see Python three managed up by Make Migration's and then Python

67
00:04:42,720 --> 00:04:45,630
three managed by my grid.

68
00:04:46,600 --> 00:04:52,920
Okay, so once the migrations are created now, let's actually go ahead and create a super user and

69
00:04:52,920 --> 00:04:57,780
kind of add a couple of items to this particular food model.

70
00:04:58,020 --> 00:05:05,950
So in order to create a super user, I will see Python three manage start by create super user.

71
00:05:06,750 --> 00:05:09,240
Let's leave the name to default.

72
00:05:09,480 --> 00:05:11,700
Let's add some dummy e-mail address.

73
00:05:14,120 --> 00:05:16,780
Super Bowl user is going to be the password.

74
00:05:18,070 --> 00:05:22,400
Okay, so now once the super user is created, I can now run our applications.

75
00:05:22,430 --> 00:05:26,340
So I would say Python three managed up by runs.

76
00:05:26,350 --> 00:05:27,010
So server.

77
00:05:28,300 --> 00:05:30,490
And now let's open up the browser window.

78
00:05:30,940 --> 00:05:31,570
And in here.

79
00:05:32,870 --> 00:05:34,040
Let me just.

80
00:05:35,400 --> 00:05:37,390
Go to this address, sir.

81
00:05:37,500 --> 00:05:39,870
As you can see, our application is up and running.

82
00:05:40,320 --> 00:05:42,170
Now let's go to slash admen.

83
00:05:42,510 --> 00:05:46,980
Let's type in the username and the password.

84
00:05:47,360 --> 00:05:47,620
Okay.

85
00:05:47,670 --> 00:05:51,630
So now, once we are in here, as you can see, you won't see any model here.

86
00:05:52,110 --> 00:05:56,250
And that's because you actually need to register your model and admin, not by a file.

87
00:05:56,640 --> 00:05:58,920
So let's go ahead and import the model first.

88
00:05:58,950 --> 00:06:03,150
So I will say import food, because that's the name of our model.

89
00:06:03,600 --> 00:06:05,400
And now let's do Admon.

90
00:06:05,550 --> 00:06:10,080
Don't cite, don't register and just register the food model.

91
00:06:10,900 --> 00:06:11,110
Okay.

92
00:06:11,280 --> 00:06:14,770
So once we have registered this, we can go back over here.

93
00:06:15,300 --> 00:06:16,860
And when I had to refresh.

94
00:06:17,280 --> 00:06:17,520
Okay.

95
00:06:17,550 --> 00:06:19,620
I guess the server is not running.

96
00:06:20,220 --> 00:06:22,320
It sees no model named food.

97
00:06:23,820 --> 00:06:32,280
Ochiai, actually, for what you see from d'Hondt models, import food that should fix the error.

98
00:06:33,400 --> 00:06:38,230
And now if I hit refresh, as you can see, we actually have our food model here.

99
00:06:38,710 --> 00:06:42,280
So now let's try to add some food items to this particular model.

100
00:06:42,550 --> 00:06:43,600
Just as an example.

101
00:06:43,720 --> 00:06:46,270
So let's go ahead and add an item.

102
00:06:46,390 --> 00:06:47,350
Let's see.

103
00:06:47,530 --> 00:06:49,120
We want to add eggs.

104
00:06:49,210 --> 00:06:50,320
So LNC egg.

105
00:06:51,040 --> 00:06:55,400
And now let's actually take some accurate values for these micro OCO.

106
00:06:55,570 --> 00:06:57,280
So let's check eggs macros.

107
00:06:57,310 --> 00:07:03,130
So I would see protein in egg and that should actually give me the macro.

108
00:07:03,660 --> 00:07:03,850
Okay.

109
00:07:04,060 --> 00:07:05,650
So 400 grams of eggs.

110
00:07:05,950 --> 00:07:06,870
These are the macros.

111
00:07:06,940 --> 00:07:11,980
So the protein content of hundred grams of eggs is 13 grams.

112
00:07:12,160 --> 00:07:16,000
And if I actually switch to let's say.

113
00:07:17,670 --> 00:07:18,390
Egg roll.

114
00:07:18,750 --> 00:07:25,710
And let's see if I see one medium that will actually give me macro off one egg.

115
00:07:25,860 --> 00:07:28,890
So let's add this item name here, which is egg.

116
00:07:30,480 --> 00:07:35,130
Let me type in raw and I can see something like.

117
00:07:36,360 --> 00:07:37,050
Medium.

118
00:07:39,180 --> 00:07:41,230
Forty four grams.

119
00:07:41,340 --> 00:07:44,280
Let's say this is just the name of the food item.

120
00:07:44,370 --> 00:07:49,260
So this will actually give us a description about the food item, which we have here.

121
00:07:49,530 --> 00:07:52,530
And now the protein content of that is six grams.

122
00:07:53,220 --> 00:07:55,980
Let's also check the carbohydrate content as well.

123
00:07:56,490 --> 00:08:02,010
So if you actually take a look over here, the cowboy hat, the content of that is point three.

124
00:08:02,550 --> 00:08:10,470
So I can see point three grams here and the fat content is nothing but four point two grams.

125
00:08:10,480 --> 00:08:12,570
So I would see four point two.

126
00:08:14,350 --> 00:08:20,460
And now, as far as the calories are concerned, we have 63 calories in that amount of egg.

127
00:08:20,680 --> 00:08:22,870
So I will see 63 up or here.

128
00:08:23,410 --> 00:08:24,730
And now I'll see if that.

129
00:08:24,850 --> 00:08:29,770
So currently it will see food object one and it won't display the name of the item.

130
00:08:30,280 --> 00:08:36,260
So in order to display the name of the item, you just need to go to models and define the SDM method.

131
00:08:36,280 --> 00:08:36,970
Up over here.

132
00:08:37,630 --> 00:08:45,700
So you simply need to type in def double underscore SD simply bossin cell for you and then make this

133
00:08:45,700 --> 00:08:46,510
thing return.

134
00:08:47,440 --> 00:08:48,290
Self-taught.

135
00:08:48,470 --> 00:08:48,730
Name.

136
00:08:48,880 --> 00:08:51,400
So that will actually make sure to display the name.

137
00:08:51,480 --> 00:08:52,150
A boy here.

138
00:08:52,390 --> 00:08:56,260
So when I had to refresh, as you can see now you have the name of that item.

139
00:08:56,920 --> 00:09:01,630
Now I'm actually going to go ahead and add a couple more food items in here.

140
00:09:02,440 --> 00:09:05,470
So as to make our application look realistic.

141
00:09:05,530 --> 00:09:09,520
So you can actually go ahead and add a couple of items of your choice.

142
00:09:09,550 --> 00:09:13,810
You can actually search for the micro nutrients in Google and add them up over here.

143
00:09:14,020 --> 00:09:17,470
So I won't be going through the steps of adding those up over here.

144
00:09:17,950 --> 00:09:19,360
So that's it for this lecture.

145
00:09:19,570 --> 00:09:24,830
And in the upcoming lecture, what we will do is that once we have added a couple items in here, we

146
00:09:24,900 --> 00:09:31,690
will actually go ahead and create a view or to allow the user to select one of these items from our

147
00:09:31,690 --> 00:09:32,170
list.

148
00:09:32,620 --> 00:09:34,300
So thank you very much for watching.

149
00:09:34,390 --> 00:09:36,160
And I'll see you guys next time.

150
00:09:36,400 --> 00:09:36,880
Thank you.

