1
00:00:00,120 --> 00:00:01,890
Hello and welcome to this lecture.

2
00:00:02,040 --> 00:00:08,730
And in this lecture, we will go ahead and learn about adding the user's ability to add a particular

3
00:00:08,730 --> 00:00:11,760
food item selected over here to the database.

4
00:00:12,180 --> 00:00:17,630
So in the previous lecture, we have actually created this particular model, which is the consumable.

5
00:00:17,910 --> 00:00:25,140
And now we wish to add some data or we wish to add a functionality which allows user to add the food

6
00:00:25,150 --> 00:00:28,020
consumed or here into this particular model.

7
00:00:28,620 --> 00:00:33,660
So for that very purpose, what we need to do is that we need to go ahead and modify this form right

8
00:00:33,660 --> 00:00:37,260
here and we need to be able to add a submit button right here.

9
00:00:37,800 --> 00:00:39,690
So let's quickly go ahead and do that.

10
00:00:39,930 --> 00:00:42,450
So I will go to the indexed on each YAML file.

11
00:00:42,930 --> 00:00:49,710
And in here, first of all, you need to go into the form and in here I just need to add a button.

12
00:00:50,100 --> 00:00:51,220
So I will see button.

13
00:00:51,630 --> 00:00:55,110
And in here, the type of this thing is going to be submit.

14
00:00:55,380 --> 00:00:57,510
So that will actually add a submit button.

15
00:00:57,540 --> 00:01:00,320
And let's say you want to name this button as add.

16
00:01:00,450 --> 00:01:01,750
So I will add in here.

17
00:01:02,330 --> 00:01:08,960
However, the problem here is that we have not yet mentioned the type of method.

18
00:01:09,150 --> 00:01:10,860
This form is going to use.

19
00:01:10,890 --> 00:01:13,830
So this form is going to actually post data.

20
00:01:14,220 --> 00:01:17,940
So the form method over here should actually be equal to post.

21
00:01:18,360 --> 00:01:20,700
So I will say form method equals.

22
00:01:21,060 --> 00:01:22,290
That's going to be post.

23
00:01:22,680 --> 00:01:24,510
And you don't need an action here.

24
00:01:24,540 --> 00:01:26,490
So I will get rid of this action right here.

25
00:01:27,420 --> 00:01:33,690
So now what this post method would do is that it will actually pick the option value which is selected,

26
00:01:34,080 --> 00:01:40,770
and then it will go ahead and put that value up into a consumer database or the consumer model.

27
00:01:41,010 --> 00:01:47,040
So in order to go ahead and extract this value from this specific select box, you obviously need to

28
00:01:47,040 --> 00:01:53,190
go ahead and give this select box some sort of an I.D. because we are going to make use of that I.D.

29
00:01:53,220 --> 00:02:00,840
and the name of this select box inside of you to get the data when the user clicks this particular submit

30
00:02:00,840 --> 00:02:01,230
button.

31
00:02:01,380 --> 00:02:04,690
So here, let's go ahead and add in some sort of name.

32
00:02:04,770 --> 00:02:12,960
So I will see food and the school consumed as the name of the select box, and I will go ahead and make

33
00:02:12,960 --> 00:02:16,550
sure to use the same thing as the ideas felt.

34
00:02:17,040 --> 00:02:24,210
So now once we have the name and I.D. and once we have a submit button, let's go back here and refresh.

35
00:02:24,360 --> 00:02:27,930
So as you can see now, I can select something and click on ADD.

36
00:02:28,380 --> 00:02:31,110
And now nothing happens here when I click on ADD.

37
00:02:31,500 --> 00:02:37,200
That's because we have not yet handled what happens when the user actually submits a post request.

38
00:02:37,620 --> 00:02:43,320
So now, in order to handle the post request, we will go to The View start b y file, which is this

39
00:02:43,320 --> 00:02:45,390
file right here and in here.

40
00:02:45,660 --> 00:02:50,760
As you can see now, we do have the next few here, but this is not for the post request.

41
00:02:51,150 --> 00:02:53,310
This is just to go ahead and get the data.

42
00:02:53,790 --> 00:02:59,670
So now, in order to handle the post request here, first of all, will go ahead and make sure if the

43
00:02:59,670 --> 00:03:01,650
incoming request is a post request.

44
00:03:01,740 --> 00:03:09,360
So in order to make sure if the incoming request is a post request, I will see if request don't method.

45
00:03:10,080 --> 00:03:13,860
If this is equal, equal, do post.

46
00:03:14,460 --> 00:03:19,440
In that case, what we want to do is that we want to actually get the data submitted by the user.

47
00:03:19,560 --> 00:03:21,570
And I am missing a quotation here.

48
00:03:21,660 --> 00:03:23,790
So after adding that I can now go ahead.

49
00:03:24,270 --> 00:03:30,690
And first of all, get the data which is submitted by the user, which is nothing but this option which

50
00:03:30,690 --> 00:03:34,380
is selected from this particular select menu or the select box.

51
00:03:34,950 --> 00:03:37,350
So let's go ahead and do that real quick.

52
00:03:37,620 --> 00:03:41,810
So I will see food and the school consumed.

53
00:03:42,300 --> 00:03:45,660
So I will get the food consumed here by saying request.

54
00:03:47,400 --> 00:03:48,960
Don't post.

55
00:03:49,170 --> 00:03:51,990
So that would actually give me the data from the post request.

56
00:03:52,380 --> 00:03:54,490
However, we don't want the entire data.

57
00:03:54,540 --> 00:03:56,850
But we only want the food consumed.

58
00:03:56,880 --> 00:04:01,060
So I will see food in the school consumed.

59
00:04:01,260 --> 00:04:05,520
So now I will have access to whatever value the user has selected.

60
00:04:06,030 --> 00:04:10,400
Now, this will just give us the text name of the food item consumed.

61
00:04:10,440 --> 00:04:17,399
So remember that if you just go back here and if you select one of these items, this is not an actual

62
00:04:17,399 --> 00:04:17,850
object.

63
00:04:17,880 --> 00:04:22,530
But this is simply some plain text, which is nothing but the name of the food item.

64
00:04:23,010 --> 00:04:30,900
So even if I click add up or here, what technically happens is that this particular thing will only

65
00:04:30,900 --> 00:04:34,590
give us the name and not an actual food object.

66
00:04:35,100 --> 00:04:40,170
So try to understand the difference here, the difference between just the name and just the food object.

67
00:04:40,530 --> 00:04:42,390
What we want is the food object.

68
00:04:42,420 --> 00:04:45,420
However, this thing right here is only the name.

69
00:04:45,900 --> 00:04:49,020
So how would you get an actual object from this name?

70
00:04:49,380 --> 00:04:55,560
So in order to get an actual object, which is the food object from its name, we need to go ahead and

71
00:04:55,560 --> 00:04:56,970
write in another query here.

72
00:04:57,420 --> 00:05:00,690
So you need to do something like food, which is nothing but the model.

73
00:05:01,080 --> 00:05:04,200
So from the food model, you want to get an object.

74
00:05:04,380 --> 00:05:12,990
So I will say food don't objects dot get and in here to this particular kid, I will pass in that I

75
00:05:12,990 --> 00:05:18,180
want the name of this object to be equal to the name which we just got.

76
00:05:18,600 --> 00:05:24,000
So the name which we just got from this particular post request is currently received in food consumed.

77
00:05:24,390 --> 00:05:31,140
So I will simply go ahead and just make sure to get that name and pass it on over here.

78
00:05:31,290 --> 00:05:38,190
So what this will do is that it will actually take that name value and out of all the food objects,

79
00:05:38,520 --> 00:05:44,970
it will select only that object whose name is actually the name which the user has selected over here.

80
00:05:45,000 --> 00:05:49,570
So that means if I select also a hill and if I click on ADD.

81
00:05:49,770 --> 00:05:57,820
So technically what happens is that if I go back to my code, I will only get orzo here.

82
00:05:58,470 --> 00:06:00,120
So autres not an object.

83
00:06:00,150 --> 00:06:01,830
It's simply some plain text.

84
00:06:02,160 --> 00:06:08,920
However, to get the actual OT's object from the food model, we have this query right over here, which

85
00:06:08,930 --> 00:06:15,030
is food that objects target and it will get that particular object whose name is OT's.

86
00:06:15,660 --> 00:06:19,890
So now once we have this particular object, you do need to see it into something.

87
00:06:20,220 --> 00:06:24,120
So I will see it then do something like consume.

88
00:06:24,330 --> 00:06:27,770
So I will say consume equals the food consumed.

89
00:06:27,780 --> 00:06:30,450
So now and consume we actually have an object.

90
00:06:30,510 --> 00:06:31,710
And not just the name.

91
00:06:32,280 --> 00:06:35,130
So I hope that this actually clears up your confusion.

92
00:06:35,880 --> 00:06:37,590
We now have the food object.

93
00:06:37,650 --> 00:06:44,520
However, if you take a look at our model, which is this model right here, we don't just want the

94
00:06:44,520 --> 00:06:48,780
food consumed object, but we also want the user object as well.

95
00:06:49,260 --> 00:06:52,320
So now how exactly can we go ahead and get the user?

96
00:06:52,990 --> 00:06:59,940
So when the user actually add something to this particular model, which is the consumer model, we

97
00:06:59,940 --> 00:07:04,140
usually want to go ahead and add it to a user who's currently logged in.

98
00:07:04,320 --> 00:07:07,590
So let's say, for example, currently we have a super user.

99
00:07:08,130 --> 00:07:13,110
So none of the super user is logged then we actually want to get super user.

100
00:07:13,620 --> 00:07:17,640
And add this food object to the consumption list of that super user.

101
00:07:18,240 --> 00:07:21,720
So getting the current user is actually quite simple.

102
00:07:22,080 --> 00:07:24,930
You just go ahead and see request dot user.

103
00:07:24,960 --> 00:07:27,690
And that will give you the user who's currently logged in.

104
00:07:28,140 --> 00:07:30,840
So I will see this thing into something called as user.

105
00:07:31,290 --> 00:07:31,500
OK.

106
00:07:31,590 --> 00:07:35,250
So now once we have the user, that is the user object.

107
00:07:35,670 --> 00:07:38,100
And once we also have the food object as well.

108
00:07:38,520 --> 00:07:45,120
Now, I can actually go ahead and construct an object of the type consume, which is nothing but of

109
00:07:45,120 --> 00:07:51,870
the type of this model can zoom right over here so that we can add that object to the table consume.

110
00:07:52,350 --> 00:07:57,390
So now let's head back to the views and I will see consume.

111
00:07:57,480 --> 00:08:01,460
So this will be the object name and the object is going to be of the type consume.

112
00:08:01,620 --> 00:08:09,660
And to this particular consume object, Will Possin user asked the user and food consumed as nothing

113
00:08:09,660 --> 00:08:11,190
but this consume right here.

114
00:08:12,030 --> 00:08:14,250
So I will see this equals consume.

115
00:08:15,280 --> 00:08:15,510
Okay.

116
00:08:15,600 --> 00:08:20,700
So now once the object is created, we now simply need to go ahead and see if that object.

117
00:08:20,790 --> 00:08:23,790
So I will see consume don't save.

118
00:08:24,390 --> 00:08:27,630
And once the object was saved, we are pretty much good to go.

119
00:08:27,990 --> 00:08:31,350
So this is what happens when the request method is post.

120
00:08:31,590 --> 00:08:38,850
However, if you take a look at this code, this particular code will now be a part of the LDS block.

121
00:08:39,210 --> 00:08:42,450
So I will say else this would happen.

122
00:08:42,570 --> 00:08:46,140
Which is it will get all the food items and it will displayed.

123
00:08:46,500 --> 00:08:47,990
Them up in deselect box.

124
00:08:48,240 --> 00:08:50,550
However, this part is going to remain outside.

125
00:08:51,120 --> 00:08:57,720
Now, one more thing which you need to do here is that you can either exclude this out of the else block.

126
00:08:58,710 --> 00:09:04,470
And the reason for that is because even if you actually go ahead and select one item and click on ADD,

127
00:09:04,980 --> 00:09:08,070
you would still want access to these items right over here.

128
00:09:08,400 --> 00:09:13,050
So for that very purpose, I will actually place it inside the else block.

129
00:09:13,500 --> 00:09:18,870
However, I will also copy this line of code and pasted inside the block as well.

130
00:09:18,900 --> 00:09:25,230
That means even if the if Block executes or the else block execute all, we will always want these items.

131
00:09:25,590 --> 00:09:31,890
And if you don't have this particular line of code here, what would happen is that if you just submit

132
00:09:31,920 --> 00:09:39,240
a post request, then there would be no food object remaining to be passed up over here.

133
00:09:39,960 --> 00:09:41,850
So that's one thing which you need to remember.

134
00:09:42,300 --> 00:09:42,490
Okay.

135
00:09:42,690 --> 00:09:47,010
So now once we are done with this, let's see if this thing would work.

136
00:09:47,190 --> 00:09:52,320
So, first of all, you do need to make sure that your long bin as an admin.

137
00:09:52,460 --> 00:09:55,020
So, yeah, I'm LogMeIn as an admin.

138
00:09:55,440 --> 00:09:58,620
So currently, if I go to consumers, I have two objects.

139
00:09:58,740 --> 00:10:06,390
So let me delete those objects first so that we are sure that we are able to add the items we are from.

140
00:10:06,960 --> 00:10:08,970
So now let's actually cool ahead.

141
00:10:09,320 --> 00:10:10,530
Go to the homepage.

142
00:10:11,300 --> 00:10:13,050
Let's select a brown bread.

143
00:10:13,260 --> 00:10:14,640
And when I click on ADD.

144
00:10:16,000 --> 00:10:17,980
OK, it seems consumers not defined.

145
00:10:18,340 --> 00:10:20,200
That should be a minor error.

146
00:10:21,160 --> 00:10:22,900
So it said line number 10.

147
00:10:23,960 --> 00:10:29,400
Sue, yeah, we have this error because we have not imported the consumer model.

148
00:10:30,000 --> 00:10:32,370
So let me just go ahead and do that real quick.

149
00:10:32,910 --> 00:10:37,740
And now on just make sure that you don't refresh this, but instead you visit the.

150
00:10:37,740 --> 00:10:38,160
You are.

151
00:10:39,370 --> 00:10:41,290
And now let's elect brown bread.

152
00:10:41,560 --> 00:10:48,670
And when I click on ADD, as you can see, the page refreshed and now we need to make sure that it is

153
00:10:48,700 --> 00:10:50,920
added by going into the admin panel.

154
00:10:51,610 --> 00:10:55,770
So if I go to consumers, as you can see, we have the consume option three.

155
00:10:56,320 --> 00:11:02,500
And if I open this thing up, I will actually have the brown bread, which is being added by this user

156
00:11:02,500 --> 00:11:02,980
right here.

157
00:11:03,310 --> 00:11:10,600
So that means we are now actually able to add a food item along with a particular user to this particular

158
00:11:10,600 --> 00:11:13,570
form, which we have on our index page.

159
00:11:14,320 --> 00:11:15,920
So that's it for this lecture.

160
00:11:16,360 --> 00:11:21,700
And now in the upcoming lecture, what we will do is that we will actually go ahead and create a table

161
00:11:22,060 --> 00:11:26,910
that will list out all the user consumption activity on this particular speech.

162
00:11:27,280 --> 00:11:33,070
So this next page should now also display the items which are actually consumed by this specific use

163
00:11:33,070 --> 00:11:33,190
it.

164
00:11:34,150 --> 00:11:38,080
So thank you very much for watching and I'll see you guys next time.

165
00:11:38,470 --> 00:11:39,040
Thank you.

