1
00:00:00,190 --> 00:00:02,070
So hello and welcome to this lecture.

2
00:00:02,280 --> 00:00:07,710
And in this lecture, we will go ahead and add a couple more functionalities to our application.

3
00:00:08,100 --> 00:00:14,700
However, one minor change which I have done here, is that I have added these bulldog's over here to

4
00:00:14,700 --> 00:00:21,390
the NIH chairman of these tables so that these things will actually look a little bit bolder and it

5
00:00:21,390 --> 00:00:25,860
will give a nice look and feel to the particular table, which we have here.

6
00:00:26,070 --> 00:00:31,850
So the only thing which you need to do is that you need to append these brake tags over here and append

7
00:00:31,860 --> 00:00:32,189
this.

8
00:00:32,250 --> 00:00:34,050
And GM, which stands for Gramp's.

9
00:00:34,440 --> 00:00:39,120
So as this is Kalari, I will just to keep Cal up over here.

10
00:00:39,720 --> 00:00:42,000
So this will actually look a lot better.

11
00:00:42,520 --> 00:00:42,710
Okay.

12
00:00:42,900 --> 00:00:49,650
So now after doing this, let's now learn how to go ahead and add a progress bar at the top up or here,

13
00:00:49,680 --> 00:00:56,430
which will display the total number of calories which are consumed in a form of a graph or in form of

14
00:00:56,460 --> 00:00:57,210
a progress bar.

15
00:00:57,780 --> 00:01:00,660
So in order to do that, what we will do is that.

16
00:01:01,770 --> 00:01:09,030
I will go inside a container, which is this tag right here and right inside this main container after

17
00:01:09,030 --> 00:01:09,990
the container tag.

18
00:01:10,050 --> 00:01:14,280
We'll go ahead and give some space in here by adding some brake tags.

19
00:01:14,610 --> 00:01:17,070
So let me add a couple of big tags here.

20
00:01:18,090 --> 00:01:23,970
So when you go ahead and do that, as you can see, there will be a little bit of space left at the

21
00:01:23,970 --> 00:01:24,330
top.

22
00:01:24,780 --> 00:01:31,950
So now here I will see something like calorie goal, which will actually display the number of calories

23
00:01:31,950 --> 00:01:33,510
we are supposed to consume today.

24
00:01:34,050 --> 00:01:42,000
So in order to type that Titelman, I will first create a heading each for and I will see calorie goal

25
00:01:43,740 --> 00:01:47,940
and then I will again make use of a break tag to have some space.

26
00:01:48,390 --> 00:01:53,560
And right after that I will go ahead and create a route to actually hold our progress bar.

27
00:01:54,150 --> 00:01:58,450
So I will see div class equal struhl in there.

28
00:01:58,530 --> 00:02:01,230
I will create another div which is going to be for the column.

29
00:02:01,260 --> 00:02:06,270
So Dave, class equals let's say call Dash M.D. Nashed nine.

30
00:02:06,450 --> 00:02:10,979
That means it will actually span nine columns and they will be offset of one.

31
00:02:11,700 --> 00:02:16,680
So now once we have this specific column, I will now go ahead and create a progress bine here.

32
00:02:17,250 --> 00:02:21,140
Now in order to create a progress bar, first of all, I'll create a view.

33
00:02:21,360 --> 00:02:26,700
So I will call it as div class equals progress.

34
00:02:26,790 --> 00:02:28,680
So this is nothing but a progress bar.

35
00:02:29,220 --> 00:02:34,980
And now in order to actually get a progress bar, you can actually get a progress bar from the bootstraps

36
00:02:35,010 --> 00:02:36,000
official Web site.

37
00:02:36,090 --> 00:02:45,570
So if you go ahead and search for bootstrap progress bar, it will actually give you access to the bootstraps

38
00:02:45,720 --> 00:02:46,290
progress bar.

39
00:02:46,890 --> 00:02:51,180
So as you can see, these are the different kinds of progress past which we have.

40
00:02:51,600 --> 00:02:56,730
And currently we actually want a progress bar, which looks something like this.

41
00:02:56,850 --> 00:02:59,820
So you can select any one of these progress bars right here.

42
00:03:00,360 --> 00:03:03,060
Go back here and pasted right up in there.

43
00:03:03,690 --> 00:03:05,640
So this is just a progress bar.

44
00:03:07,000 --> 00:03:09,690
And the rule of this is going to be progress bar.

45
00:03:10,260 --> 00:03:14,310
The style is going to be containing the word.

46
00:03:14,440 --> 00:03:19,750
So let's go ahead and set the current with the zero percent and the area value.

47
00:03:19,780 --> 00:03:22,420
Now, let's add that value to zero as well.

48
00:03:22,540 --> 00:03:24,850
And let's also said the maximum value to zero.

49
00:03:25,330 --> 00:03:26,710
So if you go back here.

50
00:03:28,540 --> 00:03:33,910
And if you would refresh, as you can see, you have your progress bar right up over here.

51
00:03:34,390 --> 00:03:38,050
However, it's actually sticking to this menu right here.

52
00:03:38,260 --> 00:03:40,120
So let's actually add some space in here.

53
00:03:40,720 --> 00:03:43,540
So I will again make use of a couple break tanks here.

54
00:03:44,830 --> 00:03:46,840
And now this will look a little bit better.

55
00:03:47,140 --> 00:03:50,410
Okay, so now currently the progress bar appears to be empty.

56
00:03:50,590 --> 00:03:57,220
So in order to fill up this progress bar, you actually need to go ahead and set its attribute, which

57
00:03:57,220 --> 00:03:59,170
is nothing but the width attribute.

58
00:03:59,650 --> 00:04:07,160
So let's say if you go ahead and set the world to, let's see, 50 percent, let's see what happens.

59
00:04:07,190 --> 00:04:11,720
So if you go ahead and hit refresh, as you can see, the progress bar kind of filled up.

60
00:04:12,100 --> 00:04:19,690
However, we want to make this progress bar dynamic, that is as the calories of the user increases.

61
00:04:20,140 --> 00:04:25,660
We want to go ahead and fill up this progress bar depending upon the total number of calories required.

62
00:04:26,500 --> 00:04:29,740
So we want to make this value of 50 percent as dynamic.

63
00:04:30,670 --> 00:04:33,730
So we are again going to do the same thing using JavaScript.

64
00:04:34,270 --> 00:04:39,520
However, even before that, let's actually try to change the color of this progress bar.

65
00:04:39,790 --> 00:04:47,230
So in order to change the color of this progress bar, you just need to go ahead and assign it a class

66
00:04:47,830 --> 00:04:49,840
which is Biji Dash success.

67
00:04:49,870 --> 00:04:52,990
So I will cbg dash success.

68
00:04:53,110 --> 00:04:55,990
And this will actually change its color to green.

69
00:04:56,530 --> 00:04:56,760
Okay.

70
00:04:56,950 --> 00:04:58,930
That actually looks a little bit better.

71
00:04:59,290 --> 00:05:03,430
Now let's get back and change its bid to zero percent.

72
00:05:04,390 --> 00:05:08,680
And now let's dynamically assign the word to this particular progress bar.

73
00:05:09,130 --> 00:05:15,280
So let's say you actually want to now go ahead and mark this point as a zero kind of Reagen zoomed.

74
00:05:15,370 --> 00:05:21,430
And let's say if the users target is to go ahead and consume 2000 calories in a day, you actually now

75
00:05:21,430 --> 00:05:26,530
want to go ahead and mention that this point over here is 2000 calories.

76
00:05:26,890 --> 00:05:31,880
So as the progress bar progresses ahead, the calories would move from zero to 2000.

77
00:05:32,320 --> 00:05:36,910
So, first of all, let's go ahead and calculate the percentage of calories consumed.

78
00:05:37,030 --> 00:05:41,290
So in order to do that, I will get back into my script tag right at the very bottom.

79
00:05:41,710 --> 00:05:45,040
And then you here, let's calculate the current calorie percentage.

80
00:05:45,340 --> 00:05:50,050
So I will create a variable called less Calpol, which is calorie percentage.

81
00:05:50,320 --> 00:05:55,120
And it will actually go ahead, get the current calories and divided by 2000.

82
00:05:55,210 --> 00:06:00,280
So the current calories are actually stored in a variable called Lah's calories.

83
00:06:00,460 --> 00:06:01,130
So I will do.

84
00:06:01,150 --> 00:06:03,240
Calories divided by 2000.

85
00:06:03,460 --> 00:06:11,200
And when we multiply it by 100, it will actually give us the percentage of calories which are currently

86
00:06:11,200 --> 00:06:13,490
consumed as compared to 2000.

87
00:06:13,870 --> 00:06:19,900
So if the user's current consumption is a thousand or so thousand, divided by 2000 would be point five

88
00:06:20,050 --> 00:06:21,140
point five and two.

89
00:06:21,160 --> 00:06:22,900
One hundred would be 50 percent.

90
00:06:23,020 --> 00:06:30,760
So now once we have this percentage now, we just need to go ahead and assign this percentage to this

91
00:06:30,910 --> 00:06:37,120
particular progress bar, which we have here now in order to assign properties to this particular progress

92
00:06:37,120 --> 00:06:37,450
bar.

93
00:06:37,840 --> 00:06:41,800
You first need to go ahead and get access to this particular progress bar.

94
00:06:42,070 --> 00:06:45,070
And now this progress pie does not have any sort of I.D..

95
00:06:45,460 --> 00:06:49,570
However, it does have a last name, which is called as Progress Bar.

96
00:06:50,020 --> 00:06:52,180
So let's make use of the same class name.

97
00:06:52,690 --> 00:06:56,380
Go back over here and get access to the progress bar.

98
00:06:56,860 --> 00:07:01,040
So I would see document don't get element.

99
00:07:01,180 --> 00:07:06,970
And now, instead of using gett element by idee, I would get elements by last name.

100
00:07:07,180 --> 00:07:10,480
And here we want to get a class name, which is Progress Bar.

101
00:07:10,540 --> 00:07:12,160
So I will get the progress by here.

102
00:07:12,550 --> 00:07:17,560
And as we are using get elements by idee.

103
00:07:17,740 --> 00:07:21,580
This is actually used when there are actually multiple elements present on your site.

104
00:07:22,000 --> 00:07:24,730
So currently there is only one progress bar present.

105
00:07:24,820 --> 00:07:30,130
However, you need to mention that up over here that we are accessing the first progress bar.

106
00:07:30,610 --> 00:07:34,510
So the counting of any elements by class name starts from zero.

107
00:07:34,600 --> 00:07:37,100
So here I need to mention an index zero.

108
00:07:37,180 --> 00:07:40,090
This means we want to access the fullest progress bar.

109
00:07:40,720 --> 00:07:43,180
And now once we have the access to the progress bar.

110
00:07:43,600 --> 00:07:44,830
Now, instead of setting it?

111
00:07:44,830 --> 00:07:50,080
S each gemal, we actually want to set its property or attribute.

112
00:07:50,680 --> 00:07:54,670
That attribute is nothing but the width attribute.

113
00:07:55,090 --> 00:07:59,980
So now I will go ahead and make use of a method which is called as set attribute.

114
00:08:00,370 --> 00:08:07,180
So I will say this dot set attribute and here you need to mention the attribute which you want to set.

115
00:08:07,270 --> 00:08:10,400
So I want to set the style of the progress bar.

116
00:08:12,490 --> 00:08:13,930
So I will type in style here.

117
00:08:14,620 --> 00:08:17,910
And from the style, I actually want to set the width.

118
00:08:18,040 --> 00:08:19,540
So I will save weight.

119
00:08:20,350 --> 00:08:27,100
And now I actually want to set the world equal to the calorie percentage, which we have just called.

120
00:08:27,170 --> 00:08:32,299
Related, so I will go ahead and in order to append this value, I will use a plus sign.

121
00:08:32,330 --> 00:08:33,890
So I will say plus Calpol.

122
00:08:34,669 --> 00:08:38,360
And then I will assign the person that symbol right here.

123
00:08:38,600 --> 00:08:40,549
So I will add a percent sign in here.

124
00:08:41,179 --> 00:08:44,810
So once this thing is done, this might seem a little bit confusing.

125
00:08:44,840 --> 00:08:50,300
But this is just what we have up over here inside the progress bar.

126
00:08:50,510 --> 00:08:52,520
It will actually go ahead and set the style.

127
00:08:52,550 --> 00:08:53,550
Equals would.

128
00:08:54,050 --> 00:08:57,680
Then it will substitute this value zero with the Khal percentage.

129
00:08:57,710 --> 00:09:00,050
And it will append the person sign the front.

130
00:09:01,670 --> 00:09:03,560
So now if I go ahead and save this.

131
00:09:03,710 --> 00:09:04,970
Let's see if it works.

132
00:09:05,000 --> 00:09:09,590
So when I had refresh, as you can see, the progress bar is right here.

133
00:09:09,890 --> 00:09:16,460
And that's because the total calorie consumption is nine twenty nine calories, which is around half

134
00:09:16,460 --> 00:09:18,560
of 2000, which appears to be so.

135
00:09:19,040 --> 00:09:24,250
Now let's try to add some calorie heavy Milawa here, which is oats.

136
00:09:24,470 --> 00:09:26,570
And let's see the progress bar moves ahead.

137
00:09:27,080 --> 00:09:29,000
So I will see oats add.

138
00:09:29,270 --> 00:09:33,530
And as you can see, as soon as I add that, it actually went ahead.

139
00:09:33,650 --> 00:09:38,210
So, again, if I add that it again moved ahead, if I add eggs.

140
00:09:38,870 --> 00:09:44,150
This actually moves ahead, but it moves at a very slow pace because eggs actually have less amount

141
00:09:44,150 --> 00:09:44,870
of calories.

142
00:09:45,320 --> 00:09:48,560
However, if you will notice, this actually works.

143
00:09:49,010 --> 00:09:57,380
And as you can see at this point, we are almost at nineteen hundred calories and progress bar as close

144
00:09:57,380 --> 00:09:58,760
to the mark.

145
00:09:58,850 --> 00:10:05,450
So now as you can see, 1982 calories and we are just about to touch the 2000 calorie mark.

146
00:10:05,660 --> 00:10:13,310
So that means we were successful in adding this particular progress bar to actually track the total

147
00:10:13,310 --> 00:10:15,530
number of calories consumed by a user.

148
00:10:15,740 --> 00:10:17,270
Again, says Target for the day.

149
00:10:17,780 --> 00:10:19,400
And that's it for this lecture.

150
00:10:19,580 --> 00:10:21,470
And I'll see you guys in the next one.

151
00:10:21,740 --> 00:10:22,240
Thank you.

