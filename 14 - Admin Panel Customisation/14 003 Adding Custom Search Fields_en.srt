1
00:00:00,090 --> 00:00:06,450
So in this lecture, let's go ahead and learn how to add a search law here, which allows us to search

2
00:00:06,450 --> 00:00:08,350
depending upon different parameters.

3
00:00:08,430 --> 00:00:11,970
So adding search fill is a lot simpler than you think.

4
00:00:12,240 --> 00:00:13,440
You just need to go ahead.

5
00:00:13,440 --> 00:00:19,020
And just as you have created this little display in here, you just need to mention the search fields

6
00:00:19,020 --> 00:00:24,960
inside this specific class so you can type in search and rescue fields.

7
00:00:25,350 --> 00:00:30,990
And let's say if you only want to search depending upon the title, so I can simply type in title over

8
00:00:30,990 --> 00:00:31,230
here.

9
00:00:31,260 --> 00:00:36,350
So now once you go ahead and add title in here, as you can see now you have a search field in here.

10
00:00:36,870 --> 00:00:39,270
So let's say I want to search for Bich.

11
00:00:39,270 --> 00:00:43,890
So when I go head type and Bich, as you can see, I have Bigo here.

12
00:00:44,760 --> 00:00:46,830
So now let's go ahead.

13
00:00:46,830 --> 00:00:50,600
And instead of Pikelet search for a laptop.

14
00:00:50,610 --> 00:00:52,440
So then I make a search for a laptop.

15
00:00:52,860 --> 00:00:55,950
As you can see, I get laptop bag and EBC laptop.

16
00:00:56,760 --> 00:01:03,030
Now let's assume I want to search depending upon the category so I can simply go ahead and make a search

17
00:01:03,030 --> 00:01:04,890
according to the category as well.

18
00:01:05,340 --> 00:01:09,130
So let's go ahead and add the category freeloader.

19
00:01:09,540 --> 00:01:12,600
So so simply go ahead and category.

20
00:01:13,140 --> 00:01:17,270
So now this will allow me to search as per the category.

21
00:01:17,370 --> 00:01:22,470
So go ahead, get rid of that and make sure to have the comma at the end.

22
00:01:22,830 --> 00:01:30,510
And now if I hit refresh, as you can see, it's going to give me no result because there's no product

23
00:01:30,510 --> 00:01:32,460
whose category was laptop.

24
00:01:32,640 --> 00:01:38,760
So now if I go ahead and if I search for the feel, which is fashion, and when I click on such as you

25
00:01:38,760 --> 00:01:45,030
can see, I get the products who has the category as fashion in a similar manner, you can go ahead

26
00:01:45,030 --> 00:01:48,210
and use any kind of search field which you want.

27
00:01:48,240 --> 00:01:53,400
So let's say if you want to search depending upon description, you simply go ahead and make a change

28
00:01:53,400 --> 00:01:55,800
over here and type in description over here.

29
00:01:55,950 --> 00:02:00,400
And now it's actually going to go ahead and search depending upon your description.

30
00:02:00,840 --> 00:02:03,890
So let's say I type in bike here.

31
00:02:04,260 --> 00:02:09,930
As you can see, the description of this particular thing contains bike, and this is why I get this

32
00:02:09,930 --> 00:02:10,300
result.

33
00:02:10,320 --> 00:02:14,460
So that's how you can go ahead and add any kind of search field which you want.

34
00:02:14,730 --> 00:02:20,790
So in the next lecture, what we will do is that will go ahead and we will learn how to customize this

35
00:02:20,790 --> 00:02:21,850
specific feel.

36
00:02:21,930 --> 00:02:26,760
So this field right here is the action field, which means that you can select any one of these products

37
00:02:26,760 --> 00:02:30,230
in here and you can only delete these products for now.

38
00:02:30,600 --> 00:02:36,630
But let's say instead of actually deleting these things, let's say you want to define your own custom

39
00:02:36,630 --> 00:02:37,150
action.

40
00:02:37,170 --> 00:02:43,980
So let's say, for example, if you want to change the category of certain items on one click, you

41
00:02:43,980 --> 00:02:46,770
can do that using this particular action.

42
00:02:47,220 --> 00:02:52,110
So in the upcoming lecture, let's learn how to customize this specific action fit.

43
00:02:53,040 --> 00:02:54,780
So thank you very much for watching.

44
00:02:55,140 --> 00:02:57,180
And I'll see you guys next time.

45
00:02:57,420 --> 00:02:58,020
Thank you.

