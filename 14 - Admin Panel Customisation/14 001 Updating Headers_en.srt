1
00:00:00,150 --> 00:00:05,880
Hello and welcome to this new section, and in the section, we will learn about how to customize the

2
00:00:05,880 --> 00:00:07,170
Django admin panel.

3
00:00:07,650 --> 00:00:11,220
So in the previous section we have created this e-commerce website.

4
00:00:11,490 --> 00:00:17,640
But if you have a look at the admin panel of our e-commerce website, the admin panel will look absolutely

5
00:00:17,640 --> 00:00:19,040
basic and generic.

6
00:00:19,470 --> 00:00:25,100
So if we go to admin, as you can see, we have the basic looking admin panel in here.

7
00:00:25,410 --> 00:00:31,940
And now let's say if you want to customize this specific admin panel according to your own choice,

8
00:00:32,070 --> 00:00:37,770
so we can actually go ahead and customize this admin panel as according to our need.

9
00:00:38,220 --> 00:00:44,310
So Django, not only does provide us with this by default admin panel, but it actually gives us a lot

10
00:00:44,310 --> 00:00:49,610
of options to play around and modify this admin panel as like we want.

11
00:00:50,100 --> 00:00:54,290
So this entire section is dedicated to customizing this admin panel.

12
00:00:54,630 --> 00:01:00,810
And in this specific lecture, we will learn about how to customize the site header, the site title

13
00:01:00,810 --> 00:01:01,940
and the indexed title.

14
00:01:02,310 --> 00:01:05,379
So let's go ahead and try changing the site title first.

15
00:01:05,940 --> 00:01:11,640
So now, in order to make any changes to the Django admin panel, you actually need to go to your code.

16
00:01:12,030 --> 00:01:17,430
And in here you first need to go ahead and go to the admin Lopevi file of your app.

17
00:01:17,700 --> 00:01:20,370
And in here you can make the changes which you want.

18
00:01:20,700 --> 00:01:28,050
So in order to change the admin header, you simply need to go ahead and type in admin DOT site.

19
00:01:29,170 --> 00:01:36,410
Don't cite underscore header, and you can go ahead and set this value to anything which you want.

20
00:01:36,430 --> 00:01:39,700
So let's say you want to type in e commerce.

21
00:01:41,690 --> 00:01:49,400
Sight see that and if we switch back to the admin and hit refresh, as you can see, you now have the

22
00:01:49,400 --> 00:01:53,050
name of the site change here, that is, you have changed the site.

23
00:01:53,360 --> 00:01:56,070
Now, what if you actually want to change the site title?

24
00:01:56,090 --> 00:02:01,880
So right now, if you have over this thing, as you can see, the site title is Django Site Admin.

25
00:02:02,840 --> 00:02:11,030
So if you wish to change that, you simply go ahead and type in admin, dot site, dot site and the

26
00:02:11,030 --> 00:02:13,100
school title equals.

27
00:02:13,970 --> 00:02:19,020
And let's see, we want to change it to something like ABC shopping.

28
00:02:19,370 --> 00:02:25,040
So when I go ahead do that and when I hit refresh, as you can see, the site title actually changed.

29
00:02:25,040 --> 00:02:28,160
It says Site Administration, ABC Shopping.

30
00:02:28,880 --> 00:02:32,610
Now let's say if you want to change the index site right over here.

31
00:02:33,080 --> 00:02:43,250
So in order to do that, you simply go ahead and type in admin dot site DOT index index underscore title

32
00:02:44,150 --> 00:02:44,840
equals.

33
00:02:45,200 --> 00:02:49,880
Let's say it says something like, uh, Manege, ABC.

34
00:02:52,320 --> 00:02:52,980
Shopping.

35
00:02:54,670 --> 00:02:59,710
So once you go ahead, save that and you hit refresh, as you can see, the site and next title has

36
00:02:59,710 --> 00:03:01,950
been changed to manage ABC shopping.

37
00:03:01,960 --> 00:03:06,450
So in this manner, you can go ahead and make the customizations as you want.

38
00:03:06,970 --> 00:03:11,800
And this is how you change the site, header the site title and decide in the title.

39
00:03:12,040 --> 00:03:18,400
So these were a few basic changes and in the next lecture will go ahead and we will learn how to display

40
00:03:18,400 --> 00:03:20,260
the other model fields over here.

41
00:03:20,380 --> 00:03:26,190
So right now, if you go to products, as you can see, we only have the product object name over here.

42
00:03:26,590 --> 00:03:32,800
But what if you actually want to change this and you also want to display the product price, the product

43
00:03:32,800 --> 00:03:35,780
discount price, the product category and the description.

44
00:03:35,920 --> 00:03:39,110
So we are going to learn how to do that in the upcoming lecture.

45
00:03:39,700 --> 00:03:41,560
So thank you very much for watching.

46
00:03:41,560 --> 00:03:43,600
And I'll see you guys next time.

47
00:03:43,810 --> 00:03:44,350
Thank you.

