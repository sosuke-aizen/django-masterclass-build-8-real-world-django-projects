# Django Masterclass : Build 8 Real World Django Projects

### 01 - Course Introduction/:

- 01 001 Course Introduction.mp4
- 01 002 Introduction To Django.mp4
- 01 002 Introduction-to-Django.pdf

### 02 - Downloading & Installing Required Software/:

- 02 001 Installing Python On Windows.mp4
- 02 002 Installing Python On Mac.mp4
- 02 003 Installing Django On Windows.mp4
- 02 004 Installing Django On Mac.mp4
- 02 005 Installing VS Code.mp4

### 03 - Project 1 Food Menu App  Setting Up A Django Project/:

- 03 001 food.txt
- 03 001 Project Overview What We Will Build.mp4
- 03 002 How To Create & Setup Django Project.mp4
- 03 003 Running Our App On Development Server.mp4

### 04 - Views & URL Patterns In Django/:

- 04 001 How-Django-App-Works.pdf
- 04 001 How Views Work In Django.mp4
- 04 002 Views In Django Implementation.mp4
- 04 003 URL Patterns.mp4
- 04 004 How Django URL Patterns Work Internally.mp4
- 04 004 How-Django-url-patterns-work.pdf
- 04 005 Writing Another View.mp4

### 05 - Database & Models In Django/:

- 05 001 Databases-Models-In-Django.pdf
- 05 001 Introduction To Database & Models.mp4
- 05 002 Database & Models.mp4
- 05 003 Data-Storage-In-Django.pdf
- 05 003 How Data Storage Works In Django.mp4
- 05 004 Using The Interactive Shell.mp4
- 05 005 Django Admin Panel & Creating Super User.mp4
- 05 006 How Data Retrieval Works In Django.mp4
- 05 006 Retrieving-Objects-In-Django.pdf
- 05 007 Reading Data From Database.mp4

### 06 - Templates/:

- 06 001 Django Templates.mp4
- 06 002 Passing Context To Templates.mp4
- 06 003 How-Templates-Work-In-Django.pdf
- 06 003 Why We Need Templates.mp4
- 06 004 Creating The Detail View.mp4
- 06 005 Completing The Detail View.mp4
- 06 006 Django Template Language.mp4
- 06 007 Removing Hardcoded URLs.mp4
- 06 008 Namespacing.mp4

### 07 - Static Files & Site Design/:

- 07 001 Static Files.mp4
- 07 002 Load static replace with just static as load static is depricated.mp4
- 07 003 More About Static Files.mp4
- 07 004 Creating Navbar.mp4
- 07 005 Creating Base Template.mp4
- 07 006 Adding Image Field To Model.mp4
- 07 007 Adding Actual Images.mp4
- 07 008 Designing The Detail View.mp4

### 08 - Forms In Django/:

- 08 001 Adding Form To Add Items.mp4
- 08 002 Adding Base Template To Form.mp4
- 08 003 Implementing The Edit Functionality.mp4
- 08 004 Implementing Delete Functionality.mp4

### 09 - Authentication In Django/:

- 09 001 Creating User Registration Form.mp4
- 09 002 Registration Success Message.mp4
- 09 003 Saving Users.mp4
- 09 004 Adding Additional Field.mp4
- 09 005 Logging In Users.mp4
- 09 006 Redirecting Registered Users & Logout Functionality.mp4
- 09 007 Adding Login Option To Navbar.mp4
- 09 008 Restricting Routes.mp4
- 09 009 Creating The Profile Model.mp4
- 09 010 Adding Path To Upload Images.mp4
- 09 011 Adding The User Profile Picture.mp4
- 09 012 Setting Up The Default Profile Picture.mp4

### 10 - Django Signals & Class Based Views/:

- 10 001 Django-Signals.pdf
- 10 001 What Are Django Signals.mp4
- 10 002 Implementing Django Signals.mp4
- 10 003 Class Based Views In Django.mp4
- 10 004 Implementing Class Based Detail View.mp4
- 10 005 Add User To Post.mp4
- 10 006 Adding Get Absolute URL Method.mp4
- 10 007 Automating User Association.mp4
- 10 008 Design Touchup Login Form.mp4
- 10 009 Design Touchup Register Page.mp4
- 10 010 Design Touchup Add Item Page.mp4
- 10 011 Section Conclusion What We Learned.mp4
- 10 012 Source Code.html
- 10 012 Source Code.md

### 11 - Building REST APIs With Python & Django/:

- 11 001 What is an API.mp4
- 11 001 What-is-a-REST-API.pdf
- 11 002 Introduction To Django Rest Framework.mp4
- 11 002 introduction-to-django-rest-framework.pdf
- 11 003 Setting Up The Django Project & Movie Model.mp4
- 11 004 Creating Serializer.mp4
- 11 005 Setting Up Views & URLs for API.mp4
- 11 006 Adding API Endpoints.mp4
- 11 007 Adding Image Field To API.mp4

### 12 - Pagination, Virtual Environment, Search & User Permissions/:

- 12 001 movieapp.zip
- 12 001 Section Intro What We Will Learn.mp4
- 12 002 Setting Up Virtual Environment For Our Project.mp4
- 12 003 Setting Up The Movies Model.mp4
- 12 004 Creating The View & Template.mp4
- 12 005 Adding Pagination.mp4
- 12 006 Adding Search Functionality.mp4
- 12 007 User Permissions.mp4
- 12 008 Source Code.html
- 12 008 Source Code.md

### 13 - Project 2 Building An E-commerce Site/:

- 13 001 ecom.txt
- 13 001 Project overview what we will build.mp4
- 13 002 Setting Up Project.mp4
- 13 003 Creating Product Model.mp4
- 13 004 Adding Products To Database.mp4
- 13 005 Building The Index View.mp4
- 13 006 Displaying Products On Index Page.mp4
- 13 007 Adding CSS To Our Site.mp4
- 13 008 Adding Search Functionality.mp4
- 13 009 Adding Pagination.mp4
- 13 010 Creating The Detail View For Products.mp4
- 13 011 Linking Index View With Detail View.mp4
- 13 012 Using Local Storage To Save Cart Items.mp4
- 13 013 Adding Query & Creating The Cart.mp4
- 13 014 Handling Button Click.mp4
- 13 015 Getting Products ID.mp4
- 13 016 Saving Items Into Cart.mp4
- 13 017 Displaying Number Of Items On Navbar.mp4
- 13 018 Adding A Popover.mp4
- 13 019 Modifying Popover Content.mp4
- 13 020 Adding Cart Items In Popover.mp4
- 13 021 Checkout Page Template.mp4
- 13 022 Modifying Local Storage.mp4
- 13 023 Adding Cart Items To List Group.mp4
- 13 024 Adding Checkout Form.mp4
- 13 025 Adding Cart Items To Database.mp4
- 13 026 Adding Item Prices.mp4
- 13 027 Calculating Order Total.mp4
- 13 028 Adding Order Total To Database.mp4
- 13 029 Fixing The Add-To-Cart Bug.mp4
- 13 030 Conclusion What We Learned.mp4
- 13 031 E-commerce Site Source Code.html
- 13 031 E-commerce Site Source Code.md

### 14 - Admin Panel Customisation/:

- 14 001 Updating Headers.mp4
- 14 002 Adding Custom Fields.mp4
- 14 003 Adding Custom Search Fields.mp4
- 14 004 Modifying Action Lists.mp4
- 14 005 Making Fields Editable.mp4

### 15 - Project 3 Building A Web Based CV Generator/:

- 15 001 cv.txt
- 15 001 Project Overview What We Will Build.mp4
- 15 002 Project Setup.mp4
- 15 003 Building Models.mp4
- 15 004 Creating forms.mp4
- 15 005 Building Views.mp4
- 15 006 POST Method.mp4
- 15 007 Building Template.mp4
- 15 008 Downloading Packages For MAC Users.mp4
- 15 009 Downloading Packages For Windows Users.mp4
- 15 010 Using PDFKIT.mp4
- 15 011 Creating List Of Profiles.mp4
- 15 012 CV Generator Source Code.html
- 15 012 CV Generator Source Code.md

### 16 - Project 4 Building A Web Based Site Scraper/:

- 16 001 Project Overview What We Will Build.mp4
- 16 001 scraper.txt
- 16 002 Project Setup & Installing BeautifulSoup & Requests Library.mp4
- 16 003 How To Use Requests & BeautifulSoup.mp4
- 16 004 Adding Code To Views.mp4
- 16 005 Saving Links To Database.mp4
- 16 006 Displaying Links In Bootstrap Table.mp4
- 16 007 Accepting URLs From Users.mp4
- 16 008 Link Scraper Source Code.html
- 16 008 Link Scraper Source Code.md

### 17 - Project 5 Building A Macro Nutrient & Calorie Tracker Using Django & ChartJS/:

- 17 001 Creating Food Model.mp4
- 17 002 Listing Out Food Items.mp4
- 17 003 Adding Select Box.mp4
- 17 004 Adding Consume Model.mp4
- 17 005 User Functionality To Add Food.mp4
- 17 006 Listing Consumed Food Items.mp4
- 17 007 Adding Bootstrap.mp4
- 17 008 Designing Table For Consumed Food.mp4
- 17 009 Calculating Total Macros.mp4
- 17 010 Adding The Calorie Progress Bar.mp4
- 17 011 Adding Navbar.mp4
- 17 012 Integrating Chart JS.mp4
- 17 013 Designing Delete Functionality.mp4
- 17 014 Fixing Alignment Issue.mp4
- 17 015 Source Code.html
- 17 015 Source Code.md

### 18 - Project 6 Building A Social Media App/:

- 18 001 Setting Up The Project.mp4
- 18 002 Creating Login Form.mp4
- 18 003 Rendering Login Form.mp4
- 18 004 Handling POST Request For Login.mp4
- 18 005 Creating Super User.mp4
- 18 006 Logout Functionality.mp4
- 18 007 Adding Base Template.mp4
- 18 008 Protected Route.mp4
- 18 009 Password Change View.mp4
- 18 010 Password Reset View.mp4
- 18 011 Password Reset Done View.mp4
- 18 012 Password Reset Confirm View.mp4
- 18 013 Password Reset Complete View.mp4
- 18 014 User Registration Form.mp4
- 18 015 Registering Users.mp4
- 18 016 Extending User Model.mp4
- 18 017 Edit Profile.mp4
- 18 018 Post Model.mp4
- 18 019 Making Migrations.mp4
- 18 020 Creating Post.mp4
- 18 021 Submitting Post Part 1.mp4
- 18 022 Submitting Post Part 2.mp4
- 18 023 Creating Index Page.mp4
- 18 024 Why use Tailwind.mp4
- 18 025 Setting Up Tailwind For Django Project.mp4
- 18 026 Styling Navbar Part 1.mp4
- 18 027 Styling Navbar Part 2.mp4
- 18 028 Styling Login Form.mp4
- 18 029 Designing Register Form.mp4
- 18 030 Styling Index Page Part 1.mp4
- 18 031 Styling Index Page Part 2.mp4
- 18 032 Adding Social Icons.mp4
- 18 033 Styling Edit Form.mp4
- 18 034 Styling Password Change Form.mp4
- 18 035 Removing Borders.mp4
- 18 036 Styling Create Page.mp4
- 18 037 Creating Feed Page.mp4
- 18 038 Like Functionality Part 1.mp4
- 18 039 Like Functionality Part 2.mp4
- 18 040 Like Functionality Part 3.mp4
- 18 041 Like Functionality Part 4.mp4
- 18 042 Like Functionality Part 5.mp4
- 18 043 Comment Functionality Part 1.mp4
- 18 044 Comment Functionality Part 2.mp4
- 18 045 Comment Functionality Part 3.mp4
- 18 046 Comment Functionality Part 4.mp4
- 18 047 Comment Functionality Part 5.mp4
- 18 048 Comment Functionality Part 6.mp4

### 19 - Project 7 Build An Advanced Expense Tracker/:

- 19 001 Setting Up The Project.mp4
- 19 002 Creating Expense Model.mp4
- 19 003 Creating Admin User.mp4
- 19 004 Index View.mp4
- 19 005 Setting Up Tailwind.mp4
- 19 006 Creating A Base Template.mp4
- 19 007 Creating Expense Form.mp4
- 19 008 Styling Expense Form.mp4
- 19 009 Handling Post Request.mp4
- 19 010 Getting Expenses.mp4
- 19 011 Designing Expense Table.mp4
- 19 012 Edit Part 1.mp4
- 19 013 Edit Part 2.mp4
- 19 014 Edit Part 3.mp4
- 19 015 Delete Functionality.mp4
- 19 016 Adding Image Buttons.mp4
- 19 017 Styling The App.mp4
- 19 018 Expense Sum.mp4
- 19 019 Humanize.mp4
- 19 020 Total Across Week & Year.mp4
- 19 021 Displaying Sums.mp4
- 19 022 Calculating Sum Of Expenses For 30 Days.mp4
- 19 023 Categorical Expenses.mp4
- 19 024 Setting Up Expense Charts.mp4
- 19 025 Getting Categorical Values.mp4
- 19 026 Adding Expenses To Charts.mp4
- 19 027 Adding Categories To Charts.mp4
- 19 028 expensetracker.zip
- 19 028 Source Code.html
- 19 028 Source Code.md

### 20 - Conclusion/:

- 20 001 Course Conclusion.mp4
