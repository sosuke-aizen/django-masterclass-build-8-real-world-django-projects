1
00:00:00,060 --> 00:00:05,220
So in this lecture, let's go ahead and learn how to create our very own action, feel right here.

2
00:00:05,430 --> 00:00:08,790
So if you have a look over here right now, we only have the lead field.

3
00:00:09,150 --> 00:00:16,180
And let's see, we want to create a action which basically changes the category of products to default.

4
00:00:16,860 --> 00:00:18,870
So let's go ahead and learn how to do that.

5
00:00:19,200 --> 00:00:21,260
So let's go back to our code right here.

6
00:00:21,720 --> 00:00:27,060
And in order to do the same, we first need to go ahead and define our very own action.

7
00:00:27,510 --> 00:00:33,330
So let's say I want to define an action, so I'll go ahead and simply define that action up over here

8
00:00:33,330 --> 00:00:35,240
in this specific class itself.

9
00:00:35,730 --> 00:00:44,250
So I'll say something like this and let's say I want to name this action as a change category.

10
00:00:45,660 --> 00:00:53,190
To default, so this is going to be our method and this is going to accept a few things.

11
00:00:53,580 --> 00:00:56,960
First of all, it's going to get reference to the object itself.

12
00:00:57,210 --> 00:01:02,520
And the reason why we passing the reference to the object itself is so that this method understands

13
00:01:02,550 --> 00:01:08,520
which specific object or which specific item on the list we need to go ahead and perform this action

14
00:01:08,520 --> 00:01:08,750
on.

15
00:01:08,760 --> 00:01:14,490
And that's why we passing that product itself over here using the self parameter, then it's going to

16
00:01:14,490 --> 00:01:19,350
need to have the request and then it's going to need to have the query set.

17
00:01:19,350 --> 00:01:23,520
And the query set is nothing, but it's actually the list of all these items in here.

18
00:01:23,550 --> 00:01:28,860
So now once we have that, what we want to do is then as we actually want to change the categories of

19
00:01:28,860 --> 00:01:33,420
all these items, that means we want to update all these items in here.

20
00:01:33,540 --> 00:01:39,210
So in order to update these items, as I also mentioned, these items are actually present in the query

21
00:01:39,210 --> 00:01:39,540
set.

22
00:01:39,900 --> 00:01:43,110
So I simply need to type in query said, not update.

23
00:01:44,040 --> 00:01:48,200
And now we need to mention what specific things do we need to update.

24
00:01:48,360 --> 00:01:53,680
So we need to update the category, which is nothing but a fill in our model.

25
00:01:53,700 --> 00:01:57,540
So if you go to the model, we are talking about the category FELIN here.

26
00:01:58,080 --> 00:02:00,670
So we'll simply use that category in here.

27
00:02:00,870 --> 00:02:06,270
So we want to set the category of the product to, let's see, default.

28
00:02:07,090 --> 00:02:09,430
So will simply type that thing in here.

29
00:02:10,470 --> 00:02:18,030
Now, as we have this method over here, you need to make sure to mention this method over here, inside

30
00:02:18,030 --> 00:02:18,800
actions.

31
00:02:19,350 --> 00:02:23,250
So in order to modify the action list, you need to type in actions.

32
00:02:23,820 --> 00:02:29,450
And the actions is going to include the methods which you want to be present in the action list.

33
00:02:29,820 --> 00:02:36,960
So we want to add this specific method in there so it'll simply go ahead and paste it up over here and

34
00:02:36,960 --> 00:02:39,360
always make sure to add a comma at the end.

35
00:02:39,690 --> 00:02:46,980
So now if I go ahead, hit refresh and now if I go here, as you can see, we have the change Category

36
00:02:46,980 --> 00:02:48,760
two default option right here.

37
00:02:48,990 --> 00:02:54,600
So whenever I want to change the category of certain products, I can simply select those products.

38
00:02:54,600 --> 00:02:58,510
I can go here and I can click on change category to default.

39
00:02:59,010 --> 00:03:06,420
So when I click that and when I click on Google, as you can see, the category of these two items has

40
00:03:06,420 --> 00:03:12,060
now been changed to default, which means we have successfully added that field over here.

41
00:03:12,120 --> 00:03:15,030
So now let's see if you don't like this name.

42
00:03:15,030 --> 00:03:20,040
And for some reason, you want to change this name right here so you can do that as well.

43
00:03:20,310 --> 00:03:27,930
So in order to change that particular name, you can simply go ahead and type in a line of code which

44
00:03:27,930 --> 00:03:35,550
says that you want to change its description so you can see change category two, default not short

45
00:03:35,550 --> 00:03:38,280
and the school description.

46
00:03:38,470 --> 00:03:40,350
And that should actually be in the school.

47
00:03:40,830 --> 00:03:48,010
And you can send the short description to, let's say, something like the default category.

48
00:03:48,300 --> 00:03:54,840
So once you go ahead, do that and hit refresh, as you can see now, it sees in default category instead

49
00:03:54,840 --> 00:03:57,120
of having this long method name.

50
00:03:58,360 --> 00:04:04,780
So that's how you can go ahead and you can actually customize the Shango actions over here and you can

51
00:04:04,780 --> 00:04:09,450
actually create any action of your own choice which is suitable for your web.

52
00:04:09,580 --> 00:04:11,170
So that's it for this lecture.

53
00:04:11,560 --> 00:04:13,570
And I'll see you guys in the next one.

54
00:04:13,930 --> 00:04:14,530
Thank you.

