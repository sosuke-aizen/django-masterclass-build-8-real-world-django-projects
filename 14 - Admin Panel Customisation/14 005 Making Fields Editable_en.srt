1
00:00:00,060 --> 00:00:06,300
So in this lecture, we are going to learn a few more admen customisation tricks, so the very first

2
00:00:06,300 --> 00:00:13,440
thing which we will learn in this lecture is how exactly can we go ahead and make the admin only display

3
00:00:13,440 --> 00:00:15,050
a certain fields and farms.

4
00:00:15,240 --> 00:00:21,030
So if you actually go ahead and if you go to the products over here, as you can see, you have a bunch

5
00:00:21,030 --> 00:00:21,890
of fields in here.

6
00:00:22,260 --> 00:00:27,630
And let's say for some reason you don't want to display all the fields, but instead you only want to

7
00:00:27,630 --> 00:00:30,060
display a certain number of fields in here.

8
00:00:30,660 --> 00:00:32,900
So how exactly can you go ahead and do that?

9
00:00:33,180 --> 00:00:34,950
So doing that is quite simple.

10
00:00:35,070 --> 00:00:41,220
You just need to go to the admin and in here you need to type in fields and you just need to include

11
00:00:41,220 --> 00:00:43,080
those fields which you want to display.

12
00:00:43,200 --> 00:00:50,670
So let's say if you want to only display the title and the price, you can include those fields in here.

13
00:00:52,090 --> 00:00:59,140
And now if we actually go here and hit refresh, as you can see, you only have been displayed the title

14
00:00:59,140 --> 00:01:02,960
and price and you are not shown anything else in this particular form.

15
00:01:02,980 --> 00:01:06,760
So this is one way of hiding certain fields in this specific form.

16
00:01:07,060 --> 00:01:13,720
So now once we go ahead and learn that, let's now learn how to actually make the admin be able to edit

17
00:01:13,720 --> 00:01:16,750
these fields away here right inside the last page.

18
00:01:17,200 --> 00:01:21,870
So right now, as you can see, we can only view these items in here.

19
00:01:21,880 --> 00:01:27,250
And if you were to edit these items, you need to go in there and you need to manually edit them over

20
00:01:27,250 --> 00:01:27,570
here.

21
00:01:27,700 --> 00:01:33,750
But let's say for making it simple, you want the admin to be able to edit those items in here.

22
00:01:34,270 --> 00:01:41,620
So in order to make these list items editable, you simply need to go ahead and type in list and let's

23
00:01:41,620 --> 00:01:43,960
go editable.

24
00:01:44,230 --> 00:01:48,080
And you need to equate this to the fields which you want to make editable.

25
00:01:48,490 --> 00:01:54,370
So let's say I want the admin to be able to edit the price of the products so I can simply go ahead

26
00:01:54,370 --> 00:01:58,450
and type in the name of the field, which I want to make editable.

27
00:01:58,660 --> 00:02:00,360
So I'll type in price in here.

28
00:02:01,270 --> 00:02:03,400
And now if I go back, hit refresh.

29
00:02:03,400 --> 00:02:06,910
As you can see now, this actually becomes a editable field.

30
00:02:07,450 --> 00:02:15,580
Now let's say I also want to make the category to be editable so I can simply go ahead and add in category

31
00:02:15,580 --> 00:02:16,560
over here as well.

32
00:02:16,900 --> 00:02:19,150
You have a comma and we should be good to go.

33
00:02:19,300 --> 00:02:25,930
So now if I go ahead and hit refresh, as you can see now, the admin can also edit the category from

34
00:02:25,930 --> 00:02:26,160
here.

35
00:02:26,530 --> 00:02:28,180
So that's it for this lecture.

36
00:02:28,180 --> 00:02:32,230
And that's all about customizing the admin panel in general.

37
00:02:32,260 --> 00:02:36,160
So thank you very much for watching and I'll see you guys in the next section.

38
00:02:36,190 --> 00:02:36,690
Thank you.

