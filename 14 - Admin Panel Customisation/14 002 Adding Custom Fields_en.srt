1
00:00:00,060 --> 00:00:05,070
So right now, as you can see, we have these product objects displayed over here and now let's see

2
00:00:05,070 --> 00:00:10,860
if you want to change that and if you want to display the actual names so you can do that easily by

3
00:00:10,860 --> 00:00:12,030
just going into models.

4
00:00:12,030 --> 00:00:14,600
And you can have these string representation over here.

5
00:00:15,090 --> 00:00:25,570
So you can see def double underscore Asgeir self and you can simply return self, not title over here.

6
00:00:25,620 --> 00:00:30,210
And once you do that, you will get the name of these specific products in here.

7
00:00:30,840 --> 00:00:34,320
So as you can see now we have the name of those specific products.

8
00:00:34,330 --> 00:00:35,580
That's all well and good.

9
00:00:35,850 --> 00:00:41,550
But what if we actually also want to display the other feels like price discount price category and

10
00:00:41,550 --> 00:00:42,840
everything like that over here?

11
00:00:42,930 --> 00:00:49,050
So in order to display those things, first of all, you again need to go to the admin dot a file of

12
00:00:49,110 --> 00:00:53,100
that specific product, and in here you need to create a class.

13
00:00:53,640 --> 00:00:56,000
So in here, let's create a class.

14
00:00:56,010 --> 00:01:01,050
And in that class, you actually need to go ahead and list out all the fields which you want to display

15
00:01:01,440 --> 00:01:05,720
so you can type in class and then you need to type in the class name.

16
00:01:05,730 --> 00:01:11,520
So let's say we type in the class name as the model name and admin soil type and product.

17
00:01:13,210 --> 00:01:19,600
Admin as the class name, and I'll inherit from admin dot model admin.

18
00:01:20,020 --> 00:01:25,390
Now you can actually name this class as anything, but you always need to make sure that you make this

19
00:01:25,390 --> 00:01:28,460
class inherit the model admin class from admin.

20
00:01:28,870 --> 00:01:34,210
So once you have done that, you only need to type in a list and the school display.

21
00:01:34,600 --> 00:01:39,190
And in here you just mentioned those fields which you actually want to display up over there.

22
00:01:39,610 --> 00:01:42,520
So let's say you want to display the title, then let's see.

23
00:01:42,520 --> 00:01:49,780
The next fill in your model is the price, the discount price and the category so you can display them

24
00:01:49,780 --> 00:01:50,940
up over here as well.

25
00:01:51,550 --> 00:01:58,450
So for better understanding, let's also open the model stockpile file and let's copy each one of these

26
00:01:58,450 --> 00:01:59,280
fields over here.

27
00:02:00,460 --> 00:02:03,670
So let's also copy the discount prices fell.

28
00:02:05,950 --> 00:02:09,520
And let's also display other fields as well, like the.

29
00:02:11,120 --> 00:02:12,980
Category, description and image.

30
00:02:14,960 --> 00:02:18,400
So simply copy that pasted up over here.

31
00:02:27,590 --> 00:02:33,410
So now, once you go ahead and do that, the final thing which you need to do is that as you have registered

32
00:02:33,410 --> 00:02:37,820
the models over here, you also need to go ahead and register this class as well.

33
00:02:38,360 --> 00:02:44,960
So along with products, you also can register the product admin, which is the class which we have

34
00:02:44,960 --> 00:02:45,680
just created.

35
00:02:46,400 --> 00:02:54,020
So now if we go back and if we have a look over here, as you can see now, not only do we have just

36
00:02:54,020 --> 00:02:59,450
the title over here, but we also have the price, the discount price, the category.

37
00:02:59,450 --> 00:03:03,230
And I guess we have mentioned category over here twice.

38
00:03:04,490 --> 00:03:09,650
And instead of category, we actually want to have description over here.

39
00:03:10,740 --> 00:03:12,690
So let's get the description.

40
00:03:15,190 --> 00:03:21,000
And now if we go ahead and hit refresh, as you can see now, we have the description over here as well.

41
00:03:21,640 --> 00:03:26,080
So that's how you can go ahead and actually display all the fields over here.

42
00:03:26,080 --> 00:03:29,420
And let's say for some reason, you don't want to display a specific field.

43
00:03:29,440 --> 00:03:32,290
So let's say I don't want to display this image filled in here.

44
00:03:32,740 --> 00:03:34,810
I can simply go ahead, get rid of that.

45
00:03:35,200 --> 00:03:37,030
And we should be good to go.

46
00:03:37,030 --> 00:03:39,360
And everything should actually work fine.

47
00:03:40,590 --> 00:03:46,250
So now if I go back, hit refresh, as you can see, now we have the image people removed from that.

48
00:03:47,100 --> 00:03:52,600
So that's how you can go ahead and customize the way the objects in your admin database look like.

49
00:03:53,400 --> 00:03:54,980
So that's it for this lecture.

50
00:03:55,020 --> 00:03:59,050
And in the next lecture will go ahead and learn how to add a search field.

51
00:03:59,220 --> 00:04:04,530
So we'll go ahead and learn how to add textbooks in here, which allows you to search these products

52
00:04:04,530 --> 00:04:06,280
depending upon a specific field.

53
00:04:06,300 --> 00:04:10,830
So let's say, for example, if you want to search depending upon the title, you should be able to

54
00:04:10,830 --> 00:04:12,450
search depending upon the title.

55
00:04:12,780 --> 00:04:16,709
If you want to search, depending on the category, you can do that as well.

56
00:04:16,740 --> 00:04:20,579
So thank you very much for watching and I'll see you guys next time.

57
00:04:20,850 --> 00:04:21,390
Thank you.

