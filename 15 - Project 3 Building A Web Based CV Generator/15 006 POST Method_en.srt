1
00:00:00,510 --> 00:00:07,050
So now, in order to handle the form submission, we now need to go ahead and go into the views and

2
00:00:07,050 --> 00:00:11,410
in here we need to check if we get a post request.

3
00:00:11,430 --> 00:00:17,610
So right now, if we have a look at our form or whenever we submit this form, we should actually get

4
00:00:17,610 --> 00:00:18,690
a post request.

5
00:00:18,960 --> 00:00:24,930
And for that specific reason, you always need to go ahead and actually make a method type over here

6
00:00:24,930 --> 00:00:25,820
inside your form.

7
00:00:26,310 --> 00:00:32,670
So you need to mention the method as post so that whenever you actually hit the submit button on your

8
00:00:32,670 --> 00:00:35,100
form, we actually get a post request.

9
00:00:35,880 --> 00:00:42,000
So now once you go ahead and do that now, you can handle the post request up over here inside your

10
00:00:42,000 --> 00:00:43,450
views dot by file.

11
00:00:44,040 --> 00:00:50,940
So in order to accept or basically handle the post request, first of all, you need to make sure that

12
00:00:50,940 --> 00:00:57,300
if the request is post, so whenever you actually just request this webpage, which is the home page,

13
00:00:57,390 --> 00:01:02,300
that's not actually a post request, but the post request is when you actually submit this form.

14
00:01:02,850 --> 00:01:09,360
So whenever that happens, you actually want to get all the details from these respective fields and

15
00:01:09,360 --> 00:01:11,830
you need to submit them to the database.

16
00:01:11,850 --> 00:01:13,650
So doing that is quite simple.

17
00:01:13,950 --> 00:01:18,590
You make use of this request over here and see if the request is a post request.

18
00:01:18,720 --> 00:01:24,470
So you see something like if a request dot method.

19
00:01:25,200 --> 00:01:27,360
So post is actually a method.

20
00:01:27,540 --> 00:01:30,660
So we'll see if that method is equal to post.

21
00:01:31,050 --> 00:01:37,230
Then we want to perform a set of actions and that set of actions is nothing but to get all the details

22
00:01:37,230 --> 00:01:42,600
which are actually submitted to this form right here in the respective input fields and the text area.

23
00:01:43,230 --> 00:01:49,410
So now in order to get those fields, you simply type in the file name, which is named in this case

24
00:01:50,280 --> 00:01:51,360
for the first field.

25
00:01:51,510 --> 00:01:53,790
And I'll type and request DOT.

26
00:01:54,330 --> 00:02:00,120
Then you mentioned the method, which is post and then to get the data of that respective field you

27
00:02:00,120 --> 00:02:07,150
type and get and then you again specify the filename over here and have the by default value over here.

28
00:02:07,500 --> 00:02:13,350
So for now we are going to leave that thing as empty and you need to do the same thing for all the fields

29
00:02:13,350 --> 00:02:13,870
over here.

30
00:02:14,220 --> 00:02:19,130
So let me quickly go ahead and do the same thing for almost all fields, which we have.

31
00:02:19,140 --> 00:02:21,030
So we have nine fields over here.

32
00:02:21,030 --> 00:02:25,370
So I'll simply go ahead and copy and paste this thing for eight more times over here.

33
00:02:25,620 --> 00:02:29,080
And now let's go ahead and modify each one of them one by one.

34
00:02:29,160 --> 00:02:30,830
So this is going to be email.

35
00:02:31,890 --> 00:02:33,930
Let me change this to email as well.

36
00:02:34,980 --> 00:02:36,370
This is phone.

37
00:02:37,650 --> 00:02:43,240
This is going to be for the next one is summary.

38
00:02:43,830 --> 00:02:46,410
This one is going to be Summerlee.

39
00:02:48,530 --> 00:02:49,880
Then we have degree.

40
00:02:52,970 --> 00:02:55,280
Then we have school.

41
00:02:58,080 --> 00:03:00,210
Then we have the university.

42
00:03:07,160 --> 00:03:08,960
Then we have the previous rule.

43
00:03:11,790 --> 00:03:20,070
And let's change this thing to previous and the school rule as well, and the final one is skills.

44
00:03:22,640 --> 00:03:30,110
And now once we have these particular details, we go ahead and create a object or hear of the model,

45
00:03:30,110 --> 00:03:31,030
which is profile.

46
00:03:31,460 --> 00:03:37,490
So in order to create an object of the profile model, we simply need to go ahead and type in profile

47
00:03:37,490 --> 00:03:41,830
equals and then type in profile, which is actually the model name.

48
00:03:42,590 --> 00:03:45,730
And in here you simply need to submit all the details.

49
00:03:45,950 --> 00:03:51,400
So I'll type in name equals name, email equals email.

50
00:03:51,950 --> 00:03:55,070
Then we have phone equals.

51
00:03:56,930 --> 00:04:09,650
Phone, then somebody equals somebody, then we have degree equals degree, then we have school equals

52
00:04:10,220 --> 00:04:19,190
school, then we have university equals university, then we have previous rule equals previous rule.

53
00:04:19,760 --> 00:04:25,150
And let's see if some field is not left out and the final field is actually skills.

54
00:04:25,150 --> 00:04:27,100
So let's also pass that as well.

55
00:04:27,500 --> 00:04:30,710
So I'll see skills equals skills.

56
00:04:31,220 --> 00:04:36,770
So this means that you have simply created an object over here of the class or the model profile.

57
00:04:37,160 --> 00:04:42,830
So now once you have to save this data into the database, you simply go ahead and save this specific

58
00:04:42,830 --> 00:04:45,840
model so you can type in profile, not save.

59
00:04:45,860 --> 00:04:51,770
And that's actually simply going to go ahead and save the data into the back end into your database.

60
00:04:52,880 --> 00:04:58,310
And now once we go ahead and have this specific thing, we can now go ahead and try posting the data

61
00:04:58,310 --> 00:05:00,920
and see if this thing is actually posted.

62
00:05:00,950 --> 00:05:02,660
So let's go ahead, see if the code.

63
00:05:03,860 --> 00:05:05,660
And I guess the subways not running.

64
00:05:06,900 --> 00:05:13,770
And yeah, we do actually have a small area over here, so this should actually be a comparison operator,

65
00:05:13,770 --> 00:05:17,200
which is equal to and not a assignment operator.

66
00:05:17,370 --> 00:05:20,530
So once we go ahead fix that, we are pretty much good to go.

67
00:05:20,940 --> 00:05:22,230
So let's hit refresh.

68
00:05:22,890 --> 00:05:24,600
Let's type in my name here.

69
00:05:28,160 --> 00:05:38,900
Let's type in some dummy e-mail, so dummy and dummy dot com, let's type in some random phone number.

70
00:05:41,860 --> 00:05:45,500
Let's type in some random about you over here.

71
00:05:45,640 --> 00:05:51,220
So if you actually don't want to type in the text manually, you can simply go ahead and search for

72
00:05:51,220 --> 00:05:52,930
lorem ipsum.

73
00:05:52,930 --> 00:05:58,690
And it's actually going to give you a bunch of text, random text, which you can use to fill up your

74
00:05:58,690 --> 00:05:59,230
forms.

75
00:05:59,560 --> 00:06:09,280
So I can simply go ahead and I can just copy some of the text, random text pasted in here.

76
00:06:10,420 --> 00:06:19,570
And let's see, the degree is engineering school is Amitay.

77
00:06:21,510 --> 00:06:30,870
I'll type in some random university like X, Y, Z University, I'll type in some previous work experience

78
00:06:30,870 --> 00:06:35,210
as a bunch of random text, and I'll also add the same text over here.

79
00:06:35,220 --> 00:06:41,790
And when I click on Submit, as you can see, we actually get an error over here because we have not

80
00:06:41,790 --> 00:06:45,770
yet included the Sears RF token in our form.

81
00:06:45,960 --> 00:06:52,980
So always remember that whenever you are making or creating a form, you always need to include X RF

82
00:06:52,980 --> 00:06:57,550
token inside your form to avoid the cross site request forgery.

83
00:06:58,080 --> 00:06:59,890
So let's go ahead and fix that.

84
00:07:00,060 --> 00:07:05,790
So in here you just need to go inside your form, which is right here, and you simply need to type

85
00:07:05,790 --> 00:07:15,760
in the CSIRO to see as out of underscore Tolkan is what you need once you go ahead, hit refresh.

86
00:07:16,440 --> 00:07:19,470
Now let's submit the details.

87
00:07:21,550 --> 00:07:22,390
One more time.

88
00:07:23,600 --> 00:07:30,650
And now when I click on Submit, we again have an error and it says that profile got an unexpected Keywood

89
00:07:30,650 --> 00:07:31,820
argument previously.

90
00:07:32,000 --> 00:07:34,130
So let's go ahead and see that as well.

91
00:07:34,310 --> 00:07:36,230
What actually has happened?

92
00:07:36,770 --> 00:07:42,150
So I'm purposely not editing out these errors so that you know how exactly you can debug your code.

93
00:07:42,860 --> 00:07:49,160
So now if you go to the models we have and I guess we might have misspelled this thing somewhere, which

94
00:07:49,160 --> 00:07:50,700
is why we are getting an error.

95
00:07:50,900 --> 00:07:52,700
So this is serious work.

96
00:07:53,040 --> 00:07:53,870
Looks fine.

97
00:07:54,290 --> 00:08:01,660
OK, so I've actually typed in previous rule over here, which should actually be previous work.

98
00:08:01,670 --> 00:08:04,660
And let's see, what do we actually have in the database?

99
00:08:06,210 --> 00:08:11,910
So in the database that is in the models, we have listed this thing as previous work in the form as

100
00:08:11,910 --> 00:08:12,150
well.

101
00:08:12,150 --> 00:08:17,670
I have typed this thing as previous work, but right over here I said something like previous rule.

102
00:08:17,690 --> 00:08:20,660
So let me just change this thing to previous work as well.

103
00:08:23,450 --> 00:08:28,720
And once you make change over here, you also go ahead and make the change over here as well.

104
00:08:38,150 --> 00:08:41,559
So once you fix that, everything should be working fine.

105
00:08:43,350 --> 00:08:47,600
So now let me just go back and refresh and type in this detail one more time.

106
00:08:50,600 --> 00:08:57,380
So now when I click submit, as you can see, the form is submitted now in order to see if the form

107
00:08:57,380 --> 00:09:01,820
is actually submitted at the back end, I can simply go ahead and type in slash admen.

108
00:09:02,210 --> 00:09:03,740
I can go to profiles.

109
00:09:04,280 --> 00:09:11,390
And as you can see, if I click on Profile Object, we now have all of these details captured at the

110
00:09:11,390 --> 00:09:11,960
back end.

111
00:09:12,230 --> 00:09:17,810
That means we have done with the first part of building our app, and that is to actually collect all

112
00:09:17,810 --> 00:09:19,280
gather data from the user.

113
00:09:19,790 --> 00:09:25,700
Now, once we have collected this particular data in the upcoming lecture, we can now go ahead and

114
00:09:25,700 --> 00:09:31,040
simply learn how to create or render a PDF out of from the model.

115
00:09:31,700 --> 00:09:35,630
So thank you very much for watching and I'll see you guys next time.

116
00:09:35,990 --> 00:09:36,560
Thank you.

