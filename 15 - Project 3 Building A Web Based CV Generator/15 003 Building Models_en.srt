1
00:00:00,060 --> 00:00:06,120
So when I see Regenerator, you actually need to go ahead and accept a few details from the user, like

2
00:00:06,120 --> 00:00:14,520
his name, his e-mail, his phone number, his school, his university, his previous jobs, and all

3
00:00:14,520 --> 00:00:18,190
the details which you actually want for a proper CV.

4
00:00:18,720 --> 00:00:24,780
Now, in order to get those details and in order to store those details, you actually need to have

5
00:00:24,780 --> 00:00:26,460
a table in the database.

6
00:00:26,460 --> 00:00:33,330
And in order to create a table in the database, we first need to go ahead and create a model which

7
00:00:33,330 --> 00:00:35,790
is nothing but a blueprint for our database.

8
00:00:36,180 --> 00:00:38,300
So let's go ahead and create a model now.

9
00:00:38,390 --> 00:00:40,790
So let's go to the model stockpile file.

10
00:00:41,070 --> 00:00:44,130
And in here we simply need to go ahead and set up a model.

11
00:00:44,340 --> 00:00:52,470
And that model should include all the fields which actually help us to collect the details of that specific

12
00:00:52,470 --> 00:00:52,950
user.

13
00:00:53,310 --> 00:00:56,330
So let's go ahead and create the model here.

14
00:00:56,340 --> 00:01:03,550
So I'll type in class and in the CV we are actually going to place a person's profile.

15
00:01:03,840 --> 00:01:06,630
So let's name this class as profile.

16
00:01:06,780 --> 00:01:12,690
You can name it as anything, but I have named it as profile and then it's going to inherit from Model

17
00:01:12,690 --> 00:01:13,430
Start model.

18
00:01:14,160 --> 00:01:18,320
And now the very first thing which we do is that we create all the fields.

19
00:01:18,660 --> 00:01:24,900
So obvious thing which we need to accept is we need to accept the candidate's name and it's going to

20
00:01:24,900 --> 00:01:26,190
be model start.

21
00:01:26,550 --> 00:01:32,720
Garfield and the Max lenth is going to be, let's see, 200.

22
00:01:33,270 --> 00:01:36,620
So we now need to go ahead and create a lot of fields over here.

23
00:01:36,630 --> 00:01:38,150
So let's go ahead and do that.

24
00:01:38,820 --> 00:01:44,130
So let's go ahead and create a seam field for email and for phone number.

25
00:01:44,220 --> 00:01:49,590
So let's replace this with email and let's replace this with phone number.

26
00:01:49,710 --> 00:01:51,330
So I'll type and phone over here.

27
00:01:52,000 --> 00:01:58,400
And now let's go ahead and create a field or which kind of gives us the information about the candidate.

28
00:01:58,410 --> 00:02:05,460
So let's say a field which resembles something like this about a section where the candidate can actually

29
00:02:05,580 --> 00:02:07,610
write a few lines about himself.

30
00:02:08,160 --> 00:02:17,190
So let's go ahead and name this field as somebody and let's say this is going to be a text field, not

31
00:02:17,190 --> 00:02:20,520
the character field, because it's going to accept a lot of details.

32
00:02:21,100 --> 00:02:26,030
So this is going to be text field and let's add the max length to two thousand.

33
00:02:26,400 --> 00:02:29,910
So in here, the candidate can actually write as much as he wants.

34
00:02:30,450 --> 00:02:34,710
And now let's go ahead and also create a feel for degree.

35
00:02:37,020 --> 00:02:45,060
Which is also going to have the masland asked two hundred now let's create a feel for school, let's

36
00:02:45,060 --> 00:02:47,990
create the same one for university as well.

37
00:02:50,420 --> 00:02:55,650
And now let's go ahead and create one more TextField for the previous rules.

38
00:02:55,670 --> 00:02:59,330
That is the places where the candidate has worked previously.

39
00:02:59,750 --> 00:03:05,630
So I'll type in previous and the school work and this is going to be a TextField.

40
00:03:05,660 --> 00:03:07,910
So I'll type in text field over here.

41
00:03:08,000 --> 00:03:13,880
And let's go ahead and add another TextField down here, which is going to say something like skills

42
00:03:13,880 --> 00:03:16,430
where the candidate can actually list out her skills.

43
00:03:17,120 --> 00:03:19,520
So I'll say something like skills.

44
00:03:19,880 --> 00:03:26,180
And let's see the max length of this is going to be a thousand and let's make the max length of this

45
00:03:26,180 --> 00:03:27,350
to be thousand as well.

46
00:03:27,680 --> 00:03:31,610
OK, so now once we have this model, we are pretty much good to go.

47
00:03:31,940 --> 00:03:37,670
So now in order to make a database table out of this model, we need to go ahead and make migration's.

48
00:03:38,300 --> 00:03:47,330
So let's go ahead and type in Python three managed by MK Migration's.

49
00:03:50,620 --> 00:04:01,750
Manage not by my greed and everything is fine now we can just go ahead and we can check if this table

50
00:04:01,750 --> 00:04:03,490
is actually created in the back end.

51
00:04:03,970 --> 00:04:11,560
So let's go ahead and create a super user as well in order to make sure that we do have the table created

52
00:04:11,560 --> 00:04:12,310
at the back end.

53
00:04:12,520 --> 00:04:21,670
So in order to create the super user, you simply type in Python three managed to be create a super

54
00:04:21,730 --> 00:04:27,000
user and now I'll have the user name as my name itself.

55
00:04:27,610 --> 00:04:30,280
I'll add a dummy email address here.

56
00:04:35,650 --> 00:04:41,690
I will set the password to super user and now as you can see, the super user has been created.

57
00:04:42,130 --> 00:04:48,730
So now let's go ahead and run the server, log in to the super user and see if the table is actually

58
00:04:48,730 --> 00:04:49,960
created at the back end.

59
00:04:50,200 --> 00:04:54,990
So I'll type in Python three managed by a run.

60
00:04:55,030 --> 00:04:59,670
So now let's actually open up the Web browser.

61
00:05:00,010 --> 00:05:06,970
And now in here, let me just go ahead and go to one twenty seven point zero point zero point one,

62
00:05:06,970 --> 00:05:10,220
call eight thousand slash admen.

63
00:05:10,240 --> 00:05:12,470
And this should actually be one twenty seven.

64
00:05:13,290 --> 00:05:15,100
OK, so now let me just go ahead.

65
00:05:15,100 --> 00:05:15,700
Log in.

66
00:05:18,900 --> 00:05:24,570
And now, as you can see, there's no model here because we are yet to register that model in the admin,

67
00:05:24,570 --> 00:05:25,510
not by fire.

68
00:05:26,100 --> 00:05:28,530
So let's go to admin, not by file.

69
00:05:31,270 --> 00:05:38,920
So which is right here and here, import the model, so I'll type in from the models import and the

70
00:05:38,920 --> 00:05:47,680
model is profile and now let's go ahead and do admin aside, dot register and register the model, which

71
00:05:47,680 --> 00:05:49,000
is nothing but profile.

72
00:05:50,260 --> 00:05:55,640
And once we go ahead and do that, once we hit refresh, as you can see, we have profile over here.

73
00:05:56,620 --> 00:06:00,040
So now if I click over here, as you can see, we have no profile.

74
00:06:00,040 --> 00:06:06,490
But when I click on this ADD option, as you can see, we have these fields which are going to actually

75
00:06:06,490 --> 00:06:12,310
allow us to enter all the details and save all the details of the candidate.

76
00:06:12,790 --> 00:06:19,240
Now, this is quite OK for an admin, but we actually want to go ahead and make an interface for the

77
00:06:19,240 --> 00:06:26,230
end user so that he can actually submit this data via a form and that data gets submitted to the back

78
00:06:26,230 --> 00:06:26,480
end.

79
00:06:27,040 --> 00:06:33,130
So now we need to actually go ahead and create a form to accept all the user data and store it over

80
00:06:33,130 --> 00:06:34,030
here in the backend.

81
00:06:34,270 --> 00:06:40,450
So in the next lecture, we will go ahead and create a simple form and we won't be actually using Django

82
00:06:40,450 --> 00:06:46,600
forms, but instead we will use a normal form and we will use the post method to actually save the form

83
00:06:46,600 --> 00:06:47,080
data.

84
00:06:47,470 --> 00:06:53,800
So in the next lecture, let's go ahead and create a form for the user to fill out this data right over

85
00:06:53,800 --> 00:06:54,070
here.

86
00:06:54,610 --> 00:06:58,480
So thank you very much for watching and I'll see you guys next time.

87
00:06:58,780 --> 00:06:59,320
Thank you.

