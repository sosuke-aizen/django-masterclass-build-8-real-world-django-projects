1
00:00:00,360 --> 00:00:06,300
So hello and welcome to this new section, and this is what exactly we will be building in this entire

2
00:00:06,300 --> 00:00:12,420
section, so we will build this series region, Rita, or as you mentioned, Rita, which actually allows

3
00:00:12,420 --> 00:00:14,400
you to enter your data.

4
00:00:14,400 --> 00:00:17,540
That is your educational and personal data.

5
00:00:17,940 --> 00:00:24,360
And it's actually going to go ahead and create a -- or it's actually going to go ahead and create

6
00:00:24,360 --> 00:00:26,890
a CV in a PDF format for you.

7
00:00:27,330 --> 00:00:31,500
So let's go ahead and see how exactly this works even before we start making it.

8
00:00:31,980 --> 00:00:35,100
So I'll go ahead and fill out my personal details in here.

9
00:00:35,910 --> 00:00:41,180
And these details won't be correct, but let's just go ahead and fill them for representational purposes.

10
00:00:41,190 --> 00:00:43,940
So let's say there's going to be a demo at demo dot com.

11
00:00:44,670 --> 00:00:48,210
Let me into some random phone number.

12
00:00:49,700 --> 00:00:53,260
And I've copied some paragraphs over here, which I can piece it up over here.

13
00:00:54,490 --> 00:01:01,980
Let's see, I type in a degree, let's see, I type in some random data in here as well.

14
00:01:08,330 --> 00:01:15,080
And now let's go ahead and hit submit, and as you can see, as soon as I had submit, the data actually

15
00:01:15,080 --> 00:01:16,620
got submitted to the back end.

16
00:01:17,300 --> 00:01:22,640
Now, in order to actually have a look at the data which is present in our database, I can simply go

17
00:01:22,640 --> 00:01:24,840
to list at enter.

18
00:01:24,860 --> 00:01:30,500
And as you can see, these are the bunch of profiles which have actually submitted data to the back

19
00:01:30,500 --> 00:01:30,710
end.

20
00:01:31,280 --> 00:01:37,430
Now, depending upon the person's name, you can actually go ahead and can download the quiz by clicking

21
00:01:37,430 --> 00:01:38,960
on the download button.

22
00:01:39,440 --> 00:01:45,290
So if you actually want to download the CV for the user, which we have just created, I can simply

23
00:01:45,290 --> 00:01:47,450
go ahead and click on Download KVI.

24
00:01:49,910 --> 00:01:54,420
And it's going to take a while, and as you can see, the download has started up over here.

25
00:01:54,770 --> 00:01:58,240
So when the download is finished, I can simply open this thing up.

26
00:01:58,610 --> 00:02:04,340
And as you can see, I now have my CV generated up over here in a PDA format.

27
00:02:04,610 --> 00:02:07,950
So I have my name, email address, phone number.

28
00:02:08,300 --> 00:02:13,460
Then we have the summary over here, which is nothing but something about you.

29
00:02:13,970 --> 00:02:19,890
We have skills, we have education and we have previous work experience over here listed up as well.

30
00:02:20,450 --> 00:02:25,280
So this is the kind of advanced app which we will be building in this specific section.

31
00:02:25,280 --> 00:02:32,210
That is we will go ahead and develop this question later, which can accept a candidate's data and dynamically

32
00:02:32,210 --> 00:02:36,320
generate a CV or a resume in a PDF format for him.

33
00:02:37,130 --> 00:02:41,690
So from the next lecture on what let's actually go ahead and start building this app.

