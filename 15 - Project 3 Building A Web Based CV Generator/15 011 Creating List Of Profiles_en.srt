1
00:00:00,090 --> 00:00:06,180
So let's not go ahead and make a few modifications to the look and feel of this specific template so

2
00:00:06,180 --> 00:00:07,980
we won't be doing much over here.

3
00:00:08,100 --> 00:00:11,760
Instead, what we will just do is that we will add this horizontal line.

4
00:00:11,760 --> 00:00:14,290
So we're here at the end of each section.

5
00:00:14,670 --> 00:00:16,730
So let's go ahead and do that quickly.

6
00:00:16,950 --> 00:00:19,130
So let's copy the H.R. tag.

7
00:00:19,620 --> 00:00:27,210
And just as we have this tag after the user profile details, I'll go ahead and add that up after the

8
00:00:27,210 --> 00:00:27,800
summary.

9
00:00:27,850 --> 00:00:32,549
I'll go ahead and add that up after listing out these skills at the education.

10
00:00:32,549 --> 00:00:35,440
We don't want to add that because this is our final section.

11
00:00:36,090 --> 00:00:41,700
So one more interesting thing which you can do here is that as the user has school, you can type in

12
00:00:41,700 --> 00:00:44,690
schooling and have the user school set up over here.

13
00:00:44,700 --> 00:00:51,210
And as now, the user also has a degree and he has obtained a degree from a specific university.

14
00:00:51,510 --> 00:00:54,660
You can go ahead and make use of this degree over here as well.

15
00:00:55,080 --> 00:00:58,740
So you can see something like let's see the.

16
00:01:00,440 --> 00:01:02,000
User profile law degree.

17
00:01:04,569 --> 00:01:05,680
And you can see.

18
00:01:07,040 --> 00:01:15,050
From this specific university and now I guess we also forgot to add rules in the previous lecture so

19
00:01:15,050 --> 00:01:20,330
you can go ahead and add that up as well, so you can go ahead and make use of this previous work feel

20
00:01:20,330 --> 00:01:23,230
to add up the previous rules of the candidate.

21
00:01:23,750 --> 00:01:28,360
So I'll go ahead and again, copied and pasted up over here.

22
00:01:28,550 --> 00:01:33,620
I'll simply go ahead, copy this particular tag pasted up over here and I'll see something like.

23
00:01:35,110 --> 00:01:39,820
Previous work and let's see, this is going to say something like.

24
00:01:41,180 --> 00:01:43,250
Previous under school work.

25
00:01:44,230 --> 00:01:48,210
So now once we have these things up, there should look a little bit different.

26
00:01:48,230 --> 00:01:53,600
So let's go ahead and try that thing for the user profile one.

27
00:01:54,720 --> 00:01:56,990
So it's going to, again, download the PDF.

28
00:01:56,990 --> 00:02:01,540
And when I open it up, as you can see now, this thing looks much better.

29
00:02:02,120 --> 00:02:04,810
So we have a nice looking CV right over here.

30
00:02:05,360 --> 00:02:09,330
So you can style this thing up in any fashion or in any manner which you want.

31
00:02:09,350 --> 00:02:15,380
And now let's go ahead and do the final thing of actually listing up the profiles on a specific page

32
00:02:15,530 --> 00:02:19,400
so that we can download the PDF of any particular student.

33
00:02:19,880 --> 00:02:21,710
So doing that is quite simple.

34
00:02:21,710 --> 00:02:23,550
You can do that on your own as well.

35
00:02:23,570 --> 00:02:28,970
You simply need to go ahead, create a view and get all the objects, render all of those objects on

36
00:02:28,970 --> 00:02:31,460
that template by using the context.

37
00:02:31,640 --> 00:02:33,440
So you can post this video.

38
00:02:33,450 --> 00:02:37,590
You can try that on your own as well, or you can just go ahead and follow along.

39
00:02:38,120 --> 00:02:47,300
So let's go ahead and type in Dev and let's give it some sort of simple name, like a list, and it's

40
00:02:47,300 --> 00:02:52,490
going to accept a request and then we'll get the profile.

41
00:02:52,670 --> 00:02:53,230
So I'll see.

42
00:02:53,240 --> 00:03:01,970
Profile equals profile dot objects dot all because we obviously want all of the candidates which are

43
00:03:01,970 --> 00:03:03,680
actually present in our database.

44
00:03:04,010 --> 00:03:12,460
And then I'll simply type and return, render and will render the template which we have not yet created.

45
00:03:12,470 --> 00:03:20,840
So let's create a template and PDF and let's name this template as Liz Dot H.M. and then we will finally

46
00:03:20,840 --> 00:03:22,460
pass in the context as.

47
00:03:24,080 --> 00:03:29,750
Profile as OK, so once we have this thing, let's go ahead and create the template, which is list

48
00:03:29,750 --> 00:03:30,730
dot HTML.

49
00:03:31,340 --> 00:03:33,390
So let's go to the templates directly.

50
00:03:33,410 --> 00:03:39,050
Let's go to PDF, create a new file and I'll see something like list and dot HTML.

51
00:03:40,110 --> 00:03:47,900
OK, so in here, let's actually have the tag and in here I'll go ahead and simply add up a bootstrap

52
00:03:47,910 --> 00:03:49,140
CDN link over here.

53
00:03:49,520 --> 00:03:54,360
So I already have that link copied with me, so I'll simply go ahead and add it up over here.

54
00:03:55,490 --> 00:03:59,050
So if you want, you can go ahead and add bootstrap to your site as well.

55
00:03:59,630 --> 00:04:02,650
So after adding bootstrap, let's create a container.

56
00:04:02,720 --> 00:04:05,710
So that's going to be div class container.

57
00:04:05,930 --> 00:04:08,510
And in there, let's go ahead and create a row and column.

58
00:04:08,990 --> 00:04:17,870
So I'll see div the class equals rule and we can also go ahead and set the margin for this rule using

59
00:04:17,870 --> 00:04:21,769
a bootstrap built in class which is called as M Dash five.

60
00:04:21,890 --> 00:04:25,350
So that's actually going to leave a margin of five on either side.

61
00:04:25,790 --> 00:04:28,040
And now let's go ahead and create a column.

62
00:04:28,040 --> 00:04:32,630
So div class equals call dash dash 12.

63
00:04:32,930 --> 00:04:33,940
This should be called.

64
00:04:34,460 --> 00:04:40,250
And now once we have this, let's go ahead and add something like E heading which see something like

65
00:04:40,550 --> 00:04:42,650
CV database.

66
00:04:44,090 --> 00:04:46,340
Users or let's see.

67
00:04:47,390 --> 00:04:54,920
Profile list, and now once we have that, let's actually go outside this room and let's go ahead and

68
00:04:54,920 --> 00:04:59,150
loop through all the profiles so I'll make use of the templates and in here.

69
00:04:59,150 --> 00:05:02,420
So I'll see for profile in.

70
00:05:04,710 --> 00:05:11,370
Profiles because profiles are context, object and will make use of info over here, so Chelsea and

71
00:05:11,460 --> 00:05:18,600
forth and in here, let's go ahead and create another room so I'll see Dave.

72
00:05:20,060 --> 00:05:25,890
The class is going to be equal to row, so class is going to be equal to row and in there.

73
00:05:25,910 --> 00:05:33,770
Let's go ahead and create a column so I will see the class equals called Mudie Dash six.

74
00:05:35,760 --> 00:05:42,090
And I'll go ahead and get the profile nymphos, so I'll see profile that name, and because we want

75
00:05:42,090 --> 00:05:47,400
to display the profile name and after displaying the profile name ahead of it, we actually want to

76
00:05:47,400 --> 00:05:53,880
go ahead and actually have a link to that specific person's video page so that we can download that

77
00:05:53,880 --> 00:05:54,270
page.

78
00:05:54,400 --> 00:05:59,070
So in here I can type in the e-TAG and the issue of this is going to be slash.

79
00:06:01,730 --> 00:06:08,120
Profile, not ID, because what we want to do is that we want to emulate the action of actually visiting

80
00:06:08,120 --> 00:06:12,590
the Web page, which is localhost hotspot eight thousand, slash the ID.

81
00:06:12,860 --> 00:06:18,860
So I'll simply go ahead and give it an ID as nothing but the profile ID for that specific name.

82
00:06:19,610 --> 00:06:22,490
Now, once we have that, let's say we see something like.

83
00:06:23,890 --> 00:06:30,970
Download KVI in this links name, and you can also make this link act or look like a button by using

84
00:06:30,970 --> 00:06:39,430
bootstrap, by adding a simple class to this so you can see class equals Mithian between Dasch warning

85
00:06:39,430 --> 00:06:43,540
and you can also go ahead and write when the tag ends.

86
00:06:43,800 --> 00:06:50,320
You can add each other tag here to separate all of the list names so you can see each other over here.

87
00:06:50,470 --> 00:06:55,600
So now once we have this view and once you have rendered the template, let's actually go ahead, create

88
00:06:55,600 --> 00:06:58,390
a real pattern for this specific view.

89
00:06:59,020 --> 00:07:03,640
So we'll see something like that and let us have the path as a list.

90
00:07:04,270 --> 00:07:08,050
So the view is nothing but views, dot.

91
00:07:09,910 --> 00:07:15,340
List and let's also have the name for this thing as list.

92
00:07:15,620 --> 00:07:17,710
Give a comma and you should be good to go.

93
00:07:18,400 --> 00:07:20,380
So now the server is up and running.

94
00:07:20,650 --> 00:07:28,930
So now if I go to slash list, as you can see, it's not displaying any names over here because we might

95
00:07:28,930 --> 00:07:31,690
have missed something up while creating The View.

96
00:07:32,170 --> 00:07:37,650
OK, so the reason why it was not working is because we have used the contacts as profiles in here,

97
00:07:38,080 --> 00:07:39,820
but all year it's is profile.

98
00:07:40,180 --> 00:07:42,240
So this should actually be profiles.

99
00:07:42,280 --> 00:07:47,410
And once we make it profiles, you also need to change the context over here as profiles.

100
00:07:47,620 --> 00:07:53,050
So once you go ahead and do that and once you hit refresh, as you can see now you have the list of

101
00:07:53,050 --> 00:07:56,000
all the candidates for which you want to download the CV.

102
00:07:56,470 --> 00:08:01,720
So if I click on download c.B for Jim, as you can see at the bottom left of the screen, it's showing

103
00:08:01,720 --> 00:08:03,480
that you are as slash, too.

104
00:08:03,490 --> 00:08:06,330
And for this, it's showing that you are slash one.

105
00:08:06,340 --> 00:08:12,370
So whenever you click on these respective download TV buttons or links over here, it's actually going

106
00:08:12,370 --> 00:08:15,220
to go ahead and download the respective CVS for you.

107
00:08:15,760 --> 00:08:19,190
And I guess the bootstrap button class isn't working either.

108
00:08:19,480 --> 00:08:21,910
So let's go ahead and fix that as well.

109
00:08:22,780 --> 00:08:26,620
So if we go to list dot HTML.

110
00:08:28,510 --> 00:08:34,179
I again, misspell this thing and I actually apologize for the typos and everything that's actually

111
00:08:34,179 --> 00:08:38,059
a part of programming and also this should actually be a teacher.

112
00:08:38,919 --> 00:08:43,730
And once we go ahead and fix that, as you can see, our list actually looks much more better.

113
00:08:43,929 --> 00:08:45,460
So that's it for this lecture.

114
00:08:45,460 --> 00:08:48,880
And hopefully you guys enjoyed making this speech and radio.

115
00:08:49,480 --> 00:08:51,250
So thank you very much for watching.

116
00:08:51,250 --> 00:08:54,640
And in the upcoming lectures will be building a different project.

117
00:08:54,910 --> 00:08:56,560
So thank you very much for watching.

118
00:08:56,560 --> 00:08:58,450
And I'll see you guys next time.

119
00:08:58,720 --> 00:08:59,230
Thank you.

