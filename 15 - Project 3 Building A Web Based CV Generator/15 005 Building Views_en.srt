1
00:00:00,060 --> 00:00:04,380
So now let's go ahead and create a view to render this particular form.

2
00:00:05,250 --> 00:00:10,200
So in order to create a view, let's go to the user profile of your app.

3
00:00:10,650 --> 00:00:15,200
And the very first thing which you do here is that you go ahead and impose the model.

4
00:00:15,630 --> 00:00:21,670
So you type in from DOT models and that's going to be profile.

5
00:00:22,410 --> 00:00:29,490
And now, once you have this, let's go ahead and create a view soil type def and let's name this view

6
00:00:29,490 --> 00:00:36,870
as accept, because it's going to accept the user data and it's going to take a request.

7
00:00:37,270 --> 00:00:44,880
And now in order to render a specific template, I can simply type in return render and then I can see

8
00:00:44,880 --> 00:00:51,230
something like request and then I can simply type in the template name over here.

9
00:00:51,240 --> 00:00:58,200
So the template name is except dot html, which is actually present in the PDF directory so I can type

10
00:00:58,200 --> 00:00:59,460
in PDF slash.

11
00:01:00,520 --> 00:01:02,200
Except each time.

12
00:01:02,830 --> 00:01:06,710
So once we are done with this, this is actually going to render the template.

13
00:01:07,120 --> 00:01:13,960
Now, let's associate this particular view with a all, and we do that in the euro stopp file.

14
00:01:14,620 --> 00:01:20,500
So now let's go to the euro stockpile file in your myside directly and in here.

15
00:01:20,560 --> 00:01:26,310
First of all, let's import that particular view so that we can associate the view with the euro.

16
00:01:26,680 --> 00:01:33,400
So I'll say something like from the is PDAF import use.

17
00:01:33,820 --> 00:01:40,810
And now once we have views imported, I can see a path and let's say we actually want to place it in

18
00:01:40,810 --> 00:01:41,590
the home page.

19
00:01:41,600 --> 00:01:44,760
So I leave the part as empty and I'll save you.

20
00:01:44,860 --> 00:01:52,630
Don't, except because accepters the name of the view and I said the name equals except as well.

21
00:01:53,890 --> 00:02:00,610
So now once we have done that, we should actually get the form whenever we visit our home page of our

22
00:02:00,610 --> 00:02:01,050
site.

23
00:02:01,720 --> 00:02:03,370
So let's see if that thing works.

24
00:02:03,520 --> 00:02:05,350
So the server is up and running.

25
00:02:06,220 --> 00:02:10,180
So let's go to the homepage.

26
00:02:10,180 --> 00:02:15,370
And when I had enter, as you can see now, you have a a formal way here.

27
00:02:15,370 --> 00:02:21,340
But I guess there's some issue with the form inside of the e-mail because we have this live tag over

28
00:02:21,340 --> 00:02:24,690
here and we also don't have the button either.

29
00:02:25,330 --> 00:02:28,930
So let's go ahead and make a few changes to this particular form.

30
00:02:29,260 --> 00:02:36,640
So in the excerpt, if I have a look over here, as you can see, we have the div tag inside the about

31
00:02:36,640 --> 00:02:39,040
U.S. So let's go ahead and fix that.

32
00:02:39,550 --> 00:02:42,580
So even before we fix that, let's first go ahead.

33
00:02:42,940 --> 00:02:48,310
And instead of having this form just lying around in here, let's actually create a proper container

34
00:02:48,310 --> 00:02:48,790
in here.

35
00:02:48,790 --> 00:02:56,060
So I'll say div class equals container and then actually have a row and column in here.

36
00:02:56,080 --> 00:03:05,110
So class equals rule and then let's create div class equals call, dash and dash.

37
00:03:05,110 --> 00:03:10,930
And let's see, this is going to span seven columns so I'll see seven in here and then actually paste

38
00:03:10,930 --> 00:03:11,910
in the form in here.

39
00:03:12,340 --> 00:03:17,790
And now if I hit refresh, as you can see, the form is a style and a much more better way.

40
00:03:17,800 --> 00:03:21,080
But we still have this division here for some weird reason.

41
00:03:21,340 --> 00:03:22,840
So let's go ahead and fix that.

42
00:03:23,050 --> 00:03:28,810
So the reason why we actually have that particular issue is because we have actually created these text

43
00:03:28,810 --> 00:03:34,870
areas right here and we actually need to make a few changes to these particular text areas.

44
00:03:35,260 --> 00:03:41,560
The very first change which we need to make here is that you always need to go ahead and close those

45
00:03:41,560 --> 00:03:44,080
text areas whenever you have created them.

46
00:03:44,470 --> 00:03:48,240
So let's go ahead and create a closing text area tag in here.

47
00:03:49,170 --> 00:03:56,310
And let's do the same thing for all the text areas which we have, so we have the text area right over

48
00:03:56,320 --> 00:03:57,900
here, we have close that.

49
00:03:58,680 --> 00:04:00,740
Let's close this one as well.

50
00:04:03,210 --> 00:04:05,850
And let's do the same thing with this as well.

51
00:04:07,080 --> 00:04:10,770
OK, so now once we do that, go back over here and hit refresh.

52
00:04:10,770 --> 00:04:14,520
As you can see now, the form works absolutely fine.

53
00:04:14,640 --> 00:04:20,700
So we have each and every field over here like Meems email, phone number, about you degree and everything

54
00:04:20,700 --> 00:04:21,300
like that.

55
00:04:21,690 --> 00:04:24,750
So that means now our form is actually working.

56
00:04:25,710 --> 00:04:31,980
So now once we have this form, if you submit some details right now in this particular form, the details

57
00:04:31,980 --> 00:04:38,220
aren't going to be stored in this particular form because we now need to go ahead and write the logic

58
00:04:38,670 --> 00:04:43,200
to define what happens when we actually submit this particular form.

59
00:04:43,980 --> 00:04:47,340
So in the next election, we will go ahead and learn how to do that.

60
00:04:47,580 --> 00:04:51,480
So thank you very much for watching and I'll see you guys next time.

61
00:04:51,960 --> 00:04:52,530
Thank you.

