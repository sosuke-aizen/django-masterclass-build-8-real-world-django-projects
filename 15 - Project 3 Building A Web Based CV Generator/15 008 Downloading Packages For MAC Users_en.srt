1
00:00:00,150 --> 00:00:06,990
To convert this specific HTML document into a PDF, we will need to download a couple of packages.

2
00:00:07,330 --> 00:00:13,710
So in order to download these packages, the steps for downloading the packages on Windows and on Mac

3
00:00:13,710 --> 00:00:15,190
are actually going to be different.

4
00:00:15,570 --> 00:00:19,970
So this specific lecture is for downloading those packages on a Mac.

5
00:00:19,980 --> 00:00:22,610
So if you're a Mac user, follow this lecture.

6
00:00:22,890 --> 00:00:28,920
And if you're a Windows user, please follow the next lecture in which we will discuss the steps to

7
00:00:28,920 --> 00:00:31,900
download those packages specific to Windows.

8
00:00:32,040 --> 00:00:35,520
So if your server is up and running, simply go ahead and stop the server.

9
00:00:35,640 --> 00:00:41,290
And the very first package, which will install in our virtual environment right now is called the speed

10
00:00:41,310 --> 00:00:41,840
of kit.

11
00:00:42,240 --> 00:00:49,170
So in order to install a PDF kit, you simply type in PIP, install PDAF kit.

12
00:00:54,260 --> 00:00:59,990
And now, as you can see, it has been successfully installed and the next package, which we need to

13
00:00:59,990 --> 00:01:05,360
install after PDAF kid is W.G. e-mail to PDAF.

14
00:01:05,630 --> 00:01:08,930
So in order to install that package, you type in Braille.

15
00:01:09,680 --> 00:01:14,510
So blue is actually a tool like PIP which helps you to install different packages.

16
00:01:14,840 --> 00:01:17,570
So simply to open you install.

17
00:01:19,480 --> 00:01:22,180
Cask room slash.

18
00:01:24,190 --> 00:01:36,190
Cask slash W.G., HDMI to PDF, so simply go ahead and hit, enter and Brew is actually going to install

19
00:01:36,460 --> 00:01:38,010
the package for you.

20
00:01:38,680 --> 00:01:44,050
Now I'm assuming that you was actually installed on your machine, so if you don't have to install,

21
00:01:44,440 --> 00:01:47,440
you simply need to go ahead and install you on your Mac.

22
00:01:47,470 --> 00:01:53,200
So if you simply do a simple Google search of how to install you on your Mac, you will get a single

23
00:01:53,200 --> 00:01:59,560
command which you can execute on your terminal and that will actually install for you.

24
00:02:00,010 --> 00:02:07,450
So right now, as I have already installed, it's installing the WP HTML, the PDF package for us.

25
00:02:07,600 --> 00:02:13,360
And even before it does that, as you can see, it's updating homebrew because in order to be able to

26
00:02:13,360 --> 00:02:17,030
download and install the package, you need to update itself as well.

27
00:02:17,440 --> 00:02:22,920
So after installing a couple of things, it's actually going to go ahead and ask you for your passwords.

28
00:02:22,920 --> 00:02:24,910
So simply type in your password over here.

29
00:02:27,220 --> 00:02:33,900
And had Intel and as you can see, it sees W.G. e-mail to PDA for successfully installed.

30
00:02:34,750 --> 00:02:36,590
So that's it for this lecture.

31
00:02:36,610 --> 00:02:42,370
And now as we have these packages installed in the next lecture, we will actually go ahead and first

32
00:02:42,370 --> 00:02:47,780
of all, have a video for installation of SIM packages for Windows users.

33
00:02:47,800 --> 00:02:51,990
So if you are not a Windows user, you can skip that and then the lecture next to it.

34
00:02:52,000 --> 00:02:58,510
We are going to go ahead and discuss about how to use these packages to pass in this particular HTML

35
00:02:58,510 --> 00:03:03,740
document, which is this HTML document, and convert it into a PDF.

36
00:03:04,120 --> 00:03:07,840
So thank you very much for watching and I'll see you guys next time.

37
00:03:08,050 --> 00:03:08,620
Thank you.

