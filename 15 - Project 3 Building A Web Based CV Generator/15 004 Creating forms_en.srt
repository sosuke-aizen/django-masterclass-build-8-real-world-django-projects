1
00:00:00,150 --> 00:00:06,480
So in this lecture, let's go ahead and start creating the form for our website, so the very first

2
00:00:06,480 --> 00:00:12,330
thing which we need to do is that we need to use bootstrap because obviously bootstrap is going to make

3
00:00:12,330 --> 00:00:14,370
our form look much more better.

4
00:00:14,790 --> 00:00:20,280
So the very first thing which we obviously need to do is that we need to go ahead and grab the bootstrap

5
00:00:20,280 --> 00:00:20,840
CDN.

6
00:00:21,450 --> 00:00:28,200
So let's go ahead to go to Google and search for bootstrap for CDN, click on the first language pops

7
00:00:28,200 --> 00:00:34,320
up and by now you should actually know how exactly you can go ahead and use the bootstrap CDN so it

8
00:00:34,320 --> 00:00:37,120
will copy the CDN for the success.

9
00:00:37,170 --> 00:00:41,910
So simply go ahead, copy DCG and from here go back to your code editor.

10
00:00:42,180 --> 00:00:48,940
And in here we actually now need to create each HTML template so as to serve a form to our user.

11
00:00:49,350 --> 00:00:54,780
So whenever you want to create a template, you simply go into your app directly, which is in this

12
00:00:54,780 --> 00:00:55,800
case PDF.

13
00:00:56,190 --> 00:01:01,890
You simply go ahead, create a new director or a folder, call it as templates.

14
00:01:02,070 --> 00:01:07,980
And in there again, create a new folder with the app name, which is PDF in this case.

15
00:01:08,760 --> 00:01:15,800
So now once you have this PDA folder, create a new file in here and create an HTML template in here.

16
00:01:15,990 --> 00:01:22,860
So I'll just go ahead and name this HTML template as except that e-mail because it's actually going

17
00:01:22,860 --> 00:01:24,570
to accept the user data.

18
00:01:25,350 --> 00:01:32,330
So now once we have this, let's go ahead and add the head and body tags and the HTML tags in here.

19
00:01:32,820 --> 00:01:39,210
So I'll simply go ahead typing and exclamation mark and the abbreviation is going to write down the

20
00:01:39,210 --> 00:01:40,590
e-mail code for me.

21
00:01:41,070 --> 00:01:45,000
So when I go ahead and hit enter, I now have the head and the body tags.

22
00:01:45,660 --> 00:01:51,010
So now let's simply go ahead and paste the bootstrap CDN link in here.

23
00:01:51,360 --> 00:01:57,930
So as you can see, I've posted that link and now let's go ahead and also get the form from bootstrap

24
00:01:57,930 --> 00:02:02,970
itself because you obviously don't want to write the form from scratch.

25
00:02:02,970 --> 00:02:09,020
Search for bootstrap forms, go to the official link, which is from get bootstrap dot com.

26
00:02:09,270 --> 00:02:15,330
And in here, if you scroll down a little bit, you will be able to see this particular form right here.

27
00:02:15,600 --> 00:02:21,750
Now, this is actually a login form, but you can make a few changes in this particular form to suit

28
00:02:21,750 --> 00:02:22,770
your own needs.

29
00:02:23,070 --> 00:02:28,080
So I'll simply go ahead, copy this form and go back.

30
00:02:28,090 --> 00:02:33,750
You pick it up in the body and will make a few changes in here.

31
00:02:33,900 --> 00:02:40,500
So first of all, we'll just keep this e-mail file right here and we will get rid of the other things

32
00:02:40,500 --> 00:02:44,100
over here except for the button and in here.

33
00:02:44,760 --> 00:02:50,160
Let's go ahead and let the lead this small tag as well.

34
00:02:51,140 --> 00:02:55,740
So we only want the label as well as the input field.

35
00:02:56,610 --> 00:03:06,630
And let's change the text of the label to except name and let's go ahead and change the input pipe over

36
00:03:06,630 --> 00:03:06,960
here.

37
00:03:06,990 --> 00:03:13,290
So this is actually going to be text and we will also go ahead and change the idea over here as well.

38
00:03:14,610 --> 00:03:17,850
So first of all, let's also get rid of the placeholder.

39
00:03:18,360 --> 00:03:20,310
If you want, you can add a placeholder.

40
00:03:20,670 --> 00:03:25,380
And in here, I'll also get rid of this class as well.

41
00:03:26,070 --> 00:03:32,940
And for the idea will go ahead and change this idea according to the names which we have here.

42
00:03:33,210 --> 00:03:38,880
So for the first feel as we are accepting the name, I'll just go ahead and mention the name and ID

43
00:03:38,880 --> 00:03:41,530
for that particular input field as name itself.

44
00:03:42,090 --> 00:03:48,680
So let's go ahead and type in the ideas name and also said the name equals a name.

45
00:03:49,440 --> 00:03:53,040
So we are going to do the same thing for all the other fields as well.

46
00:03:53,430 --> 00:03:56,560
So let me just go ahead copy this particular form group.

47
00:03:57,390 --> 00:04:00,170
So let me copy that pasted up over here.

48
00:04:00,210 --> 00:04:05,980
And this should actually accept the email and you can also get rid of this for as well.

49
00:04:06,570 --> 00:04:11,490
And this is actually going to accept email as email is actually our second file.

50
00:04:11,490 --> 00:04:18,300
So I'll type an email over here and the input type is equal text and the idea and name are going to

51
00:04:18,300 --> 00:04:21,089
be email and email.

52
00:04:21,120 --> 00:04:26,230
So now once you have email, you also need to go ahead and do the same thing for phone as well.

53
00:04:26,580 --> 00:04:28,650
So let's do the same thing for phone.

54
00:04:28,800 --> 00:04:31,350
So it'll again, go ahead, copy the same thing.

55
00:04:32,340 --> 00:04:33,470
Piece the door here.

56
00:04:33,690 --> 00:04:40,810
I will see the phone and this is going to be phone and this is going to be phone as well.

57
00:04:41,880 --> 00:04:43,790
And now for the text area.

58
00:04:43,860 --> 00:04:49,920
So if you actually go ahead and have a look at this model for the summary, we now have the text file

59
00:04:49,920 --> 00:04:50,640
right away here.

60
00:04:51,270 --> 00:04:55,020
So for the text field, you don't need to have the input type.

61
00:04:55,020 --> 00:04:58,520
Instead, you can use something which is called as a text area.

62
00:04:59,100 --> 00:05:04,740
So whenever you want to use text area, you simply go ahead, first of all, copy this particular form

63
00:05:04,740 --> 00:05:06,370
group pasted over here.

64
00:05:07,140 --> 00:05:15,000
And as this is going to accept the summary, I'll go ahead and see about you.

65
00:05:15,690 --> 00:05:23,850
And in here, instead of input assembly type and text area and ask for the ID and name and type in somebody

66
00:05:23,850 --> 00:05:26,730
and somebody over here.

67
00:05:27,030 --> 00:05:32,870
So now once you go ahead and do that, you can do almost the same thing for all these fields as well.

68
00:05:32,880 --> 00:05:37,320
So you can do the same thing for degree, school, university, previous work and skills.

69
00:05:37,860 --> 00:05:44,550
So for these three things, we are using the character feel to create a input, feel for accepting these

70
00:05:44,550 --> 00:05:47,780
details so you can go ahead and do that on your own as well.

71
00:05:48,090 --> 00:05:50,950
So it'll simply go ahead and create mine up over here.

72
00:05:51,540 --> 00:06:00,510
So simply go ahead and copy these and create three more of these one, two and three, and I'll simply

73
00:06:00,510 --> 00:06:02,100
go ahead and keep making changes.

74
00:06:02,100 --> 00:06:03,260
So we hit accordingly.

75
00:06:03,600 --> 00:06:14,460
So now I need the degree school and university, so I'll simply go ahead and type in a degree school.

76
00:06:16,070 --> 00:06:23,360
And Universal City, and let me also make sure to make changes over here as well.

77
00:06:45,710 --> 00:06:52,760
So not only two things which are remaining are the previous work and skills, so simply go ahead and

78
00:06:52,760 --> 00:07:01,520
get the text area for both of these and the piece the over here previous work.

79
00:07:05,130 --> 00:07:15,480
And for the idea and name that's going to be previous and the school work and previous industrial work,

80
00:07:16,770 --> 00:07:26,190
and the final thing is the skills, so I'll see something like skills and now I'll change this thing

81
00:07:26,190 --> 00:07:30,210
to skills and skills.

82
00:07:31,890 --> 00:07:35,340
OK, so now once we have this form, we are pretty much good to go.

83
00:07:35,760 --> 00:07:42,090
So now in order to test this particular form, we actually need to go ahead and create a view for this

84
00:07:42,090 --> 00:07:45,510
form which is going to render this particular template.

85
00:07:45,870 --> 00:07:48,990
So in the next lecture will go ahead and create that for you.

86
00:07:48,990 --> 00:07:55,470
And we will also associate that particular view with a specific you are so that whenever you visit that

87
00:07:55,470 --> 00:08:00,570
you are told you will actually go ahead and get this form wherein you can submit all the data.

88
00:08:01,080 --> 00:08:05,070
So thank you very much for watching and I'll see you guys next time.

89
00:08:05,340 --> 00:08:05,910
Thank you.

