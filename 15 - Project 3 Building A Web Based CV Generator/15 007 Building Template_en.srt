1
00:00:00,150 --> 00:00:06,930
OK, so now that we actually have the data in the background, let's now go ahead and learn how to generate

2
00:00:06,930 --> 00:00:10,110
a PDF out of the data which we have now back in.

3
00:00:10,230 --> 00:00:15,780
So as you can see, we have created this form but submits the data and saves it to the database.

4
00:00:16,170 --> 00:00:23,670
But now our challenge is to find out how to turn this data into a PDF of our own style, which has a

5
00:00:23,670 --> 00:00:24,870
specific format.

6
00:00:25,470 --> 00:00:33,060
So the way in which the logic of creating a PDF in this situation works is that instead of creating

7
00:00:33,060 --> 00:00:40,680
a PDF, you actually go ahead and create a HTML page or and each HTML template, which actually has

8
00:00:40,680 --> 00:00:42,560
the layout of your CV.

9
00:00:43,230 --> 00:00:50,550
And once you have that layout of the CV, you simply go ahead and convert that template or pass that

10
00:00:50,550 --> 00:00:52,440
template to a library.

11
00:00:52,440 --> 00:00:58,950
And that library is actually going to go ahead, take up your HTML template and then it's going to convert

12
00:00:58,950 --> 00:01:02,490
that HTML template into a downloadable PDF file.

13
00:01:03,150 --> 00:01:10,080
So in order to create a PDF out of this data, our first job is to actually go ahead and create a template

14
00:01:10,350 --> 00:01:11,860
for our PDF.

15
00:01:12,180 --> 00:01:17,410
So in order to create a template for your PDF, these steps are no different.

16
00:01:17,430 --> 00:01:22,930
You simply go ahead and create a template, as we used to do for any other Django application.

17
00:01:23,370 --> 00:01:28,030
So you simply go to PDF and you can go ahead and create a new in here.

18
00:01:28,050 --> 00:01:32,010
So I'll go ahead and create a new file and animate as.

19
00:01:32,860 --> 00:01:36,740
Resume, we don't e-mail or CV dot e-mail.

20
00:01:37,120 --> 00:01:40,670
You can go ahead and give this thing any kind of name which you want.

21
00:01:41,260 --> 00:01:46,450
So now once you have this specific file, you can now go ahead, start writing some code in here.

22
00:01:46,810 --> 00:01:50,380
So I'll just go ahead and include the basic HTML tags.

23
00:01:50,530 --> 00:01:55,720
And that's actually going to be the head tag and the body tag and the HDMI attacks.

24
00:01:56,290 --> 00:02:02,860
And now in here, we will go ahead and create a simple layout to fill up this entire template with the

25
00:02:02,860 --> 00:02:08,280
details of the user, like the name, email, phone number, Summerlee degrees.

26
00:02:08,289 --> 00:02:10,060
Cool, so on and so forth.

27
00:02:10,680 --> 00:02:17,500
So the very first thing which I will do here is that I will go ahead and I'll get the user profile name,

28
00:02:17,740 --> 00:02:21,670
so I'll go ahead and type in and each to tag in here and in here.

29
00:02:21,670 --> 00:02:26,610
Let's go ahead and get the name, which is the name of the candidate.

30
00:02:27,040 --> 00:02:34,030
So in order to get that, you also need to remember that you must pass in a context within a particular

31
00:02:34,030 --> 00:02:34,330
view.

32
00:02:34,570 --> 00:02:40,750
So if you go to The View, start by file, as you can see, we do have a view to actually render the

33
00:02:40,750 --> 00:02:44,790
form, but we don't have a view to render this particular HTML template.

34
00:02:45,040 --> 00:02:50,980
So even before we start creating this template, let's first go ahead and create a view to render this

35
00:02:50,980 --> 00:02:57,880
template and let's actually pass in the context so that we can use that context over here to get the

36
00:02:57,880 --> 00:02:59,770
data from the database.

37
00:03:00,250 --> 00:03:07,370
So in order to create a view, simply go ahead and type in def and let's name this view as a let's a

38
00:03:07,450 --> 00:03:08,130
resum�.

39
00:03:08,740 --> 00:03:12,070
And this is actually going to accept a request.

40
00:03:13,740 --> 00:03:19,620
And it's also willing to accept an ID and the ID is going to be nothing, but it's actually going to

41
00:03:19,620 --> 00:03:24,040
be the ID of the user for which we want to download a PDF.

42
00:03:24,300 --> 00:03:31,290
So, for example, if we have a look at our model, we will have multiple profiles saved up in our database.

43
00:03:31,560 --> 00:03:37,510
And at a certain point of time, we only want to download one specific profile.

44
00:03:37,980 --> 00:03:44,160
So in here you need to pass in an ID as well so that you know that you want to download the CV of that

45
00:03:44,160 --> 00:03:47,430
specific person who has that specific ID.

46
00:03:48,240 --> 00:03:54,890
OK, so now let's go ahead and make use of this I.D. to get the profile of that user.

47
00:03:55,080 --> 00:03:57,410
So I'll see user profile.

48
00:03:57,750 --> 00:04:02,400
And let's now go ahead and extract the objects from the profile model.

49
00:04:02,530 --> 00:04:03,870
So I'll say profile.

50
00:04:05,390 --> 00:04:12,950
Not object dot, and now as we actually want to get a single object, I'll type and get and we want

51
00:04:12,950 --> 00:04:20,230
to get the object of the model profile of the peak of the primary, he is going to be equal to it.

52
00:04:20,810 --> 00:04:27,000
So you can also type in ID equals idea here, but for now, I'll just go ahead and keep it pecky.

53
00:04:27,830 --> 00:04:33,830
OK, so now once we have this object now, we need to basically go ahead and pass on this particular

54
00:04:33,830 --> 00:04:34,460
object.

55
00:04:34,760 --> 00:04:35,120
Do you.

56
00:04:35,120 --> 00:04:35,930
A template.

57
00:04:36,110 --> 00:04:49,040
So I'll simply say return rindo then I'll pass in a request and then I'll see PDF slash and the name

58
00:04:49,040 --> 00:04:55,980
of our template is resuming dot each H.M. and then I'll go ahead and passing the context over here.

59
00:04:56,360 --> 00:05:02,880
So in order to pass in the context, L.C user profile and user profile.

60
00:05:03,620 --> 00:05:10,160
OK, so once we do that we are pretty much good to go and now we can go ahead and make use of this user

61
00:05:10,160 --> 00:05:14,900
profile context in here to actually extract the details of the users.

62
00:05:15,410 --> 00:05:17,220
So let's go ahead and do that as well.

63
00:05:17,330 --> 00:05:18,500
So let's go ahead.

64
00:05:18,500 --> 00:05:25,940
And first of all, get the name over here in the H2 tag so I'll see something like user the school profile

65
00:05:26,120 --> 00:05:27,470
dot name.

66
00:05:28,250 --> 00:05:33,980
Then let's go ahead and also get the other fields as well, like the email and the phone number.

67
00:05:34,160 --> 00:05:37,400
So I'll also go ahead copy this.

68
00:05:39,220 --> 00:05:46,660
Piece it over here and change this thing to let's see after the name you want to display the email,

69
00:05:46,660 --> 00:05:49,830
so I'll type in email over here and let's see.

70
00:05:49,870 --> 00:05:51,290
This is going to be fun.

71
00:05:51,820 --> 00:05:57,160
So once you have these things, let's go ahead and add a line over here.

72
00:05:57,160 --> 00:06:00,610
So I'll type in each other for a horizontal line.

73
00:06:00,640 --> 00:06:05,810
And now let's go ahead and display the other details like the summary and the skills.

74
00:06:06,220 --> 00:06:09,940
So right after this, I'll go ahead and add a brick tag as well.

75
00:06:10,390 --> 00:06:15,900
So I'll type in backslash slash and now let's add a paragraph.

76
00:06:15,910 --> 00:06:18,970
So I'll go ahead, add a paragraph over here.

77
00:06:18,970 --> 00:06:22,770
And this paragraph is going to see something like somebody.

78
00:06:23,320 --> 00:06:27,630
And now let's add the actual summary in another paragraph tag.

79
00:06:27,640 --> 00:06:33,990
And in order to get the somebody else type open user profile dot summary over here.

80
00:06:34,150 --> 00:06:35,340
And now let's go ahead.

81
00:06:35,350 --> 00:06:37,270
Do the same thing for skills as well.

82
00:06:37,300 --> 00:06:38,110
So I'll go ahead.

83
00:06:38,110 --> 00:06:45,930
Simply copy that we start up over here C skills and also use user profile dot skills.

84
00:06:47,020 --> 00:06:50,260
And now let's do the same thing for education as well.

85
00:06:50,260 --> 00:06:51,460
So I'll simply go ahead.

86
00:06:51,460 --> 00:06:52,300
Copy that.

87
00:06:52,610 --> 00:06:53,500
I will see.

88
00:06:54,960 --> 00:07:02,940
Education and in here, in order to list out the education, we will add the school and the university

89
00:07:02,940 --> 00:07:03,790
name over here.

90
00:07:04,110 --> 00:07:09,480
So in order to add them up, I'll create an unordered list and within that I'll create a list element.

91
00:07:09,610 --> 00:07:11,060
So I'll see list element.

92
00:07:11,070 --> 00:07:22,050
And in here, the first thing would be user profile dot school and the next thing would be user profile

93
00:07:22,050 --> 00:07:22,530
dot.

94
00:07:22,650 --> 00:07:24,750
That's going to be university.

95
00:07:28,430 --> 00:07:31,590
So once we have these things, we are pretty much in good to go.

96
00:07:32,450 --> 00:07:39,200
So now once we have set up this specific thing, let's actually go ahead and also create a dual pattern

97
00:07:39,620 --> 00:07:45,320
for this specific view so that we can actually go ahead, use this particular view to render the template

98
00:07:45,320 --> 00:07:46,670
which we have just created.

99
00:07:47,010 --> 00:07:53,720
And as you can see, we do have an error over here because, yeah, we forgot to make it objects.

100
00:07:53,720 --> 00:08:01,250
So this should actually be objects because we get a specific object from objects and for some reason

101
00:08:01,370 --> 00:08:04,030
the error just won't go OK.

102
00:08:04,070 --> 00:08:05,810
So for now, the server is up and running.

103
00:08:06,470 --> 00:08:11,940
So let's go ahead and create a real pattern for this specific view which is resuming.

104
00:08:12,710 --> 00:08:19,790
So go ahead, go to your all spy file and I will simply go ahead and type in path.

105
00:08:20,750 --> 00:08:27,020
And for this, let's see, the wall is going to be the ID, which we have just passed.

106
00:08:27,200 --> 00:08:33,500
That means that whenever the user types and localhost about eight slash one, that means we want to

107
00:08:33,500 --> 00:08:36,860
get the profile of the user whose ID is one.

108
00:08:37,250 --> 00:08:39,190
So I'll pass on that idea here.

109
00:08:39,200 --> 00:08:43,000
So I'll see integer ID as ID is an integer value.

110
00:08:43,580 --> 00:08:47,030
I'll give a slash and then I can simply see Vieuxtemps.

111
00:08:48,180 --> 00:08:53,480
Resum� Gomaa name equals resume.

112
00:08:54,840 --> 00:08:58,500
OK, so once we go ahead do that, everything should be working fine.

113
00:08:59,130 --> 00:09:05,910
So now let's go back over here and let's say when I go ahead and type in one, as you can see, we got

114
00:09:05,910 --> 00:09:09,690
the details of the user whose idea was one.

115
00:09:10,600 --> 00:09:15,360
And as you can see, this thing right here is nothing but the template of our CV.

116
00:09:15,980 --> 00:09:21,640
Now, I know that this looks actually quite simple, so we are going to bump it up in the upcoming lectures.

117
00:09:21,640 --> 00:09:27,550
But for now, you know that the template actually works, but we don't actually want to use this template.

118
00:09:27,670 --> 00:09:33,340
Instead, what we want in place of this template is that we want an actual PDF file, which should be

119
00:09:33,340 --> 00:09:37,060
downloadable when we go ahead and visit this specific idea.

120
00:09:37,690 --> 00:09:39,930
So how exactly can you go ahead and do that?

121
00:09:40,300 --> 00:09:48,040
So in order to do that now, you need to go ahead, use or pass this specific HTML template to a specific

122
00:09:48,070 --> 00:09:54,440
library or a specific module which is going to convert this HTML template into a --.

123
00:09:54,640 --> 00:09:58,400
So we will learn how exactly can we do that in the upcoming lecture.

124
00:09:58,570 --> 00:10:02,500
So thank you very much for watching and I'll see you guys next time.

125
00:10:02,740 --> 00:10:03,340
Thank you.

