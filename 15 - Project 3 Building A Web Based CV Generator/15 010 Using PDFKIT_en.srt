1
00:00:00,150 --> 00:00:06,060
Now, let's go ahead and let's make use of the packages which we have installed and let's go ahead and

2
00:00:06,060 --> 00:00:10,290
use that to convert this HTML document into a PDF.

3
00:00:10,830 --> 00:00:15,920
So let's go back to our code right here and we will go to The View Torpy file right over here.

4
00:00:16,560 --> 00:00:21,630
And even before we start writing some code in here, let's first go ahead and import a bunch of packages

5
00:00:21,630 --> 00:00:22,000
in here.

6
00:00:22,530 --> 00:00:26,340
So the very first package is obviously going to be a PDF kit.

7
00:00:26,700 --> 00:00:30,900
So assembly type, an import PDF kit.

8
00:00:32,189 --> 00:00:38,880
As we now have it downloaded and installed, and then we will need to go ahead and also input HTP response

9
00:00:38,880 --> 00:00:39,400
as well.

10
00:00:39,810 --> 00:00:45,540
So we'll see, uh, from Shango dot htp import.

11
00:00:45,850 --> 00:00:47,490
That's going to be HTP.

12
00:00:48,800 --> 00:00:58,130
Response, and we will also go ahead and import a template luder as well, so see from Django dot template

13
00:00:58,940 --> 00:01:01,110
import LoDo.

14
00:01:02,390 --> 00:01:05,410
And another thing which you need to import is Iyo.

15
00:01:05,600 --> 00:01:09,320
So I'll say import uyou and we are going to go.

16
00:01:09,590 --> 00:01:15,920
OK, so now as we actually want to go ahead and make use of the template and instead EPRDF out of it,

17
00:01:16,310 --> 00:01:20,810
the very first thing which we do is that we actually get that a specific template.

18
00:01:20,840 --> 00:01:24,480
So right now, as you can see, we have directly passed that template over here.

19
00:01:24,950 --> 00:01:30,650
So instead of that, we will use the loader to actually load up the template and see it into some sort

20
00:01:30,650 --> 00:01:31,670
of variable.

21
00:01:32,060 --> 00:01:39,010
So you'll see something like template equals and then I'll see LoDo don't get yet another school template.

22
00:01:39,350 --> 00:01:43,870
And in order to get that template, I simply need to pass XPath over here.

23
00:01:43,880 --> 00:01:48,410
So I'll see PDF slash resum� dot html.

24
00:01:48,950 --> 00:01:55,280
And after that, once we have this particular template, I'll go ahead and render this template by passing

25
00:01:55,280 --> 00:01:56,150
in the context.

26
00:01:56,510 --> 00:02:02,210
So right now when we load this template, this template is going to be just empty and it won't have

27
00:02:02,210 --> 00:02:07,130
any kind of context that it won't have any kind of dynamic data that is the user data.

28
00:02:07,550 --> 00:02:14,180
Now, in order to pass in the user data, alsi e-mail equals template dot rindo.

29
00:02:14,450 --> 00:02:18,420
And while rendering this template, we make sure to pass in the context.

30
00:02:18,440 --> 00:02:21,100
So this thing right here is nothing but a context.

31
00:02:21,470 --> 00:02:24,330
So it'll simply go ahead and pass that thing up over here.

32
00:02:24,770 --> 00:02:29,370
And for now, you can completely ignore this line because we are going to modify this later.

33
00:02:29,810 --> 00:02:36,170
So this is what we are doing for right now and now the reason why we have actually extracted this template

34
00:02:36,170 --> 00:02:42,440
and we have used the loader to get template and we have used this HTML variable to actually store the

35
00:02:42,440 --> 00:02:49,610
render template is because now we need to pass in this particular render template to the PDF get library,

36
00:02:49,610 --> 00:02:52,010
which we had installed in the prior lecture.

37
00:02:52,550 --> 00:03:00,470
So now let's go ahead and use that PDF library so I'll see PDF equals that's going to be PDF get DOT

38
00:03:00,770 --> 00:03:03,500
and we will use this from string method.

39
00:03:03,980 --> 00:03:11,060
So what does from String Method does is that it takes an HTML string and it converts that HTML string

40
00:03:11,060 --> 00:03:19,430
into a PDF document, so it'll say from another school string and then it actually needs to accept the

41
00:03:19,430 --> 00:03:20,380
e-mail string.

42
00:03:20,390 --> 00:03:25,510
So we already have this additional string created for us, which is nothing but a render template.

43
00:03:25,520 --> 00:03:27,470
So it'll need to pass in that thing over here.

44
00:03:27,500 --> 00:03:33,560
So I'll see each HTML then it needs to accept a parameter which is going to be false for our specific

45
00:03:33,560 --> 00:03:33,920
case.

46
00:03:34,220 --> 00:03:38,200
And in here we need to pass in something which is called as options.

47
00:03:38,660 --> 00:03:44,450
And now these options are not actually defined, but the other options which define the layout of our

48
00:03:44,450 --> 00:03:45,420
PDF page.

49
00:03:45,440 --> 00:03:51,700
So, for example, we can go ahead and define the size and the encoding of that specific page.

50
00:03:51,740 --> 00:03:53,840
So let's specify those options over here.

51
00:03:53,840 --> 00:04:05,570
So L.C options equals and let's say the page size is going to be, let's see, Lettow, that means it's

52
00:04:05,570 --> 00:04:14,500
going to be a PDF of a size of a letter and the encoding is going to be, let's say, UTF Dash eight.

53
00:04:14,510 --> 00:04:20,100
And once we have these things, we have specifically said those options to render that particular HDMI

54
00:04:20,100 --> 00:04:20,540
speech.

55
00:04:20,959 --> 00:04:28,550
And now after actually having that HTML page rendered and pasted over here PDF, it is actually going

56
00:04:28,550 --> 00:04:31,730
to convert that into a specific PDF.

57
00:04:32,390 --> 00:04:37,620
Now, once we have this PDF, we now want this PDF to be automatically downloaded.

58
00:04:38,090 --> 00:04:41,090
So for that, we need to enlist a few more options.

59
00:04:41,300 --> 00:04:46,730
So the very first thing which we need to do is that we need to get rid of all the things which we are

60
00:04:46,730 --> 00:04:47,530
returning here.

61
00:04:47,540 --> 00:04:51,170
And instead we will go ahead and return a specific response for this.

62
00:04:51,800 --> 00:04:54,560
So I'll simply go ahead and type in response.

63
00:04:56,570 --> 00:04:59,610
And we have not yet created this response as of now.

64
00:04:59,660 --> 00:05:01,730
So let's go ahead and create this response.

65
00:05:02,030 --> 00:05:04,580
So L.C response equals.

66
00:05:06,280 --> 00:05:13,960
SCDP response and in here, we actually want to return a plea of in this particular HDB responsibility

67
00:05:13,990 --> 00:05:17,160
PDF, which is nothing but this parameter right here.

68
00:05:17,170 --> 00:05:23,540
And then we also need to specify the type of the content which this HTP response needs to deliver.

69
00:05:24,010 --> 00:05:26,830
So I'll say content underscore.

70
00:05:28,360 --> 00:05:33,850
Type and this is going to be nothing but applications, applications.

71
00:05:35,430 --> 00:05:36,030
PDF.

72
00:05:37,300 --> 00:05:42,230
So once we have that thing, we now need to mention that we want to make this speedy of downloadable,

73
00:05:42,230 --> 00:05:45,330
so we need to set a few response parameters as well.

74
00:05:45,340 --> 00:05:54,280
So I'll see something like response of content, dash dispositioned.

75
00:05:54,280 --> 00:05:56,770
And this is going to say something like attachment.

76
00:05:58,450 --> 00:06:04,780
Because this is going to be EPRDF attachment and then we need to specify the name of the file of our

77
00:06:04,780 --> 00:06:11,920
PDF, so I'll see file name equals, and that's going to be, let's see, resum� dot.

78
00:06:13,010 --> 00:06:18,600
PDF, and then we finally have this thing, we go ahead and return this specific response.

79
00:06:18,620 --> 00:06:21,530
So now once we have that, we are pretty much good to go.

80
00:06:22,010 --> 00:06:29,250
So now let's see if this thing works when we go ahead and see one to download this specific document.

81
00:06:30,050 --> 00:06:31,030
So let me go ahead.

82
00:06:31,040 --> 00:06:32,050
Go back over here.

83
00:06:32,300 --> 00:06:34,840
And first of all, I need to fire up the server.

84
00:06:35,510 --> 00:06:39,320
So Python three managed by one server.

85
00:06:40,370 --> 00:06:43,960
And now let's get back and let's refresh.

86
00:06:44,270 --> 00:06:46,310
And now let me just type in slash one.

87
00:06:48,160 --> 00:06:50,170
And we do have an area over here.

88
00:06:51,900 --> 00:06:54,690
OK, so I guess we have misspelled including.

89
00:06:55,750 --> 00:06:59,410
There should be encoding, so extremely sorry for that.

90
00:06:59,440 --> 00:07:03,340
And now once we go ahead, hit refresh and type and slash one.

91
00:07:04,980 --> 00:07:07,030
A PDAF should be downloaded over here.

92
00:07:07,050 --> 00:07:10,420
And as you can see, we have UPD of downloaded over here.

93
00:07:10,440 --> 00:07:16,350
So when I simply go ahead and open this thing up, as you can see now, we have the c.v generated over

94
00:07:16,350 --> 00:07:18,250
here in a PDF format.

95
00:07:18,270 --> 00:07:24,570
And now, as you can see, we have this particular response, not in the form of a HTML page, but instead

96
00:07:24,570 --> 00:07:31,350
it's actually a PDA which is automatically downloaded to your computer and this specific path, that

97
00:07:31,350 --> 00:07:37,440
means we successfully went ahead and created a PDF out of the data which was stored in our database.

98
00:07:38,340 --> 00:07:42,960
So the same logic can actually be used to create Piaf's of different sorts.

99
00:07:43,140 --> 00:07:49,020
So if you want to go ahead and create invoice for your specific webapp, you can go ahead and make use

100
00:07:49,020 --> 00:07:54,810
of the same logic and same steps to generate PDF out of the data which is stored in your database.

101
00:07:55,230 --> 00:07:58,670
So now let's try to go ahead and add another detail here.

102
00:07:58,680 --> 00:07:59,580
So let's see.

103
00:08:00,660 --> 00:08:03,240
The name of the person is Jim.

104
00:08:03,270 --> 00:08:11,430
Let's see, the email address is demo at demo, dot com and let's see the phone numbers.

105
00:08:11,430 --> 00:08:15,380
Some random number, like nine nine nine nine nine nine.

106
00:08:15,870 --> 00:08:16,830
Let's see it.

107
00:08:16,830 --> 00:08:19,610
See something like this is Jim.

108
00:08:20,370 --> 00:08:20,850
Let's see.

109
00:08:20,850 --> 00:08:23,610
The degree is V-Tech.

110
00:08:24,690 --> 00:08:25,170
Let's see.

111
00:08:25,170 --> 00:08:30,960
The school is X, Y, Z, School of Science.

112
00:08:30,960 --> 00:08:33,720
Let's say the university is Mittie.

113
00:08:34,049 --> 00:08:42,360
Let's say previous work is worked as a programmer at Company X.

114
00:08:44,010 --> 00:08:49,920
Let's see skills, Python, Shango and.

115
00:08:52,610 --> 00:08:53,330
Giwa.

116
00:08:54,460 --> 00:08:55,070
Script.

117
00:08:55,750 --> 00:08:59,600
So let's go ahead, submit this data, and as you can see, the data is submitted.

118
00:09:00,100 --> 00:09:03,350
Now, let's get the specific idea of this person.

119
00:09:03,370 --> 00:09:08,260
So let me go to admen and see what kind of objects do we have saved in our back and.

120
00:09:09,360 --> 00:09:15,630
OK, so if we go to profiles, as you can see, we only have two profiles, so now if I go ahead and

121
00:09:15,630 --> 00:09:22,530
if I want to download data for gems, I can go ahead and slash to hit enter and it's going to go ahead

122
00:09:22,530 --> 00:09:24,080
and download the CV for me.

123
00:09:24,090 --> 00:09:29,850
So when I open this thing up, as you can see now we have the CV Fortum, which has all the details

124
00:09:29,850 --> 00:09:30,880
listed up over here.

125
00:09:30,900 --> 00:09:36,780
So in the next lecture, what we can do is that we can go ahead and tweak up this particular HMO document

126
00:09:36,780 --> 00:09:39,380
a little bit to make it look a little bit better.

127
00:09:39,390 --> 00:09:46,560
And after doing that, we will also go ahead and create a page which is going to list out all the candidates

128
00:09:46,560 --> 00:09:52,200
which we actually have in our database so that the user does not have to manually go ahead and type

129
00:09:52,200 --> 00:09:57,890
in the you are to slash one or you are a slash to to download the respective resumes.

130
00:09:58,290 --> 00:10:00,150
So thank you very much for watching.

131
00:10:00,150 --> 00:10:02,040
And I'll see you guys next time.

132
00:10:02,340 --> 00:10:02,880
Thank you.

