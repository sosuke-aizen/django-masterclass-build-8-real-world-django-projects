1
00:00:00,480 --> 00:00:09,060
So here's how to go ahead and install the packages like PDF, get an e-mail to PDF on Windows, so if

2
00:00:09,060 --> 00:00:15,210
you're on Windows, simply open up the command prompt and in here, you just need to go ahead and type

3
00:00:15,210 --> 00:00:19,850
in PIP, install PDF kit.

4
00:00:19,860 --> 00:00:24,930
And as you can see, it's actually going to go ahead and download and install PDF get for you.

5
00:00:25,740 --> 00:00:32,610
So now once it is installed, you now need to go ahead and download W.G. e-mail to PDF.

6
00:00:33,120 --> 00:00:38,700
So in order to download that, you simply need to open up Google Chrome or any of the Web browser and

7
00:00:38,700 --> 00:00:43,800
you need to simply search for W.G. HDMI to PDF.

8
00:00:45,180 --> 00:00:51,380
So now just go to the download link for this specific site, which is the official site.

9
00:00:51,390 --> 00:00:58,230
So the official site, which will be looking at is WGBH html to PDF dot org, simply go to its download

10
00:00:58,230 --> 00:00:58,740
section.

11
00:00:59,220 --> 00:01:04,030
And in here, as you can see, you will have the different kinds of files which you can download.

12
00:01:04,470 --> 00:01:07,660
So here we have the file for Windows as well.

13
00:01:07,680 --> 00:01:10,860
So here we have the 32 or 64 bit version.

14
00:01:11,490 --> 00:01:12,980
So simply click over here.

15
00:01:15,210 --> 00:01:19,710
So now it's actually going to go ahead and download this specific file for you.

16
00:01:19,740 --> 00:01:24,890
So once the file is downloaded, you simply need to go ahead and download and install this file.

17
00:01:24,900 --> 00:01:30,240
And after going through the installation steps, you then actually need to go ahead and add the pad

18
00:01:30,240 --> 00:01:32,700
for this inside the environment variable.

19
00:01:32,720 --> 00:01:34,440
So simply open this thing up.

20
00:01:35,820 --> 00:01:36,660
Open it up.

21
00:01:38,250 --> 00:01:43,710
And as you can see, it's asking you to basically go ahead and extract the somewhere, so in order to

22
00:01:43,710 --> 00:01:46,020
extract this, I'll simply go ahead.

23
00:01:47,440 --> 00:01:50,060
And extract it to a specific folder.

24
00:01:50,170 --> 00:01:59,290
So let's go ahead and extract it to see and in the C drive, let's see, I extracted directly inside

25
00:01:59,290 --> 00:02:02,040
that drive, so I'll go ahead and click, OK?

26
00:02:05,880 --> 00:02:09,300
And now, as you can see, it has extracted those files for us.

27
00:02:09,320 --> 00:02:15,710
So now let's go to the C drive and in here, as you can see, you actually have this particular folder.

28
00:02:16,050 --> 00:02:20,820
And if you open this thing up, you will actually have the Benfold.

29
00:02:20,940 --> 00:02:26,350
And if you go to the Benfold, this is where the HTML, the -- is present.

30
00:02:26,730 --> 00:02:31,530
So now what you need to do is that you need to simply copy this specific path.

31
00:02:31,950 --> 00:02:37,080
And once you copy this path, you need to go to your computer, click on properties.

32
00:02:38,010 --> 00:02:44,520
And after clicking on properties, you need to go to advance system settings, go to Entwinement Preambles

33
00:02:44,760 --> 00:02:49,560
and in here for the path variable, you simply need to go ahead like that and click on edit.

34
00:02:49,710 --> 00:02:52,970
So you simply need to go ahead and add that new python here.

35
00:02:54,100 --> 00:03:02,030
And click on OK, click on OK, and you should be good to go, so once you do the specific step, W.G.

36
00:03:02,060 --> 00:03:07,540
HTML will be downloaded and installed on your machine and you can now actually go ahead and use that

37
00:03:07,540 --> 00:03:09,040
package in your project.

