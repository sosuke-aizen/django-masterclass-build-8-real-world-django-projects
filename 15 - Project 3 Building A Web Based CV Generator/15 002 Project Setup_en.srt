1
00:00:00,060 --> 00:00:06,220
So in this lecture, let's go ahead and start building PDF, CV generator.

2
00:00:06,689 --> 00:00:12,410
So the very first thing which I have done here is that I have created this particular folder called

3
00:00:12,630 --> 00:00:16,100
PDF, which will contain all of our project files.

4
00:00:16,710 --> 00:00:23,220
So let's open up the terminal and navigate down to that particular folder in order to create the Django

5
00:00:23,220 --> 00:00:23,770
project.

6
00:00:24,090 --> 00:00:29,940
So if you are already familiar with the steps of creating a Django project, then you can skip this

7
00:00:29,940 --> 00:00:30,290
part.

8
00:00:30,570 --> 00:00:33,840
But if you are not, then you can continue watching this lecture.

9
00:00:34,350 --> 00:00:38,430
So let's go to the next up and PDF.

10
00:00:38,880 --> 00:00:43,770
And in here, let's create a virtual environment, soil type and what shall.

11
00:00:46,130 --> 00:00:49,590
Envy and let's create the environment as envy.

12
00:00:50,510 --> 00:00:54,220
And now it's actually going to create the virtual environment for us.

13
00:00:56,600 --> 00:01:02,180
So now if I type in less, as you can see, we have the virtual environment, so now let's go ahead

14
00:01:02,180 --> 00:01:04,220
and activate that virtual environment.

15
00:01:04,670 --> 00:01:08,030
So I'll type in seed envy and then.

16
00:01:08,540 --> 00:01:11,380
So then slash activate.

17
00:01:11,880 --> 00:01:17,630
Now, if you're on windows, the steps to activate the virtual environment are a little bit different.

18
00:01:17,900 --> 00:01:23,060
So now once we have this virtual environment, let's go ahead and create the Jianguo project.

19
00:01:23,090 --> 00:01:27,050
So let me just go back to the directory, which is the PDF directly.

20
00:01:27,380 --> 00:01:35,330
And in here, first of all, you need to install Shango so type and Pip install Shango and then it's

21
00:01:35,330 --> 00:01:40,700
going to install Shango for us so that we can go ahead and create the Jianguo project.

22
00:01:40,850 --> 00:01:46,990
OK, so now as you can see, Django successfully installed and now you can go ahead and create each

23
00:01:47,150 --> 00:01:56,320
new project by typing in Django MASH Admin Start project and let's name our project as my site.

24
00:01:57,050 --> 00:02:01,880
And now let's go into the myside directory and create an app.

25
00:02:02,390 --> 00:02:08,130
So I'll go ahead and type in KDDI, my site, and now let's go ahead and create an appeal.

26
00:02:08,330 --> 00:02:18,110
So I'll say something like Django Basche admin start up and let's say we name this app as a PDF, so

27
00:02:18,110 --> 00:02:20,130
I'll simply type in PDF over here.

28
00:02:20,600 --> 00:02:26,210
OK, so now once we're done with that means that we have created the project as well as the app.

29
00:02:26,600 --> 00:02:31,610
Now let's open up a visual studio code and let's open up this particular project.

30
00:02:32,480 --> 00:02:34,610
So let's go to file open.

31
00:02:36,410 --> 00:02:42,560
Let's go to desktop and let's go to PDF.

32
00:02:42,560 --> 00:02:45,940
And we actually want to open the project, which is my site.

33
00:02:46,520 --> 00:02:48,320
So simply open that thing up.

34
00:02:48,680 --> 00:02:55,430
And now if we go over here in the PDF, as you can see, we have the entire project as well as app over

35
00:02:55,430 --> 00:02:55,730
here.

36
00:02:56,510 --> 00:03:02,630
Now, the very first thing which you always do after creating an app inside a project is that you go

37
00:03:02,630 --> 00:03:04,880
ahead, go into the settings, not by file.

38
00:03:05,390 --> 00:03:10,030
And in here, the very first thing which you do is that you include your app name here.

39
00:03:10,040 --> 00:03:14,390
So I'll type in PDF over here, give a comma and we are good to go.

40
00:03:15,140 --> 00:03:19,940
So now let's go ahead and try to run the server and let's see if that thing works.

41
00:03:20,300 --> 00:03:21,950
So I'll type in Python three.

42
00:03:24,380 --> 00:03:27,390
Managed by Ron Silver.

43
00:03:28,370 --> 00:03:33,170
And as you can see, the server is up and running as well, but the only thing which we need to do is

44
00:03:33,170 --> 00:03:38,750
that we need to type in python, manage to migrate to make a few migrations.

45
00:03:39,170 --> 00:03:46,550
So I'll stop the server and then I'll type in Python three managed up by.

46
00:03:48,560 --> 00:03:52,880
Migrate and hit enter, and it's going to make all the migrations for us.

47
00:03:53,490 --> 00:03:59,120
OK, so now once we are done with the basic project set up in the next lecture, we will go ahead and

48
00:03:59,120 --> 00:04:06,840
start creating the model for our app, which can accept the details which we want to put up on the screen.

49
00:04:06,950 --> 00:04:13,610
So, for example, we want to put up the name, email, phone number, the degrees and the school name,

50
00:04:13,610 --> 00:04:18,399
the university name, previous job roles, skills and everything like that.

51
00:04:18,620 --> 00:04:25,270
And first of all, we need to make sure that we accept these details from a person who's using our app.

52
00:04:25,760 --> 00:04:28,760
So in the next lecture will go ahead and create a model.

53
00:04:28,940 --> 00:04:30,860
So thank you very much for watching.

54
00:04:30,860 --> 00:04:32,840
And I'll see you guys next time.

55
00:04:33,140 --> 00:04:33,710
Thank you.

