1
00:00:00,180 --> 00:00:03,180
So now let's proceed ahead with our code right here.

2
00:00:03,660 --> 00:00:09,330
So in here we have checked if the card exist and if the card does not exist, we have created this particular

3
00:00:09,350 --> 00:00:10,950
Jason on object right here.

4
00:00:11,610 --> 00:00:18,300
However, one more thing which I would like to mention here is that this particular card, which we

5
00:00:18,300 --> 00:00:22,050
are mentioning over here, is nothing but the card in our local storage.

6
00:00:22,440 --> 00:00:26,190
And this variable right here, which we are creating as card, is nothing.

7
00:00:26,190 --> 00:00:31,720
But this is actually a JavaScript object, which we have created inside our code right here.

8
00:00:32,310 --> 00:00:37,020
So always make sure that you do get the difference between this card and this card right here.

9
00:00:37,170 --> 00:00:41,050
This card is actually the card from the local storage.

10
00:00:41,070 --> 00:00:45,090
And this card is actually the card which we will be using in our JavaScript code.

11
00:00:46,290 --> 00:00:53,040
So now how exactly do we go ahead and make sure that the values inside this and this are actually the

12
00:00:53,040 --> 00:00:55,140
same for the same purpose?

13
00:00:55,170 --> 00:01:01,800
We go ahead and get the actual values from the card and store those values up in this particular card

14
00:01:01,800 --> 00:01:02,520
variable.

15
00:01:02,610 --> 00:01:03,840
So how can we do that?

16
00:01:04,110 --> 00:01:06,240
We can do that by typing in else.

17
00:01:06,840 --> 00:01:15,810
So if the local storage not get item card is not equal to null means that there is some value in this

18
00:01:15,810 --> 00:01:18,120
particular card, which is the local storage.

19
00:01:18,120 --> 00:01:18,690
Scott.

20
00:01:19,170 --> 00:01:23,160
So now we want to get that value and we want to store that thing up over here.

21
00:01:23,550 --> 00:01:24,830
So how can we do that?

22
00:01:25,140 --> 00:01:34,290
We can simply go ahead and type got equals and I can see something like local storage, don't get item

23
00:01:34,470 --> 00:01:38,010
to get that particular item, which is nothing but the cup.

24
00:01:39,300 --> 00:01:44,700
Now it looks easy enough over here and this might make us think that the local storage card is actually

25
00:01:44,700 --> 00:01:46,760
stored in this particular card right here.

26
00:01:47,160 --> 00:01:53,940
But even before you do so, you first need to go ahead and pass that particular Gissen object in here.

27
00:01:54,030 --> 00:02:01,380
So what I exactly mean by passing, so passing over here means nothing but actually converting it into

28
00:02:01,380 --> 00:02:02,510
a suitable format.

29
00:02:03,030 --> 00:02:08,250
So in order to do that, instead of writing a whole bunch of this code right here, I'll just go ahead

30
00:02:08,250 --> 00:02:11,280
and just cut it for now, not deleted.

31
00:02:11,670 --> 00:02:15,150
And here I can say something like Jason Dot POS.

32
00:02:16,280 --> 00:02:23,780
And this is actually a function and in there, I actually need to go ahead and make a mention of biology

33
00:02:23,900 --> 00:02:27,110
and string so I can simply paste this over here.

34
00:02:27,350 --> 00:02:35,180
And once I do that, now we get the actual card, which is the local story, just got stored into our

35
00:02:35,180 --> 00:02:38,440
cart variable right here, which we have just created right now.

36
00:02:38,450 --> 00:02:42,020
And that actually completes the concretion logic in you.

37
00:02:42,830 --> 00:02:48,200
So now let's move ahead and learn about handling the button click of this add to cart button.

38
00:02:49,480 --> 00:02:54,530
So now in JavaScript we have something which is called as the document object model.

39
00:02:54,980 --> 00:03:01,640
And according to the document object model, what we can do is that we get access to this entire HTML

40
00:03:01,640 --> 00:03:02,190
document.

41
00:03:02,210 --> 00:03:07,730
So that means if you actually go ahead and again click on inspect, as you can see, we have a bunch

42
00:03:07,730 --> 00:03:15,260
of HTML code in here and this particular HTML code is nothing but entire e-mail document, which actually

43
00:03:15,260 --> 00:03:19,160
defines this particular Web page now with JavaScript.

44
00:03:19,340 --> 00:03:26,990
We now have access to all of this HTML document because of the JavaScript document object model and

45
00:03:26,990 --> 00:03:32,900
using the same document object model, we can actually go ahead access any of these elements in our

46
00:03:32,900 --> 00:03:33,950
JavaScript code.

47
00:03:34,430 --> 00:03:36,800
So how exactly can we go ahead and do that?

48
00:03:37,700 --> 00:03:40,810
So let's go ahead and learn how to do that.

49
00:03:40,850 --> 00:03:42,860
So let's go back to our code in here.

50
00:03:43,340 --> 00:03:49,640
And now we actually want to get access to the button, which is nothing but the add to cart button,

51
00:03:49,640 --> 00:03:51,060
which is this button right here.

52
00:03:51,710 --> 00:03:58,220
So in order to get access to a specific button, you actually need to use its class.

53
00:03:58,430 --> 00:04:04,730
So if you go ahead and click on inspect over here, as you can see, we have this class for this button

54
00:04:04,730 --> 00:04:06,740
as Betty Anne and Betty and warning.

55
00:04:07,220 --> 00:04:13,220
But let's go ahead and add another class to this button so that we can uniquely identify the add to

56
00:04:13,220 --> 00:04:13,900
cart button.

57
00:04:14,630 --> 00:04:17,660
So let's go back to our indexed HTML.

58
00:04:17,959 --> 00:04:23,170
And in here, let's actually go to the add to cart button.

59
00:04:23,180 --> 00:04:27,830
So I'll simply search for add to CART and this is our button right here.

60
00:04:28,190 --> 00:04:32,360
And let's add another class called Etsy to this particular button.

61
00:04:33,650 --> 00:04:35,590
And let's also do one thing.

62
00:04:35,600 --> 00:04:39,440
Let's actually replace this E with actual button.

63
00:04:43,360 --> 00:04:49,720
And once we go ahead and do that, we can now pretty much go ahead and access this button using JavaScript

64
00:04:49,720 --> 00:04:52,300
document object model using its glass.

65
00:04:52,990 --> 00:04:55,230
So let's learn how exactly can we do that?

66
00:04:55,240 --> 00:04:56,410
So we go ahead.

67
00:04:56,710 --> 00:04:57,640
Go right here.

68
00:04:58,420 --> 00:05:04,180
And in order to access the document, first of all, I'll type in Dollo and then I'll type in document,

69
00:05:04,750 --> 00:05:11,000
then I'll type in dot on, which means that we can perform any action on this document.

70
00:05:11,350 --> 00:05:15,000
So in here we actually want to capture the action, which is a click.

71
00:05:15,580 --> 00:05:23,710
So whenever something is clicked, then we give a comma and then we mention what kind of button do we

72
00:05:23,710 --> 00:05:24,920
want to handle the click.

73
00:05:24,940 --> 00:05:29,700
So we actually want to handle the click related to the button, which is nothing but add to cart.

74
00:05:30,100 --> 00:05:35,470
So in order to refer to that particular button, we go ahead and make use of the class, which we have

75
00:05:35,470 --> 00:05:36,150
just added.

76
00:05:36,160 --> 00:05:41,610
So we have added the EDC class to that particular button will mention ATC over here.

77
00:05:42,040 --> 00:05:47,920
And whenever you're mentioning a class over here, you always need to make sure that you have a dot

78
00:05:47,920 --> 00:05:49,070
before the class name.

79
00:05:49,570 --> 00:05:56,920
So this means that on the document we are actually handling the event, which is the click event, and

80
00:05:56,920 --> 00:05:59,850
we are handling it for a button whose classes ETSI.

81
00:05:59,860 --> 00:06:06,700
And now we want to mention that whenever that button is actually clicked, we want to perform the action.

82
00:06:06,910 --> 00:06:11,270
And the action in this case is nothing, but we want to execute a bunch of code in here.

83
00:06:11,920 --> 00:06:17,800
So in order to execute a bunch of code, we again give a comma and type in a function and we write in

84
00:06:17,800 --> 00:06:20,620
that bunch of code in this particular function right here.

85
00:06:20,740 --> 00:06:25,450
So this is the syntax for actually creating a function in JavaScript.

86
00:06:25,660 --> 00:06:31,030
So you type in function, then you have parentheses and then you have opening and closing curly brackets

87
00:06:31,030 --> 00:06:31,750
right over here.

88
00:06:31,870 --> 00:06:36,480
Now, in this particular thing, I can write in any bunch of code which I want.

89
00:06:36,850 --> 00:06:44,020
So in order to write in some code in here for testing purposes, I'll simply see console dot log and

90
00:06:44,020 --> 00:06:51,490
I'll say something like, uh, the, uh, two card button is clicked.

91
00:06:53,070 --> 00:06:56,740
So once we go ahead and do that, we are pretty much good to go.

92
00:06:57,180 --> 00:07:01,130
So now I can go ahead and I can simply save this code.

93
00:07:01,530 --> 00:07:07,860
And if we go to console and if we hit refresh and if I click on this thing, as you can see, it says

94
00:07:07,860 --> 00:07:09,470
the ATM card button is clicked.

95
00:07:10,020 --> 00:07:15,420
So when I click once more, as you can see again, this line is printed up over here to the console.

96
00:07:15,930 --> 00:07:19,640
So if I do this with any button right over here, I'll get that.

97
00:07:19,650 --> 00:07:21,260
The add to cart button is click.

98
00:07:21,690 --> 00:07:27,660
So which means that we have successfully handled the click associated with the add to cart button,

99
00:07:27,990 --> 00:07:31,660
but we don't know which button has been clicked.

100
00:07:31,680 --> 00:07:37,530
So for example, if I click on this button, it will still say the add to cart button is clicked.

101
00:07:37,530 --> 00:07:44,340
If I click this it will see the same thing, but we actually want to identify the idea associated with

102
00:07:44,340 --> 00:07:45,460
that particular button.

103
00:07:45,480 --> 00:07:52,980
So for example, when I click on Add to cart of this particular button, I should get the ID as the

104
00:07:52,980 --> 00:07:55,110
item is for this particular laptop.

105
00:07:55,410 --> 00:08:01,950
And when I click on add to Cart Button for the Swatch, I should actually get the idea of this particular

106
00:08:01,950 --> 00:08:04,260
watch and not this laptop right here.

107
00:08:04,950 --> 00:08:08,890
So how exactly can we go ahead and implement that logic over here?

108
00:08:09,180 --> 00:08:12,360
So we'll go ahead and learn about that in the upcoming lecture.

109
00:08:12,960 --> 00:08:16,890
So thank you very much for watching and I'll see you guys next time.

110
00:08:17,220 --> 00:08:17,760
Thank you.

