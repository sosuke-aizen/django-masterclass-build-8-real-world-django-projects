1
00:00:00,180 --> 00:00:07,170
So now our main job is to actually go ahead and get the idea of these specific buttons right over here.

2
00:00:07,860 --> 00:00:12,210
So how exactly can you go ahead and get the idea of these specific buttons?

3
00:00:12,970 --> 00:00:19,430
So if you remember, you might recall that how exactly did we get access to these specific buttons?

4
00:00:19,800 --> 00:00:25,590
So if we go to elements right over here and if we click on inspect, as you can see, we have added

5
00:00:25,590 --> 00:00:28,980
this ATC class to this particular button right here.

6
00:00:28,980 --> 00:00:34,610
And using the same ATC class, we actually got access to these particular buttons right here.

7
00:00:35,100 --> 00:00:43,470
But now as we have these classes, we also need to add something to our buttons so that we can identify

8
00:00:43,470 --> 00:00:47,490
these buttons, ID or the idea of these particular items.

9
00:00:47,670 --> 00:00:54,780
So what we essentially want to do is that we want to get the IDs of these particular items and we somehow

10
00:00:54,780 --> 00:00:57,750
need to assign those IDs to these buttons right here.

11
00:00:57,960 --> 00:00:59,780
So how exactly can we do that?

12
00:01:00,240 --> 00:01:05,980
So in order to understand how we can do that, let's jump into our code right over here.

13
00:01:06,780 --> 00:01:08,520
So let's go to our code.

14
00:01:08,520 --> 00:01:14,700
And in here, if we go to the index card, each of your is exactly where we have these buttons right

15
00:01:14,700 --> 00:01:14,950
here.

16
00:01:15,630 --> 00:01:20,230
So if you notice over here, we can go ahead and assign an I.D. to these buttons as well.

17
00:01:20,430 --> 00:01:26,970
And the simple way to do that is you can simply go ahead and type in product ID and that ID actually

18
00:01:26,970 --> 00:01:28,640
get associated with the button.

19
00:01:29,370 --> 00:01:30,960
So it's as simple as that.

20
00:01:31,090 --> 00:01:38,700
I can simply go ahead and create a attribute over here as ID and I'll sign the product ID in here by

21
00:01:38,700 --> 00:01:46,200
simply using the template syntax and then I can type in product dot I.D. It's actually that simple to

22
00:01:46,200 --> 00:01:46,740
do so.

23
00:01:46,920 --> 00:01:52,260
So now let's go back to our browser and let's see what kind of result do we get.

24
00:01:52,290 --> 00:01:58,290
So if I hit refresh now, as you can see, this button actually has an ID equal to one.

25
00:01:58,380 --> 00:02:05,310
So now if I actually go ahead, scroll down a little bit and if I inspect this particular element,

26
00:02:05,310 --> 00:02:09,479
as you can see, the button ID for this element is now equal to two.

27
00:02:09,750 --> 00:02:16,780
So now if I keep scrolling and if I check the idea of this button, as you can see now, the button,

28
00:02:16,780 --> 00:02:19,060
it is automatically assigned as full.

29
00:02:19,830 --> 00:02:21,690
So that's what we actually wanted.

30
00:02:21,840 --> 00:02:27,360
We actually wanted to assign a different ID for these individual buttons so that whenever we click these

31
00:02:27,360 --> 00:02:30,420
buttons, we can actually store these IDs.

32
00:02:30,960 --> 00:02:33,660
So let's go back to our code in here and in here.

33
00:02:34,110 --> 00:02:40,470
What we will do is that whenever a particular button is clicked, we will not only see that the buttons

34
00:02:40,470 --> 00:02:45,300
clicked, but we will also log the idea of that button to the console.

35
00:02:46,230 --> 00:02:52,560
So now here, in order to log the button to the console, first of all, will need to know how exactly

36
00:02:52,560 --> 00:02:54,480
can we extract the buttons.

37
00:02:56,070 --> 00:02:58,850
So here, let's write in the code to get the button.

38
00:02:59,910 --> 00:03:01,890
So I'll go ahead and create a variable.

39
00:03:02,550 --> 00:03:09,180
I'll type in that for creating a variable and I'll name this particular item and the school I.D. And

40
00:03:09,180 --> 00:03:17,070
now in order to get access to the ID, I can simply type in this dot I.D. and this is actually going

41
00:03:17,070 --> 00:03:19,030
to give us access to the ID.

42
00:03:19,410 --> 00:03:24,030
So this is actually a special keyword in JavaScript, which refers to the current object.

43
00:03:24,480 --> 00:03:28,300
And for the current object, we want to get the objects ID.

44
00:03:28,320 --> 00:03:35,460
So now once we have this ID will convert this idea into a string value by using the two string method.

45
00:03:35,550 --> 00:03:37,580
So I'll type in to string over here.

46
00:03:38,370 --> 00:03:44,470
So once we go ahead and do that, we are pretty much extracted the value for the item ID number.

47
00:03:44,510 --> 00:03:49,620
Simply log this item ID value onto the console and let's see what do we get.

48
00:03:49,800 --> 00:03:54,330
So I can see item the school ID and we are good to go.

49
00:03:54,660 --> 00:04:01,920
So now let's go back to our browser, hit refresh, go to console and let's click on this particular

50
00:04:01,920 --> 00:04:02,670
watch right here.

51
00:04:02,820 --> 00:04:07,340
So if I click on this watch, I guess we do have an issue here.

52
00:04:07,500 --> 00:04:10,440
It says console is not a function.

53
00:04:11,930 --> 00:04:20,089
OK, so I actually forgot to add Lagoa here, so once we go ahead, fix that hit refresh, let's actually

54
00:04:20,089 --> 00:04:21,130
click on this button.

55
00:04:21,529 --> 00:04:26,460
And as you can see, we got the idea for this particular watch right here, which is one.

56
00:04:27,140 --> 00:04:33,790
So now if I click on this spike and I click on Add to Cart, as you can see, I got the item value three

57
00:04:33,800 --> 00:04:34,370
over here.

58
00:04:35,930 --> 00:04:42,590
Now, if I click on this laptop bags add to cart, I get the values for, which means that now we are

59
00:04:42,590 --> 00:04:47,170
actually able to go ahead and get the individual values for these items in here.

60
00:04:48,240 --> 00:04:50,070
And that's what exactly we wanted.

61
00:04:50,610 --> 00:04:56,490
So in the next lecture, we can go ahead and instead of logging these values into the console, we can

62
00:04:56,490 --> 00:05:03,020
simply go ahead and stow these specific values inside this particular adjacent object.

63
00:05:03,340 --> 00:05:04,920
Thank you very much for watching.

64
00:05:04,920 --> 00:05:06,960
And I'll see you guys next time.

65
00:05:07,230 --> 00:05:07,800
Thank you.

