1
00:00:00,210 --> 00:00:07,080
So now let's go ahead and start building the Popolo, so the Poppo is going to be built in a way that

2
00:00:07,080 --> 00:00:12,540
whenever you actually go ahead and click on this particular card, you should get the items inside the

3
00:00:12,540 --> 00:00:13,250
card in there.

4
00:00:13,740 --> 00:00:20,840
So you as you will notice that we actually now want to handle the click of a particular item in here.

5
00:00:20,940 --> 00:00:24,510
And in order to handle a button, click in our website.

6
00:00:24,510 --> 00:00:30,870
We always have been using Chinguetti and JavaScript so we can actually go ahead and code this particular

7
00:00:30,870 --> 00:00:38,790
popover from scratch using Chigwedere and JavaScript, or you can use the boot straps and built in Popplewell,

8
00:00:38,790 --> 00:00:39,570
which we have.

9
00:00:39,750 --> 00:00:45,600
So in order to get that Popplewell, you can easily get that Popplewell from Boot Straps official website

10
00:00:45,600 --> 00:00:46,730
as a component.

11
00:00:47,130 --> 00:00:51,150
But even before that, I wanted to show you guys one little thing.

12
00:00:51,270 --> 00:00:57,060
So if I keep reducing the size of my window, as you can see, the navigation bar collapses because

13
00:00:57,060 --> 00:01:02,030
this is a bootstrap collapsible Nalpas and you will have a small looking button right here.

14
00:01:02,430 --> 00:01:07,410
But if I now click on this particular button, as you can see, nothing happens.

15
00:01:07,620 --> 00:01:14,160
And that's because we have not yet added the bootstrap JavaScript CDN link to our particular website.

16
00:01:14,400 --> 00:01:20,400
So if we actually go back to the code right here, as you can see, we have this bootstrap CDN link

17
00:01:20,400 --> 00:01:27,580
here, but this CVN link is actually for the access file itself and not for the actual JavaScript file.

18
00:01:28,110 --> 00:01:35,040
So in order to make this particular button functional, we need to add the bootstrap JavaScript CDN

19
00:01:35,040 --> 00:01:35,400
file.

20
00:01:35,490 --> 00:01:40,540
Now what does this button have got to do with the card popolo which we are building?

21
00:01:40,950 --> 00:01:47,550
So the card Popplewell, which we will be adding over here, also uses the same boot straps, JavaScript

22
00:01:47,560 --> 00:01:48,790
CDN file as well.

23
00:01:49,110 --> 00:01:54,840
So in order to make that Popplewell functional, you just need to go ahead and use that particular file.

24
00:01:55,080 --> 00:01:59,760
So now the question is, how exactly can you get that particular bootstrap file?

25
00:02:00,240 --> 00:02:05,670
So in order to get that, you simply need to search for download, bootstrap, then go to the bootstrap

26
00:02:05,670 --> 00:02:07,050
official website.

27
00:02:07,320 --> 00:02:13,140
And as you can see, if we scroll down a little bit and this was the case file link, which we had copied.

28
00:02:13,350 --> 00:02:16,800
Now we need to copy the link for the JavaScript as well.

29
00:02:16,930 --> 00:02:20,760
And we also need to copy this Poppo JS link as well.

30
00:02:21,000 --> 00:02:25,900
So I'll simply go ahead and copy the proper link from here.

31
00:02:26,220 --> 00:02:33,180
So let me just go ahead and copy this first show after copying that will go ahead and will not be easy.

32
00:02:33,180 --> 00:02:33,860
The door here.

33
00:02:34,020 --> 00:02:36,440
So remember that the sequence always matters.

34
00:02:36,810 --> 00:02:44,820
First, let's have the Bootstrap Citizens link, then let's have our link over here and then let's paste

35
00:02:44,820 --> 00:02:48,050
the proper dodgiest link over here.

36
00:02:48,660 --> 00:02:54,780
And once we have this specific link right at the bottom, we are going to add the bootstrap Syrian link,

37
00:02:54,780 --> 00:02:56,940
which is this link right here.

38
00:02:57,930 --> 00:03:02,130
So now let me copy this link and let's go back over here.

39
00:03:03,540 --> 00:03:06,590
And now we'll paste this link right over here.

40
00:03:08,950 --> 00:03:12,140
So once we go ahead and do that, we are pretty much good to go.

41
00:03:12,850 --> 00:03:19,600
So now if we go back to our code, hopefully this should work fine.

42
00:03:19,660 --> 00:03:26,800
So now if I just click on this, as you can see, this thing now becomes functional, which means that

43
00:03:26,800 --> 00:03:31,340
we have successfully enabled the boot straps, a JavaScript in here in our website.

44
00:03:31,930 --> 00:03:38,680
So now once we know how to do that, let's now search for the popular and add our popular in our website.

45
00:03:39,010 --> 00:03:43,660
So in order to add that Popplewell, let's simply go to boot straps official site.

46
00:03:44,080 --> 00:03:48,670
And if you scroll down a little bit, you will have this option with these components.

47
00:03:48,700 --> 00:03:52,110
So simply click on components right over here and now in here.

48
00:03:52,120 --> 00:03:53,170
Keep scrolling down.

49
00:03:53,920 --> 00:03:56,870
As you can see, we will get the pop always option here.

50
00:03:57,070 --> 00:03:58,570
So simply click on that.

51
00:03:59,170 --> 00:04:04,450
And in here, if you scroll down a little bit, you will get a popular idea.

52
00:04:04,480 --> 00:04:08,030
So if you click on this popover, you will get some content in here.

53
00:04:08,530 --> 00:04:13,900
What we actually want to Popplewell which displays text to the bottom when we click that particular

54
00:04:13,900 --> 00:04:14,510
Popplewell.

55
00:04:15,280 --> 00:04:20,200
So in order to do that, if we scroll down a little bit, as you can see, this is the Popplewell,

56
00:04:20,200 --> 00:04:21,310
which we are looking for.

57
00:04:21,430 --> 00:04:25,250
So when I click on it, as you can see, you get the content at the bottom.

58
00:04:26,050 --> 00:04:30,680
So let's simply copy this particular popover, which is this one right here.

59
00:04:31,570 --> 00:04:32,890
So let's copy that.

60
00:04:34,420 --> 00:04:41,200
Let's go back to our code, and instead of this particular link for the card, I'll just go ahead and

61
00:04:41,200 --> 00:04:47,650
replace this with a proposal and I'll assign this button as an ID equals.

62
00:04:48,730 --> 00:04:52,690
Correct, and also, let's change this thing to.

63
00:04:54,370 --> 00:04:55,960
God of zero as well.

64
00:04:56,620 --> 00:05:03,490
So now if I see this and if I go over here and hit refresh, as you can see, we have this button over

65
00:05:03,490 --> 00:05:03,810
here.

66
00:05:04,330 --> 00:05:11,380
But now if I click on this particular thing, it won't work because if you add a pop or you actually

67
00:05:11,380 --> 00:05:17,620
need to enable it, and in order to enable Popplewell, you actually need to go ahead, copy this code

68
00:05:17,620 --> 00:05:19,470
and pasted in your JavaScript.

69
00:05:19,930 --> 00:05:21,270
So let's copy that.

70
00:05:21,280 --> 00:05:27,330
And let's go back to our code and let's actually paste this thing over here.

71
00:05:29,170 --> 00:05:32,730
So when I go ahead and do that, they should pretty much work.

72
00:05:32,770 --> 00:05:38,800
So when I hit refresh and click on this, as you can see, our popcorn now worked and we have a nice

73
00:05:38,800 --> 00:05:40,040
looking over in here.

74
00:05:40,870 --> 00:05:46,960
So now, instead of having this random text in the popular, let's say you want to change this particular

75
00:05:46,960 --> 00:05:47,500
text.

76
00:05:47,710 --> 00:05:53,680
So in the next lecture, we will go ahead and learn how to change this particular text inside this particular

77
00:05:53,680 --> 00:05:54,290
bubble.

78
00:05:54,730 --> 00:05:56,650
So thank you very much for watching.

79
00:05:56,650 --> 00:05:58,570
And I'll see you guys next time.

80
00:05:58,780 --> 00:05:59,350
Thank you.

