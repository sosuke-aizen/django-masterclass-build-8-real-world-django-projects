1
00:00:00,060 --> 00:00:05,970
Hello and welcome to this lecture and from this lecture onwards will start building the add to cart

2
00:00:05,970 --> 00:00:07,950
functionality for our website.

3
00:00:08,580 --> 00:00:14,520
Now, before you went jumping into the code to create the add to cart functionality, I would like to

4
00:00:14,520 --> 00:00:19,800
explain you guys how exactly we are going to create the add to cart functionality.

5
00:00:20,400 --> 00:00:22,520
So ASPO the functionality.

6
00:00:22,920 --> 00:00:29,040
What should happen is that whenever you click on one of these buttons right here, that particular item

7
00:00:29,040 --> 00:00:31,160
should be added up into the card.

8
00:00:31,170 --> 00:00:37,230
So according to this functionality, what should happen is that whenever you click on one of these buttons

9
00:00:37,230 --> 00:00:42,000
right here, that particular item should be actually added up into the cart.

10
00:00:42,570 --> 00:00:44,510
So how exactly can we do that?

11
00:00:45,090 --> 00:00:52,080
So in order to add that particular functionality, we first need to understand what Chinguetti and we

12
00:00:52,080 --> 00:00:54,690
also need to learn a little bit of JavaScript.

13
00:00:55,200 --> 00:01:00,420
So if you don't already know about the query and JavaScript, you don't need to worry at all.

14
00:01:00,450 --> 00:01:04,230
I will actually go ahead and teach you guys along the way.

15
00:01:04,860 --> 00:01:11,160
So now let's imagine the scenario that you click on this particular button right here.

16
00:01:11,640 --> 00:01:17,870
So what we expect is we expect this particular item added to the card.

17
00:01:18,910 --> 00:01:25,080
Now, this makes sense in the actual word, but if you think about how the computer works or how this

18
00:01:25,080 --> 00:01:31,890
website works, what we essentially want to do is that whenever we click this particular button, we

19
00:01:31,890 --> 00:01:39,240
actually want to get the idea of this particular item, because obviously only then we can know that

20
00:01:39,240 --> 00:01:41,870
we can add that particular item to the card.

21
00:01:42,270 --> 00:01:48,780
So as a first step, just remember that whenever you click this button, you want to get the idea of

22
00:01:48,900 --> 00:01:50,880
this particular computer.

23
00:01:51,840 --> 00:01:59,220
Now, the next step after getting that ID is that you need to store this idea into some kind of a variable

24
00:01:59,700 --> 00:02:06,240
or you need to have some sort of storage in your browser itself, which will actually hold the idea

25
00:02:06,240 --> 00:02:07,560
of this particular item.

26
00:02:07,560 --> 00:02:13,280
Because obviously, when you go ahead and select this item, this items ID need to be stored.

27
00:02:13,440 --> 00:02:18,350
Then when you go ahead and when you click on this particular laptop bags add to cart button.

28
00:02:18,780 --> 00:02:22,300
We want to store the ID of this laptop bag as well.

29
00:02:22,770 --> 00:02:27,270
So we actually need a way to store elements onto your browser itself.

30
00:02:27,390 --> 00:02:33,330
So how exactly can we go ahead and do that so we can do that by using something which is called as a

31
00:02:33,330 --> 00:02:34,320
local storage?

32
00:02:34,950 --> 00:02:36,930
So in your browser, if you just.

33
00:02:36,930 --> 00:02:37,130
Right.

34
00:02:37,170 --> 00:02:42,540
Click over here and click on inspect, it will actually give you a developer console right here.

35
00:02:43,080 --> 00:02:49,260
So now if I go to console, yeah, I can actually go ahead and I can write in a bunch of JavaScript

36
00:02:49,260 --> 00:02:50,100
code in here.

37
00:02:50,520 --> 00:02:57,630
So as you can see, we do have some JavaScript errors in here, and that's probably because we have

38
00:02:57,630 --> 00:03:00,560
multiple bootstrap JavaScript clients in there.

39
00:03:00,570 --> 00:03:02,380
So we will actually ignore this for now.

40
00:03:02,820 --> 00:03:08,430
And now let's go ahead and learn about how we can store the items in the hallway here in the browser

41
00:03:08,430 --> 00:03:11,380
using something which is called as a local storage.

42
00:03:12,000 --> 00:03:18,180
So whenever you want to access local storage, you simply go ahead and type in local storage in here.

43
00:03:18,390 --> 00:03:24,960
So when I go head type that thing in and when I hit enter, as you can see, it says that the length

44
00:03:24,960 --> 00:03:30,340
of the local storage is zero and it has absolutely no items in the local storage.

45
00:03:30,360 --> 00:03:35,380
So let me actually zoom in a little bit so that you'll be able to see in a much more better way.

46
00:03:35,670 --> 00:03:38,910
So as you can see, currently, the local storage is empty.

47
00:03:38,910 --> 00:03:46,500
And now let's go ahead and try to create a card in this particular local store which can hold items.

48
00:03:47,010 --> 00:03:54,210
So in order to do that, I can simply type in local storage and we will use a method which is called

49
00:03:54,210 --> 00:04:03,690
as a set item, so I can type in local storage on set item and then I can set any item in this particular

50
00:04:03,690 --> 00:04:04,540
local storage.

51
00:04:05,010 --> 00:04:13,800
So as you can see, when I go over this thing, this is that the item which people store here are stored

52
00:04:13,800 --> 00:04:15,000
as a key value pair.

53
00:04:15,360 --> 00:04:18,750
So I hope you already know what a key value actually is.

54
00:04:19,170 --> 00:04:25,830
So key is nothing but the name of the value and the value is the actual value stored at that particular

55
00:04:25,830 --> 00:04:26,400
position.

56
00:04:26,790 --> 00:04:35,400
So if I say the key is going to be got and then I can give comma and I can save any kind of value in

57
00:04:35,400 --> 00:04:43,010
here so I can see one over here and now once I go ahead and hit enter, we get undefined over here.

58
00:04:43,410 --> 00:04:51,340
But after executing this line of code now in the local storage, the value of card is stored as one.

59
00:04:51,630 --> 00:04:58,170
So if you want to check what value do we have stored in the local storage, I can simply type in local

60
00:04:58,170 --> 00:04:59,790
storage in here and.

61
00:04:59,880 --> 00:05:08,100
I can use a method which is called ASKAP item, so I can type in local storage and get item and then

62
00:05:08,100 --> 00:05:14,410
I can simply passing the name over here or the key over here.

63
00:05:14,430 --> 00:05:17,820
So the key here is card so as to get the keys value.

64
00:05:17,820 --> 00:05:23,160
We type in the keys name here and when I go ahead hit and I get the value as one.

65
00:05:24,210 --> 00:05:30,570
So let's see, we again want to save some items in there so I can type in local storage that said item

66
00:05:30,810 --> 00:05:38,070
and then I can see something like name and I can said the name value to any value which I want.

67
00:05:38,460 --> 00:05:40,680
So let's say I want to save my name here.

68
00:05:40,830 --> 00:05:42,380
I'll type in my name here.

69
00:05:42,390 --> 00:05:49,920
And when I hit Enter, the value is going to be stored so I can retrieve this value by using local storage

70
00:05:49,920 --> 00:05:50,940
to get item.

71
00:05:51,300 --> 00:05:55,860
And now instead of God, if I type a name in here, I'll get my name.

72
00:05:56,760 --> 00:06:00,950
So it's that easy to store something in the local storage.

73
00:06:01,440 --> 00:06:07,500
So now that we know how local storage works, we now want to go ahead and save our items.

74
00:06:07,500 --> 00:06:11,200
Idy into this particular local storage, which is the card.

75
00:06:11,970 --> 00:06:17,870
So now if I type in local storage, as you can see, I get these values stored in there.

76
00:06:18,210 --> 00:06:21,600
So it says we have two items in our local storage.

77
00:06:21,930 --> 00:06:24,750
One is the card and the second one is the name.

78
00:06:24,750 --> 00:06:30,360
And we have a specific value for card and we have specific value for name over here as well.

79
00:06:30,570 --> 00:06:36,810
So in this manner, you can save any number of key value pairs inside this local storage.

80
00:06:37,260 --> 00:06:42,630
So now in order to clear up the local storage, I can simply type in local storage.

81
00:06:42,930 --> 00:06:44,490
DAUDT Clear.

82
00:06:46,280 --> 00:06:49,640
And when I do that thing, the local storage would be cleared.

83
00:06:49,670 --> 00:06:55,910
So now if I try to type in local storage, as you can see, there's absolutely nothing inside this local

84
00:06:55,910 --> 00:06:56,460
storage.

85
00:06:57,230 --> 00:07:03,890
So now once we know what local storage actually is in the upcoming lecture, we will go ahead and we'll

86
00:07:03,890 --> 00:07:10,370
write in some JavaScript code in our index dot html page, which is this piece right here, so that

87
00:07:10,370 --> 00:07:17,540
whenever we click this particular button, our code should automatically get the idea of that particular

88
00:07:17,540 --> 00:07:22,420
item and it should store that idea away here into the local storage.

89
00:07:23,300 --> 00:07:26,420
So we are going to learn how to do that in the next lecture.

90
00:07:26,630 --> 00:07:30,650
So thank you very much for watching and I'll see you guys next time.

91
00:07:30,980 --> 00:07:31,550
Thank you.

