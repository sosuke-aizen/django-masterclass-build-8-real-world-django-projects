1
00:00:01,470 --> 00:00:09,510
So in this lecture, let's start building the detailed view for webapp, so in order to build a new

2
00:00:09,510 --> 00:00:14,630
view or in order to build a completely new functionality, what you do is that you create a model,

3
00:00:14,640 --> 00:00:19,080
then you create a view, and then you create a template for that particular thing.

4
00:00:19,110 --> 00:00:24,510
So right now, we don't need to create a model because obviously we have the current model, which is

5
00:00:24,510 --> 00:00:29,310
the products, and we only have to create the view out of the same model, which is products.

6
00:00:29,880 --> 00:00:34,600
So the only thing which we need to do now is we need to create a new view.

7
00:00:34,980 --> 00:00:40,360
So let's go to the Vieuxtemps profile and create a view and call it as the detailed view.

8
00:00:40,420 --> 00:00:43,080
So I'll type in depth detail in here.

9
00:00:43,770 --> 00:00:47,450
And this view is going to accept the request as usual.

10
00:00:47,910 --> 00:00:51,900
And in here, this is also going to accept the idea.

11
00:00:52,350 --> 00:00:55,200
Now, why exactly do we need to pass an idea here?

12
00:00:55,710 --> 00:01:01,770
Because we want to design a detailed view in a way that whenever we click on one of these items in here,

13
00:01:02,160 --> 00:01:06,050
we should actually get the detail of only that particular item.

14
00:01:06,390 --> 00:01:11,690
That means in order to get the detail of a particular item, we would need its ID.

15
00:01:12,510 --> 00:01:15,750
So in order to get that idea, we get that idea from here.

16
00:01:16,590 --> 00:01:24,000
OK, so once we have that ID, we can now make use of this ID to actually find out that particular item.

17
00:01:24,450 --> 00:01:27,050
So how exactly are we going to find out that item?

18
00:01:27,180 --> 00:01:34,410
We are going to do that by using the products model so we can query the database using the Django Aurum

19
00:01:34,530 --> 00:01:37,590
and we will simply go ahead and type in product.

20
00:01:40,110 --> 00:01:46,250
And the school object, so this time you want a single object instead of objects, hence I have named

21
00:01:46,250 --> 00:01:53,570
this thing as object and we are going to get that from the model, which is products, not objects.

22
00:01:54,260 --> 00:01:58,770
And now instead of typing, not all, we actually want a single item in here.

23
00:01:58,850 --> 00:02:00,560
So instead I'll type and get.

24
00:02:00,860 --> 00:02:07,840
And here we only want to get the product whose idea is going to be equal to this idea right here.

25
00:02:08,000 --> 00:02:10,120
So I'll type in ID equals ID.

26
00:02:10,490 --> 00:02:17,930
That means we want a product whose ID is equal to this ID right over here, which we have passed in

27
00:02:17,930 --> 00:02:18,140
here.

28
00:02:18,920 --> 00:02:24,770
So now once we have that particular product, we can simply return that product and render that up with

29
00:02:24,770 --> 00:02:27,030
a template as simple as that.

30
00:02:27,260 --> 00:02:35,090
So I'll type in return, render first of all, passingly request the new person the template over here.

31
00:02:35,120 --> 00:02:37,760
So right now we don't have any sort of template in here.

32
00:02:38,240 --> 00:02:47,210
So I'll simply go ahead and type in shop slash and let's name this template as detailed HDMI and now

33
00:02:47,210 --> 00:02:50,570
simply pass in the context which is product object.

34
00:02:51,050 --> 00:02:52,100
So I'll type in.

35
00:02:53,470 --> 00:02:59,770
Product and the school object as product object.

36
00:03:01,040 --> 00:03:06,320
OK, so once we do that now, our next job is to actually set up this template, which is detailed,

37
00:03:06,320 --> 00:03:07,000
not HDMI.

38
00:03:07,820 --> 00:03:09,680
So now let's go to the.

39
00:03:12,140 --> 00:03:13,280
Templates folder.

40
00:03:13,310 --> 00:03:19,740
And in here, go to the shop and create a new file and a limit as detailed HTML.

41
00:03:20,300 --> 00:03:24,350
So once we have this template, let's go ahead and write in a bunch of code in here.

42
00:03:24,380 --> 00:03:30,920
So first of all, I'll go ahead and include the as usual code, which is the general tag the head again,

43
00:03:30,930 --> 00:03:31,620
the body tag.

44
00:03:32,240 --> 00:03:38,300
Then make sure that you have the bootstrap link over here so that your template actually adopts the

45
00:03:38,300 --> 00:03:39,290
bootstrap styling.

46
00:03:39,890 --> 00:03:44,930
So in actual projects, you are going to create a single static directory, which is going to hold all

47
00:03:44,930 --> 00:03:47,540
of your static files, including the bootstrap link.

48
00:03:47,780 --> 00:03:49,970
But we are going to make those changes later.

49
00:03:50,120 --> 00:03:57,020
So for now, you can simply go ahead and copy this bootstrap link from here and simply paste that up

50
00:03:57,020 --> 00:03:59,510
over here and the head tag.

51
00:03:59,930 --> 00:04:06,110
So this is not actually a good practice to repeat a bunch of code multiple times because that causes

52
00:04:06,110 --> 00:04:07,850
redundancy, which is not good.

53
00:04:08,660 --> 00:04:13,430
So we are going to actually develop a project once it's completed.

54
00:04:13,430 --> 00:04:18,260
And in there we will actually only have a single bootstrap link throughout the entire project.

55
00:04:18,290 --> 00:04:23,720
So now once we have this bootstrap link, we can now go ahead and start creating the products detailed

56
00:04:23,720 --> 00:04:24,350
view in here.

57
00:04:25,070 --> 00:04:31,790
So for the product detail, we what we wish to do is that if you go to the browser here for the detailed

58
00:04:31,790 --> 00:04:37,640
view, what we want to do is that we want to have the product image on the left side and right on the

59
00:04:37,640 --> 00:04:38,880
right side, on the top.

60
00:04:38,900 --> 00:04:40,840
We want to display the product title.

61
00:04:40,850 --> 00:04:43,120
Then we want to display its actual price.

62
00:04:43,490 --> 00:04:45,940
Then we want to display its discounted price.

63
00:04:45,950 --> 00:04:48,250
Then we want to display the product description.

64
00:04:48,620 --> 00:04:54,650
And in here we want to have some buttons like let's add to CART and have you want to have some other

65
00:04:54,650 --> 00:04:56,190
sort of button like by now.

66
00:04:56,660 --> 00:04:58,280
So let's go ahead and design that.

67
00:04:59,390 --> 00:05:06,410
So let's go back over here to our HTML code and in here, as usual, or with bootstrap, we first go

68
00:05:06,410 --> 00:05:07,720
ahead and create a container.

69
00:05:08,090 --> 00:05:16,340
So I'll type in the glass container and then I'll go ahead and create a room.

70
00:05:16,640 --> 00:05:20,300
So I'll type in div class equal through.

71
00:05:21,110 --> 00:05:23,570
And then inside this role we will create a column.

72
00:05:23,570 --> 00:05:27,300
So I'll again type and div class equals column.

73
00:05:28,220 --> 00:05:31,310
So this is actually going to span six columns.

74
00:05:31,310 --> 00:05:38,390
So I'll type in called Mad Dash six because obviously out of the 12 columns in the first six columns,

75
00:05:38,390 --> 00:05:40,520
we only want to display the product's image.

76
00:05:40,910 --> 00:05:44,760
So now let's go ahead and get access to the products image in here.

77
00:05:45,770 --> 00:05:51,050
So again, in this particular column, I'm actually going to create or draw one more time.

78
00:05:51,590 --> 00:05:52,790
So I'll type in Dave.

79
00:05:54,940 --> 00:05:58,960
That's going to have a class of rule and India.

80
00:05:58,990 --> 00:06:05,310
I am going to have the column again, elegant, open live class and in here I can see something like

81
00:06:05,310 --> 00:06:12,910
called Dash and Dash 12, which means that this image spans all the 12 columns inside this room, which

82
00:06:12,910 --> 00:06:14,140
span six columns.

83
00:06:14,680 --> 00:06:17,440
So in here now, let's go ahead and include an image.

84
00:06:17,440 --> 00:06:19,000
So I'll use the image tag.

85
00:06:19,120 --> 00:06:24,760
And the source for this image is going to be from the product object right here.

86
00:06:24,820 --> 00:06:30,520
So I can simply use the template syntax in here and I can see product and Lesco object.

87
00:06:30,520 --> 00:06:33,010
And in the product object, we actually have the image.

88
00:06:33,010 --> 00:06:36,380
You are all stored in the image field, so I'll type an image right here.

89
00:06:36,790 --> 00:06:38,620
So I hope this makes sense to you.

90
00:06:39,280 --> 00:06:46,180
And in here you can also go ahead and have the style for this particular image so you can set the height

91
00:06:46,180 --> 00:06:47,020
and weight for this.

92
00:06:47,380 --> 00:06:53,470
But for now, I'm just going to leave it empty and let's get rid of this all the time because we don't

93
00:06:53,470 --> 00:06:54,310
need it right now.

94
00:06:54,910 --> 00:06:58,030
So once we go ahead and do that, we are pretty much good to go.

95
00:06:58,750 --> 00:07:04,330
So now let's also go ahead and create the right hand side part, which is to display the product title,

96
00:07:04,330 --> 00:07:06,350
the image price and everything like that.

97
00:07:06,910 --> 00:07:13,660
So here we have this particular role and in the same role, we are going to go in the next six columns,

98
00:07:13,660 --> 00:07:14,320
which we have.

99
00:07:14,320 --> 00:07:16,960
So we have utilized the first six one right over here.

100
00:07:17,380 --> 00:07:20,750
Now, let's do the same thing for the next six as well.

101
00:07:20,770 --> 00:07:23,200
So for that, let's go over here and let's step.

102
00:07:23,200 --> 00:07:27,700
And this is going to be class called Dash and Dash six.

103
00:07:28,480 --> 00:07:31,090
And in the let's go ahead and create a row.

104
00:07:31,280 --> 00:07:34,390
So div class is going to be low.

105
00:07:35,200 --> 00:07:41,410
And then over here, let's go ahead and again make a column which spans all the columns as we have done

106
00:07:41,410 --> 00:07:41,680
here.

107
00:07:41,980 --> 00:07:47,240
So again, dipen div class equals call dash mely dash 12.

108
00:07:48,070 --> 00:07:51,470
And in here, Allfirst, go ahead and mention the product title.

109
00:07:51,490 --> 00:07:56,910
So in here I can type in product and underscore object dot title.

110
00:07:58,180 --> 00:08:00,700
So this is the same thing which we have done right here.

111
00:08:01,090 --> 00:08:07,330
Just as we have access the product image here, we have access the product title and now after accessing

112
00:08:07,330 --> 00:08:11,800
the product title, I can now go ahead and access the product's price over here.

113
00:08:12,220 --> 00:08:18,580
So for that very purpose, I can simply go ahead and recreate this particular rule down right here so

114
00:08:18,580 --> 00:08:21,550
I can go below right here, piece this thing over here.

115
00:08:21,550 --> 00:08:23,800
And instead of the title, I can see price.

116
00:08:25,290 --> 00:08:31,710
And this rule actually ends here, and after that, I can create another rule and I can display the

117
00:08:31,710 --> 00:08:35,669
discounted price here, so I will again go ahead and pass the same thing in here.

118
00:08:36,120 --> 00:08:39,030
And I will say something like product object.

119
00:08:39,299 --> 00:08:44,039
And in order to access the discounted price, I'll use the discount price over here.

120
00:08:44,640 --> 00:08:47,860
So I'll go ahead based discount pricing here.

121
00:08:48,300 --> 00:08:51,640
So once we go ahead and do that, we are pretty much good to go.

122
00:08:51,720 --> 00:08:55,840
Now, the only thing which remains is that we want to display the description.

123
00:08:56,460 --> 00:08:57,570
So let's go ahead.

124
00:08:57,840 --> 00:09:05,430
Copy this once again, pasted up over here and now instead of the price, I can simply go ahead and

125
00:09:05,430 --> 00:09:06,810
access the description.

126
00:09:06,820 --> 00:09:08,930
So I see description over here.

127
00:09:10,030 --> 00:09:12,610
And once we are done with that, we are good to go.

128
00:09:13,240 --> 00:09:19,030
So now once we have this particular template design, we can actually go ahead and add in a bunch of

129
00:09:19,030 --> 00:09:20,090
more elements in here.

130
00:09:20,590 --> 00:09:24,990
But even before that, let's go ahead and see how this template exactly looks like.

131
00:09:25,480 --> 00:09:30,730
So in order to go ahead and make this template functional, we need to create the URL patterns for this

132
00:09:30,730 --> 00:09:31,600
particular view.

133
00:09:32,200 --> 00:09:34,810
So we are going to do that in the next lecture.

134
00:09:35,020 --> 00:09:38,980
So thank you very much for watching and I'll see you guys next time.

135
00:09:39,250 --> 00:09:39,820
Thank you.

