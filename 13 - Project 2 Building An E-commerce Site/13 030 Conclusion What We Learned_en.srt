1
00:00:00,120 --> 00:00:04,420
So congratulations on finally making it to the end of the e-commerce section.

2
00:00:04,740 --> 00:00:10,230
I hope you guys had great fun building the e-commerce Web site and I hope you gained a lot of skills

3
00:00:10,230 --> 00:00:10,770
doing so.

4
00:00:11,460 --> 00:00:15,670
So now let's go through what exactly we have learned in this specific section.

5
00:00:15,780 --> 00:00:20,210
So these are the major things which we have learned while building our e-commerce site.

6
00:00:20,760 --> 00:00:25,600
So first of all, we have learned how to use local storage to actually create account.

7
00:00:26,070 --> 00:00:31,740
So in many cases, you would actually need to go ahead and use the browser storage capabilities to actually

8
00:00:31,740 --> 00:00:34,130
store some data for your general web app.

9
00:00:34,410 --> 00:00:36,720
And you can do the same using local storage.

10
00:00:36,960 --> 00:00:40,320
And this is what exactly we have done in our e-commerce website.

11
00:00:40,320 --> 00:00:45,870
That is, we actually went ahead, learned about local storage and stored our card items in there even

12
00:00:45,870 --> 00:00:48,690
before actually entering them into our database.

13
00:00:49,740 --> 00:00:55,140
Then the next most important thing, which we have learned is how exactly to integrate JavaScript and

14
00:00:55,140 --> 00:00:57,760
Shango code or more specifically, Python code.

15
00:00:58,230 --> 00:01:04,200
So we have learned how JavaScript and Python can actually work together to help you build a complete

16
00:01:04,200 --> 00:01:09,810
full stack application wherein you actually go ahead, make use of the data from the front end and actually

17
00:01:09,810 --> 00:01:14,650
using the backend and also get the data from the back end and kind of displayed on the front end.

18
00:01:15,030 --> 00:01:21,420
So we have actually made the entire card functionality solely depending upon JavaScript Shango and a

19
00:01:21,420 --> 00:01:23,000
little bit of Chinguetti as well.

20
00:01:24,660 --> 00:01:30,780
So we have also learned how to post from data directly to the database, so we have not made use of

21
00:01:30,780 --> 00:01:33,820
any kind of Django forms or anything like that over here.

22
00:01:33,840 --> 00:01:40,200
So we just directly went ahead, use the post method and we have used the post method by actually collecting

23
00:01:40,200 --> 00:01:45,660
all the data in a general view and actually using the safe method to actually save objects.

24
00:01:46,080 --> 00:01:51,150
So this is one of the more simple approach which you can take to actually collect data from the user

25
00:01:51,150 --> 00:01:52,830
and store it to the back end.

26
00:01:53,070 --> 00:01:57,510
And you can use this for creating any sort of website which you want in Django.

27
00:01:58,610 --> 00:02:02,780
We have also learned how to use Ceccarelli to handle front end user actions.

28
00:02:03,170 --> 00:02:08,210
Now, I know that security is not the most suitable option and this is it.

29
00:02:08,419 --> 00:02:13,820
But if you are building some simple Web applications, you can actually go ahead and make use of equerry

30
00:02:13,820 --> 00:02:19,260
because it's a very lightweight library and it can perform almost everything which you need to.

31
00:02:19,330 --> 00:02:26,330
Now, I'm aware that you can use some advanced frameworks like Angular or Bugis to actually perform

32
00:02:26,330 --> 00:02:29,950
the same task, but she really gets the job done as well.

33
00:02:29,960 --> 00:02:31,370
And four simple sites.

34
00:02:31,370 --> 00:02:35,670
You can actually go ahead to check ready and handle different end user actions.

35
00:02:35,690 --> 00:02:42,320
So we have also used quite a lot in order to go ahead and basically work with the Python and JavaScript

36
00:02:42,320 --> 00:02:42,640
code.

37
00:02:43,550 --> 00:02:45,610
So that's the end of the section.

38
00:02:45,620 --> 00:02:48,560
And I hope you guys enjoyed making the e-commerce site.

39
00:02:48,860 --> 00:02:54,110
So if you have faced any kind of doubts or difficulties along the way, feel free to post in the Q&A

40
00:02:54,110 --> 00:02:56,570
section and I will be there to help you out.

41
00:02:56,780 --> 00:02:57,350
Thank you.

