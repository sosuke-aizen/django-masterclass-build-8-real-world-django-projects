1
00:00:00,210 --> 00:00:05,450
So now let's go ahead and start building a page to display all these products on our site.

2
00:00:06,060 --> 00:00:11,130
So the very first thing which we need to always do with Shango is that we need to create a model, that

3
00:00:11,130 --> 00:00:14,130
we need to create a view, and then we need to create a template.

4
00:00:14,130 --> 00:00:17,760
Because Django follows the MBT added actual style.

5
00:00:18,420 --> 00:00:20,360
So we already have the model.

6
00:00:20,370 --> 00:00:22,320
We have the data in the model as well.

7
00:00:22,380 --> 00:00:24,550
Now let's start working on our views.

8
00:00:25,170 --> 00:00:30,720
So in this particular view, first of all, we will go ahead and import our model because we obviously

9
00:00:30,720 --> 00:00:39,690
want to use data from this products model, soil type and from DOT models, import products.

10
00:00:40,230 --> 00:00:45,420
So once we have imported the products, we can now go ahead and create a view for this.

11
00:00:45,690 --> 00:00:50,250
So we want to display all the products on our homepage.

12
00:00:50,250 --> 00:00:54,060
So we'll create a view in here and call that view as index.

13
00:00:54,060 --> 00:00:55,730
So I'll type in dev index.

14
00:00:56,250 --> 00:00:58,590
It's going to accept a request, as usual.

15
00:01:00,300 --> 00:01:07,950
And then I can simply go ahead and retrieve all the products by typing in products, dot objects, dot

16
00:01:07,950 --> 00:01:11,580
all, and we are going to save that in the product.

17
00:01:13,120 --> 00:01:18,610
Objects or product objects is going to be equal to products, DOT.

18
00:01:19,700 --> 00:01:21,450
Objects, not all.

19
00:01:21,890 --> 00:01:27,650
And this is going to give us all the products now, we simply need to pass these product objects as

20
00:01:27,650 --> 00:01:35,540
the context, along with the template to render function so I can type and return, render passing the

21
00:01:35,540 --> 00:01:38,600
request first, then you need to pass in the template.

22
00:01:38,630 --> 00:01:43,580
So right now, we don't have any sort of template in here, but we are going to create that as soon

23
00:01:43,580 --> 00:01:45,200
as we are done designing the CPU.

24
00:01:45,780 --> 00:01:49,850
So the Apne in case is shop.

25
00:01:50,270 --> 00:01:53,490
So I'll type in shop slash and like start html.

26
00:01:53,870 --> 00:01:58,820
So we need to create a template with indexed HTML as its name.

27
00:01:59,120 --> 00:02:07,970
And now let's pass in the actual context, which is nothing but product and the school objects.

28
00:02:09,570 --> 00:02:11,670
As product objects.

29
00:02:13,610 --> 00:02:16,610
So once we are done with this, we are pretty much good to go.

30
00:02:17,150 --> 00:02:20,270
Now let's go ahead and create the template in here.

31
00:02:20,780 --> 00:02:24,440
So let's go to the app, which is shop.

32
00:02:24,900 --> 00:02:28,760
Let's create a new folder called it as Templates.

33
00:02:29,360 --> 00:02:37,610
And in the let's go ahead and create another folder, call it a shop and let's create the actual template

34
00:02:37,610 --> 00:02:44,090
in here and start HDMI so we can retrieve all these products in here by designing this particular HTML

35
00:02:44,090 --> 00:02:44,490
page.

36
00:02:44,810 --> 00:02:49,040
But even before that, let's create a dual pattern for this particular view.

37
00:02:49,400 --> 00:02:56,900
So in order to set up the URL pattern for this view, you can go to the supply of your site, which

38
00:02:56,900 --> 00:03:00,460
is this thing right here and here, and put the views at first.

39
00:03:00,470 --> 00:03:02,240
So we'll type in from shop.

40
00:03:03,410 --> 00:03:11,900
We need to import views because we want to associate you all with the view, which we have just created,

41
00:03:11,900 --> 00:03:13,010
which is the index for you.

42
00:03:13,670 --> 00:03:20,420
So now this is going to be part and as we are designing the index page, so we don't need a specific

43
00:03:20,420 --> 00:03:21,230
you are here.

44
00:03:21,230 --> 00:03:23,420
We just want to displayed on the homepage.

45
00:03:23,420 --> 00:03:29,600
And henceforth we are going to keep this thing as empty and now it's going to be you start index, which

46
00:03:29,600 --> 00:03:30,260
is our view.

47
00:03:30,410 --> 00:03:34,010
And let's see, the name of this view is also going to be index.

48
00:03:34,420 --> 00:03:37,290
So once we are done with this, we are pretty much good to go.

49
00:03:37,670 --> 00:03:43,040
Now we come back and actually retrieve the objects in here from product objects.

50
00:03:43,610 --> 00:03:45,590
And I guess I have misspelled this.

51
00:03:46,220 --> 00:03:47,620
So let me just fix that.

52
00:03:48,230 --> 00:03:50,000
And now let's go ahead.

53
00:03:50,170 --> 00:03:57,080
Go to the next HTML and in here in order to retrieve the products you type in.

54
00:03:59,190 --> 00:04:00,300
For product.

55
00:04:03,390 --> 00:04:03,900
In.

56
00:04:05,580 --> 00:04:14,370
Product underscore objects, which is nothing but this context right here, and we and the for loop

57
00:04:14,370 --> 00:04:22,200
in here by typing in and fall and now you can simply go ahead and display the product information in

58
00:04:22,200 --> 00:04:29,490
here by typing in product, not title product, not price product, not discount price, so on and so

59
00:04:29,490 --> 00:04:29,850
forth.

60
00:04:30,360 --> 00:04:37,260
So for now, just let's try to go ahead and display the product title and let's see if we get all the

61
00:04:37,260 --> 00:04:39,720
products from our database just to test.

62
00:04:40,350 --> 00:04:45,330
So now let's open up our Web browser and let's visit the index page.

63
00:04:46,720 --> 00:04:52,780
And as you can see, we have the product names over here, which is X, Y, Z, you watch EBC laptop

64
00:04:52,780 --> 00:04:56,900
and a bike, so which means everything is working just fine.

65
00:04:57,310 --> 00:05:01,750
Now, you obviously don't want to display the products in this particular fashion that is.

66
00:05:01,900 --> 00:05:07,040
Now you need some sort of styling in here to make your e-commerce site look much more attractive.

67
00:05:07,450 --> 00:05:13,840
So let's go ahead and fix that in the upcoming lecture by using bootstrap and using some and column

68
00:05:13,840 --> 00:05:17,350
classes to align all these items in a proper fashion.

69
00:05:17,710 --> 00:05:19,630
So thank you very much for watching.

70
00:05:19,630 --> 00:05:21,550
And I'll see you guys next time.

71
00:05:21,850 --> 00:05:22,420
Thank you.

