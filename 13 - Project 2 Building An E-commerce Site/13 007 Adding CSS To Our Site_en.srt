1
00:00:00,180 --> 00:00:05,600
So now let's go ahead and try to add some styling to our particular Web page.

2
00:00:05,640 --> 00:00:11,970
So as you can see, I've added a new product in here, which is a laptop bag, and I have also fixed

3
00:00:11,970 --> 00:00:14,520
the image for this particular bike right here.

4
00:00:14,940 --> 00:00:18,200
So our layout now looks pretty much consistent.

5
00:00:18,210 --> 00:00:25,110
But the thing is still, if you actually have a look at the layout, we still have a few more changes

6
00:00:25,110 --> 00:00:25,570
to do.

7
00:00:25,590 --> 00:00:32,400
So, for example, let's say if you want to display the title of the product or the name of the product,

8
00:00:32,970 --> 00:00:35,490
a little bit bigger as compared to the price.

9
00:00:35,650 --> 00:00:40,860
So we can make these changes by changing the contents of the user's file.

10
00:00:41,310 --> 00:00:45,000
So right now, this particular template does not have any Sears's file.

11
00:00:45,300 --> 00:00:49,860
So let's go ahead and create one using the static folder.

12
00:00:50,130 --> 00:00:57,120
So first of all, whenever we actually want to create a user's file for our template, the as usual

13
00:00:57,120 --> 00:01:00,880
thing which we do is that we add the static folder.

14
00:01:01,470 --> 00:01:06,170
So let's go into the app, which is shop, and let's create a new folder.

15
00:01:06,510 --> 00:01:09,540
So I'll call that folder as static.

16
00:01:11,880 --> 00:01:18,180
And once we have this static folder in here, we can go ahead and again create a folder with the name

17
00:01:18,180 --> 00:01:20,860
of the app, so I'll type in shop in here.

18
00:01:22,410 --> 00:01:27,250
So once we have this folder, we can now go ahead and add static files to it.

19
00:01:27,600 --> 00:01:32,430
So let's go ahead and create a new file and a limitless style.

20
00:01:32,440 --> 00:01:33,410
DOCSIS.

21
00:01:34,200 --> 00:01:40,380
OK, so once we have the static file, let's now link this static file or these dialog.

22
00:01:40,380 --> 00:01:44,890
Sears's file do a template which is index dot HDMI.

23
00:01:44,940 --> 00:01:51,840
So now the very first thing which we do is that we go right after the bootstrap time ends, which is

24
00:01:51,840 --> 00:01:54,300
right here in the head tag of the HTML.

25
00:01:54,840 --> 00:01:57,350
We simply go ahead and type in link.

26
00:01:57,360 --> 00:02:02,970
Rel equals stylesheet and the each ref is going to be the link of the static file.

27
00:02:03,600 --> 00:02:07,770
Now you cannot directly go ahead and mention the lingo here.

28
00:02:07,770 --> 00:02:10,229
First of all, you need to mention static in here.

29
00:02:10,780 --> 00:02:17,850
So we will use the template syntax and first of all, will type in static, meaning that we would see

30
00:02:18,210 --> 00:02:24,360
this HTML document that it would need to first find the path of the static files for this particular

31
00:02:24,360 --> 00:02:24,900
project.

32
00:02:25,410 --> 00:02:32,220
And once it does so, it then needs to go ahead and go to shop slash.

33
00:02:33,500 --> 00:02:40,280
Style DOCSIS, as you can see now, we are pretty much into that particular path, which means that

34
00:02:40,280 --> 00:02:44,140
US users file is now linked to the HTML template.

35
00:02:44,690 --> 00:02:48,990
Now in order to check if this thing is actually attached in a proper manner.

36
00:02:49,160 --> 00:02:55,580
We go to these style boxes and let's assign a background color to the body and let's see if the background

37
00:02:55,580 --> 00:02:56,960
color actually changes.

38
00:02:57,590 --> 00:02:59,030
So the background.

39
00:03:00,540 --> 00:03:04,020
Dash color is going to be, let's say, red.

40
00:03:05,740 --> 00:03:13,930
And now if we go back here and hit refresh, as you can see, we are going to get an error because whenever

41
00:03:13,930 --> 00:03:20,260
you go ahead and create a static file, you actually need to mention a tag in here, which is load static

42
00:03:20,260 --> 00:03:20,760
files.

43
00:03:21,370 --> 00:03:24,710
So in order to add that tag up here, let's go ahead.

44
00:03:24,730 --> 00:03:27,280
Let's go right about to the first line.

45
00:03:27,730 --> 00:03:32,730
And in here, you need to type in load static files.

46
00:03:33,580 --> 00:03:37,340
So once we make the change, hopefully we don't have any error this time.

47
00:03:37,390 --> 00:03:42,730
So when I go ahead and hit refresh, as you can see, we don't have any error as of now.

48
00:03:43,060 --> 00:03:47,800
But as you can see, we don't actually have the background color as well.

49
00:03:47,830 --> 00:03:50,890
So let me just go ahead and see what went wrong.

50
00:03:50,920 --> 00:03:58,270
OK, so when I went ahead and when I actually restarted my soul, the static file actually loaded up.

51
00:03:58,570 --> 00:04:04,630
And many times what happens is that whenever you make changes to the static files, you may need to

52
00:04:04,630 --> 00:04:07,940
restart the server to properly load that static file.

53
00:04:08,080 --> 00:04:12,590
So as you can see, we now have the background red and we are not going to keep it that way.

54
00:04:12,970 --> 00:04:18,399
This is just to test if our static file or the access file is attached to a template.

55
00:04:19,000 --> 00:04:22,750
So now once we know that it works, I'll go ahead, get rid of this.

56
00:04:23,140 --> 00:04:27,570
And now let's come down and start actually styling our Web page.

57
00:04:28,120 --> 00:04:34,240
So the very first thing which we will do here is that we will actually go ahead said the color of the

58
00:04:34,240 --> 00:04:35,380
price over here.

59
00:04:35,530 --> 00:04:40,730
So as you can see right now, the price is actually having a regular blackish color.

60
00:04:40,870 --> 00:04:42,720
So let's go ahead and make it green.

61
00:04:43,360 --> 00:04:47,410
So I'll go ahead and first of all, get access to the price.

62
00:04:47,860 --> 00:04:52,980
So the price is actually present over here and it has a class of context.

63
00:04:52,990 --> 00:04:57,820
So let's use the class selector and actually style up the product price.

64
00:04:58,300 --> 00:05:00,280
So I'll type in DOT.

65
00:05:01,560 --> 00:05:03,960
God dash text.

66
00:05:06,580 --> 00:05:14,350
And now, once we have selected this particular class, I can now go ahead and add the color to green.

67
00:05:14,530 --> 00:05:19,100
So as you can see, when we hit refresh, we have this in the greenish color.

68
00:05:19,570 --> 00:05:26,390
And now let's also set the font with so as to make the text of these prices a little bit bolder.

69
00:05:26,620 --> 00:05:34,490
So I'll go ahead and type in font weight and let's a different way to, let's say eight hundred.

70
00:05:35,230 --> 00:05:37,030
Let's see how that thing looks like.

71
00:05:37,060 --> 00:05:42,670
So when I go ahead and hit refresh, as you can see, the font looks a little bit bigger and better.

72
00:05:43,870 --> 00:05:51,130
So now let's also set a little bit of font way to the product title as well so I can go ahead and select

73
00:05:51,130 --> 00:06:02,130
the product title on the basis of the car title plus so I can go ahead, copy that, use the class selector

74
00:06:02,920 --> 00:06:07,180
and in here, just step in front of it to be, let's see, 900.

75
00:06:08,510 --> 00:06:15,440
So once we make that change and hit refresh, as you can see now, we have this thing look more bold.

76
00:06:15,530 --> 00:06:20,300
And one more thing which you could do here is that you can add a dollar sign in here right before the

77
00:06:20,300 --> 00:06:20,780
price.

78
00:06:20,900 --> 00:06:27,440
So in order to do that, you can simply go here and add a dollar sign right before the product price

79
00:06:27,590 --> 00:06:29,030
and you special.

80
00:06:29,240 --> 00:06:34,830
So now if we switch back and hit refresh, as you can see, our site actually looks quite better.

81
00:06:34,850 --> 00:06:36,410
So that's it for this lecture.

82
00:06:36,800 --> 00:06:42,620
And in the next lecture, we will go ahead and add these search functionality for our site so that we

83
00:06:42,620 --> 00:06:47,480
can go ahead and make a search for finding any particular product in here.

84
00:06:48,140 --> 00:06:50,180
So thank you very much for watching.

85
00:06:50,180 --> 00:06:52,190
And I'll see you guys next time.

86
00:06:52,490 --> 00:06:53,000
Thank you.

