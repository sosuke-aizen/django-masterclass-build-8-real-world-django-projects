1
00:00:00,690 --> 00:00:06,250
So in this lecture, we will go ahead and learn how to add pagination to website.

2
00:00:06,750 --> 00:00:12,480
So right now, if you can see we have these items in here and I've added this additional item in here.

3
00:00:12,990 --> 00:00:18,510
So what you can do is that you can go ahead and add as many items as you want over here.

4
00:00:18,990 --> 00:00:26,100
So for pagination, what we wish to do is that instead of having all the items listed on a single page,

5
00:00:26,100 --> 00:00:30,750
let's say we want to limit the number of items which we display on a single page.

6
00:00:31,380 --> 00:00:34,080
So right now, we don't have much items here.

7
00:00:34,080 --> 00:00:37,920
So we actually add pagination for only four items.

8
00:00:37,930 --> 00:00:41,380
But you can simply change that by changing a single number.

9
00:00:41,430 --> 00:00:46,650
So let's go ahead and see how exactly we can design pagination for this particular website.

10
00:00:47,010 --> 00:00:54,030
So the very first thing which you need to do here is you need to go ahead, go into The View, which

11
00:00:54,030 --> 00:00:58,680
is the index view, and then here once you implement the search functionality.

12
00:00:58,710 --> 00:01:01,390
So right now, you have these search functionality in here.

13
00:01:01,980 --> 00:01:07,950
So whenever you implement and actually finish off the search functionality, then you actually need

14
00:01:07,950 --> 00:01:12,360
to go ahead and have the pagination code right below the search code.

15
00:01:12,900 --> 00:01:19,650
And the reason why you want to have the pagination code below the search code is obviously because,

16
00:01:20,010 --> 00:01:26,000
first of all, you make a search, then you get a list of product which matches up your search criteria.

17
00:01:26,130 --> 00:01:32,580
And when that match happens, you now have a bunch of products in your product, objects, and then

18
00:01:32,580 --> 00:01:38,250
you actually want to pazhani to display those products on the first page.

19
00:01:39,120 --> 00:01:42,480
So now let's go ahead and see how exactly can we implement this.

20
00:01:43,200 --> 00:01:49,130
So what you can do here is that, first of all, you need to go ahead and import the packaging itself.

21
00:01:49,140 --> 00:01:57,620
So in order to import Ginetta, you type from Jianguo Dot Cool dot, but you need to import.

22
00:01:57,630 --> 00:02:00,060
That's going to be Janita.

23
00:02:00,510 --> 00:02:06,660
So once you have imported pajarito, we now go ahead and create the instance of the patinated.

24
00:02:07,320 --> 00:02:14,050
So in order to create the instance will go right here when the code for the search actually ends.

25
00:02:14,070 --> 00:02:15,850
So the search code ends over here.

26
00:02:16,290 --> 00:02:23,310
So here, I'll go ahead and give a comment and say something like Baddaginnie to code and let's actually

27
00:02:23,310 --> 00:02:26,610
go ahead and name this thing as search code.

28
00:02:29,570 --> 00:02:35,460
So now that we add these comments, we now know what kind of good blog performs, what sort of action.

29
00:02:36,080 --> 00:02:39,350
So now in here, let's instantiate the passingly the object.

30
00:02:39,350 --> 00:02:44,500
So I'll type in Beijing later equals that's going to be pajarito.

31
00:02:44,540 --> 00:02:49,630
And in here we are actually going to pass in the product objects, which we have right over here.

32
00:02:50,240 --> 00:02:52,490
So it'll pass in the product objects in here.

33
00:02:53,570 --> 00:02:58,490
And then you actually want to display the number of items you want on a single page.

34
00:02:58,790 --> 00:03:04,700
So right now, let's set this limit to four because we don't have a lot of objects in our database to

35
00:03:04,700 --> 00:03:05,150
test.

36
00:03:05,660 --> 00:03:06,930
So let's keep it to full.

37
00:03:06,950 --> 00:03:12,710
And later on, you can simply go ahead and change this single number if you want less number or let's

38
00:03:12,710 --> 00:03:14,930
say more number of items on your Web page.

39
00:03:15,860 --> 00:03:19,670
So now once we have that, we now go ahead and get the page.

40
00:03:19,910 --> 00:03:22,700
So I'll type in page equals.

41
00:03:23,180 --> 00:03:25,490
That's actually going to be a request.

42
00:03:26,610 --> 00:03:27,280
Dr..

43
00:03:28,730 --> 00:03:35,590
Get that get, which means we are actually extracted, the PGA and we have sold it into pitch and now

44
00:03:35,590 --> 00:03:40,920
we simply passing this particular pitch to the progeny to to get that particular pitch.

45
00:03:40,940 --> 00:03:47,300
So once we have the speech number, we can actually get that particular page from the pagination and

46
00:03:47,300 --> 00:03:55,370
we can see if those objects and product objects so I can type in product objects equals Barceloneta.

47
00:03:57,990 --> 00:04:05,190
Don't get underscore peach, and I can get those items on that particular page by typing in page over

48
00:04:05,190 --> 00:04:05,470
here.

49
00:04:06,240 --> 00:04:08,340
So that means we are pretty much good to go.

50
00:04:08,460 --> 00:04:14,940
And now what finally happens is that whenever we get the items from that page, those objects actually

51
00:04:14,940 --> 00:04:19,240
get passed as product objects, as a context or here.

52
00:04:19,470 --> 00:04:25,530
So as you can see, these product objects, which are nothing but the products we get after fascinating.

53
00:04:25,680 --> 00:04:27,370
Are being passed right over here.

54
00:04:27,990 --> 00:04:34,210
So once we do that now, we need to go ahead and make a few changes in the template, which is the next

55
00:04:34,320 --> 00:04:35,360
e-mail template.

56
00:04:35,700 --> 00:04:38,730
So let's go back to the next HTML page.

57
00:04:39,270 --> 00:04:44,430
And in here in the next e-mail, we wish to add the Virginny the buttons.

58
00:04:44,460 --> 00:04:48,650
That is the one, two, three and four number of pages which we have.

59
00:04:49,080 --> 00:04:54,900
So if you actually switch to Google Chrome, you as you can see here, we have our products and now

60
00:04:55,090 --> 00:05:02,130
we want to have the navigation or the Bayji, NATO's navigation, which displays which page we are currently

61
00:05:02,130 --> 00:05:04,920
on, how to go to the previous page and the next page.

62
00:05:05,340 --> 00:05:08,220
So let's go ahead and add that navigation up over here.

63
00:05:08,250 --> 00:05:14,380
So if you go to Visual Studio Code, you need to go right below when the product listing actually ends.

64
00:05:14,400 --> 00:05:20,900
So right now, if you have a look over here, we have this particular thing, which is the search bar.

65
00:05:21,360 --> 00:05:25,110
Then we have this particular rule which holds all the products.

66
00:05:25,110 --> 00:05:29,660
And this mainly over here is the actual live for the container.

67
00:05:30,120 --> 00:05:33,780
So you want to stay inside the container and we need to create a new.

68
00:05:34,770 --> 00:05:43,950
So I'll go ahead and type and div class equals rule and then let's go ahead and create the actual navigation

69
00:05:43,950 --> 00:05:44,320
in here.

70
00:05:45,120 --> 00:05:52,920
So first of all, I'll have a column in here, so I'll type in the class called Dash and Dash.

71
00:05:53,190 --> 00:06:00,840
And let's say this column only spans three columns and let's go ahead and give it some offset soil type,

72
00:06:00,840 --> 00:06:05,900
an offset dash and dash for.

73
00:06:06,810 --> 00:06:12,030
So now in here we go ahead and create a dual class that is a class for the unordered list.

74
00:06:12,390 --> 00:06:13,440
So I'll type in.

75
00:06:14,190 --> 00:06:14,730
You Will.

76
00:06:15,910 --> 00:06:17,430
The class is going to be.

77
00:06:18,610 --> 00:06:24,880
Pagination and in here, we now need to go ahead and add list elements, but even before that, let's

78
00:06:24,880 --> 00:06:27,010
go ahead and learn how exactly we can add them.

79
00:06:27,430 --> 00:06:32,550
So, first of all, we actually want to go ahead and add a link for the previous piece.

80
00:06:32,920 --> 00:06:36,660
So how exactly would we know that you want to enter the previous page?

81
00:06:36,790 --> 00:06:41,400
So in order to do that, we make use of the conditional here.

82
00:06:41,800 --> 00:06:49,750
So we say that if and in here, in order to check if the page actually has the previous page, you first

83
00:06:49,750 --> 00:06:54,550
go ahead and make use of the context, which is product objects right here.

84
00:06:54,740 --> 00:06:58,960
So you see if the product object actually has some previous content.

85
00:06:59,380 --> 00:07:01,780
So you see if product.

86
00:07:03,170 --> 00:07:13,980
And Lesco objects, dot has underscore previous, and we go ahead and end the iffier, so LeBon end

87
00:07:14,010 --> 00:07:16,170
of so now similar to this.

88
00:07:16,550 --> 00:07:22,110
We also want to go ahead and make the navigation for proceeding to the next page as well.

89
00:07:22,550 --> 00:07:32,510
So in order to make sure if the product object has next, we dipen if product objects DOT has and let's

90
00:07:32,540 --> 00:07:36,460
go next and we end that a block over here.

91
00:07:36,710 --> 00:07:37,850
So I'll type in.

92
00:07:37,980 --> 00:07:43,070
And if so, we have taken care of both of these things here and now.

93
00:07:43,070 --> 00:07:44,990
Let's go ahead and write some code in there.

94
00:07:45,590 --> 00:07:50,120
So when will we discover that the product object actually has some previous items?

95
00:07:50,450 --> 00:07:55,690
And that is what we want to do, is that we first want to go ahead and create a list element.

96
00:07:55,700 --> 00:08:02,990
So I'll type in L.A. and the class for this is going to be page dash item.

97
00:08:04,590 --> 00:08:11,280
And in this Eli class, we actually want to include the link to the previous page, so in here I'll

98
00:08:11,280 --> 00:08:19,710
type an E, the H for this is going to be questionmark page because we obviously type in questionmark

99
00:08:19,710 --> 00:08:22,380
page then equal to the page number.

100
00:08:23,100 --> 00:08:25,740
So this is going to be the static part.

101
00:08:25,740 --> 00:08:31,080
And now in order to create the dynamic part, which is the page number, we use the template syntax.

102
00:08:31,290 --> 00:08:37,049
And in here, in order to get the previous page number, we get that previous page number from the product

103
00:08:37,049 --> 00:08:38,179
object itself.

104
00:08:38,640 --> 00:08:48,140
So I'll type in product, underscore objects, dot previous and the scope page underscore no.

105
00:08:48,600 --> 00:08:54,440
So this is actually going to give us access to the previous page number and in here for the links name

106
00:08:54,450 --> 00:08:55,650
we are going to type in.

107
00:08:56,840 --> 00:09:03,800
Previous so now once we have that, we are pretty much done designing this particular element and we

108
00:09:03,800 --> 00:09:04,990
have ended this year.

109
00:09:05,270 --> 00:09:09,060
Now let's write the same code for getting the page number of the next page.

110
00:09:09,560 --> 00:09:16,190
So for that very purpose, we can simply copy this particular code right here and pasted right in here.

111
00:09:16,730 --> 00:09:19,350
And there are a few changes we need to make here.

112
00:09:19,580 --> 00:09:23,200
So instead of this previous, we will see something like next.

113
00:09:23,720 --> 00:09:30,350
And now in order to get the next page number, I can simply go ahead replace this previous with next.

114
00:09:30,350 --> 00:09:31,640
And we are good to go.

115
00:09:32,300 --> 00:09:38,990
So now once we go ahead and do that now, the only part which remains is how exactly can we access the

116
00:09:39,080 --> 00:09:39,860
current page.

117
00:09:40,310 --> 00:09:43,550
So accessing the current page is quite simple for that.

118
00:09:43,560 --> 00:09:49,070
What you can simply do is you can go right here in between, because obviously we want to display the

119
00:09:49,070 --> 00:09:53,010
current page in between the previous page and the next page.

120
00:09:53,330 --> 00:09:57,650
So in here I can go ahead and simply paste the code, which we had over here.

121
00:09:58,220 --> 00:10:04,310
And we will see this is going to have a class of active because this is currently going to be the active

122
00:10:04,310 --> 00:10:04,630
link.

123
00:10:05,180 --> 00:10:11,450
And then for the each level here, what we can do is that in order to get the page number, you can

124
00:10:11,450 --> 00:10:19,160
simply go ahead and type in product, underscore objects, dot simply the number and not the page numbers.

125
00:10:19,180 --> 00:10:20,510
So now we have this code.

126
00:10:20,510 --> 00:10:21,950
We are pretty much good to go.

127
00:10:22,100 --> 00:10:25,740
So let's now go ahead and see if it actually works.

128
00:10:25,760 --> 00:10:27,620
So let me go back over here.

129
00:10:27,740 --> 00:10:33,380
And if I hit refresh, as you can see now, we only have four items in here and we have previous and

130
00:10:33,380 --> 00:10:35,930
the next to the previous one won't work.

131
00:10:35,930 --> 00:10:40,460
And if you click on next, as you can see, we have the next page in here.

132
00:10:40,910 --> 00:10:42,800
But there's one issue right over here.

133
00:10:43,340 --> 00:10:49,400
That is if you actually go to the page, NATO, as you can see, we have still named this thing as previous

134
00:10:49,400 --> 00:10:50,920
and that needs to be changed.

135
00:10:51,470 --> 00:10:56,770
So in here, instead of displaying something like previous, we see something like the current.

136
00:10:57,170 --> 00:10:58,100
So I'll see.

137
00:11:00,090 --> 00:11:04,080
Current over here, I'll leave the court and now if we go back.

138
00:11:05,030 --> 00:11:09,740
And if we hit refresh, as you can see, we have the current because we don't have the previous, so

139
00:11:09,740 --> 00:11:12,710
we only have access to the current page and the next page.

140
00:11:13,190 --> 00:11:17,610
And now in here, as there is no next page, we don't have any sort of next in here.

141
00:11:17,630 --> 00:11:21,030
So we only have the previous and we have the current.

142
00:11:21,410 --> 00:11:24,880
So if I click on previous, I go back to this particular page right here.

143
00:11:24,890 --> 00:11:30,410
And one more thing which you could do here is that instead of actually having these display as links,

144
00:11:30,410 --> 00:11:36,830
you can make them look sort of like a button by adding a simple class called Ask Page Link.

145
00:11:37,250 --> 00:11:40,820
So in here, if you go back, you can simply go ahead.

146
00:11:40,820 --> 00:11:45,740
And for this particular unordered list, we have the class as pagination.

147
00:11:45,740 --> 00:11:50,720
But for the link tags over here, you can see that it has a class of.

148
00:11:51,660 --> 00:11:58,470
Page does link, and you can do the same thing with all the link tag which we have here, so you can

149
00:11:58,470 --> 00:12:02,820
copy this pasted over here and pasted over here as well.

150
00:12:03,150 --> 00:12:07,140
So now if you save the code, go back and hit refresh.

151
00:12:07,150 --> 00:12:12,660
As you can see now you have the proper buttons in here instead of actually having those links.

152
00:12:12,660 --> 00:12:18,300
So you have denounced the previous and the current displayed as a proper button.

153
00:12:18,630 --> 00:12:22,200
And we also have highlighted the current page, which we are currently on.

154
00:12:23,670 --> 00:12:25,290
So that's it for this lecture.

155
00:12:25,320 --> 00:12:28,650
And I hope you guys understood how to implement pagination.

156
00:12:28,860 --> 00:12:33,560
And in the next lecture, we will go ahead and design the detailed view for these products.

157
00:12:33,630 --> 00:12:39,210
Right now, as you can see, we don't have any sort of detailed view in here which actually displays

158
00:12:39,210 --> 00:12:42,870
the details of these items over here.

159
00:12:42,990 --> 00:12:47,880
So for that, we are going to create a completely different view, a completely different you are a

160
00:12:47,880 --> 00:12:55,020
pattern, and we are going to see how we can link those detailed views with these individual items right

161
00:12:55,020 --> 00:12:55,260
here.

162
00:12:55,410 --> 00:12:57,330
So thank you very much for watching.

163
00:12:57,330 --> 00:12:59,310
And I'll see you guys next time.

164
00:12:59,670 --> 00:13:00,290
Thank you.

