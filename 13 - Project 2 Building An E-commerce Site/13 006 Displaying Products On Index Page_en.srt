1
00:00:00,300 --> 00:00:06,750
So in this lecture, let's go ahead and start adding some styling to our product speech, so the very

2
00:00:06,750 --> 00:00:13,230
first thing which we will do here is that we will go ahead and get bootstrap for our website so that

3
00:00:13,230 --> 00:00:14,710
our site looks much more better.

4
00:00:15,150 --> 00:00:21,050
So let's search for bootstrap full and make sure that you are using the same version as mine.

5
00:00:21,060 --> 00:00:27,690
And we should actually go to the bootstraps official website, which is get bootstrap dotcom.

6
00:00:27,990 --> 00:00:32,930
So if you simply go here, you should be able to find the option to download.

7
00:00:32,940 --> 00:00:34,400
So click on download in here.

8
00:00:34,770 --> 00:00:41,010
And if you scroll down a little bit, you should actually have this bootstrap CDN in here and you can

9
00:00:41,010 --> 00:00:45,760
actually copy these seedlings and you don't need anything else except for them.

10
00:00:46,050 --> 00:00:49,260
So once you go ahead and do that, you're pretty much good to go.

11
00:00:50,010 --> 00:00:57,270
So now in the next e-mail, we will actually get rid of these particular things in here so you can either

12
00:00:57,270 --> 00:01:02,100
delete them completely or you can just go ahead and keep them at the bottom.

13
00:01:02,310 --> 00:01:05,970
So for now, let's go ahead and add the e-mail tags in here.

14
00:01:06,480 --> 00:01:12,720
So I'll just go ahead and use a exclamation mark and hit enter on Visual Studio Code, and that should

15
00:01:12,720 --> 00:01:16,080
actually give you a HTML template to work with.

16
00:01:16,650 --> 00:01:23,180
So now once we have this particular hashtag, you can go ahead and paste that bootstrap link up in here.

17
00:01:23,820 --> 00:01:27,540
So once you have pasted that, now your site is going to use bootstrap.

18
00:01:28,290 --> 00:01:34,800
So now in the body tag, we are going to go ahead and start defining the bootstrap container and we

19
00:01:34,800 --> 00:01:36,420
are going to add rows and columns.

20
00:01:36,690 --> 00:01:43,380
But even before that, I want to tell you guys how exactly we are actually planning to create this particular

21
00:01:43,380 --> 00:01:43,810
site.

22
00:01:44,340 --> 00:01:48,980
So if we open up the browser, as you can see, we have the browser window.

23
00:01:49,380 --> 00:01:57,840
So the way in which we want to design the products listing page is that we want actually one, two,

24
00:01:57,840 --> 00:02:00,370
three and four products listed up over here.

25
00:02:00,810 --> 00:02:06,540
So the first product is going to be here along with the product image, the product name, the product

26
00:02:06,540 --> 00:02:07,050
price.

27
00:02:07,500 --> 00:02:10,199
And the second product is going to have the same layout.

28
00:02:10,229 --> 00:02:14,070
So the second product is going to be aligned to the right of the first one.

29
00:02:14,520 --> 00:02:16,680
And then we are going to have the third one.

30
00:02:16,680 --> 00:02:18,360
Then we are going to have the fourth one.

31
00:02:18,750 --> 00:02:24,570
And if we keep on adding any more products, those should be added up over here in the next room.

32
00:02:25,290 --> 00:02:27,900
So now how exactly can we go ahead and do that?

33
00:02:28,200 --> 00:02:32,000
So first of all, we need to go ahead and create a container.

34
00:02:33,000 --> 00:02:42,420
So I'll type in the glass container, which is going to hold up all the contents of our beach and in

35
00:02:42,420 --> 00:02:43,530
the aisle create a room.

36
00:02:43,830 --> 00:02:46,490
So the glass is going to be able to roll.

37
00:02:47,310 --> 00:02:52,130
And in this particular room, we are going to go ahead and set up some columns.

38
00:02:52,350 --> 00:02:58,770
So as we only want four products in here, out of the 12 bootstrap columns, which we have, we divide

39
00:02:58,770 --> 00:03:00,900
12 by four, which means we get three.

40
00:03:00,900 --> 00:03:03,340
So each column should have a size of three.

41
00:03:03,960 --> 00:03:05,820
So I'll type in div.

42
00:03:06,970 --> 00:03:16,240
Plus, call me three, so that means each one of the product actually spans three columns on a medium

43
00:03:16,240 --> 00:03:22,310
sized device, so we can do that for the larger devices and smaller devices as well.

44
00:03:22,330 --> 00:03:25,640
But for now, let's just stick to the medium sized devices.

45
00:03:26,380 --> 00:03:32,770
So now once we have that in there, we are going to go ahead and we are going to create a card for each

46
00:03:32,770 --> 00:03:33,740
one of our product.

47
00:03:34,150 --> 00:03:40,180
So I'll go ahead and I'll type in the glass equals God.

48
00:03:40,870 --> 00:03:45,480
And the card is going to obviously have the image as well as the card body.

49
00:03:45,490 --> 00:03:48,040
And in the card body, we are going to have card text.

50
00:03:48,580 --> 00:03:53,850
So I'll type in image and the source is going to be the part for the image.

51
00:03:53,980 --> 00:03:58,260
And in here we are also going to add the class as well.

52
00:03:58,270 --> 00:04:04,930
So the class is going to be the bootstrap class, which is nothing but God dash HMG desktop.

53
00:04:06,210 --> 00:04:11,650
And this is the by default bootstrap class for the academics, so make sure to add that.

54
00:04:11,670 --> 00:04:13,530
And now let's define the card body.

55
00:04:13,650 --> 00:04:19,140
So div class is going to be God body.

56
00:04:20,149 --> 00:04:26,720
And the God body is going to have some text and that text is going to be nothing but the product name,

57
00:04:26,910 --> 00:04:33,550
so I'll type in the class and the class here is going to be called title.

58
00:04:34,160 --> 00:04:38,840
So these classes are actually quite important because they are going to ensure that this particular

59
00:04:38,840 --> 00:04:41,570
development have some specific looks.

60
00:04:41,840 --> 00:04:48,530
And in here, for now, let's type in product name so I can type in product name in here.

61
00:04:49,550 --> 00:04:55,600
And let's also add some context in here right after this particular div tag ends.

62
00:04:56,150 --> 00:05:04,670
So elegant, yet another div and I'll add the class as God does, text the car title and context actually

63
00:05:04,670 --> 00:05:08,990
a little bit different and this should actually list our product.

64
00:05:10,470 --> 00:05:11,010
Price.

65
00:05:13,860 --> 00:05:16,660
OK, so once we do that, we are pretty much good to go.

66
00:05:17,040 --> 00:05:22,590
So now this is just a template and we have not added our dynamic content, which is this right over

67
00:05:22,590 --> 00:05:22,880
here.

68
00:05:23,190 --> 00:05:24,840
But soon we are going to do that.

69
00:05:24,930 --> 00:05:27,520
And I'll explain you guys how this thing is going to work.

70
00:05:28,110 --> 00:05:33,270
So if you go back over here, as you can see, we are going to have a single drawing here.

71
00:05:33,690 --> 00:05:36,750
And in the single room we want to display multiple products.

72
00:05:37,140 --> 00:05:42,540
So in order to display multiple products, we first actually need to loop through each one of those

73
00:05:42,540 --> 00:05:43,050
products.

74
00:05:43,590 --> 00:05:50,730
So we are going to start a for loop from inside this particular room and we are going to use the for

75
00:05:50,730 --> 00:05:52,320
loop for each particular column.

76
00:05:52,890 --> 00:05:58,410
So this will give you an idea where exactly you need to start this particular for loop.

77
00:05:58,640 --> 00:06:04,310
So I will just go ahead, get that and you need to paste it right after the.

78
00:06:06,570 --> 00:06:12,090
And that's because we want to loop through all the columns in here, and this thing is the time to create

79
00:06:12,090 --> 00:06:12,630
columns.

80
00:06:13,290 --> 00:06:17,970
And as you can see, this is where exactly the column ends.

81
00:06:18,390 --> 00:06:22,930
Visual Studio is going to give you a clue that this div tag ends over here.

82
00:06:23,160 --> 00:06:26,590
So we are going to go ahead and end our loop right over here.

83
00:06:27,270 --> 00:06:34,140
So let's go ahead, cut that from here and let's again make sure that we're pasting it in the right

84
00:06:34,140 --> 00:06:34,590
place.

85
00:06:34,940 --> 00:06:36,220
So just pasted in here.

86
00:06:37,140 --> 00:06:40,590
So once we go ahead and do that, let's also delete this.

87
00:06:41,130 --> 00:06:46,530
And now we will actually have the product or title in here in place of the product name.

88
00:06:47,010 --> 00:06:48,000
So I'll type in.

89
00:06:49,510 --> 00:06:50,740
Product not.

90
00:06:52,770 --> 00:06:53,640
Title in here.

91
00:06:55,050 --> 00:07:04,890
And product not pricing here, soil type and product price and for the image, we are going to go ahead

92
00:07:05,670 --> 00:07:10,170
and we will just type in the image source as.

93
00:07:11,500 --> 00:07:14,100
Product, dot image.

94
00:07:15,470 --> 00:07:22,940
And now let's go ahead, save the code and see how our site looks like, so let's switch back to the

95
00:07:22,940 --> 00:07:23,520
browser.

96
00:07:23,540 --> 00:07:29,120
And when I hit refresh, as you can see, we do have the product name and the product price, but for

97
00:07:29,120 --> 00:07:31,510
some reason, we don't have the product image yet.

98
00:07:32,300 --> 00:07:34,460
And that's because, again, misspelled this.

99
00:07:35,150 --> 00:07:40,850
And I don't know, for some reason, some of the keys of my keyboard actually sometimes get stuck.

100
00:07:41,600 --> 00:07:45,520
So now if you refresh, as you can see, we have the product images in here.

101
00:07:45,680 --> 00:07:51,980
And if you notice this thing right here, as you can see, the bike's image is not aligned properly.

102
00:07:51,980 --> 00:07:56,020
And that's because we have not used the image of the same size.

103
00:07:56,030 --> 00:08:02,180
So before starting the next lecture, I'll make sure to find an image of appropriate size and replace

104
00:08:02,210 --> 00:08:03,420
this image with a new one.

105
00:08:03,740 --> 00:08:09,060
But for now, as you can see, the products are listed in a pretty simple and decent way.

106
00:08:09,350 --> 00:08:12,760
We are yet to style them using property as code.

107
00:08:13,170 --> 00:08:18,290
So in the upcoming lecture, we will go ahead and add a little bit of styling to these products so that

108
00:08:18,290 --> 00:08:19,680
it looks much more better.

109
00:08:20,330 --> 00:08:24,260
So thank you very much for watching and I'll see you guys next time.

110
00:08:24,650 --> 00:08:25,220
Thank you.

