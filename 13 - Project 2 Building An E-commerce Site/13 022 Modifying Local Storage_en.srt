1
00:00:00,570 --> 00:00:07,320
So now let's actually go ahead and figure out a way and learn how exactly can we get the items from

2
00:00:07,320 --> 00:00:10,500
the car and display them on a completely new page?

3
00:00:10,920 --> 00:00:16,170
So in the previous case, it was actually quite easy when you actually want to display those items up

4
00:00:16,170 --> 00:00:16,720
over here.

5
00:00:16,740 --> 00:00:23,160
It was quite simple because you can simply go ahead and get those items over here or item names from

6
00:00:23,160 --> 00:00:26,700
here as they were actually present on the page itself.

7
00:00:26,850 --> 00:00:33,230
But now, how exactly are we going to go ahead and get those card items in here on this specific page?

8
00:00:33,900 --> 00:00:39,930
So in order to get those items in here and the first and foremost thing which you need to do is that

9
00:00:39,930 --> 00:00:42,420
you need to get access to the local storage.

10
00:00:42,780 --> 00:00:48,630
So remember that even if you don't have access to these items right here, you still have access to

11
00:00:48,630 --> 00:00:50,580
the local storage on these pages.

12
00:00:50,580 --> 00:00:55,410
But that is you have access to the local storage throughout your entire website.

13
00:00:56,010 --> 00:01:02,230
So now once you have access to that local storage, you can only get the idea here.

14
00:01:02,430 --> 00:01:09,510
So which means that if you go ahead access your local storage and the moment you only have access to

15
00:01:09,510 --> 00:01:17,460
the idea of the items which are added in your cart, what you not only want to display the item ideally

16
00:01:17,460 --> 00:01:22,740
here, but you also want to display their name as well and the quantity as well.

17
00:01:23,670 --> 00:01:26,810
So now that thing is actually a problem in here.

18
00:01:27,030 --> 00:01:32,520
The very first thing which we should have done right away when we have started this project is that

19
00:01:32,940 --> 00:01:39,720
along with storing the item in the inside the cart, we should have actually also stored the name and

20
00:01:39,720 --> 00:01:42,150
the quantity of those items as well.

21
00:01:42,450 --> 00:01:50,700
So if you go back over here to the indexed on HTML page, so let's go back over here and let's go to

22
00:01:50,700 --> 00:01:54,580
the code where we have saved the items inside our car.

23
00:01:54,690 --> 00:01:59,240
That's basically storing the item ID inside local storage.

24
00:01:59,550 --> 00:02:03,650
So as you can see, this is the code which we have used in order to do so.

25
00:02:04,260 --> 00:02:09,930
So right here, as you can notice, we have simply store the item in the inside the cart and we have

26
00:02:09,930 --> 00:02:11,390
implemented its quantity.

27
00:02:11,430 --> 00:02:12,740
So that's all we have done.

28
00:02:13,580 --> 00:02:20,090
So now, in order to make this particular thing also stole the name of that particular item, which

29
00:02:20,090 --> 00:02:25,590
we have added, we need to make a few modifications to this particular code.

30
00:02:26,120 --> 00:02:32,440
So right now, if you notice, we have only stored these values in the form of a key value pair.

31
00:02:32,720 --> 00:02:34,640
So what do I exactly mean by that?

32
00:02:34,670 --> 00:02:38,630
So, for example, if you do local storage, don't get item.

33
00:02:38,630 --> 00:02:39,620
And if you do.

34
00:02:42,550 --> 00:02:49,690
Over here, as you can see, you get all these items and these item values can be actually accessed

35
00:02:49,690 --> 00:02:53,130
by the index, so if you do count of one, you will get three.

36
00:02:53,140 --> 00:02:55,470
If you do cut of two, you'll get three.

37
00:02:55,480 --> 00:02:57,650
If you do cut off three, you will get one.

38
00:02:57,670 --> 00:02:58,930
So on and so forth.

39
00:02:58,960 --> 00:03:04,270
So now, instead of just doing cut of one, let's say we want to add another index to the card, which

40
00:03:04,270 --> 00:03:07,020
actually gives us access to the name.

41
00:03:07,030 --> 00:03:13,840
So let's say, for example, whenever you want to get the quantity, you see something like card, then

42
00:03:13,840 --> 00:03:17,150
you see the item ID, which is either one, two and three.

43
00:03:17,170 --> 00:03:24,280
And in order to get the quantity, you do card, then item I.B. and then the index, which is zero for

44
00:03:24,280 --> 00:03:24,840
quantity.

45
00:03:24,880 --> 00:03:27,590
So I guess this is actually quite confusing for you.

46
00:03:27,940 --> 00:03:31,900
So let's actually implement this in practice so as to clear up the confusion.

47
00:03:32,140 --> 00:03:38,950
So right now, if you go here in this particular code right now, we have access to that particular

48
00:03:38,950 --> 00:03:42,350
items, quantity when we type in card of item ID.

49
00:03:43,000 --> 00:03:49,030
So now instead of doing this particular thing, we now want to get access to the quantity from a new

50
00:03:49,030 --> 00:03:49,640
variable.

51
00:03:50,080 --> 00:03:57,130
So instead of just card item over here, let's say we want to do something like card item ID and now

52
00:03:57,130 --> 00:04:02,740
we will store the quantity at the location, which is the card item ID of zero.

53
00:04:03,370 --> 00:04:06,550
So now this might sound a little bit confusing.

54
00:04:06,910 --> 00:04:09,230
So let me explain you guys with an example.

55
00:04:09,250 --> 00:04:14,230
So first of all, at this moment, let's understand where exactly we have stored the quantity.

56
00:04:14,770 --> 00:04:19,750
So right now we have studied the quantity inside the card of item ID.

57
00:04:20,019 --> 00:04:26,890
So whenever you want to access the quantity of a specific item, you do this, that is card and then

58
00:04:26,890 --> 00:04:30,280
you specify the item ID for which you want the quantity.

59
00:04:30,520 --> 00:04:36,790
So for example, let's say if I want the quantity of item one, I'll simply do kind of one over here.

60
00:04:37,510 --> 00:04:44,440
Now, here we have used the single index and now what we wish to do is that we wish to use multidimensional

61
00:04:44,440 --> 00:04:47,140
index to access the quantity of our card.

62
00:04:47,710 --> 00:04:53,700
So let's go ahead and first of all, get access to the quantity for the usual way.

63
00:04:53,830 --> 00:05:02,540
So I'll see quantity equals and also a card of simply item on the school ID.

64
00:05:02,770 --> 00:05:06,430
So now I get access to quantity in the current scenario.

65
00:05:06,970 --> 00:05:11,650
But for now, let's go ahead and let's assume we want to modify that.

66
00:05:11,890 --> 00:05:18,820
So now, instead of saving the quantity in the cost of item ID, we now want to see the quantity in

67
00:05:18,820 --> 00:05:22,360
cost of item ID and another index, which is zero.

68
00:05:23,170 --> 00:05:29,470
So now this is our new data structure, which is going to store the quantity at this particular location.

69
00:05:30,220 --> 00:05:36,490
So now instead of having a card item, it equals got item ID plus one instead.

70
00:05:36,500 --> 00:05:43,540
We will now go ahead and use card item idea of zero and we will increment that thing instead and we

71
00:05:43,540 --> 00:05:46,000
will get rid of this code, which we have right here.

72
00:05:46,750 --> 00:05:48,340
So now let's get rid of that.

73
00:05:48,490 --> 00:05:57,280
And now once we got the quantity from here, let's also go ahead and actually add that quantity in place

74
00:05:57,280 --> 00:05:58,540
of the quantity.

75
00:05:58,540 --> 00:06:00,790
And I hope this doesn't confuse you.

76
00:06:01,210 --> 00:06:04,390
So now the quantity is actually present at cut off.

77
00:06:05,760 --> 00:06:14,220
Itemized of zero and now will simply assigned quantity to this particular thing, and we're good to

78
00:06:14,220 --> 00:06:14,470
go.

79
00:06:15,060 --> 00:06:17,350
So I hope this thing actually makes sense to you.

80
00:06:17,760 --> 00:06:24,060
So in this particular code, we have simply extracted the quantity from where the quantity is actually

81
00:06:24,060 --> 00:06:26,220
now placed inside our new code.

82
00:06:26,790 --> 00:06:29,840
And we have simply implemented the value of quantity by one.

83
00:06:30,120 --> 00:06:36,150
And once we have incremented that quantity, we have simply assigned that incremented quantity to the

84
00:06:36,150 --> 00:06:38,680
actual quantity stored inside the car.

85
00:06:38,700 --> 00:06:41,180
And I hope that actually makes sense to you.

86
00:06:41,610 --> 00:06:48,240
And now in the spot, instead of having to implement cost of item ID, which is nothing but quantity

87
00:06:48,240 --> 00:06:50,970
itself, I can simply go ahead and type in.

88
00:06:52,510 --> 00:06:53,920
Quantity equals one.

89
00:06:55,390 --> 00:07:02,020
And now once we have got the quantity in this specific case, we not only have to assign quantity,

90
00:07:02,020 --> 00:07:08,710
but we also want to assign a name, because remember, the only reason why we have modified this data

91
00:07:08,710 --> 00:07:13,820
structure is because we want to be able to add the name as well.

92
00:07:13,840 --> 00:07:20,770
So we not only want to add the quantity to the local storage, we already have that, but we also want

93
00:07:20,770 --> 00:07:26,650
to add the name to the local storage as well so that we can access that particular name in our checkout

94
00:07:26,650 --> 00:07:27,010
page.

95
00:07:27,640 --> 00:07:33,160
So now once we have the quantity, let's now go ahead and extract the name.

96
00:07:33,430 --> 00:07:38,000
Sunim is going to be extracted from document doget element by ID.

97
00:07:38,110 --> 00:07:42,930
So if you recall, we have extracted the name in this specific fashion right here.

98
00:07:44,050 --> 00:07:49,150
So we will use the exact same code in here and make just a single modification.

99
00:07:50,050 --> 00:07:56,350
So in this way, I get the name here, but now instead of X, I'll simply go ahead and make use of item.

100
00:07:57,130 --> 00:08:00,140
So this is actually going to give us access to all the names.

101
00:08:00,160 --> 00:08:02,310
And now let me end this with a semicolon.

102
00:08:02,500 --> 00:08:10,870
And then once we have the quantity and once we have the name, let's save the quantity and name inside

103
00:08:10,870 --> 00:08:11,400
the card.

104
00:08:11,800 --> 00:08:17,370
So now here, as you can see, we have simply saved the quantity inside the card and now let's save

105
00:08:17,380 --> 00:08:18,530
both of them over here.

106
00:08:18,880 --> 00:08:23,800
So let's say cut off itemized equals.

107
00:08:24,130 --> 00:08:30,080
And in order to save them both together, I can simply type in quantity, call my name.

108
00:08:30,790 --> 00:08:35,350
So now, just as we have seen the quantity at this position.

109
00:08:35,360 --> 00:08:39,679
So whenever you want to access quantity, you timecode item idea of zero.

110
00:08:40,150 --> 00:08:46,780
So whenever you want to access the name, you say cut item ID of one and that's going to give you access

111
00:08:46,780 --> 00:08:47,290
to the name.

112
00:08:47,860 --> 00:08:55,900
So for example, let's say if you want to get access to the name of item whose item idea is one, so

113
00:08:55,900 --> 00:09:02,320
you simply type in cart, then you specify the item, it is one, and then for name you specify one

114
00:09:02,320 --> 00:09:02,860
over here.

115
00:09:03,280 --> 00:09:08,080
So this thing is going to give you access to the court item whose ID is one.

116
00:09:08,200 --> 00:09:15,310
And this one over here signifies that you want the name and whenever you want to get access to the quantity,

117
00:09:15,310 --> 00:09:17,860
you simply go ahead and replace this with zero.

118
00:09:18,340 --> 00:09:24,400
So this will now give you access to the quantity of the item present in the card, which has an ID as

119
00:09:24,400 --> 00:09:24,700
one.

120
00:09:25,250 --> 00:09:29,370
So I hope this thing makes sense and you get the concept.

121
00:09:29,590 --> 00:09:35,680
So if you have any doubts or difficulties, do let me know in the USC section and I will be there to

122
00:09:35,680 --> 00:09:36,460
help you out.

123
00:09:36,860 --> 00:09:42,550
OK, so once we have that one additional thing, which we need to do here is that if you just scroll

124
00:09:42,550 --> 00:09:47,740
down a little bit, as you can see here, we have used the single index value.

125
00:09:47,980 --> 00:09:53,230
So for example, in order to access the quantity, we have used card of X.

126
00:09:53,890 --> 00:10:00,280
And as I mentioned, whenever you want to access the quantity now, we no longer use this, but instead

127
00:10:00,280 --> 00:10:02,530
you need to specify a zero here.

128
00:10:03,190 --> 00:10:08,500
Now, this thing is actually going to give you access to the quantity of that specific item.

129
00:10:09,600 --> 00:10:15,090
So now, once we are done with this, we have successfully modified the way of actually storing elements

130
00:10:15,090 --> 00:10:16,500
into our local storage.

131
00:10:17,010 --> 00:10:21,350
So now let's actually go back to the console and hit refresh.

132
00:10:21,360 --> 00:10:29,230
And first of all, we need to clear up the local storage because it would still have some previous content.

133
00:10:29,250 --> 00:10:35,580
So once we have cleared that now let's try to go ahead and actually add some items to the cart and let's

134
00:10:35,580 --> 00:10:36,690
see how this works.

135
00:10:37,410 --> 00:10:43,140
So now, if I click on this particular watch, as you can see, instead of a single item, we now have

136
00:10:43,140 --> 00:10:45,600
an array over here and over here.

137
00:10:45,690 --> 00:10:50,250
It is one that means the quantity is one and it sees watch.

138
00:10:50,250 --> 00:10:53,200
But as you can see, there's actually a bunch of space in there.

139
00:10:53,760 --> 00:10:56,030
So let's go ahead and fix this issue.

140
00:10:56,060 --> 00:10:57,930
So let's go back to the code.

141
00:10:58,410 --> 00:11:05,130
And we have this issue because we might have a ton of blank space around our product title.

142
00:11:05,220 --> 00:11:06,320
So let's fix that.

143
00:11:06,900 --> 00:11:10,500
And as you can see, we have a bunch of white space in here.

144
00:11:10,920 --> 00:11:13,260
So once we go ahead, get rid of the white space.

145
00:11:13,620 --> 00:11:15,840
This should not be a problem.

146
00:11:16,290 --> 00:11:21,420
So now if we see that, let me again clear up the local storage.

147
00:11:21,630 --> 00:11:23,470
So local storage to clear.

148
00:11:24,690 --> 00:11:26,610
And now if we go ahead and add that.

149
00:11:28,210 --> 00:11:34,030
OK, I didn't rephrase this, so let me just refresh this and let me clear up the local storage one

150
00:11:34,030 --> 00:11:34,590
more time.

151
00:11:34,600 --> 00:11:40,630
And now if we add this for some reason, we still have that issue here.

152
00:11:43,590 --> 00:11:50,700
OK, so for the second item, we no longer have that issue, so we have EBC laptop and this is probably

153
00:11:50,700 --> 00:11:54,030
because the local storage might not have been cleared.

154
00:11:54,040 --> 00:11:57,050
So again, pipe and local storage, not clear.

155
00:11:57,780 --> 00:12:00,910
And hopefully this time everything should work just fine.

156
00:12:00,930 --> 00:12:03,090
So now let me add this, Pikoli here.

157
00:12:03,750 --> 00:12:06,030
Let me add this laptop back twice.

158
00:12:06,180 --> 00:12:11,070
And now if we actually have a look, as you can see, it works perfectly fine.

159
00:12:11,340 --> 00:12:15,770
So now you might have noticed that at a specific index.

160
00:12:15,780 --> 00:12:22,860
So, for example, at the specific index three, we have the bike and it shows the quantity over here.

161
00:12:23,370 --> 00:12:28,560
And for the next four, we have the laptop bag and we have the quantity as two.

162
00:12:28,950 --> 00:12:35,700
So now instead of saving a single value, we now have two values stored inside a specific area.

163
00:12:36,360 --> 00:12:42,870
So now we can actually make use of these particular values as they are now into our local storage.

164
00:12:42,870 --> 00:12:46,580
And now we can display these values into our checkout page.

165
00:12:47,010 --> 00:12:53,010
So if we go to checkout, as you can see right now, we have this list group, but soon we are going

166
00:12:53,010 --> 00:12:58,190
to go ahead and modify this list group and display the items from our local storage.

167
00:12:58,210 --> 00:13:01,120
So we are going to do that in the upcoming lecture.

168
00:13:01,590 --> 00:13:03,630
So thank you very much for watching.

169
00:13:03,630 --> 00:13:05,730
And I'll see you guys next time.

170
00:13:06,060 --> 00:13:06,570
Thank you.

