1
00:00:00,300 --> 00:00:05,700
So now let's go ahead and fix this particular issue, but then when you click on this card, you get

2
00:00:05,700 --> 00:00:07,090
this random text right here.

3
00:00:07,860 --> 00:00:11,820
So in order to fix that, first of all, you need to go into your code editor.

4
00:00:11,820 --> 00:00:16,890
And in here, the first thing which you need to do is that you need to get access to that particular

5
00:00:16,890 --> 00:00:17,430
element.

6
00:00:17,880 --> 00:00:23,530
So the element is nothing but the popular and we have assign it an ID of card.

7
00:00:23,730 --> 00:00:30,240
So if you actually go here, as you can see, this is our Popolo and it has an ID Ascott.

8
00:00:30,570 --> 00:00:36,300
So using the same I.D., we will try to modify the content of that particular thing up over here and

9
00:00:36,300 --> 00:00:37,360
our JavaScript code.

10
00:00:37,920 --> 00:00:43,830
So right inside this function will go ahead and add in a bunch of code which modifies the content of

11
00:00:43,830 --> 00:00:45,090
that particular pople.

12
00:00:45,870 --> 00:00:52,240
So I'll get access to that particular element by typing in document dot, get element by ID.

13
00:00:52,980 --> 00:00:57,420
And now let's specify the idea of that particular thing, which is nothing but cut.

14
00:00:58,050 --> 00:01:00,800
And now we will use the set attribute method.

15
00:01:00,900 --> 00:01:11,880
So I'll type and set attribute and then will set the content of that particular Popplewell to some HTML.

16
00:01:12,090 --> 00:01:17,460
So in order to set the content, I'll simply type in data content in here.

17
00:01:17,730 --> 00:01:24,580
And then after typing that thing in, I can have any HTML code in here so I can have it heading toward

18
00:01:24,580 --> 00:01:24,870
here.

19
00:01:25,240 --> 00:01:35,460
Let's see each two and we can display something over here like let's see, this is your cart and now

20
00:01:35,460 --> 00:01:38,860
let's go ahead and save this code and let's see what do we get.

21
00:01:38,880 --> 00:01:44,830
So when I click on this thing, as you can see, this thing right here sees that this is your card.

22
00:01:45,360 --> 00:01:51,330
However, there is one issue that basically when you go ahead and add in some each Tamil Tigers in here,

23
00:01:51,630 --> 00:01:54,310
it's actually going to display that as a string.

24
00:01:54,450 --> 00:01:57,770
So in order to fix that, you need to go to your card.

25
00:01:58,740 --> 00:02:06,480
And in here you need to add in a property which is called Last Data Nasch e-mail.

26
00:02:07,110 --> 00:02:08,850
And you need to set this thing to.

27
00:02:10,190 --> 00:02:16,820
True, so once you go ahead and fix that, and now if you hit refresh, as you can see now you get that

28
00:02:16,820 --> 00:02:20,760
this is your cart and I guess this is a lot bigger.

29
00:02:20,780 --> 00:02:26,540
So let's go ahead and fix that by changing the H2 to let's see.

30
00:02:27,590 --> 00:02:29,540
Each five would be fine, I guess.

31
00:02:29,780 --> 00:02:32,060
So now, once we go ahead, make that change.

32
00:02:32,450 --> 00:02:35,240
As you can see, it just seems that this is your cart.

33
00:02:35,750 --> 00:02:37,280
So that's all fine and good.

34
00:02:37,580 --> 00:02:43,910
But now what we wish to do is that we wish to go ahead and display the actual items which we have in

35
00:02:43,910 --> 00:02:46,850
our cart over here inside this particular popplewell.

36
00:02:47,120 --> 00:02:53,480
So we will go ahead and learn how to do that in the next lecture as the procedure for doing so is a

37
00:02:53,480 --> 00:02:56,470
little bit complicated and would take a lot of time.

38
00:02:56,810 --> 00:03:00,680
So thank you very much for watching and I'll see you guys next time.

39
00:03:00,860 --> 00:03:01,460
Thank you.

