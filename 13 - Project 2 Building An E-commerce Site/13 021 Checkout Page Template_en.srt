1
00:00:00,090 --> 00:00:06,630
So now once we have the speed, these elements in a Poplarville, let's go ahead and learn how to develop

2
00:00:06,630 --> 00:00:08,460
the checkout functionality.

3
00:00:09,060 --> 00:00:14,700
So first of all, let's go ahead and add an E checkout button right over here inside this particular

4
00:00:14,700 --> 00:00:15,330
popplewell.

5
00:00:15,570 --> 00:00:17,250
So let's go back to our code.

6
00:00:17,400 --> 00:00:22,230
And the only thing which you need to do here is that you simply need to go ahead.

7
00:00:22,230 --> 00:00:27,840
And in here, right before we actually said the content of the proposal, I simply need to go ahead

8
00:00:28,020 --> 00:00:34,590
and type in God's string and I can create e-mail string to place a button up over here.

9
00:00:34,800 --> 00:00:37,140
So I'll type in counseling plus equals.

10
00:00:37,710 --> 00:00:46,050
And in here I can see e it ref as a means to add a button and follow a liquid.

11
00:00:46,050 --> 00:00:47,760
This thing to slash.

12
00:00:48,950 --> 00:00:49,670
Check out.

13
00:00:52,320 --> 00:00:55,680
And make sure to use single quotes over here.

14
00:00:57,450 --> 00:01:05,099
And now Al Gore had to close this particular tag and in here I can add a button so I can see something

15
00:01:05,099 --> 00:01:14,060
like Button Plus equals here as I want to use a bootstrap button, I'll type in between Betty and Dash,

16
00:01:14,220 --> 00:01:16,190
let's see warning.

17
00:01:16,920 --> 00:01:19,800
And let's also give this thing an I.D. So let's see.

18
00:01:19,800 --> 00:01:24,500
The ID is going to be equal to check out.

19
00:01:24,510 --> 00:01:33,410
Let's actually close this tag in here so I'll see something like slash button and I'll also end the

20
00:01:33,420 --> 00:01:38,970
tag over here by typing in E and finally close the string and add a semicolon.

21
00:01:39,570 --> 00:01:44,590
So let me just make sure that the string is actually correct and it pretty much looks OK.

22
00:01:45,060 --> 00:01:50,670
So now let me just skip ahead, see the code and let's see if we do have that button up over here.

23
00:01:50,850 --> 00:01:57,240
So now if I hit refresh and when I click on card, we have actually a button over here, but I guess

24
00:01:57,250 --> 00:01:58,680
we forgot to give it a name.

25
00:01:59,280 --> 00:02:02,250
So let's type in the name as check up.

26
00:02:03,390 --> 00:02:06,060
So now if we go ahead, hit refresh.

27
00:02:06,070 --> 00:02:09,210
As you can see, we now have a check out, but no way here.

28
00:02:09,900 --> 00:02:16,920
So now if I click on checkout, as you can see, we get an error because we have not yet defined the

29
00:02:16,920 --> 00:02:17,880
URL pattern.

30
00:02:18,090 --> 00:02:21,490
Now, we have created any sort of a template for the check out page.

31
00:02:22,050 --> 00:02:23,740
So let's go ahead and do the same.

32
00:02:24,210 --> 00:02:30,120
So the very first thing which we need to do in order to set up the checkout functionality is that we

33
00:02:30,120 --> 00:02:32,280
first need to go ahead and create a view.

34
00:02:32,490 --> 00:02:37,980
So let's go to The View start by file and in here, create a new view for checkout.

35
00:02:38,370 --> 00:02:41,220
So I'll see the checkout.

36
00:02:41,760 --> 00:02:45,030
And this is going to take a request, as usual.

37
00:02:45,420 --> 00:02:51,990
And in here, the only thing which we need to do is that we need to go ahead and we need to render a

38
00:02:51,990 --> 00:02:52,740
template.

39
00:02:52,740 --> 00:02:57,360
So we'll see a return render and I'll render a template.

40
00:02:58,020 --> 00:03:00,870
So first of all, I'll personally request.

41
00:03:03,390 --> 00:03:09,150
And we don't have any sort of context in here, so I'll directly go ahead and put in a template, so

42
00:03:09,150 --> 00:03:13,290
I'll type in shop slash, let's say check out DOT.

43
00:03:14,310 --> 00:03:18,020
So we don't right now have this particular template in our template.

44
00:03:18,090 --> 00:03:22,850
Lecrae So let's go ahead and actually create that particular template.

45
00:03:23,580 --> 00:03:29,340
So I'll go ahead, go to files, let's go to templates.

46
00:03:29,340 --> 00:03:36,150
And in here inside the shop, I'll create a new file and I'll call this thing as check out, not HTML.

47
00:03:37,140 --> 00:03:46,170
So in here, I'll simply go ahead and add some HTML code and for now let's see this thing only see something

48
00:03:46,170 --> 00:03:50,340
like this is a checkout page.

49
00:03:50,550 --> 00:03:53,850
And now once we go ahead, see that we are pretty much good to go.

50
00:03:54,150 --> 00:04:00,000
But if we go back over here and hit refresh, as you can see, we still don't get that page over here

51
00:04:00,000 --> 00:04:04,990
because we need to associate you all pattern with this particular view right here.

52
00:04:05,340 --> 00:04:07,350
So let's go ahead and do that as well.

53
00:04:07,680 --> 00:04:10,880
So for that, let's go back to the real spy file.

54
00:04:10,890 --> 00:04:15,850
And in here, let's simply add the usual pattern for checkout.

55
00:04:16,380 --> 00:04:17,640
So I'll type in path.

56
00:04:17,760 --> 00:04:19,560
And the path is actually simple.

57
00:04:19,560 --> 00:04:24,950
It's just checkout slash and then let's associate of you with this.

58
00:04:25,350 --> 00:04:31,650
So that should be used dot checkout, which is the view which we have just created right now.

59
00:04:32,040 --> 00:04:37,920
And let's say the name of this thing is going to be equal to check out as well.

60
00:04:38,430 --> 00:04:43,020
So now once we go ahead and do that, we are pretty much good to go.

61
00:04:44,250 --> 00:04:52,290
So now if we go back over here and if we hit refresh and if we go to check out, as you can see, we

62
00:04:52,290 --> 00:04:54,510
are now redirected to the checkout page.

63
00:04:55,140 --> 00:05:02,010
So now, in order to design this checkout page, let's say you want to display all of the items in Elkhart

64
00:05:02,010 --> 00:05:02,660
or here.

65
00:05:03,420 --> 00:05:10,620
So in order to do that, let's go ahead and make use of a particular bootstrap component, which is

66
00:05:10,620 --> 00:05:12,350
called as the last group.

67
00:05:12,390 --> 00:05:18,420
So as we actually want to display the items inside our cart over here in the form of a list, we will

68
00:05:18,420 --> 00:05:22,050
make use of the component, which is list group and bootstrap.

69
00:05:22,890 --> 00:05:29,250
So I'll see list group bootstrap in order to get the code for the list.

70
00:05:30,330 --> 00:05:32,850
So now if we go ahead.

71
00:05:34,720 --> 00:05:41,290
And let me just type in bootstrap for here or else we are going to get the lowest group for bootstrap

72
00:05:41,290 --> 00:05:45,080
version three and if we go ahead and add that, we might get an error.

73
00:05:45,730 --> 00:05:49,180
Okay, so we are on to the bootstraps website.

74
00:05:49,360 --> 00:05:54,520
So as you can see, these are the different kinds of little groups which we can use.

75
00:05:54,790 --> 00:06:01,230
So let's say for now we just want to use a little group, which is simple.

76
00:06:02,740 --> 00:06:04,790
So I guess this would be OK.

77
00:06:04,810 --> 00:06:11,620
So it has the items and here we can split the price of the product or let's say the quantity of our

78
00:06:11,620 --> 00:06:12,050
product.

79
00:06:12,550 --> 00:06:14,250
So let's go ahead and get that.

80
00:06:14,260 --> 00:06:24,370
So I'll simply copy this particular group and let's go back over here inside each HTML code, inside

81
00:06:24,370 --> 00:06:29,850
the checkout dot, each HTML, and let's go ahead and create a proper container here.

82
00:06:30,100 --> 00:06:31,510
So I'll see something like.

83
00:06:33,160 --> 00:06:37,520
The class is going to be container then inside the container.

84
00:06:37,540 --> 00:06:40,790
Let's go ahead and as usual, create a row and column.

85
00:06:41,350 --> 00:06:52,550
So this should be div class called ACMD Dash 12 and then div that's going to be class equals rule.

86
00:06:53,620 --> 00:06:57,760
And now let's actually go ahead and piece this thing in here.

87
00:06:58,780 --> 00:07:05,530
So now if we go here and check out Page and if we hit refresh, as you can see, we do get the items,

88
00:07:05,530 --> 00:07:07,930
but the bootstrap styling is not yet applied.

89
00:07:08,230 --> 00:07:13,980
And that's actually because we don't have the bootstrap CDN link for this particular file.

90
00:07:14,920 --> 00:07:20,690
So it's not a good practice to have the client links in each one of those HTML page.

91
00:07:20,710 --> 00:07:26,860
Instead, we should actually create a base that each HTML file, which is not only going to hold these

92
00:07:26,860 --> 00:07:31,000
links, but it should also hold the navigation bar as well.

93
00:07:31,150 --> 00:07:39,360
So for now, let's simply go ahead and just get the bootstrap and link, which is this thing right here.

94
00:07:39,640 --> 00:07:42,160
So I'll simply go ahead copy that from here.

95
00:07:44,050 --> 00:07:47,380
And let me just pasted up over here and the head tag.

96
00:07:49,640 --> 00:07:54,360
And now, once we do so, as you can see, this thing works just fine.

97
00:07:55,250 --> 00:08:01,910
OK, so once we have that, our next job is to go ahead and actually populate this particular list group

98
00:08:01,910 --> 00:08:02,460
right here.

99
00:08:02,990 --> 00:08:07,230
So we are going to populate this particular list group in the next lecture.

100
00:08:07,790 --> 00:08:09,710
So thank you very much for watching.

101
00:08:09,710 --> 00:08:11,630
And I'll see you guys next time.

102
00:08:11,900 --> 00:08:12,530
Thank you.

