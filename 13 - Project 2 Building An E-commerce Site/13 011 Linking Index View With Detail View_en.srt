1
00:00:00,090 --> 00:00:05,100
So now let's go ahead and set up the usual patterns for this particular detail right here.

2
00:00:05,610 --> 00:00:09,330
So in order to set that, let's go to the Alstott profile.

3
00:00:09,330 --> 00:00:13,880
And in here, what you simply now need to do is that you need to type in path.

4
00:00:14,460 --> 00:00:16,600
Then you need to set up the URL.

5
00:00:16,710 --> 00:00:21,110
So in here, what we wish to do is that we only wish to get the items ID.

6
00:00:21,150 --> 00:00:27,990
So, for example, if you type in the you are the one that means you want to get the item whose ID is

7
00:00:27,990 --> 00:00:28,320
one.

8
00:00:28,320 --> 00:00:35,700
If you type and you are to you want to get the items id as to so in here for this particular part we

9
00:00:35,700 --> 00:00:38,170
want to get the ID which we have passed in here.

10
00:00:38,370 --> 00:00:41,960
So in order to get that I can simply go ahead and type in.

11
00:00:42,030 --> 00:00:48,090
And because we want to say that we want to use an integer over here and in here, I can simply type

12
00:00:48,090 --> 00:00:53,080
an ID and then I'll go ahead and simply give as much as we do with every you are.

13
00:00:53,520 --> 00:00:57,550
And now we want to access the view, which is nothing but the detailed view.

14
00:00:57,690 --> 00:00:59,610
So in order to do that, I will die.

15
00:00:59,610 --> 00:01:05,760
When you start in detail and the name of this view is going to be nothing but the detail.

16
00:01:06,870 --> 00:01:11,730
So once we have that, let's give a comma and let's see if this thing actually works out.

17
00:01:12,000 --> 00:01:15,360
So let me make sure that the server is up and running.

18
00:01:15,900 --> 00:01:21,210
And now if we go right here and now, if we do slash one.

19
00:01:22,370 --> 00:01:27,490
And hit enter, as you can see now we get the detail page for this particular watch right here.

20
00:01:27,560 --> 00:01:32,560
So as you can see, the detail page actually looks pretty boring and dull.

21
00:01:33,020 --> 00:01:40,880
So we have the watch over here and we have the watch title, the actual price, the discounted price,

22
00:01:41,000 --> 00:01:43,170
and we have the description as well.

23
00:01:43,340 --> 00:01:50,210
And as you can see, it looks pretty dull and boring and it kind of looks ugly as well because no e-commerce

24
00:01:50,210 --> 00:01:52,640
site would actually list out the products in this way.

25
00:01:52,880 --> 00:01:58,100
So we are actually going to do that or we are actually going to design this particular thing in the

26
00:01:58,100 --> 00:01:59,170
upcoming lectures.

27
00:01:59,390 --> 00:02:03,140
But for now, we just want to make sure that our product actually works.

28
00:02:03,470 --> 00:02:08,539
So currently everything works fine as we are able to access these details in here.

29
00:02:08,840 --> 00:02:12,540
So now let's go ahead and do the same thing for some other products.

30
00:02:12,540 --> 00:02:13,280
So let's see.

31
00:02:13,280 --> 00:02:15,290
I type the product ID3.

32
00:02:15,680 --> 00:02:21,710
I now get the details of this particular bike right here, which is that the product's name is bike.

33
00:02:22,250 --> 00:02:25,280
The actual price is three hundred discounted prices.

34
00:02:25,280 --> 00:02:25,880
Two hundred.

35
00:02:26,090 --> 00:02:29,010
And we have the product description over here as well.

36
00:02:29,030 --> 00:02:32,030
So we will be styling that in the upcoming lectures.

37
00:02:32,210 --> 00:02:37,110
So for now, you can just walk with this and if you want, you can fix this right now as well.

38
00:02:37,400 --> 00:02:43,220
You simply have to go ahead and attach a user's file with the particular little view and you can just

39
00:02:43,220 --> 00:02:45,920
go ahead and make some changes in the Xerces file.

40
00:02:45,980 --> 00:02:51,370
So right now, what we want to focus on is that we want to go ahead and create two buttons in here.

41
00:02:51,560 --> 00:02:59,030
So if you actually go to the index page, we actually want to create a button here, which is a view,

42
00:02:59,030 --> 00:03:02,990
the item which allows us to go to the detailed view of that particular item.

43
00:03:03,230 --> 00:03:07,120
And then we actually want to have the add to cart button over here as well.

44
00:03:08,320 --> 00:03:13,570
So in here, we actually want to have the view item button, which is actually going to allow us to

45
00:03:13,570 --> 00:03:17,030
view the items detail page whenever we click on that particular item.

46
00:03:17,500 --> 00:03:23,210
And here in the bottom, we actually want to add a button would see something like add to cart.

47
00:03:23,590 --> 00:03:26,180
So let's go ahead and add those two buttons in here.

48
00:03:26,830 --> 00:03:31,770
So let's go back to our code for the index dot html page.

49
00:03:32,230 --> 00:03:39,370
And in here, if you go inside this product objects, as you can see, we have this column, then we

50
00:03:39,370 --> 00:03:40,230
have this card.

51
00:03:40,240 --> 00:03:44,200
So this Gaudencio and this is actually a do for the card body.

52
00:03:44,380 --> 00:03:49,540
So right inside the card body, I can go ahead and create a new button in here so I can see something

53
00:03:49,540 --> 00:03:50,230
like Button.

54
00:03:50,770 --> 00:03:56,370
And the class for this is going to be, let's say, between Betty and Dash warning.

55
00:03:57,280 --> 00:04:04,570
And I can see something like let's see if you and right at the bottom I can go ahead and create another

56
00:04:04,570 --> 00:04:07,510
button class so I can just go ahead and copy that.

57
00:04:10,110 --> 00:04:14,670
And I can be sitting here and I can see something like AD.

58
00:04:15,910 --> 00:04:20,260
You got so let's see how exactly this thing looks like.

59
00:04:20,290 --> 00:04:25,400
So as you can see, if I hit refresh, we have the view and the add to cart buttons in here.

60
00:04:25,420 --> 00:04:29,690
So now once we have these buttons, you can now go ahead and make these things as functional.

61
00:04:30,190 --> 00:04:36,010
So one thing which you could do here is that instead of having the button, we can use the tags in here

62
00:04:36,010 --> 00:04:36,540
as well.

63
00:04:37,150 --> 00:04:40,300
So that is actually going to be a link.

64
00:04:40,300 --> 00:04:42,530
But they will function as a button.

65
00:04:42,550 --> 00:04:47,740
So now if you hit refresh, as you can see, they look pretty much the same due to the bootstrap class,

66
00:04:47,740 --> 00:04:49,170
which is the class.

67
00:04:49,480 --> 00:04:51,660
But these things are now actually links.

68
00:04:52,090 --> 00:04:57,790
And now in here, what we wish to do is that we want to add the actual links to those products.

69
00:04:58,120 --> 00:05:03,580
So, for example, when I click on this particular button, I should be actually redirected to this

70
00:05:03,580 --> 00:05:10,270
page, which is one which is going to be the detail page for the product, whose idea is one.

71
00:05:10,810 --> 00:05:13,560
So how exactly can I go ahead and implement that?

72
00:05:14,050 --> 00:05:17,680
So in order to implement that, I can simply type in H.F..

73
00:05:20,040 --> 00:05:27,330
And I can equate this thing to less, and in here I can simply go ahead and access the product ID so

74
00:05:27,330 --> 00:05:31,530
I can see something like product, which is nothing but the current object right here.

75
00:05:31,950 --> 00:05:36,150
And instead of the products title or the products price, I want to access it.

76
00:05:36,180 --> 00:05:38,320
So I'll simply type in product, not it.

77
00:05:39,030 --> 00:05:40,900
So now let's see what happens in here.

78
00:05:41,010 --> 00:05:45,450
So if I hit refresh now and if I go ahead and click on this.

79
00:05:46,780 --> 00:05:52,480
For some reason, it's giving me a different ID in here, so let me just go ahead and see what happened

80
00:05:52,480 --> 00:05:52,860
in here.

81
00:05:57,640 --> 00:06:03,670
OK, so I guess we are missing a closing bracket in here, and once we go ahead and fix that, they

82
00:06:03,680 --> 00:06:04,700
should actually work.

83
00:06:04,960 --> 00:06:11,080
So, yeah, now if I go to view, as you can see, the view has redirected me to the product detail

84
00:06:11,080 --> 00:06:12,610
page of that particular product.

85
00:06:13,060 --> 00:06:15,040
So now let's click on this thing.

86
00:06:15,040 --> 00:06:20,130
And as you can see, now you have the detail page for laptop in here.

87
00:06:20,290 --> 00:06:23,410
It looks pretty ugly, but it's at least functional.

88
00:06:24,250 --> 00:06:30,250
So now let's click on this and you have the detail for like and let's click on Dbag and you have the

89
00:06:30,580 --> 00:06:36,520
detailed page for the bag as well, which means that the detailed view functionality is completed and

90
00:06:36,520 --> 00:06:40,000
now we can access the details of the products which we have in here.

91
00:06:40,840 --> 00:06:42,540
So that's it for this lecture.

92
00:06:42,640 --> 00:06:48,160
And in the upcoming lecture, what we will do is that will go ahead and make this add to cart button

93
00:06:48,160 --> 00:06:49,130
functional as well.

94
00:06:49,660 --> 00:06:55,450
So whenever you click this particular add to cart button, this particular product should be added up

95
00:06:55,450 --> 00:06:56,320
to your card.

96
00:06:56,380 --> 00:07:02,590
So we are going to create the cart functionality and the cart functionality is going to take at least

97
00:07:02,590 --> 00:07:05,230
two to three lectures to be fully functional.

98
00:07:05,500 --> 00:07:11,350
So we are actually going to proceed that particular section in a much more relaxed way as that is actually

99
00:07:11,350 --> 00:07:15,530
going to be the usage of a little bit of query and JavaScript.

100
00:07:15,550 --> 00:07:21,130
So obviously some of you might know and some of you might not know how to use Sequiera and JavaScript.

101
00:07:21,490 --> 00:07:23,550
So we are going to go through that as well.

102
00:07:24,100 --> 00:07:25,590
So that's it for this lecture.

103
00:07:25,660 --> 00:07:30,230
And in the upcoming lecture, let's start building the card for our e-commerce website.

104
00:07:30,760 --> 00:07:32,680
So thank you very much for watching.

105
00:07:32,680 --> 00:07:34,660
And I'll see you guys next time.

106
00:07:35,110 --> 00:07:35,710
Thank you.

