1
00:00:00,720 --> 00:00:07,560
So now that we know how the local storage works, let's figure out a way to actually store the items

2
00:00:07,560 --> 00:00:09,880
ID into this particular local storage.

3
00:00:10,470 --> 00:00:17,190
So in this particular lecture, we are going to handle the button clicks of the add to cart button.

4
00:00:17,880 --> 00:00:24,910
And in order to do that, we actually need to go ahead and jump a little bit into some query and JavaScript.

5
00:00:25,290 --> 00:00:31,530
So the first thing which you need to do is you need to go ahead, go to Google and search for the equerry.

6
00:00:31,860 --> 00:00:36,090
And the first language pops up is going to be the official link from query.

7
00:00:36,660 --> 00:00:39,640
So Chinguetti is nothing but a library.

8
00:00:39,660 --> 00:00:46,470
It's actually a JavaScript library which allows us to actually work with the HTML dom, which is nothing

9
00:00:46,470 --> 00:00:48,840
but the HTML document object model.

10
00:00:49,230 --> 00:00:56,430
And the same query is going to help us handle the button clicks and the actions which happen after we

11
00:00:56,430 --> 00:00:57,720
handle the button clicks.

12
00:00:57,840 --> 00:00:59,790
So you can either download query.

13
00:00:59,790 --> 00:01:07,080
But the recommended way to use query is that you simply go ahead and click on this and query CDN and

14
00:01:07,080 --> 00:01:10,860
when you click over there you should actually get a bunch of links in here.

15
00:01:11,250 --> 00:01:12,750
So I can go ahead.

16
00:01:12,760 --> 00:01:19,650
I can select the latest version of Ceccarelli, which is three point X, and I will select this version

17
00:01:19,650 --> 00:01:27,570
which is uncompressed and I'll simply go ahead copy this particular script tag from here and I will

18
00:01:27,570 --> 00:01:29,610
go back to our code.

19
00:01:30,210 --> 00:01:37,380
And as we want to use this in the indexed HTML, I will go inside the head tag for index dot html,

20
00:01:37,980 --> 00:01:44,180
which is this tag right here, and I'll paste this particular link right over here.

21
00:01:47,830 --> 00:01:52,700
OK, so now once we have this particular jury link, we are pretty much good to go.

22
00:01:53,470 --> 00:01:57,020
And now let's go ahead and start writing some JavaScript code in here.

23
00:01:57,130 --> 00:02:03,940
So the very first thing which we people do here is that will actually go ahead and go right below here

24
00:02:03,950 --> 00:02:08,740
where the body tag ends and we can go ahead and write in our JavaScript code in here.

25
00:02:08,960 --> 00:02:14,760
So whenever you want to write in any JavaScript code, you always begin your code with the script tag.

26
00:02:15,160 --> 00:02:16,830
So you need to type and script.

27
00:02:17,980 --> 00:02:22,180
The type is going to be text slash JavaScript.

28
00:02:23,110 --> 00:02:29,050
You need to then close this and you can write all the JavaScript code associated with the HTML page

29
00:02:29,050 --> 00:02:29,420
in here.

30
00:02:29,440 --> 00:02:34,720
And one more thing which I would like to mention here is that you can use this particular same script

31
00:02:34,720 --> 00:02:37,630
tag or here in the head tag as well.

32
00:02:37,960 --> 00:02:45,040
But the script tag, which you write after the body tag actually loads up when the entire HTML page

33
00:02:45,040 --> 00:02:45,790
is loaded up.

34
00:02:46,210 --> 00:02:48,190
So what do I exactly mean by that?

35
00:02:48,730 --> 00:02:54,910
So let's say if you have this particular website, which has a bunch of elements in here, so we always

36
00:02:54,910 --> 00:03:01,720
want to make sure that we execute the JavaScript code once the page body actually loads up and henceforth

37
00:03:01,720 --> 00:03:07,120
the way in which we ensure that as by placing the script right after the body tag.

38
00:03:07,840 --> 00:03:13,840
OK, so now once we have JavaScript in here, in order to make sure of the JavaScript is actually working,

39
00:03:14,140 --> 00:03:17,640
I can simply go ahead and log something into the console.

40
00:03:18,010 --> 00:03:22,390
So the console is nothing but this interface right here, which we actually use.

41
00:03:22,780 --> 00:03:30,130
And I can simply say something like console, dot log and I can see something away here like let's say

42
00:03:30,910 --> 00:03:32,440
this is working.

43
00:03:33,830 --> 00:03:40,400
And add a semicolon in here, so with JavaScript, you do need to make sure that you have semicolon

44
00:03:40,400 --> 00:03:41,780
at the end of each line.

45
00:03:42,530 --> 00:03:49,160
So now if I go ahead and hit refresh, as you can see, we get the message over here as this is working.

46
00:03:49,850 --> 00:03:54,710
Now, let me actually go ahead and solve this issue a little bit.

47
00:03:54,720 --> 00:04:01,970
So for that will actually go ahead and get rid of the bootstrap JavaScript tag in here, because right

48
00:04:01,970 --> 00:04:03,920
now, at the moment, we don't need that.

49
00:04:04,760 --> 00:04:10,340
So let me just go ahead, get rid of this tag and hopefully that get rid of this error as well.

50
00:04:10,460 --> 00:04:10,820
OK.

51
00:04:10,850 --> 00:04:17,720
So as you can see, as soon as we remove that boot straps, just like we no longer have that error and

52
00:04:17,720 --> 00:04:21,170
we have a way here that this is working, that's all fine and good.

53
00:04:21,200 --> 00:04:23,400
OK, so now the JavaScript is working.

54
00:04:23,420 --> 00:04:30,260
We now need to go ahead and write in the logic here in order to go ahead and add items to the particular

55
00:04:30,260 --> 00:04:30,670
card.

56
00:04:31,310 --> 00:04:36,530
So even before we add items to the card, we actually need to create the card itself.

57
00:04:36,980 --> 00:04:42,200
So we need to create a card when the card does not exist in the local storage.

58
00:04:42,710 --> 00:04:44,580
So what do I actually mean by that?

59
00:04:44,990 --> 00:04:52,820
So now if I actually go ahead and type in local storage, not get item, and if I do something like,

60
00:04:53,000 --> 00:04:56,370
let's say card, let's see, what do we get?

61
00:04:56,840 --> 00:04:58,350
So then I go ahead and do that.

62
00:04:58,640 --> 00:05:05,060
It says that the value for card is null because right now we don't have any card over here because we

63
00:05:05,060 --> 00:05:07,340
have cleared up our local storage.

64
00:05:07,350 --> 00:05:09,950
So now we actually need to create a card.

65
00:05:10,190 --> 00:05:15,350
If the local storage not get item cards, value is equal to null.

66
00:05:15,380 --> 00:05:17,480
So now let's go ahead and create a card.

67
00:05:17,480 --> 00:05:20,570
And let's write the same logic in here so I can see.

68
00:05:20,570 --> 00:05:26,450
If so, remember that whatever we write over here is actually the JavaScript code and we will be using

69
00:05:26,450 --> 00:05:27,850
the JavaScript syntax.

70
00:05:28,190 --> 00:05:34,150
So it's pretty much the same as any other language like Shabwah or the C programming language.

71
00:05:34,160 --> 00:05:37,900
It's a little bit different from Python, but will get the hang of it.

72
00:05:38,330 --> 00:05:46,250
So we write F and in parentheses we type in local storage, don't get item and we had checked for the

73
00:05:46,250 --> 00:05:53,090
item we just got, so we checked if the card value is equal to null and if the value of this card is

74
00:05:53,090 --> 00:05:59,090
equal to null, then what we wish to do is that we actually wish to go ahead and create the card in

75
00:05:59,090 --> 00:05:59,340
here.

76
00:05:59,570 --> 00:06:02,810
So what do we exactly mean by creating the card in here?

77
00:06:03,350 --> 00:06:10,370
By creating a card, we just mean that we want to create a JavaScript variable in here so I can type

78
00:06:10,370 --> 00:06:15,300
in that card and that will actually go ahead and create a card in there.

79
00:06:16,180 --> 00:06:21,230
However, there's one small issue with this particular thing, and that issue is nothing.

80
00:06:21,230 --> 00:06:29,110
But in our card, we just don't want to do a single item and we just don't want to store the items ID

81
00:06:29,120 --> 00:06:29,690
itself.

82
00:06:29,700 --> 00:06:33,050
But we also want to store the quantity of items as well.

83
00:06:33,230 --> 00:06:40,580
So let's say, for example, if I click on this particular watch, add to cart button two times means

84
00:06:40,580 --> 00:06:43,750
that this word should be added twice into my card.

85
00:06:44,270 --> 00:06:50,660
So the card should not only keep track of the idea of the Swatch, but it should also keep track of

86
00:06:50,660 --> 00:06:51,870
the quantity as well.

87
00:06:52,820 --> 00:06:57,660
So in order to do that, we cannot have a simple data structure like a variable in here.

88
00:06:57,980 --> 00:07:01,100
Instead, we actually want something much more complex.

89
00:07:01,580 --> 00:07:06,140
So in that case, we will be using something which is called as an object.

90
00:07:06,500 --> 00:07:12,060
So, Jason, which is as when or people also like to call it Jason.

91
00:07:12,290 --> 00:07:17,300
So Jason is nothing, but it stands for JavaScript object notation.

92
00:07:18,180 --> 00:07:20,050
So we will be using that over here.

93
00:07:20,090 --> 00:07:26,660
So I'll type in card equals and I'll use these curly braces to mention that this is going to be adjacent

94
00:07:26,660 --> 00:07:27,110
object.

95
00:07:27,650 --> 00:07:32,660
Now, if you don't know how to use an object works, you don't need to worry at all.

96
00:07:32,660 --> 00:07:37,430
You will be able to understand what Jason Object looks like when we go ahead and actually store some

97
00:07:37,430 --> 00:07:38,630
items into the card.

98
00:07:39,260 --> 00:07:46,850
OK, so now this code basically means that we have checked if the card actually existed by using this

99
00:07:46,850 --> 00:07:52,910
particular logic right here and if this is equal to null means that the card does not exist.

100
00:07:52,910 --> 00:07:55,850
And henceforth we have created a card over here.

101
00:07:56,480 --> 00:08:02,570
So now in the upcoming lecture will go ahead and write in the code to actually handle the button.

102
00:08:02,570 --> 00:08:03,380
Click over here.

103
00:08:03,530 --> 00:08:10,130
So now once we have the card, now you want to receive the item inside that card when we click this

104
00:08:10,130 --> 00:08:10,520
button.

105
00:08:11,150 --> 00:08:15,590
So in the upcoming lecture, we will learn about handling the add to cart button click.

106
00:08:15,830 --> 00:08:19,760
So thank you very much for watching and I'll see you guys next time.

107
00:08:20,210 --> 00:08:20,810
Thank you.

