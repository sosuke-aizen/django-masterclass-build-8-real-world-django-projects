1
00:00:00,330 --> 00:00:07,710
Now, let's go ahead and do this final price into our database, so the steps to do this is actually

2
00:00:07,710 --> 00:00:08,490
quite simple.

3
00:00:08,490 --> 00:00:13,350
You just need to go ahead and make modifications to your model first.

4
00:00:13,570 --> 00:00:19,800
So let's first go to the model stockpile file and then here in the order, you can simply go ahead and

5
00:00:20,550 --> 00:00:22,710
add another field, which is called total.

6
00:00:23,650 --> 00:00:30,720
So I can say something like total equals models, DOT, and let's say we want to store this in the form

7
00:00:30,720 --> 00:00:31,490
of a character.

8
00:00:31,500 --> 00:00:37,900
So Coffield, the max length is going to be equal to, let's say, 200.

9
00:00:38,400 --> 00:00:44,180
So you can actually go ahead and stow this in an integer or load format as well if you want to.

10
00:00:44,190 --> 00:00:48,070
But for now I'm just going to use the Coffield for this.

11
00:00:48,640 --> 00:00:53,590
OK, so now let's go ahead and make the migration's as we have made changes to the model.

12
00:00:53,730 --> 00:00:58,360
So I'll say something like Python three manage dot.

13
00:00:58,680 --> 00:01:01,590
Why make migration's.

14
00:01:03,090 --> 00:01:10,020
And now let's go ahead and provide you one of default for everything, so that means it's basically

15
00:01:10,020 --> 00:01:16,680
now asking us that, OK, you have actually added this new field, but you already have certain items

16
00:01:16,680 --> 00:01:17,580
in your database.

17
00:01:17,910 --> 00:01:24,030
And for those particular orders, which we have in our database, they don't have a value for the total

18
00:01:24,030 --> 00:01:25,770
cost or the total amount.

19
00:01:26,250 --> 00:01:30,900
So now we can go ahead and provide a default value by typing in the option one.

20
00:01:31,080 --> 00:01:31,830
So in here.

21
00:01:37,370 --> 00:01:43,010
So in here, I have simply added the default value as one and now the new field has been created.

22
00:01:43,340 --> 00:01:52,340
So now let me just type in Python three managed by my greed and everything is not working well.

23
00:01:52,550 --> 00:01:57,130
Now, let's go ahead and see if we actually have that autofill in our database.

24
00:01:57,140 --> 00:02:05,900
So let's go to slash admen and let's just go ahead and make sure that the server is up and running.

25
00:02:07,220 --> 00:02:12,070
Python three managed IPY run server.

26
00:02:12,230 --> 00:02:14,060
And now let's go to the admin.

27
00:02:14,300 --> 00:02:15,500
Let's log in.

28
00:02:21,470 --> 00:02:26,510
And now if we have a look over here, as you can see, we have the total overhead here as well.

29
00:02:27,230 --> 00:02:32,420
And now let's go ahead and make the changes to the form as well.

30
00:02:33,020 --> 00:02:39,620
So now in here, if we go to the checkout page, as you can see, we don't currently have any fill up

31
00:02:39,620 --> 00:02:43,760
over here which actually accepts the order total.

32
00:02:44,150 --> 00:02:46,980
So now let's go ahead and add that fill up over here.

33
00:02:47,360 --> 00:02:51,980
So let's go ahead and create another form group for the total amount as well.

34
00:02:52,040 --> 00:02:58,670
So what I will simply do here is that I'll simply go ahead, copy this particular class and I'll paste

35
00:02:58,670 --> 00:02:59,480
it over here.

36
00:03:00,500 --> 00:03:04,460
And for this particular thing, let's go ahead and make a few changes.

37
00:03:04,470 --> 00:03:15,560
So instead of ZIP, I will say something like price or let's say I see something like the amount to

38
00:03:15,560 --> 00:03:18,020
be paid and for the input type.

39
00:03:18,260 --> 00:03:20,810
So now let's go ahead and delete everything from the input.

40
00:03:20,810 --> 00:03:23,750
Time for now and let's add new attributes over here.

41
00:03:24,380 --> 00:03:32,030
So this is going to be read-only and the type is going to be equal to the text and the class is going

42
00:03:32,030 --> 00:03:34,420
to be form control.

43
00:03:35,660 --> 00:03:42,200
And the idea for this is going to be total and the name for this is going to be total as well.

44
00:03:42,740 --> 00:03:46,470
So now once we go ahead and do that, we are pretty much good to go.

45
00:03:47,120 --> 00:03:54,260
Now, what we wish to do is that now we have this input field law here, which we can only read and

46
00:03:54,260 --> 00:03:56,430
we cannot add the manual value.

47
00:03:56,510 --> 00:04:03,470
So now we actually want to go ahead and get the total value of that particular thing from here and actually

48
00:04:03,470 --> 00:04:08,820
assign that value to this particular input file right here, which is nothing but the total.

49
00:04:09,260 --> 00:04:10,810
So let's go ahead and do that.

50
00:04:11,660 --> 00:04:17,209
So right over here, what I will do is that I'll simply go ahead and I'll get access to the input field.

51
00:04:17,660 --> 00:04:26,500
So I'll see dollar has total to get access to that particular field and I will set its value equal to

52
00:04:27,170 --> 00:04:27,620
total.

53
00:04:29,510 --> 00:04:35,120
So this total is this total, which we have right here and now, once we have modified the form and

54
00:04:35,120 --> 00:04:40,260
once we have modified the models as well, we need to now modify the view over here.

55
00:04:40,940 --> 00:04:46,810
So let's go ahead and at the end, add a new field, which is called less total.

56
00:04:46,820 --> 00:04:54,360
And that's actually going to be equal to requestor post doget and get the total from here.

57
00:04:54,620 --> 00:04:57,990
And now we also need to add to the lower here as well.

58
00:04:58,550 --> 00:05:01,400
So let's say total equals total.

59
00:05:02,630 --> 00:05:06,090
So now once we go ahead and do that, we are pretty much good to go.

60
00:05:06,350 --> 00:05:11,910
And now let's go ahead and see if the checkout thing works, though.

61
00:05:11,960 --> 00:05:13,880
Let's go back to our site.

62
00:05:15,630 --> 00:05:17,520
And now let me hit refresh.

63
00:05:18,690 --> 00:05:21,240
And now let's add a few items.

64
00:05:23,550 --> 00:05:30,280
And now we have the total let me add some damning details over here, and as you can see, for the amount

65
00:05:30,280 --> 00:05:35,850
to be paid, we now have this automatically full value and we cannot change that value up over here.

66
00:05:36,480 --> 00:05:40,320
So now when I click on Please Order, the order has now been placed.

67
00:05:40,800 --> 00:05:45,570
And now let's go to the admin and see if we actually have the total for that order.

68
00:05:45,810 --> 00:05:50,580
So this order, Object five is the latest one for the order which we have just placed.

69
00:05:50,610 --> 00:05:56,670
So if you click over here and if you scroll down, as you can see now, we also got the total added

70
00:05:56,670 --> 00:05:58,460
up over here in the database.

71
00:05:58,500 --> 00:06:00,600
So that's it for this lecture.

72
00:06:00,780 --> 00:06:07,470
And I hope you guys be able to understand how we have to the total value of our current order in our

73
00:06:07,470 --> 00:06:08,180
database.

74
00:06:08,190 --> 00:06:10,110
So thank you very much for watching.

75
00:06:10,110 --> 00:06:12,090
And I'll see you guys next time.

76
00:06:12,300 --> 00:06:12,900
Thank you.

