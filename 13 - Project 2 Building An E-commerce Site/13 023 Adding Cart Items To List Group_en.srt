1
00:00:00,150 --> 00:00:06,150
So now in this lecture, let's try to go ahead and populate this particular list group right here.

2
00:00:06,570 --> 00:00:12,450
So what we wish to do here is that we wish to get the items from our cart, which are actually present

3
00:00:12,450 --> 00:00:16,350
in our local storage, and we wish to display those items over here.

4
00:00:16,920 --> 00:00:23,260
So what we want to do is that we want to generate this particular list group right here dynamically.

5
00:00:23,310 --> 00:00:29,460
So if you have a look at this particular list items over here, they are currently being generated in

6
00:00:29,460 --> 00:00:35,880
a static manner, which means that if you actually go to the page, which is nothing but the check out

7
00:00:35,880 --> 00:00:41,510
page, as you can see, this is how we have generated the particular list items.

8
00:00:41,820 --> 00:00:44,420
So as you can see, this is our list group right here.

9
00:00:44,430 --> 00:00:48,320
And these are the items which we have added up statically.

10
00:00:48,570 --> 00:00:53,340
So instead of having these static items over here, we will go ahead and delete that.

11
00:00:53,520 --> 00:00:59,240
And instead we will actually create those items or those list items in a dynamic manner.

12
00:01:00,180 --> 00:01:04,940
So you make sure that you just have this an order list, glass and the rest.

13
00:01:04,950 --> 00:01:07,830
We are going to make it appear dynamically.

14
00:01:08,190 --> 00:01:13,260
So now once we get rid of all that, the next thing which we need to do is that we actually need to

15
00:01:13,260 --> 00:01:14,930
go ahead and writing a script.

16
00:01:14,940 --> 00:01:21,720
I go here and write in some JavaScript code to actually get the items stored in the local storage.

17
00:01:22,080 --> 00:01:28,410
So let's go right when the body tag ends and add a script tag over here and in there.

18
00:01:28,950 --> 00:01:35,250
Let's go ahead and type in the script type as text slash.

19
00:01:37,010 --> 00:01:42,830
JavaScript, and once we go ahead and do that, we now go ahead and first of all, get the items from

20
00:01:42,830 --> 00:01:43,870
our local storage.

21
00:01:43,880 --> 00:01:50,450
So in order to get the items from our local storage, first of all, we make use of the local storage

22
00:01:50,450 --> 00:01:56,440
and receive the items from the local storage to our local variable over here.

23
00:01:56,870 --> 00:01:59,180
So we check the local storage is null.

24
00:01:59,570 --> 00:02:07,940
So we check if the local storage don't get item and we are looking for an item which is nothing but

25
00:02:07,940 --> 00:02:08,380
cut.

26
00:02:08,990 --> 00:02:15,620
So if the card is equal to null means that we actually need to create a new variable called Lescott

27
00:02:16,130 --> 00:02:16,850
soil type.

28
00:02:16,850 --> 00:02:20,920
And VA got over here and this is going to be empty.

29
00:02:21,440 --> 00:02:24,220
So we have created a new variable right over here.

30
00:02:24,680 --> 00:02:31,880
Or else what we wish to do is that if the card is not equal to null means that if the card actually

31
00:02:31,880 --> 00:02:37,530
has some sort of items, we wish to go ahead and get those items from the local storage.

32
00:02:37,880 --> 00:02:46,670
So here we make use of this JavaScript variable card and we will equate it to Jason not pass.

33
00:02:46,850 --> 00:02:53,450
And we are going to pass the local storage item, which is called soil type in local storage, don't

34
00:02:54,200 --> 00:02:57,180
get item and will get the item which is cut.

35
00:02:58,190 --> 00:03:05,240
So what we have simply done here is that we have extracted the item, which is nothing but cut from

36
00:03:05,240 --> 00:03:06,230
the local storage.

37
00:03:06,560 --> 00:03:12,920
And then in order to see with any JavaScript object notation format, we have simply used Jaeson, not

38
00:03:12,920 --> 00:03:18,560
POS, which is going to pass this particular object and then it's going to serve it into this particular

39
00:03:18,560 --> 00:03:19,420
card right here.

40
00:03:19,760 --> 00:03:24,980
And this card is actually the variable, which is local to this particular JavaScript.

41
00:03:25,130 --> 00:03:31,820
OK, so now once we have access to this particular card means that we now have the card items to work

42
00:03:31,820 --> 00:03:32,130
with.

43
00:03:32,510 --> 00:03:38,780
Now once we have this particular card items, we can now loop through one of each card items which are

44
00:03:38,780 --> 00:03:44,710
present in this particular card, and we can assign these items to the list group.

45
00:03:44,900 --> 00:03:47,040
So how exactly are we going to do that?

46
00:03:47,450 --> 00:03:52,190
So first of all, to loop through all the items, let's write a for loop.

47
00:03:52,370 --> 00:03:59,600
So I'll see for and in order to loop through all the items inside the card, I will say for item and

48
00:03:59,600 --> 00:04:06,350
card, which means with every iteration, we will now have access to each item inside our card.

49
00:04:06,860 --> 00:04:14,090
And now in here we actually want to get the name of the item in the card and the quantity of the item.

50
00:04:14,540 --> 00:04:17,209
So will say let name equals.

51
00:04:17,570 --> 00:04:23,810
And in order to get the name of that particular item inside the car, I type in card, which is going

52
00:04:23,810 --> 00:04:30,740
to give me access to the card object and now we will specify the idea of that particular item in the

53
00:04:30,740 --> 00:04:31,080
card.

54
00:04:31,460 --> 00:04:37,820
So for example, if I want to access card whose ID is one, I can simply type in one over here.

55
00:04:38,300 --> 00:04:43,790
Now, in order to get this in a dynamic fashion, I can simply type an item over here, which means

56
00:04:43,790 --> 00:04:49,880
for the first iteration, the idea of that item is going to be equal to one or whatever that item is

57
00:04:49,880 --> 00:04:50,720
actually present.

58
00:04:51,080 --> 00:04:58,100
So, for example, if the item with an ID two is present as the first item, I will get to over here.

59
00:04:58,130 --> 00:05:04,670
So now after that, in order to access the name, we will mention the index as one because the name

60
00:05:04,670 --> 00:05:07,940
is located or saved at index position one.

61
00:05:08,420 --> 00:05:14,690
Whereas if you want to access the quantity, the quantity is placed or stored at the index position

62
00:05:14,690 --> 00:05:15,080
zero.

63
00:05:15,290 --> 00:05:22,310
So now if I want to access the quantity of the same item, I can say let let's see.

64
00:05:23,410 --> 00:05:31,630
Quantity equals and I can say cut, I will use the same index over here, which is item, but now as

65
00:05:31,630 --> 00:05:36,180
I actually want to access the quantity, I'll have an index as zero here.

66
00:05:36,880 --> 00:05:39,220
So I hope that this thing makes sense.

67
00:05:39,370 --> 00:05:46,030
And you have understood how exactly we have extracted the name and the quantity from those items.

68
00:05:46,540 --> 00:05:52,810
OK, so now once we have those got items, we now need to somehow go ahead and pass on the card items,

69
00:05:52,810 --> 00:05:54,950
name and quantity to the list group.

70
00:05:55,420 --> 00:06:01,300
So as I earlier mentioned, we are actually creating these lists groups in a dynamic manner.

71
00:06:01,450 --> 00:06:08,290
So here in order to do the same, I'll go ahead and create a string and I'll call this thing as item

72
00:06:08,290 --> 00:06:13,750
string, which is going to hold nothing but the name and the quantity of items.

73
00:06:14,200 --> 00:06:21,940
So in here, instead of simply passing in the name and the item quantity, I'll simply go ahead and

74
00:06:21,940 --> 00:06:23,680
create a list item over here.

75
00:06:24,010 --> 00:06:26,980
So in here we will use the JavaScript syntax.

76
00:06:27,400 --> 00:06:33,910
So use this particular symbol which is present to the left hand side of the number one on your keyboard.

77
00:06:34,150 --> 00:06:40,450
And remember that these are not actually the single codes, but they are actually a special symbol which

78
00:06:40,450 --> 00:06:42,970
allow us to write some HTML code in here.

79
00:06:43,000 --> 00:06:46,750
So this specifically is a new feature of JavaScript S6.

80
00:06:47,410 --> 00:06:51,720
So now let's go ahead and write in the list item tag over here.

81
00:06:51,730 --> 00:06:59,410
So I'll type in L.A. The class for this is going to be a list dash group.

82
00:07:01,030 --> 00:07:08,620
Dash item and then simply close the specific tag over here and also make sure that you end this double

83
00:07:08,620 --> 00:07:15,760
quotes in here and then you simply end this allied tag over here by typing in L.A. now, all here in

84
00:07:15,760 --> 00:07:23,200
order to access this particular name, JavaScript variable, simply type in a dollar sign and in curly

85
00:07:23,200 --> 00:07:24,950
braces, I'll simply type and name.

86
00:07:25,300 --> 00:07:32,160
So this way you are able to actually append a dynamic value inside this particular item string.

87
00:07:32,200 --> 00:07:38,070
Now, once you have particular name over here, you can also go ahead and append the quantity as well.

88
00:07:38,080 --> 00:07:44,620
So I can simply give a dash over here and I can see something like Dollo and I'll append the quantity

89
00:07:45,040 --> 00:07:47,620
over here by typing in quantity.

90
00:07:48,760 --> 00:07:55,720
So now once we have this particular string, we can now go ahead and simply append this particular string

91
00:07:56,080 --> 00:07:57,520
to item list.

92
00:07:57,730 --> 00:08:01,690
So the item list is nothing but this list right here.

93
00:08:01,720 --> 00:08:07,620
So currently, as you can see, this unordered list does not have any specific ideas.

94
00:08:08,050 --> 00:08:15,230
But what we wish to do is that we wish to upend this particular dynamic HTML to this particular list

95
00:08:15,280 --> 00:08:15,610
group.

96
00:08:15,790 --> 00:08:20,530
And henceforth we must assign some sort of an ID to this particular class.

97
00:08:21,010 --> 00:08:22,910
So let's actually go ahead and do that.

98
00:08:23,050 --> 00:08:30,670
So I'll see ID equals and then let's just assign it some ID like let's see item on the school list.

99
00:08:31,300 --> 00:08:31,650
OK.

100
00:08:31,690 --> 00:08:37,320
So now once we have an ID for that we can make use of the same ID and append elements to it.

101
00:08:37,570 --> 00:08:47,530
So I can say Dollo and then I'll use a hassane as an ID selector and I'll item on the school list and

102
00:08:47,620 --> 00:08:53,920
I'll type and up and which means that it needs to upend the item string which we have just created.

103
00:08:54,670 --> 00:09:00,850
So now hopefully this thing should work fine and also make sure that you enclose this thing in single

104
00:09:00,850 --> 00:09:01,320
quotes.

105
00:09:02,090 --> 00:09:07,770
OK, so once you're done with this, simply save the code and let's go back over here, hit refresh.

106
00:09:08,380 --> 00:09:12,040
And as you can see for the moment, it's showing empty.

107
00:09:12,040 --> 00:09:15,360
And that may be because we don't have any elements in our cart.

108
00:09:15,880 --> 00:09:18,750
So now let's go ahead and try to add some elements.

109
00:09:18,760 --> 00:09:24,910
And now if I go to the card and if I click on checkout, as you can see, for some real reason, we

110
00:09:24,910 --> 00:09:26,650
don't have anything up over here.

111
00:09:27,370 --> 00:09:33,130
So let's actually go to the console and see what do we have in our local storage.

112
00:09:34,170 --> 00:09:41,130
OK, so we actually had an issue there, because what happened is that when we actually ran the school,

113
00:09:41,190 --> 00:09:43,930
we forgot the Jaquie reeling right over here.

114
00:09:44,490 --> 00:09:50,760
So when you go ahead and add that link and when we went back and if we hit refresh, as you can see,

115
00:09:51,180 --> 00:09:53,730
we no longer now have an error over here.

116
00:09:53,730 --> 00:09:56,890
And we do have the items displayed up over here.

117
00:09:56,940 --> 00:10:02,660
And also, if you have a look over here, we again have this additional quotations over here.

118
00:10:02,790 --> 00:10:07,430
And that's because we have mistakenly added a quote over here.

119
00:10:07,470 --> 00:10:13,950
So if we get rid of that and if we hit refresh, as you can see, we now have the nice looking items

120
00:10:14,040 --> 00:10:17,400
stacked up on top of each other, along with the quantity as well.

121
00:10:17,610 --> 00:10:23,940
So that means if you have a look over here, we now have the items in a nice and appropriate fashion.

122
00:10:23,940 --> 00:10:27,460
So, you know, the number of items which we have in our cart.

123
00:10:27,690 --> 00:10:33,990
So now if we go ahead and let's say if I increment the laptop's quantity and now if I go to the card

124
00:10:33,990 --> 00:10:40,280
and if I try to check out, as you can see, as I clicked on the laptops, add to cart button twice.

125
00:10:40,560 --> 00:10:43,200
Now we have three items for the laptop over here.

126
00:10:43,530 --> 00:10:49,650
Now, our next job is to actually go ahead and create a check out form over here, which is going to

127
00:10:49,650 --> 00:10:56,940
accept the name, the e-mail address, your actual address, local address, the city, state and zip

128
00:10:56,940 --> 00:11:02,440
code so that the site owner can actually ship those items to your place.

129
00:11:02,910 --> 00:11:07,950
So in the next lecture, what we will do is that will go ahead and try to create a checkout form over

130
00:11:07,950 --> 00:11:10,620
here, which is going to accept all of your details.

131
00:11:11,010 --> 00:11:17,040
And we are going to see how we can submit this form along with the other items.

132
00:11:17,310 --> 00:11:19,230
So thank you very much for watching.

133
00:11:19,230 --> 00:11:21,120
And I'll see you guys next time.

134
00:11:21,360 --> 00:11:21,960
Thank you.

