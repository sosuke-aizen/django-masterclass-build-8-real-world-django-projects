1
00:00:01,450 --> 00:00:05,410
So now let's go ahead and create the super user.

2
00:00:05,620 --> 00:00:09,820
And check if we have a model and add some data to that model.

3
00:00:10,450 --> 00:00:18,820
So first of all, in order to create a super user, you type in Python three managed dot b y.

4
00:00:19,970 --> 00:00:22,490
We eat Super Bowl, use it.

5
00:00:23,150 --> 00:00:26,610
And I actually want my name to be the username, so.

6
00:00:26,930 --> 00:00:27,800
Press enter.

7
00:00:27,950 --> 00:00:30,290
Let me add some dummy e-mail address here.

8
00:00:31,560 --> 00:00:34,540
And let's see, the password is Super Bowl user.

9
00:00:35,000 --> 00:00:36,440
Super user.

10
00:00:37,280 --> 00:00:39,770
OK, so now the super user has been created.

11
00:00:40,070 --> 00:00:42,200
Okay, so now let's go ahead and run the server.

12
00:00:42,410 --> 00:00:47,420
So Python three managed by runs server.

13
00:00:49,100 --> 00:00:52,880
And let me open up the browser window.

14
00:00:53,240 --> 00:00:58,160
And in here, let's log in to the admin site.

15
00:00:58,400 --> 00:01:02,300
So one twenty seven point zero point zero point one.

16
00:01:02,680 --> 00:01:03,920
All in eight thousand.

17
00:01:05,600 --> 00:01:06,690
Slash admen.

18
00:01:07,580 --> 00:01:08,900
And let me just log in.

19
00:01:13,610 --> 00:01:18,470
And now, as you can see, we don't have a model right now because we actually need to register that

20
00:01:18,470 --> 00:01:20,620
model in the advent of PI.

21
00:01:21,140 --> 00:01:22,580
So let's import the model.

22
00:01:22,790 --> 00:01:23,870
So from DOT.

23
00:01:26,700 --> 00:01:33,000
That should be models input, and the model name is nothing but products.

24
00:01:33,390 --> 00:01:34,830
And now let's register that.

25
00:01:34,920 --> 00:01:39,080
So admen dot site, dot register.

26
00:01:41,340 --> 00:01:45,050
And in here, let's Possin products.

27
00:01:45,540 --> 00:01:53,310
And this should actually be product, not products, because if you now go to the admin site, it will

28
00:01:53,310 --> 00:01:56,820
say products and it will have an additional or over here.

29
00:01:56,970 --> 00:01:58,080
But it does not matter.

30
00:01:59,010 --> 00:02:02,940
It's just better to name your model as Cingular.

31
00:02:03,180 --> 00:02:06,240
Okay, so now let's go ahead and add some products in here.

32
00:02:06,660 --> 00:02:13,470
So let's say the first product, which we are going to add, is going to be a watch sue the type, an

33
00:02:14,160 --> 00:02:16,280
X, Y, Z watch.

34
00:02:17,190 --> 00:02:21,030
And let's see, the price of this product is going to be a hundred dollars.

35
00:02:21,510 --> 00:02:26,430
And let's say the discounted price is going to be something like seventy nine point ninety nine.

36
00:02:27,150 --> 00:02:32,670
And the category is going to be, let's say, fashion.

37
00:02:33,810 --> 00:02:36,990
And let's see, we add some Dumi description in here.

38
00:02:37,530 --> 00:02:38,510
So let's see.

39
00:02:38,700 --> 00:02:42,450
This is a very good watch.

40
00:02:43,440 --> 00:02:44,610
And now for the image.

41
00:02:45,090 --> 00:02:50,460
I'll show you guys how exactly can you use some images which are not copyright protected.

42
00:02:50,550 --> 00:02:58,320
So, first of all, you just go ahead and search for the image of a watch by going into Google and you

43
00:02:58,320 --> 00:03:01,320
cannot use these images as they are copyright protected.

44
00:03:01,830 --> 00:03:05,280
So when you go to Google and in here, you can go to tools.

45
00:03:05,520 --> 00:03:11,010
And in here, you need to go to usage rights and you need to select this option, which is label for

46
00:03:11,010 --> 00:03:12,540
use with modification.

47
00:03:13,470 --> 00:03:17,010
And that's actually going to make sure that they give you an image.

48
00:03:17,160 --> 00:03:20,880
And it's actually going to give you access to the images which you can use in your site.

49
00:03:21,390 --> 00:03:26,970
So now the color we are going to select is going to be transparent because we obviously want a transparent

50
00:03:26,970 --> 00:03:28,310
color for the size.

51
00:03:28,320 --> 00:03:31,590
I can just type in, uh, 720.

52
00:03:33,040 --> 00:03:34,780
By 720.

53
00:03:36,530 --> 00:03:42,360
And now, as you can see, we only have the images of the specific sizes here.

54
00:03:42,810 --> 00:03:45,600
So let's go ahead and pick this particular image.

55
00:03:46,140 --> 00:03:49,620
So when you click over here, let this image load up over here.

56
00:03:50,730 --> 00:03:56,080
And once the image is fully loaded up, you can simply go ahead and copy the image address.

57
00:03:56,460 --> 00:04:00,120
So once you have this image address, you can now go back to your Admon site.

58
00:04:00,150 --> 00:04:03,960
So let's go back to the admin side and paste in the you are lower here.

59
00:04:04,590 --> 00:04:09,870
So once we go ahead and click save, as you can see now we have our product over here.

60
00:04:10,710 --> 00:04:14,430
And whenever you want to retrieve the image, you actually retrieve the images.

61
00:04:14,430 --> 00:04:18,880
Spath and the browser is actually going to load up your image on your Web page.

62
00:04:18,910 --> 00:04:23,160
So now let's go ahead and add a few more products in here for our site.

63
00:04:23,370 --> 00:04:25,320
So let me just go ahead at the product.

64
00:04:25,620 --> 00:04:31,410
So let's say we add a few more products like EBC laptop.

65
00:04:31,890 --> 00:04:34,530
Let's say the price is going to be five hundred dollars.

66
00:04:34,560 --> 00:04:41,160
The discounted price is going to be 450 category is electronics.

67
00:04:43,380 --> 00:04:44,810
The description is, let's see.

68
00:04:45,930 --> 00:04:48,420
Oh, brand new.

69
00:04:49,200 --> 00:04:54,120
Fifteen inch laptop from the brand.

70
00:04:54,480 --> 00:04:55,170
ABC.

71
00:04:56,490 --> 00:05:00,820
And for the image, let's go ahead and search for laptop.

72
00:05:02,310 --> 00:05:04,410
And you should have an image over here.

73
00:05:04,590 --> 00:05:10,530
So for this size, you actually do need to make sure that the product size is quite appropriate.

74
00:05:11,160 --> 00:05:13,710
Or else we are going to have an issue.

75
00:05:14,960 --> 00:05:21,540
And I don't know, for some reason, Google is not displaying the option, which allows users to actually

76
00:05:21,540 --> 00:05:23,620
select the specific size.

77
00:05:23,880 --> 00:05:26,490
But earlier it used to show that particular option.

78
00:05:26,940 --> 00:05:30,000
But for some weird reason, it's not showing me the option currently.

79
00:05:30,030 --> 00:05:36,120
So if you go to Google and if you click on sites, it should also allow you to specify the exact size

80
00:05:36,120 --> 00:05:36,810
of the image.

81
00:05:37,860 --> 00:05:43,110
So we actually want to use the images of these same size so that it looks consistent.

82
00:05:43,500 --> 00:05:46,700
OK, so this image actually looks of a similar size.

83
00:05:46,710 --> 00:05:48,510
So I'm going to go ahead and use that.

84
00:05:49,710 --> 00:05:52,350
So let me just copy the image address.

85
00:05:53,360 --> 00:05:57,420
And now let's go back here at the image up over here and click on Save.

86
00:05:58,940 --> 00:06:00,430
And let's add one more product.

87
00:06:00,460 --> 00:06:00,880
Let's see.

88
00:06:00,910 --> 00:06:08,770
This is going to be e bike price of this bike is going to be, let's say, three hundred bucks.

89
00:06:09,430 --> 00:06:15,040
Discounted prices, 200 category is sports.

90
00:06:16,180 --> 00:06:18,100
And let's add some Dumi description.

91
00:06:18,340 --> 00:06:19,780
This is a.

92
00:06:21,310 --> 00:06:22,480
By and for the image.

93
00:06:22,540 --> 00:06:25,000
Let's go ahead and search for PYC.

94
00:06:32,340 --> 00:06:34,270
And let me just use this image.

95
00:06:34,930 --> 00:06:37,910
Copy the image, address the space it in.

96
00:06:37,960 --> 00:06:38,410
Oh, here.

97
00:06:39,630 --> 00:06:39,870
Okay.

98
00:06:39,910 --> 00:06:45,220
So now once we have these products in here in the upcoming lecture, we can actually go ahead and start

99
00:06:45,220 --> 00:06:48,340
working on the page, which displays these products.

100
00:06:48,970 --> 00:06:52,690
So for now, these are just some objects which we have in our database.

101
00:06:52,720 --> 00:06:58,030
But in the upcoming lecture, we will go ahead and define the next page to list out these products.

102
00:06:58,270 --> 00:06:59,980
So thank you very much for watching.

103
00:07:00,040 --> 00:07:01,870
And I'll see you guys next time.

104
00:07:02,200 --> 00:07:02,730
Thank you.

