1
00:00:00,240 --> 00:00:01,650
And welcome to this lecture.

2
00:00:01,770 --> 00:00:07,610
So in this lecture, we will try to go ahead and create a navigation bar on our website.

3
00:00:07,650 --> 00:00:12,450
So in that particular navigation bar, we will have the menu of our website.

4
00:00:12,450 --> 00:00:17,740
And also we are going to display the number of items which we have in our cart.

5
00:00:18,210 --> 00:00:24,120
So now, in order to add navigation bar, you simply can go ahead and add or create your navigation

6
00:00:24,120 --> 00:00:24,940
bar over here.

7
00:00:25,320 --> 00:00:31,410
And in order to save time, instead of actually writing the code for navigation bar, we will go ahead

8
00:00:31,410 --> 00:00:36,000
and directly use the navigation bar from the bootstraps official site.

9
00:00:36,030 --> 00:00:43,740
So in order to get the navigation bar, simply go ahead and search for bootstrap navigation bar on the

10
00:00:43,740 --> 00:00:44,500
Web browser.

11
00:00:44,550 --> 00:00:49,260
And if you scroll down a little bit, this is the link which you want to actually click.

12
00:00:49,320 --> 00:00:51,720
So this is the official link from Bootstrap.

13
00:00:52,020 --> 00:00:57,810
And there he'll actually have a bunch of navigation bar, which we can actually use in our Web site.

14
00:00:57,840 --> 00:01:03,660
So as you can see, if I keep on scrolling down, I get different kinds of navigation bar and let's

15
00:01:03,660 --> 00:01:06,850
say we want to use this particular navigation bar right here.

16
00:01:07,980 --> 00:01:11,580
So in order to do that, I'll simply copy this code.

17
00:01:12,800 --> 00:01:21,410
And then let's go back to our code editor and in here, let's paste this code in here, but even before

18
00:01:21,410 --> 00:01:27,330
pasting this code, it's actually better if you have a row and column in here inside the container.

19
00:01:27,440 --> 00:01:33,560
So you need to go right inside the container, right before where this rule actually starts.

20
00:01:33,860 --> 00:01:39,710
And in here, I simply need to create another div and it'll say the glass is equal to.

21
00:01:42,830 --> 00:01:49,750
And then I'll simply go ahead and create a column as well, sort of class is equal to call Dash and

22
00:01:50,270 --> 00:01:51,170
Dash 12.

23
00:01:52,130 --> 00:01:55,280
And in here, I'm going to paste in my navigation bar.

24
00:01:55,940 --> 00:01:58,840
So now let's see how the navigation bar looks like.

25
00:01:58,850 --> 00:02:01,990
So let me just go ahead and activate the server first.

26
00:02:02,090 --> 00:02:05,450
Once the subway is up and running, I can simply go to the homepage.

27
00:02:06,910 --> 00:02:11,770
And in here, if I see you here, as you can see, we have this nav bar right over here.

28
00:02:11,890 --> 00:02:17,530
So if I minimize this a little bit, as you can see, we have this navigation bar right over here and

29
00:02:17,530 --> 00:02:19,870
right where this pricing is actually placed.

30
00:02:19,870 --> 00:02:25,930
Let's say you want to go ahead and change it to display the card items inside our car.

31
00:02:26,350 --> 00:02:28,800
So how exactly can we go ahead and do that?

32
00:02:29,050 --> 00:02:32,490
So in order to do that, you simply need to go back to your code.

33
00:02:32,920 --> 00:02:39,880
And in here, if you go to the JavaScript right here, as you can see, we actually have the card items

34
00:02:39,880 --> 00:02:40,610
stored in here.

35
00:02:41,110 --> 00:02:46,970
So from this, we can actually obtain the number of items which this particular card has.

36
00:02:47,320 --> 00:02:51,120
So in order to do that, you simply need to go ahead, go right here.

37
00:02:51,610 --> 00:02:56,270
And first of all, you need to get access to the length of the card.

38
00:02:56,290 --> 00:03:02,320
So in order to get access to the length of the card, you simply type an object dot keys.

39
00:03:02,890 --> 00:03:07,330
And in here we actually want to get the objects from the card.

40
00:03:07,600 --> 00:03:09,100
So I'll type in card here.

41
00:03:09,100 --> 00:03:15,490
And then instead of getting those objects, we actually want the length of that particular card so I

42
00:03:15,490 --> 00:03:17,290
can type in Lento here.

43
00:03:17,680 --> 00:03:23,050
So this is actually going to give me the length of the number of items which we currently have in the

44
00:03:23,050 --> 00:03:23,460
card.

45
00:03:23,950 --> 00:03:29,230
And now in order to test if this thing actually works, let's log this into a console.

46
00:03:29,230 --> 00:03:36,310
So I'll type in console dot log and I'll include this code in there so that we can display this on the

47
00:03:36,310 --> 00:03:36,940
console.

48
00:03:37,660 --> 00:03:46,150
So now if we see this code and we go back to our website and now if I just open up the console, let's

49
00:03:46,150 --> 00:03:47,230
see what do we get here?

50
00:03:47,770 --> 00:03:50,050
So right now, as you can see, we get nothing.

51
00:03:50,080 --> 00:03:55,600
So when I add this item, the value of card is actually displayed as one.

52
00:03:56,050 --> 00:04:02,560
So now if I keep adding this item, I'll still get one over here that because this is actually a single

53
00:04:02,560 --> 00:04:07,990
item and the card actually only holds the total number of unique items.

54
00:04:08,050 --> 00:04:13,910
So now if I actually want to make it, do I have to add another product which is not this product?

55
00:04:13,990 --> 00:04:20,620
So if I click on this, as you can see, the value gets assigned as to now, if I click on this bike,

56
00:04:20,620 --> 00:04:25,930
the value now becomes three, which means that we have three unique items in that particular card.

57
00:04:26,560 --> 00:04:31,480
And now if I keep on clicking this, as you can see, the number does not change unless and until I

58
00:04:31,480 --> 00:04:33,040
click on a unique item.

59
00:04:33,070 --> 00:04:38,260
So now if I click on this, as you can see, the number of items on the card become full.

60
00:04:39,400 --> 00:04:40,980
So that's all fine and good.

61
00:04:40,990 --> 00:04:47,710
We know how to actually go ahead and display the number of items in the console, but now we actually

62
00:04:47,710 --> 00:04:51,640
want to display that up over here in place of this pricing.

63
00:04:52,000 --> 00:04:54,370
So how exactly are we able to do that?

64
00:04:54,760 --> 00:04:58,210
So in order to do that, we simply go back to our code.

65
00:04:58,330 --> 00:05:05,320
And now, first of all, using the document object model, we will get access to this particular navigation

66
00:05:05,320 --> 00:05:07,120
bar item over here.

67
00:05:07,300 --> 00:05:13,810
So if you actually want to look up this item in your code, so if you go up over here, this is the

68
00:05:13,810 --> 00:05:15,380
pricing item right over here.

69
00:05:15,520 --> 00:05:21,460
And for this, you can actually go ahead and assign it some kind of an ID so you can see something like

70
00:05:21,820 --> 00:05:24,700
ID equals to let's see God.

71
00:05:25,670 --> 00:05:31,930
And now once we have assigned an idea to this particular element, you can now access this element using

72
00:05:31,930 --> 00:05:33,360
the document object model.

73
00:05:33,880 --> 00:05:36,450
So you will be using a little bit of JavaScript.

74
00:05:36,940 --> 00:05:46,000
So I'll say something like document dot, get element by ID and will get the element, which is nothing

75
00:05:46,000 --> 00:05:48,520
but which has the I.D. card.

76
00:05:48,940 --> 00:05:56,320
So now once we have access to that particular card, we want to set it in an e-mail to this specific

77
00:05:56,320 --> 00:06:04,300
value so I can see DOT in HDMI equals and I'll simply go ahead, cut this from here.

78
00:06:05,470 --> 00:06:11,350
And I'll simply assign this value up over here and let's get rid of the council, not block from here

79
00:06:12,310 --> 00:06:15,100
and now, if we do so, let's see what do we get?

80
00:06:15,130 --> 00:06:18,820
So now if I had refresh and if I add this item.

81
00:06:19,970 --> 00:06:25,550
As you can see, this thing became full, which is the number of items inside the cart, but instead

82
00:06:25,550 --> 00:06:31,790
of just full, let's say you want to display something like card and then display full in parentheses.

83
00:06:32,030 --> 00:06:33,840
So we can also do that as well.

84
00:06:34,520 --> 00:06:40,220
So in there, we will simply go ahead and type in this equals and in quotations.

85
00:06:40,220 --> 00:06:42,230
We want to display card as a string.

86
00:06:42,380 --> 00:06:48,020
And then I'll include the opening parentheses here and we will open this with the actual number which

87
00:06:48,020 --> 00:06:48,980
we get from here.

88
00:06:49,280 --> 00:06:53,090
And I'll again append this with a closing parenthesis right over here.

89
00:06:54,020 --> 00:06:59,400
So now once we go ahead and see if that we should be actually able to get the appropriate result.

90
00:06:59,420 --> 00:07:03,200
So if I go ahead, hit refresh and click on Add to Cart Button.

91
00:07:03,330 --> 00:07:07,230
As you can see, we get the card and the number of items inside our card.

92
00:07:07,250 --> 00:07:12,560
And one more thing which you can do here is that instead of this pricing, you can set the by default

93
00:07:12,560 --> 00:07:16,070
value of this to, let's say, card of zero.

94
00:07:18,210 --> 00:07:23,040
So that actually makes sense when you go ahead and hit refresh, you have zero items and then we keep

95
00:07:23,040 --> 00:07:23,970
on adding items.

96
00:07:24,270 --> 00:07:25,900
The value over here changes.

97
00:07:26,370 --> 00:07:27,920
So that's it for this lecture.

98
00:07:27,960 --> 00:07:34,260
And I hope you guys be able to understand how to actually go ahead and create a navigation bar and assign

99
00:07:34,260 --> 00:07:36,840
the number of items inside the car over here.

100
00:07:37,500 --> 00:07:43,200
So in the next election, we will go ahead and we will make this card more functional by adding a popover

101
00:07:43,200 --> 00:07:48,510
to this card, which means that whenever you go ahead and click on this particular card, you should

102
00:07:48,510 --> 00:07:54,570
actually get a pop over here, which displays the number of actual items which are present in your card.

103
00:07:55,500 --> 00:07:59,430
So thank you very much for watching and I'll see you guys next time.

104
00:07:59,640 --> 00:08:00,210
Thank you.

