1
00:00:00,970 --> 00:00:07,260
Now, in order to store these values up into the JSON object, let's go ahead and write in some code.

2
00:00:07,900 --> 00:00:14,020
So the way in which we are going to store these values into the card is we are not actually going to

3
00:00:14,020 --> 00:00:20,710
go ahead and simply assign code equals item ID, because what that would actually do is that that would

4
00:00:20,710 --> 00:00:24,380
simply override if we go ahead and add a new value in here.

5
00:00:24,850 --> 00:00:31,060
So instead of doing that, we are actually going to go ahead and see if the values inside the card in

6
00:00:31,060 --> 00:00:32,040
a key value pair.

7
00:00:32,409 --> 00:00:38,460
So what I exactly mean by that show, the card which we are creating here, is adjacent object.

8
00:00:38,470 --> 00:00:43,990
It sort of functions like an array, which means that we have a specific index for a card.

9
00:00:43,990 --> 00:00:47,680
So which means in order to store the item in the card, period.

10
00:00:47,710 --> 00:00:48,100
All right.

11
00:00:48,100 --> 00:00:54,850
Here we actually need to go ahead and type in card and then we can assign an index value to this particular

12
00:00:54,850 --> 00:00:55,210
card.

13
00:00:55,270 --> 00:00:59,230
So I can say card of zero and I can save some value in here.

14
00:00:59,230 --> 00:01:06,180
Like one I can see card of, let's say three and I can see some value in here six.

15
00:01:06,910 --> 00:01:12,520
So the way in which we are going to store values in a card is that we are not going to store IDs in

16
00:01:12,520 --> 00:01:12,760
here.

17
00:01:13,300 --> 00:01:19,910
Instead, we are actually going to treat this index values or the key values as the item ID.

18
00:01:20,050 --> 00:01:28,000
So for example, if I want to store the item ID one, I can simply go ahead and type in card of one

19
00:01:28,000 --> 00:01:35,500
here and then this one or any value over here is going to represent the amount of items whose ID is

20
00:01:35,500 --> 00:01:35,790
one.

21
00:01:36,160 --> 00:01:41,420
So let's say, for example, we are storing the watch variable or the watch product in here.

22
00:01:41,890 --> 00:01:44,380
So the ID of our watch is one.

23
00:01:44,680 --> 00:01:52,060
So in order to save our watch, I'll simply say card of one, which means we want to say watch and I'll

24
00:01:52,060 --> 00:01:53,660
equate this to the quantity.

25
00:01:53,920 --> 00:01:58,120
So let's say if I want to watch as I sign this thing, the value of two.

26
00:01:58,600 --> 00:02:01,690
So if I want to steal a bike, I can say card.

27
00:02:02,080 --> 00:02:05,410
And then the idea for the bike is actually three.

28
00:02:05,420 --> 00:02:10,430
So I can see a card of three equals and let's see if I only want a single bike.

29
00:02:10,690 --> 00:02:16,660
So this is the man or this is the way in which will be storing the objects inside our card.

30
00:02:17,590 --> 00:02:24,970
So in here and of these values, I can simply go ahead and use item I.D. because this value here is

31
00:02:24,970 --> 00:02:27,880
nothing but the item ID, which we just got from here.

32
00:02:28,120 --> 00:02:34,280
So now I hope you guys understood the way in which we are going to store values inside this particular

33
00:02:34,290 --> 00:02:35,230
adjacent object.

34
00:02:35,500 --> 00:02:37,960
OK, so now let's implement this in practice.

35
00:02:38,650 --> 00:02:45,330
So first of all, we actually want to check if a particular item is present in the card.

36
00:02:45,460 --> 00:02:49,430
So let's see if you want to check if the watch is present in the card.

37
00:02:49,450 --> 00:02:59,810
I can simply type in if card and I can see the ID has one because one is actually the ID for a watch.

38
00:03:00,460 --> 00:03:07,330
So if I see if this is not equal to undefined, which means that the watch is actually present in the

39
00:03:07,330 --> 00:03:12,220
card, so if the watch was not present, this would actually turn out to be false.

40
00:03:12,370 --> 00:03:19,270
So if the card is not undefined, means it's actually defined and if it's defined, which means that

41
00:03:19,270 --> 00:03:22,620
we already have that watch in our cart.

42
00:03:23,080 --> 00:03:30,460
So in that case, what I want to do is that I simply want to increment the value of a watch over here

43
00:03:30,460 --> 00:03:32,860
or the quantity of watch over here by one.

44
00:03:33,610 --> 00:03:37,750
So in order to access the quantity of our watch, I can simply type in card.

45
00:03:39,670 --> 00:03:46,480
Of one, and I'll type cut of one equals cut off one.

46
00:03:47,590 --> 00:03:53,900
Plus one, because obviously I want to implement the value of my watch now all year.

47
00:03:54,250 --> 00:03:58,290
We are not actually talking about watch, but we are talking about all the products.

48
00:03:58,570 --> 00:04:03,830
So instead of this one value in here, I can simply go ahead and use it on my head.

49
00:04:04,330 --> 00:04:07,360
So I hope these things are making sense now.

50
00:04:07,390 --> 00:04:14,230
So this actually means that if a particular item is actually present in your cart, then you go ahead

51
00:04:14,230 --> 00:04:20,519
and simply increment its value by one, because we obviously want our site to work in that fashion.

52
00:04:20,860 --> 00:04:25,570
So let's see if I already have this particular item inside a cart.

53
00:04:25,720 --> 00:04:28,300
I don't want to add this item one more time.

54
00:04:28,480 --> 00:04:33,700
Instead, I just want to go ahead and increment the quantity of that particular item.

55
00:04:34,150 --> 00:04:36,340
So this is what exactly our code does.

56
00:04:36,760 --> 00:04:42,400
It takes that particular value, which is the quantity and increments, that quantity by one.

57
00:04:43,510 --> 00:04:46,600
So I hope this thing makes sense else.

58
00:04:46,600 --> 00:04:52,600
And in the end spot, we are going to write in the code, which means that the item has never been added

59
00:04:52,600 --> 00:04:53,240
to the card.

60
00:04:53,260 --> 00:04:56,680
So this is the first time we are adding that item into the card.

61
00:04:57,220 --> 00:05:03,610
So in that case, what we simply wish to do is that we simply wish to make the value or the make the

62
00:05:03,610 --> 00:05:06,060
quantity of that item equal to one.

63
00:05:06,430 --> 00:05:08,830
So I'll type in court.

64
00:05:11,010 --> 00:05:12,900
Item ID equals one.

65
00:05:13,800 --> 00:05:20,430
So this simply means that if I go ahead and if I click on this button and if the watch was not actually

66
00:05:20,430 --> 00:05:26,130
present inside the card, I simply want to make what quantity equal to one.

67
00:05:27,090 --> 00:05:29,370
So I hope these things actually make sense.

68
00:05:29,400 --> 00:05:35,400
So now once we know how this thing works, let's actually go ahead and try to print out this particular

69
00:05:35,400 --> 00:05:36,240
card right here.

70
00:05:37,680 --> 00:05:45,060
So now if I go ahead and type in console, dot, log and card, that will actually print my card on

71
00:05:45,060 --> 00:05:45,750
the console.

72
00:05:46,410 --> 00:05:53,520
So now if I go ahead and go to the browser and refresh, if I click on Add to Cart, as you can see,

73
00:05:53,730 --> 00:05:56,250
we get one one over here.

74
00:05:56,430 --> 00:06:01,410
So this specific one means the idea of the product, which we have just added.

75
00:06:01,440 --> 00:06:06,150
So we have just added the watch over here and hence we got the value as one.

76
00:06:06,300 --> 00:06:11,070
And this one over here means the quantity of that particular item, which is watch.

77
00:06:11,590 --> 00:06:16,830
So when I go ahead and click this button one more time, as you can see now, the quantity has increased

78
00:06:16,830 --> 00:06:17,310
to two.

79
00:06:18,090 --> 00:06:22,960
Now, let's see what happens when we click on the laptops, add to cart button.

80
00:06:22,980 --> 00:06:28,770
So when I click that, as you can see, we have second object added up over here, which is laptop,

81
00:06:28,800 --> 00:06:29,940
whose ID is too.

82
00:06:30,360 --> 00:06:34,110
But the quantity of laptops which we have in our cart are only one.

83
00:06:34,320 --> 00:06:36,010
So when I click that one more time.

84
00:06:36,030 --> 00:06:37,560
Now it's quantity becomes two.

85
00:06:38,070 --> 00:06:42,390
When I click this one more time, the quantity of laptop actually becomes three.

86
00:06:42,840 --> 00:06:48,810
But the quantity of water still remains too, right over here, which means that now we're able to actually

87
00:06:48,810 --> 00:06:50,200
print out our card.

88
00:06:51,780 --> 00:06:59,370
Now, once we are able to do that now, let's go ahead and actually learn how to save this card inside

89
00:06:59,520 --> 00:07:00,630
our local storage.

90
00:07:00,720 --> 00:07:07,020
So remember, this card value or discount variable is actually the JavaScript adjacent variable, which

91
00:07:07,020 --> 00:07:08,570
we have just created over here.

92
00:07:09,060 --> 00:07:16,320
But now you also need to make sure that we receive this particular card inside a local storage, because

93
00:07:16,320 --> 00:07:22,500
what would happen is that if I go ahead and just hit refresh and if I again, click on this button right

94
00:07:22,500 --> 00:07:26,600
here, as you can see, my card is now absolutely clear.

95
00:07:26,640 --> 00:07:31,190
That is whatever items which I had added previously, I now vanished.

96
00:07:31,680 --> 00:07:37,950
So in order to avoid this, we go ahead and make sure that we receive this card value into our local

97
00:07:37,950 --> 00:07:38,720
storage card.

98
00:07:39,240 --> 00:07:41,400
So saving that is pretty much simple.

99
00:07:41,430 --> 00:07:49,410
I can simply drive in local storage towards that item and then we set the value for court and we are

100
00:07:49,410 --> 00:07:52,130
going to set the value of KROT over here.

101
00:07:52,140 --> 00:07:58,470
But even before we actually assign that value directly over here, we need to use the Jason don't screen

102
00:07:58,470 --> 00:08:02,040
you for a method which will actually convert that value into a string.

103
00:08:02,610 --> 00:08:09,410
So I'll type in Jason Dawid swingy file and only then I'll go ahead and pass this card value in here.

104
00:08:09,630 --> 00:08:12,750
So simply copy that and be starting up over here.

105
00:08:12,780 --> 00:08:16,420
So now let's end this with semicolon and now we're good to go.

106
00:08:16,950 --> 00:08:19,100
And now let's see how this thing works out.

107
00:08:19,110 --> 00:08:20,880
So I'll go ahead, hit refresh.

108
00:08:20,880 --> 00:08:22,020
So I'll add this.

109
00:08:22,050 --> 00:08:23,700
So these items are added.

110
00:08:24,330 --> 00:08:26,090
I'll add a laptop as well.

111
00:08:27,360 --> 00:08:33,450
And now if I go ahead and hit refresh and now if I go ahead and try to add this bike, as you can see,

112
00:08:33,780 --> 00:08:40,679
the bike is added as well as the items which we had previously also now retained because we are actually

113
00:08:40,679 --> 00:08:43,770
saving all of this got into our local storage.

114
00:08:43,780 --> 00:08:45,360
So that's it for this lecture.

115
00:08:45,420 --> 00:08:51,930
And I hope you guys be able to understand how we have designed this particular add to cart functionality.

116
00:08:52,170 --> 00:08:57,090
The functionality is not actually complete yet, but we are actually done with the ground work, which

117
00:08:57,090 --> 00:08:59,360
is associated with creating a card.

118
00:08:59,910 --> 00:09:05,760
So in the next lecture, what we will do is that we will actually try to display the number of items

119
00:09:05,760 --> 00:09:09,630
in our cart and do some navigation bar over here.

120
00:09:09,720 --> 00:09:16,560
So in the next lecture will add enough buy in here and will try to display the value of our card items

121
00:09:16,560 --> 00:09:18,360
in that particular navigation bar.

122
00:09:18,510 --> 00:09:22,650
So thank you very much for watching and I'll see you guys next time.

123
00:09:22,680 --> 00:09:23,220
Thank you.

