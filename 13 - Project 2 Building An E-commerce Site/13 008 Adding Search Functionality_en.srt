1
00:00:00,060 --> 00:00:06,840
So now let's go ahead and try to add the search functionality to our site, so in here, what we will

2
00:00:06,840 --> 00:00:14,220
attempt to do is we will add a search bar at the top of all the products which we have, and the search

3
00:00:14,220 --> 00:00:19,950
bar is going to be nothing, but it's going to be a text box and it's going to have a small button which

4
00:00:19,970 --> 00:00:20,930
says search.

5
00:00:21,210 --> 00:00:28,170
So let's first go ahead and actually add that search in the template using HTML and then we will go

6
00:00:28,170 --> 00:00:31,650
ahead and make it functional using Python code.

7
00:00:31,680 --> 00:00:35,970
So now let's switch back to the e-mail page.

8
00:00:36,270 --> 00:00:41,130
And in here, as you can see, we have a container and inside that container, we have placed zero for

9
00:00:41,130 --> 00:00:42,630
displaying the actual products.

10
00:00:43,080 --> 00:00:49,680
So let's go inside the container and create a different rule, a completely different role for adding

11
00:00:49,680 --> 00:00:50,450
a search bar.

12
00:00:50,610 --> 00:00:54,470
So I'll type and div that's going to be class rule.

13
00:00:55,290 --> 00:01:01,890
And in here, the very first thing which we do in order to add the search bar is that we will first

14
00:01:01,890 --> 00:01:07,890
of all go ahead and create a column which spans the entire 12 columns.

15
00:01:08,190 --> 00:01:09,450
So I'll type in div.

16
00:01:10,680 --> 00:01:19,470
Plus, called Dash and Dash 12, and once we have this column, instead of directly creating the search

17
00:01:19,470 --> 00:01:23,490
bar or the input field, we will go ahead and create that up in a car.

18
00:01:23,530 --> 00:01:25,650
So, again, go ahead, create a div.

19
00:01:25,920 --> 00:01:30,960
And the class for this is going to be called and I'll type in card s.m.

20
00:01:31,860 --> 00:01:35,600
And once we have this card, we can add the card body as well.

21
00:01:35,730 --> 00:01:40,140
So I'll type and div class equals God dash body.

22
00:01:40,140 --> 00:01:46,740
First of all, I'll go ahead and create a row in here so I'll type and rule as the class as well and

23
00:01:46,740 --> 00:01:49,230
I'll type no gateaux.

24
00:01:50,350 --> 00:01:54,550
And we want to align items to the center, so I'll type in a line.

25
00:01:56,470 --> 00:01:58,390
Items center.

26
00:01:58,810 --> 00:02:04,810
So these are nothing, but they are just the prespecified bootstrap classes, which we are using to

27
00:02:04,810 --> 00:02:06,890
style the particular input element.

28
00:02:06,910 --> 00:02:09,360
So once we have that, we are pretty much good to go.

29
00:02:09,370 --> 00:02:12,110
So we have a room in here and in there.

30
00:02:12,460 --> 00:02:14,260
Let's go ahead and create a column.

31
00:02:14,650 --> 00:02:15,910
So I'll type and div.

32
00:02:17,730 --> 00:02:24,040
Plus, that's going to be column and in that column, we are going to have our input field, soil type

33
00:02:24,060 --> 00:02:24,660
and input.

34
00:02:27,500 --> 00:02:34,790
The type is going to be search and let's go ahead and assign some name to this particular search field,

35
00:02:34,790 --> 00:02:41,870
so I'll type a name equals, let's say item name because we'll be searching for items here and then

36
00:02:41,990 --> 00:02:44,390
let's assign a placeholder to this.

37
00:02:44,390 --> 00:02:50,150
So placeholder is nothing, but it's something which our application is going to display.

38
00:02:50,570 --> 00:02:51,740
So I'll type in.

39
00:02:54,010 --> 00:03:01,810
Search for products, so this is what is going to be displayed inside the text field and then let's

40
00:03:01,810 --> 00:03:04,480
add some genetic bootstrap classes in here.

41
00:03:04,870 --> 00:03:09,610
So as this is going to be the input field, we are going to add a few classes.

42
00:03:10,390 --> 00:03:20,530
So I'll type in class equals and that's going to be form control and then form.

43
00:03:22,620 --> 00:03:26,400
Control, that's going to be bodiless.

44
00:03:27,540 --> 00:03:32,820
OK, so once we have that, we are pretty much done designing this particular column, which is nothing

45
00:03:32,820 --> 00:03:34,110
but the input field.

46
00:03:34,470 --> 00:03:37,760
Now you also need to go ahead and define a button as well.

47
00:03:37,770 --> 00:03:41,130
So we are going to do that in a different column, soil type.

48
00:03:41,130 --> 00:03:45,780
And if the class is going to be called dash auto.

49
00:03:47,280 --> 00:03:57,660
And in here, let's define a button, which will have a class as written between success and the type

50
00:03:57,660 --> 00:04:02,280
of this button is obviously going to be submitted as this is a submit button.

51
00:04:02,790 --> 00:04:06,460
And let's add some text in here would say something like search.

52
00:04:07,440 --> 00:04:11,470
So once we go ahead and do that, we should have our input fill up on our site.

53
00:04:12,090 --> 00:04:14,850
So let me just save that and hop back in here.

54
00:04:14,850 --> 00:04:20,339
And if I hit refresh, as you can see, we have a nice looking search bar in here, which is going to

55
00:04:20,339 --> 00:04:22,140
now allow us to search products.

56
00:04:22,500 --> 00:04:24,400
So for now, this is not functional.

57
00:04:24,720 --> 00:04:27,300
So let's go ahead and make this search bar functional.

58
00:04:27,570 --> 00:04:31,080
So, as you all know, when will we actually want to implement search?

59
00:04:31,110 --> 00:04:37,020
The very first thing which we do is that we try to get access to this particular input, feel right

60
00:04:37,020 --> 00:04:37,280
here.

61
00:04:37,530 --> 00:04:44,400
And in order to get access to that input field, we have this memory here, which is item name.

62
00:04:45,210 --> 00:04:47,880
And I guess I have misspelled that a little bit.

63
00:04:47,890 --> 00:04:49,710
So let me just fix that.

64
00:04:49,740 --> 00:04:53,410
OK, so now let's come back to our main problems.

65
00:04:53,420 --> 00:04:58,830
So first of all, in order to get access to the input field, we make sure that we have access to the

66
00:04:58,830 --> 00:04:59,210
name.

67
00:04:59,220 --> 00:05:04,470
So using this particular name, we can get what's inside the input field, which is this field right

68
00:05:04,470 --> 00:05:04,740
here.

69
00:05:05,520 --> 00:05:11,040
So now once we have this name, let's go ahead and actually get the data which is entered into this

70
00:05:11,040 --> 00:05:12,140
particular input field.

71
00:05:12,690 --> 00:05:13,830
So let's go to The View.

72
00:05:13,830 --> 00:05:18,840
Start by and in here, let's go ahead and get access to the data.

73
00:05:18,840 --> 00:05:25,050
So I'll type an item, underscore name, and that's actually going to be equal to request.

74
00:05:27,530 --> 00:05:36,020
Dot, that's going to be get dot yet and we will type an item underscored Neiman here, which is nothing

75
00:05:36,020 --> 00:05:38,510
but the name of the TextField right here.

76
00:05:39,170 --> 00:05:44,750
So now once we have access to that, we can now go ahead and make a search for that particular enter

77
00:05:44,780 --> 00:05:45,320
text.

78
00:05:45,650 --> 00:05:57,870
So I'll type in if item name is not equal to empty and if item name is not none.

79
00:05:58,790 --> 00:06:04,040
So in this case, we actually want to go ahead and make a search for this particular item name inside

80
00:06:04,040 --> 00:06:05,240
our product objects.

81
00:06:05,630 --> 00:06:06,710
So I'll open.

82
00:06:06,710 --> 00:06:09,320
That means product objects equals.

83
00:06:10,510 --> 00:06:12,580
Product objects to.

84
00:06:14,950 --> 00:06:21,190
And inside this filter will make sure if the product's title is equal to item, name, soil type and

85
00:06:21,190 --> 00:06:28,780
title is going to be equal to item name, and in order to make this search a little bit more lenient,

86
00:06:28,780 --> 00:06:32,260
I'll go ahead and type and underscore I contains.

87
00:06:34,890 --> 00:06:38,190
So once we go ahead and do that, we are pretty much good to go.

88
00:06:39,000 --> 00:06:45,600
So now let's make sure that the product objects are the one which apostille as a contact and yes, they

89
00:06:45,600 --> 00:06:46,440
actually are.

90
00:06:46,980 --> 00:06:50,140
So now once we are done with this, we are pretty much good to go.

91
00:06:50,190 --> 00:06:53,070
So let me just go ahead and make sure everything is fine.

92
00:06:53,550 --> 00:06:57,210
So first of all, we have extracted the item name.

93
00:06:57,210 --> 00:07:01,450
Then we have actually checked of the item name is not empty and is not done.

94
00:07:01,950 --> 00:07:08,190
Then we have used product objects, which is nothing but the list of the products to actually filter

95
00:07:08,190 --> 00:07:11,530
out the products where the title is equal, the item name.

96
00:07:11,580 --> 00:07:14,780
We have also added I contains over here as well.

97
00:07:15,150 --> 00:07:17,360
Let me check if there's any typo in here.

98
00:07:18,210 --> 00:07:21,630
And yeah, I guess I am missing a underscore in here.

99
00:07:21,630 --> 00:07:24,490
So this should be double in the school I contains.

100
00:07:24,510 --> 00:07:27,490
And now let's also go ahead and check the template as well.

101
00:07:27,510 --> 00:07:29,820
So let's go to the index dot each shamal.

102
00:07:30,240 --> 00:07:33,410
So here we have defined ERU inside that room.

103
00:07:33,430 --> 00:07:39,950
We have this particular column which spans all the columns and in there we have created this card here.

104
00:07:40,050 --> 00:07:44,880
So we have the input file and the submit button, which is well and fine.

105
00:07:45,480 --> 00:07:54,060
And I guess we actually do need to mention this particular thing as not a div, but actually a form,

106
00:07:54,060 --> 00:07:58,760
because we obviously have an input field and we are using a button in here.

107
00:07:59,130 --> 00:08:02,220
So let's go ahead and make this as a form.

108
00:08:04,210 --> 00:08:09,790
And right where the stiff ends, which is this tag right here, let's also change this thing to form.

109
00:08:10,570 --> 00:08:14,680
So once again, let me just go through it and see if everything is correct.

110
00:08:15,340 --> 00:08:17,720
And, yeah, it actually looks OK.

111
00:08:18,040 --> 00:08:22,600
And now let me just go ahead, see if they could go back in here, hit refresh.

112
00:08:23,020 --> 00:08:27,610
And let's now make a search for a product which is called Last Laptop.

113
00:08:27,690 --> 00:08:29,680
So and I go ahead and search for a laptop.

114
00:08:30,130 --> 00:08:33,760
As you can see, I have the laptop as well as the laptop bag.

115
00:08:33,880 --> 00:08:37,289
And when I search for bike, we do have a bike here.

116
00:08:38,080 --> 00:08:45,600
And when we search for what we do have a watching him, which means now our search is working just fine.

117
00:08:45,610 --> 00:08:49,370
And if you're trying to attempt this good, I hope your search is working as well.

118
00:08:50,140 --> 00:08:56,320
So that's it for this lecture and in the upcoming lecture will go ahead and learn how to add pagination

119
00:08:56,320 --> 00:08:57,680
to this particular site.

120
00:08:58,210 --> 00:09:00,190
So thank you very much for watching.

121
00:09:00,190 --> 00:09:02,170
And I'll see you guys next time.

122
00:09:02,530 --> 00:09:03,130
Thank you.

