1
00:00:00,120 --> 00:00:06,880
So in this lecture, let's go ahead and add the content to this particular popover right here.

2
00:00:07,440 --> 00:00:14,250
So the very first thing which you need to do here is that we need to go ahead and get the items from

3
00:00:14,250 --> 00:00:16,450
the cart and display them over here.

4
00:00:17,100 --> 00:00:22,320
So, for example, right now, if you have a look over here, we don't have anything except for this

5
00:00:22,320 --> 00:00:23,340
random text.

6
00:00:23,790 --> 00:00:30,210
So in order to replace this text and add items over here, first of all, we need to figure out a way

7
00:00:30,210 --> 00:00:33,690
to actually get the items added to our cart.

8
00:00:34,410 --> 00:00:41,100
So we obviously know that the items which are added to the card are present in the local storage.

9
00:00:41,490 --> 00:00:44,640
However, there is one small issue.

10
00:00:44,910 --> 00:00:52,890
That is if you have a look at the local storage, we only have the product ID inside the local storage

11
00:00:52,890 --> 00:00:55,460
and we don't have the name of these products.

12
00:00:55,890 --> 00:01:00,410
So, for example, if you go to the console.

13
00:01:00,720 --> 00:01:03,060
So let me just open up the console right here.

14
00:01:04,410 --> 00:01:12,750
And if you just go ahead and this particular item and if you do local storage, as you can see, you

15
00:01:12,750 --> 00:01:15,960
only get the item idea here added to the card.

16
00:01:17,610 --> 00:01:23,340
So you can actually go ahead extract this particular item, Heidi, and you can display this particular

17
00:01:23,340 --> 00:01:29,590
thing in a popular but how exactly are you going to get access to the name over here?

18
00:01:30,540 --> 00:01:34,410
So that's the main problem we need to work on right now.

19
00:01:35,190 --> 00:01:42,420
So the very first thing which we do in order to get rid of that issue is that if you just go ahead and

20
00:01:42,420 --> 00:01:51,360
inspect this particular element, as you can see, if you have that name up over here and having that

21
00:01:51,360 --> 00:01:53,310
name over here is not an issue.

22
00:01:53,310 --> 00:01:57,410
But the problem is how you are going to get access to this particular name.

23
00:01:57,840 --> 00:02:04,800
So whenever you need to get access to a particular element, you can get access to that element by using

24
00:02:04,800 --> 00:02:07,290
the get element by ID method.

25
00:02:07,620 --> 00:02:12,890
So for that, we need to assign an ID to these particular names right here.

26
00:02:13,230 --> 00:02:15,790
So we need to assign an ID to this name.

27
00:02:15,810 --> 00:02:19,040
We need to assign NIIT to this name, to this name as well.

28
00:02:19,620 --> 00:02:24,150
So we need to assign unique I.D. to these elements right here.

29
00:02:25,020 --> 00:02:30,270
And in order to do that, you can simply go ahead, hop back to your code.

30
00:02:30,960 --> 00:02:33,240
So let me just open up Visual Studio code.

31
00:02:33,870 --> 00:02:41,240
And in here, if you go to the next e-mail and if you search where the product name is actually displayed,

32
00:02:41,610 --> 00:02:48,230
so right now the product name is displayed up over here, which is nothing but the product title.

33
00:02:48,630 --> 00:02:57,720
And in order to assign this particular thing and ID, I can simply type an ID equals and then I can

34
00:02:57,720 --> 00:03:07,350
see product dot ID and let me just enclose this thing in quotations and now this thing is going to associate

35
00:03:07,350 --> 00:03:10,540
an ID with the card title.

36
00:03:10,830 --> 00:03:13,490
However, there is one small issue with that.

37
00:03:13,830 --> 00:03:20,550
So if you save the school and now if you hit refresh, as you can see, the watch actually has an I.D.

38
00:03:20,550 --> 00:03:20,970
one.

39
00:03:21,360 --> 00:03:27,480
But as you can see, the button over here, which is associated with the watch, also has an ID equal

40
00:03:27,480 --> 00:03:27,920
to one.

41
00:03:28,440 --> 00:03:34,050
So if you keep scrolling down a little bit, let's see, what do we have for other products over here?

42
00:03:34,500 --> 00:03:42,510
So if you go to this particular laptop, if you inspect, you will get the ID to for this particular

43
00:03:42,510 --> 00:03:45,410
laptop title, which is the product title.

44
00:03:45,420 --> 00:03:52,740
But if you scroll down a little bit, as you can see, you also have ideas, too, for the aquacade

45
00:03:52,740 --> 00:03:54,080
button of the same product.

46
00:03:54,690 --> 00:03:56,940
So that is actually a problem.

47
00:03:56,940 --> 00:04:00,520
And there is a very simple fix for this specific problem.

48
00:04:00,990 --> 00:04:09,060
So the simple fix for this problem is that you simply go ahead and you append some random text in here

49
00:04:09,060 --> 00:04:14,010
so as to distinguish the product titles ID from the product button.

50
00:04:14,910 --> 00:04:21,899
So I can simply say over here, as this is a name, I can see something like a name and then we will

51
00:04:21,899 --> 00:04:24,210
have the product ID after that.

52
00:04:24,600 --> 00:04:32,100
So now if I save this code and if I go back, hit refresh, as you can see now, instead of having the

53
00:04:32,100 --> 00:04:38,270
IDs do, this particular laptop's title now has an ID as an M2.

54
00:04:38,640 --> 00:04:45,420
And now if you check for the idea of this particular watch right here, if you right.

55
00:04:45,420 --> 00:04:50,550
Click and if you click on inspect, as you can see, it has an ideas and then one.

56
00:04:50,560 --> 00:04:57,240
So that means now the issue of having different ideas for the title and the buttons is actually cleared

57
00:04:57,450 --> 00:05:00,140
and now we have different ideas for both of them.

58
00:05:00,180 --> 00:05:05,130
So now once we're done with that, let's go ahead and write for the logic.

59
00:05:05,340 --> 00:05:11,950
So now once we have access to the title now, we can actually go ahead and display that title up over

60
00:05:11,970 --> 00:05:13,650
here inside the Poppo.

61
00:05:13,680 --> 00:05:16,070
So now let's go ahead and start doing that.

62
00:05:16,740 --> 00:05:20,180
So let's go to the JavaScript over here and in here.

63
00:05:20,310 --> 00:05:24,870
What we will do is that we will create a brand new function in that particular function.

64
00:05:24,870 --> 00:05:31,650
What we wish to do is that we wish to get all the items from the card and display those items in a popplewell.

65
00:05:32,100 --> 00:05:34,250
So let's create a JavaScript function.

66
00:05:34,500 --> 00:05:39,720
So whenever you need to create a function in JavaScript, you simply type in function.

67
00:05:41,370 --> 00:05:43,830
So this is actually a function definition.

68
00:05:44,040 --> 00:05:51,510
So it'll simply go ahead and name this function as, let's say, display code, and this is going to

69
00:05:51,510 --> 00:05:54,510
accept the card as a parameter.

70
00:05:54,570 --> 00:06:00,180
So the card here is nothing, but it's actually the card which we have as a variable right here.

71
00:06:01,020 --> 00:06:07,800
So once we have passed that card over here, we can now go ahead and make use of this card to loop through

72
00:06:07,800 --> 00:06:11,080
the card and get the items from the card.

73
00:06:11,190 --> 00:06:16,830
And when we get those items from the card, we are actually going to append those items in a.

74
00:06:17,160 --> 00:06:23,640
A suffix string, and then we are going to use that particular human string and then we will assign

75
00:06:23,640 --> 00:06:26,140
that Jimal string to our Popolo.

76
00:06:26,640 --> 00:06:32,820
So as you can see right here, we have created this particular string which says that this is your card.

77
00:06:33,210 --> 00:06:40,630
And we have basically assigned that string to the Popplewell element by using that element by IDE method.

78
00:06:41,010 --> 00:06:44,680
So we are going to do something of similar sort over here.

79
00:06:45,000 --> 00:06:47,660
So let's first go ahead and create a string.

80
00:06:47,850 --> 00:06:55,230
So in here, as we need to store multiple things into a string, I'll simply go ahead and create a string.

81
00:06:55,410 --> 00:06:57,230
So I'll say something like that.

82
00:06:57,240 --> 00:07:00,150
And let's call this string as string.

83
00:07:00,410 --> 00:07:03,420
So diamond called string.

84
00:07:05,990 --> 00:07:12,380
And for now, this thing is going to be empty because we don't have anything inside the spring, and

85
00:07:12,380 --> 00:07:18,970
now whenever we want to open something to this particular spring, we simply go ahead and open hot spring

86
00:07:20,420 --> 00:07:21,470
plus equals.

87
00:07:21,890 --> 00:07:28,920
And then you just need your posehn some HTML code in here so that you can append some items to it.

88
00:07:29,120 --> 00:07:31,670
So let's say, for example, I need to open this.

89
00:07:31,760 --> 00:07:33,210
This is your card right here.

90
00:07:33,590 --> 00:07:36,750
I can simply go ahead, copy that particular thing from here.

91
00:07:37,280 --> 00:07:40,730
So let me just copy that and paste it up over here.

92
00:07:41,600 --> 00:07:49,720
So this card string plus equals simply means that we are seeing card string equals card string plus

93
00:07:49,740 --> 00:07:51,520
this particular e-mail code.

94
00:07:51,980 --> 00:07:54,040
And we do have a typo here.

95
00:07:54,050 --> 00:07:55,310
So let me fix that.

96
00:07:55,340 --> 00:07:56,990
So this should actually be string.

97
00:07:58,950 --> 00:08:04,120
And now let's actually police this e-mail string inside quotations.

98
00:08:05,790 --> 00:08:12,600
So now once we have appended that to the screen, now let's go ahead and actually get items from this

99
00:08:12,600 --> 00:08:13,960
particular card right here.

100
00:08:13,980 --> 00:08:21,450
So to get those items, you simply need to go ahead and you can actually loop through all of these items.

101
00:08:22,200 --> 00:08:28,050
So I will say something like four by X.

102
00:08:28,350 --> 00:08:34,740
So X is actually a variable we have created here to loop through the car and I'll say something like

103
00:08:34,740 --> 00:08:35,940
in car.

104
00:08:36,480 --> 00:08:40,270
So that actually allows us to loop through all the items inside the car.

105
00:08:41,010 --> 00:08:48,750
And now once we have that, we can now go ahead and we need to get two things from the items inside

106
00:08:48,750 --> 00:08:49,300
our car.

107
00:08:49,740 --> 00:08:55,680
So the first thing we need to get from here is that we need to get the name.

108
00:08:55,680 --> 00:08:56,940
So we all know here.

109
00:08:56,940 --> 00:09:03,630
How exactly do we get the name from the card by using the document or get element by ID method.

110
00:09:04,260 --> 00:09:12,090
So I'll simply say something like document dot, get eliminated by ID.

111
00:09:12,210 --> 00:09:16,510
And in here you all know how exactly can we get those elements over here.

112
00:09:16,530 --> 00:09:22,500
So for example, if you need to get the element, watch, you simply go ahead and type in the ideas

113
00:09:22,500 --> 00:09:26,520
and then one so I can simply go ahead and type in and then one over here.

114
00:09:27,150 --> 00:09:34,590
So instead of having a name over here, I'll simply enclose a name in parentheses because an M is actually

115
00:09:34,590 --> 00:09:37,550
going to be common for all of the items in our car.

116
00:09:38,130 --> 00:09:43,420
So I'll type in plus and then I'll add X over here.

117
00:09:43,950 --> 00:09:45,990
So this X over here is nothing.

118
00:09:45,990 --> 00:09:52,390
But this is actually the value of variable right here, which is the item inside the car.

119
00:09:53,310 --> 00:09:57,140
So this actually is going to get the item from my cart.

120
00:09:57,900 --> 00:10:02,920
And in order to get it in HTML, I'll type in and I assume over here.

121
00:10:03,090 --> 00:10:11,160
OK, so now once we have this particular item over here, we need to append that item to the card screen

122
00:10:11,160 --> 00:10:11,690
right here.

123
00:10:11,910 --> 00:10:13,110
So I'll simply see.

124
00:10:15,130 --> 00:10:27,460
Can't string plus equals this particular text, and now once we have that along with this particular

125
00:10:27,460 --> 00:10:31,220
item, we also want it's quantity as well.

126
00:10:31,360 --> 00:10:34,090
So getting the quantity from the item is quite simple.

127
00:10:34,450 --> 00:10:42,790
You can simply type in cart of X, and that's actually going to give you the quantity of that specific

128
00:10:42,790 --> 00:10:43,180
item.

129
00:10:43,820 --> 00:10:49,630
Now, as you need to append this to this particular string, you can simply type in a plus sign here

130
00:10:49,810 --> 00:10:58,330
and you can also go ahead and add some text like Mutebi over here to see that this specific thing is

131
00:10:58,330 --> 00:11:01,340
actually the quantity of this specific item.

132
00:11:01,360 --> 00:11:08,050
And now, once we go ahead and do that, we also need to make sure that we also associate index value

133
00:11:08,050 --> 00:11:08,960
with each item.

134
00:11:08,980 --> 00:11:14,620
So, for example, for each item we want to display something like this is item number one.

135
00:11:14,830 --> 00:11:20,140
For item number two, we want to display something like item number two, so on and so forth.

136
00:11:20,560 --> 00:11:26,770
So in order to do the same, we go ahead and create another variable on the top and let's name that

137
00:11:26,770 --> 00:11:28,870
variable as part index.

138
00:11:29,470 --> 00:11:33,100
So I'll simply go ahead and I'll create a variable over here.

139
00:11:33,280 --> 00:11:39,520
So I'll see our card index and assign this thing a value as one.

140
00:11:40,360 --> 00:11:46,930
And now once we have this card index inside this particular loop, I'll increment the value of card

141
00:11:46,930 --> 00:11:49,390
index every time this loop executes.

142
00:11:49,580 --> 00:11:55,080
So I'll see here called index plus equals one.

143
00:11:55,690 --> 00:12:01,840
And now let's also append that value up over here inside the card string so I can see something like

144
00:12:02,770 --> 00:12:05,800
card string plus equals.

145
00:12:06,100 --> 00:12:09,010
And that's actually going to be the card index.

146
00:12:11,230 --> 00:12:17,410
And now once we go ahead and do that, we are pretty much in good to go and we have successfully extracted

147
00:12:17,410 --> 00:12:24,040
the items from the cart and we have successfully got their names as well by using this method right

148
00:12:24,040 --> 00:12:24,260
here.

149
00:12:24,850 --> 00:12:26,580
So I hope you got the point.

150
00:12:26,590 --> 00:12:33,350
Like, why exactly you have used this name over here in order to get the items name.

151
00:12:33,610 --> 00:12:39,910
So now once we have done all the work over here and we have defined this function, let's call this

152
00:12:39,910 --> 00:12:41,100
function here itself.

153
00:12:41,560 --> 00:12:46,230
So I'll see this play card and I'll simply go ahead and pass and card value in here.

154
00:12:47,080 --> 00:12:53,050
And now, once we go ahead and do that, our next job is to go ahead, take this particular string,

155
00:12:53,050 --> 00:12:59,530
which is the card string, and assign this particular card string value to our Poppo.

156
00:12:59,740 --> 00:13:07,210
So we'll go right here and I'll type and document dot cat element by ID.

157
00:13:09,770 --> 00:13:17,930
And we actually want to get our popover and the idea of our popover is actually car and Alousi said

158
00:13:17,930 --> 00:13:23,240
attribute method and I'll see something like data.

159
00:13:24,740 --> 00:13:29,050
Content and we will append the card screen right here.

160
00:13:31,190 --> 00:13:37,990
So this is nothing, but this is exactly what we have done right over here in this particular case,

161
00:13:38,000 --> 00:13:44,630
and now you can actually go ahead and get rid of this thing by deleting it or commenting it out, because

162
00:13:44,630 --> 00:13:49,420
we no longer need that and you actually need this thing.

163
00:13:49,940 --> 00:13:55,310
And one more thing which you need to do here is that whenever you go ahead and add item to the cut,

164
00:13:55,610 --> 00:14:02,240
you actually need to use the Popplewell method so you can simply go ahead and copy this Popol method

165
00:14:02,480 --> 00:14:06,110
from here and piece that right away here at the bottom.

166
00:14:07,430 --> 00:14:09,650
So now let's see if this thing works.

167
00:14:09,650 --> 00:14:12,320
And I'm hopeful that it will work fine.

168
00:14:12,650 --> 00:14:16,730
But if it doesn't, then we'll go ahead and troubleshoot the issues.

169
00:14:16,980 --> 00:14:18,170
So let's save the code.

170
00:14:19,460 --> 00:14:20,960
Let's go back over here.

171
00:14:21,410 --> 00:14:22,940
Let me hit refresh.

172
00:14:22,970 --> 00:14:27,290
And when I go ahead, click on Add to Cart to Cart.

173
00:14:27,980 --> 00:14:33,340
And when I click on this cart, it's still not showing me anything over here.

174
00:14:33,590 --> 00:14:36,330
So let's go ahead and check in the console.

175
00:14:36,350 --> 00:14:39,520
What exactly has gone wrong?

176
00:14:39,560 --> 00:14:47,180
OK, so I actually figure out my issue over here, so I guess you don't have to actually comment this

177
00:14:47,180 --> 00:14:47,660
thing out.

178
00:14:47,660 --> 00:14:49,180
I did that by mistake.

179
00:14:49,700 --> 00:14:51,980
This is not the thing which you should comment about.

180
00:14:51,980 --> 00:14:58,640
But instead, this particular function is something which you want to get rid of, because this is exactly

181
00:14:58,640 --> 00:15:00,580
what we have changed right over here.

182
00:15:00,620 --> 00:15:07,100
So we have actually set up the get element by ID card method and we have set that attribute up over

183
00:15:07,100 --> 00:15:07,480
here.

184
00:15:07,910 --> 00:15:13,090
That is, we have set up the card Springle here and we have also use the POPPEL function in here.

185
00:15:13,550 --> 00:15:16,930
So you actually need to get rid of this particular function.

186
00:15:17,240 --> 00:15:20,580
So once you go ahead, get rid of that, everything should work fine.

187
00:15:20,960 --> 00:15:23,360
So now if you hold back, hit refresh.

188
00:15:23,360 --> 00:15:30,860
And if we add some items inside our cart and if we click on this, as you can see, we have those items

189
00:15:30,860 --> 00:15:31,990
added up over here.

190
00:15:32,000 --> 00:15:35,420
So we have one item, number one, X, Y, Z.

191
00:15:35,420 --> 00:15:37,100
Watch the quantity is three.

192
00:15:37,490 --> 00:15:41,090
Then we have item number two, which is EBC laptop.

193
00:15:41,090 --> 00:15:42,440
The quantity is three.

194
00:15:42,840 --> 00:15:46,910
Then we have the third item and then we have the fourth item right over here.

195
00:15:47,870 --> 00:15:49,910
So for now, this thing works fine.

196
00:15:49,910 --> 00:15:54,920
But if you notice, the items are actually all placed in a single line.

197
00:15:55,790 --> 00:15:58,660
So let's go ahead and simply find a fix for that.

198
00:15:59,150 --> 00:16:02,960
So for fixing that particular thing, it's actually quite simple.

199
00:16:02,960 --> 00:16:05,930
You just need to append a black tag over here.

200
00:16:06,170 --> 00:16:12,170
So I'll simply go ahead Tiepin plus and I'll simply dipen be over here.

201
00:16:12,200 --> 00:16:17,750
So now once we go ahead do that, let's also add a semicolon in here.

202
00:16:18,500 --> 00:16:26,000
And now if we hit refresh, as you can see now, we have all the items displayed up over here in improper

203
00:16:26,000 --> 00:16:26,540
fashion.

204
00:16:26,900 --> 00:16:33,420
So that means we have successfully implemented how to display the items inside our car.

205
00:16:33,890 --> 00:16:40,100
So in the next lecture, what we will do is that we will go ahead and we will add a checkout button

206
00:16:40,100 --> 00:16:41,300
in here at the bottom.

207
00:16:41,600 --> 00:16:47,240
And whenever the user clicks this particular checkout button, you should be redirected to the checkout

208
00:16:47,240 --> 00:16:47,660
page.

209
00:16:47,660 --> 00:16:52,640
And on that checkout page, these following items should be displayed.

210
00:16:53,150 --> 00:16:56,780
So we are going to learn how to do that in the upcoming lecture.

211
00:16:57,020 --> 00:16:59,120
So thank you very much for watching.

212
00:16:59,120 --> 00:17:01,130
And I'll see you guys next time.

213
00:17:01,400 --> 00:17:01,940
Thank you.

