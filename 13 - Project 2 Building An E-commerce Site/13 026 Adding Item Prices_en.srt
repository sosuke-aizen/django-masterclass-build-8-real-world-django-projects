1
00:00:00,300 --> 00:00:07,230
So now in this lecture, let's actually go ahead and see how you can display the price of the items

2
00:00:07,230 --> 00:00:15,690
over here and finally calculate the total of the items or the total cost of your order by adding the

3
00:00:15,690 --> 00:00:19,890
costs of these multiple products, including the quantity as well.

4
00:00:20,610 --> 00:00:26,160
So now the very first thing which you need to do here is that you need to go ahead and you first need

5
00:00:26,160 --> 00:00:30,840
a way to actually store these values into the local storage.

6
00:00:30,990 --> 00:00:37,380
So right now, what we are doing is that we are only storing the item ID and the quantity inside the

7
00:00:37,380 --> 00:00:38,200
local storage.

8
00:00:38,490 --> 00:00:43,650
So now we also need a way to actually store these values in the local storage as well.

9
00:00:43,830 --> 00:00:49,590
So how exactly can you go ahead and extract these values and store them in the local storage?

10
00:00:50,250 --> 00:00:57,750
So the very first thing which you need to do is that in order to get the values of the respective items,

11
00:00:58,110 --> 00:01:02,130
you need to associate a product ID with these prices right here.

12
00:01:02,490 --> 00:01:08,760
So if you switch back to the code and if you go to the index taught each HTML page, as you can see,

13
00:01:09,180 --> 00:01:14,250
we have associated a product ID with each and every button over here.

14
00:01:14,400 --> 00:01:20,250
So, for example, with the add to cart button right over here, we have associated a product ID and

15
00:01:20,250 --> 00:01:24,430
now we actually want to do the same thing with the product price as well.

16
00:01:24,450 --> 00:01:27,860
So as you can see, we have this product price right here.

17
00:01:27,990 --> 00:01:34,720
And what we wish to do is that we also wish to associate a unique ID with this product price as well.

18
00:01:35,190 --> 00:01:41,910
So in here, as you can see for the product title, we have actually associated a unique product ID.

19
00:01:41,910 --> 00:01:46,070
That is, we have typed in a name and then we have this product ID right here.

20
00:01:46,440 --> 00:01:49,870
So let's do the same thing with the product price as well.

21
00:01:49,890 --> 00:01:57,690
So I'll type an ID equals and then I'll say something like, let's see price and then let's get the

22
00:01:57,720 --> 00:02:01,580
unique ID by typing in product ID.

23
00:02:02,310 --> 00:02:09,150
And now once we have associated a unique ID with the product, now we can actually go ahead and save

24
00:02:09,180 --> 00:02:13,350
this particular product price in the local storage.

25
00:02:14,190 --> 00:02:20,340
So for that, you actually need to go ahead and go to the logic where we actually store the item ID

26
00:02:20,340 --> 00:02:22,210
and the quantity in the local storage.

27
00:02:22,230 --> 00:02:27,120
So as you can see right here, we have extracted the quantity of the item.

28
00:02:27,120 --> 00:02:32,880
We have also extracted the product ID and we have basically stored them right over here.

29
00:02:33,060 --> 00:02:37,510
So as you can see, for the quantity, we have seen the quantity at position zero.

30
00:02:37,950 --> 00:02:43,950
So now if you actually want to see the price of the product as well, you can simply go ahead and save

31
00:02:43,950 --> 00:02:51,390
it at a particular position, which is index position, do so in order to get the price and added over

32
00:02:51,390 --> 00:02:51,830
here.

33
00:02:51,870 --> 00:02:57,470
What I can simply do is that I can type in court item idea of two and get the actual price.

34
00:02:57,480 --> 00:02:58,890
So let's do the same thing.

35
00:02:58,890 --> 00:03:08,460
So I'll see kind of item ID, so card of item ID and we are going to save the price at the position

36
00:03:08,460 --> 00:03:08,730
too.

37
00:03:08,730 --> 00:03:12,990
So I'll type in two over here and that's actually going to be equal to.

38
00:03:13,200 --> 00:03:19,880
So here in order to get the price we will just make use of the ID which we have assigned right now.

39
00:03:19,920 --> 00:03:25,200
So if you actually have a look at the price, we will actually use this idea, which is price and the

40
00:03:25,200 --> 00:03:26,100
product ID.

41
00:03:26,520 --> 00:03:28,420
So let's make use of that over here.

42
00:03:29,250 --> 00:03:40,260
So let's go back here and I'll say something like document don't get element by ID and then we'll actually

43
00:03:40,260 --> 00:03:42,510
get the ID, which is nothing but price.

44
00:03:43,290 --> 00:03:51,410
Plus it's nothing but the item ID, so I'll type in item ID and then I'll say something like Daudt in

45
00:03:51,600 --> 00:03:54,810
HTML in order to get the HDMI inside of it.

46
00:03:55,290 --> 00:04:00,690
Now there's one issue with this approach that we have used the same approach for the name as well,

47
00:04:01,020 --> 00:04:05,480
and it basically works in case of name because we are using a string here.

48
00:04:05,580 --> 00:04:09,000
So name of the product is nothing, but it's actually a string.

49
00:04:09,120 --> 00:04:15,930
And now the issue with price is that price is not a string, but instead it's actually a integer value

50
00:04:15,930 --> 00:04:17,180
or float value.

51
00:04:17,640 --> 00:04:24,480
So whenever you get go ahead and get this value of price from here, you always need to make sure that

52
00:04:24,480 --> 00:04:26,570
you convert this thing into a float.

53
00:04:27,030 --> 00:04:30,750
So I'll simply go ahead, cut this thing and I'll type in.

54
00:04:31,600 --> 00:04:38,560
Bass flute, which is actually a method of function which basically allows us to convert a string into

55
00:04:38,560 --> 00:04:39,010
a flute.

56
00:04:39,370 --> 00:04:44,920
So in the Alps, this code, which we have just typed in, and what this will do is that it will get

57
00:04:44,920 --> 00:04:50,260
the price of that particular product and it will convert it into a floating point value.

58
00:04:51,040 --> 00:04:52,980
Now, this actually looks fine.

59
00:04:53,410 --> 00:04:57,550
We have got the price value and we have simply stored that price value in here.

60
00:04:58,360 --> 00:05:04,300
However, there is one issue with this particular thing that is this logic, that is this particular

61
00:05:04,300 --> 00:05:08,840
if block is actually written, if the product is already present in the cart.

62
00:05:09,280 --> 00:05:15,700
So, for example, let's say we have a watch in our cart and we had the same watch again.

63
00:05:15,880 --> 00:05:22,150
So in that case, what should happen is that it should not only include the price of the watch, but

64
00:05:22,150 --> 00:05:25,460
it should also include the price of the previous watch as well.

65
00:05:25,510 --> 00:05:30,750
So that means when I first add an item to the cart, this logic should actually work.

66
00:05:30,760 --> 00:05:36,610
But when I add the watch for the second time, what happens in this particular case is that the price

67
00:05:36,610 --> 00:05:37,480
does not change.

68
00:05:38,380 --> 00:05:44,350
So what we want to do here is that we not only want to get the current price, but we also want to add

69
00:05:44,350 --> 00:05:47,350
the previous price if the item was added previously.

70
00:05:47,860 --> 00:05:54,130
So in here, instead of just having a cart of item idea of two equals to this thing, I will say this

71
00:05:54,130 --> 00:05:58,180
equals this plus the new value.

72
00:05:58,240 --> 00:06:04,410
So what this means is that let's see if I add a watch, the value of the price is going to be a hundred.

73
00:06:04,510 --> 00:06:10,600
And now when I add the watch for the second time, what happens is that it takes the first value, which

74
00:06:10,600 --> 00:06:16,450
is 100, and then it takes the new value, which is, again, hundred, and it basically adds it to

75
00:06:16,450 --> 00:06:19,510
the previous value so that the new value becomes 200.

76
00:06:20,110 --> 00:06:24,550
So now what happens is that add the same watch for the third time.

77
00:06:24,970 --> 00:06:30,520
The value of this thing is already two hundred and it's again going to take a hundred from here and

78
00:06:30,520 --> 00:06:34,340
now the total value of item actually becomes three hundred.

79
00:06:35,230 --> 00:06:41,260
So this is the way in which we will be storing the price in this particular cart of item idea of two.

80
00:06:41,290 --> 00:06:47,020
So now this works for having multiple elements, but what if you are actually adding the item inside

81
00:06:47,020 --> 00:06:48,940
the card for the very first time?

82
00:06:49,090 --> 00:06:54,850
So in that case, what you simply wish to do is that you simply wish to get the value by using this

83
00:06:54,850 --> 00:06:55,580
code right here.

84
00:06:55,690 --> 00:06:57,220
So I'll simply copy that.

85
00:06:57,760 --> 00:07:04,930
And now in here, what we will do is that, first of all, I will create a variable called price, and

86
00:07:04,930 --> 00:07:12,280
I'll simply assign this a value, which is the actual value of the product or the actual price of the

87
00:07:12,280 --> 00:07:12,670
product.

88
00:07:12,700 --> 00:07:18,340
Now, once you get this particular price, what you do is that you simply go ahead and add this price

89
00:07:18,340 --> 00:07:22,510
up over here, along with the quantity and name so that the index position zero.

90
00:07:22,530 --> 00:07:25,450
We have saved the quantity at indisposition one.

91
00:07:25,450 --> 00:07:28,180
We have the name and that index position.

92
00:07:28,180 --> 00:07:32,680
Do we actually want to have the price, which is nothing but this thing right here.

93
00:07:32,890 --> 00:07:36,700
So once you go ahead and do that, you're pretty much good to go.

94
00:07:36,760 --> 00:07:43,150
So now let's simply save the code and now let's test this code in the console and see if this thing

95
00:07:43,150 --> 00:07:44,050
actually works.

96
00:07:44,410 --> 00:07:50,590
So first of all, I'll just go ahead, open this thing up, open up the console as well, and let's

97
00:07:50,590 --> 00:07:51,690
just clear the card.

98
00:07:52,300 --> 00:07:58,350
So let me just go to console and I'll say something like local storage, not clear.

99
00:07:59,470 --> 00:08:02,320
And now once it's cleared, I'll add this Warshel.

100
00:08:04,080 --> 00:08:12,930
And I guess I need to clear the local storage and then actually hit refresh.

101
00:08:14,390 --> 00:08:17,330
So local stories not clear.

102
00:08:18,620 --> 00:08:23,090
And then hit refresh and then actually hit the article button.

103
00:08:23,330 --> 00:08:30,320
And as you can see, if I click on this, for some reason, it's saying that the value of what is undefined.

104
00:08:30,330 --> 00:08:33,450
So let's see why exactly do we get this particular error.

105
00:08:33,590 --> 00:08:38,140
So let's first analyze if the idea is associated with this thing.

106
00:08:38,150 --> 00:08:41,600
So if we go ahead and look over here, have a look over here.

107
00:08:42,140 --> 00:08:45,500
We have price one here for this product, for this price.

108
00:08:45,500 --> 00:08:50,630
If we go ahead and inspect, as you can see, we also have 500 over here.

109
00:08:50,870 --> 00:08:58,250
And I guess the reason why it's actually showing us an error is because we have this dollar sign associated

110
00:08:58,250 --> 00:08:59,930
with that particular price.

111
00:09:00,320 --> 00:09:05,300
So let's actually go ahead and try removing the dollar sign and see if this thing works out.

112
00:09:05,540 --> 00:09:12,320
So what I will do here is that I'll go ahead and get rid of this particular dollar sign from here and

113
00:09:12,320 --> 00:09:15,240
also remove any white spaces which we have.

114
00:09:15,410 --> 00:09:20,650
So now once we go ahead and fix that, let's see if this thing works out right now.

115
00:09:20,990 --> 00:09:23,450
So let's again go back to the console.

116
00:09:25,000 --> 00:09:27,580
And let's clear up the local storage.

117
00:09:29,040 --> 00:09:32,480
So local storage, not clear.

118
00:09:33,960 --> 00:09:38,220
Now, let's hit refresh and now if I click on Add to Cart.

119
00:09:40,090 --> 00:09:45,520
And if I click over here right now, as you can see now, we have the value stored over here, which

120
00:09:45,520 --> 00:09:46,040
is 100.

121
00:09:46,720 --> 00:09:51,510
So now let me click this button one more time and let's see if the value changes to two hundred.

122
00:09:51,790 --> 00:09:53,290
So that should actually happen.

123
00:09:53,440 --> 00:09:57,320
That is, it should take the current value and add a new value in here.

124
00:09:57,370 --> 00:10:03,790
So when I click over here and when I click this, as you can see, the value has now changed to two

125
00:10:03,790 --> 00:10:04,220
hundred.

126
00:10:04,810 --> 00:10:08,560
So when I add one more time, let's see what do we get.

127
00:10:09,040 --> 00:10:12,970
So as you can see now, the total value becomes three hundred for three watches.

128
00:10:13,540 --> 00:10:16,160
So now let's try the same thing for the bike as well.

129
00:10:16,180 --> 00:10:18,490
So I'll click on the bike twice.

130
00:10:19,600 --> 00:10:25,510
And now as you can see, the value of two bikes is shown to be 600.

131
00:10:25,690 --> 00:10:31,440
That means we are now successfully able to actually store the price of items.

132
00:10:31,450 --> 00:10:35,530
That is the total price of the items inside the local storage.

133
00:10:36,010 --> 00:10:42,700
Now, our next job is to go ahead and make use of these values and display these values on the checkout

134
00:10:42,700 --> 00:10:43,420
page as well.

135
00:10:43,930 --> 00:10:50,020
So right now, as we have those values in the local storage, we just need to go ahead and display those

136
00:10:50,020 --> 00:10:51,470
values up over here.

137
00:10:51,640 --> 00:10:54,260
So we are going to do that in the upcoming lecture.

138
00:10:54,640 --> 00:10:56,740
So thank you very much for watching.

139
00:10:56,740 --> 00:10:58,660
And I'll see you guys next time.

140
00:10:58,930 --> 00:10:59,500
Thank you.

