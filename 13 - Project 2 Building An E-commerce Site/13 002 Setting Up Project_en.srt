1
00:00:00,060 --> 00:00:05,550
Hello and welcome to this lecture, and in this lecture will start creating the e-commerce Web site.

2
00:00:06,120 --> 00:00:12,360
So for this lecture we are going to go ahead and set up the virtual environment and create a triangle

3
00:00:12,360 --> 00:00:16,720
project as well as create the app inside our Shango project.

4
00:00:17,130 --> 00:00:23,040
So you're already familiar with the process of actually creating a project, so this might sound a lot

5
00:00:23,040 --> 00:00:24,000
simpler to you.

6
00:00:24,240 --> 00:00:26,430
So what you can do is that you can go ahead.

7
00:00:26,430 --> 00:00:32,670
First of all, create a folder called As eCom, and in there you can install the virtual environment

8
00:00:32,670 --> 00:00:38,790
and you can name that virtual environment as Ekom N.V. and then you can activate that virtual environment,

9
00:00:39,060 --> 00:00:46,230
install Shango in that environment, and create a triangle project named as eCom site, and then create

10
00:00:46,230 --> 00:00:49,920
an app which is called a shop inside that particular project.

11
00:00:50,370 --> 00:00:52,950
So you can go ahead and try that on your own.

12
00:00:53,130 --> 00:00:58,410
But for some of the students who have difficulty creating these or have some confusion, you can follow

13
00:00:58,410 --> 00:00:58,830
along.

14
00:00:59,190 --> 00:01:06,600
So what I have done here is that I have created a folder called us Ekom, and I opened it up in Visual

15
00:01:06,600 --> 00:01:09,980
Studio Code and I have opened up the terminal here.

16
00:01:10,410 --> 00:01:13,950
So let's navigate the desktop slash eCom.

17
00:01:14,640 --> 00:01:20,250
And in here, first thing which we do is that we need to go ahead and create a virtual environment.

18
00:01:20,250 --> 00:01:22,800
So virtual environment is already installed.

19
00:01:22,800 --> 00:01:28,870
So I'll type in virtual envy and let's name this environment as Ekom N.V..

20
00:01:29,880 --> 00:01:36,930
So once you go ahead and hit enter, as you can see, it's actually going to set up the virtual environment

21
00:01:36,930 --> 00:01:37,450
for us.

22
00:01:38,460 --> 00:01:44,430
So now once the virtual environment is set up, if you see over here in this folder, we have economy

23
00:01:44,430 --> 00:01:44,860
envy.

24
00:01:44,910 --> 00:01:47,910
So let's KDDI into Ekom and we.

25
00:01:48,980 --> 00:01:55,120
And now let's open source Ben Slash activist.

26
00:01:55,730 --> 00:01:58,220
So now the virtual environment is activated.

27
00:01:58,640 --> 00:02:02,690
Let's switch back to our folder, which is Ekom.

28
00:02:02,780 --> 00:02:11,570
And in here, the very first thing which we do is that we install Shango, so Pip install Shango, and

29
00:02:11,570 --> 00:02:13,550
that's going to install Zango for us.

30
00:02:15,140 --> 00:02:19,610
So now once Zango is installed, we create a project by typing in.

31
00:02:21,630 --> 00:02:23,220
Shango admen.

32
00:02:25,230 --> 00:02:34,270
Start project and let's say we are going to name this thing as Ekom site hit Enter.

33
00:02:34,290 --> 00:02:41,100
And now if we see over here, we have the eCom side project and then you seed into the e-commerce site

34
00:02:41,460 --> 00:02:43,170
and here you create an app.

35
00:02:43,170 --> 00:02:54,720
So you type in Django admin start up and we are going to call our app as shop hit enter and that app

36
00:02:54,720 --> 00:02:56,220
is going to be created for you.

37
00:02:56,820 --> 00:03:03,270
So now if you go back to Visual Studio Code, as you can see, we have the e-commerce site and we have

38
00:03:03,300 --> 00:03:04,300
shop over here.

39
00:03:04,920 --> 00:03:10,080
So we have the e-commerce site, the multiple files related to e-commerce site, and we have shop.

40
00:03:10,080 --> 00:03:12,990
And it's filed as well in their respective directories.

41
00:03:13,560 --> 00:03:16,250
So that means we have successfully set up the project.

42
00:03:16,260 --> 00:03:18,970
We have successfully created the app as well.

43
00:03:19,770 --> 00:03:25,890
So now the final thing which we need to do is that we need to go into the settings profile, which is

44
00:03:25,890 --> 00:03:31,060
this file right here and in the install apps, we just have to name app and help.

45
00:03:31,140 --> 00:03:32,700
So I need to type and shop.

46
00:03:33,870 --> 00:03:42,240
Give a comma safety code, and we are good to go, so we are done creating the CHANGA project as well

47
00:03:42,240 --> 00:03:43,500
as setting up the app.

48
00:03:43,860 --> 00:03:49,740
So in the next lecture we will go ahead and we will create a model called US Products so that we will

49
00:03:49,740 --> 00:03:53,560
be able to store some products onto our e-commerce website.

50
00:03:54,090 --> 00:03:58,110
So thank you very much for watching and I'll see you guys next time.

51
00:03:58,530 --> 00:03:59,160
Thank you.

