1
00:00:00,710 --> 00:00:07,570
So in this lecture, let's go ahead and try to add the form over here at the bottom of our list items,

2
00:00:08,210 --> 00:00:14,600
so in order to get the form, you can actually go ahead and design your form from scratch or else what

3
00:00:14,600 --> 00:00:18,080
you can simply do is that you can search for bootstrap for forms.

4
00:00:18,350 --> 00:00:23,690
And if you click on the first link, which is from get bootstrap dot com, you can actually get access

5
00:00:23,690 --> 00:00:29,540
to all of the forms which are by default present and you just need to copy them and paste them in your

6
00:00:29,540 --> 00:00:29,880
code.

7
00:00:30,320 --> 00:00:33,610
So if you go here, as you can see, there are a bunch of forms.

8
00:00:33,620 --> 00:00:38,260
So here you have a form in order to enter your email address and password.

9
00:00:38,510 --> 00:00:43,670
And if you actually go down over here, as you can see, you have multiple forms which you can choose

10
00:00:43,670 --> 00:00:43,970
from.

11
00:00:44,510 --> 00:00:51,650
So right now we are actually going to choose a form which accepts the different kinds of details, like

12
00:00:51,650 --> 00:00:54,710
the name, address, uh, everything like that.

13
00:00:55,010 --> 00:00:59,780
So if you actually have a look at this particular form, as you can see, this is the form which we

14
00:00:59,780 --> 00:01:00,520
are looking for.

15
00:01:00,890 --> 00:01:03,580
It accepts the email, the password.

16
00:01:03,590 --> 00:01:06,290
So in place of password, we will have something else.

17
00:01:06,590 --> 00:01:12,800
Let's say we want to accept the name and then we will accept the email address, the city, so on and

18
00:01:12,800 --> 00:01:13,420
so forth.

19
00:01:13,910 --> 00:01:17,040
So let's go ahead and actually pick this form from here.

20
00:01:17,240 --> 00:01:20,640
So let's simply copy this code, which is present right over here.

21
00:01:20,690 --> 00:01:29,530
So simply copy that and let's move back to our code and in here, right where this particular rule ends.

22
00:01:29,900 --> 00:01:31,350
So this is a container.

23
00:01:31,460 --> 00:01:33,170
This is a column.

24
00:01:34,220 --> 00:01:37,190
And this particular column actually ends here.

25
00:01:37,460 --> 00:01:43,460
And for some weird reason, I have actually placed the row inside a column, which is not actually a

26
00:01:43,460 --> 00:01:44,180
thing to do.

27
00:01:44,180 --> 00:01:46,190
So I'll just go ahead, get this from here.

28
00:01:46,970 --> 00:01:50,840
Uh, piece this up over here and get rid of this additional live.

29
00:01:51,360 --> 00:01:55,710
So now if we have a look at this, as you can see, it looks much more better.

30
00:01:55,850 --> 00:01:57,980
So this is actually a proper way to do that.

31
00:01:58,340 --> 00:02:04,100
And I don't know, for some weird reason, I've actually wrapped up the row inside the column and that

32
00:02:04,100 --> 00:02:05,630
should not actually be the case.

33
00:02:05,660 --> 00:02:09,610
OK, so now once we have this rule ending over here, let's create another row.

34
00:02:09,740 --> 00:02:17,320
So I'll open the class is going to be low and then let's go ahead and create columns.

35
00:02:17,720 --> 00:02:25,100
So I'll type and the class is going to be called Dash and Dash 12 and in there will actually go ahead

36
00:02:25,100 --> 00:02:27,630
and paste this particular form right here.

37
00:02:27,650 --> 00:02:30,950
So let me just again copy that and piece it up over here.

38
00:02:31,760 --> 00:02:37,310
So now if we switch back and hit refresh, as you can see now we have that form up over here.

39
00:02:37,850 --> 00:02:41,320
Now, let's make a few modifications to this particular form.

40
00:02:41,630 --> 00:02:46,630
So let's say instead of having the e-mail first, let's see if you want to accept the name.

41
00:02:46,640 --> 00:02:48,900
So let's go to the e-mail field right here.

42
00:02:49,430 --> 00:02:51,040
Let's replace this with name.

43
00:02:51,290 --> 00:02:54,620
Then let's replace this input type with text.

44
00:02:55,760 --> 00:02:58,760
And let's also get rid of the placeholder as well.

45
00:02:58,770 --> 00:03:03,440
And let's say the placeholder is something like John and in place of password.

46
00:03:03,440 --> 00:03:06,160
Let's say we go ahead and want to accept the e-mail.

47
00:03:06,500 --> 00:03:08,390
So I'll type an email over here.

48
00:03:09,890 --> 00:03:13,130
And the type is going to be, let's say, text as well.

49
00:03:13,370 --> 00:03:19,880
And let's say the placeholder is John and.

50
00:03:21,540 --> 00:03:22,820
John Dotcom.

51
00:03:24,300 --> 00:03:30,090
And now let's move on to the next part, which is the address and let's keep address as it is, and

52
00:03:30,090 --> 00:03:31,680
we don't want to change that for now.

53
00:03:31,950 --> 00:03:33,980
And the next one is the address line two.

54
00:03:34,410 --> 00:03:36,930
So for now, we'll just go ahead and get rid of that.

55
00:03:37,020 --> 00:03:44,130
And let's see for the next one, we actually want to have the city, the state and the zip code as it

56
00:03:44,130 --> 00:03:44,430
is.

57
00:03:45,150 --> 00:03:51,090
And let's also go ahead and get rid of this particular checkbox as well, because for now, we don't

58
00:03:51,090 --> 00:03:51,630
need that.

59
00:03:52,080 --> 00:03:54,480
So let's delete this entirely.

60
00:03:54,870 --> 00:04:00,690
And now once we get rid of that, we should have the form which if you want, and in here, instead

61
00:04:00,690 --> 00:04:05,880
of signing, let's say we change that thing to place order.

62
00:04:06,270 --> 00:04:09,580
OK, so once we have that, we are pretty much good to go.

63
00:04:09,990 --> 00:04:16,140
Now, the only thing which we need to do is that we need to actually go ahead and assign the name and

64
00:04:16,140 --> 00:04:18,730
ID for each one of these individual fields.

65
00:04:18,810 --> 00:04:24,470
So what we essentially want to do is that we want to submit this entire form using Shango.

66
00:04:24,840 --> 00:04:31,050
So in order to submit this form, we first of all need to get access to the name and IDs of this particular

67
00:04:31,050 --> 00:04:31,910
form elements.

68
00:04:32,610 --> 00:04:38,880
So in order to get access to the form elements right over here, first of all, you need to assign them

69
00:04:38,880 --> 00:04:41,210
a particular ID and a particular name.

70
00:04:41,640 --> 00:04:47,370
So in order to assign particular ID and name, you simply go to the input field of the respective fields

71
00:04:47,790 --> 00:04:54,590
and you type an ID equals, let's say name for this, and the name is going to be equal to name.

72
00:04:55,140 --> 00:04:59,650
So you need to assign the ID and name for each one of the input elements right here.

73
00:05:00,240 --> 00:05:02,510
So let's do the same thing for email as well.

74
00:05:02,730 --> 00:05:08,340
So the name is going to be email and the ID is going to be email as well.

75
00:05:09,330 --> 00:05:18,470
And then for this one, we are going to have the �lys�es address and the name as address as well.

76
00:05:19,200 --> 00:05:28,260
And for the city, let's go ahead and type in the ID as city and the name as city is.

77
00:05:28,260 --> 00:05:32,760
But so you can do that manually and it will actually go ahead and take some time.

78
00:05:33,120 --> 00:05:35,460
And for the state, let's actually go ahead.

79
00:05:35,460 --> 00:05:43,950
And instead of having a select box over here, let's simply go ahead and make use of the regular tax

80
00:05:43,950 --> 00:05:44,940
box right over here.

81
00:05:45,120 --> 00:05:50,130
So the idea for this is going to be state and the name is going to be state as well.

82
00:05:50,910 --> 00:05:57,290
So now let's do the same thing that is assigned the name and ID for the zip as well.

83
00:05:58,050 --> 00:06:03,830
So the type is going to be taxed and the ID is actually going to be zip.

84
00:06:04,760 --> 00:06:10,970
Or let's say zip code, and the name is going to be zip code as well.

85
00:06:11,840 --> 00:06:18,050
OK, so now once we have changed the names and IDs of the respective fields, we are pretty much good

86
00:06:18,050 --> 00:06:18,470
to go.

87
00:06:19,040 --> 00:06:23,690
So now what we need to do is that we need to handle this particular submit request.

88
00:06:23,960 --> 00:06:29,300
So, for example, whenever you go ahead and click this particular form, this form should actually

89
00:06:29,300 --> 00:06:34,960
post the data, meaning that the phone method, which they should be using, should be the post method.

90
00:06:35,360 --> 00:06:40,090
So we will be using the post method to actually post this data into the database.

91
00:06:40,460 --> 00:06:45,440
So in order to handle that post request now we need to go into the stockpile file.

92
00:06:45,740 --> 00:06:50,930
And in here, as you can see, this particular thing is the check out to you, which we already have.

93
00:06:51,200 --> 00:06:58,400
So in this particular checkout view, we first of all, go ahead and see if the request method is post.

94
00:06:58,540 --> 00:07:06,890
So for that to happen, if a request, which is the request right here, not method, if the method

95
00:07:06,890 --> 00:07:14,000
is equal, equal to nothing but post, which means that the form is trying to post some data using this

96
00:07:14,000 --> 00:07:20,000
view, then in that case, what we wish to do is that first of all, we need to get all the items which

97
00:07:20,000 --> 00:07:26,180
are submitted to the post request and the items are nothing but the name, the email, the address,

98
00:07:26,180 --> 00:07:28,790
the city and the state, so on and so forth.

99
00:07:28,940 --> 00:07:34,970
So now in order to get all those items, what we do is that we go ahead and type and name equals.

100
00:07:35,840 --> 00:07:44,480
And in order to get the name from the request, we dipen request dot, the method name, which is post.

101
00:07:44,780 --> 00:07:49,070
And from that we will get the element which is nothing but name.

102
00:07:49,370 --> 00:07:54,400
So I'll type in name over here and here, the default value for name.

103
00:07:54,660 --> 00:07:56,110
We are going to leave it empty.

104
00:07:56,540 --> 00:08:02,090
So we are going to do this for all the fields which we have in our form in order to actually submit

105
00:08:02,090 --> 00:08:02,620
this form.

106
00:08:03,230 --> 00:08:10,730
So I'll type an email over here now, so let's type an email so we will get the email in the same fashion.

107
00:08:10,730 --> 00:08:12,920
So I'll add email then.

108
00:08:12,920 --> 00:08:15,680
Let's also add the address.

109
00:08:20,210 --> 00:08:27,590
And now let's get the city so type and city equals city.

110
00:08:30,140 --> 00:08:35,360
And now let's also get the state as well, so state is going to be equal to.

111
00:08:38,750 --> 00:08:48,890
Request or post, get the state, then let's do the final thing, which is zip code, so zip code is

112
00:08:48,890 --> 00:08:52,820
going to be equal to get.

113
00:08:55,980 --> 00:08:57,600
Zip code, OK?

114
00:08:57,620 --> 00:09:02,880
So once we have extracted all these fields, which is name, email, address, city, state and zip

115
00:09:02,880 --> 00:09:08,400
code, now we actually need some model in our database so that we can stow these things.

116
00:09:08,730 --> 00:09:12,240
So right now, we don't have any model to store all this data.

117
00:09:12,480 --> 00:09:18,840
And hence, in order to store this particular order data, let's go ahead and let's actually make a

118
00:09:18,840 --> 00:09:19,900
particular model.

119
00:09:20,280 --> 00:09:25,230
So let's go back to the model Stoppie file, which is this file right here.

120
00:09:25,230 --> 00:09:31,620
And let's create a new model called Ordos in order to store the order related data.

121
00:09:31,980 --> 00:09:35,170
So this is going to inherit from Model Start model.

122
00:09:35,880 --> 00:09:39,930
And now let's go ahead and use the exact same fields over here.

123
00:09:40,410 --> 00:09:47,160
So in this particular model field, we are not only going to include these fields over here, but we

124
00:09:47,160 --> 00:09:53,190
are also going to actually go ahead and include the items field as well, which is going to store the

125
00:09:53,190 --> 00:09:55,810
items which are ordered by that particular person.

126
00:09:56,280 --> 00:10:00,480
So here the forcefield which will be creating is going to be items.

127
00:10:00,900 --> 00:10:04,750
So I'll type in items equals model start.

128
00:10:05,170 --> 00:10:11,400
Let's see, this is going to be a character field and for the max length of this particular thing will

129
00:10:11,400 --> 00:10:14,050
set the masland equal to, let's say, a thousand.

130
00:10:14,520 --> 00:10:20,680
And now let's go ahead and create the other fields as well, such as name, email, address, city,

131
00:10:20,700 --> 00:10:21,800
so on and so forth.

132
00:10:22,230 --> 00:10:25,530
So you can post this video and create these fields on your own.

133
00:10:26,820 --> 00:10:32,700
OK, so now as you can see, I've actually created this model here and now will actually go ahead and

134
00:10:32,700 --> 00:10:34,890
make migration's for this particular model.

135
00:10:35,250 --> 00:10:47,100
So I'll type in Python three managed by MK Migration's and then I'll Dipen Python three managed by my

136
00:10:47,100 --> 00:10:47,580
grid.

137
00:10:48,390 --> 00:10:53,870
And once we go ahead and do that, the order table will be actually created and our back in.

138
00:10:54,240 --> 00:11:00,690
And now once we have that particular order table, we can now actually go ahead and get this data which

139
00:11:00,690 --> 00:11:06,100
we have got from the post request and actually save that data and order table.

140
00:11:06,420 --> 00:11:13,700
So in order to do that, I'll type in all the equals and then I'll create the object of the class order

141
00:11:14,040 --> 00:11:18,000
and in here will simply pass in all these fields right over here.

142
00:11:18,360 --> 00:11:23,670
So you definitely need to make sure that whatever names you are using over here for the field actually

143
00:11:23,670 --> 00:11:26,000
match up with the names which are given over here.

144
00:11:26,550 --> 00:11:30,600
So I'll type in order and then I'll type in the respective fields.

145
00:11:30,930 --> 00:11:32,850
So I'll type and name equals name.

146
00:11:33,240 --> 00:11:34,350
Then I'll give a comma.

147
00:11:34,350 --> 00:11:38,730
I'll add the next field, which is email, which is going to be equal to email.

148
00:11:38,970 --> 00:11:41,820
Then the address is going to be equal to address.

149
00:11:42,120 --> 00:11:45,510
And you need to do this for all the fields which we have over here.

150
00:12:00,160 --> 00:12:03,530
And once we have actually done that, we are pretty much good to go.

151
00:12:03,820 --> 00:12:09,420
And now, as you can see, we actually have this particular field right here, which is the ordered

152
00:12:09,430 --> 00:12:09,940
items.

153
00:12:09,940 --> 00:12:13,980
And we also have that ordered items field over here as well.

154
00:12:13,990 --> 00:12:16,930
But we are not going to deal with that as of now.

155
00:12:17,170 --> 00:12:21,580
As of now, we just want to extract the name email and all these fields right here.

156
00:12:21,970 --> 00:12:28,120
So now once we have this particular object and once you have all that data substituted in that particular

157
00:12:28,120 --> 00:12:34,410
object to actually store this into the database, you simply need to store this particular object.

158
00:12:34,720 --> 00:12:36,810
So you need to type in order Dot Steve.

159
00:12:37,420 --> 00:12:42,880
And that's actually going to take all this data submitted to the form and it's going to create an object

160
00:12:42,880 --> 00:12:43,390
out of it.

161
00:12:43,600 --> 00:12:48,730
And this method is actually going to save that particular data into your database.

162
00:12:49,600 --> 00:12:54,260
So now let's see if we are actually able to make this work.

163
00:12:54,730 --> 00:12:58,060
So, first of all, let's see if we have the order stable.

164
00:12:58,240 --> 00:13:01,630
And I first need to make sure that the server is up and running.

165
00:13:05,420 --> 00:13:12,540
So once the server is up and running, let's now go to localhost forty eight thousand slash admin.

166
00:13:13,470 --> 00:13:14,930
Let's actually log in.

167
00:13:17,180 --> 00:13:23,090
And now if you have a look over here, we only have products because we have not yet registered the

168
00:13:23,090 --> 00:13:26,060
model, which is the orders model.

169
00:13:26,330 --> 00:13:30,050
So let's go ahead and register that in the admin, not be why.

170
00:13:30,170 --> 00:13:33,170
So here I'll simply say from DOT models.

171
00:13:35,120 --> 00:13:35,900
Import.

172
00:13:37,100 --> 00:13:44,710
Orders or orders from DOD models impose order or you can also go ahead and directly type in order over

173
00:13:44,750 --> 00:13:48,140
here as well, because we already have from DOD models.

174
00:13:48,650 --> 00:13:56,210
And now in order to register that, I'll need to type an admin DOD side DOD register and simply register

175
00:13:56,250 --> 00:13:57,730
the model, which is order.

176
00:13:58,340 --> 00:14:03,710
And one more important thing, which I forgot to mention over here, is that if you actually go to the

177
00:14:04,080 --> 00:14:11,150
you start by file, as we have actually received or we have actually created the object of audio here,

178
00:14:11,480 --> 00:14:14,970
you also need to go ahead and import order over here as well.

179
00:14:14,990 --> 00:14:17,220
So I have just imported order over here.

180
00:14:17,930 --> 00:14:22,080
So now once we are done with that, hopefully everything should work fine.

181
00:14:22,100 --> 00:14:27,980
So if I had to refresh, we have orders over here and now let's try to actually submit a order over

182
00:14:27,980 --> 00:14:30,260
here and let's see what do we get.

183
00:14:30,740 --> 00:14:33,310
So have added some dummy data over here.

184
00:14:33,560 --> 00:14:40,640
And when I click on Place Order, or we do actually have some sort of an issue over here, which is

185
00:14:41,120 --> 00:14:44,420
a local variable name referenced before assignment.

186
00:14:45,030 --> 00:14:47,240
So let's see what exactly went wrong.

187
00:14:48,020 --> 00:14:54,320
OK, so the reason for the error was actually we have forgotten to indent this particular thing.

188
00:14:54,680 --> 00:14:59,590
So this should actually be present inside the if statement and not outside of it.

189
00:14:59,990 --> 00:15:05,090
And one more thing which you need to do over here is that whenever you're submitting the form data,

190
00:15:05,120 --> 00:15:09,840
you also need to go ahead and add the CSF token right over here.

191
00:15:10,250 --> 00:15:13,580
So right inside the form and simply type in C.

192
00:15:13,580 --> 00:15:16,820
S out of underscore token.

193
00:15:17,090 --> 00:15:23,720
And this is the token which is used for security purposes and it actually stands for cross site request

194
00:15:23,720 --> 00:15:24,320
forgery.

195
00:15:24,320 --> 00:15:29,960
Which means that which means that if someone attempts a cross site request forgery, it won't be able

196
00:15:29,960 --> 00:15:30,590
to do so.

197
00:15:30,980 --> 00:15:37,850
So now once we go ahead and fix those two issues and now if I try to submit some dummy data and place

198
00:15:37,850 --> 00:15:44,100
order, as you can see, we do get the same page over here and we don't have any errors.

199
00:15:44,120 --> 00:15:49,550
So when I hit refresh and when we go to Ordos over here, we have the order object one.

200
00:15:49,550 --> 00:15:55,320
And if I click on that, as you can see, we have all the details submitted right over here.

201
00:15:55,940 --> 00:16:02,090
So as you can see, we have all the form fields submitted and the only field which is remaining is items

202
00:16:02,090 --> 00:16:02,600
over here.

203
00:16:03,110 --> 00:16:08,750
And in the upcoming lecture, we will go ahead and learn how we can also get this particular item feel

204
00:16:09,080 --> 00:16:14,930
and get all the items which we have in our cart and have them submit to the database.

205
00:16:15,140 --> 00:16:19,010
So thank you very much for watching and I'll see you guys next time.

206
00:16:19,490 --> 00:16:20,030
Thank you.

