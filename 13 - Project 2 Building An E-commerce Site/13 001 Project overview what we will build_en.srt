1
00:00:00,390 --> 00:00:06,810
So hello and welcome to this new section, and this is what exactly we will be building in this new

2
00:00:06,810 --> 00:00:07,950
specific section.

3
00:00:08,460 --> 00:00:13,340
So this is the e-commerce site which will be building and this is how it exactly looks.

4
00:00:13,830 --> 00:00:16,350
So you have these products listed up over here.

5
00:00:16,350 --> 00:00:21,270
And we have actually limited the number of products on a certain page to four products.

6
00:00:21,720 --> 00:00:26,820
And now if you actually go ahead and click on next, you will be redirected to a new page, which is

7
00:00:26,820 --> 00:00:28,800
going to have next set of products.

8
00:00:28,830 --> 00:00:32,729
So now let's go ahead and see what all features are included in the site.

9
00:00:33,330 --> 00:00:36,540
So in the site, you can actually go ahead and search for products.

10
00:00:36,550 --> 00:00:41,370
So let's say I want to search for what I can simply type in what you enter.

11
00:00:41,370 --> 00:00:45,960
And as you can see, I will get all the watches which are actually available on this site.

12
00:00:46,410 --> 00:00:49,170
So let's see if I type in laptop hit enter.

13
00:00:49,200 --> 00:00:53,730
As you can see, I got the actual laptop as well as the laptop back.

14
00:00:54,300 --> 00:00:57,180
So if I type in bike, I'll get bike over here.

15
00:00:57,420 --> 00:01:01,440
And this search feature is actually built completely using Shango.

16
00:01:01,560 --> 00:01:06,610
Now, another feature of this site is that you can actually go ahead and add products to your cart.

17
00:01:06,780 --> 00:01:12,660
So right now, if you see the car that's empty and now if I click on Add to Cart, as you can see,

18
00:01:12,660 --> 00:01:14,130
this actually changed to one.

19
00:01:14,460 --> 00:01:18,960
If I click on Add to Cart over here for this bike, it actually changed to two.

20
00:01:18,960 --> 00:01:24,660
And now if I actually go ahead and click on this particular cart, as you can see, it says that you

21
00:01:24,660 --> 00:01:29,360
have added these following items and that is it is item number one is X, Y, Z.

22
00:01:29,370 --> 00:01:30,900
Watch quantity is one.

23
00:01:30,900 --> 00:01:34,550
Item number two is bike, whose quantity is also one.

24
00:01:35,010 --> 00:01:40,140
So if you want to add another bike over here, you can simply go ahead and click on this one more time.

25
00:01:40,590 --> 00:01:42,000
And when you click order.

26
00:01:42,480 --> 00:01:46,830
And if you click on Cart, as you can see now, there are two bikes added.

27
00:01:47,250 --> 00:01:52,950
Now you can simply go ahead and click on checkout and it will actually go ahead and redirect you to

28
00:01:52,950 --> 00:01:54,000
the checkout page.

29
00:01:54,360 --> 00:02:00,150
And as you can see, we have one watch added up over here and the cost is added up over here for that

30
00:02:00,150 --> 00:02:01,140
specific watch.

31
00:02:01,530 --> 00:02:04,620
And we also have two bikes added up over here.

32
00:02:04,620 --> 00:02:07,440
And this is the total cost of those two bikes.

33
00:02:07,860 --> 00:02:10,350
And here we have our grand total as well.

34
00:02:11,039 --> 00:02:14,330
So now let's go ahead and let's try to place order here.

35
00:02:14,910 --> 00:02:16,740
So I'll go ahead and place my order.

36
00:02:17,490 --> 00:02:20,530
So I'll actually go ahead and fill up my details over here.

37
00:02:20,550 --> 00:02:26,730
So this is an autofill form, so I'll simply go ahead and paste these contents.

38
00:02:26,730 --> 00:02:32,550
And when I click on a placeholder, as you can see the page refreshed, which means that the order has

39
00:02:32,550 --> 00:02:33,450
now been placed.

40
00:02:34,020 --> 00:02:40,590
Now, if I actually want to review or see the kind of orders which I have, I can simply go ahead and

41
00:02:40,590 --> 00:02:41,460
go to Admon.

42
00:02:42,700 --> 00:02:46,300
And I can log in over here using the admin credentials.

43
00:02:48,790 --> 00:02:55,460
And if I go ahead, go to Ordos, as you can see, I have all the other details over here.

44
00:02:55,600 --> 00:02:58,110
So this is the order which I have placed right now.

45
00:02:58,120 --> 00:03:01,150
So it displays the type of items which we have ordered.

46
00:03:01,510 --> 00:03:07,720
It displays the name of the customer who has actually ordered those items and displays the email and

47
00:03:07,720 --> 00:03:09,280
it displays the total as well.

48
00:03:10,540 --> 00:03:16,880
So now if I click on this, as you can see, I can also get access to the user's address as well.

49
00:03:16,900 --> 00:03:22,870
So as you can see, we have the address of the city listed and the zip code of that specific user.

50
00:03:22,880 --> 00:03:28,810
And also, if you go to the products, as you can see, you have the option to go through all the products

51
00:03:28,810 --> 00:03:30,580
which are actually present on your site.

52
00:03:30,940 --> 00:03:33,580
And you can go ahead and edit the details over here.

53
00:03:34,000 --> 00:03:39,490
So, for example, we have these products like the dynamic watch laptop bag, bike.

54
00:03:39,510 --> 00:03:45,070
We have the price, the discount price category, so on and so forth, everything over here.

55
00:03:45,250 --> 00:03:51,610
So this is what exactly will build in this specific section that will build this specific e-commerce

56
00:03:51,610 --> 00:03:56,950
website with the features like adding products, searching the products, adding the product to cart,

57
00:03:57,340 --> 00:04:01,410
creating a dynamic cart and creating the checkout functionality as well.

58
00:04:01,780 --> 00:04:06,230
And we also have certain features like search and pagination as well.

59
00:04:06,460 --> 00:04:11,560
So from the upcoming lecture, let's go ahead and start building this specific site.

