1
00:00:00,090 --> 00:00:06,540
So now what we want to do is that we want to get the items from our local storage and actually assign

2
00:00:06,540 --> 00:00:10,590
those items up over here and save them and the items feel right here.

3
00:00:11,070 --> 00:00:17,490
So right now, if you actually go to this document over here, these items are not actually the part

4
00:00:17,490 --> 00:00:19,200
of the input field right here.

5
00:00:19,920 --> 00:00:28,200
So how exactly can we go ahead and and if we do it here and use that field as a form field so that we

6
00:00:28,200 --> 00:00:30,430
can finally submit this data over here?

7
00:00:30,480 --> 00:00:36,600
Because the way in which this form works is that if you actually go to the SPU right here, we extract

8
00:00:36,600 --> 00:00:42,720
all the items like name, email, address, city, everything like that over here, and that items actually

9
00:00:42,720 --> 00:00:44,970
get saved into this particular object.

10
00:00:45,300 --> 00:00:50,860
And then we finally go ahead and see if that object and it gets updated into the database.

11
00:00:51,300 --> 00:00:58,030
So now we actually need to add an item speal to our form to actually get those items up over here.

12
00:00:58,650 --> 00:01:05,580
So in order to do the same, let's first go to the checkout dot html and then here will actually go

13
00:01:05,580 --> 00:01:12,640
ahead and add another input fee over here and we will set the type of that input field as hidden.

14
00:01:13,020 --> 00:01:15,000
So let's go ahead and do that over here.

15
00:01:15,660 --> 00:01:20,910
So what I will do here is that I'll simply type in input type equals.

16
00:01:20,910 --> 00:01:24,020
And instead of text, the type of this thing should be hidden.

17
00:01:24,300 --> 00:01:29,250
And once we have this hidden over here, I'll go ahead and assign this an idea as item.

18
00:01:29,250 --> 00:01:33,650
And I'll also assign the name for this particular input field as item as well.

19
00:01:34,470 --> 00:01:39,140
And let's actually make these items because we are storing multiple items in here.

20
00:01:39,660 --> 00:01:45,300
And now, once we have this specific input field, our next job is to actually go ahead and fill out

21
00:01:45,300 --> 00:01:46,230
this input field.

22
00:01:46,590 --> 00:01:52,290
Now, we obviously cannot ask the user to actually fill out that input field because obviously he doesn't

23
00:01:52,290 --> 00:01:55,940
want to take all these items and fill them up manually.

24
00:01:56,130 --> 00:02:02,490
So instead, we actually need a way to automatically assign this input field, the values inside our

25
00:02:02,490 --> 00:02:02,940
cart.

26
00:02:03,510 --> 00:02:09,870
So in order to do that will simply go ahead and make use of JavaScript and query right over here.

27
00:02:10,229 --> 00:02:15,750
So in this particular JavaScript code, what we will do is that, first of all, will go ahead and select

28
00:02:15,750 --> 00:02:16,770
the items field.

29
00:02:17,220 --> 00:02:25,370
So I'll type in dollar and I'll select that on the basis of its ID, so I'll type in hash items.

30
00:02:26,610 --> 00:02:32,850
So once we have selected that, I will set its value equal to the value of the items which are actually

31
00:02:32,850 --> 00:02:33,960
present in the cart.

32
00:02:34,470 --> 00:02:41,610
So I can simply go ahead and type in Jason dot string Newfie, which will actually convert this thing

33
00:02:41,610 --> 00:02:48,630
into a string and then I will actually pass and over here, which means that this specific line of code

34
00:02:48,840 --> 00:02:54,330
will actually take the values inside the card, which is the variable card which we have just created

35
00:02:54,330 --> 00:02:54,900
right here.

36
00:02:54,990 --> 00:03:02,730
And it will actually convert that JSON object into a string and it will assign its value to the element

37
00:03:02,730 --> 00:03:09,870
whose ID is items and the element whose ID is items is nothing but the hidden input field, which we

38
00:03:09,870 --> 00:03:12,790
have just created, which is nothing but this field right here.

39
00:03:13,410 --> 00:03:20,910
So that means this field is now automatically being filled with the help of Jaquie and JavaScript.

40
00:03:21,450 --> 00:03:27,470
So now that means whenever we go ahead and submit this form, along with all the other details now,

41
00:03:27,480 --> 00:03:30,330
we also have access to items over here as well.

42
00:03:30,930 --> 00:03:34,250
So now let's go ahead and extract items over here as well.

43
00:03:34,260 --> 00:03:36,240
So I'll type in items equals.

44
00:03:37,050 --> 00:03:39,990
And I guess you have actually named that particular.

45
00:03:40,840 --> 00:03:46,860
Thing as items over here as well, and yet we have named that thing as items, so that won't be an issue.

46
00:03:47,320 --> 00:03:49,240
So we'll type in items equals.

47
00:03:50,770 --> 00:04:00,730
Requests don't post, don't get, and we'll get the items and the by default is going to be empty.

48
00:04:01,060 --> 00:04:06,460
So now once we have added these items over here, you also need to pass on those items over here as

49
00:04:06,460 --> 00:04:06,780
well.

50
00:04:06,790 --> 00:04:09,250
So you need to type in items equals.

51
00:04:11,270 --> 00:04:15,170
Items, so here you have a comma and you should be good to go.

52
00:04:15,890 --> 00:04:18,779
So now let's go ahead and see if this thing works.

53
00:04:18,800 --> 00:04:21,320
So let me actually go to the document.

54
00:04:21,500 --> 00:04:23,260
Let's go back to the home page.

55
00:04:23,810 --> 00:04:26,240
Let's add in a few more items.

56
00:04:26,540 --> 00:04:30,890
Let's go to check out let's fill this thing with some dummy data.

57
00:04:30,890 --> 00:04:33,930
And when I click on Please Order, the order is placed.

58
00:04:33,950 --> 00:04:39,830
So now when I go ahead and hit refresh, it's actually showing for over here because I tested this thing

59
00:04:39,830 --> 00:04:41,450
by placing two other orders.

60
00:04:41,840 --> 00:04:48,350
So now when I click on this, as you can see now, we have this particular item submitted over here

61
00:04:48,350 --> 00:04:49,800
in the form of a string.

62
00:04:50,360 --> 00:04:55,400
So as you can see now, we have the order details over here, which is nothing but our card.

63
00:04:55,670 --> 00:05:00,210
And we also have the other details as well submitted in the database.

64
00:05:00,260 --> 00:05:02,450
So that's it for this lecture.

65
00:05:02,570 --> 00:05:09,530
And I hope you guys be able to understand how to actually place the order and collect all these details.

66
00:05:09,870 --> 00:05:15,980
However, there is one issue with our site currently that is if you actually have a look at checkout

67
00:05:15,980 --> 00:05:20,150
page, as you can see, it's actually showing up the items.

68
00:05:20,150 --> 00:05:22,400
It's actually showing of the quantity as well.

69
00:05:22,760 --> 00:05:28,340
But it does not show the price and the total amount of the orders, please.

70
00:05:28,400 --> 00:05:34,940
So, for example, we want to check out Page, which not only shows the ordered items and the quantity,

71
00:05:34,940 --> 00:05:41,990
but we also want the price of these individual items and the total price of all the items which we have

72
00:05:41,990 --> 00:05:43,120
added in our card.

73
00:05:43,580 --> 00:05:48,050
So we are going to go ahead and design that functionality in the upcoming lecture.

74
00:05:48,410 --> 00:05:50,480
So thank you very much for watching.

75
00:05:50,480 --> 00:05:52,520
And I'll see you guys next time.

76
00:05:52,760 --> 00:05:53,360
Thank you.

