1
00:00:00,240 --> 00:00:07,320
So in this lecture, let's actually go ahead and display the price of the items from the local storage

2
00:00:07,320 --> 00:00:08,500
to the checkout page.

3
00:00:08,940 --> 00:00:15,630
So let's go to the checkout HTML file and in here, let's actually figure out a way to get the items

4
00:00:15,630 --> 00:00:16,820
from the local storage.

5
00:00:16,860 --> 00:00:22,740
So if you have a look at this particular thing, we have basically got the card from the local storage

6
00:00:22,740 --> 00:00:27,190
and we have stole it in the local JavaScript variable we just got right here.

7
00:00:27,630 --> 00:00:33,870
That means we already have access to the card variable right here and from here for each item.

8
00:00:33,880 --> 00:00:36,220
We just want to get access to its price.

9
00:00:36,750 --> 00:00:39,860
So as you can see here, we have got access to the name.

10
00:00:39,870 --> 00:00:45,480
We have got access to the quantity and in the same manner, we just now need to go ahead and get access

11
00:00:45,480 --> 00:00:46,170
to the price.

12
00:00:46,350 --> 00:00:54,300
So for price, I'll say something like let price equals and then I'll type in cut of item ID.

13
00:00:56,140 --> 00:01:03,760
A cut of item, and then, as I mentioned, the price is actually stood at a next position to so simply

14
00:01:03,760 --> 00:01:04,920
dipen to over here.

15
00:01:05,450 --> 00:01:09,130
So now we got access to the price from the local storage.

16
00:01:09,610 --> 00:01:16,400
Now, what we essentially want to do is that we simply want to go ahead and display that price up over

17
00:01:16,420 --> 00:01:18,620
here by passing it to the checkout page.

18
00:01:19,000 --> 00:01:25,600
So as you can see, we have used this item string to pass the name and the quantity of the item using

19
00:01:25,600 --> 00:01:26,400
the list item.

20
00:01:26,800 --> 00:01:29,220
And we do the same thing with the price as well.

21
00:01:29,770 --> 00:01:32,710
So let's go ahead and modify this thing a little bit.

22
00:01:32,710 --> 00:01:37,480
And instead of showing them like that, let's go ahead and use a Spangler's.

23
00:01:37,570 --> 00:01:43,930
So I'll say something like spane plus equals and let's say that's going to be empty for now.

24
00:01:44,140 --> 00:01:48,190
And let's go ahead and include this in a span tag as well.

25
00:01:48,460 --> 00:01:50,750
So I'll type in span.

26
00:01:51,610 --> 00:01:56,040
And now let's add another Spangler's over here to display the price as well.

27
00:01:56,050 --> 00:02:02,020
So I'll simply go ahead copy that piece to the here and now in place of the quantity.

28
00:02:02,020 --> 00:02:07,770
I'll simply go ahead and type in price, so I'll type in price in here and that's it.

29
00:02:07,930 --> 00:02:12,790
That's how simple it is to actually go ahead and display those items up over here.

30
00:02:12,940 --> 00:02:18,160
So now when I hit refresh, as you can see, we now do have the quantity as well.

31
00:02:18,160 --> 00:02:22,390
But as you can see, the quantity and price happened.

32
00:02:22,390 --> 00:02:27,410
You just concatenate because we have not included any kind of spacing here.

33
00:02:28,180 --> 00:02:35,320
So in order to solve this particular issue, let's actually go ahead and assign the price a different

34
00:02:35,320 --> 00:02:36,140
Spangler's.

35
00:02:36,160 --> 00:02:43,720
So for this, I'll simply go ahead and type in the class as bad as we need to display it as a badge.

36
00:02:43,720 --> 00:02:54,310
And I'll type something like badge dash, let's see warning and I'll say something like badge.

37
00:02:55,270 --> 00:03:01,270
Bill, so these are nothing, but they are actually the bootstrap classes to change the look and feel

38
00:03:01,270 --> 00:03:02,050
of the price.

39
00:03:02,080 --> 00:03:06,440
So when I hit refresh, as you can see now, the price is displayed over here.

40
00:03:06,610 --> 00:03:11,560
So for three watches, the total price is three hundred and forty two bikes.

41
00:03:11,760 --> 00:03:13,730
We have the price as six hundred.

42
00:03:14,020 --> 00:03:19,720
So one more thing which we can do here is that we can modify this thing to say something like, let's

43
00:03:19,720 --> 00:03:23,950
see two of this particular item and then we can display the price.

44
00:03:24,550 --> 00:03:28,300
So what I can do here is that I can simply cut this thing from here.

45
00:03:30,130 --> 00:03:31,840
And I'll say something like.

46
00:03:32,960 --> 00:03:33,650
This.

47
00:03:35,270 --> 00:03:36,560
Then I'll say off.

48
00:03:37,770 --> 00:03:42,800
And then we'll display the item name and then we will have the price up over here.

49
00:03:45,190 --> 00:03:46,740
So hopefully this thing should work.

50
00:03:47,850 --> 00:03:50,460
So now if we go back and hit refresh.

51
00:03:51,760 --> 00:03:57,910
I guess it actually went out of the Spanish class and we don't want that, so let's actually get this

52
00:03:57,910 --> 00:03:58,450
thing.

53
00:03:59,430 --> 00:04:02,570
Good from here and have it include right over here.

54
00:04:05,310 --> 00:04:11,880
And now it should actually work fine, and now when I hit refresh, as you can see, we have three of

55
00:04:11,880 --> 00:04:14,340
X, Y, Z watches, which cost 300.

56
00:04:14,730 --> 00:04:17,440
We have two of Bich, which actually cost 600.

57
00:04:17,970 --> 00:04:22,780
So we can go ahead and customize this thing later and style it in a much more better way.

58
00:04:22,800 --> 00:04:29,850
But now the next thing which we wish to do over here is that now we need to calculate the total of all

59
00:04:29,850 --> 00:04:31,270
the items added over here.

60
00:04:31,350 --> 00:04:37,560
So, for example, now we have the total for each individual items, but now we also need to have the

61
00:04:37,560 --> 00:04:38,460
final sum.

62
00:04:38,460 --> 00:04:40,840
That is the final amount which we need to pay.

63
00:04:41,340 --> 00:04:43,340
So six hundred plus three hundred.

64
00:04:43,350 --> 00:04:48,030
So we need a way to display 900 up over here as the total or the amount.

65
00:04:48,060 --> 00:04:51,420
So now in order to do that, it's actually quite simple.

66
00:04:51,420 --> 00:04:54,120
You just need to go ahead, go into your checkout page.

67
00:04:54,330 --> 00:04:59,940
And in here we need to go exactly right where we actually add the price of the individual items.

68
00:05:00,300 --> 00:05:01,970
So if we scroll down a little bit.

69
00:05:02,160 --> 00:05:07,530
So right over here, what we do is that we go ahead and make use of the price which is actually stored

70
00:05:07,920 --> 00:05:09,630
and calculate the total.

71
00:05:09,840 --> 00:05:15,390
So in here, as we already loop through all the items in here and we have the price of those individual

72
00:05:15,390 --> 00:05:18,330
items, now, we simply need to calculate the total.

73
00:05:18,720 --> 00:05:22,320
So first of all, I'll go ahead and create a variable called total.

74
00:05:22,320 --> 00:05:25,880
So I'll select total equals zero.

75
00:05:26,460 --> 00:05:31,520
And now what we actually need to look through and add the amount to the total.

76
00:05:31,890 --> 00:05:37,110
And one more thing which you need to take care of is that you actually need to go ahead, cut this total

77
00:05:37,110 --> 00:05:43,680
from here and pasted out of the loop over here, because every time the loop executes, the value of

78
00:05:43,680 --> 00:05:45,540
total would actually become zero.

79
00:05:46,020 --> 00:05:54,870
So in here I can simply dipen total equals total plus the individual price of each item, which is nothing

80
00:05:54,870 --> 00:05:56,790
but cost of item of two.

81
00:05:57,660 --> 00:05:59,610
So I can see got.

82
00:06:01,870 --> 00:06:05,420
Item off and then simply have to order here.

83
00:06:05,920 --> 00:06:10,930
So that's actually going to loop through all the individual prices and it's going to add it up to the

84
00:06:10,930 --> 00:06:11,340
total.

85
00:06:12,100 --> 00:06:17,710
Now, once we have this particular total now, we can actually go ahead and display this total up on

86
00:06:17,710 --> 00:06:18,650
the checkout page.

87
00:06:19,000 --> 00:06:24,740
So what we will do here is that will go ahead and add or create another string for the total over here.

88
00:06:24,820 --> 00:06:30,610
So I'll say something like total price equals.

89
00:06:31,030 --> 00:06:35,310
And in here, let's go ahead and create the list item class.

90
00:06:35,770 --> 00:06:39,990
So I'll go ahead and use the same syntax which we have used over here.

91
00:06:40,840 --> 00:06:42,290
So I'll use the sign.

92
00:06:42,310 --> 00:06:46,390
So inside of these quotes, I'll go ahead and create a life class.

93
00:06:46,390 --> 00:06:47,830
So I'll say I like class.

94
00:06:49,830 --> 00:06:50,710
Equals.

95
00:06:50,730 --> 00:06:53,310
That's going to be a list.

96
00:06:55,260 --> 00:06:57,570
Group item.

97
00:06:59,840 --> 00:07:08,330
And I'll add a few more properties in here, so I'll see something like deflects flags, justify content

98
00:07:08,630 --> 00:07:11,510
between and line.

99
00:07:12,930 --> 00:07:14,940
Items to the center.

100
00:07:15,970 --> 00:07:22,030
So these are nothing but the bootstrap classes and in here now, let me just go ahead and the L.A. tag

101
00:07:22,450 --> 00:07:26,620
and let me just actually display the total law here in bold letters.

102
00:07:26,620 --> 00:07:30,910
So I'll use the bettag in here for making the price bold.

103
00:07:31,090 --> 00:07:37,660
And I'll say something like, let's see your total.

104
00:07:38,690 --> 00:07:45,320
And now let me just go ahead and add the total law here by typing in a dollar sign so we'll see dollar.

105
00:07:47,130 --> 00:07:55,980
And then total, and now once we have the string, let me make sure that everything is correct and everything

106
00:07:55,980 --> 00:07:57,220
seems to be OK.

107
00:07:57,330 --> 00:08:05,250
But one thing which you need to remember is that as we only want to display the total price one time.

108
00:08:05,250 --> 00:08:08,430
So you can actually go ahead, cut this from here.

109
00:08:08,430 --> 00:08:14,080
And instead of having it placed inside the for loop, you can actually go ahead and place it outside

110
00:08:14,080 --> 00:08:18,870
the for loop right over here, because you don't obviously want to go ahead and loop through the total

111
00:08:18,870 --> 00:08:20,010
price every time.

112
00:08:20,340 --> 00:08:21,810
And this loop executes.

113
00:08:21,960 --> 00:08:24,260
So make sure that you get it out over here.

114
00:08:24,690 --> 00:08:30,810
And once we have this thing over here, we can now go ahead and assign this particular total price string

115
00:08:31,140 --> 00:08:32,880
to the unordered list right here.

116
00:08:33,390 --> 00:08:38,940
So now, in order to assign the total price to the unordered list, will go ahead and make use of this

117
00:08:38,940 --> 00:08:39,929
particular ID.

118
00:08:41,710 --> 00:08:49,570
So we can simply scroll down over here and we can see something like Dollo and now will get access to

119
00:08:49,570 --> 00:08:58,150
the item list, so I'll see hash item list and I'll type and not append because we want to open to that

120
00:08:58,150 --> 00:09:03,230
particular list and we wish to open the total price string, which we have just created.

121
00:09:03,400 --> 00:09:06,950
So once we go ahead and do that, let's see if this thing works.

122
00:09:07,630 --> 00:09:13,750
So now when I hit refresh, as you can see, it sees your total and we have the total up over here,

123
00:09:13,930 --> 00:09:16,120
which means that this thing now works.

124
00:09:16,750 --> 00:09:21,280
So now let's actually go ahead and add a few more items.

125
00:09:21,670 --> 00:09:24,160
Let's see if I had a couple bags here.

126
00:09:24,430 --> 00:09:30,250
And now, if you go to the card and check out, as you can see, as we have added four bags in here,

127
00:09:30,580 --> 00:09:37,750
the cost for four bags is four hundred dollars and 400 plus 300 is 700 plus six hundred is thirteen

128
00:09:37,750 --> 00:09:38,170
hundred.

129
00:09:38,440 --> 00:09:40,210
And that's what our total is.

130
00:09:40,750 --> 00:09:47,290
That means now we have successfully displayed the total number of items, the total number of the total

131
00:09:47,290 --> 00:09:52,560
cost of each item and the grand total of our existing order as well.

132
00:09:53,720 --> 00:10:00,320
So that's it for this lecture and in the next lecture, what we will do is that will go ahead and make

133
00:10:00,320 --> 00:10:05,030
a provision to actually submit this total or cost to the database as well.

134
00:10:05,090 --> 00:10:12,320
So right now, if you'll notice, we only have access to the total number of items and the user details

135
00:10:12,320 --> 00:10:12,950
over here.

136
00:10:12,950 --> 00:10:18,390
But now we'll go ahead and create a field to actually store the total price as well.

137
00:10:18,800 --> 00:10:21,270
So we are going to do that in the next lecture.

138
00:10:21,350 --> 00:10:23,360
So thank you very much for watching.

139
00:10:23,360 --> 00:10:25,340
And I'll see you guys next time.

140
00:10:25,790 --> 00:10:26,330
Thank you.

