1
00:00:00,450 --> 00:00:05,010
Now, let's go ahead and let's create a model to save our products.

2
00:00:05,040 --> 00:00:11,040
So you go to the model store, people file, and in here you want to create a model called as products.

3
00:00:11,310 --> 00:00:16,290
And you have to guess what kind of details you want to see related to a product.

4
00:00:16,530 --> 00:00:21,500
So any product on an e-commerce website has a title that is the name of the product.

5
00:00:21,840 --> 00:00:26,350
It has the price and it also has a discounted price as well.

6
00:00:26,490 --> 00:00:28,430
Then it also has a category.

7
00:00:28,740 --> 00:00:35,490
So, for example, if your product is, uh, some sort of computer, so it falls into the category like

8
00:00:35,790 --> 00:00:42,380
computers and accessories or electronics, then the product also needs to have some sort of description.

9
00:00:42,390 --> 00:00:44,540
So you have the description feel as well.

10
00:00:44,940 --> 00:00:50,420
And then the most important thing a product needs to have is that it needs to have an image.

11
00:00:50,790 --> 00:00:56,340
So in order to save all of this data, let's go ahead and create a model for products.

12
00:00:56,850 --> 00:01:04,290
So I'll type in class products and it's going to inherit from model store model.

13
00:01:05,410 --> 00:01:07,920
And now let's define those fields here.

14
00:01:08,080 --> 00:01:14,800
So, first of all, will define title, which is going to be Models Start Garfield, because obviously

15
00:01:15,130 --> 00:01:17,730
you want to steal characters in this particular field.

16
00:01:17,740 --> 00:01:19,210
So the max.

17
00:01:20,530 --> 00:01:21,220
Lenth.

18
00:01:22,980 --> 00:01:29,280
Is going to be, let's say, 200, then we need to define the price here so the price is going to be

19
00:01:29,280 --> 00:01:36,900
modest start this is going to be a flawed field as the price can be in decimal values like four point

20
00:01:36,900 --> 00:01:41,090
five dollars or four point ninety nine dollars, so on and so forth.

21
00:01:41,490 --> 00:01:45,230
After that, we also need to mention the discounted price as well.

22
00:01:45,630 --> 00:01:50,750
So keep in mind that the regular price and the discounted price are two completely different things.

23
00:01:50,760 --> 00:01:56,640
And most of the e-commerce website actually show the discounted price along with the regular price as

24
00:01:56,640 --> 00:02:04,660
well, cell type and discount price, which is going to be equal to model start.

25
00:02:05,220 --> 00:02:10,570
This is also going to be a fluid field and then we define the category over here.

26
00:02:11,910 --> 00:02:17,010
So the category is going to be models start gaffield as well.

27
00:02:17,030 --> 00:02:22,130
And let's add the max length to 200 as well.

28
00:02:22,590 --> 00:02:29,210
And then we need to have the product description now for the product description instead of using a

29
00:02:29,220 --> 00:02:29,710
coffee.

30
00:02:29,910 --> 00:02:32,580
We are going to use something which is called as a text field.

31
00:02:33,000 --> 00:02:38,550
So we type in description equals models.

32
00:02:38,550 --> 00:02:43,440
DOT, text field and text field is very similar to coffee.

33
00:02:43,590 --> 00:02:46,730
But the thing with text field is that you can fit in more data.

34
00:02:46,740 --> 00:02:53,940
So whenever you actually want to have only a few lines of characters, you use the characterful.

35
00:02:54,210 --> 00:03:01,050
But let's say if you have a lot of data, so obviously a product description of a product on an e-commerce

36
00:03:01,050 --> 00:03:03,480
website is going to be vast.

37
00:03:03,690 --> 00:03:09,180
It's going to be a lot more detailed and hence what we are going to use TextField for this.

38
00:03:09,960 --> 00:03:14,480
And now the final thing which we do is that we can add the image over here.

39
00:03:15,030 --> 00:03:21,120
But for the image, what we will do is that we won't be using the image file, but instead we will be

40
00:03:21,120 --> 00:03:26,000
using a regular characterful and we are going to to the image.

41
00:03:26,040 --> 00:03:27,070
You are linear.

42
00:03:27,210 --> 00:03:33,330
And the reason for that is because most of the websites actually have image onto a different server

43
00:03:33,330 --> 00:03:36,870
and they have the regular functioning of the website on a different level.

44
00:03:37,260 --> 00:03:42,900
So it's actually better to host your images on some different server and refer those images up onto

45
00:03:42,900 --> 00:03:54,510
your site using the are so for image, I'll type in model start gaffield as well, and the max length

46
00:03:54,510 --> 00:03:59,940
is going to be, let's say 300 because image URLs can be a lot longer.

47
00:04:00,780 --> 00:04:04,330
OK, so once we have this model, we are pretty much good to go.

48
00:04:04,680 --> 00:04:11,430
So now in order to create a database table of this model in the backend you now need to make migration's

49
00:04:11,760 --> 00:04:14,910
so we all know how exactly do we make migration's.

50
00:04:15,660 --> 00:04:17,450
First of all, let me just clear the screen.

51
00:04:18,149 --> 00:04:25,440
And first of all, in order to make migration's we type in Python three managed by.

52
00:04:27,440 --> 00:04:28,250
Margaret.

53
00:04:30,880 --> 00:04:31,840
Python three.

54
00:04:33,130 --> 00:04:41,950
Managed by MK Migration's and again, you type in Python three, managed by my grade, so you actually

55
00:04:41,950 --> 00:04:44,740
only have to type make migration's and then migrate.

56
00:04:45,110 --> 00:04:46,630
That should do the trick as well.

57
00:04:46,660 --> 00:04:49,860
So once we make the migration's, our model should be created.

58
00:04:49,870 --> 00:04:55,570
So in the next lecture we will actually go ahead and we will create the super user and we will add some

59
00:04:55,570 --> 00:04:57,530
dummy data into this particular model.

60
00:04:58,090 --> 00:05:02,080
So thank you very much for watching and I'll see you guys next time.

61
00:05:02,320 --> 00:05:02,920
Thank you.

