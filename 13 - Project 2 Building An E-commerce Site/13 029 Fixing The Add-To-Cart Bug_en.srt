1
00:00:00,120 --> 00:00:06,420
Hello and welcome to this lecture, and in this specific lecture, I actually wanted to address a bug

2
00:00:06,420 --> 00:00:09,110
which was posted by one of our students.

3
00:00:09,420 --> 00:00:15,900
So I got a query from William Roberts, and he's one of our students.

4
00:00:15,900 --> 00:00:21,600
And he mentioned that there's one potential issue with one of the features of our class.

5
00:00:21,600 --> 00:00:27,990
So technically, what he mentioned is that the code for adding items to the shopping cart does not work,

6
00:00:27,990 --> 00:00:33,320
especially when we are actually trying to add items from different pages.

7
00:00:33,330 --> 00:00:39,870
And if those items does not exist on that page, then those particular items are not shown in the cut.

8
00:00:40,260 --> 00:00:47,910
So let's say, for example, if the card actually has an item whose ID is three and if we are actually

9
00:00:47,910 --> 00:00:55,080
going to page two, the item with 83 does not exist, then it won't be able to fetch those items and

10
00:00:55,080 --> 00:00:56,290
display them in a popular.

11
00:00:56,850 --> 00:01:02,400
So this was actually a bug and this mostly occurred due to adding pagination and this happened because

12
00:01:02,400 --> 00:01:05,150
all the items were not displayed on the same page.

13
00:01:05,160 --> 00:01:08,190
So I didn't realize that this was really a bug.

14
00:01:08,530 --> 00:01:14,360
But thanks to William, because of him, it was possible for me to know that that bug existed.

15
00:01:14,610 --> 00:01:16,490
So I came up with a fix for that.

16
00:01:16,740 --> 00:01:21,480
So let me replicate the bug so that you will get an idea of what that bug actually was.

17
00:01:21,510 --> 00:01:27,720
So, yeah, if you go to the browser, as you can see, we have this item here and now.

18
00:01:27,750 --> 00:01:32,990
If I add a laptop, if I hit refresh, my laptop is added as well.

19
00:01:33,000 --> 00:01:36,900
So it works really well if you just take it like that.

20
00:01:36,900 --> 00:01:41,170
But if I search for a laptop, go to this page.

21
00:01:41,310 --> 00:01:46,950
Now, what happens is that if I click on this, as you can see, nothing happens because now it's not

22
00:01:46,950 --> 00:01:52,390
actually able to find those two items which were added to the cut and that was causing an issue.

23
00:01:52,470 --> 00:01:55,360
So as you can see, the card button no longer works here.

24
00:01:55,890 --> 00:02:04,080
And the reason why this happens is because if you actually take a look at the add to cart functionality,

25
00:02:04,290 --> 00:02:10,020
which is nothing but this thing right here, what we have essentially done here is that we are trying

26
00:02:10,020 --> 00:02:11,780
to get the IDs.

27
00:02:11,780 --> 00:02:17,820
So first of all, we actually got the idea of items from the card and then we are trying to access those

28
00:02:17,820 --> 00:02:21,450
items or get those particular items from our HTML documents.

29
00:02:21,460 --> 00:02:29,910
So this line does exactly that, that as it's trying to fetch all the items from the HTML document.

30
00:02:29,910 --> 00:02:34,720
And obviously this does not work in this case because it does not have those items on this page.

31
00:02:35,220 --> 00:02:42,630
However, if I go back here and refresh and now if I click over here now it's able to fetch those items

32
00:02:42,630 --> 00:02:44,730
because those items are actually present here.

33
00:02:44,760 --> 00:02:49,980
So what happens if I make a search and now I need to add some items?

34
00:02:49,980 --> 00:02:51,280
I need to view my card.

35
00:02:51,300 --> 00:02:52,520
It does not work here.

36
00:02:53,160 --> 00:03:01,800
So in order to fix this, what we have done is that instead of fetching those items from the HTML document,

37
00:03:01,800 --> 00:03:04,650
instead we will try to fetch items from the card itself.

38
00:03:04,740 --> 00:03:10,320
So after adding the checkout functionality for our application, what we have done is that we have actually

39
00:03:10,320 --> 00:03:13,490
added the item name as well as quantity to the local storage.

40
00:03:13,890 --> 00:03:19,680
So that means if we already have some content stored inside the local storage, we no longer need to

41
00:03:19,680 --> 00:03:23,270
go ahead and fetch items using the document.

42
00:03:23,280 --> 00:03:26,310
Instead, we would fetch items from the local storage itself.

43
00:03:26,700 --> 00:03:33,960
So over here, what I have done is that instead of getting items from the cart over here, what I have

44
00:03:33,960 --> 00:03:40,080
done is that I've actually gotten those items from the cart object and what this card object cart object

45
00:03:40,080 --> 00:03:40,560
is nothing.

46
00:03:40,560 --> 00:03:46,710
But we essentially got access to the card which is stored in the local storage, and then I have converted

47
00:03:46,710 --> 00:03:51,760
it into adjacent formats so that we will be able to loop through the quantities and names of them.

48
00:03:52,200 --> 00:03:55,730
So let me actually give you a brief demo of that over here.

49
00:03:55,830 --> 00:03:58,200
So let's clear this.

50
00:04:02,340 --> 00:04:06,420
OK, so here now, if we actually go to the.

51
00:04:07,600 --> 00:04:17,430
Local stories, so if I were the local storage, don't get item and if I would do get item count, now,

52
00:04:17,440 --> 00:04:21,600
as you can see, we already have the quantity as well as items in the car.

53
00:04:21,630 --> 00:04:24,480
So we no longer need to go ahead and access those from here.

54
00:04:25,030 --> 00:04:30,280
So once we do that now, I have actually converted this thing into Aitchison object.

55
00:04:30,290 --> 00:04:38,680
So here I have said Jason Dot, that's going to be pass and then I have simply passed in local storage

56
00:04:39,280 --> 00:04:41,320
dot that's get

57
00:04:43,690 --> 00:04:46,680
item and got.

58
00:04:47,980 --> 00:04:53,680
So what this does is that it actually gives us this particular juice on which we could access and to

59
00:04:53,680 --> 00:04:55,840
access each individual item inside that.

60
00:04:55,840 --> 00:05:02,080
Jason, I have actually used it for Loop, so if you select each item from these, you will actually

61
00:05:02,080 --> 00:05:04,940
get access to these specific items which are stored in there.

62
00:05:05,500 --> 00:05:12,130
So once you got access to those particular items, what I have done is that I have tried to access the

63
00:05:12,130 --> 00:05:13,450
items first property.

64
00:05:13,450 --> 00:05:18,760
So the first property which I have tried to access you or the item of zero, this actually gives us

65
00:05:18,760 --> 00:05:21,460
the quantity and item of one gives us the name.

66
00:05:21,490 --> 00:05:27,850
So now instead of actually getting the quantity as well as the name like that, instead what we have

67
00:05:27,850 --> 00:05:32,740
done is that we got those particular quantities from the card object itself.

68
00:05:32,770 --> 00:05:36,100
So this gives us the quantity, this gives us the name.

69
00:05:36,370 --> 00:05:39,870
And then essentially we have combined them together as a string.

70
00:05:39,880 --> 00:05:45,280
We have added the breakdown here and then we have made that into account string and that costarring

71
00:05:45,280 --> 00:05:46,420
is displayed up over here.

72
00:05:46,870 --> 00:05:50,230
So now let's check how this newly modified thing would work.

73
00:05:50,890 --> 00:05:56,650
So now if I just go to the homepage, as you can see, we have all the items.

74
00:05:56,650 --> 00:06:00,580
And obviously, if I go to the card, the card will be shown here.

75
00:06:01,120 --> 00:06:09,670
However, if I go to search laptop, if I search for a laptop, as you can see the other items on present

76
00:06:09,670 --> 00:06:15,190
here, and therefore, if I go back here and if I check that, as you can see now, we are actually

77
00:06:15,190 --> 00:06:16,480
able to access the card.

78
00:06:16,480 --> 00:06:22,690
And it also shows the name of the watch as well as the name of the product, which is back, even though

79
00:06:22,690 --> 00:06:24,200
they are not present over here.

80
00:06:24,670 --> 00:06:27,330
So if you notice, those items are not present here.

81
00:06:27,340 --> 00:06:31,480
Still, we are able to display them in the card and this is how we fix the back.

82
00:06:31,510 --> 00:06:36,450
So hopefully this addresses every student issue in the course, which was related to this book.

83
00:06:36,940 --> 00:06:39,880
And thanks again to William for pointing this thing out.

84
00:06:40,150 --> 00:06:41,350
It was of great help.

