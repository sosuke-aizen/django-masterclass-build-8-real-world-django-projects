1
00:00:00,180 --> 00:00:06,030
Hello and welcome to this lecture and in this lecture, we will go ahead and add the added functionality

2
00:00:06,030 --> 00:00:08,580
or update functionality to our application.

3
00:00:09,030 --> 00:00:14,400
So just as we have this add functionality over here, we'll go ahead and design the added functionality

4
00:00:14,400 --> 00:00:17,400
for each individual items in a similar fashion.

5
00:00:17,820 --> 00:00:19,880
So let's go back to our code right here.

6
00:00:20,070 --> 00:00:24,210
And the first thing which you need to do over here is that you need to go ahead.

7
00:00:24,420 --> 00:00:30,120
And as usual, you first need to go ahead and set up the usual pattern for the added functionality.

8
00:00:30,460 --> 00:00:34,800
So we'll go ahead and right after the path for creating items.

9
00:00:34,830 --> 00:00:40,780
I'll go ahead and type in something like, let's see, edit and in here will create a path.

10
00:00:40,920 --> 00:00:46,530
So the path is going to say something like, let's say update, because we want to update items.

11
00:00:46,980 --> 00:00:53,910
And now, along with this update, you also need to pass in the idea of the item which you want to edit,

12
00:00:54,510 --> 00:00:59,340
because in the add functionality, you can simply go ahead and randomly add any item.

13
00:00:59,730 --> 00:01:01,700
It has got nothing to do with the idea.

14
00:01:01,950 --> 00:01:08,370
But whenever you are editing a particular item, you need to make sure that you get its ID because you

15
00:01:08,370 --> 00:01:12,360
only want to edit that specific item and not any other one.

16
00:01:13,050 --> 00:01:19,680
So now, along with the update over here, you also go ahead and pass in the ID so I can type in an

17
00:01:19,830 --> 00:01:24,510
idea here, just as we have done right over here and give a slash after that.

18
00:01:25,790 --> 00:01:31,400
OK, so once you have the specific path, mention now we go ahead and add a view to this as well.

19
00:01:31,440 --> 00:01:32,780
So we'll type in views.

20
00:01:34,090 --> 00:01:38,290
Dot, leave the sports update and the school item.

21
00:01:39,440 --> 00:01:44,470
And I'll give it the same name as well, which is update and the school item.

22
00:01:45,050 --> 00:01:50,600
Now, as usual, this is going to show you an error because this view does not exist at the moment.

23
00:01:51,050 --> 00:01:57,680
Now, once we have created this in the add functionality, what we have done here is that we we had

24
00:01:57,680 --> 00:01:58,990
created a form right here.

25
00:01:59,450 --> 00:02:05,180
But in case of update functionality, you don't need to specifically go ahead and create this item form

26
00:02:05,180 --> 00:02:05,750
right here.

27
00:02:06,260 --> 00:02:12,290
And the main reason for that is because we are actually going to go ahead and use the same item form

28
00:02:12,470 --> 00:02:13,680
for the edit as well.

29
00:02:14,270 --> 00:02:16,610
So this is one step you need to skip for now.

30
00:02:17,240 --> 00:02:21,120
That is, you don't want to go ahead and create another edit form right here.

31
00:02:22,160 --> 00:02:26,150
So now you can go to the user here and create the views directly.

32
00:02:26,180 --> 00:02:28,400
So let me just go to you start profile file.

33
00:02:28,820 --> 00:02:34,430
And just as we have created The View for create item and again, go ahead of you for the update item

34
00:02:34,430 --> 00:02:34,820
as well.

35
00:02:35,270 --> 00:02:39,530
So I'll type an update and the school item.

36
00:02:40,250 --> 00:02:43,100
And in here I'll type in request.

37
00:02:44,630 --> 00:02:50,120
And along with the request, as I earlier mentioned, this is also going to accept the idea of the item,

38
00:02:50,120 --> 00:02:51,050
which we want to edit.

39
00:02:51,320 --> 00:02:58,070
So now, first of all, even before we have a form here, first we get access to the item which we want

40
00:02:58,070 --> 00:02:58,580
to edit.

41
00:02:58,820 --> 00:03:02,810
So we type in item equals item, not objects.

42
00:03:04,660 --> 00:03:10,330
Dart kit, and we get the item whose ID is going to be equal to ID?

43
00:03:11,910 --> 00:03:17,190
Now, once we go ahead and do that, we again use the same syntax for form which we have used here,

44
00:03:17,280 --> 00:03:21,690
so we'll create a form object over here by typing in form equals item form.

45
00:03:22,530 --> 00:03:26,520
And now in here you need to type and request DOT.

46
00:03:27,790 --> 00:03:28,390
Post.

47
00:03:29,370 --> 00:03:29,820
Or.

48
00:03:31,270 --> 00:03:31,790
None.

49
00:03:31,810 --> 00:03:37,960
And now, along with this, you also need to pass this item to this particular form, because when you

50
00:03:37,960 --> 00:03:43,790
go ahead and edit a particular item, this item data should already be present inside the form.

51
00:03:44,200 --> 00:03:50,170
So in order to pass that data, you simply go ahead, give a comma and type An instance equals and then

52
00:03:50,170 --> 00:03:52,740
passing the item over here as simple as that.

53
00:03:53,350 --> 00:03:56,260
And then after that, as usual, we type in if.

54
00:03:57,560 --> 00:04:03,690
Form DOT is valid in order to check the validity of the form of the form as valid.

55
00:04:03,710 --> 00:04:10,790
We go ahead and save the data for the form, the form that save and then we return redirect.

56
00:04:11,810 --> 00:04:15,650
And we need to get to the next page, as usual, so we dipen food.

57
00:04:17,260 --> 00:04:18,640
Coolen index.

58
00:04:20,079 --> 00:04:26,230
And now the final thing which we do is that we render a template, so we dipen return, render.

59
00:04:26,560 --> 00:04:31,860
And this is going to accept a request and we are going to pass in the template as well.

60
00:04:31,870 --> 00:04:36,400
So that's going to be food slash.

61
00:04:36,710 --> 00:04:39,610
And we will pass in the same template here, which is item form.

62
00:04:39,610 --> 00:04:42,970
So I'll type an item dash form, not HTML.

63
00:04:43,570 --> 00:04:47,260
And here for the context, we are actually going to pass in two things.

64
00:04:47,530 --> 00:04:51,340
First is will pass in the form as usual, because we do need the form.

65
00:04:51,610 --> 00:04:56,570
And along with this form, we also need to go ahead and pass this particular item as well.

66
00:04:56,590 --> 00:04:58,760
So I'll type an item, call an item.

67
00:04:59,260 --> 00:05:02,830
So now once we go ahead and do that, we are now good to go.

68
00:05:02,860 --> 00:05:09,880
So now if you want to edit one of those items, you simply have to go to hood slash update and then

69
00:05:09,880 --> 00:05:12,950
you need to specify the idea of the item which you want to update.

70
00:05:12,970 --> 00:05:15,750
So in my case, I want to edit the item number one.

71
00:05:15,760 --> 00:05:20,900
So I go ahead and hit enter and here I will have the option to edit the item.

72
00:05:21,250 --> 00:05:26,080
So now if I go ahead and type in a little bit the way here and click save.

73
00:05:26,200 --> 00:05:30,430
As you can see, the name of the first item changed to edit the pizza.

74
00:05:30,880 --> 00:05:37,720
Now, this actually works fine, but what we want to do is that we want to have a button over here for

75
00:05:37,720 --> 00:05:43,210
each item so that whenever we click one of these buttons, it's going to redirect us to the edit page.

76
00:05:43,210 --> 00:05:47,950
And we don't have to manually add in the you are allowed to edit each one of those items.

77
00:05:47,980 --> 00:05:51,390
So we are going to implement that functionality in the upcoming lecture.

78
00:05:51,400 --> 00:05:56,250
But for now, hopefully you guys be able to understand how to implement the added functionality.

79
00:05:56,740 --> 00:06:01,810
So the only thing which we have done here is that we have set up the you are all stored by file for

80
00:06:01,810 --> 00:06:04,450
the added functionality or the update functionality.

81
00:06:04,870 --> 00:06:10,390
We have created The View, which is called as the update item view over here.

82
00:06:11,260 --> 00:06:17,080
And we have simply extracted the item which we want to edit, and we have passed that item as an instance

83
00:06:17,080 --> 00:06:17,710
to the form.

84
00:06:17,800 --> 00:06:23,590
And the only difference is that we need to pass an item along with the form as well, because that data

85
00:06:23,590 --> 00:06:25,240
of the item needs to be edited.

86
00:06:26,320 --> 00:06:30,490
And for this, you don't have to explicitly go ahead and create a form here.

87
00:06:30,490 --> 00:06:36,610
As we have created for the item form, we have simply used the same form over here and created the object

88
00:06:36,610 --> 00:06:37,200
out of it.

89
00:06:37,780 --> 00:06:41,660
So thank you very much for watching and I'll see you guys next time.

90
00:06:42,070 --> 00:06:42,640
Thank you.

