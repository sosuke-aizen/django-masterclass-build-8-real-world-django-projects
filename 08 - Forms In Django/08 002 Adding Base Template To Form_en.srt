1
00:00:00,780 --> 00:00:06,120
OK, so when I actually tested this code right over here for the form, I actually got an error.

2
00:00:06,150 --> 00:00:10,400
It said Templated does not exist and this was nothing but a minor issue.

3
00:00:10,620 --> 00:00:15,430
And what I had done is that I had forgotten to add dot html over here for the template.

4
00:00:15,870 --> 00:00:23,580
So when we go ahead and fix that and when you go ahead and go to slash, who'd slash add to the specific

5
00:00:23,580 --> 00:00:23,910
you are.

6
00:00:24,060 --> 00:00:26,820
As you can see, you will have this form right here.

7
00:00:27,180 --> 00:00:34,530
So now here you can go ahead and add in the item name so I can type in let's see, new.

8
00:00:35,450 --> 00:00:41,450
Pancake over here and let's see, the item description is this is a new pancake.

9
00:00:42,650 --> 00:00:48,140
And let's see, the item price is, let's say 14 dollars, and by default, we already have the image

10
00:00:48,140 --> 00:00:48,770
for the item.

11
00:00:49,130 --> 00:00:54,800
Now, when I go ahead and click save, as you can see, we are not only redirected to a new page, but

12
00:00:54,800 --> 00:01:00,170
also if you scroll down a little bit, it's going to show you the newly created item over here, which

13
00:01:00,170 --> 00:01:04,430
is New Pankey, which means that our form has successfully executed.

14
00:01:04,459 --> 00:01:12,920
So now what we will do is that will go ahead and add this new order, which is what slash ad to this

15
00:01:12,920 --> 00:01:18,270
particular ad item option over here, which is going to allow us to go ahead and get access to the phone.

16
00:01:18,410 --> 00:01:26,810
So now we will go to the base of the e-mail page and in here for the ad item link right over here will

17
00:01:26,810 --> 00:01:33,650
go ahead and add the link, which is nothing but link for the form so I can go ahead and type in you

18
00:01:33,650 --> 00:01:34,000
all.

19
00:01:34,460 --> 00:01:40,850
And this is going to be food, colon, create item and create item is the name of the you are.

20
00:01:41,090 --> 00:01:47,310
So if you go to your by a file, as you can see the name of this, you are a discrete item.

21
00:01:47,780 --> 00:01:52,360
So now I can simply go ahead and use that over here.

22
00:01:52,700 --> 00:01:56,420
So I've added that up over here and this should be good to go.

23
00:01:56,990 --> 00:02:02,960
So now if I go back, hit refresh and when I click on add item, as you can see, we are now redirected

24
00:02:02,960 --> 00:02:04,670
to this form right here and now.

25
00:02:04,670 --> 00:02:05,950
I can create a new item.

26
00:02:05,960 --> 00:02:06,590
So let's see.

27
00:02:06,590 --> 00:02:08,320
I type a new item over here.

28
00:02:08,330 --> 00:02:11,300
Item description is let's say this is a new.

29
00:02:12,830 --> 00:02:19,430
Item, the price is forty five dollars now when I go ahead, click save, as you can see, the new item

30
00:02:19,430 --> 00:02:21,050
is added up over here as well.

31
00:02:21,290 --> 00:02:26,390
Now, one thing which you might have noticed here is that whenever you go into the add item, as you

32
00:02:26,390 --> 00:02:28,760
can see, it looks pretty dull and boring.

33
00:02:28,760 --> 00:02:34,760
And that's because we have not yet applied the styling from the Bistrot HTML to this particular form.

34
00:02:35,270 --> 00:02:37,670
So now let's go ahead and apply that as well.

35
00:02:38,060 --> 00:02:40,430
So now let's go back to the code.

36
00:02:41,030 --> 00:02:42,650
And if you go to the.

37
00:02:44,210 --> 00:02:51,350
Form template here, you will see that we have not yet applied any any styling, so the navigation bar

38
00:02:51,350 --> 00:02:52,790
is not pleasant either.

39
00:02:53,270 --> 00:02:56,030
So now let's make this thing extend the base template.

40
00:02:56,030 --> 00:02:59,240
So I'll type and extent that's going to be.

41
00:03:00,880 --> 00:03:07,510
Food slash based on each H.M. and now will have the blood body over here, soil type in.

42
00:03:09,060 --> 00:03:14,930
Block body and will type in the end, block right over heels and lock.

43
00:03:15,960 --> 00:03:21,630
So now if you go ahead and hit refresh, as you can see now, the bootstrap styling as well as the navigation

44
00:03:21,630 --> 00:03:23,660
bar is going to be added up over here.

45
00:03:24,060 --> 00:03:25,610
And you will have a form.

46
00:03:25,950 --> 00:03:30,720
Now, as you can see, the form still looks pretty dull and boring and you can add a little bit of styling

47
00:03:30,720 --> 00:03:31,970
to it using bootstrap.

48
00:03:31,980 --> 00:03:36,900
But for now, we will leave it as it is and we are going to style this and the upcoming lectures.

49
00:03:37,690 --> 00:03:43,920
But the main thing which we need to learn now is that, OK, we have added the functionality to go ahead

50
00:03:43,920 --> 00:03:48,430
and add items over here to our food app and it works absolutely fine.

51
00:03:48,690 --> 00:03:50,930
That is, all the items are added up over here.

52
00:03:51,270 --> 00:03:57,430
But what if you don't want to add a new item instead you want to update the existing items.

53
00:03:57,990 --> 00:04:04,320
So for that, we actually need to go ahead and create a new functionality for updating all these items

54
00:04:04,320 --> 00:04:07,020
over here, which we are going to do in the next lecture.

55
00:04:07,530 --> 00:04:09,330
So thank you very much for watching.

56
00:04:09,330 --> 00:04:11,550
And I'll see you guys next time.

57
00:04:11,820 --> 00:04:12,450
Thank you.

