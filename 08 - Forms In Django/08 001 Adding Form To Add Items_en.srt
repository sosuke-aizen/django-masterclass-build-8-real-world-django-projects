1
00:00:00,180 --> 00:00:01,960
Hello and welcome to this lecture.

2
00:00:02,100 --> 00:00:09,330
And in this lecture, we will go ahead and learn how to add a form which allows users to add food items

3
00:00:09,330 --> 00:00:10,220
to our website.

4
00:00:10,830 --> 00:00:14,200
So I hope you guys already know what a form actually is.

5
00:00:14,220 --> 00:00:20,130
It's nothing, but it's actually a page which has a bunch of input elements and a button so that whenever

6
00:00:20,130 --> 00:00:25,470
you go ahead and type some things in into the input feel and hit the button, that data is going to

7
00:00:25,470 --> 00:00:27,060
be submitted to the database.

8
00:00:27,240 --> 00:00:32,759
So if you don't already have an idea of what a form is, you can simply go ahead and have a look over

9
00:00:32,759 --> 00:00:33,060
here.

10
00:00:33,540 --> 00:00:39,330
So this thing right over here is nothing but a form which allows us to edit this particular item over

11
00:00:39,330 --> 00:00:39,640
here.

12
00:00:40,170 --> 00:00:44,060
So we are going to have something similar over here on our website.

13
00:00:44,310 --> 00:00:50,250
So instead of having to enter data from the admin panel, the users can actually go ahead and enter

14
00:00:50,250 --> 00:00:51,620
data directly over here.

15
00:00:52,630 --> 00:01:00,310
So let's go ahead and learn how to do that, so let's go ahead, go to the code editor and first of

16
00:01:00,310 --> 00:01:05,200
all, you need to go ahead and understand that in order to create a form, you you'll actually need

17
00:01:05,200 --> 00:01:10,840
to go ahead, set up the you are all partitions, create a view, create a template and everything like

18
00:01:10,840 --> 00:01:11,160
that.

19
00:01:11,500 --> 00:01:17,440
So just as we have followed these steps for creating a particular view, so creating a particular view,

20
00:01:17,440 --> 00:01:17,950
you first.

21
00:01:17,950 --> 00:01:21,960
Go ahead, go into The View, start by a file, which is this file right here.

22
00:01:22,180 --> 00:01:27,550
You write a view, then you pass in a template, then you go ahead, create a template, then you go

23
00:01:27,550 --> 00:01:33,640
ahead and set up the you are all pattern of that particular view in the you are stored by file.

24
00:01:33,670 --> 00:01:39,730
So this is a set procedure, we all know for creating a view and we are going to follow the same procedure

25
00:01:39,730 --> 00:01:41,220
to add a form as well.

26
00:01:41,830 --> 00:01:46,110
So the very first thing which we do here is that we set up a path for the form.

27
00:01:46,120 --> 00:01:53,130
So let's go ahead and set up the pad for a form which allows us to add items.

28
00:01:53,530 --> 00:01:59,650
So I'll go ahead and have a comment here and I'll say something like and items so that we understand

29
00:01:59,650 --> 00:02:02,380
that this particular part is for adding items.

30
00:02:02,710 --> 00:02:07,300
So I'll type in over here and let's say the name of this thing is going to be add.

31
00:02:07,630 --> 00:02:13,840
So whenever you type in slash food, slash add, we are actually going to be presented with a form to

32
00:02:13,840 --> 00:02:15,240
add the items.

33
00:02:15,250 --> 00:02:18,280
So let's go ahead and also assign a view to this.

34
00:02:18,280 --> 00:02:19,750
So I'll type and you don't.

35
00:02:20,320 --> 00:02:23,170
Let's say the name of that view is going to be create item.

36
00:02:23,500 --> 00:02:28,870
Now remember that we have not yet created this view as of now, but we are going to create that very

37
00:02:28,870 --> 00:02:29,190
soon.

38
00:02:29,860 --> 00:02:33,040
And also, I'll just go ahead and assign a name to this.

39
00:02:33,040 --> 00:02:33,950
You are as well.

40
00:02:33,970 --> 00:02:34,600
So let's see.

41
00:02:34,600 --> 00:02:39,660
The name which I assign over here is going to be create item as well.

42
00:02:40,610 --> 00:02:44,790
Go ahead, give a coma, and you have successfully set up the pathway here.

43
00:02:44,900 --> 00:02:48,980
Now this thing is going to show you an error because we have not created The View.

44
00:02:50,060 --> 00:02:57,110
OK, so now once this thing is created for having a form, you actually need to do one extra thing.

45
00:02:57,440 --> 00:03:03,230
And that extra thing is that you need to go ahead and go into your food app, which is this app right

46
00:03:03,230 --> 00:03:03,530
here.

47
00:03:03,860 --> 00:03:08,090
And you need to create a new file, which is called US Form Stoppie File.

48
00:03:08,270 --> 00:03:10,190
So let's go ahead and create that file up.

49
00:03:10,610 --> 00:03:16,340
So I'll just go ahead, click on new file and I'll type in as forms start up.

50
00:03:17,120 --> 00:03:23,280
So now this forms dorper is actually going to have a class for each form, which we are creating.

51
00:03:23,720 --> 00:03:28,100
So in here, the first thing which we do is that we go ahead and import forms.

52
00:03:28,490 --> 00:03:35,720
So the form, sort by file, I'll just go ahead and type in from Django import forms.

53
00:03:36,050 --> 00:03:41,480
And once I have imported forms, I'll also need to go ahead and import the model, which is Eitam,

54
00:03:41,480 --> 00:03:44,390
because this form is going to be for the item table.

55
00:03:44,600 --> 00:03:49,570
So I'll open from that models import item.

56
00:03:50,300 --> 00:03:54,860
And now once we go ahead and do that, will create a class for this particular form.

57
00:03:55,220 --> 00:03:59,390
So I lebon class and let's name this form as item form.

58
00:03:59,660 --> 00:04:06,500
So item form over here because this is a form to add items and in here it actually needs to inherit

59
00:04:06,890 --> 00:04:12,290
from the model form so I can type in forms dot model.

60
00:04:13,590 --> 00:04:18,930
From over here now in here, we actually need to create a middle class, which is going to hold the

61
00:04:18,930 --> 00:04:26,230
information about what fields should be present in that particular form, soil type and class matter.

62
00:04:27,000 --> 00:04:30,070
And here we need to specify the model which we are using.

63
00:04:30,090 --> 00:04:35,250
So the model which we want to use is the item model because this is a form related to item.

64
00:04:35,670 --> 00:04:38,210
And we also need to specify fields here.

65
00:04:39,360 --> 00:04:44,160
Which means that which specific fields do we want on that particular farm?

66
00:04:44,520 --> 00:04:49,830
So in our case, the fields are nothing but these fields right here, which is nothing but the item,

67
00:04:49,830 --> 00:04:51,990
name, description, price and image.

68
00:04:52,510 --> 00:05:00,660
So I can simply go ahead and add those fields over here so I can type an item and the school name item

69
00:05:00,660 --> 00:05:04,830
on the school description item and the school price.

70
00:05:04,830 --> 00:05:08,130
And the final one is item on the school image.

71
00:05:08,950 --> 00:05:13,180
So once I go ahead and do that, this class is pretty much ready.

72
00:05:13,740 --> 00:05:20,640
So the main purpose of this class is to tell Shango that you want to use the model, which is ideal

73
00:05:20,640 --> 00:05:21,900
for this particular form.

74
00:05:22,210 --> 00:05:28,170
And these are the fields which we want to use or we want to accept the data from user in that particular

75
00:05:28,170 --> 00:05:28,500
form.

76
00:05:28,680 --> 00:05:30,900
Okay, so now we have completed two steps.

77
00:05:30,930 --> 00:05:33,240
One step is we have actually set up the one.

78
00:05:33,240 --> 00:05:37,980
Then we have created the Swampscott profile and we have set up the item form right here.

79
00:05:38,700 --> 00:05:41,640
Now we need to create the actual template for the form.

80
00:05:42,150 --> 00:05:47,460
So again, in order to create a template, we already know that all of our site templates are actually

81
00:05:47,460 --> 00:05:48,400
present over here.

82
00:05:48,510 --> 00:05:50,820
So let's go ahead and create a new file.

83
00:05:52,000 --> 00:05:59,360
And let's name this template as let's say item, dash form, not HTML.

84
00:05:59,920 --> 00:06:04,510
Now, once you go ahead and do that now, you can go ahead and start designing the form here.

85
00:06:04,540 --> 00:06:08,890
So now in order to create a form, we simply use the form tag.

86
00:06:09,190 --> 00:06:11,850
And the method for this is going to be the post method.

87
00:06:12,790 --> 00:06:18,980
And now, even before you start creating a form, you actually need to use a tag which is called as

88
00:06:19,000 --> 00:06:20,770
the CSR of token tag.

89
00:06:20,830 --> 00:06:26,860
So this tag is used for security and I'm not going to go into much detail of this particular tag.

90
00:06:27,010 --> 00:06:32,200
But just for the sake of understanding this particular tag, which will be using here, is going to

91
00:06:32,200 --> 00:06:36,030
actually prevent cross site scripting while using forms.

92
00:06:36,040 --> 00:06:39,540
So simply dipen see us out of underscore token.

93
00:06:39,910 --> 00:06:45,580
And now what you have to do is that you simply have to go ahead and use the form here so you can simply

94
00:06:45,580 --> 00:06:50,320
type in from here and simply have a button at the end.

95
00:06:50,650 --> 00:06:56,370
So you can type in one type equals submit and let's say this thing, say something like S�vres.

96
00:06:56,590 --> 00:06:58,470
OK, so now let's understand the code.

97
00:06:58,870 --> 00:07:01,320
So this thing right here is nothing but the form tag.

98
00:07:01,330 --> 00:07:07,130
And as we actually want to post the data to the database, we are using the post method right over here.

99
00:07:07,600 --> 00:07:11,110
This is a token which you actually need to include for security reasons.

100
00:07:11,590 --> 00:07:15,760
This is simply a button over here, which is the submit button, which you have in the form.

101
00:07:16,150 --> 00:07:18,070
And this form is actually nothing.

102
00:07:18,070 --> 00:07:23,440
But it's actually a context which we will pass from a view which we have not created.

103
00:07:23,620 --> 00:07:27,610
So now let's go ahead and create a view to pass this from context.

104
00:07:28,240 --> 00:07:31,180
So now let's go to The View stockpile file.

105
00:07:31,390 --> 00:07:34,170
And in here, we need to create a new view for the form.

106
00:07:34,840 --> 00:07:37,330
So I'll go ahead and type in def.

107
00:07:37,780 --> 00:07:39,880
And the name of this view is nothing.

108
00:07:39,880 --> 00:07:44,020
But we have actually mentioned that name over here, which is called Let's Create Item.

109
00:07:44,030 --> 00:07:46,160
So you need to use the same name over there.

110
00:07:47,110 --> 00:07:51,310
So let's go to The View, Stoppie file and type and create underscore.

111
00:07:52,740 --> 00:07:53,370
All right, Jim.

112
00:07:54,510 --> 00:08:01,530
And this is going to have a request and now what we do here is that, first of all, we actually get

113
00:08:01,650 --> 00:08:03,270
the form which we are using.

114
00:08:03,780 --> 00:08:05,910
So we have already created a form here.

115
00:08:05,920 --> 00:08:11,970
So if you go to the form Stoppie file over here, as you can see, we have created the item form here.

116
00:08:12,420 --> 00:08:17,520
So this item form is actually a class and we are actually going to go ahead and create an object out

117
00:08:17,520 --> 00:08:18,270
of that class.

118
00:08:18,750 --> 00:08:26,280
So we'll type in form equals item form and then we actually need to pass in a request.

119
00:08:26,440 --> 00:08:33,900
So we type in request or post, meaning that we are either using the post request or we are using none.

120
00:08:33,929 --> 00:08:39,750
So now once we have this form object, the next thing which we do is that we actually need to go ahead

121
00:08:39,750 --> 00:08:42,809
and submit the form data or save the form data.

122
00:08:43,230 --> 00:08:49,060
Now, even before saving the form data, we need to figure out if the data of this form is valid.

123
00:08:49,650 --> 00:08:51,930
So in order to check that I type and if.

124
00:08:53,160 --> 00:09:00,750
Form dot is valid, so this is valid is actually a function or method which makes sure that the data

125
00:09:00,750 --> 00:09:02,740
which is entered into the form is valid.

126
00:09:03,540 --> 00:09:08,520
And now once we are sure that the data is valid, we can now go ahead and save the form so I can type

127
00:09:08,520 --> 00:09:10,000
and form not see what we hear.

128
00:09:10,740 --> 00:09:14,760
So the same method is actually going to help us to save the form.

129
00:09:15,180 --> 00:09:21,630
And now once the form letter is saved and once the user has actually entered the form data, we then

130
00:09:21,630 --> 00:09:24,430
want to redirect the user to some sort of a new page.

131
00:09:25,020 --> 00:09:30,240
So let's say, for example, if the user enters all the details inside the form, we want to redirect

132
00:09:30,240 --> 00:09:31,400
them to the next page.

133
00:09:31,800 --> 00:09:33,690
So I'll type in return.

134
00:09:33,990 --> 00:09:34,800
Redirect.

135
00:09:34,800 --> 00:09:38,590
So the redirect method actually redirects users to a specific page.

136
00:09:39,000 --> 00:09:41,340
So you want to redirect them to the next page.

137
00:09:41,340 --> 00:09:48,750
So I'll type in Food Colon Index and remember why we have typed in food over here, because we have

138
00:09:48,750 --> 00:09:50,870
used namespace for apps.

139
00:09:50,880 --> 00:09:55,980
And now once we have that, the final thing which we need to do now is that we need to go ahead and

140
00:09:55,980 --> 00:09:58,170
return from the main view right over here.

141
00:09:58,170 --> 00:10:00,510
So I'll type and return Rendel.

142
00:10:00,630 --> 00:10:06,120
And here we actually want to render the template, which is nothing but the template which we have just

143
00:10:06,120 --> 00:10:09,100
created, which is item dash form.

144
00:10:09,120 --> 00:10:11,290
So first of all, a person request.

145
00:10:11,310 --> 00:10:17,160
So this is the same usual thing which we do when we return a template or render a template that is you

146
00:10:17,160 --> 00:10:21,900
pass on the request, you pass on the template name and then you pass in the context.

147
00:10:22,410 --> 00:10:23,670
So here's the request.

148
00:10:23,760 --> 00:10:25,890
Now, let's go ahead and add in the template.

149
00:10:25,890 --> 00:10:30,390
So the template is food slash item dash.

150
00:10:31,360 --> 00:10:31,900
Form.

151
00:10:33,290 --> 00:10:39,590
And now for the context, as I earlier mentioned, we actually want to pass this form right here so

152
00:10:39,590 --> 00:10:45,290
I can simply go ahead and type in form and type and form over here.

153
00:10:45,950 --> 00:10:49,580
So now, as I also mentioned, if you go to the.

154
00:10:51,360 --> 00:10:55,120
Item form dot html, here is the context which I'm talking about.

155
00:10:55,410 --> 00:10:59,550
So now let's understand the flow of how exactly this form of will work.

156
00:11:00,180 --> 00:11:04,980
So first of all, let's go to the YOU to start by a file, which is this file right here.

157
00:11:05,060 --> 00:11:07,800
Okay, so this should actually be views, not view.

158
00:11:08,370 --> 00:11:10,730
So after fixing that, let's go to the floor.

159
00:11:11,190 --> 00:11:17,160
So whenever you go to the path, which is food S�gaard, it's actually going to look for the view,

160
00:11:17,160 --> 00:11:18,720
which is called Let's Create Item.

161
00:11:19,170 --> 00:11:21,870
So now let's go to The View, start pivo.

162
00:11:21,870 --> 00:11:23,880
And this is the create item view right here.

163
00:11:24,450 --> 00:11:29,210
So now it's actually going to create an object out of the item form class.

164
00:11:29,670 --> 00:11:32,440
Now, where exactly is this item form class present?

165
00:11:32,910 --> 00:11:34,560
This class is actually present.

166
00:11:35,620 --> 00:11:42,920
In The File, which is from STOPP, so it's actually going to look at this particular class.

167
00:11:42,940 --> 00:11:47,410
It's actually going to look at the model which this class is using and the fields over here.

168
00:11:47,830 --> 00:11:53,200
So from that, it will create a new form with these fields for this particular model.

169
00:11:53,710 --> 00:11:56,520
So now let's go back from where we came.

170
00:11:56,530 --> 00:11:57,950
So let's go back over here.

171
00:11:58,270 --> 00:12:01,430
Now, once it has a form, it's going to create the object from that.

172
00:12:01,900 --> 00:12:07,580
And now once we have the object, it's actually going to render a template, which is Eitam dash form.

173
00:12:07,780 --> 00:12:13,650
So the item that's formed template is nothing but the template, which is this template right here.

174
00:12:14,020 --> 00:12:17,680
And this is actually going to go ahead and use the context as a form.

175
00:12:17,680 --> 00:12:23,230
That means it is going to have all those fields over here for the item and then it's going to have a

176
00:12:23,230 --> 00:12:23,980
submit button.

177
00:12:24,460 --> 00:12:29,620
Now, when we go ahead and submit this form, what this will do is that it will check of the form item

178
00:12:29,620 --> 00:12:30,150
is valid.

179
00:12:30,160 --> 00:12:36,370
Then on the submit button, it's going to submit the form and then we will be finally redirected to

180
00:12:36,370 --> 00:12:38,650
the index page, which is this page right here.

181
00:12:39,400 --> 00:12:41,880
Now, as you can see, we are getting errors over here.

182
00:12:41,890 --> 00:12:45,720
That is because we have not yet imported item form and redirect.

183
00:12:46,270 --> 00:12:47,680
So let's go ahead and do that.

184
00:12:48,310 --> 00:12:53,410
So we are actually going to import redirect from the tangoed or shortcuts.

185
00:12:53,410 --> 00:12:56,580
So let's dipen redirect over here.

186
00:12:56,590 --> 00:13:02,620
And for the item form, we want to import that from forms.

187
00:13:02,650 --> 00:13:04,900
So from DOT.

188
00:13:06,060 --> 00:13:12,580
Forms, which is nothing, but the forms that people file, will type an import item form.

189
00:13:12,870 --> 00:13:18,450
So once we have added these imports over here, we can now go ahead and run and test our form.

190
00:13:18,600 --> 00:13:23,700
So we will go ahead and test this form in the upcoming lecture and see if this thing works fine.

191
00:13:23,880 --> 00:13:27,780
So thank you very much for watching and I'll see you guys next time.

192
00:13:28,260 --> 00:13:28,800
Thank you.

