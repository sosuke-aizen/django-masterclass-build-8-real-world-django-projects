1
00:00:00,240 --> 00:00:05,970
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to implement

2
00:00:06,000 --> 00:00:07,710
the delete functionality.

3
00:00:08,189 --> 00:00:14,100
So until now, we have added the item functionality and the update item functionality.

4
00:00:14,430 --> 00:00:18,610
Now we also need to go ahead and have a provision to delete an item.

5
00:00:19,140 --> 00:00:20,760
So let's go ahead and do that.

6
00:00:21,120 --> 00:00:27,150
So first of all, we will actually go into the all star profile and set up a dual pattern to delete

7
00:00:27,150 --> 00:00:28,210
a particular item.

8
00:00:28,230 --> 00:00:31,140
So let's go ahead and add delete here.

9
00:00:31,410 --> 00:00:38,040
And now in order to add the path for delete, we type in path and let's say for delete, we name this

10
00:00:38,040 --> 00:00:45,750
thing as delete, slash and as usual, as you want to delete a particular item, you also need the idea

11
00:00:45,750 --> 00:00:46,720
of that item.

12
00:00:47,070 --> 00:00:54,750
So I'll just go ahead and pass the idea here by typing an integer ID and then have a slash in here.

13
00:00:55,660 --> 00:00:58,510
Now we also need to create a view for that as well.

14
00:00:58,530 --> 00:01:01,380
So for now, let me just go ahead and a stipend.

15
00:01:01,940 --> 00:01:08,850
You start delete and the school item and let's say the name of this particular you are is nothing but

16
00:01:09,630 --> 00:01:14,720
delete and the school item just give a comma and now you're good to go.

17
00:01:15,270 --> 00:01:18,530
And now let's go ahead and create this delete item.

18
00:01:18,540 --> 00:01:19,160
Rewrite that.

19
00:01:19,180 --> 00:01:22,230
So now let's go to the user profile.

20
00:01:24,030 --> 00:01:30,700
Which is this file right here, and let's create the view for delete, so I'll type and def delete and

21
00:01:30,720 --> 00:01:36,180
ask of you, oh let's say delete underscore item as we have named that thing as item.

22
00:01:36,480 --> 00:01:39,150
And as usual, this is going to accept a request.

23
00:01:39,390 --> 00:01:41,780
So I'll type on request on my ID.

24
00:01:42,060 --> 00:01:45,250
That is the idea of the item which is going to be deleted.

25
00:01:45,450 --> 00:01:51,180
So first of all, in this particular delete view we actually want to get the idea of the item which

26
00:01:51,180 --> 00:01:52,110
we want to delete.

27
00:01:52,350 --> 00:02:02,340
So let's say we type an item and we get that item by typing in item, dot, object, start, get and

28
00:02:02,340 --> 00:02:06,900
we get the object whose ID is equal to this ID right here.

29
00:02:07,170 --> 00:02:09,259
So I'll type an ID equals ID.

30
00:02:09,600 --> 00:02:14,310
So this is the same thing which we have done in the update item of you over here.

31
00:02:14,820 --> 00:02:18,630
And now we go ahead and check if the method is equal to post.

32
00:02:18,930 --> 00:02:22,410
So we type in F request dot method.

33
00:02:23,440 --> 00:02:28,600
That should be method of that is equal to the post method.

34
00:02:30,570 --> 00:02:36,300
That means now we need to go ahead and delete the item and deleting the item is actually quite simple.

35
00:02:36,310 --> 00:02:41,940
You just need to go ahead and type an item, not delete, and you're good to go.

36
00:02:42,150 --> 00:02:47,160
So this delete method is automatically going to delete that specific item for you.

37
00:02:48,000 --> 00:02:51,030
Now, once the item is deleted, we need to return back.

38
00:02:52,720 --> 00:02:59,110
Do a particular page, so we type in return redirect so that we are redirected to a page which is nothing

39
00:02:59,110 --> 00:03:02,200
but food colon index.

40
00:03:03,460 --> 00:03:09,430
And then finally what we do here is that once we go ahead and click the delete button, let's assume

41
00:03:09,430 --> 00:03:15,030
you want to send a user to a particular page which confirms that you want to delete an item.

42
00:03:15,340 --> 00:03:20,190
So for that, what we do is that we return and render a page here.

43
00:03:20,680 --> 00:03:24,700
So we type, return, render and then pass the request as usual.

44
00:03:24,710 --> 00:03:30,580
Then we pass in a template over here, which is going to be a template which conforms if a particular

45
00:03:30,580 --> 00:03:31,960
item has to be deleted.

46
00:03:32,320 --> 00:03:37,690
So right now we don't have that template created, but still we are going to pass it first and then

47
00:03:37,690 --> 00:03:39,430
we will go ahead and create the template.

48
00:03:39,880 --> 00:03:46,090
So I'll type in food because that's going to be present in the food folder or the food app, the food

49
00:03:46,090 --> 00:03:46,600
slash.

50
00:03:46,780 --> 00:03:52,570
And let's say we name the template as item, dash, delete, dot, HDMI.

51
00:03:53,840 --> 00:03:54,840
That should be done.

52
00:03:55,310 --> 00:04:02,210
And now let's go ahead and also pass in the context over here as item, which is nothing but this item

53
00:04:02,210 --> 00:04:06,490
right here, because this page now needs to know which item has to be deleted.

54
00:04:06,830 --> 00:04:09,430
So it'll pass an item and item over here.

55
00:04:10,790 --> 00:04:16,459
OK, so now once we are done with this, let's go ahead and create this particular page, which is item,

56
00:04:16,459 --> 00:04:18,380
dash, delete, dot HDMI.

57
00:04:19,730 --> 00:04:26,000
But even before we do that, what we now need to do is that we need to figure out how exactly can we

58
00:04:26,000 --> 00:04:30,440
go ahead and delete a particular item and what exactly do I mean by that?

59
00:04:31,190 --> 00:04:36,560
So what I mean by that is that let's say if you go to the website and if you click on the details,

60
00:04:36,830 --> 00:04:41,810
what we wish to do here is that we wish to have a button over here, which is going to say something

61
00:04:41,810 --> 00:04:42,770
like delete.

62
00:04:42,780 --> 00:04:48,650
So whenever you go ahead and click that particular button, this particular item should be deleted.

63
00:04:49,130 --> 00:04:51,920
So now let's go ahead and implement that functionality.

64
00:04:52,340 --> 00:04:59,550
So in order to add a delete button to the detail page, we first need to go ahead and go into the detail

65
00:04:59,550 --> 00:05:00,750
that each Jemal page.

66
00:05:01,550 --> 00:05:04,430
So let's go to the detail page.

67
00:05:04,730 --> 00:05:10,130
And in here, let's add a button right over here where we have the item, name, description and price.

68
00:05:10,400 --> 00:05:13,160
Let's also go ahead and add an HGF tag.

69
00:05:13,190 --> 00:05:15,440
So I'll type in each of equals.

70
00:05:15,830 --> 00:05:19,700
And let's say this is going to be a link for now.

71
00:05:19,880 --> 00:05:21,850
So this is going to be delete.

72
00:05:22,430 --> 00:05:29,900
And now for the HP over here, we just now need to go ahead and associate this URL, which is the URL

73
00:05:29,900 --> 00:05:31,730
for delete right over there.

74
00:05:32,600 --> 00:05:34,760
So now let's go back.

75
00:05:36,570 --> 00:05:42,360
To the detail page and the you are all for that delete is nothing, but it's actually.

76
00:05:44,020 --> 00:05:54,370
Food, so we type in you all, which is in the food app, and the name of that you are is delete underscore

77
00:05:54,370 --> 00:05:54,770
item.

78
00:05:55,780 --> 00:06:01,950
So this is nothing but this name right here of that particular you are OK.

79
00:06:02,260 --> 00:06:05,680
So now once that is done, we are pretty much good to go.

80
00:06:05,770 --> 00:06:10,980
So now let's open up the app once again and now let's hit refresh.

81
00:06:10,990 --> 00:06:17,620
And now if you go to detail of any one of these pages, I guess we do have some error over here and

82
00:06:17,620 --> 00:06:19,800
it says no reverse match at food.

83
00:06:19,900 --> 00:06:24,940
OK, so the issue is that we have actually passed in the wall right here.

84
00:06:24,940 --> 00:06:30,580
But along with this, you are you also need to pass and the idea of the item which is deleted.

85
00:06:30,970 --> 00:06:35,080
So we just need to go ahead and type an item ID.

86
00:06:35,890 --> 00:06:43,060
So now once we are done with that, let's again go back, go back over here and refresh and hopefully

87
00:06:43,060 --> 00:06:45,080
this time everything should work fine.

88
00:06:45,580 --> 00:06:49,850
So now when we click on details, as you can see, you have a delete button right here.

89
00:06:50,470 --> 00:06:55,090
Now, if you click on this button again, you are going to have an error because we have not yet added

90
00:06:55,090 --> 00:06:59,020
the template which conforms if you want to delete that particular item.

91
00:06:59,050 --> 00:07:00,550
So let's go ahead and do that.

92
00:07:01,150 --> 00:07:05,680
So now let's go ahead and create a new template to confirm the deletion.

93
00:07:05,950 --> 00:07:09,820
So and templates in the food app, we go ahead and create a new file.

94
00:07:10,790 --> 00:07:15,340
Which is called as item, bash, delete, dot, HDMI.

95
00:07:16,190 --> 00:07:20,160
Now, once you have this file, let's have some basic bunch of code in here.

96
00:07:20,210 --> 00:07:22,990
So first of all, we'll go ahead and create a form here.

97
00:07:23,480 --> 00:07:27,070
And the method for this is going to be post.

98
00:07:27,080 --> 00:07:29,650
So the method is going to be post.

99
00:07:30,200 --> 00:07:35,240
And now once we have added the method over here, let's go ahead and add in some content in here.

100
00:07:35,420 --> 00:07:41,790
So first of all, we will add in the CSF token in here because we always need a series of tokens whenever

101
00:07:41,810 --> 00:07:43,560
we are creating forms in Django.

102
00:07:43,760 --> 00:07:46,250
So see as out of underscore.

103
00:07:47,710 --> 00:07:56,620
Token and now here we just go ahead and add a heading or we just display a message to alert the user

104
00:07:56,620 --> 00:07:59,050
that he's about to delete a particular item.

105
00:07:59,050 --> 00:08:05,500
So we type something like, are you sure you want to delete?

106
00:08:05,770 --> 00:08:12,130
And here we just add in the name of the item, which we are deleting now in here to get access to the

107
00:08:12,130 --> 00:08:13,270
name of the item.

108
00:08:13,510 --> 00:08:17,080
As you already know, we have passed an item as the context.

109
00:08:17,080 --> 00:08:23,470
So I can type an item that item underscore name that's going to just go ahead and pull up the name of

110
00:08:23,470 --> 00:08:28,760
the item which we are about to delete, and then we simply go ahead and add in a button in here.

111
00:08:28,930 --> 00:08:30,250
So that's going to be button.

112
00:08:31,400 --> 00:08:36,850
And the type is going to be submit and let's say it is conform.

113
00:08:37,940 --> 00:08:42,260
OK, so now let's go ahead, save the code and see if everything works fine.

114
00:08:43,010 --> 00:08:45,140
So now let's go back.

115
00:08:45,260 --> 00:08:46,770
Let's refresh.

116
00:08:46,790 --> 00:08:48,110
Let's go into details.

117
00:08:48,290 --> 00:08:49,330
Let's go to the lead.

118
00:08:49,820 --> 00:08:56,170
And again, we get an error over here with these local variable item, a reference before assignment.

119
00:08:56,810 --> 00:09:00,240
So let's figure out where exactly this error is actually coming from.

120
00:09:00,260 --> 00:09:05,330
Now, whenever you get such kind of errors, and I'm sure if you're learning Jianguo for the very first

121
00:09:05,330 --> 00:09:07,580
time, you always want to get such errors.

122
00:09:07,910 --> 00:09:11,450
So you always need to know how to read this particular error lock.

123
00:09:11,870 --> 00:09:18,020
So right now, if we read this particular error log, you'll notice that we have an error in this particular

124
00:09:18,020 --> 00:09:21,970
file, which is Vieuxtemps pie in line number 47.

125
00:09:21,980 --> 00:09:24,430
So let's go to your stockpile.

126
00:09:24,440 --> 00:09:26,150
Let's go to line number 47.

127
00:09:26,720 --> 00:09:30,140
And here, as you can see, we have an error right over here.

128
00:09:30,260 --> 00:09:37,790
And that's because we have used item over here and that should actually be item the model, not this

129
00:09:37,790 --> 00:09:38,880
item right over here.

130
00:09:39,140 --> 00:09:42,710
So this item is nothing but the model which we have imported.

131
00:09:43,010 --> 00:09:45,060
And we need to make sure that we use that.

132
00:09:45,560 --> 00:09:50,790
Now, once I go ahead, see the code and now let's go back and refresh.

133
00:09:50,810 --> 00:09:51,860
Click on details.

134
00:09:51,860 --> 00:09:52,680
Click on delete.

135
00:09:53,030 --> 00:09:57,600
So as you can see, it says, Are you sure you want to delete edit that pizza.

136
00:09:57,620 --> 00:10:01,500
So when I click confirm, as you can see, that pizza is gone from here.

137
00:10:02,060 --> 00:10:05,270
Now, let's say we also delete these two items over here.

138
00:10:05,450 --> 00:10:06,830
So let's go to details.

139
00:10:06,950 --> 00:10:08,150
Let's click on lead.

140
00:10:08,280 --> 00:10:11,000
Click on confirm that item has gone.

141
00:10:11,360 --> 00:10:12,950
Let's also delete this one.

142
00:10:15,200 --> 00:10:21,920
So that item is gone as well, which means we have successfully implemented the delete functionality

143
00:10:21,920 --> 00:10:28,520
in Shango, so that's it for this lecture and now we have completed the basic crud operation is tango,

144
00:10:28,520 --> 00:10:34,340
that is create, read, update and delete, which means now you can basically go ahead and create any

145
00:10:34,340 --> 00:10:39,430
kind of application wherein you need to create, read, update and delete data from a database.

146
00:10:39,890 --> 00:10:44,930
But now one of the most important things, which is authentication is remaining.

147
00:10:44,930 --> 00:10:50,860
That is which particular users should be able to log in, register into your website.

148
00:10:51,110 --> 00:10:56,090
So, for example, let's say if you are creating a particular site, you need to make sure that when

149
00:10:56,090 --> 00:11:03,650
people are users interacting with your website, you always need to make sure that you have the details,

150
00:11:03,980 --> 00:11:10,730
like the email address, the username and password, so that only authenticated users will be able to

151
00:11:10,730 --> 00:11:13,070
actually make modifications to your website.

152
00:11:13,700 --> 00:11:15,440
So that's it for this lecture.

153
00:11:15,440 --> 00:11:20,060
And from the next section onwards, we are going to dive deeper into authentication.

154
00:11:20,060 --> 00:11:26,610
We will learn how exactly can you register user or make him log in and log out into your website.

155
00:11:26,780 --> 00:11:28,670
So thank you very much for watching.

156
00:11:28,670 --> 00:11:30,770
And I'll see you guys next time.

157
00:11:31,190 --> 00:11:31,760
Thank you.

