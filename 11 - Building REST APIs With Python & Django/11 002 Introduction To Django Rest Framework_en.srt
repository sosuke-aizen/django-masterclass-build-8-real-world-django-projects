1
00:00:00,060 --> 00:00:08,460
So now let's go ahead and learn about Zango rest framework, so once we know what exactly an EPA is,

2
00:00:08,460 --> 00:00:14,880
we will now go ahead and make use of the general framework to make Epis and Django app.

3
00:00:15,570 --> 00:00:20,100
So the very first question is why exactly would you need an API?

4
00:00:20,220 --> 00:00:26,250
So why can't you just simply go ahead and build a simple website that offers data to your clients?

5
00:00:26,250 --> 00:00:29,250
And why exactly would you need an API?

6
00:00:30,740 --> 00:00:32,150
Make money selling data.

7
00:00:32,420 --> 00:00:39,260
So if you have a look at multiple websites which actually offer Eppi, so the API might be to provide

8
00:00:39,260 --> 00:00:46,100
stock data, the FBI might be to provide whether data or any kind of other information, you always

9
00:00:46,100 --> 00:00:52,400
will notice that these websites actually have a business model in place which allows them to sell this

10
00:00:52,400 --> 00:00:56,180
data to third parties and gain some revenue out of it.

11
00:00:56,720 --> 00:01:01,490
So with APIs, you can actually go ahead and you can sell the data, which you have.

12
00:01:01,580 --> 00:01:07,310
So, for example, let's say for some reason you have collected some data which is present on your website,

13
00:01:07,310 --> 00:01:09,830
and that data is used by your website.

14
00:01:10,040 --> 00:01:13,330
But let's see if the data is not confidential.

15
00:01:13,340 --> 00:01:19,220
And if you actually want to go ahead and release that data to the public, you can actually go ahead

16
00:01:19,220 --> 00:01:25,220
and make an API to share that data with public, either for free or you can charge a certain amount

17
00:01:25,220 --> 00:01:27,140
of fee for each API request.

18
00:01:27,800 --> 00:01:31,150
So this is the first reason why people actually make epis.

19
00:01:31,670 --> 00:01:34,730
The next thing is if you want to develop a mobile app.

20
00:01:35,180 --> 00:01:41,570
So let's say if you actually want to study startup and if you want to be present on the Web as well

21
00:01:41,570 --> 00:01:44,630
as on the mobile phones as well in terms of apps.

22
00:01:44,930 --> 00:01:50,870
So whenever you go ahead and make a website, you will actually have the website connected to a backend,

23
00:01:50,870 --> 00:01:52,390
which is a database server.

24
00:01:52,400 --> 00:01:58,530
And that database was actually going to host all of your data, which will be used by your Web app.

25
00:01:58,910 --> 00:02:04,460
So the connection between database and the Web app is quite OK, but what if you actually also want

26
00:02:04,460 --> 00:02:07,740
to share the same database with your phone as well?

27
00:02:07,790 --> 00:02:13,730
So let's see if you also want to make an Android app, then you can actually go ahead, write an API,

28
00:02:13,730 --> 00:02:19,910
which actually connects your mobile app with your backend database so that the same data is available

29
00:02:19,910 --> 00:02:22,550
on your Web app as well as your smartphone app.

30
00:02:22,580 --> 00:02:26,560
So this is another reason why you would need API for your website.

31
00:02:28,130 --> 00:02:33,120
And the third reason is let other developers integrate your content on their site.

32
00:02:33,140 --> 00:02:40,730
So by making use of an API, you can actually allow other developers to integrate your site content

33
00:02:40,880 --> 00:02:41,930
onto their site.

34
00:02:42,170 --> 00:02:48,350
And by using API, you can not just allow them to integrate content, but you can actually integrate

35
00:02:48,350 --> 00:02:54,210
an entire functionality of your site into some other people's site or the third party site.

36
00:02:54,560 --> 00:02:57,540
So the common example of this is payment gateway.

37
00:02:57,890 --> 00:03:03,260
So let's say, for example, if you're building an e-commerce website, you don't have to actually go

38
00:03:03,260 --> 00:03:05,660
ahead and write in the payment gateway from scratch.

39
00:03:05,840 --> 00:03:10,130
Instead, you can just go ahead and use the PayPal API or the stripe API.

40
00:03:10,460 --> 00:03:15,650
And using that API, you can actually integrate Payment Gateway into your site.

41
00:03:16,130 --> 00:03:23,390
That is, you're taking the entire functionality of handling payments from people or from Stripe, and

42
00:03:23,390 --> 00:03:27,050
you're actually integrating that into your existing website.

43
00:03:27,500 --> 00:03:33,410
So this was only possible because sites like PayPal ascribe actually provide you with an API, which

44
00:03:33,410 --> 00:03:34,870
you can integrate in your site.

45
00:03:35,330 --> 00:03:40,430
So if you want to build a similar functionality for your site, you will actually need to go ahead and

46
00:03:40,430 --> 00:03:41,900
make your own API.

47
00:03:41,900 --> 00:03:45,010
So now let's learn a few things about Django framework.

48
00:03:45,280 --> 00:03:51,050
So it's actually a powerful Web toolkit for building Web APIs, especially for Shango apps.

49
00:03:51,660 --> 00:03:58,650
So as I mentioned, the Januaries framework can only be used for building Web apps for Django applications.

50
00:03:58,670 --> 00:04:04,310
So if you have any Django application, you don't need to actually go ahead and start building the apps

51
00:04:04,310 --> 00:04:05,060
from scratch.

52
00:04:05,300 --> 00:04:11,960
Instead, you can use the Django test framework and it will be a whole lot easier to create APIs using

53
00:04:11,960 --> 00:04:13,400
these Django test framework.

54
00:04:14,300 --> 00:04:17,670
Now here are the requirements for using Django as a framework.

55
00:04:18,160 --> 00:04:20,450
You'll obviously need a Django application.

56
00:04:20,450 --> 00:04:24,400
So Django, this framework only works when you're making Django apps.

57
00:04:24,410 --> 00:04:30,740
So that's quite obvious from the name of the framework itself and the Python version, which Django

58
00:04:30,740 --> 00:04:34,400
this framework currently supports is Python three point five and above.

59
00:04:34,700 --> 00:04:38,570
So you should be either using three point five, three point six or three point seven.

60
00:04:38,900 --> 00:04:40,820
Either one of them should work just fine.

61
00:04:41,840 --> 00:04:48,770
And the Django version, which Django Framework currently supports, is Django version one point eleven

62
00:04:48,770 --> 00:04:49,330
and above.

63
00:04:49,730 --> 00:04:53,780
So you can use one point eleven to point to two point one or two point two.

64
00:04:54,680 --> 00:04:59,440
So now let's go ahead and install Django as a framework in the next lecture.

