1
00:00:00,240 --> 00:00:06,150
Hello and welcome to this show and in this lecture, let's actually go ahead and learn something about

2
00:00:06,150 --> 00:00:09,930
adding API endpoints to rest API.

3
00:00:10,500 --> 00:00:17,460
So now the name API endpoints might sound a little bit confusing, but let's go ahead and take an example

4
00:00:17,460 --> 00:00:19,600
and it will all make sense to you.

5
00:00:20,220 --> 00:00:25,380
So let's go to the model, which is the movie data model right here and now.

6
00:00:25,380 --> 00:00:27,960
As you can see, we have a bunch of fields in there.

7
00:00:27,960 --> 00:00:32,170
So we have the name of the movie, the duration of the movie and rating of the movie.

8
00:00:32,700 --> 00:00:41,490
So let's say, uh, you actually want your API to work in a way which allows the users to use your API

9
00:00:41,490 --> 00:00:46,770
in a manner that they should be able to get different types of movies upon request.

10
00:00:47,100 --> 00:00:52,470
So let's say, for example, if they only want to request for action movies, they should get action

11
00:00:52,470 --> 00:00:52,980
movies.

12
00:00:53,160 --> 00:00:58,650
If they only want comedy movies, they should only get the comedy movies and they should not get anything

13
00:00:58,650 --> 00:00:58,950
else.

14
00:00:59,580 --> 00:01:06,720
So how exactly can we go ahead and implement this in a rest API so we can do that by using something

15
00:01:06,720 --> 00:01:09,150
which is called as API endpoints?

16
00:01:10,470 --> 00:01:13,370
So let's go ahead and learn how exactly can we do that?

17
00:01:13,920 --> 00:01:22,140
So in order to categorize our movies, we actually need to have a field in our movie data model which

18
00:01:22,140 --> 00:01:26,340
allows us to determine which category does this particular movie belong to.

19
00:01:27,030 --> 00:01:32,850
So here what we will do is that we will make a slight modification to this particular model by adding

20
00:01:32,850 --> 00:01:35,220
a new field, which is coreless type.

21
00:01:35,670 --> 00:01:37,350
So I can type and type here.

22
00:01:37,860 --> 00:01:45,290
But as type is actually a keyword, we cannot use that and said, I'll just go ahead and add DiVittorio

23
00:01:45,300 --> 00:01:45,570
here.

24
00:01:45,840 --> 00:01:52,860
So this debris field is going to give us information about the type of movie and it can be an action

25
00:01:52,860 --> 00:01:56,460
movie, a comedy movie thriller or something like that.

26
00:01:56,680 --> 00:02:00,090
So let's go ahead and add in model start character.

27
00:02:00,090 --> 00:02:06,410
Feel for this particular field and let's add the max length to two hundred.

28
00:02:07,530 --> 00:02:12,940
And also we need to have some default value for the current field, which we have just created.

29
00:02:12,960 --> 00:02:13,740
So let's see.

30
00:02:13,740 --> 00:02:18,600
The current movies which we have in our database are all action movies.

31
00:02:18,600 --> 00:02:22,260
So I'll type in default equals action.

32
00:02:23,500 --> 00:02:29,850
So once we have that, we are pretty much good to go now as we have made changes to this particular

33
00:02:29,850 --> 00:02:30,300
model.

34
00:02:30,660 --> 00:02:32,970
Now we need to make migration's once again.

35
00:02:33,420 --> 00:02:34,950
So let's open up the terminal.

36
00:02:35,230 --> 00:02:37,770
First of all, stop the server here.

37
00:02:39,200 --> 00:02:50,360
And now, in order to make Migration's Al Dipen Python three managed IPY Make Migration's, then I need

38
00:02:50,360 --> 00:02:57,730
to type in Python three managed IPY sequel migrate.

39
00:02:57,740 --> 00:03:02,370
That's going to be movies 0002.

40
00:03:02,410 --> 00:03:10,970
Now let's go ahead and type in Python three managed IPY migrate and then I go ahead and hit enter.

41
00:03:11,510 --> 00:03:17,810
The modifications to the database are now complete and now once the model is modified, the next thing

42
00:03:17,810 --> 00:03:23,030
which we need to do is that we need to create a special kind of overview in our views.

43
00:03:23,030 --> 00:03:24,000
Togba file.

44
00:03:24,500 --> 00:03:27,260
So let's jump into The View Stoppie file right here.

45
00:03:27,590 --> 00:03:32,480
And let's say you need to create a view for action movies.

46
00:03:32,690 --> 00:03:39,080
So now, as earlier mentioned, we can now go ahead and create different kinds of views for different

47
00:03:39,080 --> 00:03:40,340
categories of movies.

48
00:03:40,370 --> 00:03:42,490
So here's how exactly you can do that.

49
00:03:42,890 --> 00:03:45,980
You can type in class the name of The View.

50
00:03:46,280 --> 00:03:52,790
So let's see, as we are working with action movies, I can type an action view set.

51
00:03:53,930 --> 00:03:57,650
And this is going to inherit from you said dot model.

52
00:03:57,680 --> 00:04:00,160
You said so there's nothing new here.

53
00:04:00,320 --> 00:04:03,580
We are simply going to use the same code as we had over here.

54
00:04:04,040 --> 00:04:09,440
But the only thing which we are going to change is that while creating the database, we are not going

55
00:04:09,440 --> 00:04:10,130
to query all.

56
00:04:10,130 --> 00:04:16,880
We are not going to get all of the objects, but we are actually going to fill the objects by type so

57
00:04:17,060 --> 00:04:24,080
I can type in query set equals movie data, dot objects.

58
00:04:26,200 --> 00:04:34,180
Don't filter, and we are going to filter movies on the basis of the type, so the type is going to

59
00:04:34,180 --> 00:04:43,450
be action over here and now will, as usual, mentioned the serializer class, which is movie serializer.

60
00:04:44,720 --> 00:04:47,510
So once we have that, we are pretty much good to go.

61
00:04:48,080 --> 00:04:53,690
So now once we have this particular view, we need to set up a dual pattern for it over here in the

62
00:04:53,690 --> 00:04:55,090
US and start by file.

63
00:04:55,790 --> 00:05:03,130
So the very first thing which we are going to do here is that we are no longer going to use the DeFalco

64
00:05:03,140 --> 00:05:06,940
to what we are going to use something which is called as a simple router.

65
00:05:07,130 --> 00:05:09,590
So change this default router to.

66
00:05:11,170 --> 00:05:12,170
Simple rota.

67
00:05:12,760 --> 00:05:18,700
And now once we have this simple water, we can now go ahead and register the newly created view right

68
00:05:18,700 --> 00:05:21,880
over here so you can type in row to register.

69
00:05:23,200 --> 00:05:30,460
And then let's assign a you of action to this particular new view, and that's actually going to be

70
00:05:31,060 --> 00:05:31,600
action.

71
00:05:31,660 --> 00:05:34,770
You said so action, you said.

72
00:05:35,350 --> 00:05:38,240
And we also need to make sure that we import action.

73
00:05:38,260 --> 00:05:39,230
You said over here.

74
00:05:39,760 --> 00:05:46,110
So in order to import action, you said we go ahead and Dipen Colma action.

75
00:05:46,120 --> 00:05:49,420
You said and once we do that, we are pretty much good to go.

76
00:05:50,710 --> 00:05:56,640
Now, once this view is actually registered, let's actually go ahead Cevdet and let's run the server.

77
00:05:56,740 --> 00:06:03,070
So Python three managed IPY run so let's open up our proposal.

78
00:06:05,710 --> 00:06:13,840
And if we go to movies, we have this, and if we go to action, as you can see, we almost have all

79
00:06:13,840 --> 00:06:16,750
the movies here because all of them are action movies.

80
00:06:17,260 --> 00:06:18,600
Now, let's do one thing.

81
00:06:18,640 --> 00:06:23,440
Let's actually try to add a movie with some different type.

82
00:06:23,680 --> 00:06:29,800
So right now, if you go down over here, as you can see, this does not actually allow you to add a

83
00:06:29,800 --> 00:06:32,200
feel, which is the type feel.

84
00:06:32,500 --> 00:06:40,000
So in order to modify that, what you can simply do is that you can go to the serialized or file and

85
00:06:40,000 --> 00:06:44,130
in here also add type, which is dippie.

86
00:06:44,830 --> 00:06:50,440
And now if you go back over here and hit refresh, as you can see now, this form should allow us to

87
00:06:50,470 --> 00:06:52,570
add the type of the movie as well.

88
00:06:53,260 --> 00:06:55,230
So let's go ahead and create a new movie.

89
00:06:55,300 --> 00:06:58,500
Let's call it as a comedy movie.

90
00:06:59,110 --> 00:07:01,090
Let's see, the duration is one hour.

91
00:07:02,990 --> 00:07:08,870
Let's see, the rating is four point eight and the type is obviously going to be.

92
00:07:10,250 --> 00:07:17,020
Comedy, and when I go ahead and click post, as you can see now, we do have a comedy movie over here,

93
00:07:17,300 --> 00:07:23,930
but now if we actually go ahead and type in action over here, let's see what do we get?

94
00:07:24,170 --> 00:07:30,320
So when I type in action, as you can see now, you only get an action movie over here and not a comedy

95
00:07:30,320 --> 00:07:32,110
movie, which we have just created.

96
00:07:32,390 --> 00:07:35,720
And that's because the type of that movie is different.

97
00:07:36,290 --> 00:07:43,280
So now if you actually want to create a separate endpoint for the comedy movie, what you simply need

98
00:07:43,280 --> 00:07:49,100
to do is that you need to repeat the same steps as you have done for this particular action movie.

99
00:07:49,910 --> 00:07:56,550
So what you can simply do here is that you can go ahead and simply create a new class over here for

100
00:07:56,810 --> 00:08:02,210
a new view, which is going to be the comedy category so you can type in class.

101
00:08:05,490 --> 00:08:14,700
Comedy, which you said you can type, and you said the old model, you said, and then you can also

102
00:08:14,700 --> 00:08:19,920
make a query said like movie DataDot objects.

103
00:08:22,600 --> 00:08:30,820
Not filter, and this time we are going to add a filter, which this type is equal to comedy, and then

104
00:08:30,850 --> 00:08:35,230
the serializer class is going to be the same, which is the movie Serializer.

105
00:08:35,620 --> 00:08:41,080
So now once we have this, let's go ahead and add you all for this as well.

106
00:08:41,710 --> 00:08:47,710
So in here, let's go ahead and register a new route so I'll type and rotate out register.

107
00:08:48,430 --> 00:08:58,630
This is going to be comedy and this is going to be comedy value set and make sure that you have imported

108
00:08:58,630 --> 00:09:00,320
the comedy you set over here.

109
00:09:00,580 --> 00:09:03,700
So once we are done with this, we are pretty much good to go.

110
00:09:03,730 --> 00:09:10,380
So now let's see the service up and running and we do have some Eddo here and line number 18.

111
00:09:10,630 --> 00:09:14,510
So let's go over there and I'm actually missing a equals sign over here.

112
00:09:14,530 --> 00:09:17,110
So once we add that, we are pretty much good to go.

113
00:09:17,680 --> 00:09:25,480
So now if I type in comedy over here and enter, as you can see, it's actually showing us to comedy

114
00:09:25,480 --> 00:09:26,460
movies over here.

115
00:09:26,860 --> 00:09:31,360
And I guess I have accidentally added the same movie twice.

116
00:09:31,360 --> 00:09:34,790
And henceforth we are getting the two movies right over here.

117
00:09:35,470 --> 00:09:38,980
But now let's actually go ahead and add another comedy movie.

118
00:09:39,010 --> 00:09:42,010
Let's type in new comedy.

119
00:09:42,580 --> 00:09:46,210
Movie duration is, let's see, two hours.

120
00:09:46,390 --> 00:09:46,810
Let's see.

121
00:09:46,810 --> 00:09:48,040
The rating is five.

122
00:09:49,130 --> 00:09:52,120
The type should be comedy.

123
00:09:53,110 --> 00:09:54,190
Let's click post.

124
00:09:55,950 --> 00:10:03,420
And now if you actually visit comedy, as you can see now you have three movies right over here which

125
00:10:03,420 --> 00:10:08,410
are comedy movies, and now if you want action movies, you can type an action.

126
00:10:09,000 --> 00:10:10,950
Now you have a list of action movies.

127
00:10:10,950 --> 00:10:16,770
And if you only want movies, you can simply type in movies here and you are going to get all the movies.

128
00:10:17,160 --> 00:10:20,220
That is action movies as well as comedy movies.

129
00:10:20,230 --> 00:10:21,930
So that's it for this lecture.

130
00:10:21,960 --> 00:10:28,200
And hopefully you guys now understood what Eppi and points actually are and what is the use.

131
00:10:28,590 --> 00:10:35,580
So the eppi endpoints actually allow you to give different sorts of results to the end user using a

132
00:10:35,580 --> 00:10:36,180
different you.

133
00:10:36,180 --> 00:10:37,320
All right over here.

134
00:10:37,530 --> 00:10:42,720
So when you only want to display a certain kind of information to the user, you can have different

135
00:10:42,720 --> 00:10:45,200
endpoints for different types of info.

136
00:10:46,170 --> 00:10:47,720
So that's it for this lecture.

137
00:10:47,820 --> 00:10:53,230
And in the next lecture will go ahead and learn how to add image fields to your API.

138
00:10:53,940 --> 00:10:57,900
So thank you very much for watching and I'll see you guys next time.

139
00:10:58,290 --> 00:10:58,830
Thank you.

