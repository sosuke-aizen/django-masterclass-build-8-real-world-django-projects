1
00:00:00,120 --> 00:00:05,800
So let's go ahead and start building the views for this particular serializer right away.

2
00:00:06,420 --> 00:00:12,780
So the very first thing which you need to do is that you need to go to the used up of your movies.

3
00:00:13,380 --> 00:00:19,590
And in here you need to go ahead and import something which is called, as you said, from a risk framework.

4
00:00:19,830 --> 00:00:25,080
So you need to type in from rest and Lesco framework import.

5
00:00:26,840 --> 00:00:33,950
You said and also you need to go ahead and import the movie Serializer, which we have just created,

6
00:00:33,960 --> 00:00:35,860
so you need to import that as well.

7
00:00:36,170 --> 00:00:39,410
And that's actually present in the serializer stockpile file.

8
00:00:39,680 --> 00:00:43,810
So we'll dipen from Dawid serializer import.

9
00:00:44,030 --> 00:00:47,200
That's going to be movie serializer.

10
00:00:48,200 --> 00:00:52,960
And we also need to import the model as well, which is movie data.

11
00:00:53,210 --> 00:01:00,400
So we'll actually go ahead and type in from DOT models import.

12
00:01:00,500 --> 00:01:02,150
That's going to be movie data.

13
00:01:02,870 --> 00:01:09,530
OK, so once we have these imports, we can now go ahead and simply create a Class-Based feel for this

14
00:01:09,530 --> 00:01:10,780
particular serializer.

15
00:01:10,970 --> 00:01:17,660
So I'll type in class and let's name Disp as movie you set.

16
00:01:18,440 --> 00:01:26,720
And this is going to be you said start model, you said, and now it simply needs to have a query set

17
00:01:26,720 --> 00:01:28,100
and a serializer class.

18
00:01:28,670 --> 00:01:35,210
So I'll type in query set and for the queries that will actually query the model which is nothing but

19
00:01:35,210 --> 00:01:36,410
the movie data model.

20
00:01:36,590 --> 00:01:38,690
So I'll type in movie data.

21
00:01:40,060 --> 00:01:41,770
Not objects.

22
00:01:43,530 --> 00:01:50,610
Not all, and then I'll just go ahead and have a serializer class over here, so I'll type in serializer

23
00:01:50,610 --> 00:01:56,880
class is going to be nothing but the serializer, which we have just created, which is the movie Serializer.

24
00:01:56,940 --> 00:01:59,620
So I can just copy that and paste it up over here.

25
00:02:00,870 --> 00:02:04,000
OK, so once we are done with this, that's pretty much it.

26
00:02:04,020 --> 00:02:06,820
We do have a view for our serializer right now.

27
00:02:07,140 --> 00:02:12,810
Now our next job is to actually go ahead and set up a dual pattern for this particular view.

28
00:02:12,870 --> 00:02:15,980
So now let's go ahead and set up the usual pattern for this.

29
00:02:16,710 --> 00:02:18,140
So let me just go ahead.

30
00:02:18,150 --> 00:02:20,160
Go to you.

31
00:02:20,190 --> 00:02:21,180
I'll start by.

32
00:02:21,780 --> 00:02:26,240
And in here, the very first thing which you need to do is that you need to import include.

33
00:02:27,480 --> 00:02:32,280
And after that, instead of creating the you are in a regular way.

34
00:02:32,280 --> 00:02:37,590
What we need to do here is that as we are using the Changaris framework, we need to go ahead and import

35
00:02:37,590 --> 00:02:39,780
something which is called as rotas.

36
00:02:40,350 --> 00:02:47,670
So we have something which is called Lesotho's, which actually allows us to create roots for our epis.

37
00:02:47,970 --> 00:02:52,860
So let's go ahead and type in from this framework import.

38
00:02:54,000 --> 00:02:55,280
That's going to be Rotor's.

39
00:02:55,860 --> 00:03:03,600
And now what we do is that we also import the movie, you use it as well, so we'll type in from movies.

40
00:03:04,740 --> 00:03:05,250
Don't.

41
00:03:06,770 --> 00:03:15,650
We use import, that's going to be movie set, said so once we have this view, let's go ahead and create

42
00:03:15,650 --> 00:03:16,090
a room.

43
00:03:16,670 --> 00:03:22,450
So in order to create a route, we'll go ahead and use the doubters, which we have just imported.

44
00:03:22,460 --> 00:03:23,450
So we'll type in.

45
00:03:24,640 --> 00:03:32,140
Rotor equals rotors, which is disrupters right here, and the rotor, which will be using for now,

46
00:03:32,140 --> 00:03:33,860
is going to be a default rotor.

47
00:03:34,660 --> 00:03:40,030
So once we have this default rotor with us, we are not going to register the view, which is nothing,

48
00:03:40,030 --> 00:03:41,620
but the movie said.

49
00:03:42,130 --> 00:03:43,720
So let me just copy that.

50
00:03:43,730 --> 00:03:48,660
And in here we will type in the rotor dart register.

51
00:03:49,660 --> 00:03:52,490
That's going to be a path here.

52
00:03:52,960 --> 00:04:01,230
So we want to add a pad for the movies epic as movies itself, and then we will actually go ahead,

53
00:04:01,240 --> 00:04:04,160
give a comma and paste that view over here.

54
00:04:04,480 --> 00:04:07,990
So once we go ahead do that, we are pretty much good to go.

55
00:04:08,530 --> 00:04:12,760
And now the only thing which remains is that we simply need to go ahead and include this.

56
00:04:12,760 --> 00:04:16,959
You are all over here in the a pattern so I can type in path.

57
00:04:18,649 --> 00:04:26,770
I can keep this thing as empty and I can type and include and just include the rotor that you ordered,

58
00:04:26,780 --> 00:04:28,240
which we have just created.

59
00:04:28,250 --> 00:04:33,890
So it's going to be allowed to adopt yours and give a comma at the end.

60
00:04:34,190 --> 00:04:37,130
And once we are done with this, we are pretty much good to go.

61
00:04:37,640 --> 00:04:41,880
So now let's go ahead and see if this app actually works.

62
00:04:42,560 --> 00:04:49,670
So what I will do for now is that I just go ahead and open up the terminal and the server is already

63
00:04:49,670 --> 00:04:50,390
up and running.

64
00:04:51,170 --> 00:04:53,780
So now if we go to.

65
00:04:55,350 --> 00:05:03,900
Slash movies, as you can see now, we have this epic over here, so you can see we already have this

66
00:05:03,900 --> 00:05:09,030
detail here, which is we have the ID as one, we have the name as my movie.

67
00:05:09,390 --> 00:05:11,620
We have the duration and reading as well.

68
00:05:12,480 --> 00:05:18,490
And if you scroll down a little bit, as you can see, we also have a form over here to submit data.

69
00:05:18,840 --> 00:05:21,840
So let's go ahead and try to add another movie over here.

70
00:05:22,060 --> 00:05:23,010
So let's see.

71
00:05:23,610 --> 00:05:28,200
All the new movie is my new movie.

72
00:05:28,830 --> 00:05:31,430
Let's see, the duration is of three hours.

73
00:05:31,440 --> 00:05:33,760
Let's see, the rating is four point five.

74
00:05:34,500 --> 00:05:40,470
So when we go ahead and click post, as you can see now, we have this new movie added up over here,

75
00:05:40,470 --> 00:05:42,360
which is called as my new movie.

76
00:05:42,360 --> 00:05:49,230
And if you again go here to movies and hit refresh, as you can see now, instead of one movie, we

77
00:05:49,230 --> 00:05:55,620
have two movies here in ages and format, which means now Eppi works successfully.

78
00:05:55,650 --> 00:06:02,250
So that's how simple it is to actually go ahead and create a simple API using Januaries framework for

79
00:06:02,250 --> 00:06:04,290
any particular Django application.

80
00:06:04,500 --> 00:06:09,620
So in the upcoming lectures, we will learn about a few more features of the Django framework.

81
00:06:10,110 --> 00:06:15,600
But for now, let's actually go ahead and revise what we have done in this particular lecture.

82
00:06:15,960 --> 00:06:21,420
So in the previous lecture, we had already created this particular serializer and now we actually wanted

83
00:06:21,420 --> 00:06:23,660
a view for this particular serializer.

84
00:06:23,970 --> 00:06:25,350
So we first went ahead.

85
00:06:25,590 --> 00:06:27,120
We imported a few things.

86
00:06:27,120 --> 00:06:33,240
We had imported view sets from this framework because we actually need those view sets to inherit in

87
00:06:33,240 --> 00:06:35,900
this particular class because we needed a model.

88
00:06:35,910 --> 00:06:40,910
You said then we have imported the serializer itself, which we have created right over here.

89
00:06:41,340 --> 00:06:45,900
We have imported this particular model as well, which is our movie data model.

90
00:06:46,260 --> 00:06:53,330
And then we have simply created a view which is nothing but a class based view by typing in class movie.

91
00:06:53,370 --> 00:06:54,360
You said so movie.

92
00:06:54,360 --> 00:06:59,040
You said here is the name of our view we have inherited from you said stock model.

93
00:06:59,070 --> 00:07:06,090
You said then we have basically made a query set over here and we have created the movie data model.

94
00:07:06,570 --> 00:07:11,900
And after creating the model, we have also added a serializer class over here, which is nothing but

95
00:07:12,030 --> 00:07:13,140
movie serializer.

96
00:07:13,380 --> 00:07:19,650
And once we were done with that, we actually went ahead and we created a new model pattern for this

97
00:07:19,650 --> 00:07:20,490
particular view.

98
00:07:20,640 --> 00:07:27,000
So in order to create that, we have used something which is called as rotas, which we have also imported

99
00:07:27,000 --> 00:07:28,110
from the last framework.

100
00:07:28,470 --> 00:07:33,720
And using the Sotos, we have used the default to to create our actual Rotel.

101
00:07:33,720 --> 00:07:37,390
And we have registered the movie you set into that Rouda.

102
00:07:37,860 --> 00:07:44,220
Now, once this was registered, we have simply included Rotu that you are also here in the you are

103
00:07:44,220 --> 00:07:51,710
the patterns so that whenever we go ahead and type in movies, we should actually get the movie API.

104
00:07:51,810 --> 00:07:53,540
So that's it for this lecture.

105
00:07:53,550 --> 00:07:58,660
And in the upcoming lecture, we are going to learn something about API endpoints.

106
00:07:59,130 --> 00:08:03,210
So thank you very much for watching and I'll see you guys next time.

107
00:08:03,570 --> 00:08:04,170
Thank you.

