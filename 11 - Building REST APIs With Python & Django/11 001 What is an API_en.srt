1
00:00:00,060 --> 00:00:06,930
Hello and welcome to this new section on how to build recipes using the Django risk framework.

2
00:00:07,230 --> 00:00:13,350
So even before we actually go ahead and start learning about how exactly can we use sangomas framework

3
00:00:13,350 --> 00:00:18,480
to build recipes, we first need to understand what real steps actually are.

4
00:00:18,630 --> 00:00:25,630
So if you already know what APIs are, you can skip this part and you can proceed with the next lecture.

5
00:00:25,650 --> 00:00:30,180
But for those of you who don't know what WPA is, is what it actually is.

6
00:00:30,780 --> 00:00:38,400
So let's assume you actually want to build a mobile application, which is going to allow users to access

7
00:00:38,400 --> 00:00:42,150
or see the movie timings across your city.

8
00:00:42,630 --> 00:00:49,680
So let's say, for example, I want an app which easily allows me to check movie timings across my area

9
00:00:49,680 --> 00:00:55,680
or across my city, or for some reason, I want to check the movie timings across the globe.

10
00:00:56,070 --> 00:01:02,730
So building such app is actually quite an easy task because you just have to go ahead and build a simple

11
00:01:02,730 --> 00:01:07,950
view and list out all the movies, all the movie theaters, and you just have to display the movies

12
00:01:07,950 --> 00:01:09,050
which are playing over there.

13
00:01:09,060 --> 00:01:15,750
But the main challenge here in building this app is where exactly are you going to get the movie data

14
00:01:15,750 --> 00:01:16,740
across the globe?

15
00:01:17,190 --> 00:01:21,700
So that's one major challenge which you are going to face while developing this app.

16
00:01:22,230 --> 00:01:27,810
So what you'll need to do is that you'll need to hire multiple people across the globe so that each

17
00:01:27,810 --> 00:01:32,730
one of those can actually work in an area and update movie timings from there.

18
00:01:33,270 --> 00:01:36,150
So this method is actually quite inefficient.

19
00:01:36,390 --> 00:01:42,270
And if you're a single developer or if you don't have a lot of money to begin with or you have a very

20
00:01:42,270 --> 00:01:46,110
large startup capital, then this can be a problematic thing.

21
00:01:46,320 --> 00:01:52,140
So the common solution to this problem is that let's say there's one company and that company has a

22
00:01:52,140 --> 00:01:55,860
server which actually has all the movie timings data on there.

23
00:01:55,890 --> 00:02:02,400
So now what this company decides to do is that they decide to make something which is called as an API,

24
00:02:02,430 --> 00:02:05,340
which stands for application program interface.

25
00:02:05,910 --> 00:02:13,500
And now what this particular API allows the company to do is that it allows the company to actually

26
00:02:13,500 --> 00:02:16,350
share that movie data with people like us.

27
00:02:17,070 --> 00:02:24,000
So the way in which this is going to work is that the mobile application which we are building is actually

28
00:02:24,000 --> 00:02:28,230
going to send a request requesting for the data from the API.

29
00:02:28,560 --> 00:02:35,440
And the EPA is now going to act as a communication between the company server and the client.

30
00:02:35,940 --> 00:02:43,080
So once the API gets a request for accessing a certain kind of movie data, what the company so does

31
00:02:43,080 --> 00:02:47,400
is that it communicates with the API and it sends back an appropriate response.

32
00:02:48,180 --> 00:02:54,690
So your API is nothing, but it's an interface which allows communication between two devices.

33
00:02:54,900 --> 00:02:56,790
That's exactly what an API is.

34
00:02:57,120 --> 00:03:01,440
Now let's go ahead and learn what exactly API is in much more depth.

35
00:03:01,980 --> 00:03:05,850
So an API is a code which allows two programs to communicate.

36
00:03:06,240 --> 00:03:12,420
So while building real life software, you actually need to communicate a lot of data between two different

37
00:03:12,420 --> 00:03:14,180
modules or two different softwares.

38
00:03:14,610 --> 00:03:21,360
And one of the most common examples where you might have used an API is when you use some service like

39
00:03:21,360 --> 00:03:23,160
people or scribe.

40
00:03:23,340 --> 00:03:29,250
So what happens is that whenever you are shopping on an ecommerce website and there is an option for

41
00:03:29,250 --> 00:03:35,730
you to make a payment via people, what the developers of that website actually do is that they have

42
00:03:35,730 --> 00:03:39,570
used the people's payment gateway into your into their website.

43
00:03:40,050 --> 00:03:46,410
So they are essentially using the people's API, which allows them to integrate people with their own

44
00:03:46,410 --> 00:03:47,250
e-commerce style.

45
00:03:47,250 --> 00:03:53,520
And the way in which these two applications communicate is that when you make a successful payment,

46
00:03:53,520 --> 00:03:59,130
we are PayPal people actually in forms that e-commerce site that your payment has been successfully

47
00:03:59,130 --> 00:03:59,460
made.

48
00:03:59,460 --> 00:04:02,530
And that's how the e-commerce site actually conforms your order.

49
00:04:03,480 --> 00:04:08,440
So API has nothing, but it's a code which allows two programs to communicate.

50
00:04:09,330 --> 00:04:13,640
Now a developer can write a program which can communicate with an API.

51
00:04:13,830 --> 00:04:19,860
So whenever there's some communication to be made, a developer can simply go ahead, write a program

52
00:04:19,860 --> 00:04:23,260
which can communicate with the API of some other website.

53
00:04:23,280 --> 00:04:29,970
So in the example I have just given, developer of the ecommerce website is essentially writing a program

54
00:04:30,000 --> 00:04:32,770
which communicates with the API of people.

55
00:04:32,790 --> 00:04:34,920
Now we know what an API is.

56
00:04:35,250 --> 00:04:38,220
Now let's learn what exactly is a restful API.

57
00:04:38,850 --> 00:04:47,000
So restful API is an API that uses HTTP request to get put post and delete data.

58
00:04:47,520 --> 00:04:48,900
So we trust APIs.

59
00:04:49,020 --> 00:04:55,350
You cannot just get data, but you can also go ahead and you can edit the data, you can delete the

60
00:04:55,350 --> 00:04:59,230
data, you can modify the data and also post some data.

61
00:04:59,250 --> 00:04:59,640
So.

62
00:05:00,100 --> 00:05:07,470
Arrest is nothing but an FBI which uses the EDP request to perform all those operations of adding,

63
00:05:07,470 --> 00:05:09,450
deleting, editing and posting data.

64
00:05:10,710 --> 00:05:17,430
So now what exactly stands for sorest actually stands for representational state transfer?

65
00:05:17,610 --> 00:05:21,340
So what do we exactly mean by representational state transfer?

66
00:05:21,450 --> 00:05:25,640
So we are actually going to learn what exactly is representational state transfer.

67
00:05:26,100 --> 00:05:28,180
But for now, let's move on to the next point.

68
00:05:28,800 --> 00:05:32,940
So this is actually an architectural style for developing Web services.

69
00:05:33,480 --> 00:05:36,610
So they could be EPWs of multiple kinds.

70
00:05:36,630 --> 00:05:42,630
So, for example, there can be a soup API, which is some other kind of architectural style for developing

71
00:05:42,630 --> 00:05:43,530
Web services.

72
00:05:43,530 --> 00:05:46,350
And rest is the one which we are discussing right now.

73
00:05:46,500 --> 00:05:52,440
Now, the rest is actually preferred over SoPE due to the fact that it actually leverages less bandwidth.

74
00:05:52,440 --> 00:05:58,800
So whenever you are using rest or WhatsApp, you're always saving on bandwidth while building your API.

75
00:05:58,830 --> 00:06:01,810
So that's one of the reasons why reste is actually preferred over.

76
00:06:01,990 --> 00:06:08,670
So now let's go ahead and learn what exactly is representational state transfer.

77
00:06:09,210 --> 00:06:11,920
So this is how actually API works.

78
00:06:11,940 --> 00:06:17,520
So you have a company server here which is actually connected to a database server right over here.

79
00:06:18,030 --> 00:06:24,390
So in real life situations, there could be a single server, which is the server, as well as the database

80
00:06:24,390 --> 00:06:24,890
server.

81
00:06:24,900 --> 00:06:30,120
But in some cases, the database server might be different than the original server.

82
00:06:30,570 --> 00:06:36,960
So what usually happens is that let's say you are on this mobile phone and your request for the movie

83
00:06:36,960 --> 00:06:37,390
data.

84
00:06:37,920 --> 00:06:43,170
So when you request for movie data, that API request actually goes to the API.

85
00:06:43,650 --> 00:06:50,250
And now what the API does is that it validates your request and it's actually going to look for the

86
00:06:50,250 --> 00:06:53,250
requested data into the database right here.

87
00:06:54,060 --> 00:07:01,050
Now, what reste actually does is that it does not take the actual data and it does not pass.

88
00:07:01,050 --> 00:07:03,210
The actual data were here to this phone.

89
00:07:03,570 --> 00:07:10,250
Instead, what it does is that the rest only transfers the representation of that particular resource.

90
00:07:10,650 --> 00:07:15,410
So the data which is actually present over here in the database is called as a resource.

91
00:07:15,780 --> 00:07:22,270
And what it does is that rest just does the job of actually transferring the state of that resource.

92
00:07:22,770 --> 00:07:25,860
So it's going to say what is the current state of that resource?

93
00:07:25,860 --> 00:07:29,490
And it's going to transfer that particular representation over here.

94
00:07:29,520 --> 00:07:35,040
That is the representation of state and not the actual resource or the actual data.

95
00:07:35,580 --> 00:07:37,770
So this is how it actually works.

96
00:07:37,770 --> 00:07:45,540
And this is why we call this API as an API, because it actually transfers the representational state

97
00:07:45,540 --> 00:07:46,770
of the resource.

98
00:07:46,800 --> 00:07:50,820
So now let's go ahead and learn what are the different kinds of requests.

99
00:07:51,450 --> 00:07:57,630
So we have the HDB get, which is actually used to retrieve resource representation, that is to get

100
00:07:57,630 --> 00:07:58,150
data.

101
00:07:58,200 --> 00:08:05,460
So whenever you actually want to get some data from the API, we have HDB get as API request, which

102
00:08:05,460 --> 00:08:06,890
we need to send to the API.

103
00:08:07,110 --> 00:08:11,040
Then we have HDB post, which is used to create new resource.

104
00:08:11,310 --> 00:08:17,160
That is, whenever you want to create new data and you want to post it to the server, we use the HDB

105
00:08:17,160 --> 00:08:18,120
post request.

106
00:08:19,140 --> 00:08:23,100
Then we have HTP put, which is kind of similar to the post.

107
00:08:23,370 --> 00:08:29,760
But the thing is put is actually used to update existing resource and that is nothing, but it means

108
00:08:29,760 --> 00:08:30,600
editing data.

109
00:08:31,360 --> 00:08:36,140
Then we have SCDP delete, which actually allows us to delete the resources on the server.

110
00:08:36,480 --> 00:08:38,890
That is, it allows us to delete data.

111
00:08:39,210 --> 00:08:45,570
So using the rest API, you can actually perform all these basic operations like retrieving the data,

112
00:08:45,570 --> 00:08:48,500
posting data, editing data and deleting data.

113
00:08:48,720 --> 00:08:55,470
So now that we know what an API is, how it exactly works, now let's learn how an API request looks

114
00:08:55,470 --> 00:08:55,800
like.

115
00:08:56,010 --> 00:08:58,760
So this is how a general API request looks like.

116
00:08:58,770 --> 00:09:06,080
So let's say this X, Y, Z is actually a site which is going to provide us the data related to movies.

117
00:09:06,480 --> 00:09:12,420
So whenever you want to access an API, there is usually a special you are able to access that particular

118
00:09:12,420 --> 00:09:12,960
API.

119
00:09:13,290 --> 00:09:20,160
So what the developers of that site do is that they have a special API page which enlist all the request

120
00:09:20,160 --> 00:09:22,670
of the API request, which you can actually make.

121
00:09:22,890 --> 00:09:29,010
So the developers say that whenever you want to access movie data on our website, you simply go to

122
00:09:29,010 --> 00:09:30,120
this particular you are.

123
00:09:30,540 --> 00:09:35,880
So when you go to that particular you are, you're automatically going to get data related to all the

124
00:09:35,880 --> 00:09:36,390
movies.

125
00:09:37,030 --> 00:09:42,690
Now, instead of you specifically want one particular movie, you can actually go to some other you

126
00:09:42,690 --> 00:09:44,730
are, which might look something like this.

127
00:09:44,760 --> 00:09:48,090
Now the users can be actually said to anything.

128
00:09:48,090 --> 00:09:50,180
It's all upon the developers choice.

129
00:09:50,520 --> 00:09:52,230
These are just the examples.

130
00:09:52,230 --> 00:09:57,570
And it does not mean that the API request of all the sites are going to look like this.

131
00:09:57,600 --> 00:09:59,610
So now once we know what an API.

132
00:09:59,860 --> 00:10:06,830
Wester's, now let's learn how exactly the EPA response looks like, so as I said earlier, that whenever

133
00:10:06,830 --> 00:10:12,450
you request for a particular data to the EPA, so this is actually they get requests.

134
00:10:12,830 --> 00:10:18,650
So when you send this particular request, that is whenever you are requesting for a movie data of a

135
00:10:18,650 --> 00:10:22,040
movie named Spider-Man, this is what you actually get.

136
00:10:22,650 --> 00:10:28,310
So you might get the name, you might get the duration, you might get the release date, and you might

137
00:10:28,310 --> 00:10:32,630
also get the rating depending upon what the EPA actually provides.

138
00:10:33,320 --> 00:10:40,880
So one thing to learn here is that whenever you get the EPA response, the response is mostly in the

139
00:10:40,880 --> 00:10:42,410
form of Jason.

140
00:10:42,470 --> 00:10:45,830
So Jason actually stands for JavaScript object notation.

141
00:10:46,220 --> 00:10:53,000
So whenever you make a request, the API, you get the response in a form of the JavaScript object,

142
00:10:53,000 --> 00:10:54,350
which looks something like this.

143
00:10:54,620 --> 00:10:59,760
So we have the opening library's closing libraries and we have a name Value Value-Based over here.

144
00:10:59,780 --> 00:11:06,650
And the reason why the EPA responds back in adjacent format is that for programa, it's much easier

145
00:11:06,650 --> 00:11:07,880
to work with this format.

146
00:11:07,910 --> 00:11:14,360
So, for example, let's say some other developer wants to make a website where he actually wants to

147
00:11:14,780 --> 00:11:17,390
display all the movie names and the movie timings.

148
00:11:17,660 --> 00:11:23,090
He can actually pretty much go ahead and use this JavaScript notation to read the data, which is present

149
00:11:23,090 --> 00:11:23,870
right over here.

150
00:11:23,900 --> 00:11:29,070
So now that we know what an API actually is and how exactly it works.

151
00:11:29,510 --> 00:11:35,310
Now, the main question which arises is how can we actually go ahead and create our own rest API?

152
00:11:35,990 --> 00:11:41,930
So in order to create our own rest API, especially in Shango, we have something which is called the

153
00:11:41,940 --> 00:11:47,600
Django Test Framework, which allows us to create APIs from scratch in order to create an API.

154
00:11:47,600 --> 00:11:53,750
First of all, you need to have some sort of data on to your website, because obviously an API is something

155
00:11:53,780 --> 00:11:56,810
which is used to communicate between two applications.

156
00:11:57,170 --> 00:12:00,970
And when two applications communicate, you need to exchange some data.

157
00:12:01,160 --> 00:12:04,170
So you obviously need to have some data on your website.

158
00:12:05,060 --> 00:12:11,300
So in the upcoming lecture, what we will do is that we will try to build an API, which actually gives

159
00:12:11,300 --> 00:12:20,000
out details of specific movies like the name of the movie, the rating, the duration, so on and so

160
00:12:20,000 --> 00:12:20,210
forth.

161
00:12:20,210 --> 00:12:23,580
And we are going to build that using the Django framework.

162
00:12:23,880 --> 00:12:27,770
So thank you very much for watching and I'll see you guys next time.

163
00:12:28,190 --> 00:12:28,760
Thank you.

