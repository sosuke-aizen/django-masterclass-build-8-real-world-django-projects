1
00:00:00,150 --> 00:00:02,290
So hello and welcome to this lecture.

2
00:00:02,310 --> 00:00:08,090
And in this lecture, we will go ahead and learn how to add an image, the FBI.

3
00:00:08,610 --> 00:00:14,310
So right now, if you actually go ahead and take a look at our API, as you can see, these are the

4
00:00:14,310 --> 00:00:16,910
fields which we currently have in our API.

5
00:00:17,370 --> 00:00:23,700
So let's say, for example, in this particular field, you also want to add another field, which is

6
00:00:23,700 --> 00:00:24,680
an image field.

7
00:00:25,050 --> 00:00:27,620
So how exactly can you go ahead and do that?

8
00:00:28,020 --> 00:00:34,200
So even before you go ahead and add an image file here, you actually need a package which allows you

9
00:00:34,200 --> 00:00:36,220
to work with images in Django.

10
00:00:36,660 --> 00:00:42,450
So in order to go ahead and work with images and Django, you need to install a package which is called

11
00:00:42,450 --> 00:00:43,350
as below.

12
00:00:43,830 --> 00:00:52,620
So in order to install pillow, you simply type in PIP, install by double O.W. That's below hit enter

13
00:00:52,890 --> 00:00:56,230
and below is going to be automatically installed for you.

14
00:00:56,610 --> 00:01:02,220
So if you're using a Mac and if you're using Python three as I am, you just need to go ahead and type

15
00:01:02,220 --> 00:01:04,080
and three install.

16
00:01:05,540 --> 00:01:11,660
Below, and that's going to install Pelo for you now, in my case, as you can see, I already have

17
00:01:11,660 --> 00:01:16,180
it installed and henceforward, I don't need to go back again and install it from scratch.

18
00:01:17,090 --> 00:01:22,330
So once below is installed, we can now go ahead and make further changes to our code here.

19
00:01:23,030 --> 00:01:29,660
So as we want to add a new image, Felin here, the very first thing which you need to go ahead and

20
00:01:29,660 --> 00:01:36,020
do here is you simply need to create a feel, which is image, and you need to create the data for this

21
00:01:36,020 --> 00:01:43,490
field as models start image field so that Janger actually understand that this particular field right

22
00:01:43,490 --> 00:01:47,430
here, which we are actually adding in the model, is actually the image field.

23
00:01:47,990 --> 00:01:52,670
Now to this particular image field as we are trying to store a particular image.

24
00:01:53,730 --> 00:02:00,570
You need to mention the part where exactly we want to place this image and so let's say, for example,

25
00:02:00,570 --> 00:02:07,410
we want to place this image in a specific folder, you simply go ahead and create a parameter here which

26
00:02:07,420 --> 00:02:14,180
says upload underscore to you, which defines where exactly the image is going to be stored.

27
00:02:14,670 --> 00:02:19,810
So we type in upload to equals and then you can specify a path over here.

28
00:02:20,220 --> 00:02:25,920
So let's say we want to store it and do some sort of a directory called images so I can simply type

29
00:02:25,920 --> 00:02:28,630
in images here and give a slash.

30
00:02:29,280 --> 00:02:35,460
Now, the next parameter over here, which we need to define, is we need to define what kind of name

31
00:02:35,580 --> 00:02:40,730
do you want to give this image so we can see default equals?

32
00:02:41,040 --> 00:02:49,600
And as we first want to upload this to the images folder, I will see images slash and let's see, we

33
00:02:49,620 --> 00:02:57,190
type in none slash no img dot GPG as the name of our image.

34
00:02:57,600 --> 00:03:04,170
So this actually defines the part where we want to add the image and this is actually the default image

35
00:03:04,170 --> 00:03:07,620
or the by default value which we set for this specific field.

36
00:03:08,070 --> 00:03:10,900
So what do we exactly mean by the default value?

37
00:03:11,160 --> 00:03:12,960
So the default value is nothing.

38
00:03:12,960 --> 00:03:18,090
But let's say, for example, if you go ahead and create a particular movie and let's say you don't

39
00:03:18,090 --> 00:03:20,350
assign an image to that particular movie.

40
00:03:20,670 --> 00:03:26,910
So in that case, the by default image it's going to pick up is going to be this specific image right

41
00:03:26,910 --> 00:03:27,150
here.

42
00:03:27,780 --> 00:03:31,170
So now once you go ahead and do that, you're pretty much good to go.

43
00:03:32,070 --> 00:03:38,660
So now, as we have made changes to this specific model, we need to go ahead and make migration's.

44
00:03:38,850 --> 00:03:50,880
So I'll see Python three managed by Make Migration's and then Python three managed by migrate.

45
00:03:51,930 --> 00:03:58,620
So as you can see, the migrations have been applied and now we successfully have that particular field

46
00:03:58,620 --> 00:03:59,970
added up to our models.

47
00:04:00,420 --> 00:04:06,900
But as we are actually using an API here, we also need to make sure that we serialize that particular

48
00:04:06,900 --> 00:04:08,310
image file, which we have.

49
00:04:08,730 --> 00:04:12,860
So we serialize anything in this particular serializer start by file.

50
00:04:13,110 --> 00:04:17,740
And in here, what we simply do is that we also serialized that particular image field.

51
00:04:18,180 --> 00:04:25,020
So in order to serialize that, you may type an image equals serializer dot.

52
00:04:25,440 --> 00:04:31,440
And then as we want the image field and type an image field here and here, you just go ahead and set

53
00:04:31,440 --> 00:04:33,330
the maximum length for this field.

54
00:04:33,720 --> 00:04:36,420
So I'll type in max length equals.

55
00:04:36,630 --> 00:04:43,140
And as this is an image field, it should not have any kind of maximum length, hence will set the value

56
00:04:43,140 --> 00:04:44,760
of maximum length to none.

57
00:04:45,390 --> 00:04:50,850
And then we need to define another parameter over here, which is use underscore.

58
00:04:50,850 --> 00:04:56,070
You are meaning it will ask us if this particular field uses a particular.

59
00:04:56,070 --> 00:04:56,460
You are.

60
00:04:56,910 --> 00:04:59,730
And in this case, yeah, it actually does.

61
00:05:00,060 --> 00:05:05,970
Whatever image which we are using in our app is going to be stored at a specific path or a specific.

62
00:05:05,970 --> 00:05:09,090
You are henceforth will mention Drew over here.

63
00:05:10,140 --> 00:05:15,960
Now once we actually serialize the image field, you also need to make sure that you add that field

64
00:05:15,960 --> 00:05:20,500
up over here in the fields so I can simply type an image here.

65
00:05:21,060 --> 00:05:25,120
So once we go ahead and do that, we are pretty much good to go.

66
00:05:25,680 --> 00:05:32,820
So after actually adding the image field and after serializing that image field, what's left is that

67
00:05:32,820 --> 00:05:36,730
you need to define a path where the image has to be uploaded.

68
00:05:37,290 --> 00:05:42,760
So how can you actually go ahead and create a path where image needs to be uploaded?

69
00:05:42,810 --> 00:05:48,510
So first of all, you need to go ahead and create some sort of a folder or some sort of a directory

70
00:05:48,810 --> 00:05:50,700
where you can store these images.

71
00:05:51,210 --> 00:05:53,660
So where exactly can we store these images?

72
00:05:53,880 --> 00:06:00,540
So in order to do that, you need to go to the base directory, which is in this case, my site, and

73
00:06:00,540 --> 00:06:05,400
simply create a new director for you over here and call it as something like, let's say, media.

74
00:06:06,180 --> 00:06:12,530
Now, let's say you wish to save the images in this specific directorate.

75
00:06:13,470 --> 00:06:17,040
So how can you actually go ahead and add images here?

76
00:06:17,100 --> 00:06:19,050
So adding images here is quite simple.

77
00:06:19,060 --> 00:06:23,700
You can just go ahead and drop any image in the Zekry and you're good to go.

78
00:06:24,240 --> 00:06:31,080
But now you need to tell Django that it needs to save all of these images up over here in this media

79
00:06:31,080 --> 00:06:31,560
folder.

80
00:06:32,070 --> 00:06:36,590
So how can we go ahead and let Django know that it has to use this folder?

81
00:06:36,870 --> 00:06:42,870
So in order to do that, you simply go to the settings dot by file, and here you define something which

82
00:06:42,870 --> 00:06:44,690
is called as a media route.

83
00:06:45,060 --> 00:06:47,270
Now, what exactly is a media route?

84
00:06:47,280 --> 00:06:52,890
A media route is nothing, but it's actually a path where Django is going to look.

85
00:06:52,960 --> 00:07:00,280
For all of the media, for your specific site, so in order to define a media route, you type in media

86
00:07:00,430 --> 00:07:08,920
and the school route, then type equals and then you need to specify the actual part of this particular

87
00:07:08,920 --> 00:07:10,010
media directory.

88
00:07:10,390 --> 00:07:16,960
Now, getting the actual part of this directly is quite difficult because this actually lies on your

89
00:07:16,960 --> 00:07:23,430
operating system and now you have to get the part of this directory from your operating system.

90
00:07:23,860 --> 00:07:30,280
So in order to get the parts from your operating system, you type in OS, which is the operating system

91
00:07:30,280 --> 00:07:38,500
module in Python, then you can type in Oyston path, which actually gets the part of your operating

92
00:07:38,500 --> 00:07:39,070
system.

93
00:07:39,310 --> 00:07:43,550
And then you need to join this path with the folder, which is media.

94
00:07:43,750 --> 00:07:46,590
So in order to join that, I can say join.

95
00:07:47,020 --> 00:07:50,500
And first you get the base directory or the base path.

96
00:07:50,710 --> 00:07:56,920
So you see base underscore desire, which gives you the best path.

97
00:07:57,250 --> 00:08:01,900
And we need to join that with the media, which is nothing but this directory right here.

98
00:08:02,410 --> 00:08:08,380
So what this line of code essentially does is that it actually gets the best practice SPAD from your

99
00:08:08,380 --> 00:08:13,960
operating system and it basically joins that up with the media directly, which is the strategy right

100
00:08:13,960 --> 00:08:14,180
here.

101
00:08:14,860 --> 00:08:20,080
So once you go ahead and do that, you also need to define another parameter, which is called mass

102
00:08:20,440 --> 00:08:21,310
media.

103
00:08:21,580 --> 00:08:24,670
Underscore you are and this is actually nothing.

104
00:08:24,670 --> 00:08:29,500
But it's simply the path or simply your directory, which is nothing but media.

105
00:08:29,800 --> 00:08:32,820
So make sure that you enclose type thing and slashes.

106
00:08:33,370 --> 00:08:37,600
So once you go ahead and do that, you're pretty much good to go.

107
00:08:37,630 --> 00:08:40,059
You have defined this path over here as well.

108
00:08:40,539 --> 00:08:47,290
However, there's one additional thing which you need to do, and that thing is to basically go ahead

109
00:08:47,590 --> 00:08:53,290
and add this particular static path up over here and you'll start by file.

110
00:08:53,650 --> 00:08:55,750
So right now in the you are Spartan.

111
00:08:55,750 --> 00:09:01,120
As you can see, we have these pazzo here and these parts are actually called as dynamic parts.

112
00:09:01,480 --> 00:09:07,510
But the path for this particular media folder is a static one, and that is it's not going to change

113
00:09:07,510 --> 00:09:07,890
at all.

114
00:09:08,200 --> 00:09:11,950
However, you actually need to define that path up over here.

115
00:09:12,310 --> 00:09:13,500
So how can you do that?

116
00:09:13,510 --> 00:09:15,360
So doing that is quite simple.

117
00:09:15,370 --> 00:09:18,650
You first actually need to go ahead and import these static path.

118
00:09:18,850 --> 00:09:25,210
So for the same, you actually go ahead and type in from Django dot config.

119
00:09:26,840 --> 00:09:29,900
Don't you are not static?

120
00:09:31,450 --> 00:09:42,580
Import static and then you can actually go ahead and type in from Django dot com import settings, so

121
00:09:42,580 --> 00:09:48,400
once you go ahead and do that, you can now simply go ahead and add that static part by appending to

122
00:09:48,400 --> 00:09:55,150
this you are Partons by typing in static and then in order to get the actual pad and type and settings,

123
00:09:56,020 --> 00:10:00,040
which is nothing but the settings file right here and we want to get the media.

124
00:10:00,040 --> 00:10:02,830
You are all from here so you can type in.

125
00:10:04,290 --> 00:10:12,480
Media underscore you are all that should actually be a dot, and then you need to specify the document

126
00:10:12,480 --> 00:10:13,370
route as well.

127
00:10:13,380 --> 00:10:21,030
So you type in document and the school route equals and then you need to specify the media and the school

128
00:10:21,030 --> 00:10:21,570
route.

129
00:10:23,670 --> 00:10:29,460
Which you have actually defined up over here, and as this is also from settings, you actually need

130
00:10:29,460 --> 00:10:34,370
to type in settings, dot media route, just as we have done with the media.

131
00:10:34,380 --> 00:10:35,300
You are right here.

132
00:10:35,850 --> 00:10:39,420
So once we have defined these two things, we are pretty much done.

133
00:10:39,600 --> 00:10:44,350
So now let's go ahead, run our app and see if this thing works so well.

134
00:10:44,370 --> 00:10:45,090
Go ahead, Randi.

135
00:10:45,120 --> 00:10:51,240
So now, if we actually go to movies here and if you scroll down a little bit, as you can see, you

136
00:10:51,240 --> 00:10:54,040
now have the image feel added up over here as well.

137
00:10:54,060 --> 00:10:58,700
And now, if I wanted to add some movie here, I can simply go ahead type in some name here.

138
00:11:02,010 --> 00:11:10,290
Type in the duration type and the rating type and the type, and I can choose any image which I want.

139
00:11:13,040 --> 00:11:15,720
So choose a particular image from the director.

140
00:11:16,280 --> 00:11:21,060
And when I click post, as you can see, that movie is going to be added up over here.

141
00:11:21,440 --> 00:11:24,470
And as you can see, it has this image feel as well.

142
00:11:24,500 --> 00:11:29,640
And when I click this particular image, feel, as you can see, I actually have that image up.

143
00:11:30,380 --> 00:11:32,050
So that's it for this lecture.

144
00:11:32,060 --> 00:11:36,890
And I hope you guys be able to understand how to add image peels and Shango.

145
00:11:37,280 --> 00:11:45,090
So that marks the end of this specific section of making Epis using the Django framework.

146
00:11:45,440 --> 00:11:49,580
So thank you very much for watching and I'll see you guys in the next section.

147
00:11:49,850 --> 00:11:50,450
Thank you.

