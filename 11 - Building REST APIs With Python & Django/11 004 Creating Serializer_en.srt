1
00:00:00,150 --> 00:00:05,610
Hello and welcome to this lecture, and in this lecture, we will learn about one of the most important

2
00:00:05,610 --> 00:00:12,350
concepts and January's framework, and that is nothing but serialisation of models.

3
00:00:12,780 --> 00:00:19,260
So even before we learn what exactly serialisation is, you need to understand how data is currently

4
00:00:19,260 --> 00:00:21,270
stored and Django application.

5
00:00:21,520 --> 00:00:27,090
So as we all know, we have added the sample movie data over here, which has the name of the movie,

6
00:00:27,480 --> 00:00:30,920
the duration of the movie and the rating of the movie right over here.

7
00:00:31,320 --> 00:00:36,560
And we can actually go ahead and add the data related to other movies as well.

8
00:00:36,570 --> 00:00:39,330
So we already knew that there is nothing new in that.

9
00:00:39,690 --> 00:00:45,420
But the only thing which you need to learn is in which particular format this data is actually stored

10
00:00:45,420 --> 00:00:45,690
here.

11
00:00:45,990 --> 00:00:50,460
So in our case, the data is stored in a user model.

12
00:00:50,460 --> 00:00:57,270
It's actually stored in a database table, which is nothing but the movie data table, which has been

13
00:00:57,270 --> 00:00:59,380
created from the movie data model.

14
00:01:00,030 --> 00:01:07,770
So now that data is actually stored in the model, we need to be able to access this data for our API

15
00:01:07,770 --> 00:01:08,460
purposes.

16
00:01:08,670 --> 00:01:15,810
So we actually need a way to take that data from a model and we need to put it out to the users via

17
00:01:15,810 --> 00:01:16,450
an API.

18
00:01:17,460 --> 00:01:25,080
So what's idealizes allow us to do or what serialisation allows us to do is that it allows complex data

19
00:01:25,560 --> 00:01:34,080
such as query sets, model instances to be converted to native python data types that can then be easily

20
00:01:34,080 --> 00:01:38,550
rendered into formats like JSON or XML or other content types.

21
00:01:39,030 --> 00:01:41,680
So we essentially want to do the same procedure here.

22
00:01:42,390 --> 00:01:48,480
So currently this my movie over here is nothing, but it's actually a model instance and we want to

23
00:01:48,480 --> 00:01:56,310
convert that model instance into some format like Jason or XML, because that's what an API actually

24
00:01:56,310 --> 00:01:56,600
does.

25
00:01:56,610 --> 00:02:03,720
That means that if we want to generate Egilsson response for our API, we need to take this data which

26
00:02:03,720 --> 00:02:11,020
we currently have in our Django models, and we need to convert that into JSON using serializer or serialisation.

27
00:02:11,460 --> 00:02:14,430
So how exactly can we perform serialisation?

28
00:02:15,330 --> 00:02:21,510
So in order to create this model here, we actually need to go into our app, which is movies, and

29
00:02:21,510 --> 00:02:25,230
we need to create a file called Serializer Stoppie.

30
00:02:25,530 --> 00:02:33,450
So let's go ahead, create a new file, let's call it as serializer dot by and in here will go ahead

31
00:02:33,450 --> 00:02:35,010
and create a laser.

32
00:02:35,940 --> 00:02:41,910
So in order to create a serializer, first of all, you obviously need to import idealizes from this

33
00:02:41,940 --> 00:02:42,570
framework.

34
00:02:42,570 --> 00:02:49,380
So we already have a framework installed so we can directly import serializer here by typing and from

35
00:02:49,620 --> 00:02:54,650
reste underscore framework import Cydia Lazar's.

36
00:02:55,380 --> 00:03:02,190
And now once we have this idealizes imported, we need our model as well because we are going to serialized

37
00:03:02,190 --> 00:03:02,870
that model.

38
00:03:03,330 --> 00:03:10,110
So we'll type in from DOT models and we want to import the movie data model.

39
00:03:10,870 --> 00:03:15,550
And now once we have imported them, let's go ahead and learn how to create a serializer.

40
00:03:16,050 --> 00:03:21,630
So in order to create a serializer for any model, what you essentially do is that you create a class.

41
00:03:22,110 --> 00:03:29,400
So we type in class and let's say we name this thing as the movie Serializer because it's going to serialize

42
00:03:29,400 --> 00:03:30,840
the movie data model.

43
00:03:31,350 --> 00:03:32,790
So I'll type and movie.

44
00:03:34,240 --> 00:03:44,170
Serializer, this class is actually going to inherit from serializer, not model serializer, so model

45
00:03:44,170 --> 00:03:50,560
serializer is one type of serializer, which we are using right now, and a model serializer is nothing.

46
00:03:50,560 --> 00:03:55,160
But as the name suggests, it's something which utilizes your model.

47
00:03:55,390 --> 00:03:56,600
So nothing new in that.

48
00:03:57,460 --> 00:04:03,640
Now, once we have this, we are going to have a middle class in here, which is actually going to contain

49
00:04:03,640 --> 00:04:08,660
the name of the models and the fields, soil type and class matter.

50
00:04:09,310 --> 00:04:14,330
And in here, you actually need to pass in the name of the model, which you want to serialize.

51
00:04:14,710 --> 00:04:18,300
So here we want to serialize the movie data model.

52
00:04:18,310 --> 00:04:23,440
Hence we will go ahead and type in model equals.

53
00:04:23,950 --> 00:04:27,040
That's going to be movie data.

54
00:04:28,480 --> 00:04:34,750
And now, once we have the model, we need to now mention, which fields do we want to mention in the

55
00:04:34,750 --> 00:04:35,880
EPA response?

56
00:04:36,400 --> 00:04:39,430
So currently we have these fields here.

57
00:04:39,460 --> 00:04:43,530
And let's assume that you want to display all of them in the EPA response.

58
00:04:43,540 --> 00:04:46,450
You can simply go ahead and type in fields here.

59
00:04:46,900 --> 00:04:52,210
And let's go ahead and add these fields one by one so we have it.

60
00:04:52,780 --> 00:04:58,300
Then we have name and it is actually by default field, which is not present here.

61
00:04:58,300 --> 00:05:03,690
But each item which you add in here and movie data is going to have a unique ID.

62
00:05:04,060 --> 00:05:05,910
That's why we have it over here.

63
00:05:06,790 --> 00:05:10,030
And then it can have duration.

64
00:05:11,870 --> 00:05:19,010
And reading, so once we have added all those wheels, we are pretty much done creating the serializer.

65
00:05:19,190 --> 00:05:24,890
So now what the serializer does is that, first of all, it's actually going to take a look at which

66
00:05:24,890 --> 00:05:26,160
model it's working with.

67
00:05:26,600 --> 00:05:30,910
So it says that the model which we are working with is a movie data model.

68
00:05:31,460 --> 00:05:37,040
Then what the serializer does is that it looks at the field, which it needs to basically go ahead and

69
00:05:37,040 --> 00:05:37,720
serialize.

70
00:05:38,120 --> 00:05:43,040
So it's going to go into this model, look for these fields right here, and it's going to generate

71
00:05:43,040 --> 00:05:47,030
adjacent response out of these fields and it's going to give it back to us.

72
00:05:47,750 --> 00:05:54,770
Now, this serializer does a job of just utilizing the models, but what we need to do is that once

73
00:05:54,890 --> 00:06:00,680
these models are serialized, we need to take that response and we need to create a view out of this

74
00:06:00,680 --> 00:06:06,670
response, because how else are we going to display the serialized response to the end user?

75
00:06:07,520 --> 00:06:13,730
And our next job is to actually go ahead and create views for this particular serializer.

76
00:06:14,390 --> 00:06:20,930
So just as with any Jianguo application, like we create a model, then we create a view, then we associate

77
00:06:20,930 --> 00:06:22,280
that you are all with that view.

78
00:06:22,640 --> 00:06:24,500
The same thing works here as well.

79
00:06:24,720 --> 00:06:27,020
That is, first of all, we have created the model.

80
00:06:27,320 --> 00:06:33,380
And now instead of directly jumping to creating views and creating new models, what we do is that after

81
00:06:33,380 --> 00:06:35,660
creating models, we create serializer.

82
00:06:35,900 --> 00:06:42,860
Then after creating the serializer, we make use of the serializer to create a view which actually displays

83
00:06:43,160 --> 00:06:46,060
the data which is generated by the serializer.

84
00:06:46,400 --> 00:06:50,600
And then we go ahead and associate you are with that particular view.

85
00:06:51,320 --> 00:06:59,210
So I hope this flow actually makes sense to you and you understand how exactly API creation process

86
00:06:59,210 --> 00:06:59,520
work.

87
00:06:59,540 --> 00:07:04,590
So in the next lecture we will go ahead and we will build a view for this particular serializer.

88
00:07:05,150 --> 00:07:09,110
So thank you very much for watching and I'll see you guys next time.

89
00:07:09,380 --> 00:07:09,920
Thank you.

