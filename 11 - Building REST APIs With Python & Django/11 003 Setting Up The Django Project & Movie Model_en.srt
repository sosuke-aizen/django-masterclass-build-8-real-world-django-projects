1
00:00:00,180 --> 00:00:01,890
Hello and welcome to this lecture.

2
00:00:02,130 --> 00:00:08,390
So in this lecture, we will actually go ahead and set up as framework for our project.

3
00:00:08,760 --> 00:00:14,670
So essentially we will go ahead and download svengalis framework and we will set up a Shango project

4
00:00:14,880 --> 00:00:21,450
in which we are actually going to build a model and we are going to use that model to build our API.

5
00:00:21,930 --> 00:00:26,670
So the very first thing which you need to do is that you need to open up the command prompt of your

6
00:00:26,670 --> 00:00:27,530
own windows.

7
00:00:27,540 --> 00:00:31,040
And if you're on Mac, you actually need to open up the terminal.

8
00:00:31,350 --> 00:00:37,980
And here in order to install Januaries framework, you simply need to type in a command, which is PIP,

9
00:00:37,980 --> 00:00:40,330
install, Django, reste framework.

10
00:00:40,860 --> 00:00:47,400
Now, in my case, I'm actually having Python two as well as Python three installed on my machine and

11
00:00:47,400 --> 00:00:51,000
hence I am going to type in three install.

12
00:00:51,270 --> 00:00:55,750
That's going to be Django Reste framework.

13
00:00:55,920 --> 00:01:02,220
So now once I go ahead and hit enter Django, this framework is going to be installed for me now as

14
00:01:02,220 --> 00:01:03,090
I already have.

15
00:01:03,090 --> 00:01:06,900
Django, this framework, it's a requirement already satisfied.

16
00:01:07,050 --> 00:01:11,230
Now in your case, you just need to type in PIP, install Django as framework.

17
00:01:11,820 --> 00:01:16,590
Now, once we have the Django Rights Framework, let's go ahead and create a Django project.

18
00:01:17,160 --> 00:01:23,310
So in order to create a Django project, first of all, I'll create a folder here and let's name this

19
00:01:23,310 --> 00:01:27,260
folder as Dezhurov, which is Django, this framework.

20
00:01:27,570 --> 00:01:34,320
And now we can open up some code editor like Visual Studio Code and start actually making a project

21
00:01:34,440 --> 00:01:34,810
way here.

22
00:01:34,830 --> 00:01:40,800
So let's open up a visual studio code and let's open up the TRF folder right over here.

23
00:01:40,830 --> 00:01:46,990
And now what we will do is that we will open up the terminal back again and let's navigate down to the

24
00:01:47,010 --> 00:01:48,070
data folder.

25
00:01:48,090 --> 00:01:52,550
So that's going to be the desktop slash the IRS.

26
00:01:53,370 --> 00:01:57,630
And now once we are here, let's go ahead and create a Django project.

27
00:01:57,870 --> 00:02:06,390
So in order to create a project, I'll type in Django Dash Admin Start project.

28
00:02:06,390 --> 00:02:09,300
And let's say we named this project as my site.

29
00:02:09,810 --> 00:02:15,700
So here what we are doing essentially is that we are going to create a regular Django project.

30
00:02:15,720 --> 00:02:19,640
We are going to have a Django app into that and into that app.

31
00:02:19,650 --> 00:02:25,410
We are actually going to have a model as we are building an API, which provides information related

32
00:02:25,410 --> 00:02:26,070
to movies.

33
00:02:26,460 --> 00:02:32,490
So that model is going to have information about movies like the name duration rating, so on and so

34
00:02:32,490 --> 00:02:32,850
forth.

35
00:02:33,160 --> 00:02:39,540
Then we are going to add some dummy data into that particular database and then we are going to build

36
00:02:39,540 --> 00:02:42,020
an API using the Django framework.

37
00:02:42,690 --> 00:02:51,620
So once we have a project that CD into the project, so I'll type in a CD that's going to be my site

38
00:02:51,870 --> 00:02:53,890
and now let's create an appeal.

39
00:02:54,390 --> 00:03:03,020
So that's going to be Django admin start up and let's say we named this app as movies.

40
00:03:03,870 --> 00:03:07,930
So once we are done with that, we now have a project and an app.

41
00:03:07,950 --> 00:03:15,090
So if you go to the data folder here, you have my site, we have the movies app and we have the myside

42
00:03:15,090 --> 00:03:15,990
directly as well.

43
00:03:16,800 --> 00:03:22,830
So now once you have this project set up, whenever you want to use Django, this framework in your

44
00:03:22,830 --> 00:03:28,320
Django project, you first need to go ahead, go to the settings that profile.

45
00:03:28,650 --> 00:03:35,090
And in here you actually need to go ahead and add reste underscore framework in the install apps.

46
00:03:35,520 --> 00:03:40,070
So let's go ahead and type in address and the school framework.

47
00:03:40,650 --> 00:03:48,570
So that means, uh, Django Project now knows that it's using race framework and also as we have created

48
00:03:48,570 --> 00:03:51,110
the app, which is movies.

49
00:03:51,540 --> 00:03:56,890
So you need to type in movies, not apps, DOT.

50
00:03:57,540 --> 00:04:04,140
And if you go to the App Store by we have movies config over here, so you need to type in movies config

51
00:04:04,140 --> 00:04:04,960
right over here.

52
00:04:05,460 --> 00:04:09,030
So once you do that, you're pretty much done setting up your project.

53
00:04:09,210 --> 00:04:12,030
So now once we have the project, let's now go ahead.

54
00:04:12,030 --> 00:04:16,329
And without wasting any time, let's directly build a model here.

55
00:04:16,740 --> 00:04:22,110
So let's go ahead and create a model which is going to hold data of a movie.

56
00:04:22,470 --> 00:04:30,540
So we'll type in class and let's name this model as movie data as it's going to hold movie data and

57
00:04:30,540 --> 00:04:34,090
then it's going to inherit from model store model.

58
00:04:34,950 --> 00:04:37,740
And now let's go ahead and add in a few fields here.

59
00:04:38,160 --> 00:04:41,120
So the first field is going to be the name of the movie.

60
00:04:41,130 --> 00:04:45,140
So I'll type in a name and that's going to be a character field.

61
00:04:45,300 --> 00:04:49,380
So I'll type and models dot coffield.

62
00:04:50,070 --> 00:04:54,180
Now let's set the max length and let's set it up to two hundred.

63
00:04:55,110 --> 00:04:59,880
And now let's go ahead and add another field for a movie later you can have.

64
00:04:59,980 --> 00:05:05,860
The rating of the movie, the duration of the movie, so on and so forth, so let's actually go ahead

65
00:05:05,860 --> 00:05:07,390
and set up the duration.

66
00:05:07,600 --> 00:05:15,070
So I'll type and duration and the duration of a movie can be something like one point five hours, one

67
00:05:15,070 --> 00:05:17,580
point two hours, so on and so forth.

68
00:05:18,040 --> 00:05:22,330
So we'll actually go ahead and use a flawed bill here for this film.

69
00:05:22,510 --> 00:05:29,830
So I'll type in model DOT flawed field, which allows us to add floating point numbers for this particular

70
00:05:29,830 --> 00:05:30,190
field.

71
00:05:30,760 --> 00:05:33,420
And let's do the same thing for the rating as well.

72
00:05:33,430 --> 00:05:38,570
So I'll type and rating equals model dot float field.

73
00:05:38,800 --> 00:05:40,630
So that should actually be float.

74
00:05:41,650 --> 00:05:43,420
So that was actually a typo.

75
00:05:44,470 --> 00:05:50,730
OK, so once we are done with that, we are pretty much done setting up the model, which is movie data,

76
00:05:50,740 --> 00:05:53,530
and this should actually be models, not model.

77
00:05:54,490 --> 00:06:00,160
OK, so now once we are done with this, we are pretty much done setting up the movie data model.

78
00:06:00,580 --> 00:06:05,780
Now let's go ahead and make migration's so that Django creates a table out of this model.

79
00:06:06,190 --> 00:06:11,770
So let's switch back to the terminal here and now in order to make migration's let's type in Python

80
00:06:11,770 --> 00:06:24,160
three managed IPY Make Migration's hit enter then let's type in Python three managed IPY will migrate.

81
00:06:25,250 --> 00:06:28,760
That's going to be movies zero zero zero one.

82
00:06:30,290 --> 00:06:32,960
And now let's finally type in Python three.

83
00:06:34,280 --> 00:06:37,400
Managed by Margaret.

84
00:06:38,900 --> 00:06:44,500
And once we are done with that, our model is now successfully converted into a database table.

85
00:06:45,380 --> 00:06:51,300
So now once we have that database table, we can now go ahead and add data to this particular model.

86
00:06:51,770 --> 00:06:58,130
But in order to add that, we need to go ahead and create a form, then you need to accept the information,

87
00:06:58,130 --> 00:07:00,440
you need to submit the form, so on and so forth.

88
00:07:00,860 --> 00:07:06,530
And hence, in order to avoid that, what we will do for now is that we will actually go ahead and instead

89
00:07:06,530 --> 00:07:11,340
of creating a form will actually go ahead and we will create a super user.

90
00:07:11,840 --> 00:07:14,160
So let's go ahead and create a super user.

91
00:07:14,360 --> 00:07:15,470
So let's step in.

92
00:07:16,250 --> 00:07:22,730
Python three managed to API create super user.

93
00:07:23,770 --> 00:07:31,270
I will actually leave my name, do my name itself, I'll add a dummy email address over here.

94
00:07:33,610 --> 00:07:39,430
The password is going to be super user, super user.

95
00:07:40,240 --> 00:07:46,180
OK, so now once the super user is created, let's go ahead and fire up the server and add some data

96
00:07:46,180 --> 00:07:47,530
via the admin panel.

97
00:07:48,010 --> 00:07:54,220
So let's type in Python three, manage stop by run server.

98
00:07:55,150 --> 00:07:58,870
And once the server is up and running, let's copy the you all.

99
00:07:59,680 --> 00:08:04,270
Let me open up a browser window and let's go to this address.

100
00:08:05,620 --> 00:08:07,090
Now let's go to admin.

101
00:08:07,420 --> 00:08:09,010
So slash admin.

102
00:08:10,030 --> 00:08:12,970
Let me type my username here.

103
00:08:17,070 --> 00:08:22,740
And now, as you can see, we don't have that model here, so in order to add that model, you need

104
00:08:22,740 --> 00:08:25,860
to go to admin or buy and put the model.

105
00:08:25,920 --> 00:08:32,450
So it will depend from not models that's going to be import movie data.

106
00:08:32,909 --> 00:08:41,610
And then you need to register this model by typing in admin, dot site, dot register and let's type

107
00:08:41,610 --> 00:08:43,110
in movie data over here.

108
00:08:43,440 --> 00:08:46,980
So once we do that now, we should be able to see that model over here.

109
00:08:47,190 --> 00:08:53,340
Now if I click that, as you can see currently, we don't have any data here, so I'll go ahead and

110
00:08:53,340 --> 00:08:54,400
add movie data.

111
00:08:54,960 --> 00:08:59,580
So let's say the movie name is going to be something like, uh, my.

112
00:09:03,040 --> 00:09:09,040
Movie, let's say the duration is two hours and let's see, the reading is four point seven.

113
00:09:09,610 --> 00:09:15,130
So when I go ahead and click on S�vres, as you can see, one movie is actually added over here.

114
00:09:15,130 --> 00:09:18,640
But the representation here sees movie data object.

115
00:09:18,940 --> 00:09:24,310
And that's because we actually need to change the string representation right over here in the models.

116
00:09:24,790 --> 00:09:31,210
So if you go here and if you type in def double underscore as the.

117
00:09:32,640 --> 00:09:39,300
Boston itself as a parameter and as a string representation, let's say we want to display the movie's

118
00:09:39,300 --> 00:09:44,290
name so you can type your return, self-taught name.

119
00:09:44,970 --> 00:09:50,640
So now instead of having this movie data object now we will have a string representation, which is

120
00:09:50,640 --> 00:09:52,320
nothing but the name of the movie.

121
00:09:53,040 --> 00:09:57,480
So when I go ahead and hit refresh, as you can see, it sees my movie over here.

122
00:09:57,510 --> 00:10:00,870
So that means we have successfully created a movie data model.

123
00:10:01,200 --> 00:10:03,670
We have added a movie over here as well.

124
00:10:03,690 --> 00:10:06,250
That means we have some kind of data to work with.

125
00:10:06,810 --> 00:10:13,920
Now we can actually go ahead and start creating an API to actually display this data or share this data

126
00:10:13,920 --> 00:10:15,190
via an API.

127
00:10:15,630 --> 00:10:21,690
So in the upcoming lecture, what we will do is that we will learn what exactly is serialization and

128
00:10:21,690 --> 00:10:28,480
how we are going to serialize this particular model so that we can display its data via an API.

129
00:10:28,800 --> 00:10:32,730
So thank you very much for watching and I'll see you guys next time.

130
00:10:33,210 --> 00:10:33,810
Thank you.

