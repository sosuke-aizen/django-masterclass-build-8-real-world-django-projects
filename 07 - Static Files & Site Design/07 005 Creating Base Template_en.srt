1
00:00:00,640 --> 00:00:07,180
Now, as I earlier mentioned, there is one major issue with this particular navigation bar, and that

2
00:00:07,180 --> 00:00:12,970
issue is nothing, but this navigation bar actually appears on the index, not HTML page.

3
00:00:12,970 --> 00:00:18,820
But now if you click on one of these items right over here, as you can see, neither the background

4
00:00:18,820 --> 00:00:21,480
color nor the navigation bar appears over here.

5
00:00:21,940 --> 00:00:28,690
But you already might know that a navigation bar has to be actually present on each one of the Web pages

6
00:00:28,690 --> 00:00:30,160
of a particular website.

7
00:00:30,520 --> 00:00:36,190
So that means even if you click on one of these items, these pages should also have that navigation

8
00:00:36,190 --> 00:00:37,240
bar up over here.

9
00:00:38,050 --> 00:00:43,390
So how exactly can we go ahead and add this navigation bar on the detail page as well?

10
00:00:44,080 --> 00:00:49,900
So one thing which you can do is that you can simply go ahead, copy this navigation bar from here,

11
00:00:50,200 --> 00:00:55,260
go into the detail, not HTML page and paste that navigation bar up over here.

12
00:00:55,480 --> 00:01:00,820
But that simply means that you are repeating certain points of code, multiple number of times, and

13
00:01:00,820 --> 00:01:02,860
that's actually not a good practice.

14
00:01:02,860 --> 00:01:08,560
And it actually goes against the common software engineering practice, which is to avoid repetition

15
00:01:08,560 --> 00:01:09,070
of code.

16
00:01:09,640 --> 00:01:15,100
And the reason why you should avoid repetition of code is because let's say if you copy this navigation

17
00:01:15,100 --> 00:01:16,310
bar from here to here.

18
00:01:16,920 --> 00:01:19,540
So right now you only have two templates.

19
00:01:19,660 --> 00:01:23,410
But what happens if your project actually has 100 templates?

20
00:01:23,860 --> 00:01:28,930
So you might actually go ahead and copy this navigation by and one hundred templates or whether there

21
00:01:28,930 --> 00:01:30,290
should be no problem at all.

22
00:01:30,610 --> 00:01:37,870
But let's say later, if your boss wants to change the layout or the design or the items on the navigation

23
00:01:37,870 --> 00:01:44,740
bar, then in order to make that simple change, you have to manually go through 100 such files and

24
00:01:44,740 --> 00:01:49,460
keep on changing the navigation bar code in each one of those 100 files.

25
00:01:49,900 --> 00:01:55,840
So in order to avoid that issue, what we do is that we use something which is called as a base template.

26
00:01:56,110 --> 00:02:02,410
So what we do in here is that we create another template in here and we will call that template as a

27
00:02:02,410 --> 00:02:10,360
base load HTML and that based on HTML is actually going to contain all the code, which is going to

28
00:02:10,360 --> 00:02:13,210
be applied to all these templates right here.

29
00:02:13,780 --> 00:02:19,720
So let's go ahead and learn how to create a base template and how to add that base template to these

30
00:02:19,720 --> 00:02:21,700
already present templates right over here.

31
00:02:21,760 --> 00:02:27,020
So in order to create a base template, simply go into the template strategy in the food, directly,

32
00:02:27,150 --> 00:02:30,780
create a new file, call it task based on each Simmel.

33
00:02:31,390 --> 00:02:37,540
And in here, what you have to do is that you have to go ahead and first of all, create some HDMI line

34
00:02:37,540 --> 00:02:37,810
here.

35
00:02:37,810 --> 00:02:42,100
So I'll just go ahead and import some basic HTML tags in here.

36
00:02:42,190 --> 00:02:47,440
And in order to avoid confusion, I'll just go ahead, get rid of the other unnecessary tags which we

37
00:02:47,440 --> 00:02:48,190
have over here.

38
00:02:48,910 --> 00:02:56,110
So once we go ahead and do that, the first thing which we do is that we go ahead and get the.

39
00:02:57,490 --> 00:02:58,470
Head of the index.

40
00:02:59,680 --> 00:03:05,740
So right now, as you can see, the head of this HMO actually contains a link to the stylesheet as well

41
00:03:05,740 --> 00:03:06,640
as bootstrap.

42
00:03:06,640 --> 00:03:12,910
So I'll simply go ahead and cut that from here, save the code, and I'll go back in the base template

43
00:03:12,910 --> 00:03:15,280
and I'll paste these links over here.

44
00:03:15,340 --> 00:03:18,890
So now once we go ahead and do that, we are almost done.

45
00:03:19,030 --> 00:03:24,840
Now, what we want to do is that we also want to cut the navigation bar from here.

46
00:03:25,210 --> 00:03:28,000
So we go ahead at this entire navigation bar.

47
00:03:28,000 --> 00:03:35,650
From here, we come back to the base template and we piece that navigation bar over here in the body

48
00:03:35,650 --> 00:03:36,970
tag of the base template.

49
00:03:38,030 --> 00:03:43,600
So now, once we have transferred the common content from the index, that e-mail file to the base dot

50
00:03:43,610 --> 00:03:50,570
H.M., now we need to go ahead and learn how to actually import this content over here in the next on

51
00:03:50,570 --> 00:03:51,190
HMO.

52
00:03:52,570 --> 00:03:59,020
Samantha, now not H.M. in the body tag with the Naftogaz, actually, and we are actually going to

53
00:03:59,020 --> 00:04:04,360
go ahead and add to tags and that is going to be nothing but a blog body tag.

54
00:04:04,540 --> 00:04:05,470
So I'll type in.

55
00:04:06,530 --> 00:04:07,190
Loch.

56
00:04:08,890 --> 00:04:14,830
Body and will add another tag, which is called As and Block Tag, so I'll type in.

57
00:04:16,589 --> 00:04:23,990
And look, and what this basically means is that we are first going to have some navigation power here

58
00:04:24,000 --> 00:04:27,870
and then the body of the block is going to start here and it's going to end here.

59
00:04:28,530 --> 00:04:33,840
And now, once we have added these tags over here, what we need to do now is that we also need to go

60
00:04:33,840 --> 00:04:37,740
ahead and use these two tags in the next e-mail as well.

61
00:04:38,490 --> 00:04:44,870
So now what I will do here is that inside this body tag, I'll simply go ahead and first of all, cut

62
00:04:44,880 --> 00:04:46,310
this entire code from here.

63
00:04:47,010 --> 00:04:50,610
And first of all, I'll add in the black body tag over here.

64
00:04:50,610 --> 00:04:58,500
So I'll type in block body and then I'll go ahead and type in and block up over here and I'll paste

65
00:04:58,560 --> 00:05:00,020
the entire code in here.

66
00:05:00,990 --> 00:05:05,840
So now if you go ahead and look at these two files, you will understand what exactly happens.

67
00:05:06,390 --> 00:05:13,110
So what this means is that whenever you want to load the index dot html page, this is the content inside

68
00:05:13,290 --> 00:05:13,980
the block.

69
00:05:14,460 --> 00:05:20,700
And it says, first of all, you need to add this navigation bar and then you need to load up the code

70
00:05:20,700 --> 00:05:27,960
inside the body and the block tag and the code inside these tags is nothing but this code right here,

71
00:05:28,020 --> 00:05:31,920
which we have enclosed over here inside the body and block.

72
00:05:32,400 --> 00:05:37,650
So now, once this thing is done, what you also need to mention is that in this particular index,

73
00:05:37,650 --> 00:05:42,140
dot H.M., you want to go ahead and import the stored.

74
00:05:43,440 --> 00:05:48,060
So in order to do that, you go on top over here and in here.

75
00:05:49,050 --> 00:05:51,750
You type in a tag which is called As.

76
00:05:52,780 --> 00:05:58,450
Extents and in here, you simply mention which particular template do you want to extend?

77
00:05:58,480 --> 00:06:02,320
So we want to extend a template, which is who would slash?

78
00:06:02,440 --> 00:06:04,060
We start H.M..

79
00:06:04,860 --> 00:06:11,340
And one final thing which you need to do over here is that as now the static files are actually present

80
00:06:11,520 --> 00:06:13,350
in the base of the HTML file.

81
00:06:13,620 --> 00:06:19,500
Now you need to go ahead, cut this load static file stack from here and you actually need to place

82
00:06:19,500 --> 00:06:21,660
it in the base, the HTML file.

83
00:06:22,260 --> 00:06:27,840
So now what happens is that whenever this template is loaded, it's actually going to read this line,

84
00:06:27,840 --> 00:06:31,170
which is extends forward slash based on HDMI.

85
00:06:31,890 --> 00:06:35,820
Then it's going to go ahead and look up for this file, which is based on HTML.

86
00:06:36,030 --> 00:06:42,840
And first of all, it's going to load up the static files and the static files are nothing but the stylesheet

87
00:06:42,840 --> 00:06:45,730
over here and the bootstrap links, which we have right over here.

88
00:06:45,870 --> 00:06:50,960
And after loading those files, it's going to first add the navigation bar right here.

89
00:06:51,510 --> 00:06:57,540
And after loading up the navigation bar, it's going to actually encounter the blood body and block

90
00:06:57,540 --> 00:06:57,890
type.

91
00:06:58,140 --> 00:07:04,590
So henceforth, it's actually going to go back to the next traditional and then it's going to load the

92
00:07:04,590 --> 00:07:09,600
code, which is present in between these two tags, which is the blood body and end block.

93
00:07:10,170 --> 00:07:12,750
So now if you go ahead, save the code.

94
00:07:12,750 --> 00:07:18,440
And if we go back over here and hit refresh, as you can see, this page still works fine.

95
00:07:19,080 --> 00:07:24,690
But again, if you go to the detail page, as you can see, the detail page does not have the bootstrap

96
00:07:24,690 --> 00:07:31,440
styling and the navigation bar yet because we have not yet added the base template, which is this template

97
00:07:31,440 --> 00:07:33,870
right here to the detail, not HTML file.

98
00:07:34,530 --> 00:07:36,630
So now let's go ahead and do that as well.

99
00:07:37,200 --> 00:07:39,360
So what I will do here is that a lot.

100
00:07:39,360 --> 00:07:43,880
Go ahead and add in the tags in here instead will directly go ahead.

101
00:07:44,220 --> 00:07:51,150
And first of all, I'll make this thing extend the base of the whole file so I can simply go ahead and

102
00:07:51,150 --> 00:07:55,130
copy this particular tag right here, be set up over here.

103
00:07:55,140 --> 00:08:02,130
So that means we have now loaded the based on each template and now we also need to add the block body

104
00:08:02,210 --> 00:08:03,480
and block as well.

105
00:08:03,690 --> 00:08:11,130
So I'll go ahead, start my blog, Waddi here and then I need to go ahead and send it right over here.

106
00:08:11,160 --> 00:08:13,560
So now let's see if this thing works out.

107
00:08:13,840 --> 00:08:16,200
So let me just go ahead, hit refresh.

108
00:08:16,200 --> 00:08:22,170
And now, as you can see, when I go on pizza, that is when I go into detail, not HTML template.

109
00:08:22,500 --> 00:08:28,980
Now, we also have this navigation bar and we also have bootstrap styling applied to the content as

110
00:08:28,980 --> 00:08:33,610
well, because as you can see, the font of this particular pizza actually has changed.

111
00:08:34,350 --> 00:08:41,010
So now you want if you go to the other items as well, as you can see, the bootstrap theme and the

112
00:08:41,010 --> 00:08:47,190
styling is now applied and the navigation bar is also present on each one of those items right over

113
00:08:47,190 --> 00:08:47,480
here.

114
00:08:48,500 --> 00:08:50,040
So that's it for this lecture.

115
00:08:50,240 --> 00:08:56,210
Hopefully you guys were able to understand how to use a base template, so a base template is nothing,

116
00:08:56,210 --> 00:09:02,090
but it's simply a template which is going to contain the code, which is common to all of your templates.

117
00:09:02,570 --> 00:09:08,810
And whenever you want to go ahead and extend that base template in some other template, you simply

118
00:09:08,810 --> 00:09:12,740
go ahead and type in extents and then mention the name of the template.

119
00:09:13,050 --> 00:09:16,130
And don't forget about the block body and block type.

120
00:09:16,400 --> 00:09:21,860
That is, you always need to mention what kind of code is going to be present inside the block body,

121
00:09:22,070 --> 00:09:25,370
whatever code you want to make sure that is present in the blood body.

122
00:09:25,670 --> 00:09:30,800
You simply enclose that code inside these two tanks, which is block, body and and block.

123
00:09:31,940 --> 00:09:33,950
So thank you very much for watching.

124
00:09:33,950 --> 00:09:36,140
And I'll see you guys next time.

125
00:09:36,410 --> 00:09:36,980
Thank you.

