1
00:00:00,550 --> 00:00:06,310
Hello and welcome to this lecture and in this lecture, we will go ahead and learn about creating the

2
00:00:06,310 --> 00:00:09,150
navigation bar for our website.

3
00:00:09,460 --> 00:00:14,040
So as you can see right now, our website does not have any kind of navigation bar in here.

4
00:00:14,050 --> 00:00:16,830
And now we will go ahead and learn how to create enough.

5
00:00:17,740 --> 00:00:22,480
Now, this creation of Narva is not actually a part of Django.

6
00:00:22,480 --> 00:00:24,850
That is, it has got nothing to do with Django.

7
00:00:25,130 --> 00:00:32,020
It's completely a simple e-mail and some styling, along with using some sort of a framework called

8
00:00:32,020 --> 00:00:32,710
bootstrap.

9
00:00:33,040 --> 00:00:40,360
But the main thing which you need to learn over here is that this navigation bar is the static part

10
00:00:40,360 --> 00:00:45,490
of your website, and Django is actually going to handle the dynamic part of your website.

11
00:00:45,790 --> 00:00:51,370
And the main reason why we are learning about the static part is because you should be able to know

12
00:00:51,520 --> 00:00:55,830
how to integrate the static part of your website with the dynamic part.

13
00:00:56,680 --> 00:01:00,660
So that's the whole purpose of learning about creating enough Byrn here.

14
00:01:01,060 --> 00:01:06,070
You can skip this tutorial if you want to, but if you want to learn how to create an avatar, then

15
00:01:06,070 --> 00:01:07,900
let's go ahead and learn how to do that.

16
00:01:08,920 --> 00:01:15,430
So let's first go here and let's search for something which is called as bootstrap.

17
00:01:16,140 --> 00:01:18,100
So what exactly is bootstrap?

18
00:01:19,000 --> 00:01:25,240
So just as Django is a back and framework Bootstrap is actually a friend and framework, meaning we

19
00:01:25,240 --> 00:01:32,710
can design Fronton using bootstrap and it has some predetermined code and it helps us to go ahead and

20
00:01:32,710 --> 00:01:36,550
create designs very quickly without having to write some code from scratch.

21
00:01:37,180 --> 00:01:42,730
So in order to use bootstrap, what you can do is that you can simply go to get bootstrap dot com or

22
00:01:42,730 --> 00:01:48,280
you can simply search for bootstrap and the first language pops up is going to be the official bootstrap

23
00:01:48,280 --> 00:01:48,580
link.

24
00:01:48,940 --> 00:01:50,410
You can simply click over there.

25
00:01:50,500 --> 00:01:58,680
And now in order to use bootstrap, click over here on download and you can use bootstrap and two ways.

26
00:01:58,930 --> 00:02:03,940
The first thing which you can do is that you can download bootstrap on your computer and you can simply

27
00:02:03,940 --> 00:02:06,010
go ahead and install it.

28
00:02:06,010 --> 00:02:11,410
And you start with a simpler way is to directly go ahead and use the bootstrap CDN.

29
00:02:11,650 --> 00:02:14,220
So what exactly is a bootstrap rapscallion?

30
00:02:14,470 --> 00:02:19,030
A bootstrap CDN is nothing, but it stands for content delivery network.

31
00:02:19,030 --> 00:02:24,760
And what you simply have to do is that you have to copy the link for the bootstrap CDN and you simply

32
00:02:24,760 --> 00:02:27,220
have to include that link into your file.

33
00:02:27,850 --> 00:02:33,460
Now, when you go ahead and include that link into your file, what happens is that you don't have to

34
00:02:33,460 --> 00:02:36,460
manually go ahead and have bootstrap install.

35
00:02:36,460 --> 00:02:42,790
But instead this link is actually going to go ahead and load up some bootstrap code automatically into

36
00:02:42,790 --> 00:02:46,040
your browser and Bootstrap will start working on your website.

37
00:02:46,720 --> 00:02:49,480
So for now, it will simply go ahead and copy that.

38
00:02:49,810 --> 00:02:57,190
And now we need to go ahead and paste this code in the index or e-mail file in the head.

39
00:02:57,190 --> 00:02:58,010
Digg over here.

40
00:02:58,510 --> 00:03:03,090
So simply go ahead, go over here and paste this code, which is the bootstrap code.

41
00:03:03,880 --> 00:03:10,120
And now if you go ahead and paste that thing in, save the code, and if you want to make sure that

42
00:03:10,120 --> 00:03:14,060
bootstrap is properly set up, you simply go here and hit refresh.

43
00:03:14,920 --> 00:03:22,060
So as you can see, as soon as I hit refresh, the font of these particular text actually changed.

44
00:03:22,390 --> 00:03:28,090
And this change was only made because we have included the bootstrap CDN link over here.

45
00:03:28,180 --> 00:03:35,740
And this is an indication that we have successfully added bootstrap to our indexed HCM of the page.

46
00:03:36,490 --> 00:03:38,380
Okay, so that's all fine and cool.

47
00:03:38,690 --> 00:03:42,830
And now let's go ahead and learn how to create a nappa and bootstrap.

48
00:03:43,330 --> 00:03:50,080
So let's go over here in the body tag now, because here's where we want the number to be present.

49
00:03:50,080 --> 00:03:54,140
And in here, what we will do is that we'll start writing the bar from here.

50
00:03:55,150 --> 00:04:00,330
So in order to create an alpha, first of all, we use a tag which is called as NAV.

51
00:04:00,700 --> 00:04:02,230
So let's go ahead and do that.

52
00:04:02,710 --> 00:04:05,670
And within the stack, we are actually going to design Arnav.

53
00:04:05,680 --> 00:04:12,640
But now what Bootstrap does is that bootstrap actually comes with a bunch of Inbal classes, which we

54
00:04:12,640 --> 00:04:14,660
can use to directly create elements.

55
00:04:14,680 --> 00:04:20,399
So, for example, if you want to go ahead and style this NAV, you have two ways to do so.

56
00:04:20,500 --> 00:04:27,520
The first way is that you can manually go inside the styluses file and style the number by yourself,

57
00:04:27,730 --> 00:04:34,150
or you can simply go ahead and use an inbuilt bootstrap class and assign that class to this particular

58
00:04:34,150 --> 00:04:37,520
nav so that this nav is going to be automatically styled.

59
00:04:37,960 --> 00:04:41,140
So, for example, you can type in NAV class equals.

60
00:04:42,180 --> 00:04:42,830
Never.

61
00:04:43,470 --> 00:04:49,320
And that's actually going to go ahead and create the navigation bar for you now once we have this navigation

62
00:04:49,320 --> 00:04:55,930
bar, let's say if you want to make this navigation bar of black or darker color.

63
00:04:56,430 --> 00:05:02,940
So for that, you can again go ahead and add in a class which is called as no dash duck.

64
00:05:03,420 --> 00:05:08,180
Now, remember that these are the Inbal Bootstrap classes and you can go ahead and use them.

65
00:05:08,790 --> 00:05:17,040
And let's say if you want to make the background of the nav bar to duck so you can type in Baghdad stock

66
00:05:17,040 --> 00:05:21,470
as another class and now you will have your LaFeber on your website.

67
00:05:21,480 --> 00:05:23,220
So if you go ahead and hit refresh.

68
00:05:24,110 --> 00:05:28,240
As you can see, we now have a little nap right over here.

69
00:05:29,010 --> 00:05:34,520
Now the size of the snack bar is actually small because we have not yet added any sort of content in

70
00:05:34,520 --> 00:05:34,820
here.

71
00:05:34,920 --> 00:05:37,040
But now let's go ahead and do that as well.

72
00:05:37,940 --> 00:05:42,410
So now on this particular nav bar, now we need to have the links.

73
00:05:42,890 --> 00:05:48,000
So the first thing we want on our number is that we want our app name right over here.

74
00:05:48,260 --> 00:05:49,750
So let's go ahead and add that.

75
00:05:50,270 --> 00:05:56,960
So the app name is going to be nothing but a link, so I'll type in E H.F. And for now, let's add the

76
00:05:56,960 --> 00:06:02,800
value to hash and now we will again go ahead and make use of a bootstrap class in here.

77
00:06:03,140 --> 00:06:04,940
So we'll type in class equals.

78
00:06:05,270 --> 00:06:11,150
And whenever you want to add a logo or the name of your application, you have to use a class which

79
00:06:11,150 --> 00:06:14,540
is called as no dash brand.

80
00:06:14,600 --> 00:06:20,090
Now simply go ahead and close this tag over here and let's say the name of our application is.

81
00:06:20,960 --> 00:06:22,550
Food app.

82
00:06:24,480 --> 00:06:28,000
Now, let's go ahead, save the code and see what do we get over here.

83
00:06:28,260 --> 00:06:33,360
So if I hit refresh, as you can see now, we have our app name right over here.

84
00:06:34,790 --> 00:06:41,510
Now, let's say if you want to add some nav items right over here so we can do that by creating another

85
00:06:41,510 --> 00:06:42,380
class over here.

86
00:06:42,800 --> 00:06:46,380
So right after this thing ends, I'll create a new class.

87
00:06:46,610 --> 00:06:49,360
Diamond of class equals love power.

88
00:06:49,550 --> 00:06:54,830
And this is going to be the rightmost part of the Navar, which is going to contain all the items.

89
00:06:55,190 --> 00:06:57,050
So here, I'll type in a link.

90
00:06:57,290 --> 00:07:04,040
So that's going to have a class which is called nav item, meaning that this is an item of a navigation

91
00:07:04,040 --> 00:07:04,310
bar.

92
00:07:05,090 --> 00:07:07,560
And now let's add the each of this thing.

93
00:07:07,580 --> 00:07:09,340
Let's say something as hash.

94
00:07:10,010 --> 00:07:17,510
And now let's say I see something like menu item over here for now as a placeholder.

95
00:07:18,500 --> 00:07:22,770
So once we have this thing, let's go ahead and see how this thing looks like.

96
00:07:22,790 --> 00:07:27,380
So when I hit refresh, as you can see, we have menu item over here on the extreme right.

97
00:07:28,100 --> 00:07:32,720
So now let's go ahead and copy that and pasted in and a few more times.

98
00:07:33,410 --> 00:07:40,430
And let's say instead of having a menu item over here, we see something like, let's say, add item

99
00:07:40,460 --> 00:07:42,250
that is an option to add item.

100
00:07:42,260 --> 00:07:49,600
Let's say we see something like the lead item and let's say we see something like view item over here.

101
00:07:51,490 --> 00:07:56,110
Now, these are just dummy names, and we are actually going to change them as we move along in the

102
00:07:56,110 --> 00:07:59,420
course, but for now, let's actually keep them as it is.

103
00:07:59,470 --> 00:08:06,040
So now if we go back and hit refresh, as you can see now, we have all the menu items added up right

104
00:08:06,040 --> 00:08:06,470
over here.

105
00:08:06,490 --> 00:08:10,050
So that means we have successfully designed the navigation bar in here.

106
00:08:10,090 --> 00:08:15,820
And in order to make it look a little bit better, you can also go ahead and add in another bootstrap

107
00:08:15,820 --> 00:08:23,110
class, which is called as a NAV link, so you can simply go ahead and type in that link in each of

108
00:08:23,110 --> 00:08:29,830
these items stating that this is actually a navigation bar which has a bunch of links.

109
00:08:30,070 --> 00:08:37,240
So once we go ahead do that and once we hit refresh, as you can see now, it actually remove the underlying

110
00:08:37,240 --> 00:08:38,380
from these elements.

111
00:08:38,380 --> 00:08:41,809
And they also added a little bit of padding and a margin to it.

112
00:08:42,549 --> 00:08:45,640
So as you can see, our menu is absolutely ready.

113
00:08:46,120 --> 00:08:51,970
But there is one issue with this particular menu, and we are going to discuss about this issue in the

114
00:08:51,970 --> 00:08:52,810
next lecture.

115
00:08:53,230 --> 00:08:55,120
So thank you very much for watching.

116
00:08:55,120 --> 00:08:57,220
And I'll see you guys next time.

117
00:08:57,490 --> 00:08:58,060
Thank you.

