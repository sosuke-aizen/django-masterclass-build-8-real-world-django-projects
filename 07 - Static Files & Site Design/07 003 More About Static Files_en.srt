1
00:00:00,060 --> 00:00:05,760
Hello and welcome to this lecture, and in this lecture, we will learn a little bit more about a static

2
00:00:05,760 --> 00:00:06,270
files.

3
00:00:06,480 --> 00:00:12,050
So we have already used static files and Shango and now let's learn a few more things about it.

4
00:00:12,690 --> 00:00:15,950
So let's first learn what exactly are static files.

5
00:00:16,590 --> 00:00:20,520
So we all know that we have CSI as a static file.

6
00:00:20,820 --> 00:00:26,270
What files, such as images as well as JavaScript can also be static files.

7
00:00:26,640 --> 00:00:32,430
So in the upcoming examples, we are also going to use images and we are also going to use some sort

8
00:00:32,430 --> 00:00:33,540
of JavaScript code.

9
00:00:33,810 --> 00:00:39,790
And that code should be actually placed in these static files, which we have just used now.

10
00:00:39,990 --> 00:00:45,990
The next thing which we are going to learn is how exactly does Django know where these static files

11
00:00:45,990 --> 00:00:46,650
are located?

12
00:00:46,890 --> 00:00:53,310
So what we have done here is that in the previous lecture we went ahead and we have created a directory

13
00:00:53,310 --> 00:00:59,670
or a folder named Astatke, and Django automatically looked at that particular static directed to look

14
00:00:59,670 --> 00:01:00,660
for static files.

15
00:01:00,840 --> 00:01:05,950
So how exactly does Django know about where exactly are these static files located?

16
00:01:06,480 --> 00:01:12,420
So if you go into the settings dot by a file of your application, you will find this particular variable

17
00:01:12,420 --> 00:01:15,690
right here, which is static and underscore you are.

18
00:01:16,110 --> 00:01:21,610
And as you can see here, the path for static files is clearly mentioned.

19
00:01:21,870 --> 00:01:28,150
So it's mentioned that the static files are actually present at this particular location right here.

20
00:01:28,770 --> 00:01:34,110
So these settings are automatically configured by a triangle so that whenever you want to actually go

21
00:01:34,110 --> 00:01:42,180
ahead and whenever we load some static files, Django now knows that as this static, you are variable

22
00:01:42,180 --> 00:01:43,560
points at this path.

23
00:01:43,740 --> 00:01:47,370
It needs to look for those static files at this particular directory.

24
00:01:47,970 --> 00:01:52,740
Now, if you want to change that, you can simply go ahead, write in some another name over here.

25
00:01:53,070 --> 00:01:56,940
And a new path will be configured for static files directly.

26
00:01:58,050 --> 00:02:04,260
Now, one more thing which you need to learn here is that if you have a look at the install app for

27
00:02:04,260 --> 00:02:07,470
your project, this is actually installed at four different projects.

28
00:02:07,470 --> 00:02:10,370
So please ignore the above a few lines of code.

29
00:02:10,680 --> 00:02:16,390
But if you have a look at the bottom, most part over here, as you can see, it says Django dot country

30
00:02:16,590 --> 00:02:17,840
dot static files.

31
00:02:18,300 --> 00:02:24,790
And this is the exact same app which actually allows you to go ahead and use static files in general.

32
00:02:24,810 --> 00:02:30,380
So this thing actually allows you to manage all the static files, which we have in our Django application.

33
00:02:30,450 --> 00:02:37,590
So remember that static files can not only be accessed files, but it also can be some image or JavaScript.

34
00:02:37,810 --> 00:02:43,650
So let's say if you want to use some static image on your website, you can actually use that particular

35
00:02:43,650 --> 00:02:46,900
image and store that image in the static directory.

36
00:02:47,160 --> 00:02:53,670
And the reason why we are storing the images, JavaScript and Xerces and are standing directly is because

37
00:02:53,940 --> 00:02:56,990
these parts are these static parts of your website.

38
00:02:57,000 --> 00:02:59,690
That means these things are not going to change.

39
00:03:00,000 --> 00:03:06,270
So let's say, for example, if you have an image of your website logo, so that logo is going to remain

40
00:03:06,270 --> 00:03:10,840
the same throughout unless and until you go ahead and manually change that logo.

41
00:03:10,860 --> 00:03:17,580
So that means whatever content is not going to change frequently is going to be present in the static

42
00:03:17,580 --> 00:03:18,510
files directory.

43
00:03:18,810 --> 00:03:23,150
And that's the very same reason why we call them as static files.

44
00:03:23,220 --> 00:03:25,110
So that's it for this lecture.

45
00:03:25,500 --> 00:03:29,960
And I hope you guys be able to learn a few more things about static files.

46
00:03:30,360 --> 00:03:34,350
So thank you very much for watching and I'll see you guys next time.

