1
00:00:00,180 --> 00:00:06,300
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how we can add an

2
00:00:06,300 --> 00:00:08,340
image field to our website.

3
00:00:08,640 --> 00:00:14,590
So right now, as you can see, we have all these items over here like pizza, burrito and cheeseburger.

4
00:00:14,940 --> 00:00:18,870
So this page actually serves as a menu or food menu.

5
00:00:19,380 --> 00:00:25,740
So what we also want to do is that as this is a digital menu, let's say you also want to add an image

6
00:00:25,740 --> 00:00:30,870
field over here so that the image will be displayed along with these names right here.

7
00:00:31,440 --> 00:00:33,210
So how exactly can we do that?

8
00:00:33,700 --> 00:00:40,200
So whenever you want to add a new field to your models, what you simply have to do is that you have

9
00:00:40,200 --> 00:00:42,540
to go to the model store profile.

10
00:00:43,090 --> 00:00:44,490
So let's go over there.

11
00:00:44,880 --> 00:00:48,180
So let me just open up the model stockpile file.

12
00:00:48,480 --> 00:00:55,200
And in here, you just need to go ahead and add another field over here for having the image for each

13
00:00:55,200 --> 00:00:56,670
one of these food items.

14
00:00:57,300 --> 00:01:02,660
Now, the way in which we will be adding images here is that we won't be adding an image field.

15
00:01:02,790 --> 00:01:06,300
Instead, we will go ahead and add a character field over here.

16
00:01:06,630 --> 00:01:13,080
And that character field for the image is actually going to store the you are of a particular image.

17
00:01:13,980 --> 00:01:21,060
So, for example, let's say if you want to save the image of pizza, then what we do is that you first

18
00:01:21,060 --> 00:01:23,580
go ahead and search for a pizza image.

19
00:01:24,240 --> 00:01:31,830
So Dipen Pizza over here and I'll go to images and let's say if you want a very specific image, you

20
00:01:31,830 --> 00:01:39,900
can simply go ahead and you can just open this image up in a new tab so as to get its link.

21
00:01:40,230 --> 00:01:41,340
Or you can simply right.

22
00:01:41,340 --> 00:01:44,070
Click over here and you can copy the image address.

23
00:01:44,580 --> 00:01:50,160
Now, if you go ahead and paste this address, as you can see, this thing right here is actually the

24
00:01:50,160 --> 00:01:52,150
address of the image which you want.

25
00:01:52,980 --> 00:01:59,520
So now, instead of having this entire image on our server, what we will do is that we will go ahead

26
00:01:59,520 --> 00:02:02,030
and use some other image right over here.

27
00:02:02,580 --> 00:02:08,669
Now, in real life situations or in real life projects, what you have to do is that you have to host

28
00:02:08,669 --> 00:02:16,610
the images on some other server and you actually can go ahead and import or use images from the other.

29
00:02:16,620 --> 00:02:19,080
So onto your own server right over here.

30
00:02:19,740 --> 00:02:23,550
So now let's go ahead and learn how to add an image to our website.

31
00:02:23,580 --> 00:02:29,610
So for that, we will simply go ahead and type in new filename as, let's say item.

32
00:02:30,740 --> 00:02:31,610
Underscore.

33
00:02:33,190 --> 00:02:36,020
Image equals that's going to be model start.

34
00:02:36,070 --> 00:02:40,000
This is going to be a character film as we are storing you are right here.

35
00:02:40,750 --> 00:02:45,970
Now, what I will do here is that I'll type in the max and the school land.

36
00:02:47,260 --> 00:02:54,790
Property of this field to be equal to, let's say let's say we do it five hundred because you are actually

37
00:02:54,790 --> 00:02:56,270
longer, OK?

38
00:02:56,500 --> 00:03:03,220
So once you have that, what you can also do here is that you can also add the by default field of the

39
00:03:03,220 --> 00:03:03,650
image.

40
00:03:03,850 --> 00:03:06,750
So what exactly do I mean by a by default field?

41
00:03:07,240 --> 00:03:12,910
So let's say if you are adding the item data to your database and let's say if you forgot to add an

42
00:03:12,910 --> 00:03:14,500
image or that particular item.

43
00:03:15,010 --> 00:03:17,260
So what exactly would happen in that case?

44
00:03:17,680 --> 00:03:23,620
So in that case, you won't have any image displayed on your webpage and that's not going to look good

45
00:03:23,620 --> 00:03:24,070
at all.

46
00:03:24,580 --> 00:03:30,070
So what we will do here is that instead of having some empty images, we will add a default image in

47
00:03:30,070 --> 00:03:35,620
here, which means that if no image is present over here, you actually need to have a placeholder for

48
00:03:35,620 --> 00:03:36,250
that image.

49
00:03:36,760 --> 00:03:44,260
So what we will do here is that will simply go ahead, give a comma and then will type in default equals.

50
00:03:45,930 --> 00:03:50,280
And we will set the value of this default to some placeholder image.

51
00:03:51,410 --> 00:03:55,850
So in order to get a placeholder, you can again go to Google and search for.

52
00:03:57,230 --> 00:03:58,930
Please hold the.

53
00:04:00,390 --> 00:04:01,860
Food image.

54
00:04:01,920 --> 00:04:07,560
So now from here, you can actually go ahead and choose any one of these images right over here, so

55
00:04:07,560 --> 00:04:10,110
I'll just simply go ahead and choose this image.

56
00:04:10,110 --> 00:04:12,870
So I'll just go ahead, copy the image address.

57
00:04:13,440 --> 00:04:19,000
And now I can go back to the code and I'll just paste that image address up over here.

58
00:04:19,779 --> 00:04:22,390
OK, so now I have the image address over here.

59
00:04:22,440 --> 00:04:23,480
We are good to go.

60
00:04:23,970 --> 00:04:26,220
So now we have added this feel right here.

61
00:04:26,400 --> 00:04:33,360
But now as you have made changes to the models, now you need to go ahead and also make migration's

62
00:04:33,480 --> 00:04:37,230
so that these changes are actually reflected back in the database.

63
00:04:37,830 --> 00:04:41,840
So let's open up the terminal and let's stop the server for now.

64
00:04:42,450 --> 00:04:49,950
And first of all, what you need to do is that you need to type in Python managed by.

65
00:04:51,160 --> 00:04:56,050
Make migration's food because we have made changes to the food app.

66
00:04:57,060 --> 00:05:02,700
So now let's go ahead, hit enter and now you need to type in Python.

67
00:05:03,570 --> 00:05:06,940
And I start by asking you will migrate.

68
00:05:07,300 --> 00:05:11,570
And as this is our second migration, it's actually going to give you a number over here.

69
00:05:11,590 --> 00:05:14,260
So you need to use that specific number over here.

70
00:05:14,770 --> 00:05:19,840
So I'll type in Eskil migrate food zero zero zero two.

71
00:05:20,950 --> 00:05:26,050
And now once I go ahead and hit enter, as you can see, it has actually created the table.

72
00:05:26,050 --> 00:05:31,990
And the final thing which we need to type in is that you need to type in Python three and stop by.

73
00:05:33,950 --> 00:05:34,720
Margaret.

74
00:05:35,690 --> 00:05:41,270
And now, as you can see, the migration's have been applied and hopefully now we have a new field in

75
00:05:41,270 --> 00:05:42,110
our database.

76
00:05:42,540 --> 00:05:47,630
So first of all, we will go ahead and see if we actually have that field in our database.

77
00:05:47,630 --> 00:05:54,180
So I'll just go ahead to open Python three, managed by a run server to run the server.

78
00:05:54,590 --> 00:06:02,630
And now let's go to the admin side or the admin panel and see if we actually have that particular field.

79
00:06:03,590 --> 00:06:09,950
So that should be admitted and now when I log in, as you can see, we have items and when I click on

80
00:06:09,950 --> 00:06:16,430
items and when I go on one of these items, as you can see now, we have this item image field over

81
00:06:16,430 --> 00:06:21,690
here and this item image is going to be present on all of these items over here.

82
00:06:21,890 --> 00:06:28,010
And as you can see, that by default, value of that particular item field is also set over here.

83
00:06:28,790 --> 00:06:35,960
Now, what we need to do is that we need to figure out how exactly can we display this image up on the

84
00:06:35,960 --> 00:06:38,840
index, not HTML page, which is this page right here.

85
00:06:38,870 --> 00:06:44,540
So right now, this page only displays the item ID and the item name.

86
00:06:44,840 --> 00:06:48,930
And now we also want to go ahead and incorporate the images as well.

87
00:06:49,160 --> 00:06:55,610
So what you can do is that you can just go ahead and get rid of all this code right here for now, or

88
00:06:55,610 --> 00:07:01,460
you can just go ahead and leave it as it is and you can delete it later so you can just go ahead and

89
00:07:01,460 --> 00:07:09,320
type in something over here like this is the old code so that you can go ahead and remove this code

90
00:07:09,320 --> 00:07:09,740
later.

91
00:07:10,190 --> 00:07:13,890
So for now, let's go ahead and understand how to add image over here.

92
00:07:14,510 --> 00:07:20,300
So first of all, we'll actually use this code right here, which is we actually need to look through

93
00:07:21,020 --> 00:07:27,290
all the items in the list and we will also use and form and block as well.

94
00:07:28,070 --> 00:07:31,370
So just go ahead and have that code in here and in here.

95
00:07:31,400 --> 00:07:36,440
What we will do is that we will use bootstrap classes to style our entire layout.

96
00:07:36,740 --> 00:07:42,530
So what we want to do over here is that if you go to the actual.

97
00:07:43,550 --> 00:07:49,250
Paige, we actually want to have a section over here wherein we have image on the left hand side and

98
00:07:49,250 --> 00:07:51,300
the item name on the right hand side.

99
00:07:51,710 --> 00:07:53,210
So let's go ahead and do that.

100
00:07:53,300 --> 00:07:56,960
So first of all, we will go ahead and create a rule and bootstrap.

101
00:07:56,970 --> 00:07:59,530
So I'll type in this class equals rule.

102
00:08:00,500 --> 00:08:03,530
And within this rule, we are going to have two columns.

103
00:08:03,560 --> 00:08:11,780
So this is going to be div class called Dash and dash three, which means that this particular column

104
00:08:11,780 --> 00:08:16,140
is going to spend three rows and we are going to have another column over here.

105
00:08:16,280 --> 00:08:20,540
So I'll type and div class equals call, dash and dash.

106
00:08:20,720 --> 00:08:24,060
And this is actually going to span just four columns.

107
00:08:24,080 --> 00:08:28,500
And now let's say we again go ahead and create another column over here.

108
00:08:29,090 --> 00:08:34,280
So div class called Dash and Dash and let's say this is only going to spend two.

109
00:08:34,730 --> 00:08:37,880
So we already know that Bootstrap actually has 12 columns.

110
00:08:38,240 --> 00:08:40,460
That is, it uses the 12 column layout.

111
00:08:40,789 --> 00:08:44,380
So these are only like four, five, six, seven, eight and nine.

112
00:08:44,870 --> 00:08:48,830
So we right now have nine columns spanned over here.

113
00:08:48,980 --> 00:08:52,730
So what we will do is that we will add an offset to the first one.

114
00:08:53,210 --> 00:08:59,180
So what offset basically means is that it's actually going to leave a space of two columns over here

115
00:08:59,180 --> 00:09:03,000
on the left hand side and then it's actually going to start this entire column.

116
00:09:03,530 --> 00:09:10,640
So we will also add a class which is called class offset dash empty dash two, which means on a medium

117
00:09:10,640 --> 00:09:13,020
device, it's going to have an offset of two.

118
00:09:13,340 --> 00:09:17,830
Now, we will go ahead and add the image over here in the first row.

119
00:09:18,260 --> 00:09:23,120
So I'll just go ahead and use the image tag and the class for this image is going to be covered.

120
00:09:23,400 --> 00:09:28,340
So this is actually going to make the image look like a card with rounded corners.

121
00:09:28,520 --> 00:09:34,130
And now I'll also set the height of this image to, let's say, 150 pixels.

122
00:09:34,820 --> 00:09:39,810
And now once we have said the height, let's actually go ahead and get the source of this image.

123
00:09:40,340 --> 00:09:45,830
So now, as we all know, the source of the image is nothing, but it should actually be the wall of

124
00:09:45,830 --> 00:09:48,220
the image which we are going to be using over here.

125
00:09:48,590 --> 00:09:54,410
And we all know that we are we have actually stood the you are all of the image in the field, which

126
00:09:54,410 --> 00:09:55,810
is called as item image.

127
00:09:56,300 --> 00:09:59,030
So let's go ahead and get access to item image.

128
00:09:59,490 --> 00:10:02,280
So we already have item stored over here.

129
00:10:02,450 --> 00:10:08,350
So in order to get its image, I can type an item, not item and school image.

130
00:10:08,750 --> 00:10:13,340
OK, so once we have added the image, let's go ahead and end this tag over here.

131
00:10:13,760 --> 00:10:16,190
So now we will have our image right here.

132
00:10:17,150 --> 00:10:22,530
So now if you go back and hit refresh, as you can see, we have all three images up over here.

133
00:10:23,130 --> 00:10:28,970
OK, so now once we have access to the image now, let's go ahead and to the right hand side that is

134
00:10:28,970 --> 00:10:29,600
over here.

135
00:10:29,960 --> 00:10:34,040
Let's go ahead and add in the item, name, the description and the price.

136
00:10:34,640 --> 00:10:42,050
So we'll go into the second column right here and I'll use the H3 tag over here to display the item

137
00:10:42,050 --> 00:10:42,370
name.

138
00:10:42,380 --> 00:10:44,360
And in here I can simply type in.

139
00:10:45,700 --> 00:10:55,000
Item, item on the name to get access to the item name then also let's go ahead and display the item

140
00:10:55,000 --> 00:10:55,750
description.

141
00:10:55,750 --> 00:10:57,580
So I'll use each file for this.

142
00:10:58,360 --> 00:10:59,560
And let's see.

143
00:10:59,620 --> 00:11:04,480
This is going to say something like item item, underscore description.

144
00:11:05,710 --> 00:11:10,860
And the final one is going to be the item price, so I'll use eight six for the price.

145
00:11:10,870 --> 00:11:18,040
So it's six and I'll use a dollar sign over here even before we actually go ahead and get the numeric

146
00:11:18,040 --> 00:11:18,520
price.

147
00:11:19,240 --> 00:11:22,630
So item dot item on the school price.

148
00:11:23,870 --> 00:11:27,470
OK, so once we have that, let's see how this thing is going to look like.

149
00:11:27,470 --> 00:11:34,370
So if I go ahead and hit refresh, as you can see, we do have everything over here except for the price.

150
00:11:34,370 --> 00:11:38,120
And I guess that's because I have a typo here.

151
00:11:40,680 --> 00:11:46,470
Now, once you fix that, when we go back and hit refresh, as you can see now, we have all these items

152
00:11:46,470 --> 00:11:47,550
listed up over here.

153
00:11:47,880 --> 00:11:53,370
And now what we will do is that we will also add a button over here, which is going to say something

154
00:11:53,370 --> 00:11:58,990
like, let's give you details, which is going to allow us to view the details of these items.

155
00:11:59,040 --> 00:12:00,980
So we will do that in the next lecture.

156
00:12:01,410 --> 00:12:07,530
So hopefully you guys be able to understand for now how we have added the image feel over here to our

157
00:12:07,530 --> 00:12:12,990
models and how we are adding the image up over here using bootstrap in a proper layout.

158
00:12:13,440 --> 00:12:17,400
So thank you very much for watching and I'll see you guys next time.

159
00:12:17,820 --> 00:12:18,450
Thank you.

