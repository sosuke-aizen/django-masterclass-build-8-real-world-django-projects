1
00:00:00,240 --> 00:00:03,940
OK, so let's continue from where we have left in the previous lecture.

2
00:00:04,350 --> 00:00:11,940
So now that once we have all these fields set up over here, let's go ahead and also add a button over

3
00:00:11,940 --> 00:00:15,510
here, which is going to allow us to view the details of these items.

4
00:00:16,140 --> 00:00:21,570
So what I will do here is that I'll simply go ahead and instead of creating a button, I'll just go

5
00:00:21,570 --> 00:00:25,530
ahead and use an aircraft tag over here and in here.

6
00:00:25,530 --> 00:00:33,900
I just assign the class for this as button, so I'll type in VPN, which is going to be a button class,

7
00:00:33,900 --> 00:00:36,720
and this is going to make this link look like a button.

8
00:00:37,200 --> 00:00:39,980
And for the color, let's say we want green color.

9
00:00:39,990 --> 00:00:44,220
So the bootstrap class you want to use for that is going to be and dash.

10
00:00:45,100 --> 00:00:48,670
Success and now let's go ahead and type in.

11
00:00:49,880 --> 00:00:50,970
Details over here.

12
00:00:51,800 --> 00:00:57,110
So now once you go back and hit refresh, as you can see now you have this button over here with these

13
00:00:57,210 --> 00:00:57,880
details.

14
00:00:57,890 --> 00:01:00,420
But right now, these buttons actually do nothing.

15
00:01:00,440 --> 00:01:03,580
That is because we have not yet added any code to them.

16
00:01:03,590 --> 00:01:10,790
But now let's go ahead and add in some code so that this respective buttons are going to redirect us

17
00:01:10,790 --> 00:01:13,160
to these respective item details over here.

18
00:01:13,790 --> 00:01:20,060
So for that, you already know that we have added we had previously added this code right here, which

19
00:01:20,060 --> 00:01:23,470
is the food detail and the item ID.

20
00:01:23,810 --> 00:01:26,530
So let's go ahead and simply get this from here.

21
00:01:28,510 --> 00:01:35,440
And let's be straight over here and now once you go ahead, save the code, everything should work fine.

22
00:01:36,220 --> 00:01:43,000
So now if I hit refresh and when I click on details, as you can see now, we are redirected into the

23
00:01:43,000 --> 00:01:46,190
details page of the respective items right over here.

24
00:01:46,660 --> 00:01:53,190
So we have simply replaced the usual normal link, which we had with the detail, but no here.

25
00:01:53,200 --> 00:01:54,550
So nothing special in that.

26
00:01:54,580 --> 00:02:01,450
So now once we go ahead and do that, let's also go ahead and replace these images with actual food

27
00:02:01,450 --> 00:02:01,990
images.

28
00:02:02,500 --> 00:02:06,770
So as you all know, we had actually used one of these images right here.

29
00:02:06,940 --> 00:02:11,050
So what I will do is that I'll just go ahead and copy that image now.

30
00:02:11,890 --> 00:02:15,730
And what I will do here is that I'll simply go to.

31
00:02:17,290 --> 00:02:18,430
The admin panel.

32
00:02:19,540 --> 00:02:25,490
And I'll just go ahead and edit these images out, so I'll paste the new image link over here, see

33
00:02:25,490 --> 00:02:30,040
if the changes so that all of them actually have a food image.

34
00:02:31,000 --> 00:02:33,760
So I'm using the same image for each one of those items.

35
00:02:33,760 --> 00:02:36,680
But you can go ahead and use other images if you want to.

36
00:02:36,880 --> 00:02:42,280
As you can see when I go ahead and hit refresh now, we actually have an image of pizza over here on

37
00:02:42,280 --> 00:02:43,210
the food items.

38
00:02:44,720 --> 00:02:50,780
So that means we have successfully implemented the functionality of adding images and making our index

39
00:02:50,780 --> 00:02:52,280
page look much more better.

40
00:02:52,730 --> 00:02:57,820
Now they are a bit of formatting issues over here, which we are going to go into in the upcoming lectures.

41
00:02:57,830 --> 00:03:00,570
But for now, this would work fine for us.

42
00:03:01,190 --> 00:03:06,800
So now in the next lecture, what we will do is that will try to go ahead and also add the image over

43
00:03:06,800 --> 00:03:08,790
here to the detailed view right here.

44
00:03:09,170 --> 00:03:11,180
So thank you very much for watching.

45
00:03:11,180 --> 00:03:13,190
And I'll see you guys next time.

46
00:03:13,700 --> 00:03:14,360
Thank you.

