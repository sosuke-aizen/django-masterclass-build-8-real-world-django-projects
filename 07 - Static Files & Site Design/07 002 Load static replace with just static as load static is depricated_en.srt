1
00:00:00,300 --> 00:00:06,840
So in the previous lecture, we had actually used the loud static file tag here in order to actually

2
00:00:06,840 --> 00:00:13,390
go ahead and make sure that whenever we are using this specific template, the static files are loaded.

3
00:00:13,860 --> 00:00:21,030
But in the recent versions of Django, this load, a static file tag has been deprecated.

4
00:00:21,150 --> 00:00:28,350
And instead of using load static files, the new syntax which we use now in order to load these static

5
00:00:28,350 --> 00:00:31,510
files is nothing but load static.

6
00:00:31,830 --> 00:00:38,280
So from now on, whenever you are actually using the load static file tag, simply go ahead and replace

7
00:00:38,280 --> 00:00:41,380
it with load static and it should work perfectly fine.

8
00:00:41,940 --> 00:00:47,490
So that's the only purpose of this specific lecture that is to go ahead and make a slight correction

9
00:00:47,500 --> 00:00:52,970
that was all about using the load static instead of using the load static files.

10
00:00:52,980 --> 00:00:56,670
So thank you very much for watching and I'll see you guys next time.

11
00:00:56,910 --> 00:00:57,480
Thank you.

