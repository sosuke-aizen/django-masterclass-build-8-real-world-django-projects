1
00:00:00,510 --> 00:00:06,120
Hello and welcome to this lecture and in this lecture, we will go ahead and learn something about a

2
00:00:06,120 --> 00:00:07,170
static files.

3
00:00:07,820 --> 00:00:10,380
So what exactly are static files?

4
00:00:10,770 --> 00:00:16,650
So right now, if you have a look at our website, as you can see, the site actually works just fine.

5
00:00:16,950 --> 00:00:22,950
But if you look at the design of our website, it looks as if the site is from the 1990s.

6
00:00:23,520 --> 00:00:31,340
So we need to make sure that Jango site not only works in a better way, but also looks fantastic.

7
00:00:31,740 --> 00:00:37,860
So for that very purpose, now we need to learn how we can go ahead and start e-mailing our website.

8
00:00:38,400 --> 00:00:44,220
So usually if you already know the front end web designing, then you might know that whenever you want

9
00:00:44,220 --> 00:00:51,770
to style a particular e-mail file, you always have a Sears's file associated with that particular e-mail

10
00:00:51,780 --> 00:00:52,110
file.

11
00:00:52,800 --> 00:00:59,580
Now, in this case, we do have these templates here, which is the indexed e-mail and the of e-mail.

12
00:00:59,580 --> 00:01:03,630
And now we need to add Xerces files to these particular files.

13
00:01:03,960 --> 00:01:05,720
So how exactly can we do that?

14
00:01:05,940 --> 00:01:12,210
So we cannot directly go ahead and add those files in here because that won't work fine.

15
00:01:12,480 --> 00:01:18,050
Instead, what you need to do is that you need to use something which is called as static files.

16
00:01:18,390 --> 00:01:24,000
So in here, what you need to do in the particular project is that you need to go ahead and create a

17
00:01:24,000 --> 00:01:27,640
folder and name that folder as static.

18
00:01:27,840 --> 00:01:29,350
So let's go ahead and do that.

19
00:01:29,700 --> 00:01:36,050
So in the food app, I'll just go ahead, create a new folder and I'll call that folder as static.

20
00:01:36,930 --> 00:01:44,190
Now, just as we have done with templates, that is, we have the templates folder in the food app and

21
00:01:44,190 --> 00:01:50,790
then again inside the templates folder, we have this food folder and then we have placed the templates

22
00:01:50,790 --> 00:01:52,510
in that particular food folder.

23
00:01:53,160 --> 00:01:59,100
Now, in a similar fashion, what we do here is that we go ahead and create another folder in the static

24
00:01:59,100 --> 00:01:59,610
folder.

25
00:02:00,460 --> 00:02:02,710
And we will call that as food.

26
00:02:04,540 --> 00:02:11,050
And now, whatever static files which we are going to have in our application are going to be stored

27
00:02:11,050 --> 00:02:12,760
in this particular food folder.

28
00:02:13,180 --> 00:02:20,620
Now, what we can do is that we can go ahead and add those Sears's files in this particular folder right

29
00:02:20,630 --> 00:02:20,860
here.

30
00:02:21,700 --> 00:02:27,890
Now, why exactly do we go ahead and add Sears's files into this particular static folder?

31
00:02:28,570 --> 00:02:34,720
And that is because all the files which are related to our website, which are static in nature, that

32
00:02:34,720 --> 00:02:35,980
is, they are not dynamic.

33
00:02:36,400 --> 00:02:43,480
Those contents of our website or of our project are going to be placed inside this particular folder

34
00:02:43,480 --> 00:02:44,020
right here.

35
00:02:44,620 --> 00:02:51,070
So, for example, even if you have some static images like the logo or the background image or anything

36
00:02:51,070 --> 00:02:56,800
like that, you can go ahead and save those images as well in this particular folder right here.

37
00:02:57,460 --> 00:03:03,970
So now let's go ahead and try to create a case file in here for the index e-mail template.

38
00:03:04,150 --> 00:03:10,880
So I'll go ahead, go into the food folder, create a new file, and I'll name this thing as stylus.

39
00:03:11,950 --> 00:03:14,540
And as you can see, we do have this file right here.

40
00:03:15,680 --> 00:03:21,910
Now, once we go ahead and have this file, let's learn how exactly can we attach this file with the

41
00:03:21,910 --> 00:03:23,480
indexed or HTML file.

42
00:03:24,130 --> 00:03:30,760
So, as you already know, whenever you want to attach a Sears's file to an HTML file, you always go

43
00:03:30,760 --> 00:03:36,010
ahead and mention the link of the Sears's file in the head tag of the HDMI.

44
00:03:36,670 --> 00:03:39,580
So now let me just go ahead, cut this code.

45
00:03:40,180 --> 00:03:42,710
First of all, write in some e-mail code in here.

46
00:03:43,210 --> 00:03:48,640
So if you're using visual studio code, you can simply go ahead typing an exclamation mark.

47
00:03:48,640 --> 00:03:54,220
And if you hit enter the code is going to be automatically added up for you.

48
00:03:54,810 --> 00:04:01,260
Now, once this HMO code is added, you can actually go ahead and paste the previous code in here.

49
00:04:01,390 --> 00:04:05,110
And now, if you save the code, everything is going to work just fine.

50
00:04:05,830 --> 00:04:11,530
Now, the only thing which we need to do is that we need to now add the link to this particular static

51
00:04:11,530 --> 00:04:15,420
file, which is still not Xerces up over here in the head tag.

52
00:04:16,209 --> 00:04:21,610
So we go ahead and type in link rel equals.

53
00:04:21,910 --> 00:04:30,070
That is going to be stylesheet because this is a stylesheet and then we type in HSF and what usually

54
00:04:30,070 --> 00:04:33,510
we do is that we specify a direct link to the Sears's file.

55
00:04:33,820 --> 00:04:39,340
But right now, as this is in the static files folder, we now need to go ahead and use some template

56
00:04:39,340 --> 00:04:43,000
syntax over here to link that particular file.

57
00:04:43,360 --> 00:04:49,630
So let me just go ahead and use this syntax right here and now.

58
00:04:49,990 --> 00:04:57,070
We want to say that we want to look up for the stylesheet in the static directly.

59
00:04:57,070 --> 00:04:58,740
So we'll mention static in here.

60
00:04:59,230 --> 00:05:02,810
And then we also mentioned the exact path of our file.

61
00:05:02,920 --> 00:05:08,550
So that's actually present in food, soil, type in food slash style.

62
00:05:08,560 --> 00:05:15,100
DOCSIS So once we go ahead and do that now, this file is successfully linked with this dialogue.

63
00:05:15,100 --> 00:05:15,940
Xerces file.

64
00:05:16,820 --> 00:05:23,120
Now, in order to confirm that what we will do here is that will add some samples is as good as of now

65
00:05:23,540 --> 00:05:29,240
and see if that style is going to be applied to this index e-mail page.

66
00:05:29,270 --> 00:05:32,710
So let's go ahead and add in the style for body.

67
00:05:33,110 --> 00:05:36,260
So let's say we want to set the background.

68
00:05:37,240 --> 00:05:40,270
Color of this thing to let's see.

69
00:05:41,570 --> 00:05:42,380
Light yellow.

70
00:05:42,500 --> 00:05:50,300
So once we go ahead and said that, and if we go back over here and hit refresh, as you can see, we

71
00:05:50,300 --> 00:05:51,770
get an error over here.

72
00:05:51,770 --> 00:05:56,510
And this error is that did you forget to load or register this tag?

73
00:05:57,430 --> 00:06:04,900
So what it exactly means is that, OK, you actually have used the cases file in here, which is in

74
00:06:04,900 --> 00:06:11,080
the template, but even before you use that, you need to actually go ahead and load this particular

75
00:06:11,080 --> 00:06:13,460
static file into this particular template.

76
00:06:14,020 --> 00:06:18,190
So how exactly can we go ahead and load this particular static file?

77
00:06:18,490 --> 00:06:20,860
And we do that by typing in.

78
00:06:22,030 --> 00:06:29,740
Loud, static files at the top of your template, so you simply type and load static files, simply

79
00:06:29,740 --> 00:06:31,080
go ahead and save the code.

80
00:06:31,090 --> 00:06:35,250
And when you go back over here and hit refresh, we do again have an error.

81
00:06:35,800 --> 00:06:40,500
And that's because this should actually be food slash, not food not.

82
00:06:40,510 --> 00:06:46,000
And if you go ahead and hit refresh, as you can see, the background color of this particular page

83
00:06:46,000 --> 00:06:46,680
changes.

84
00:06:47,560 --> 00:06:53,920
So we did have some errors over here and I thought I should do actually resolve errors without editing

85
00:06:53,920 --> 00:06:59,620
the video so that you guys will be able to understand how exactly do we go ahead and get rid of the

86
00:06:59,620 --> 00:07:00,040
errors.

87
00:07:00,530 --> 00:07:06,070
So one more error, which you could get is that despite having all the code correct, the color over

88
00:07:06,070 --> 00:07:07,330
here might not change.

89
00:07:07,600 --> 00:07:13,840
And that's because if your server was already up and running, you actually need to go ahead after making

90
00:07:13,840 --> 00:07:17,260
changes, stop the server and restart the server again.

91
00:07:17,800 --> 00:07:20,860
And once you do that, the color will be loaded up over there.

92
00:07:21,580 --> 00:07:28,560
So this means that as this page now has a background, color means that we have successfully linked

93
00:07:28,570 --> 00:07:32,830
this particular style as file with the index of each Schimel file.

94
00:07:33,040 --> 00:07:39,670
Now we can go ahead and using the styluses as file, we can style any one of the elements which is present

95
00:07:39,670 --> 00:07:40,430
right over here.

96
00:07:40,450 --> 00:07:42,130
So that's it for this lecture.

97
00:07:42,190 --> 00:07:48,670
And I hope you guys were able to understand what a static files and why exactly they are used and how

98
00:07:48,670 --> 00:07:52,960
exactly do you link a stylesheet with a template in Django.

99
00:07:53,470 --> 00:07:57,310
So thank you very much for watching and I'll see you guys next time.

100
00:07:57,700 --> 00:07:58,270
Thank you.

