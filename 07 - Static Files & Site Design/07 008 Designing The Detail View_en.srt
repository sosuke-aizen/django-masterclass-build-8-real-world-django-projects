1
00:00:00,330 --> 00:00:06,060
Hello and welcome to this lecture and in this lecture, let's actually go ahead and design this particular

2
00:00:06,060 --> 00:00:07,170
detail right here.

3
00:00:07,980 --> 00:00:13,410
So what we want to do is that we want a better format, the content of the detailed view, and also

4
00:00:13,410 --> 00:00:14,640
add the image over here.

5
00:00:15,150 --> 00:00:19,620
So right now, if you go to the detail view, this is the code which we have right now.

6
00:00:20,100 --> 00:00:26,190
And let's go ahead and add some bootstrap over here as well so as to style this in a much more better

7
00:00:26,190 --> 00:00:26,460
way.

8
00:00:27,060 --> 00:00:30,570
So first of all, I will go ahead and use a bootstrap container.

9
00:00:31,410 --> 00:00:36,660
So I'll type in the class equals container, which is going to contain all the content of our Web page.

10
00:00:36,660 --> 00:00:39,440
And within that container, let's actually have a room.

11
00:00:39,840 --> 00:00:41,730
So class equals row.

12
00:00:41,760 --> 00:00:44,970
And in this row, obviously, we are going to have two columns.

13
00:00:45,400 --> 00:00:48,630
So we will actually have two sections of a column.

14
00:00:48,930 --> 00:00:55,470
And let's say each one of those actually hold, let's say, six, six columns so I can just go ahead

15
00:00:55,470 --> 00:01:01,410
and type in the class equals call dash and dash six.

16
00:01:02,650 --> 00:01:08,270
And let's see, I use the same thing over here as well for the second column as well.

17
00:01:08,950 --> 00:01:13,320
So now that means we actually spend all the 12 columns provided by Bootstrap.

18
00:01:13,750 --> 00:01:17,580
And now let's go ahead and add the image tag up over here.

19
00:01:17,590 --> 00:01:19,330
So I'll type in image.

20
00:01:19,750 --> 00:01:24,690
And the source for this is going to be nothing but item dot item image.

21
00:01:25,000 --> 00:01:30,250
So just as we have access, the item, name, item, description and item price, I just go ahead and

22
00:01:30,250 --> 00:01:41,830
access the item image as well so I can type in something like item dot item, underscore image and we

23
00:01:41,830 --> 00:01:43,840
don't want an alternative for this.

24
00:01:44,170 --> 00:01:50,970
And let's see, the class for this image is actually card, so I'll type in class equals card so that

25
00:01:50,980 --> 00:01:52,270
this appears as a thumbnail.

26
00:01:52,450 --> 00:01:55,140
And now let's go ahead and tag over here.

27
00:01:55,900 --> 00:02:02,290
And now once we have this image in this particular column right here, we actually want these fields.

28
00:02:02,740 --> 00:02:07,030
So I can simply go ahead, got them from here and paste them right over here.

29
00:02:07,060 --> 00:02:12,480
So now once we go ahead and add them, let's see the code go back over here and hit refresh.

30
00:02:13,060 --> 00:02:19,420
And as you can see, there is certainly some issue with this because we have not set the size of the

31
00:02:19,420 --> 00:02:19,840
image.

32
00:02:20,560 --> 00:02:22,490
So let's add the size of the image.

33
00:02:22,640 --> 00:02:27,880
Let's see, we said the height of this thing to, let's say, 300 pixels.

34
00:02:27,910 --> 00:02:30,160
Let's go back here, hit refresh.

35
00:02:30,160 --> 00:02:34,990
And as you can see now, the detail page appears to be quite better.

36
00:02:35,350 --> 00:02:40,630
So here we have the image on the left hand side and on the right hand side, we have the item name,

37
00:02:40,630 --> 00:02:43,370
the item description and the price as well.

38
00:02:43,930 --> 00:02:49,180
So you can also go ahead and add in a dollar sign over here so that people actually understand that

39
00:02:49,750 --> 00:02:52,160
this is actually the price of the item.

40
00:02:52,180 --> 00:02:55,250
So as you can see now, we have added that up over here.

41
00:02:56,080 --> 00:02:59,910
So that means we have successfully designed the detailed view as well.

42
00:02:59,920 --> 00:03:05,530
And you can go ahead, have a look at the detailed view of any one of those items over here.

43
00:03:05,950 --> 00:03:08,110
And it's going to look absolutely fine.

44
00:03:09,520 --> 00:03:11,180
So that's it for this lecture.

45
00:03:11,260 --> 00:03:15,100
Hopefully, you guys were able to understand how to design the detailed view.

46
00:03:15,430 --> 00:03:21,100
So the only thing which we have done here is that we have simply added some bootstrap classes, which

47
00:03:21,100 --> 00:03:22,090
is the container.

48
00:03:22,090 --> 00:03:23,950
And within the container we have room.

49
00:03:23,980 --> 00:03:29,080
And within that group, we have simply placed two columns over here, the left column and the right

50
00:03:29,080 --> 00:03:31,720
column, which span half half of the width.

51
00:03:32,500 --> 00:03:37,000
And then we have simply added the image and the details of the food over here.

52
00:03:37,180 --> 00:03:38,760
So that's it for this lecture.

53
00:03:38,770 --> 00:03:44,290
And from the next lecture onwards, we will actually get into more interesting concepts like how to

54
00:03:44,290 --> 00:03:47,750
add forms to these particular Web pages.

55
00:03:47,950 --> 00:03:54,520
So let's say, for example, you want to allow users to go ahead and add these items over here.

56
00:03:54,730 --> 00:03:56,410
So how exactly can you do that?

57
00:03:56,470 --> 00:04:02,650
So right now, you can actually go ahead and add items from the admin panel, but the admin panel is

58
00:04:02,650 --> 00:04:04,260
for the site administrator.

59
00:04:04,270 --> 00:04:09,690
And let's assume you also want to allow users to enter, update, delete data.

60
00:04:10,090 --> 00:04:13,540
Then you will need to go ahead and create forms for them.

61
00:04:13,810 --> 00:04:19,600
So in the upcoming lecture, we will learn how to create a form which allows users to add these food

62
00:04:19,600 --> 00:04:20,149
items.

63
00:04:20,620 --> 00:04:24,700
So thank you very much for watching and I'll see you guys next time.

64
00:04:25,000 --> 00:04:25,600
Thank you.

