1
00:00:00,150 --> 00:00:06,180
Hello and welcome to this lecture and in this lecture, we will go ahead and talk something about Class-Based

2
00:00:06,180 --> 00:00:08,850
views on QinetiQ views in general.

3
00:00:09,450 --> 00:00:16,219
So you already know what views actually are and how views are used in Shango so as to go ahead and implement

4
00:00:16,230 --> 00:00:17,060
your functionality.

5
00:00:17,430 --> 00:00:20,800
So if you go to the Web, you start a of the pool.

6
00:00:20,880 --> 00:00:23,520
These are the views which we have created until now.

7
00:00:24,000 --> 00:00:28,860
So as you can see, we have this view called an index, which is going to go ahead and display the list

8
00:00:28,860 --> 00:00:29,580
of items.

9
00:00:29,890 --> 00:00:35,010
We also have this detail over here, which is another view which is going to give the detail of each

10
00:00:35,010 --> 00:00:35,760
and every item.

11
00:00:36,240 --> 00:00:42,830
So if you notice these views, as you can see, we have defined those views as a python function.

12
00:00:42,900 --> 00:00:48,840
So in Python, whenever you want to go ahead and define a function, you just type and then the function

13
00:00:48,840 --> 00:00:54,150
name, then you add the argument list over here and then you write in some code over here and then you

14
00:00:54,150 --> 00:00:54,960
return something.

15
00:00:55,500 --> 00:00:58,690
So this is the typical python function which we use here.

16
00:00:58,740 --> 00:01:06,180
So that means we were writing the triangle views until now, using these Python functions or simply

17
00:01:06,180 --> 00:01:06,960
the functions.

18
00:01:07,110 --> 00:01:12,810
And henceforth, these are views which we have written until now are actually called as the function

19
00:01:12,810 --> 00:01:15,220
we spus now in Shango.

20
00:01:15,270 --> 00:01:19,140
There is another kind of view which is called as the Class-Based views.

21
00:01:19,710 --> 00:01:24,990
So the Class-Based views are nothing but the views which are written in terms of a class.

22
00:01:25,140 --> 00:01:30,540
So instead of having the function here, we can simply go ahead and write in a class for each one of

23
00:01:30,540 --> 00:01:31,960
these views right over here.

24
00:01:32,190 --> 00:01:33,870
So you don't have to write a function.

25
00:01:33,870 --> 00:01:36,180
Instead, you simply go ahead and write a class.

26
00:01:36,180 --> 00:01:42,150
And now using the class based views, you can go ahead and write any kind of view which you want.

27
00:01:42,180 --> 00:01:46,710
So, for example, there are actually multiple class based views which we can use.

28
00:01:46,830 --> 00:01:52,920
So, for example, this particular index P right here can be replaced by something which is called as

29
00:01:52,920 --> 00:01:54,300
the class based list.

30
00:01:54,870 --> 00:01:56,550
So what exactly is a list you.

31
00:01:57,090 --> 00:02:03,090
So a list view is nothing, but it's sort of world class based view provided by Zango, which can be

32
00:02:03,090 --> 00:02:07,180
used to list a particular set of items on your Chanko webapp.

33
00:02:07,500 --> 00:02:13,200
So for example, in this particular index page, we have done exactly the same that as we have listed

34
00:02:13,200 --> 00:02:14,100
the food items.

35
00:02:14,370 --> 00:02:22,260
And henceforth this same view can be actually implemented by Chango Class-Based Peus using something

36
00:02:22,260 --> 00:02:23,430
which is called as a list for.

37
00:02:23,430 --> 00:02:29,700
You know, Django also provides other kinds of view as well, like the detailed view, the create view,

38
00:02:30,180 --> 00:02:32,240
the elite view and everything like that.

39
00:02:32,610 --> 00:02:37,890
So the detailed view is nothing, but it's actually a class based view provided by Django, which can

40
00:02:37,890 --> 00:02:42,150
be used whenever you want to display or list out details of a certain model.

41
00:02:42,750 --> 00:02:48,750
So in this particular function based view over here, that's exactly what the function based view does.

42
00:02:49,020 --> 00:02:53,280
So this function based view actually lists out the detail of a particular item.

43
00:02:53,730 --> 00:03:00,270
So the same functionality which we have implemented using a normal function based view can be implemented

44
00:03:00,270 --> 00:03:03,570
by the genetic triangle view, which is called the little view.

45
00:03:03,750 --> 00:03:09,030
So these are the different kinds of class based or generic views in Django, which you can go ahead

46
00:03:09,030 --> 00:03:09,600
and use.

47
00:03:09,600 --> 00:03:15,150
And we are going to go ahead and make use of one of them over here and see how those class views are

48
00:03:15,150 --> 00:03:15,840
going to work.

49
00:03:16,500 --> 00:03:21,840
Now, even before we learn about Class-Based views, you also need to understand how views or how much

50
00:03:21,840 --> 00:03:23,780
language application in general works.

51
00:03:24,210 --> 00:03:30,690
So in a particular Django app, we have this particular view and this particular view is connected to

52
00:03:30,690 --> 00:03:32,260
the other Stoppie file.

53
00:03:32,430 --> 00:03:37,140
So if you go to the you are a stockpile file of this particular app.

54
00:03:38,390 --> 00:03:44,900
As you can see for the index view, we have this particular path, so every view is connected with the

55
00:03:44,900 --> 00:03:45,340
path.

56
00:03:45,380 --> 00:03:50,410
So the index view, which is the view which we have just seen over here, is connected.

57
00:03:51,320 --> 00:03:57,420
And this particular view is then again rendering a particular template, which is the index dot html

58
00:03:57,440 --> 00:03:57,860
template.

59
00:03:58,340 --> 00:04:01,280
So this is the link which follows in every application.

60
00:04:01,280 --> 00:04:07,550
That is, you have a certain pattern that you all pattern is associated with a view and that view is

61
00:04:07,550 --> 00:04:10,010
associated with a certain kind of template.

62
00:04:10,400 --> 00:04:13,510
So this is the generic flow of any single application.

63
00:04:13,520 --> 00:04:19,010
And whenever you want to write a class based view, you also want to go ahead and follow the same pattern

64
00:04:19,010 --> 00:04:19,440
as well.

65
00:04:19,820 --> 00:04:25,760
That means if you go ahead and create a class based view now, we need to go ahead and we need to connect

66
00:04:25,760 --> 00:04:27,750
that particular view with the other.

67
00:04:27,770 --> 00:04:31,750
And you also need to mention the template in that particular view as well.

68
00:04:32,910 --> 00:04:38,590
So now let's go ahead and let's try to implement the index view as a class based view.

69
00:04:38,970 --> 00:04:44,040
So first of all, we'll go ahead and identify what kind of view is the index view.

70
00:04:44,400 --> 00:04:49,650
So the index view, as we have already discussed this place, nothing but a list of items.

71
00:04:50,260 --> 00:04:55,130
So now that means this can be implemented by using a class based list view.

72
00:04:55,710 --> 00:05:00,510
So what we will do here is that, first of all, will go ahead and import the generic view, which is

73
00:05:00,510 --> 00:05:01,120
the list view.

74
00:05:01,770 --> 00:05:04,350
So we'll type in from Shango.

75
00:05:05,770 --> 00:05:11,140
Not views, not generic, not least import, that's going to be for you.

76
00:05:11,410 --> 00:05:16,040
So once we have this view imported, let's go ahead and create a Class-Based view here.

77
00:05:16,540 --> 00:05:21,340
So just as for the function we use there for the class, we are going to use class.

78
00:05:21,670 --> 00:05:24,400
And in here you can name the CPU as anything.

79
00:05:24,670 --> 00:05:29,170
So I'll just go ahead and name this thing as an index class view.

80
00:05:29,950 --> 00:05:34,690
But now, as I said, this particular view needs to actually be a list view.

81
00:05:34,900 --> 00:05:39,860
So we will go ahead and inherit the properties from the list view by mentioning list view over here.

82
00:05:40,330 --> 00:05:45,220
So that means this particular view, which is the index class view, is nothing but a list view.

83
00:05:45,730 --> 00:05:51,010
Now, the good part about the Class-Based view is that you don't have to actually write in much code

84
00:05:51,010 --> 00:05:51,450
in here.

85
00:05:51,700 --> 00:05:54,880
You simply need to define a few things in the class based view.

86
00:05:55,000 --> 00:05:58,410
And the class based view is going to do a lot of work for you.

87
00:05:59,110 --> 00:06:03,850
Now, in this particular function based view, the first line of code which we have written here is

88
00:06:03,850 --> 00:06:11,110
that we have written the code to extract the items or objects from the item class right over here.

89
00:06:11,170 --> 00:06:14,060
That means we have created this particular model.

90
00:06:14,380 --> 00:06:18,270
That means we have written a query to extract objects from the item model.

91
00:06:18,550 --> 00:06:23,980
But now the Class-Based view, we don't have to do all of these things, but we simply need to go ahead

92
00:06:23,980 --> 00:06:27,070
and mention the name of the model which we are using.

93
00:06:27,220 --> 00:06:29,840
So the model which we are using is nothing but item.

94
00:06:29,860 --> 00:06:34,900
So you simply need to mention that and you don't have to write in the bunch of code which we have written

95
00:06:34,900 --> 00:06:35,580
up over here.

96
00:06:37,330 --> 00:06:42,750
So now let's go ahead and let's move on to the next part, which is to render a template.

97
00:06:42,970 --> 00:06:48,600
So now in case of a function based view, you have to return and render the particular template.

98
00:06:48,880 --> 00:06:53,760
And along with that, you also have to process the request and also present the context.

99
00:06:54,130 --> 00:06:59,680
But in case of Class-Based view, you simply have to go ahead and type in template and the school name

100
00:06:59,710 --> 00:07:06,010
and simply pass in the name of the template, which is in this case would slash index dot.

101
00:07:07,390 --> 00:07:12,410
So as you can see, we have simply copied this name and we have used that name up over here.

102
00:07:12,940 --> 00:07:13,750
So that's it.

103
00:07:14,350 --> 00:07:19,840
So once you're done with this, the only final thing which you need to pass in here is that you need

104
00:07:19,840 --> 00:07:23,590
to go ahead and pass in the context as item list.

105
00:07:24,110 --> 00:07:27,570
So you need to go ahead and define the context object name here.

106
00:07:27,580 --> 00:07:34,360
So you simply need to type in context, object name and you just need to type an item on the school

107
00:07:34,360 --> 00:07:36,790
list over here and you should be good to go.

108
00:07:37,710 --> 00:07:42,160
So this is actually the replacement for this view right here.

109
00:07:42,210 --> 00:07:47,820
So instead of writing this whole bunch of good and in a function based view, in a class based view,

110
00:07:47,820 --> 00:07:50,850
you only need to write in this many lines of code here.

111
00:07:51,360 --> 00:07:52,860
So now you can save the code.

112
00:07:53,100 --> 00:07:58,770
And now the final thing which we need to do is that now we need to go ahead and attach this view with

113
00:07:58,770 --> 00:08:04,920
the URL so that whenever we visit that particular you are we will actually be using the Class-Based

114
00:08:04,920 --> 00:08:05,220
view.

115
00:08:05,370 --> 00:08:12,210
So let's go to the URL stockpile and in here, instead of having the name as you start, index will

116
00:08:12,210 --> 00:08:14,200
simply use this name right there.

117
00:08:15,000 --> 00:08:19,340
So let's go ahead and type and views dot index class view.

118
00:08:19,710 --> 00:08:24,180
And now as this is a class based view, you need to type and not as view.

119
00:08:25,690 --> 00:08:31,120
So once we go ahead and see if that could work and Shango does, is that possible, we visit the park,

120
00:08:31,160 --> 00:08:36,850
which is argued now it will be using the index glass view, which is nothing but the list view.

121
00:08:37,510 --> 00:08:40,309
So now let's make sure this thing works fine.

122
00:08:40,690 --> 00:08:46,720
So let me just open up my proposal and now let's actually go to Slash Who.

123
00:08:46,990 --> 00:08:53,380
So when I go to slash food, as you can see, this thing still works fine, even if we have replaced

124
00:08:53,440 --> 00:08:55,810
the function based view with a Class-Based view.

125
00:08:56,770 --> 00:09:02,800
So that's how Class-Based view actually work, and we were successful in implementing the Class-Based

126
00:09:02,800 --> 00:09:08,580
view, and the view which we have used here is that we have used the list view over here.

127
00:09:09,010 --> 00:09:13,040
Now, similar to the list view, we can also create the detailed view as well.

128
00:09:13,480 --> 00:09:20,140
So in the next lecture, what we will do is that we will go ahead and will try to recreate this particular

129
00:09:20,140 --> 00:09:23,840
view, which is detail in the form of a class based on detailed view.

130
00:09:24,340 --> 00:09:28,240
So thank you very much for watching and I'll see you guys next time.

131
00:09:28,810 --> 00:09:29,380
Thank you.

