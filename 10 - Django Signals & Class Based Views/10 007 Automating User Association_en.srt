1
00:00:00,680 --> 00:00:07,700
Hello and welcome to this lecture and in this lecture, let's go ahead and automate the process of when

2
00:00:07,700 --> 00:00:14,390
the user actually fills up this particular form right here, the profile of the user feel should be

3
00:00:14,390 --> 00:00:16,940
automatically updated with the logged in user.

4
00:00:17,180 --> 00:00:21,290
So right now, we have these three fields of oil fields which we need to fill up.

5
00:00:21,620 --> 00:00:26,930
And one more field, which is actually going to be present over here, is going to be the user name.

6
00:00:26,930 --> 00:00:31,600
And we actually want to get the user name of the user who's currently logged in.

7
00:00:32,180 --> 00:00:34,890
So let's go ahead and learn how we can implement that.

8
00:00:35,480 --> 00:00:41,510
So the very first thing which we need to do now is that we need to convert the current function based

9
00:00:41,510 --> 00:00:44,480
view for adding item into a Class-Based view.

10
00:00:45,020 --> 00:00:50,180
So right now we have this particular view right here, which is called as the create item view.

11
00:00:50,480 --> 00:00:52,580
And this is actually a function based view.

12
00:00:52,850 --> 00:00:57,710
And let's now go ahead and turn it into a Class-Based view and see what happens.

13
00:00:58,190 --> 00:01:03,420
So what I will do here is that I'll simply go ahead and create a class based view over here.

14
00:01:03,530 --> 00:01:10,370
So let me just add a comment in here and let's say this is a class based view.

15
00:01:11,990 --> 00:01:17,690
For each item, OK, now, once we have this thing, the first thing which we need to do now is that

16
00:01:17,690 --> 00:01:22,660
we need to go ahead and import the create view as we want to create a Class-Based view.

17
00:01:23,090 --> 00:01:27,390
So let's go right here and just open from Chengdu.

18
00:01:27,400 --> 00:01:34,340
Luchador use dot generic dot edit that's going to be import.

19
00:01:34,770 --> 00:01:36,420
That's going to be created.

20
00:01:37,280 --> 00:01:43,520
So once we have this particular class now we can go ahead and inherit from this class in the Class-Based

21
00:01:43,520 --> 00:01:43,790
view.

22
00:01:44,210 --> 00:01:49,230
So let's go back over here and now let's start creating the Class-Based view over here.

23
00:01:49,370 --> 00:01:57,590
So I'll go ahead and type in class and let's say the name of this view is going to be create item.

24
00:01:58,070 --> 00:02:04,070
And now let's go ahead and type in create view in here so as to inherit from the creative class, which

25
00:02:04,070 --> 00:02:05,150
we have just imported.

26
00:02:05,150 --> 00:02:10,789
Now, in case of this create view, which is the Class-Based view, what we need to do is that we need

27
00:02:10,789 --> 00:02:11,810
to convert this.

28
00:02:12,860 --> 00:02:14,990
Function based view into a class based view.

29
00:02:15,020 --> 00:02:20,360
So now we need to write a class based view for this existing function based view, which we have here.

30
00:02:20,600 --> 00:02:26,620
So here what we need to do is that we need to actually go ahead and specify the model here.

31
00:02:26,990 --> 00:02:33,290
So I'll type and the model is going to be Eitam because we actually want to credit the item and then

32
00:02:33,440 --> 00:02:37,340
we need to go ahead and also specify the fields which we want to use.

33
00:02:37,610 --> 00:02:43,430
So for the item field, we actually want to use the fields, which are nothing but the item name, the

34
00:02:43,430 --> 00:02:45,550
item description and everything like that.

35
00:02:45,680 --> 00:02:52,550
So I'll mention fields over here and those are going to be nothing but the item name.

36
00:02:53,780 --> 00:02:56,150
Then we are going to have the item.

37
00:02:57,590 --> 00:02:59,750
Description the.

38
00:03:00,840 --> 00:03:08,100
Item price, and finally, we will have the item image, so type an item and school image over here

39
00:03:08,100 --> 00:03:13,020
and do note that these fields are nothing but the fields which we actually have in our models, not

40
00:03:13,020 --> 00:03:15,490
be verified, which is these items right here.

41
00:03:15,720 --> 00:03:22,400
But as you can see, we have not included this username field here because we don't want that field

42
00:03:22,410 --> 00:03:23,920
to be present in our form.

43
00:03:24,090 --> 00:03:29,850
So here we only specify those fields which we want them to be present in our form.

44
00:03:30,180 --> 00:03:34,040
And the user name field actually has to be updated automatically.

45
00:03:34,050 --> 00:03:36,880
And that's the reason why we are not including it over here.

46
00:03:36,900 --> 00:03:42,460
So once we are done with this, let's now go ahead and create a function in here to actually accept

47
00:03:42,480 --> 00:03:42,900
a form.

48
00:03:43,140 --> 00:03:50,040
So let's name that function as, let's say, form valid soil type and form underscore valid.

49
00:03:50,820 --> 00:03:53,650
And this is actually going to accept two parameters.

50
00:03:53,700 --> 00:03:57,090
One is the self and then the form itself.

51
00:03:57,570 --> 00:04:04,320
And now what we will do here is that once we have the form inside this function, what we will do is

52
00:04:04,320 --> 00:04:08,030
that we will try to get the feel from that particular form.

53
00:04:08,430 --> 00:04:12,640
So the form feel we are trying to access here is the user name.

54
00:04:12,960 --> 00:04:20,100
So in order to get the user name, feel for the form you type in form, which is nothing but this form

55
00:04:20,100 --> 00:04:24,020
right here and now, you need to get the instance of that form.

56
00:04:24,030 --> 00:04:25,770
So you type in that instance.

57
00:04:25,980 --> 00:04:32,280
And now for the for instance, you want the user name, soil type and user and the school name over

58
00:04:32,280 --> 00:04:32,570
here.

59
00:04:33,090 --> 00:04:39,730
And now we actually want to create this user name with the name of the user who's currently logged in.

60
00:04:40,170 --> 00:04:46,260
So in order to get the name of the current user, what we do is that we use the self which we have passed

61
00:04:46,260 --> 00:04:46,860
in over here.

62
00:04:46,860 --> 00:04:51,720
So we'll type in self and we get the user name from the request.

63
00:04:51,840 --> 00:04:55,490
So we type in self-taught requests, DOT, that's going to be user.

64
00:04:55,950 --> 00:05:02,250
And once we get this, what we do is that we return the super method, which is the form underscore

65
00:05:02,250 --> 00:05:02,690
valid.

66
00:05:03,180 --> 00:05:11,130
So we type in return super and then we type and dot form underscore valid.

67
00:05:11,520 --> 00:05:14,310
And in here we again pass the same thing which is form.

68
00:05:15,280 --> 00:05:21,340
And then we also need to go ahead and mention the template for this particular form, which is nothing

69
00:05:21,340 --> 00:05:27,980
but the template, which we have Boston here, which is food slash item, dash form, dot HTML.

70
00:05:28,390 --> 00:05:29,740
So let me just copy that.

71
00:05:29,920 --> 00:05:35,170
And in here, right inside the glass, we actually need to mention the template name.

72
00:05:35,650 --> 00:05:41,110
So the template name is going to be nothing but this thing right here, which we have copied just right

73
00:05:41,110 --> 00:05:41,420
now.

74
00:05:42,250 --> 00:05:47,950
OK, so once this thing is done, hopefully our application should work just fine and we should get

75
00:05:47,950 --> 00:05:50,350
the user associated with the post.

76
00:05:50,380 --> 00:05:55,330
OK, so now once we are done with creating the CPU now, the only thing which we need to do is that

77
00:05:55,570 --> 00:05:59,690
we need to go ahead and change the URL pattern of our website.

78
00:05:59,710 --> 00:06:04,200
So let's go over here and in here instead of the create item.

79
00:06:04,210 --> 00:06:08,080
You know, we need to change this thing to create item dot as view.

80
00:06:08,230 --> 00:06:12,400
So that is going to be used to create item dot.

81
00:06:12,400 --> 00:06:17,080
As for you, and once we go ahead and do that, we are pretty much good to go.

82
00:06:17,110 --> 00:06:21,760
So now once we have added that, let's go ahead and make sure the user was up and running.

83
00:06:21,820 --> 00:06:23,920
So right now I'm logged in as admin.

84
00:06:23,920 --> 00:06:29,550
So let me just log out and let me log in as a regular user.

85
00:06:29,680 --> 00:06:34,900
Let's see, sample user, which is an already registered user on my site.

86
00:06:37,070 --> 00:06:44,990
And now let's go ahead and try to add an item and let's say the item name is simple item, let's say

87
00:06:44,990 --> 00:06:47,900
the description is sample description.

88
00:06:47,900 --> 00:06:49,790
Let's say the price is 55.

89
00:06:50,450 --> 00:06:56,710
And now when I go ahead and click save, as you can see now, the item has been added up over here.

90
00:06:57,140 --> 00:07:03,470
And now if we want to check what kind of user is associated with this sample item, we need to log in

91
00:07:03,470 --> 00:07:04,300
as an admin.

92
00:07:04,400 --> 00:07:07,460
So let me just log out and log in as an admin.

93
00:07:08,390 --> 00:07:10,810
So let me just type in my name here.

94
00:07:14,840 --> 00:07:21,650
And now if we go to items and if we go to the sample item over here, as you can see now, the user

95
00:07:21,650 --> 00:07:23,910
name is automatically updated over here.

96
00:07:23,930 --> 00:07:30,270
Our sample user, because the sample user was the one who had actually added up that item.

97
00:07:30,740 --> 00:07:36,170
Now, in order to test if this thing actually works, what you can do is that you can again go back

98
00:07:36,590 --> 00:07:43,200
and you can just log out and register a new user so I can just go ahead and register a new user.

99
00:07:43,220 --> 00:07:46,720
So let me just type in register and let's see.

100
00:07:46,730 --> 00:07:52,030
The new user name is going to be a brand new user.

101
00:07:52,610 --> 00:07:57,430
Let's say the email is new at G.M. dot com.

102
00:07:57,770 --> 00:08:01,040
Let's say the password is test test.

103
00:08:01,130 --> 00:08:03,410
Four, five, six test.

104
00:08:04,750 --> 00:08:07,360
This four, five, six, and now let's log in.

105
00:08:08,810 --> 00:08:17,090
So brand new user password is, again, this there's four, five, six, and now let's go ahead and

106
00:08:17,090 --> 00:08:18,400
try to add an item.

107
00:08:18,410 --> 00:08:23,150
Let's say the item name is a brand new item.

108
00:08:24,270 --> 00:08:29,670
Let's say the description is something and let's say the price is forty three dollars.

109
00:08:30,170 --> 00:08:34,289
So when I go ahead and click save, the item is actually added up over here.

110
00:08:34,429 --> 00:08:36,260
And now let's go ahead logout.

111
00:08:36,870 --> 00:08:39,490
Let's log in again as an admin.

112
00:08:39,500 --> 00:08:45,080
And now if we go to items, as you can see, we have a brand new item which is actually added up by

113
00:08:45,080 --> 00:08:46,440
the brand new user.

114
00:08:46,940 --> 00:08:53,660
So that means we have successfully completed the process of actually adding or actually associating

115
00:08:53,990 --> 00:09:00,560
a particular user name with a particular post so that we know what kind of post is actually posted by

116
00:09:00,560 --> 00:09:01,700
what kind of user.

117
00:09:02,180 --> 00:09:08,010
So now one more quantification you can make to this particular code here is that you can go to the index

118
00:09:08,030 --> 00:09:15,260
dot html over here and you can actually have the name of the user listed up over here as well.

119
00:09:15,740 --> 00:09:22,250
So what you can do here is that you can go ahead and you can also display the name of the user who has

120
00:09:22,250 --> 00:09:23,960
posted this particular item.

121
00:09:24,410 --> 00:09:26,740
So now let's go ahead and try to do that.

122
00:09:26,750 --> 00:09:30,000
So I'll just go ahead and type in another field over here.

123
00:09:30,020 --> 00:09:34,550
Let's see, it's six and now I can just type something in over here.

124
00:09:34,550 --> 00:09:36,950
Like, let's see the.

125
00:09:38,160 --> 00:09:38,820
Item.

126
00:09:39,860 --> 00:09:41,160
Dot user.

127
00:09:41,180 --> 00:09:46,850
And this should actually be using the school name and now when you go ahead and hit refresh, as you

128
00:09:46,850 --> 00:09:51,500
can see now, you have all those items along with the detailed description.

129
00:09:51,500 --> 00:09:57,410
And also we have the name of the user who have actually posted those items as well.

130
00:09:57,860 --> 00:09:59,630
So here we have brand new user.

131
00:09:59,630 --> 00:10:01,220
Here we have sample user.

132
00:10:01,220 --> 00:10:06,730
And here for the rest of the items which did not have any user, we actually have the admin user.

133
00:10:06,860 --> 00:10:08,590
So that's it for this lecture.

134
00:10:08,720 --> 00:10:14,370
And I hope you guys were able to understand how to associate a user with a particular food item.

135
00:10:14,930 --> 00:10:16,870
So thank you very much for watching.

136
00:10:16,880 --> 00:10:23,660
And in the upcoming lecture will go ahead and try to actually make our site a little bit more attractive

137
00:10:23,660 --> 00:10:25,700
by making a few design changes.

138
00:10:25,910 --> 00:10:27,830
So thank you very much for watching.

139
00:10:27,860 --> 00:10:29,930
And I'll see you guys next time.

140
00:10:30,380 --> 00:10:30,980
Thank you.

