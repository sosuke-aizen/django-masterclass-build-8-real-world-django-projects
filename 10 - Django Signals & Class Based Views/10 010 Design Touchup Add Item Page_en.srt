1
00:00:01,250 --> 00:00:06,770
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to style this

2
00:00:06,770 --> 00:00:09,220
particular page, which is the food dash.

3
00:00:09,230 --> 00:00:15,920
And so what style in this particular template, you don't have to do much of anything if you go to the

4
00:00:15,920 --> 00:00:16,730
page right now.

5
00:00:16,730 --> 00:00:22,410
As you can see, we have already extended this particular template with the basic template over here.

6
00:00:22,760 --> 00:00:27,420
So the only thing which you need to do is that you need to add in a few bootstrap classes in here.

7
00:00:27,860 --> 00:00:32,450
Now, as a homework, what you can do is that you can try to design this page on your own.

8
00:00:32,720 --> 00:00:38,820
So please kindly post this video right now and try to see if you can implement that on your own.

9
00:00:39,470 --> 00:00:44,010
So was this video and just go through the challenge and see if it works.

10
00:00:45,230 --> 00:00:48,820
OK, so let's go through the solution of this challenge anyway.

11
00:00:48,980 --> 00:00:55,310
What you simply need to do here is that you need to use the exact same syntax which we have used in

12
00:00:55,310 --> 00:01:02,150
case of the register dot H.M. So for now you can just simply go ahead, copy all the entire code in

13
00:01:02,150 --> 00:01:02,420
here.

14
00:01:03,140 --> 00:01:04,220
Go into your.

15
00:01:05,510 --> 00:01:06,710
Item dash.

16
00:01:07,610 --> 00:01:14,960
Form dot html template pasted up over here, and the only thing which you need to change here is that

17
00:01:15,050 --> 00:01:19,550
now you don't necessarily need to access each one of those fields individually.

18
00:01:19,550 --> 00:01:23,900
So you can just go ahead and get rid of this code right here, which is the follow up.

19
00:01:24,110 --> 00:01:29,780
And instead, you can simply use the regular syntax for the form so you can type in form in here.

20
00:01:30,740 --> 00:01:33,200
And instead of sign up, you can type in add.

21
00:01:33,530 --> 00:01:39,470
And once you go ahead and do that and now if you go back, hit refresh, as you can see, you now have

22
00:01:39,470 --> 00:01:40,810
your form right over here.

23
00:01:41,030 --> 00:01:45,530
And now what you can also do is that you can just go ahead.

24
00:01:47,650 --> 00:01:53,270
And change this thing up, so instead of a register, they should actually see an item.

25
00:01:53,500 --> 00:01:57,090
So let's change this thing to add item.

26
00:01:57,100 --> 00:02:01,360
And now if you go back, it refresh, as you can see, it is add item.

27
00:02:02,300 --> 00:02:07,550
So that's it for this lecture, and I hope you guys be able to understand how the style, different

28
00:02:07,550 --> 00:02:10,490
kinds of forms and Shango using bootstrap.

29
00:02:11,120 --> 00:02:12,920
So that's it for this lecture.

30
00:02:12,920 --> 00:02:14,930
And I'll see you guys in the next one.

31
00:02:15,440 --> 00:02:16,040
Thank you.

