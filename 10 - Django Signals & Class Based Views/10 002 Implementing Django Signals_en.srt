1
00:00:00,150 --> 00:00:05,970
So now once we know what Django signals actually are, let's go ahead and start implementing Django

2
00:00:05,970 --> 00:00:06,570
Signals.

3
00:00:06,930 --> 00:00:12,240
So in order to start using Django signals, first of all, you need to go ahead, go into your users

4
00:00:12,240 --> 00:00:16,180
app and you need to create a file which is called as signal stop.

5
00:00:16,890 --> 00:00:21,090
So let's just go ahead and create a new file and let's name this thing as.

6
00:00:22,380 --> 00:00:24,330
Signal start by.

7
00:00:25,490 --> 00:00:31,880
So once you have this file, you are good to go, and now you will create a signal which is call as

8
00:00:31,880 --> 00:00:33,360
post safe signal.

9
00:00:33,770 --> 00:00:40,460
So what the perceived signal is going to do is that whenever this safe method of this particular form

10
00:00:40,460 --> 00:00:44,310
is going to be executed, it's going to fire up that particular signal.

11
00:00:44,690 --> 00:00:50,480
And what that's going to do is that it's going to tell us whenever this particular form dot safe method

12
00:00:50,480 --> 00:00:55,700
is executed, which means that we will get a signal whenever the user is actually registered.

13
00:00:55,700 --> 00:01:01,490
And using that particular signal, we can know that when the user is registered and depending upon that,

14
00:01:01,490 --> 00:01:03,800
we can go ahead and create the user profile.

15
00:01:04,280 --> 00:01:09,290
So now let's go ahead and first of all, import the signal, which is post safe.

16
00:01:09,590 --> 00:01:18,590
So in order to import that, you type from Shango dot dbe dot models, dot signals import, that's going

17
00:01:18,590 --> 00:01:19,910
to be post safe.

18
00:01:19,940 --> 00:01:26,870
So as you can see the other bunch of signals as well, which is post a delete post in it and post migrate,

19
00:01:27,110 --> 00:01:29,230
which can be used for different purposes.

20
00:01:29,290 --> 00:01:35,480
So for now we will use post safe because we want to get a signal when the user data is saved or when

21
00:01:35,480 --> 00:01:37,310
the user is actually registered.

22
00:01:37,670 --> 00:01:44,000
OK, now once this thing is done, what we want to do is that whenever the user object is saved, we

23
00:01:44,000 --> 00:01:49,340
will get the signal, meaning that we also need to input the user model as well.

24
00:01:49,760 --> 00:01:56,320
So we'll go ahead and type in from Shango dot cantrip, dot, dot, dot.

25
00:01:57,170 --> 00:01:59,990
That's going to be models input.

26
00:02:00,410 --> 00:02:02,060
That's going to be the user model.

27
00:02:02,300 --> 00:02:09,289
So now once we put the user model, we are good to go now as we are sending the post and the see a signal.

28
00:02:09,590 --> 00:02:14,120
We also want to have a receiver which is going to receive those particular signals.

29
00:02:14,460 --> 00:02:18,470
Now, in order to do that, we first need to go ahead and import the receiver.

30
00:02:18,800 --> 00:02:21,080
So we'll type in from Django.

31
00:02:22,350 --> 00:02:26,400
DOT dispatch that's going to be import receiver.

32
00:02:26,940 --> 00:02:32,510
So what does receiver will do is that it will receive the same signal and it will perform some action

33
00:02:32,730 --> 00:02:38,790
and in our case, the action is nothing, but it's to basically go ahead and create a profile for that

34
00:02:38,790 --> 00:02:39,750
particular user.

35
00:02:40,560 --> 00:02:43,560
So I hope you get the flow of this particular code.

36
00:02:43,560 --> 00:02:49,950
That is, first of all, we go ahead and we use a signal which is going to be fired up whenever we go

37
00:02:49,950 --> 00:02:56,240
ahead and register user and the user registration process is complete, then we go ahead and we use

38
00:02:56,250 --> 00:02:58,730
a receiver to receive that particular signal.

39
00:02:58,920 --> 00:03:02,890
And when the receiver receives that particular signal, we perform some action.

40
00:03:03,030 --> 00:03:09,360
Now, once we go ahead and have the receiver as well, and when the receiver receives a particular signal,

41
00:03:09,570 --> 00:03:12,610
it's actually going to go ahead and create the profile.

42
00:03:13,050 --> 00:03:16,320
So we will also go ahead and import the profile model as well.

43
00:03:16,620 --> 00:03:21,730
So the profile model is actually present in the model star profile, which is this file right here.

44
00:03:21,750 --> 00:03:23,990
So as you can see, this is the profile model.

45
00:03:24,360 --> 00:03:26,770
So let's go ahead and import that over here.

46
00:03:26,880 --> 00:03:32,580
So let's go back to the signal start profile, which is this file right here, and let's go ahead and

47
00:03:32,590 --> 00:03:33,240
import that.

48
00:03:33,240 --> 00:03:37,590
So from DOT models import, that's going to be profile.

49
00:03:38,960 --> 00:03:43,970
OK, so once this thing is done now, let's actually writing the code, which will actually implement

50
00:03:43,970 --> 00:03:45,300
all of this functionality.

51
00:03:45,650 --> 00:03:50,650
So first of all, what we will do is that will write in a function to build a profile.

52
00:03:51,020 --> 00:03:55,740
So we will type in Dev, let's say, Bill, and the skill profile.

53
00:03:56,210 --> 00:04:00,660
So this is actually a method of function which is going to build up our profile.

54
00:04:01,010 --> 00:04:03,830
So this is going to have multiple parameters over here.

55
00:04:04,190 --> 00:04:09,340
So first of all, we will have ascender over here, which is nothing, but which sends the signal.

56
00:04:09,860 --> 00:04:11,540
So we'll type and Sando.

57
00:04:12,170 --> 00:04:17,570
And then the next parameter, which we have here is we have the instance.

58
00:04:18,709 --> 00:04:25,580
So instance here is nothing, but it's the instance which is being sealed, so in this case, the user

59
00:04:25,580 --> 00:04:29,570
which is being saved is nothing but the instance, which is this user right here.

60
00:04:29,810 --> 00:04:33,200
So what we do here is that we go ahead and have a form here.

61
00:04:33,560 --> 00:04:39,170
And whenever the user enters his credentials in the form, the user is saved in a particular instance.

62
00:04:39,410 --> 00:04:42,080
And that instance is nothing but this thing right here.

63
00:04:43,150 --> 00:04:48,940
So now, once we have instance, we are also going to have a parameter here, which is called created,

64
00:04:49,390 --> 00:04:55,780
and this basically is going to hold a boolean value, which is going to tell us if the user is created

65
00:04:55,780 --> 00:04:56,300
or not.

66
00:04:56,320 --> 00:04:58,740
So it basically holds the status of the user.

67
00:04:59,050 --> 00:05:03,400
If the user is created, it's going to see a crew of the user is not created.

68
00:05:03,400 --> 00:05:04,630
It's going to be false.

69
00:05:05,320 --> 00:05:11,440
OK, so now once we have this thing, let's go ahead and pass in the final parameter, which is what

70
00:05:11,710 --> 00:05:13,060
the quarks as nothing.

71
00:05:13,060 --> 00:05:15,500
But it basically means he would argument.

72
00:05:15,790 --> 00:05:20,830
So if you have any additional arguments, you had arguments which you want to pass here, you can go

73
00:05:20,830 --> 00:05:22,660
ahead and do that using quarks.

74
00:05:22,900 --> 00:05:25,900
OK, so now let's go ahead and write in some code in here.

75
00:05:26,590 --> 00:05:31,630
So this bell profile function is actually going to create the profile object for us.

76
00:05:31,990 --> 00:05:37,870
So that means if the user is actually registered, we want to go ahead and create a profile for that

77
00:05:37,870 --> 00:05:38,290
user.

78
00:05:38,590 --> 00:05:43,500
So what we do is that we make use of this variable which is created.

79
00:05:43,510 --> 00:05:45,640
So we check if the user is created.

80
00:05:45,640 --> 00:05:52,960
So we type and if it means that we want to go ahead and we want to create the profile object.

81
00:05:52,960 --> 00:05:56,200
So we type in profile dot objects dot.

82
00:05:57,830 --> 00:05:58,430
Create.

83
00:05:59,970 --> 00:06:07,080
And to this, we are going to pass in this particular instance right here as user, so dipen user equals,

84
00:06:07,260 --> 00:06:09,930
that's going to be nothing but the instance.

85
00:06:11,280 --> 00:06:17,250
So now what this thing basically does is that it goes ahead and creates the profile object for us.

86
00:06:17,670 --> 00:06:24,210
Now, once we have this function, that's all fine and good, but when exactly do we know that we want

87
00:06:24,210 --> 00:06:26,090
to fire or call this function?

88
00:06:26,430 --> 00:06:30,650
So we actually want to fire this function when the user profile is created.

89
00:06:31,110 --> 00:06:34,650
But how exactly do we know that the user profile is created?

90
00:06:34,800 --> 00:06:37,680
We can know that by using this particular signal right here.

91
00:06:38,100 --> 00:06:42,270
So now you want to go ahead and receive this post underscores the signal.

92
00:06:42,540 --> 00:06:46,390
And upon receiving that signal, we actually want to execute this function.

93
00:06:46,890 --> 00:06:48,630
So how exactly can we do that?

94
00:06:48,870 --> 00:06:55,500
So we can do that by adding a receiver over here as a decorator to this particular function so I can

95
00:06:55,500 --> 00:06:58,220
just go ahead and type in and receive receiver.

96
00:06:58,680 --> 00:07:03,540
And in here I will make this receiver receive the post and let's go see a signal.

97
00:07:03,930 --> 00:07:05,940
So I'll type in post and let's go save.

98
00:07:06,330 --> 00:07:09,900
And we also need to name the sender for the signal as well.

99
00:07:10,230 --> 00:07:15,530
So the sender in this case is nothing but a user.

100
00:07:15,540 --> 00:07:17,170
So I'll type in user over here.

101
00:07:17,580 --> 00:07:23,820
So that means now this function is going to be called whenever it receives the post receives signal

102
00:07:23,820 --> 00:07:25,680
from the sender who is user.

103
00:07:26,610 --> 00:07:28,110
So I hope that's clear to you.

104
00:07:28,680 --> 00:07:34,620
And now let's move on to the next part, which is to basically go ahead and save the profile.

105
00:07:35,220 --> 00:07:39,960
So now whenever you want to save the profile, you write another function here, which is nothing but

106
00:07:39,960 --> 00:07:42,270
death, save and let's go profile.

107
00:07:42,840 --> 00:07:46,080
And this is basically going to go ahead and save your profile.

108
00:07:46,200 --> 00:07:51,540
So it's, again, going to have the same parameter, which is the sender is going to have the instance

109
00:07:51,840 --> 00:07:56,640
and then it's not going to have the created argument, but it's going to have the Cuonzo.

110
00:07:57,600 --> 00:08:05,190
So Dipen walks over here and now let's go ahead and S�vres the actual instance to the profile.

111
00:08:05,490 --> 00:08:09,300
So I'll type an instance, not profile.

112
00:08:11,010 --> 00:08:11,930
Not safe.

113
00:08:12,510 --> 00:08:15,480
So once that thing is done, we are pretty much good to go.

114
00:08:16,140 --> 00:08:20,730
This will actually save the profile, but this thing also needs to receive a signal.

115
00:08:21,040 --> 00:08:25,570
So I'll just go ahead and copy this receiver from here and pasted over here as well.

116
00:08:26,160 --> 00:08:29,890
And now, once we go ahead and do that, we are pretty much good to go.

117
00:08:30,780 --> 00:08:36,720
Now, the final thing which we need to do here is that we also need to go ahead and import these signals,

118
00:08:36,720 --> 00:08:37,650
which we have used.

119
00:08:38,220 --> 00:08:44,910
So in order to do that, what we do is that we go to the APSA profile and in this particular class,

120
00:08:45,270 --> 00:08:49,470
which is the user config, we simply add a configuration for user signals.

121
00:08:50,490 --> 00:08:52,440
So you type in def ready.

122
00:08:54,170 --> 00:09:01,970
And just passing the parameter as self and in here we simply type in import users.

123
00:09:03,500 --> 00:09:04,010
Dorte.

124
00:09:04,970 --> 00:09:05,700
Signals.

125
00:09:06,080 --> 00:09:09,530
So once we go ahead and do that, we are pretty much good to go.

126
00:09:10,250 --> 00:09:15,860
So once everything is done, just make sure to save your code and let's make sure that your server is

127
00:09:15,860 --> 00:09:16,480
up and running.

128
00:09:17,030 --> 00:09:21,620
And now let's simply go ahead and see if our logic actually works.

129
00:09:22,520 --> 00:09:25,340
So now let me just go to the.

130
00:09:26,290 --> 00:09:33,940
Register page, and in here, let us try to register a new user and let's name this thing as a sample

131
00:09:34,120 --> 00:09:34,820
user.

132
00:09:35,410 --> 00:09:38,010
Let's say the email sample at Gmail dot com.

133
00:09:38,380 --> 00:09:42,220
Let's see, the password is test test four, five, six.

134
00:09:44,180 --> 00:09:47,000
Test, test, four, five, six, seven.

135
00:09:47,070 --> 00:09:48,020
Click on Sign Up.

136
00:09:48,980 --> 00:09:51,060
As you can see, the user is now signed up.

137
00:09:51,080 --> 00:09:52,460
And now let's try to login.

138
00:09:52,880 --> 00:09:54,260
So the user name is.

139
00:09:55,450 --> 00:10:00,910
Sample user password is a test test for five six.

140
00:10:01,630 --> 00:10:07,360
So when I click on login, as you can see now, we are logged in and now when I go to profile, as you

141
00:10:07,360 --> 00:10:12,350
can see, the profile of this particular user is now automatically created.

142
00:10:12,880 --> 00:10:19,010
So that means now we don't have to manually go ahead and create the profile for every registered user.

143
00:10:19,330 --> 00:10:25,450
But now the process of creating the profile for every registered user is now being automated.

144
00:10:25,870 --> 00:10:28,750
And that can be done by using Django signals.

145
00:10:28,750 --> 00:10:33,340
And the signal which we have used in this case was the post signal.

146
00:10:33,760 --> 00:10:39,070
So the only thing which we have done here is that we have used the build profile function, that we

147
00:10:39,070 --> 00:10:45,190
have created a function which is just going to go ahead and create the profile object using the user

148
00:10:45,190 --> 00:10:45,730
instance.

149
00:10:46,030 --> 00:10:51,670
And then we have simply saved this particular profile using the profile instance and we have just made

150
00:10:51,670 --> 00:10:54,430
use of the post and the scores of signal over here.

151
00:10:54,790 --> 00:10:57,790
We have added a receiver here to receive that signal.

152
00:10:58,000 --> 00:11:03,110
And whenever we actually receive that signal, we have just executed these two functions.

153
00:11:03,700 --> 00:11:05,330
So that's it for this lecture.

154
00:11:05,770 --> 00:11:12,430
Hopefully you guys be able to understand what Azango signals and how they can be useful in performing

155
00:11:12,430 --> 00:11:15,820
some sort of an action when some other action has already occurred.

156
00:11:16,300 --> 00:11:20,230
So thank you very much for watching and I'll see you guys next time.

157
00:11:20,770 --> 00:11:21,340
Thank you.

