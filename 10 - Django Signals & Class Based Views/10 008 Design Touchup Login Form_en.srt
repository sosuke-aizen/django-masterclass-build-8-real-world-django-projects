1
00:00:00,690 --> 00:00:06,210
Hello and welcome to this lecture and in this lecture, let's go ahead and let's try to design our site

2
00:00:06,210 --> 00:00:07,330
in a much more better way.

3
00:00:07,800 --> 00:00:13,350
So right now, if you look at the look and feel of our website, it actually looks quite outdated because

4
00:00:13,350 --> 00:00:18,210
if you have a look at the login page, uh, login page actually looks pretty boring.

5
00:00:18,630 --> 00:00:22,950
So what we will do here is that we'll try to improve the design of our website.

6
00:00:22,950 --> 00:00:25,740
And I know that this stuff is not Shango related.

7
00:00:26,050 --> 00:00:29,070
Uh, it actually requires some bootstrap skills to do so.

8
00:00:29,400 --> 00:00:35,760
But we are going to do that anyways so that we know how to at least tile our Django website in a decent

9
00:00:35,760 --> 00:00:36,150
manner.

10
00:00:36,660 --> 00:00:38,550
So let's go ahead and get started.

11
00:00:39,210 --> 00:00:45,290
So what we will do is that we'll start off with designing the login page in a much more better way.

12
00:00:45,480 --> 00:00:51,030
We will go to the login page, which is actually present, and our users up and in here, instead of

13
00:00:51,030 --> 00:00:56,490
simply having this form, will actually go ahead and write and proper code so that this page actually

14
00:00:56,490 --> 00:00:57,210
looks better.

15
00:00:58,050 --> 00:01:01,290
So for now, what we will do, we will start off from the start.

16
00:01:01,440 --> 00:01:07,980
And first of all, you need to make this page extend the base template so that it can actually use the

17
00:01:07,980 --> 00:01:08,910
bootstrap theme.

18
00:01:09,180 --> 00:01:14,160
So the base template is nothing but the template, which is actually present in our food app.

19
00:01:14,310 --> 00:01:17,950
So what you can simply do is that you can extend from that template.

20
00:01:18,450 --> 00:01:23,260
So here, let's go ahead and use the regular syntax for extending from a template.

21
00:01:23,280 --> 00:01:32,160
So we will type extents and we want to extend from mood slash based on each jemal.

22
00:01:32,550 --> 00:01:38,930
So now once we are done with this, let's actually go ahead and have the, as usual, body and block

23
00:01:38,940 --> 00:01:39,420
codes.

24
00:01:39,570 --> 00:01:45,480
So whenever you are extending from somewhere, you also need to have the log body and block tags over

25
00:01:45,480 --> 00:01:45,770
here.

26
00:01:46,200 --> 00:01:52,180
So I'll type and log what you hear stating that the body of our page starts from here.

27
00:01:52,530 --> 00:01:54,210
So that's going to be bloody.

28
00:01:54,810 --> 00:01:59,380
And now let's go ahead and type in and block over here, OK?

29
00:01:59,400 --> 00:02:06,050
So once we are done with this, what I will do is step inside this particular body.

30
00:02:06,610 --> 00:02:11,120
Uh, let's go ahead and paste this form and let's see how exactly it will look like.

31
00:02:11,460 --> 00:02:15,760
And let's hope that the bootstrap styling is applied to this particular form right now.

32
00:02:16,200 --> 00:02:21,900
So when I hit refresh, as you can see, as we have extended it from the base template, we already

33
00:02:21,900 --> 00:02:23,370
have this navigation bar.

34
00:02:23,520 --> 00:02:29,970
And also now the form actually looks much more better as now we are using bootstrap, but still it does

35
00:02:29,970 --> 00:02:33,040
not look as professional as we want it to be.

36
00:02:33,210 --> 00:02:36,870
So let's go ahead and style that even in a much more better way.

37
00:02:37,050 --> 00:02:40,060
So for now, I just got this from here.

38
00:02:40,110 --> 00:02:46,950
We set up over here and in here we will create some or use some bootstrap classes so as to style the

39
00:02:46,950 --> 00:02:48,540
elements in a much more better way.

40
00:02:49,140 --> 00:02:54,480
So in Bootstrap, what you do is that you have something which is called as a container and you place

41
00:02:54,480 --> 00:02:56,310
each and everything inside that container.

42
00:02:56,520 --> 00:02:59,000
So let's go ahead and create a container class over here.

43
00:02:59,550 --> 00:03:06,660
So I'll type in div that's going to be class and let's name this thing as container.

44
00:03:07,230 --> 00:03:10,650
And now inside this container, we as usual have a room.

45
00:03:10,980 --> 00:03:16,200
Sole purpose of the class is going to be a rule.

46
00:03:16,830 --> 00:03:20,130
And within this rule we are actually going to create a column.

47
00:03:20,520 --> 00:03:29,130
So I'll type in the class equals call dash embley dash for meaning that we want a column which actually

48
00:03:29,130 --> 00:03:31,080
spans for bootstrap columns.

49
00:03:31,560 --> 00:03:37,090
And also when we go ahead and create this particular column, we need to add some elements to it.

50
00:03:37,740 --> 00:03:44,820
So in here, in order to actually display the log in, we actually use something which is called as

51
00:03:44,820 --> 00:03:45,280
a card.

52
00:03:45,300 --> 00:03:51,540
So in order to use a bootstrap card, you simply need to create a div, create a class for that div

53
00:03:51,540 --> 00:03:54,050
and assign a class called Ascott.

54
00:03:54,810 --> 00:04:00,110
So now once you have this card, the card actually has a header and a body.

55
00:04:00,450 --> 00:04:07,020
So in order to define the card header, you type in the class that's going to be called header, which

56
00:04:07,020 --> 00:04:09,180
is the class for defining the card header.

57
00:04:09,540 --> 00:04:14,070
And here you can also specify the properties of the text.

58
00:04:14,490 --> 00:04:19,320
So your card is going to be nothing, but it's actually going to be a container which is going to hold

59
00:04:19,320 --> 00:04:25,200
your form and the card is going to have to pass the header with say something like log in and the body,

60
00:04:25,200 --> 00:04:27,710
which is actually going to have the input fields.

61
00:04:28,200 --> 00:04:30,420
So for this, let's go ahead.

62
00:04:31,740 --> 00:04:37,810
And this guard had over here and inside this guard had we actually want a title, which is Log-in.

63
00:04:38,340 --> 00:04:44,990
So again, I created the class and the class for the car title is called the title.

64
00:04:45,960 --> 00:04:49,170
And now let's define the title as, let's say, Log-in.

65
00:04:49,710 --> 00:04:54,880
And now once we have this title now, we can go ahead and actually create the card body.

66
00:04:54,900 --> 00:04:58,890
So the card holder actually ends over here.

67
00:04:59,460 --> 00:05:02,150
So now from here we will start the card body.

68
00:05:02,160 --> 00:05:06,030
So type in of class, that's going to be Card Basche body.

69
00:05:07,240 --> 00:05:12,280
And now in here, we will actually go ahead and paste the form which we have.

70
00:05:13,850 --> 00:05:19,430
So let's go ahead, get the form from here and let's try to pace the foreman.

71
00:05:19,640 --> 00:05:25,940
Now, if you go to the site and if you hit refresh, as you can see now you have the login form, but

72
00:05:25,940 --> 00:05:29,060
the form actually looks still pretty boring and dull.

73
00:05:29,420 --> 00:05:32,010
So now let's go ahead and make a few changes in here.

74
00:05:32,480 --> 00:05:38,840
So the first change which we will make is that we will make this form appear to the center of the screen

75
00:05:39,380 --> 00:05:41,390
rather than to the extreme left over here.

76
00:05:41,750 --> 00:05:43,520
So how exactly can we do that?

77
00:05:44,030 --> 00:05:48,810
We can do that by adding the offset to this particular column in which we have the form.

78
00:05:49,310 --> 00:05:53,350
So in order to add offset, you use the offset class and bootstrap.

79
00:05:53,360 --> 00:05:57,320
So you type an offset the empty dash.

80
00:05:57,600 --> 00:06:00,400
And here we want to offset by four columns.

81
00:06:00,410 --> 00:06:02,000
So I'll add four over here.

82
00:06:02,150 --> 00:06:08,150
And now if you go back and hit refresh, as you can see now, our form will be present in the central

83
00:06:08,150 --> 00:06:08,780
location.

84
00:06:10,350 --> 00:06:16,820
So now once that thing is done, let's go ahead and also change the color of this particular card header.

85
00:06:17,220 --> 00:06:23,040
So let's say if you want to make it into a blue color, so what you can do for that purpose is that

86
00:06:23,040 --> 00:06:29,340
you can go into the card header and in here you can mention a property which is called as the background,

87
00:06:29,640 --> 00:06:33,900
and you can type an envelope over here for blue color.

88
00:06:34,710 --> 00:06:41,130
So now when you go ahead, hit refresh, as you can see, this is actually present in the blue or aquacultural,

89
00:06:41,580 --> 00:06:44,730
and it looks much more better now on this aquacultural.

90
00:06:44,730 --> 00:06:47,480
Let's see if you want to have a white font for the log in.

91
00:06:47,850 --> 00:06:53,490
You can also type in a class which is called last text dash white.

92
00:06:54,090 --> 00:06:57,330
And now, as you can see, this is going to turn into white.

93
00:06:58,020 --> 00:07:03,060
Now, one more thing which you might have noticed here is that these particular fields, which is the

94
00:07:03,060 --> 00:07:08,790
user name and password, so you can also style those individual fields using bootstrap as well.

95
00:07:09,360 --> 00:07:11,180
Now, how exactly can you do that?

96
00:07:11,460 --> 00:07:16,980
So in order to style individual fields and bootstrap, what you can do is that, first of all, you

97
00:07:16,980 --> 00:07:20,340
need to be able to access those fields in an individual manner.

98
00:07:20,370 --> 00:07:25,050
So right now, as you can see, we don't actually have any kind of field over here.

99
00:07:25,080 --> 00:07:27,640
Instead, we directly have this particular form.

100
00:07:28,320 --> 00:07:32,610
So how exactly can you get access to individual feel inside this form?

101
00:07:32,850 --> 00:07:35,990
So in order to do that, let's first get rid of the form.

102
00:07:36,420 --> 00:07:41,370
And in here, instead of directly accessing the form, let's access the form field.

103
00:07:41,610 --> 00:07:49,640
So in order to access the form field, you can go ahead and use a for loop and pipe in for fill in form.

104
00:07:50,130 --> 00:07:56,220
And by using this you actually get to access each and every individual feel inside your form.

105
00:07:56,520 --> 00:08:00,090
And now let's also make sure to end this.

106
00:08:00,240 --> 00:08:03,980
So I'll type in for to end this particular loop.

107
00:08:04,200 --> 00:08:04,930
And in here.

108
00:08:05,160 --> 00:08:10,230
Now we actually have access to each one of those fields using this field variable.

109
00:08:11,040 --> 00:08:18,570
So now what we will do is that we will place those each individual element inside a class or a bootstrap

110
00:08:18,570 --> 00:08:20,500
class, which is called US form group.

111
00:08:20,520 --> 00:08:25,890
So I can type in love class that is going to be from that group.

112
00:08:26,880 --> 00:08:30,800
And now inside this particular class, we can use these form fields.

113
00:08:31,530 --> 00:08:35,370
So in order to access the form fields, I'll type in field.

114
00:08:35,490 --> 00:08:40,559
And first of all, we would want the name of the field and then we want the input field itself.

115
00:08:40,590 --> 00:08:43,510
So I'll type in field over here.

116
00:08:44,190 --> 00:08:48,080
So now once we go ahead and see that we are pretty much good to go.

117
00:08:48,450 --> 00:08:56,640
And now if we go back over here and hit refresh, as you can see how the field looks a little bit better

118
00:08:56,760 --> 00:08:58,560
as compared to the previous ones.

119
00:08:58,560 --> 00:09:04,320
And now let's go ahead and also style up this button over here by assigning it a bootstrap class.

120
00:09:04,330 --> 00:09:10,470
So the class is going to be actually between back and full to give it a blue color.

121
00:09:10,920 --> 00:09:18,000
So now if you go back over here and hit refresh, as you can see now, your form looks a lot better

122
00:09:18,000 --> 00:09:22,200
than the previous one as now you have the fields and everything.

123
00:09:22,200 --> 00:09:23,040
SCILAB Correct.

124
00:09:23,050 --> 00:09:24,840
So that's it for this lecture.

125
00:09:24,990 --> 00:09:32,010
And hopefully you guys be able to understand how to style your login form in a decent manner using bootstrap

126
00:09:32,220 --> 00:09:33,710
and a little bit of Django.

127
00:09:34,050 --> 00:09:39,630
So the only Django part, which we have learned here, is that how you can access individual fields

128
00:09:39,630 --> 00:09:42,340
inside a form by using a follow up.

129
00:09:43,170 --> 00:09:48,800
So in the upcoming lecture, what we will do is that we will also go ahead and style the register page

130
00:09:48,810 --> 00:09:49,240
as well.

131
00:09:49,260 --> 00:09:54,340
So if you go to register right now, as you can see, the page actually looks pretty dull and boring.

132
00:09:54,960 --> 00:09:56,910
So thank you very much for watching.

133
00:09:56,910 --> 00:09:59,160
And I'll see you guys in the next lecture.

134
00:09:59,520 --> 00:10:00,090
Thank you.

