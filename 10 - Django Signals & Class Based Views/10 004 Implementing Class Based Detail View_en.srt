1
00:00:00,480 --> 00:00:06,420
Now, let's go ahead and try to implement the Class-Based legal view, so if you actually open up your

2
00:00:06,420 --> 00:00:11,640
browser, as you can see, this thing right here is nothing but the list view because it has a list

3
00:00:11,640 --> 00:00:13,090
of a bunch of items in here.

4
00:00:13,740 --> 00:00:18,570
What if you click on the details button over there, you will actually get this particular detailed

5
00:00:18,570 --> 00:00:22,050
view of the detail page of that particular item.

6
00:00:22,050 --> 00:00:26,850
And this particular view is nothing, but it's actually called Ask the Detailed View.

7
00:00:27,360 --> 00:00:32,790
So a detailed view is nothing but anything which gives you the detail of a particular item.

8
00:00:33,150 --> 00:00:39,720
So now this particular detail page can actually be implemented by using a Class-Based detailed view.

9
00:00:40,050 --> 00:00:43,350
So now let's go ahead and learn how you can implement that.

10
00:00:43,740 --> 00:00:49,320
Right now, this detail page is actually powered by this particular view, which is the function based

11
00:00:49,320 --> 00:00:51,500
view which we have named as some detail.

12
00:00:51,870 --> 00:00:55,030
But now let's go ahead and replace this with a Class-Based view.

13
00:00:55,260 --> 00:01:00,730
So now I'll just simply go ahead and first of all, import the Class-Based view, which is called as

14
00:01:00,780 --> 00:01:01,590
the detailed view.

15
00:01:01,980 --> 00:01:12,180
So I'll type in from Zango dot views dot generic dot, and this is going to be detail and type an input

16
00:01:13,050 --> 00:01:14,720
that's going to be detailed view.

17
00:01:15,570 --> 00:01:21,720
So now let's go ahead, go right beneath the detailed view, which is the function based view and widen

18
00:01:21,720 --> 00:01:22,940
a new view over here.

19
00:01:23,490 --> 00:01:30,050
So I'll type in the name of this class as full detail, so I'll type in who detail over here.

20
00:01:30,510 --> 00:01:32,880
And this needs to inherit from the little view.

21
00:01:33,360 --> 00:01:36,720
So now once we go ahead and do that, we are pretty much good to go.

22
00:01:37,440 --> 00:01:44,730
So now, in case of a detailed view, what we want to do is that we actually want to go ahead and mention

23
00:01:44,940 --> 00:01:46,970
the name of the model which we are using.

24
00:01:46,980 --> 00:01:50,260
So the model which we are using is nothing but items.

25
00:01:50,310 --> 00:01:52,620
So I'll type and model equals item.

26
00:01:53,870 --> 00:01:58,920
So once we have this particular thing now, the only thing which you need to pass to this particular

27
00:01:58,920 --> 00:02:00,920
view is nothing but the template name.

28
00:02:01,380 --> 00:02:06,490
So you need to type in the template name as nothing but this, which we have mentioned over here.

29
00:02:07,080 --> 00:02:08,090
So I'll just go ahead.

30
00:02:08,100 --> 00:02:08,970
Copy that.

31
00:02:09,360 --> 00:02:10,570
We it up over here.

32
00:02:11,490 --> 00:02:17,130
So now that's the only thing which you need to do over here and you don't have to write anything else

33
00:02:17,130 --> 00:02:18,570
in the particular detailed view.

34
00:02:19,950 --> 00:02:25,860
Now, the only thing which you need to do is that you need to modify the unstoppably file for the detail.

35
00:02:25,930 --> 00:02:33,300
So in this case, we first need to obviously go ahead and replace this with good detail, not ArcView.

36
00:02:33,630 --> 00:02:36,120
So that's the obvious change which you need to make.

37
00:02:36,450 --> 00:02:42,330
And one more change you need to make over here is that now you cannot pass this item idea here.

38
00:02:42,480 --> 00:02:49,460
So if you actually go to views, as you can see, we have actually passed in the item ID as the parameter

39
00:02:49,460 --> 00:02:49,950
over here.

40
00:02:49,950 --> 00:02:55,560
But now this item, it needs to be replaced with something which is called as the primary key, which

41
00:02:55,560 --> 00:02:56,380
is the PECKY.

42
00:02:56,850 --> 00:03:02,250
So as you can see, we have assigned the item in the speccy over here and the function based for you.

43
00:03:02,400 --> 00:03:07,260
But now we can directly pass in the speak to the unstoppably file.

44
00:03:07,500 --> 00:03:12,570
So instead of this item idy, I can now directly go ahead and type in pecky over here.

45
00:03:13,260 --> 00:03:16,650
So now once that thing is done, we are pretty much good to go.

46
00:03:17,280 --> 00:03:24,260
Now we also know that this particular view is now going to be attached with the template which is detailed,

47
00:03:24,360 --> 00:03:25,250
not HTML.

48
00:03:25,650 --> 00:03:28,900
So we also need to make a minor change over there as well.

49
00:03:29,220 --> 00:03:30,510
So let's go to the.

50
00:03:31,470 --> 00:03:33,510
And detailed what each HTML template.

51
00:03:34,470 --> 00:03:41,370
Which is this template right here and in here, what you need to do now is that you need to change this

52
00:03:41,370 --> 00:03:46,100
particular item to a different name, which is called less object.

53
00:03:46,710 --> 00:03:54,150
So now, in the actual view, what we have done here is that we have actually passed an item as the

54
00:03:54,150 --> 00:03:54,840
context.

55
00:03:55,170 --> 00:04:00,540
Now, if you don't want to pass an item as a context over there, you can simply go ahead and replace

56
00:04:00,540 --> 00:04:03,900
that name item with the name, which is nothing but an object.

57
00:04:04,260 --> 00:04:09,210
So now you can actually go here and replace this item with object.

58
00:04:10,650 --> 00:04:12,390
So do that boy here.

59
00:04:13,950 --> 00:04:16,470
Over here and over here as well.

60
00:04:18,130 --> 00:04:23,050
So now, once you do that, you're pretty much good to go so so have written the last piece for you,

61
00:04:23,410 --> 00:04:26,980
you have also modified the you are a stockpile as well.

62
00:04:27,340 --> 00:04:33,910
So that means whenever you now go to detail page of any one of the items, you should get the page which

63
00:04:33,910 --> 00:04:35,050
you are getting previously.

64
00:04:35,770 --> 00:04:39,440
So now let's go back over here and let's hit refresh.

65
00:04:39,460 --> 00:04:46,150
So when I click on details, as you can see now, I am redirected to the detail page without any issues.

66
00:04:46,180 --> 00:04:51,820
So when I click on detail of this as well, I am actually getting redirected to the detail page of that

67
00:04:51,820 --> 00:04:53,960
particular item as well.

68
00:04:53,980 --> 00:04:58,270
So that means Class-Based detailed view is functioning as we want it.

69
00:04:59,860 --> 00:05:05,990
So that's it for this lecture and hopefully you guys be able to understand how to create class based,

70
00:05:06,010 --> 00:05:12,250
detailed view, so what you can do is that you can also go ahead and modify the rest of the views which

71
00:05:12,250 --> 00:05:15,010
you have here to the Class-Based view as well.

72
00:05:15,550 --> 00:05:20,870
Now, if you would have used up by file of our food out there, a bunch of views over here.

73
00:05:21,040 --> 00:05:25,390
So we also have used to create update and delete items.

74
00:05:25,570 --> 00:05:32,290
So these respective views can also be implemented in a class based manner by using their world view,

75
00:05:32,300 --> 00:05:35,380
such as the create view, the delete view and update you.

76
00:05:36,250 --> 00:05:41,320
So we are going to do that in the upcoming lectures, but for now, I just wanted to give you guys an

77
00:05:41,320 --> 00:05:47,440
idea of what exactly are class based or generic views and Shango and how they can be used.

78
00:05:47,950 --> 00:05:49,960
So thank you very much for watching.

79
00:05:49,960 --> 00:05:51,970
And I'll see you guys next time.

80
00:05:52,360 --> 00:05:52,960
Thank you.

