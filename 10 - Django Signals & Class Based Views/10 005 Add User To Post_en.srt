1
00:00:00,060 --> 00:00:06,210
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how we can associate

2
00:00:06,210 --> 00:00:08,710
each and every user with a post.

3
00:00:08,730 --> 00:00:12,550
So what I exactly mean by associating a user with a post.

4
00:00:13,110 --> 00:00:19,200
So right now we know that any logged in user can actually go ahead and post this particular food item

5
00:00:19,200 --> 00:00:19,770
right here.

6
00:00:20,410 --> 00:00:27,780
Now, we don't just want to go ahead and do that, but we also want to see which the user has posted

7
00:00:27,780 --> 00:00:29,010
a particular post.

8
00:00:29,040 --> 00:00:34,860
So, for example, let's say I am logged in as a particular user and when I am logged in as a particular

9
00:00:34,860 --> 00:00:38,560
user and I go ahead and post some kind of food item here.

10
00:00:38,670 --> 00:00:44,820
So whenever the next person actually visits this website, he must know that this particular post has

11
00:00:44,820 --> 00:00:46,100
been posted by me.

12
00:00:46,380 --> 00:00:52,140
So let's learn how exactly can we associate this particular post with a certain user?

13
00:00:52,350 --> 00:00:58,290
So we all know that this particular post is nothing, but it's actually derived from the model, which

14
00:00:58,290 --> 00:01:01,130
is the item model, which is this model right here.

15
00:01:01,530 --> 00:01:06,000
And the users actually belong to a different model, which is called as users.

16
00:01:06,060 --> 00:01:12,300
Now, the user model, which we are using is not a custom built model, but it's actually an inbuilt

17
00:01:12,300 --> 00:01:13,760
model which we are using.

18
00:01:13,800 --> 00:01:21,390
So what we need to do now is that we need to establish some sort of relationship between the item and

19
00:01:21,390 --> 00:01:27,330
the user model so that we know that a particular item has been posted by a particular user.

20
00:01:28,110 --> 00:01:34,620
So one thing which we can do here is that we can add a user feel right over here, which actually stores

21
00:01:34,620 --> 00:01:40,160
the information of the user who's actually logged in while posting a particular post.

22
00:01:40,860 --> 00:01:43,890
So let's see how exactly can we go ahead and do that.

23
00:01:44,130 --> 00:01:47,130
So first of all, we need to import the user model here.

24
00:01:47,160 --> 00:01:52,350
So in order to improve the user model, we die from Shango Dot Cantrip.

25
00:01:53,560 --> 00:02:02,490
Dot, dot, dot models, and from here we import the user model because here we are using the inbuilt

26
00:02:02,500 --> 00:02:07,390
user model, which is from Shango dot, dot, dot, dot models.

27
00:02:07,870 --> 00:02:14,800
Now, once you go ahead and impose this user, we now need to go ahead and somehow connect this user

28
00:02:14,980 --> 00:02:19,000
or this user model with the item model, which is right over here.

29
00:02:19,750 --> 00:02:21,580
So how exactly can we do that?

30
00:02:22,080 --> 00:02:29,200
So whenever you want to establish a connection between two models, what you do is that you use one

31
00:02:29,200 --> 00:02:32,300
model as a foreign key in another model.

32
00:02:32,920 --> 00:02:38,380
So as we all know, each model actually has a primary key, which is the idea of pecky.

33
00:02:38,860 --> 00:02:45,340
So the idea or primary key of that particular model is nothing but the unique value associated with

34
00:02:45,340 --> 00:02:47,410
each entry in that particular model.

35
00:02:47,770 --> 00:02:53,820
So for example, for food item one, we have the ID, one for food, item two, we have the ID two.

36
00:02:53,830 --> 00:02:55,790
So it's a unique ID for that model.

37
00:02:56,380 --> 00:03:02,700
Now, similar to that particular primary key or the primary ID, we can also associate a foreign key.

38
00:03:03,040 --> 00:03:09,970
So foreign key is nothing, but it some feel from some other model which establishes a connection between

39
00:03:09,970 --> 00:03:11,850
the current model and the other model.

40
00:03:12,100 --> 00:03:18,280
So as now we have to establish the connection of the item model with the user, we go ahead and use

41
00:03:18,280 --> 00:03:25,570
some sort of a user feel over here and associate that as a primary key of this model, which is item.

42
00:03:26,020 --> 00:03:32,830
So here, what we will do here is that will add a new field and we'll call it as user underscore name.

43
00:03:33,250 --> 00:03:36,030
And this thing is actually going to be a foreign key.

44
00:03:36,310 --> 00:03:44,160
So in order to see that this is a foreign key, you type and models start foreign key here.

45
00:03:44,170 --> 00:03:49,850
We need to pass in the name of the model, which we want to associate with this particular model item.

46
00:03:50,170 --> 00:03:53,770
So we are talking about the user model here, so I'll type in user.

47
00:03:54,310 --> 00:04:00,070
And then after typing that thing in, we now need to go ahead and dipen undelete.

48
00:04:00,490 --> 00:04:04,390
That's going to be models, dot gasket.

49
00:04:04,870 --> 00:04:08,380
And then you need to type in the default value for this as well.

50
00:04:08,770 --> 00:04:11,890
So you need to type in default as, let's say one.

51
00:04:13,050 --> 00:04:16,779
So now let's understand what exactly do we mean by this line of code?

52
00:04:17,399 --> 00:04:23,700
So here what we have essentially done is that we have added a new field as a foreign key, and this

53
00:04:23,700 --> 00:04:25,820
key actually belongs to the user model.

54
00:04:26,100 --> 00:04:29,590
But this key acts as a foreign key in the item model.

55
00:04:29,610 --> 00:04:35,130
And this is the key which establishes a connection between item and a user.

56
00:04:35,700 --> 00:04:42,420
So now let's understand what exactly happens and how exactly a user is associated with an item.

57
00:04:42,990 --> 00:04:50,370
So whenever you go ahead and post an item from a form now, you will also be asked that which particular

58
00:04:50,370 --> 00:04:52,470
user is posting this particular form.

59
00:04:52,770 --> 00:04:57,810
So in there, we need to go ahead and mention the user I.D. over here, which is nothing but the unique

60
00:04:57,810 --> 00:04:59,730
ID of that particular user.

61
00:04:59,730 --> 00:05:05,430
And hence we have specified the user model over here, meaning that we need to associate the user model

62
00:05:05,430 --> 00:05:06,270
with the item.

63
00:05:06,810 --> 00:05:12,030
And then we have specified this field, which is undelete equals model dot cascade.

64
00:05:12,030 --> 00:05:15,660
And then we have also provided the default value for this as one.

65
00:05:16,140 --> 00:05:18,530
So why exactly do we need a default value?

66
00:05:18,660 --> 00:05:25,660
Because we already have some items and we don't know which kind of user has actually posted those items.

67
00:05:26,100 --> 00:05:32,880
So for all those users, the default is going to be equal to one, meaning that the user whose ID is

68
00:05:32,880 --> 00:05:35,040
one has posted all those items.

69
00:05:35,820 --> 00:05:38,750
So now once we are done with this, we are pretty much good to go.

70
00:05:39,240 --> 00:05:45,150
And now as we have made changes to the model, we need to go ahead and make migration's.

71
00:05:45,190 --> 00:05:52,770
So now let me just open up the command line or the terminal, and here I'll actually stop the server

72
00:05:53,430 --> 00:05:58,590
and let's go ahead and type in Python three men and stop UI.

73
00:05:59,040 --> 00:06:02,310
That's going to be make migration's.

74
00:06:03,940 --> 00:06:06,640
And now I need to type in Python three.

75
00:06:07,690 --> 00:06:17,050
Managed by Eskil Migrate, that's going to be the food app, because we have made changes to the model

76
00:06:17,050 --> 00:06:21,070
which is present in the food up, and that's going to be zero zero zero three.

77
00:06:22,240 --> 00:06:26,800
Let's enter and now let's step in Python three.

78
00:06:28,220 --> 00:06:31,280
Managed by Margaret.

79
00:06:32,870 --> 00:06:39,020
So now, as you can see, the migration's have been successfully applied, so now all the migration's

80
00:06:39,020 --> 00:06:39,890
have been applied.

81
00:06:40,250 --> 00:06:44,840
So now let's go ahead and see what happens when we go to the admin, not by file.

82
00:06:45,020 --> 00:06:46,990
So let me just go ahead and run the server.

83
00:06:47,120 --> 00:06:50,420
So Python three managed up by.

84
00:06:51,950 --> 00:06:57,680
Run server, and now let's go to the you are all right here and let's go to the admin.

85
00:06:57,800 --> 00:06:59,950
Let me log in as the super user.

86
00:06:59,960 --> 00:07:08,290
And now if you go to the items over here, as you can see, we have a cheeseburger, we have burrito.

87
00:07:08,300 --> 00:07:13,500
And as you can see now, we also have a new field, which is called as the username.

88
00:07:14,030 --> 00:07:20,720
So now you we can select any kind of user which we have in order to associate that particular post with

89
00:07:20,720 --> 00:07:21,260
the user.

90
00:07:21,830 --> 00:07:26,560
So now let's go ahead and try to add a new item and let's see how this thing works now.

91
00:07:26,630 --> 00:07:32,780
So when I go ahead and add a new item, as you can see, it was going to ask me the user name, which

92
00:07:32,780 --> 00:07:38,980
means that it's going to ask us which particular user is actually posting this food data.

93
00:07:39,020 --> 00:07:43,040
So let's say this item is going to be posted by a new user, one.

94
00:07:43,070 --> 00:07:45,620
So I'm simply going to type a new user one.

95
00:07:45,950 --> 00:07:50,120
And let's say the item name is faster.

96
00:07:51,600 --> 00:07:53,570
We'll see Italian.

97
00:07:54,720 --> 00:08:00,600
Faster as the description, let's say the price is.

98
00:08:01,580 --> 00:08:07,870
Forty dollars, and now when I go ahead and click save, as you can see, we now have a new object pastor

99
00:08:07,940 --> 00:08:14,930
and when we go ahead and click that, we now know that this item has been posted by a new user, which

100
00:08:14,930 --> 00:08:21,950
means we have successfully combined or we have successfully associated the user model with the item

101
00:08:21,950 --> 00:08:25,050
model right over here so that they actually have a connection.

102
00:08:25,700 --> 00:08:30,900
So now we can identify which particular user has posted which particular food item.

103
00:08:30,920 --> 00:08:37,640
Now here, as you could notice, that in the admin note profile, we are actually manually associating

104
00:08:37,640 --> 00:08:43,039
this user with a particular item, but an actual scenarios.

105
00:08:43,039 --> 00:08:52,670
What must happen is that if you go to a slash through slash ad, what must actually happen is that whenever

106
00:08:52,670 --> 00:08:59,330
you go ahead and fill this form up, the user who's actually logged in must be automatically entered

107
00:08:59,330 --> 00:09:01,690
as the one who is actually posting this data.

108
00:09:02,030 --> 00:09:04,010
But that does not happen right now.

109
00:09:04,520 --> 00:09:10,040
So in the upcoming lecture, what we will do is that we will learn how to submit this form in a way

110
00:09:10,370 --> 00:09:16,880
that the user who is LogMeIn is automatically associated with this particular posted item.

111
00:09:16,910 --> 00:09:22,220
So right now, we are doing it manually in the admin, not by file, but we actually want to automate

112
00:09:22,220 --> 00:09:25,940
the process so that we don't have to explicitly select the user.

113
00:09:25,940 --> 00:09:31,100
Instead, the logged in user should automatically be associated with this particular post.

114
00:09:31,800 --> 00:09:33,410
So that's it for this lecture.

115
00:09:33,410 --> 00:09:38,840
And in the next lecture, we will go ahead and write the code to associate user with this post.

116
00:09:39,230 --> 00:09:39,830
Thank you.

