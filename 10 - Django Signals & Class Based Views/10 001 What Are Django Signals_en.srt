1
00:00:00,060 --> 00:00:05,790
Hello and welcome to this lecture and in this lecture, we will go ahead and learn about what our Django

2
00:00:05,790 --> 00:00:07,860
signals and why they are used.

3
00:00:08,460 --> 00:00:13,270
So let's first understand why exactly are we using something called Ascencio Signals?

4
00:00:13,740 --> 00:00:17,220
So in the previous lecture, we have actually created the user profile.

5
00:00:17,220 --> 00:00:22,860
We have added the user profile picture and everything like that, which means that for every user in

6
00:00:22,860 --> 00:00:26,440
our database, we are actually going to have a user profile.

7
00:00:27,030 --> 00:00:33,420
Now, while creating the user profile, we have actually created the user profiles manually by logging

8
00:00:33,420 --> 00:00:34,830
into the admin panel.

9
00:00:35,310 --> 00:00:41,820
But in real life situations, what we want to do is that we automatically want to create the user profile

10
00:00:41,820 --> 00:00:43,960
as soon as the user actually registers.

11
00:00:44,400 --> 00:00:46,710
So we have a register page on our website.

12
00:00:46,920 --> 00:00:52,920
And what happens is that when we register a new user, a new user is just created and we have to create

13
00:00:52,920 --> 00:00:55,700
the profile of the user manually right now.

14
00:00:55,800 --> 00:00:58,700
But we actually want to automate that particular process.

15
00:00:59,280 --> 00:01:03,870
So how exactly can we automate the process of creating the user profile?

16
00:01:04,530 --> 00:01:08,220
We do that by using something which is called Django Signals.

17
00:01:08,380 --> 00:01:10,880
So what exactly are Django signals?

18
00:01:11,250 --> 00:01:16,620
So in order to understand Django signals, you actually need to learn about the signal dispatcher.

19
00:01:17,370 --> 00:01:24,540
So Django actually provides a signal dispatcher, which allows decoupled applications to notify something

20
00:01:24,540 --> 00:01:26,670
when some action actually happens.

21
00:01:26,700 --> 00:01:34,320
So the job of the signal dispatcher is to notify applications when some change occurs in that particular

22
00:01:34,320 --> 00:01:35,080
application.

23
00:01:35,130 --> 00:01:37,030
So what exactly do I mean by that?

24
00:01:37,560 --> 00:01:43,560
So whenever we have the register form and whenever the user actually registers, what we want to do

25
00:01:43,560 --> 00:01:49,770
is that we want to send a signal to the profile that, OK, now the user has been reduced to just go

26
00:01:49,770 --> 00:01:51,060
ahead and create a profile.

27
00:01:51,510 --> 00:01:53,330
This is what Django signals do.

28
00:01:53,340 --> 00:01:57,900
That is the sender notification when some kind of action actually occurs.

29
00:01:57,930 --> 00:02:01,590
So that's the simple explanation of what Django signals actually do.

30
00:02:02,070 --> 00:02:08,039
So now Django Signals actually work as a publisher and subscriber or send and receive.

31
00:02:08,250 --> 00:02:13,890
So just like a YouTube channel in which you get the upload notification, whenever your favorite YouTube

32
00:02:14,070 --> 00:02:20,310
or the YouTube videos subscribe to upload the video in a similar fashion in Django, when something

33
00:02:20,310 --> 00:02:25,710
actually happens, Django Signal actually sends out a notification that something has actually happened

34
00:02:26,070 --> 00:02:31,950
and the sender is actually going to send a notification and the receiver is going to receive it now

35
00:02:31,950 --> 00:02:33,150
making use of this.

36
00:02:33,450 --> 00:02:39,450
We will now actually know that when a particular user is registered and when we know that a particular

37
00:02:39,450 --> 00:02:45,570
user is registered, we can then go ahead and take action on this notification by creating his very

38
00:02:45,570 --> 00:02:46,350
own profile.

39
00:02:48,020 --> 00:02:50,130
So this is how it's going to work.

40
00:02:50,480 --> 00:02:55,820
So whenever the user registration is going to occur, we are going to get a signal and that signal is

41
00:02:55,820 --> 00:02:57,860
going to be sent to the user profile.

42
00:02:58,100 --> 00:03:04,160
And when we receive that particular signal, we are actually going to go ahead and create the user profile

43
00:03:04,160 --> 00:03:05,030
automatically.

44
00:03:06,260 --> 00:03:12,620
So let's go ahead and implement this and actually make use of Django signals to create the profile of

45
00:03:12,620 --> 00:03:13,970
the user automatically.

