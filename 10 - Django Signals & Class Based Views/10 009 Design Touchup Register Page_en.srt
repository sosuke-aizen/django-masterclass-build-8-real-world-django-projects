1
00:00:00,060 --> 00:00:06,330
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to modify the

2
00:00:06,330 --> 00:00:09,270
look and feel of this user registration page.

3
00:00:09,840 --> 00:00:15,970
So right now, as you can see, this is how the basic single user registration page looks like.

4
00:00:16,260 --> 00:00:21,750
So what we need to do now is that we need to add some bootstrap styling to it so as to make it look

5
00:00:22,080 --> 00:00:22,980
much more better.

6
00:00:23,280 --> 00:00:28,700
So now let's go to the user's app and search for the template, which is the registered HDMI.

7
00:00:29,400 --> 00:00:32,490
Now, as you can see right now, this is the code which we have.

8
00:00:32,850 --> 00:00:37,920
And now let's go ahead and use bootstrap into this particular template.

9
00:00:38,200 --> 00:00:44,190
So the very first thing which you need to do now is that in order to use bootstrap, you need to extend

10
00:00:44,190 --> 00:00:45,780
from the base template.

11
00:00:45,930 --> 00:00:56,010
So here will use the syntax for so Taipan extends and will extend the template, which is food, slash

12
00:00:56,370 --> 00:00:57,630
paste or HDMI.

13
00:00:57,750 --> 00:01:03,930
So now once we have extended the template, we can now use the block body and then block tag so as to

14
00:01:03,930 --> 00:01:06,540
indicate the starting of the code block.

15
00:01:06,870 --> 00:01:14,610
So here I'll type in lock body and here I'll type in and block.

16
00:01:17,020 --> 00:01:23,230
And now, as usual, as we have done with the login template, we are actually going to go ahead and

17
00:01:23,230 --> 00:01:29,320
create a container, create a row inside the container, and then use this particular column, that

18
00:01:29,320 --> 00:01:34,750
empty dash for class over here with an offset so as to style this in a much more better way.

19
00:01:35,320 --> 00:01:42,400
So what you can do for now is that you can simply copy the same code which you have over here in the

20
00:01:42,400 --> 00:01:43,660
login, not HTML.

21
00:01:43,750 --> 00:01:48,970
So you can copy this particular structure because you obviously don't want to type the entire thing

22
00:01:48,970 --> 00:01:49,710
once again.

23
00:01:49,720 --> 00:01:53,260
So you can just go ahead, copy that entire thing.

24
00:01:53,560 --> 00:01:55,750
Go back over here in the.

25
00:01:57,750 --> 00:02:04,740
Registered e-mail page and in here, just simply paystub bunch of code in here and now what we need

26
00:02:04,740 --> 00:02:09,419
to do is that we, first of all, need to replace this thing with register.

27
00:02:10,440 --> 00:02:15,990
And now for the car body, instead of having this form, we can go ahead and get rid of this entire

28
00:02:15,990 --> 00:02:16,750
form right here.

29
00:02:17,370 --> 00:02:19,950
So just go ahead and delete this entire form.

30
00:02:20,250 --> 00:02:24,960
And in here instead, we go ahead and post this particular form.

31
00:02:25,200 --> 00:02:29,290
So I'll just go ahead, get that and I can paste this thing over here.

32
00:02:30,240 --> 00:02:36,460
However, if you have a look at the registration form, there is one thing which is missing in here.

33
00:02:36,570 --> 00:02:41,400
So right now, if you just go ahead and paste that code in here, let's see what kind of form or what

34
00:02:41,400 --> 00:02:42,810
kind of output do we get.

35
00:02:43,470 --> 00:02:51,030
So now if I save this code and if we go back to the Web browser and when I hit refresh, as you can

36
00:02:51,030 --> 00:02:54,110
see, this is the kind of output which we get.

37
00:02:54,120 --> 00:03:00,200
So we basically get the same form, but the same form is actually present inside this particular block.

38
00:03:00,210 --> 00:03:03,480
But we don't want this kind of form to be present over here.

39
00:03:03,870 --> 00:03:06,450
Instead, we want a clean and nice looking form.

40
00:03:06,690 --> 00:03:12,870
So for that very purpose, what we will do here is that instead of just having this entire form over

41
00:03:12,870 --> 00:03:19,020
here, we can now go ahead and access each and every single one of those fields inside our form.

42
00:03:19,350 --> 00:03:21,660
So how exactly can we go ahead and do that?

43
00:03:22,170 --> 00:03:26,030
So for that, what we can do is that, first of all, we need to get rid of this form.

44
00:03:26,550 --> 00:03:32,790
And now in order to access each one of those fields inside the form, we can use a for loop.

45
00:03:32,970 --> 00:03:39,210
So we can type something like this and you can type for fill in form.

46
00:03:39,750 --> 00:03:44,810
So by using this, you now get an access to each and every field inside the form.

47
00:03:44,970 --> 00:03:46,140
So feel is nothing.

48
00:03:46,140 --> 00:03:51,210
But it's just a random variable name which we have given to a single file inside the form.

49
00:03:51,660 --> 00:03:54,030
The form, however, is the main thing right here.

50
00:03:54,270 --> 00:03:59,180
So now once we have that, let's also go ahead and use the end for over here.

51
00:03:59,190 --> 00:04:01,130
And that's because we always do that.

52
00:04:01,740 --> 00:04:08,400
And now once we have this, we can now go ahead and actually access those fields in here now in order

53
00:04:08,400 --> 00:04:09,650
to access the field.

54
00:04:09,810 --> 00:04:13,480
You can either access the field or you can also access the name of the field.

55
00:04:13,500 --> 00:04:18,649
So, for example, the name of the field might be username, password and everything like that.

56
00:04:18,660 --> 00:04:24,330
So in order to access those fields, we will first go ahead and create a new class over here.

57
00:04:24,330 --> 00:04:26,880
That is, we are going to create a new developer here.

58
00:04:27,060 --> 00:04:30,180
That's the bootstrap class of form group.

59
00:04:30,460 --> 00:04:35,880
So I'll type in Dave and the class is going to be form group.

60
00:04:36,480 --> 00:04:42,660
So this is going to make sure that whatever field we add in here in between, this stuff actually gets

61
00:04:42,690 --> 00:04:44,140
tile, ASPO, bootstrap.

62
00:04:44,850 --> 00:04:51,840
So now in here we can finally go ahead and we first get the axis of the first display, the name of

63
00:04:51,840 --> 00:04:52,320
the field.

64
00:04:52,860 --> 00:04:55,080
So we Dyersville dot name.

65
00:04:55,800 --> 00:05:02,950
And then what we will do is that will give a break here in order to have a line break.

66
00:05:03,030 --> 00:05:09,090
That means after the filename at the down side of the filename, we actually want to display the input

67
00:05:09,090 --> 00:05:09,510
field.

68
00:05:09,810 --> 00:05:12,000
So I'll type in field here.

69
00:05:12,720 --> 00:05:17,580
So now once you go ahead and do that, let's see how form actually changed.

70
00:05:17,700 --> 00:05:24,090
So when I go ahead and hit refresh, as you can see now, our form looks much more better as compared

71
00:05:24,090 --> 00:05:25,200
to the previous version.

72
00:05:25,830 --> 00:05:30,840
Now, what you can do is that you can go ahead and you can also style up this button, because right

73
00:05:30,840 --> 00:05:32,870
now it's looking pretty dull and boring.

74
00:05:33,240 --> 00:05:39,690
So let's go ahead and assign a class to this particular button so I can type in class equals that's

75
00:05:39,690 --> 00:05:43,470
going to be, let's see, Betty and Dash and full.

76
00:05:44,310 --> 00:05:50,970
And now if you go back, hit refresh, as you can see now, the button color changed and you can also

77
00:05:50,970 --> 00:05:54,440
change this thing to register or you can just keep it sign up.

78
00:05:54,460 --> 00:06:00,270
So now, as you can see, everything works just fine and bootstrap form is now completely ready and

79
00:06:00,270 --> 00:06:05,940
it looks much more cleaner as compared to the previous one, where we have a lot of information over

80
00:06:05,940 --> 00:06:07,900
here, which we don't actually need.

81
00:06:07,920 --> 00:06:09,420
So that's it for this lecture.

82
00:06:09,510 --> 00:06:16,320
And hopefully you guys be able to understand how to modify or make your registration form look much

83
00:06:16,320 --> 00:06:16,910
more better.

84
00:06:17,310 --> 00:06:19,650
And I know that this is not the best.

85
00:06:19,650 --> 00:06:25,830
But as we are learning Django and we are not yet into the front end development, so we won't focus

86
00:06:25,830 --> 00:06:26,510
much on that.

87
00:06:26,790 --> 00:06:32,710
This is just to give you guys an idea of how you can actually style the form which you get through Django.

88
00:06:33,330 --> 00:06:39,390
So in the next lecture will go ahead and will try to style this particular page, which is the page

89
00:06:39,390 --> 00:06:41,460
which allows us to add food items.

90
00:06:41,610 --> 00:06:45,510
So thank you very much for watching and I'll see you guys next time.

91
00:06:45,990 --> 00:06:46,620
Thank you.

