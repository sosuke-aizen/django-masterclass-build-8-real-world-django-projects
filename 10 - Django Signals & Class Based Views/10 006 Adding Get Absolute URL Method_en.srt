1
00:00:00,210 --> 00:00:05,340
Hello and welcome to this lecture and in this lecture, we will go ahead and talk something about the

2
00:00:05,340 --> 00:00:07,230
get absolute you are a method.

3
00:00:07,650 --> 00:00:10,610
So this is the code which I have added up over here.

4
00:00:10,620 --> 00:00:15,840
And what this is, is that it's actually a function for this item model.

5
00:00:15,870 --> 00:00:21,490
So now let's understand why exactly would we need the get absolute you are a method.

6
00:00:21,990 --> 00:00:27,870
So as we have created a class based create a view, which is this view right here, which is create

7
00:00:27,870 --> 00:00:35,430
item, what happens is that in case of a class based view, we also need to mention the you are to where

8
00:00:35,430 --> 00:00:39,930
we want and Zango to redirect us when that item is created.

9
00:00:40,230 --> 00:00:45,540
So in order to specify that you are, we have this get absolute equal method.

10
00:00:45,750 --> 00:00:48,850
So let's now take a look at the code inside this method.

11
00:00:49,230 --> 00:00:51,900
So this method will return us reverse.

12
00:00:51,910 --> 00:00:56,510
So reverse is actually a method and you also need to import the reverse method.

13
00:00:56,520 --> 00:01:00,860
A boy here from the Shango dot, you are a package.

14
00:01:00,990 --> 00:01:06,840
So now once you use this reverse method, there are two things which you need to pass in to the reverse

15
00:01:06,840 --> 00:01:07,350
method.

16
00:01:07,620 --> 00:01:14,610
So as the job of this get absolute you are method is to go ahead and redirect us to a new page.

17
00:01:14,970 --> 00:01:21,210
What we want to do is that whenever the user creates an item, we want a triangle to redirect us to

18
00:01:21,210 --> 00:01:23,830
the detailed view of that specific item.

19
00:01:24,300 --> 00:01:30,360
So let's say if you add a new item like Berga and let's say if you specify the details of that Bergere

20
00:01:30,360 --> 00:01:36,840
and the ADD item form, we should then be redirected to the detail page of that particular item, which

21
00:01:36,840 --> 00:01:37,470
is bugel.

22
00:01:37,860 --> 00:01:42,370
Henceforth, what we do is that we pass in food column detail over here.

23
00:01:42,540 --> 00:01:47,110
This means that we need Shango to redirect us to the detail you are.

24
00:01:47,580 --> 00:01:54,270
So if you go to the Alstott start by file right here, as you can see, this thing right here is nothing

25
00:01:54,270 --> 00:01:56,520
but the you are all for detail.

26
00:01:56,880 --> 00:01:59,980
And this is why we have specified this name over here.

27
00:02:00,390 --> 00:02:07,740
So what Django does is that when Django reads this line of code, it understands that it needs to visit

28
00:02:07,740 --> 00:02:08,520
the detail.

29
00:02:08,520 --> 00:02:10,350
You are all of the food app.

30
00:02:10,750 --> 00:02:15,120
So the detail you are all of the food app is nothing but this thing right here.

31
00:02:15,210 --> 00:02:22,800
And henceforth, what happens is that Django would now redirect us to this specific detail view, which

32
00:02:22,800 --> 00:02:29,610
is nothing but this UL right here, which is nothing but the usual for the detail page of that particular

33
00:02:29,610 --> 00:02:30,040
item.

34
00:02:30,510 --> 00:02:36,990
However, if you take a look at this, you are this you are also accepts the primary key of that item

35
00:02:36,990 --> 00:02:43,350
as well, because unless and until you have a primary key of a particular item, you cannot redirect

36
00:02:43,350 --> 00:02:51,360
us to that specific items, a detail page henceforth in this particular reverse function or the reverse

37
00:02:51,360 --> 00:02:51,850
method.

38
00:02:52,230 --> 00:02:55,380
You also need to pass in the primary he up over here.

39
00:02:55,920 --> 00:03:01,890
So in order to pass in the primary, he we have used this quahogs over here, which stands for keyword

40
00:03:01,890 --> 00:03:02,550
arguments.

41
00:03:02,880 --> 00:03:08,370
So whenever you need to pass in some keyword arguments to the reverse function, you use the Kwok's

42
00:03:08,370 --> 00:03:10,770
parameter up over here and here.

43
00:03:10,770 --> 00:03:17,010
We have made a reference to the primary key using self-doubt pecky and we have passed in that primary

44
00:03:17,010 --> 00:03:18,240
key as speccy.

45
00:03:18,250 --> 00:03:21,330
And once this thing is done, we are pretty much good to go.

46
00:03:21,810 --> 00:03:27,630
So for this specific lecture, make sure that you add this line of code here, which is for importing

47
00:03:27,630 --> 00:03:29,820
the reverse method or the reverse function.

48
00:03:30,240 --> 00:03:34,420
And then you also go ahead and add this method up over here as well.

49
00:03:34,650 --> 00:03:40,380
And the reason why this is actually pre typed over here is because I forgot to go ahead and explain

50
00:03:40,380 --> 00:03:42,400
this line of code from the previous lecture.

51
00:03:42,780 --> 00:03:44,670
So thank you very much for watching.

52
00:03:44,670 --> 00:03:46,620
And I'll see you guys next time.

53
00:03:46,860 --> 00:03:47,460
Thank you.

