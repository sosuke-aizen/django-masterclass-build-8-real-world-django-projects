1
00:00:00,090 --> 00:00:05,700
So this is the end of the previous section, and now let's go through the exact things which we have

2
00:00:05,700 --> 00:00:06,830
learned in this section.

3
00:00:06,870 --> 00:00:10,260
So these are the following things which we have learned in this section.

4
00:00:10,680 --> 00:00:16,890
So we have learned about views in general that as we have learned, how exactly can we create views

5
00:00:16,890 --> 00:00:21,060
in Django and what is the significance of creating views?

6
00:00:21,200 --> 00:00:23,540
So we have created multiple views.

7
00:00:23,550 --> 00:00:27,590
We have actually defined those views using a python function.

8
00:00:27,900 --> 00:00:33,360
We have passed in a request to those particular views and we have also rendered templates using that

9
00:00:33,360 --> 00:00:33,650
view.

10
00:00:34,600 --> 00:00:38,030
Then we have learned about you are all patterns we have learned.

11
00:00:38,040 --> 00:00:42,240
Why exactly do we need you are patterns for routing in our Django app.

12
00:00:42,660 --> 00:00:48,380
We have also learned the different ways of defining you are patterns either in the you are stored by

13
00:00:48,380 --> 00:00:52,030
a file of your app or you are by a file of your project.

14
00:00:52,140 --> 00:00:57,920
Then we learned about models and how models can be used to actually create database tables.

15
00:00:58,230 --> 00:01:03,720
We have learned how to define different kinds of fields inside the model, how to make migrations and

16
00:01:03,720 --> 00:01:08,820
why exactly migrations are required in order to set up a database table in the back.

17
00:01:08,820 --> 00:01:15,060
And then we have learned about how to actually go ahead and create a super user so that we can access

18
00:01:15,120 --> 00:01:16,220
the admin panel.

19
00:01:16,770 --> 00:01:22,950
So we have learned what is an admin panel, how to actually get access to admin panel and how to work

20
00:01:22,950 --> 00:01:28,190
with the admin panel to add data to our database as the site administrator.

21
00:01:29,040 --> 00:01:31,290
Then we have learned about templates.

22
00:01:31,530 --> 00:01:37,410
That is how you can actually go ahead, use HTML webpages in your CHANGA website.

23
00:01:37,410 --> 00:01:43,800
And how exactly can you pass a dynamic data obtained from the database to these templates.

24
00:01:44,250 --> 00:01:51,420
So we have actually extracted the database data using Django models, using the Django overarm and we

25
00:01:51,420 --> 00:01:56,520
have actually passed data to the template and we have dynamically rendered those templates.

26
00:01:57,590 --> 00:01:59,700
Then we have learned about static files.

27
00:01:59,830 --> 00:02:04,740
That is how exactly you can go ahead, add some sort of Sears's code to your website.

28
00:02:04,740 --> 00:02:10,949
Using static files, we have learned how to create the folder, which contains all of our static files.

29
00:02:11,190 --> 00:02:18,390
We have also learned about the load static tag, which allows us to actually use static files on our

30
00:02:18,390 --> 00:02:19,050
website.

31
00:02:20,400 --> 00:02:24,480
Then we have also completed the authentication section as well.

32
00:02:24,790 --> 00:02:31,260
We have learned how exactly can we write a functionality to log in, log out and sign up users on our

33
00:02:31,260 --> 00:02:31,740
site.

34
00:02:32,940 --> 00:02:39,420
Then we have learned about Django Signals, Class-Based views, and these are all the things which we

35
00:02:39,420 --> 00:02:41,310
have learned in this entire section.

36
00:02:42,120 --> 00:02:48,660
And using these concepts, we have actually built a food menu app and we have implemented each one of

37
00:02:48,660 --> 00:02:50,910
these features in that specific app.

38
00:02:51,420 --> 00:02:56,520
So I hope you guys enjoyed creating this specific app and learning all of these concepts.

39
00:02:56,820 --> 00:03:03,630
So if you have any doubts or difficulties understanding anything or any concept or anything inside these

40
00:03:03,630 --> 00:03:08,690
concepts, feel free to post in the Q&A section and I will be there to help you out.

41
00:03:08,700 --> 00:03:13,830
So as this section ends, from the next lecture onwards, we are going to start a brand new section

42
00:03:13,830 --> 00:03:15,600
and learn a few more concepts.

43
00:03:15,600 --> 00:03:16,300
And Django.

44
00:03:16,770 --> 00:03:17,370
Thank you.

