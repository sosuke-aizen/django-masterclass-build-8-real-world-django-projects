1
00:00:01,710 --> 00:00:07,870
Hello and welcome to this lecture, and in this lecture, we are going to talk about database and models.

2
00:00:08,610 --> 00:00:15,240
Now, we already know what views in general are and how views actually work and how they are used to

3
00:00:15,240 --> 00:00:19,620
represent some sort of Jammal or some simple text onto a webpage.

4
00:00:20,160 --> 00:00:27,000
But now, as in this particular course or in this particular section, we want to design a food application

5
00:00:27,000 --> 00:00:32,430
that is an application which displays some details of different types of foods.

6
00:00:32,850 --> 00:00:35,220
We actually need to have a database.

7
00:00:36,000 --> 00:00:41,730
Now, I hope you guys already know what a database is, but for those of you who don't know, a database

8
00:00:41,730 --> 00:00:46,030
is nothing but a place where information is stored in a tabular format.

9
00:00:46,050 --> 00:00:52,410
So to imagine how data is stored in a database, let's say if you want to do some data related to students

10
00:00:52,410 --> 00:00:56,370
for that, you create a table in a database called Our Students.

11
00:00:56,580 --> 00:01:04,709
And this table or the database table is going to have multiple columns like the name each marks address

12
00:01:04,709 --> 00:01:09,840
of that particular students, which are nothing but the attributes of the student table.

13
00:01:10,110 --> 00:01:16,380
And then you can go ahead and add a data of multiple students into that table in a tabular format.

14
00:01:16,620 --> 00:01:19,050
So this is how databases generally work.

15
00:01:19,080 --> 00:01:23,320
Now, let's talk about what exactly are models now in Django.

16
00:01:23,340 --> 00:01:26,050
We have this model start by file right here.

17
00:01:26,550 --> 00:01:29,100
So what exactly is model now?

18
00:01:29,100 --> 00:01:37,020
A model is not exactly a database or database table, but they are actually a blueprint to create these

19
00:01:37,110 --> 00:01:37,680
tables.

20
00:01:38,370 --> 00:01:45,570
So these models right here actually define the structure of how these tables will be created.

21
00:01:46,020 --> 00:01:51,600
That is, these models define which columns the table would have, what would be the name of the table,

22
00:01:51,900 --> 00:01:56,430
what would be the multiple fields inside the table and their data types as well.

23
00:01:57,630 --> 00:02:05,010
Now, once we know about databases and models, now we need to understand what kind of database does

24
00:02:05,010 --> 00:02:12,200
Django use and how exactly can we connect this Django application with the database for our convenience?

25
00:02:12,210 --> 00:02:17,860
Django actually comes with a preinstalled and a pre configured database, which is escalatory.

26
00:02:18,510 --> 00:02:24,480
So if you want to know which database Django is using, you can simply go to the settings that profile

27
00:02:24,480 --> 00:02:25,020
right here.

28
00:02:25,530 --> 00:02:32,010
And if you scroll down a little bit, as you can see, we already have the information related to database

29
00:02:32,010 --> 00:02:33,450
over here, which is configured.

30
00:02:33,450 --> 00:02:39,310
So as you can see, the database engine, which Django uses is escalatory.

31
00:02:39,810 --> 00:02:43,640
So that means Django is using escalatory as its database.

32
00:02:43,650 --> 00:02:49,290
But let's say, for example, if you want to use some other database, you can just go ahead and start

33
00:02:49,290 --> 00:02:53,560
that database and just configure that database in the settings dorper file.

34
00:02:54,060 --> 00:02:58,520
But for now, we are going to just go with CircuLite three for learning purposes.

35
00:02:58,530 --> 00:02:58,830
Okay.

36
00:02:58,860 --> 00:03:04,980
Now, once you know which database Django uses, let scroll up a bit and let's see what kind of other

37
00:03:04,980 --> 00:03:06,210
things to we have in here.

38
00:03:06,240 --> 00:03:12,280
So as you can see, if we scroll up over here, we also have this particular thing which is called us

39
00:03:12,330 --> 00:03:12,930
installed.

40
00:03:12,930 --> 00:03:19,110
And this list actually gives you information of all the apps which are currently installed in your project.

41
00:03:19,980 --> 00:03:25,070
Now, these are nothing but all the apps which are activated in the particular Django instance.

42
00:03:25,530 --> 00:03:30,750
And one more interesting thing to note here is that we have not made these apps.

43
00:03:31,050 --> 00:03:34,540
These are the by default apps which the Django project uses.

44
00:03:35,340 --> 00:03:39,990
Now, you might also notice that we have also made our own app, which is the food app.

45
00:03:40,320 --> 00:03:44,420
But you will notice that that app is not present right over here.

46
00:03:44,430 --> 00:03:47,090
So we are going to add that over here very soon.

47
00:03:47,310 --> 00:03:54,810
But even before that, let's understand why exactly does Django have all these installed app listed

48
00:03:54,810 --> 00:03:55,320
over here?

49
00:03:56,310 --> 00:04:04,740
So the main reason for that is because some of these applications here make use of at least one database

50
00:04:04,740 --> 00:04:05,280
tables.

51
00:04:06,120 --> 00:04:12,030
And hence what we need to do is that we need to create tables out of these apps right over here even

52
00:04:12,030 --> 00:04:13,290
before we can use them.

53
00:04:13,620 --> 00:04:20,519
So now, if you actually go to the server, as you can see, you always get this warning right over

54
00:04:20,519 --> 00:04:20,760
here.

55
00:04:20,779 --> 00:04:25,730
But whenever you run your server, which is that you have 15 and applied migrations.

56
00:04:25,770 --> 00:04:29,940
So what exactly does Django mean by you have an applied migration's.

57
00:04:30,120 --> 00:04:39,360
So what Django says is that you have these apps over here, which is these apps and some of these apps

58
00:04:39,360 --> 00:04:41,240
might be using database tables.

59
00:04:41,460 --> 00:04:46,950
And now what you have to do is that you have to go ahead and create the tables which are required for

60
00:04:46,950 --> 00:04:47,670
these apps.

61
00:04:48,270 --> 00:04:55,500
So in order to create that, you simply have to execute this command, which is Python managed or migrate.

62
00:04:56,010 --> 00:05:00,100
So I'll just go ahead and execute that command and let's see what exactly happens.

63
00:05:00,420 --> 00:05:01,170
So I'll type in.

64
00:05:01,260 --> 00:05:01,980
Python three.

65
00:05:03,360 --> 00:05:04,620
Managed, it's not Pibor.

66
00:05:06,770 --> 00:05:15,050
Migrate, and when I go ahead and hit, enter, as you can see, it says Apply all Migration's admin

67
00:05:15,050 --> 00:05:20,490
arth content type sessions and it actually runs some basic migrations away here.

68
00:05:21,200 --> 00:05:24,210
So what exactly happened over here?

69
00:05:24,230 --> 00:05:30,740
So what the migrate comment actually does is that it looks at all the apps which are under the install

70
00:05:30,740 --> 00:05:37,880
apps over here and create necessary database tables according to the database settings and settings

71
00:05:37,880 --> 00:05:38,780
Doc verified.

72
00:05:40,070 --> 00:05:45,680
Now this actually worked, but now we need to create our very own database as well, because this migrate

73
00:05:45,680 --> 00:05:51,700
command actually created the database which were required by the system and it did not create our database.

74
00:05:52,190 --> 00:05:54,470
So let's go ahead and learn how to create that.

75
00:05:55,670 --> 00:06:01,010
So as I mentioned, whenever you want to create a database, what you need to do is that you need to

76
00:06:01,010 --> 00:06:03,450
define a blueprint over here in the model.

77
00:06:03,860 --> 00:06:05,540
So how exactly can you do that?

78
00:06:06,170 --> 00:06:12,200
So in Django, whenever you want to create a model, a model is created by using a class.

79
00:06:12,890 --> 00:06:19,010
So right now, over here, as we want to save the food items, what we do is that we create a model

80
00:06:19,250 --> 00:06:20,750
which is called as Eitam.

81
00:06:20,900 --> 00:06:27,740
So let's go ahead and create that soil type and class item and then it needs to inherit from the class,

82
00:06:27,740 --> 00:06:29,490
which is model start model.

83
00:06:29,690 --> 00:06:32,240
So I'll type in model start model.

84
00:06:33,160 --> 00:06:39,970
And now what we do is that we define which fields do we want to store in this particular class item?

85
00:06:40,210 --> 00:06:45,730
So let's say we want the item and the school name, so we type that equals.

86
00:06:45,940 --> 00:06:49,940
And then you also need to define the data type of this particular field.

87
00:06:50,320 --> 00:06:52,870
So this is going to be a character field.

88
00:06:53,170 --> 00:06:56,110
So we type in models not.

89
00:06:58,430 --> 00:07:04,070
Garfield, and in here, you also need to define the maximum length of this particular field, so I'll

90
00:07:04,070 --> 00:07:08,720
type and Max and the school length and I'll equate it to two hundred.

91
00:07:10,050 --> 00:07:12,540
Now, let's go ahead and add some more fields here.

92
00:07:12,740 --> 00:07:20,000
So let's say we also have item description, soil type an item underscore DSC and this is also going

93
00:07:20,000 --> 00:07:22,440
to be modeled start Garfield.

94
00:07:22,550 --> 00:07:27,440
Let's say the max length of this thing is going to be also 200.

95
00:07:29,860 --> 00:07:36,280
And now let's have an integer flow here, so let's say we also want to add the price of that particular

96
00:07:36,280 --> 00:07:36,820
item.

97
00:07:36,850 --> 00:07:41,980
So I'll type an item and a school price that's going to be model start.

98
00:07:41,980 --> 00:07:47,590
And now instead of having characterful lower here, I'll have Intisar feel because this thing is going

99
00:07:47,590 --> 00:07:48,670
to be an integer.

100
00:07:50,410 --> 00:07:57,640
So now, once we are done, defining this particular model means that we have a blueprint for having

101
00:07:57,640 --> 00:07:59,560
a database table called as item.

102
00:07:59,980 --> 00:08:05,720
Now, what we need to do is that we need to create an actual database table from this particular model.

103
00:08:06,490 --> 00:08:08,220
So how exactly can we do that?

104
00:08:08,620 --> 00:08:15,280
So we already know that in order to create a database table, we need to use the migrate command, which

105
00:08:15,280 --> 00:08:16,740
we have used right over here.

106
00:08:16,750 --> 00:08:22,420
And the way in which my great command actually works is that it goes into your settings DOT profile,

107
00:08:22,450 --> 00:08:28,000
which is this file right here, and it's going to look into the install apps and create database tables

108
00:08:28,000 --> 00:08:28,910
for those apps.

109
00:08:29,650 --> 00:08:34,090
And right now we have actually defined the model for our food app.

110
00:08:34,330 --> 00:08:40,900
But if you have a look over here, we do not have the description of food app over here in the install

111
00:08:40,900 --> 00:08:41,350
apps.

112
00:08:41,950 --> 00:08:47,590
So our first job is to go ahead, go into the install apps and add our food app over here.

113
00:08:47,920 --> 00:08:49,660
So how exactly can we do that?

114
00:08:49,780 --> 00:08:52,420
So if you just go to app, start by over here.

115
00:08:52,900 --> 00:08:56,700
As you can see, it has a class which is called as food config.

116
00:08:57,280 --> 00:08:59,080
So we simply need to copy that.

117
00:08:59,620 --> 00:09:02,890
And now just go back to the settings that buy a file.

118
00:09:04,600 --> 00:09:08,590
And in here, just add the food app as.

119
00:09:10,690 --> 00:09:17,800
Foods dot, app dot, and then simply based in food config over here, give a comma and you're good

120
00:09:17,800 --> 00:09:18,230
to go.

121
00:09:18,880 --> 00:09:25,420
So now whenever you run the migrate command, Jianguo actually understands that it also needs to create

122
00:09:25,420 --> 00:09:27,550
the database for the food app as well.

123
00:09:28,120 --> 00:09:30,610
So this should actually be food, not food.

124
00:09:32,870 --> 00:09:38,630
So now whenever you go ahead and execute the come command, it's automatically going to create the database

125
00:09:38,630 --> 00:09:39,040
for you.

126
00:09:39,920 --> 00:09:43,840
So now let's go ahead, save the code and start creating the database.

127
00:09:44,090 --> 00:09:45,830
So now let's go ahead.

128
00:09:46,220 --> 00:09:52,190
And the first thing which you need to do is that even before you execute the great command, you need

129
00:09:52,190 --> 00:09:59,060
to first tell Django that you have made some changes to your database model, because as you can see,

130
00:09:59,060 --> 00:10:01,660
we have actually made changes to this file right here.

131
00:10:02,090 --> 00:10:06,350
And whenever you make some change over here, you need to convey that to Django.

132
00:10:06,650 --> 00:10:08,420
So how exactly can we do that?

133
00:10:09,090 --> 00:10:13,470
We do that by using a simple command, which is called as make migrations.

134
00:10:13,970 --> 00:10:19,040
So in here, we'll type in Python three managed by.

135
00:10:19,960 --> 00:10:28,600
Make migration's and we dipen food here as food as our apt name to which we have made changes.

136
00:10:29,290 --> 00:10:32,770
So when we go ahead and hit enter, I guess we do have some issue.

137
00:10:32,770 --> 00:10:33,460
And the code.

138
00:10:34,590 --> 00:10:41,400
So let me just head back over here and in these settings, not by file, I guess.

139
00:10:42,810 --> 00:10:50,640
OK, this should actually be apps, not app, because if you actually look over here, we are referring

140
00:10:50,640 --> 00:10:54,240
to this particular file right here and now, it should work fine.

141
00:10:54,690 --> 00:11:01,280
So let's go back and again, type in the command, which is python free managed or pivo, make migration's

142
00:11:01,290 --> 00:11:01,640
food.

143
00:11:01,700 --> 00:11:04,490
OK, so we again have another arrow here.

144
00:11:05,370 --> 00:11:09,930
So it says and it got an unexpected Keywood argument max length.

145
00:11:09,940 --> 00:11:12,760
OK, I actually made a typo here as well.

146
00:11:12,780 --> 00:11:16,440
OK, so let me just fix that and I'm extremely sorry for that.

147
00:11:16,870 --> 00:11:23,070
I can actually go ahead and remove these arrows, but it's always good to know how exactly you can go

148
00:11:23,070 --> 00:11:25,590
ahead and debug some minor errors over here.

149
00:11:25,920 --> 00:11:27,480
So that actually happens a lot.

150
00:11:28,200 --> 00:11:34,470
So now let's go ahead and again, execute Python three managed or why make Migration's food?

151
00:11:34,920 --> 00:11:39,210
And now, yeah, it has actually created the model, which is Eitam.

152
00:11:39,390 --> 00:11:45,750
So what does make Migration Command actually does is that it's going to have a look at the models in

153
00:11:45,750 --> 00:11:52,320
your app and it's going to actually look for changes and it's going to incorporate those changes in

154
00:11:52,320 --> 00:11:52,800
Django.

155
00:11:52,920 --> 00:11:58,980
So right now, looking at our model start by file, Django actually understood that we have created

156
00:11:58,980 --> 00:12:01,230
a model and it creates a model up over here.

157
00:12:01,830 --> 00:12:05,340
Now, remember, we have not created the database table yet.

158
00:12:05,670 --> 00:12:07,830
We have just added a model over here.

159
00:12:08,130 --> 00:12:11,800
Now we need to go ahead and create an actual table from here.

160
00:12:11,940 --> 00:12:18,300
So in order to do that, we simply go ahead and type in Python three men and start by.

161
00:12:19,410 --> 00:12:20,670
Sequel, my great.

162
00:12:23,120 --> 00:12:29,870
Food, and then we type in zero zero zero one, because we have this zero zero zero one over here,

163
00:12:30,290 --> 00:12:33,320
we just type it in over here and now once we go ahead.

164
00:12:34,330 --> 00:12:39,020
And hit enter, as you can see, it has executed the create table command right over here.

165
00:12:39,580 --> 00:12:45,380
And once we go ahead and do that, the final command which we typed now is that we type in the magnet

166
00:12:45,400 --> 00:12:48,370
command, which is actually going to create the table for us.

167
00:12:48,790 --> 00:12:52,390
So I'll type in Python three managed by.

168
00:12:54,230 --> 00:12:55,950
Migrate and hit enter.

169
00:12:56,660 --> 00:13:02,780
So as you can see now, it applies all the migrations and our database tables are going to be successfully

170
00:13:02,780 --> 00:13:04,220
created in the back end.

171
00:13:04,270 --> 00:13:10,400
OK, so finally, we have created our database tables and in the next lecture, we will learn how exactly

172
00:13:10,400 --> 00:13:15,140
can we go ahead and add some data to these database tables right over here.

173
00:13:15,770 --> 00:13:17,630
So thank you very much for watching.

174
00:13:17,630 --> 00:13:19,700
And I'll see you guys next time.

175
00:13:20,060 --> 00:13:20,660
Thank you.

