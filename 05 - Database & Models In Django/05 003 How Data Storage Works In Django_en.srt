1
00:00:00,060 --> 00:00:05,820
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how you can store

2
00:00:05,820 --> 00:00:07,200
data in the database.

3
00:00:08,340 --> 00:00:15,150
So now that we have learned how to create a database, people by using models in Python and Django,

4
00:00:15,420 --> 00:00:19,300
let's now learn the steps involved in adding data to the database.

5
00:00:19,350 --> 00:00:24,450
So in the previous lecture, we have created this item table, which looks something of this sort.

6
00:00:24,780 --> 00:00:29,460
So it has those fields like the item name, the item description and the item price.

7
00:00:29,730 --> 00:00:35,490
And on the left hand side, you will also have the ID feel, which is nothing but the idea of the item

8
00:00:35,490 --> 00:00:36,540
which you enter there.

9
00:00:37,140 --> 00:00:42,590
So this idea feel is going to be automatically updated by Django and you don't have to worry about it.

10
00:00:43,230 --> 00:00:49,500
So this is how the item table looks right now because we have created the table, but we don't have

11
00:00:49,500 --> 00:00:52,420
any sort of data entered into that particular table.

12
00:00:53,280 --> 00:00:59,370
So now let's learn how exactly we can go ahead and enter some data into this particular item table.

13
00:01:00,050 --> 00:01:06,630
So Django actually gives us something which is called as the Database Abstraction API, which allows

14
00:01:06,630 --> 00:01:09,150
us to perform a wide variety of operations.

15
00:01:09,660 --> 00:01:16,610
So this database, Abstraction API allows us to create an object, update an object and delete an object.

16
00:01:17,280 --> 00:01:23,490
So whenever you want to enter some data into these fields right here, you actually need to go ahead

17
00:01:23,490 --> 00:01:24,670
and create an object.

18
00:01:25,260 --> 00:01:31,920
So the first step which is involved in entering data into the database in Django is to first go ahead

19
00:01:31,920 --> 00:01:33,010
and create an object.

20
00:01:33,030 --> 00:01:39,230
So I hope you guys already know the concept of classes and objects from object oriented programming.

21
00:01:39,720 --> 00:01:45,240
So whenever you need to create an object, you actually need to go ahead and you first need a class

22
00:01:45,450 --> 00:01:47,910
to create object of that particular class.

23
00:01:48,390 --> 00:01:54,690
Now, we also know from the past lectures that whenever you want to create a model, you have something

24
00:01:54,690 --> 00:01:56,010
which is called as a class.

25
00:01:56,190 --> 00:02:00,180
So we always make use of a class while we create a particular model.

26
00:02:00,750 --> 00:02:07,800
Now, in order to create an object of that particular model is nothing but creating object of that class,

27
00:02:08,100 --> 00:02:15,210
which means if we want to create the object to save data into these fields, we need to actually go

28
00:02:15,210 --> 00:02:17,460
ahead and create object of the item class.

29
00:02:18,710 --> 00:02:26,120
So now we can do that by using this database abstraction API and then we have our first step of creating

30
00:02:26,120 --> 00:02:26,760
the object.

31
00:02:27,170 --> 00:02:32,160
Now, the next step is to actually go ahead and save that object upon creation.

32
00:02:32,420 --> 00:02:36,770
So, first of all, we go ahead, create an object of this particular item class.

33
00:02:37,100 --> 00:02:43,010
And once we have created that object, we simply go ahead, enter some data into that object and then

34
00:02:43,010 --> 00:02:47,880
receive that particular thing using the same method which we have indicated right over here.

35
00:02:48,560 --> 00:02:53,930
And once we do that, that data is now going to be stored into this table right here.

36
00:02:54,770 --> 00:02:59,900
Now, if this thing sounds a little bit confusing, then in the upcoming lecture, we will go ahead

37
00:03:00,170 --> 00:03:06,050
and we will actually practically implement this particular process of creating an object of the item

38
00:03:06,050 --> 00:03:09,320
class and then saving data into our database.

39
00:03:09,710 --> 00:03:11,600
So thank you very much for watching.

40
00:03:11,600 --> 00:03:13,610
And I'll see you guys next time.

41
00:03:14,120 --> 00:03:14,750
Thank you.

