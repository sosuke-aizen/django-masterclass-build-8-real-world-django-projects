1
00:00:00,440 --> 00:00:05,900
Hello and welcome to this lecture, and in this lecture, we are going to learn how you can add some

2
00:00:05,900 --> 00:00:08,119
sort of data to our database.

3
00:00:08,520 --> 00:00:14,000
So in the previous lecture, we have actually created the database and the database tables using this

4
00:00:14,000 --> 00:00:14,950
model right here.

5
00:00:15,230 --> 00:00:20,940
And now let's learn the different ways in which we can add some sort of data to that table.

6
00:00:21,590 --> 00:00:28,280
So in order to add data over here, we are actually going to make use of python shell.

7
00:00:28,550 --> 00:00:32,840
So right now, if you go to the terminal, as you can see, the server is up and running.

8
00:00:33,380 --> 00:00:38,410
And if you want to add some data to the database, you need to open a python shell.

9
00:00:38,570 --> 00:00:43,120
So first of all, we are actually going to stop the server by pressing and control, see?

10
00:00:44,680 --> 00:00:51,520
And now, once the subway stopped, now in order to actually use the python shell, you simply go ahead

11
00:00:51,520 --> 00:00:52,990
and type in Python three.

12
00:00:54,190 --> 00:00:56,800
Managed by Shell.

13
00:00:57,220 --> 00:01:03,690
So now once you go ahead and hit enter now you will be into the interactive python shell, which Zango

14
00:01:03,700 --> 00:01:09,550
actually provides so that you can go ahead and interact with the database model.

15
00:01:10,300 --> 00:01:14,450
Now, let's assume you want to add some data to the item table.

16
00:01:14,470 --> 00:01:19,810
So right now you already know that from this particular class we have created a table which is called

17
00:01:19,810 --> 00:01:20,660
as item.

18
00:01:21,040 --> 00:01:27,580
So in order to interact with this particular item table, we first need to go ahead and import item

19
00:01:27,580 --> 00:01:28,080
in here.

20
00:01:29,560 --> 00:01:37,690
So now we go ahead and import item, so here what we do is that we want to import item from models,

21
00:01:37,690 --> 00:01:39,370
which is actually present in food.

22
00:01:39,700 --> 00:01:47,980
So this item is actually a model which is present in the models, and these models is actually present

23
00:01:48,220 --> 00:01:50,050
in the food applied over here.

24
00:01:50,230 --> 00:02:00,490
So in order to import that, we dipen from food dot models, we import the model, which is item because

25
00:02:00,490 --> 00:02:02,360
we actually want to add an item.

26
00:02:03,070 --> 00:02:04,990
Now, once we go ahead and enter.

27
00:02:06,210 --> 00:02:13,260
As you can see, this line of code will be executed and now we have item and put it now in order to

28
00:02:13,260 --> 00:02:17,970
see what kind of data is currently stored into the item table.

29
00:02:18,030 --> 00:02:25,320
You simply go ahead and type in item dot objects dot all, and that's actually going to list out all

30
00:02:25,320 --> 00:02:28,070
the items which just stored in the item table.

31
00:02:28,200 --> 00:02:35,010
Now these items are actually stored in a database, but with Django you can actually access all the

32
00:02:35,010 --> 00:02:37,980
rules of the item table in form of objects.

33
00:02:37,980 --> 00:02:42,870
And that's why we're able to type this command in and now only go ahead and hit enter.

34
00:02:43,140 --> 00:02:49,200
As you can see, you get a query set, which is an empty query set, which means that currently there

35
00:02:49,200 --> 00:02:55,650
is no item at all into this particular database or into this particular table, which is item.

36
00:02:56,220 --> 00:03:01,450
Now, once we know how to access those items, now we need to learn how to actually add them.

37
00:03:02,100 --> 00:03:07,890
So as I also mentioned, whatever things you want to enter into the item table, they can be treated

38
00:03:07,890 --> 00:03:08,910
as objects.

39
00:03:09,210 --> 00:03:16,290
So whenever you want to create some items, you can also go ahead and do that by using objects.

40
00:03:16,300 --> 00:03:23,280
So you will create an object lesson in that object as E and this is going to be an object of the type

41
00:03:23,280 --> 00:03:23,760
item.

42
00:03:24,180 --> 00:03:28,910
So this is similar to what we do when we create an object of a particular class.

43
00:03:28,920 --> 00:03:31,770
We go ahead, name that object or something.

44
00:03:32,040 --> 00:03:38,220
Then we specify the name of the class which states that this object belongs to that class, and now

45
00:03:38,220 --> 00:03:39,990
we can set the attributes as well.

46
00:03:40,920 --> 00:03:46,650
So now the attributes which we have other item, name, description and price, so we can set those

47
00:03:46,650 --> 00:03:47,740
attributes over here.

48
00:03:48,270 --> 00:03:56,520
So here I can type an item, underscore name equals, let's say the first item is going to be Peizer,

49
00:03:57,480 --> 00:04:02,940
then simply give a comma, specify the second attribute, which is item description.

50
00:04:03,780 --> 00:04:10,850
Let's say that's going to be something like, let's say, cheesy pizza.

51
00:04:11,670 --> 00:04:14,790
And now let's finally said the item price.

52
00:04:14,790 --> 00:04:18,300
So the item price is going to be, let's say twenty dollars.

53
00:04:19,110 --> 00:04:25,620
Okay, so now once we are done with this, simply hit enter and this object will now be created, but

54
00:04:25,620 --> 00:04:29,160
this object won't be yet stored to the database.

55
00:04:29,300 --> 00:04:35,910
Now, in order to store it to the database, you can simply go ahead and type in a dot, save and hit

56
00:04:35,910 --> 00:04:36,430
enter.

57
00:04:36,690 --> 00:04:41,220
And now this object is now stored into the database.

58
00:04:41,850 --> 00:04:48,180
Now, if you want to get the unique ID of this particular object, you can simply type in a dot ID.

59
00:04:48,450 --> 00:04:55,650
And as you can see, you get the value as one, which means that the ID of this item pizza is one.

60
00:04:56,340 --> 00:05:00,810
Now, another way to get an ID is by typing in a dot beaky.

61
00:05:00,840 --> 00:05:04,230
So if you type in a dot piggy, you're also going to get one.

62
00:05:04,500 --> 00:05:07,360
And Pecky Hill stands for Primary Key.

63
00:05:07,920 --> 00:05:14,130
So if you already have learned databases and tables, then you already might know what the primary is.

64
00:05:14,280 --> 00:05:16,530
But if you don't know, then a primary.

65
00:05:16,530 --> 00:05:22,140
He is nothing, but it's an attribute which uniquely defines a particular route.

66
00:05:22,170 --> 00:05:28,020
So in this case, the primary key is going to uniquely define each and every food item, which we are

67
00:05:28,020 --> 00:05:29,080
going to add in there.

68
00:05:29,970 --> 00:05:35,070
Okay, so now once we know how to do that, let's go ahead and create another food item over here so

69
00:05:35,070 --> 00:05:38,580
I can type in B equals hooligan type An item.

70
00:05:39,500 --> 00:05:48,200
And now let's add the attributes as item underscore name equals, let's say Burgo, let's see, the

71
00:05:48,200 --> 00:05:51,590
item description is going to be something like.

72
00:05:53,560 --> 00:06:01,960
Cheezy Berger, let's see the item price is equal to.

73
00:06:03,280 --> 00:06:09,190
Ten dollars and you'll notice that we have we are not using the quotation marks for this field right

74
00:06:09,190 --> 00:06:13,660
here because this is an integer field while these fields are actually correcto fields.

75
00:06:14,440 --> 00:06:17,900
OK, so now once we have that, let's simply go ahead, hit enter.

76
00:06:18,010 --> 00:06:19,770
And now let's also say that.

77
00:06:19,780 --> 00:06:21,160
So that's going to be Bidart.

78
00:06:21,160 --> 00:06:22,300
Save it.

79
00:06:22,300 --> 00:06:22,790
Enter.

80
00:06:22,930 --> 00:06:27,790
And now if you do without ID or without Pecky.

81
00:06:29,110 --> 00:06:35,650
Now, as you can see, the idea of that is going to be to that is because this is going to be the second

82
00:06:35,650 --> 00:06:38,520
item which we have added to the item stable.

83
00:06:39,460 --> 00:06:46,390
Now in order to see what all items are actually stowed into this item class, you can simply go ahead

84
00:06:46,390 --> 00:06:52,260
and again, type an item, not objects dot all.

85
00:06:52,870 --> 00:06:59,440
And now if you go ahead and hit enter, as you can see now, you'll notice that we have two items to

86
00:06:59,440 --> 00:07:03,370
do here, but still we don't get their names.

87
00:07:03,400 --> 00:07:09,370
So, for example, we are expected to get the name of the items over here like pizza and burger, but

88
00:07:09,370 --> 00:07:15,520
instead it shows the string representation of those objects as item, object one and item object two.

89
00:07:16,180 --> 00:07:18,030
So how exactly can we fix that?

90
00:07:18,490 --> 00:07:24,520
So to fix that, we need to define the string representation of this particular model over here in the

91
00:07:24,520 --> 00:07:25,890
model stockpile file.

92
00:07:26,350 --> 00:07:32,230
So here we need to explicitly mention that, OK, we do have some items or objects stored here.

93
00:07:32,590 --> 00:07:38,100
And whether we want to retrieve those objects, we need to display them in a specific manner.

94
00:07:38,500 --> 00:07:47,710
So we do that by using the Stia function or SDR method so we can type in def double underscore SDR and

95
00:07:47,710 --> 00:07:49,930
then will pass and Salvio.

96
00:07:50,320 --> 00:07:54,880
And then here we actually want to return the item name.

97
00:07:55,000 --> 00:07:59,110
So Liben return self-taught item name.

98
00:07:59,410 --> 00:08:06,880
And once we go ahead and do that, what this string function does is that it format the output and it's

99
00:08:06,880 --> 00:08:09,560
going to return anything which we have mentioned over here.

100
00:08:10,120 --> 00:08:15,820
So now when we go ahead and query the query said we are going to get the item name instead of getting

101
00:08:16,060 --> 00:08:19,210
these items, object one and item, object two over here.

102
00:08:19,960 --> 00:08:25,540
So now you cannot directly go ahead and query them again because of you query them again.

103
00:08:25,540 --> 00:08:27,040
You are going to get the same thing.

104
00:08:27,310 --> 00:08:33,159
But now you need to go ahead, exit the Python channel, restart the shell again, import the items

105
00:08:33,159 --> 00:08:36,580
again, and then only you can get the actual result.

106
00:08:36,730 --> 00:08:38,260
So let's go ahead and do that.

107
00:08:38,539 --> 00:08:43,990
So in order to exit the Python channel, you type an exit at enter.

108
00:08:44,440 --> 00:08:46,960
And now, as you can see, are out of the python shell.

109
00:08:47,080 --> 00:08:53,110
And now in order to again started back up, you type in Python three managed to by.

110
00:08:55,200 --> 00:08:55,740
Shiell.

111
00:08:56,840 --> 00:09:01,820
And now we again need to go through the same process, that is the first thing we need to do here is

112
00:09:01,820 --> 00:09:03,650
that you need to import items.

113
00:09:04,220 --> 00:09:06,340
So from who don't?

114
00:09:07,910 --> 00:09:11,510
Models import Eitam.

115
00:09:12,810 --> 00:09:16,920
And now you can do item dot objects.

116
00:09:20,180 --> 00:09:26,990
Not all, and now once you go ahead and hit and as you can see in the query said, you have two items,

117
00:09:26,990 --> 00:09:28,690
which is pizza and burger.

118
00:09:29,210 --> 00:09:35,480
And this was made possible only because we have used this string method, which basically tells Shango

119
00:09:35,490 --> 00:09:39,680
that this particular object needs to have this sort of representation.

120
00:09:39,680 --> 00:09:42,650
That is, we need to display the item name over there.

121
00:09:42,770 --> 00:09:49,730
So that means we have successfully added and retrieved data from database over here by using Python

122
00:09:49,740 --> 00:09:50,110
Shell.

123
00:09:50,420 --> 00:09:56,120
But as you might have noticed, that this method of adding data to the database is quite tedious and

124
00:09:56,120 --> 00:09:57,560
quite time consuming.

125
00:09:58,010 --> 00:10:04,520
And for the same reason, Shango actually provides us with an admin panel which easily allows us to

126
00:10:04,520 --> 00:10:07,490
add, retrieve, edit and delete data.

127
00:10:07,520 --> 00:10:13,850
So what we will do in the upcoming lecture is that we will learn what is the Django admin will learn

128
00:10:13,850 --> 00:10:20,600
how to create the admin user for this Django site so that we can easily add data without having to do

129
00:10:20,600 --> 00:10:22,010
all these things right here.

130
00:10:22,190 --> 00:10:26,300
So thank you very much for watching and I'll see you guys next time.

131
00:10:26,660 --> 00:10:27,290
Thank you.

