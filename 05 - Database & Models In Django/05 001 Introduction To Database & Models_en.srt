1
00:00:00,060 --> 00:00:06,270
Hello and welcome to this lecture and in this lecture, we will go ahead and learn about databases and

2
00:00:06,270 --> 00:00:08,000
models in Shango.

3
00:00:08,760 --> 00:00:12,070
So in this lecture, we will learn what exactly our models.

4
00:00:12,720 --> 00:00:18,990
So until now, we have learned how to create a simple website which returns something when we type in

5
00:00:18,990 --> 00:00:20,130
a particular URL.

6
00:00:20,250 --> 00:00:24,840
However, larger websites actually need a large amount of data to be stored.

7
00:00:26,040 --> 00:00:31,410
Now the data for these websites is actually stored in the form of a database.

8
00:00:31,440 --> 00:00:37,440
So whenever you see a huge website like Facebook or Google and when they have huge chunks of data,

9
00:00:37,680 --> 00:00:42,330
that particular data is actually stored into something which is called as a database.

10
00:00:43,320 --> 00:00:50,400
Now, the data in that particular database is stored in form of tables or database tables, so you cannot

11
00:00:50,400 --> 00:00:54,960
directly go ahead and just randomly store some data into the database.

12
00:00:55,110 --> 00:01:01,080
But instead, first of all, you need to create a database table and then store some data into that

13
00:01:01,080 --> 00:01:01,470
table.

14
00:01:01,620 --> 00:01:07,980
Now, what models allow you to do is that models actually allow you to create those database tables.

15
00:01:08,160 --> 00:01:14,070
So, for example, let's say if you have to create a website, what you can do is that in order to store

16
00:01:14,070 --> 00:01:20,610
data into the website, you first need a database and then in that database you go ahead and create

17
00:01:20,610 --> 00:01:21,230
a table.

18
00:01:21,600 --> 00:01:26,420
And now in order to create a table, you need something which is called as a model.

19
00:01:26,700 --> 00:01:30,240
So models are actually used to create database tables.

20
00:01:31,700 --> 00:01:38,150
So models are the blueprint, which can be used to create database tables, so whenever you create a

21
00:01:38,150 --> 00:01:44,450
particular database table, you sort of need a schema or you sort of need a blueprint to create that

22
00:01:44,450 --> 00:01:45,470
particular table.

23
00:01:45,740 --> 00:01:50,180
And that particular blueprint or schema is nothing but your model.

24
00:01:50,210 --> 00:01:53,030
So what exactly models are?

25
00:01:53,960 --> 00:01:59,540
So models are nothing, but they are classes in Python, which means that whenever you have to create

26
00:01:59,540 --> 00:02:06,800
a particular model in Python, that means whenever you have to create a particular schema in Zango for

27
00:02:06,800 --> 00:02:11,030
building a database table, you would need to create a Python class.

28
00:02:13,320 --> 00:02:18,600
So these particular models are actually created in the models taught by a file.

29
00:02:19,080 --> 00:02:24,450
So if you have a look at your project structure, you will notice that there is a file which is called

30
00:02:24,450 --> 00:02:26,090
as models, not by file.

31
00:02:26,490 --> 00:02:32,220
And whenever you need to create a database schema or model, you just need to go ahead and create a

32
00:02:32,220 --> 00:02:36,480
class in that particular file, which is model stored by file.

33
00:02:37,800 --> 00:02:39,730
Now, let's take an example.

34
00:02:40,020 --> 00:02:45,420
So let's say if you want to store details of a particular student, so for that purpose, you would

35
00:02:45,420 --> 00:02:47,760
need a student table in your database.

36
00:02:48,360 --> 00:02:54,240
So now whenever you want to create a student table, first of all, you need to build a model or a schema

37
00:02:54,240 --> 00:02:55,810
of that particular table.

38
00:02:55,980 --> 00:03:02,250
So you simply have to go ahead and create a class which is named a student in the models that profile.

39
00:03:03,300 --> 00:03:05,370
So that's all about models.

40
00:03:05,370 --> 00:03:11,070
And hopefully you guys be able to understand what models actually are and what purpose they serve.

41
00:03:11,490 --> 00:03:13,820
So in the upcoming lecture, we will go ahead.

42
00:03:14,100 --> 00:03:19,870
We will actually create a model and see how that model can be used to create a database table.

43
00:03:20,370 --> 00:03:24,420
So thank you very much for watching and I'll see you guys next time.

44
00:03:24,780 --> 00:03:25,320
Thank you.

