1
00:00:00,120 --> 00:00:06,360
Now that we know how data is actually stored in the database, let's now learn how to retrieve data

2
00:00:06,360 --> 00:00:09,330
or retrieve objects from that particular database.

3
00:00:10,200 --> 00:00:16,590
Now in Django, there's actually a method which allows you to retrieve data from the database and in

4
00:00:16,590 --> 00:00:21,480
Django, retrieve data from a database using something which is called as a query set.

5
00:00:22,170 --> 00:00:24,090
Now, what exactly is a query said?

6
00:00:24,450 --> 00:00:30,890
A query said is nothing, but it's actually a collection of objects stored into your database.

7
00:00:31,650 --> 00:00:37,530
Now, to retrieve these objects from a database, you need to construct a query set using something

8
00:00:37,530 --> 00:00:39,180
which is called as a manager.

9
00:00:40,350 --> 00:00:42,420
Now, what exactly is a manager?

10
00:00:42,420 --> 00:00:47,370
And a manager is nothing, but it's something which every model which you create has.

11
00:00:47,790 --> 00:00:55,800
So here is a model and every model has at least one manager and that default manager is called as objects.

12
00:00:57,280 --> 00:01:03,510
Now, if this sounds a little bit confusing, then let's actually go ahead and see how exactly aquarist

13
00:01:03,520 --> 00:01:05,379
that works by taking an example.

14
00:01:05,830 --> 00:01:08,980
So in the previous lecture, we had created the item table.

15
00:01:09,280 --> 00:01:11,910
We have added some data in the item table as well.

16
00:01:12,160 --> 00:01:15,700
So let's now learn how we can retrieve data from that item table.

17
00:01:16,210 --> 00:01:20,450
So this was item table right here or the item model we can see.

18
00:01:20,950 --> 00:01:27,340
So now let's say you want to get the data which is stored into this particular item table or item model.

19
00:01:27,490 --> 00:01:29,230
So how exactly can you do that?

20
00:01:29,740 --> 00:01:35,530
So in order to extract those objects or retrieve those objects from the item, you actually need a query

21
00:01:35,530 --> 00:01:38,470
set and a query said is nothing.

22
00:01:38,470 --> 00:01:41,560
But it's a collection of objects stored into your database.

23
00:01:41,830 --> 00:01:45,950
And whenever you want to get a query said, you actually need a manager.

24
00:01:46,390 --> 00:01:53,170
So here, once we have the model and a model here is nothing but item, then we have the manager.

25
00:01:53,500 --> 00:02:00,730
And as I also mentioned, each module has at least one manager and that manager is called as objects.

26
00:02:01,270 --> 00:02:07,120
Now, once we have the item and once we have the objects, we actually have an entire query set with

27
00:02:07,120 --> 00:02:09,160
us along with this.

28
00:02:09,880 --> 00:02:16,810
We also need something which is a method to actually get all the objects associated with that particular

29
00:02:16,810 --> 00:02:17,220
model.

30
00:02:17,500 --> 00:02:20,210
And that method is nothing but the old method.

31
00:02:21,070 --> 00:02:27,010
So combining all these three things together, we can actually form a query over here which is going

32
00:02:27,010 --> 00:02:29,260
to extract the data from the database.

33
00:02:29,260 --> 00:02:33,400
And that query is nothing but items, not objects, not all.

34
00:02:34,030 --> 00:02:35,920
So here item is a model.

35
00:02:36,500 --> 00:02:41,140
Objects is actually the manager of the model, which is item.

36
00:02:41,590 --> 00:02:48,130
And then we have this all method which actually makes sure that we get all the objects stored in that

37
00:02:48,130 --> 00:02:49,570
particular database table.

38
00:02:49,600 --> 00:02:55,130
So this is how querying a database or querying a model in Django actually works.

39
00:02:55,420 --> 00:03:01,000
So in the next lecture, we will go ahead and take a real life example to understand how to retrieve

40
00:03:01,000 --> 00:03:02,540
objects from a database.

41
00:03:02,950 --> 00:03:06,970
So thank you very much for watching and I'll see you guys next time.

42
00:03:07,330 --> 00:03:07,870
Thank you.

