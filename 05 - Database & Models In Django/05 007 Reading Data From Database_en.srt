1
00:00:00,180 --> 00:00:06,240
Hello and welcome to this lecture, and in this lecture, we are going to learn how exactly can we retrieve

2
00:00:06,240 --> 00:00:10,270
the data from the database and actually displayed on our website.

3
00:00:10,770 --> 00:00:18,750
So right now, if you go to a local hotspot, it slash who, as you can see, we still have this plain

4
00:00:18,750 --> 00:00:24,510
hello world Waldor here, which we have actually passed it in over this view right over here because

5
00:00:24,510 --> 00:00:25,890
we have passed in HelloWallet.

6
00:00:26,160 --> 00:00:28,020
This page actually shows hello world.

7
00:00:28,320 --> 00:00:33,780
But right now, what we wish to do is that we wish to extract the data which we have added into our

8
00:00:33,780 --> 00:00:36,540
database and display that data up over here.

9
00:00:37,230 --> 00:00:38,930
So how exactly can we do that?

10
00:00:38,940 --> 00:00:45,740
So even before we learn how to do that, let's learn how we actually want to design our application.

11
00:00:45,870 --> 00:00:51,270
So in our application, what we want to do is that we want to list out all the food items over here,

12
00:00:51,870 --> 00:00:55,280
like the pizza bubble and everything like that.

13
00:00:56,100 --> 00:01:02,040
And when you click on that particular item, it should actually take you to the details page of that

14
00:01:02,040 --> 00:01:03,020
particular item.

15
00:01:04,379 --> 00:01:06,020
So let's learn how we can do that.

16
00:01:06,150 --> 00:01:11,820
So our first job is to display all the food items over here on the index page, which we have in our

17
00:01:11,820 --> 00:01:12,480
database.

18
00:01:13,030 --> 00:01:15,090
So let's go ahead and quickly do that.

19
00:01:15,720 --> 00:01:22,140
So first of all, we need to go into The View stockpile file right here and we need to import the model,

20
00:01:22,140 --> 00:01:23,010
which is items.

21
00:01:23,170 --> 00:01:28,440
So we will again type in from the models import item.

22
00:01:28,860 --> 00:01:34,920
And once we have this model, we can now go ahead, play around with this model and use it where we

23
00:01:34,920 --> 00:01:35,280
like.

24
00:01:35,730 --> 00:01:42,490
So in here, what we will do is that we are going to use the same code which we have used over here.

25
00:01:42,510 --> 00:01:49,050
So if you scroll up a little bit, as you can see, you will find that in order to retrieve all the

26
00:01:49,050 --> 00:01:54,730
object list, we have used this code right here, which is item that objects dot all.

27
00:01:55,200 --> 00:02:01,040
So whenever we go ahead and do that, we get the items, as are Pizza and Bhogle over here.

28
00:02:01,290 --> 00:02:03,710
So we'll use the same code over there as well.

29
00:02:04,230 --> 00:02:10,139
So you simply type an item, dot objects dot all.

30
00:02:10,979 --> 00:02:17,190
And now we actually need to store this data into some sort of a list or some sort of a variable.

31
00:02:17,400 --> 00:02:23,200
So I'll type this thing as item and this list equals this thing.

32
00:02:23,850 --> 00:02:27,970
So now we have all this particular data in the item list.

33
00:02:28,470 --> 00:02:33,870
Now the only thing which we need to do is that we need to go ahead and display that data up over here

34
00:02:33,870 --> 00:02:37,330
or return that data via HTP response.

35
00:02:37,830 --> 00:02:39,950
So now I can type an item list here.

36
00:02:40,200 --> 00:02:46,170
And what this basically does is that, first of all, it gets all the objects inside the model item.

37
00:02:46,530 --> 00:02:47,940
It saves them over here.

38
00:02:48,060 --> 00:02:53,970
And then we simply pass that up over here so that we get this as a response on the page.

39
00:02:54,360 --> 00:02:59,190
So now let me just go ahead, save that and make sure that the server is up and running.

40
00:02:59,790 --> 00:03:02,040
And now if you actually go to.

41
00:03:03,370 --> 00:03:04,130
This you are.

42
00:03:06,180 --> 00:03:12,510
As you can see, you'll get pizza and burrito here, because those are the two items which we have added

43
00:03:12,510 --> 00:03:13,680
to our database.

44
00:03:14,280 --> 00:03:22,260
Now, if you go ahead and go to the admin panel, so if you go to slash admin and if you log in over

45
00:03:22,260 --> 00:03:22,620
here.

46
00:03:25,480 --> 00:03:32,710
And now let's try to add an item over here, let's say we also add a cheese burger.

47
00:03:34,080 --> 00:03:37,230
And let's say the description is this is a.

48
00:03:38,490 --> 00:03:43,260
Bobo, let's say the price is eight dollars, click on Save.

49
00:03:44,210 --> 00:03:50,030
The item added over here and now if you go ahead and refresh, this cheeseburger is also going to be

50
00:03:50,030 --> 00:03:51,420
displayed over here as well.

51
00:03:51,890 --> 00:03:58,910
So that means we have successfully retrieved all the items from the database in here on our index speech.

52
00:04:00,140 --> 00:04:04,760
But you'll notice that these items are not actually represented properly.

53
00:04:04,790 --> 00:04:07,190
That is, they don't have any proper formatting.

54
00:04:07,190 --> 00:04:10,400
They don't have any sort of a different color or anything like that.

55
00:04:10,910 --> 00:04:14,620
So what if you actually want to style these items properly?

56
00:04:15,320 --> 00:04:19,250
So what you have to do here is that you have to style them over here.

57
00:04:19,260 --> 00:04:24,590
But as you can see, there is absolutely no way you can go ahead and do that over here.

58
00:04:25,220 --> 00:04:31,640
So how exactly do you make sure that the format of your page is correct while you retrieve those items

59
00:04:31,640 --> 00:04:32,600
from the database?

60
00:04:33,170 --> 00:04:37,180
And we can do that by using something which is called as a template.

61
00:04:37,610 --> 00:04:44,740
So what template allow you to do is that they basically allow you to have different sort of HTML files.

62
00:04:44,740 --> 00:04:50,570
So what you can do using templates is that you can have a template folder here which is going to contain

63
00:04:50,570 --> 00:04:52,310
an entire HTML document.

64
00:04:52,580 --> 00:04:59,270
And now instead of rendering this item list over here, we are going to render an entire each HTML document

65
00:04:59,270 --> 00:04:59,660
itself.

66
00:04:59,660 --> 00:05:07,310
And by using templates, we will be able to better format the content of each HTML Web page and also

67
00:05:07,310 --> 00:05:11,270
pass on this data to the template as well, which is the database data.

68
00:05:11,420 --> 00:05:16,280
So in the next lecture, what we will do is that we will learn about how templates actually work in

69
00:05:16,280 --> 00:05:22,500
Zango and we will create our very first template and also learn how to parse this data through the template.

70
00:05:23,030 --> 00:05:27,170
So thank you very much for watching and I'll see you guys next time.

71
00:05:27,680 --> 00:05:28,280
Thank you.

