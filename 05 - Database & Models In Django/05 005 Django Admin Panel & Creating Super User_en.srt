1
00:00:00,780 --> 00:00:06,840
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to create the

2
00:00:06,900 --> 00:00:14,430
super user and how to access the Chango admin panel so that we can go ahead and easily add, update,

3
00:00:14,430 --> 00:00:17,220
delete and modify the data in our database.

4
00:00:17,740 --> 00:00:21,270
So first of all, what you need to do is that you need to go to the terminal.

5
00:00:21,270 --> 00:00:25,160
And if you are actually into the Python channel, simply exit out of that.

6
00:00:25,830 --> 00:00:33,440
And now the first command which we use in Django to create a super user is create super user comment.

7
00:00:34,080 --> 00:00:35,400
So let's go ahead and do that.

8
00:00:35,760 --> 00:00:38,340
So let's step in Python three Manege.

9
00:00:39,580 --> 00:00:40,960
Dogged by.

10
00:00:42,110 --> 00:00:45,150
Create a super user.

11
00:00:45,590 --> 00:00:51,710
So what this command does is that it's going to create the admin user for our current Django project.

12
00:00:52,220 --> 00:00:57,040
So when I go ahead and execute that, it's going to ask me for the user name.

13
00:00:57,470 --> 00:01:05,209
So if I actually want to set my username to my name over here, it asks me to leave it as blank.

14
00:01:05,220 --> 00:01:09,120
So what I'll do here is that I'll type in my name here and hit enter.

15
00:01:09,380 --> 00:01:11,660
Now it's going to ask me for the email address.

16
00:01:12,020 --> 00:01:19,460
I'll simply type in some random dummy email address here, ABC and ABC dot com.

17
00:01:20,390 --> 00:01:21,980
And now for the password.

18
00:01:21,980 --> 00:01:30,130
Let's say I said the password as super user, it needs to accept the password again.

19
00:01:30,140 --> 00:01:31,490
So super.

20
00:01:32,490 --> 00:01:33,170
User.

21
00:01:33,720 --> 00:01:36,570
OK, so now the super user successfully created.

22
00:01:37,110 --> 00:01:43,880
So now once this user is created now you can actually log in into the Django admin with these credentials.

23
00:01:44,280 --> 00:01:49,480
So even before we log into the Django admin, we need to make sure that we have started up the server.

24
00:01:50,490 --> 00:01:53,760
So let's type in Python three managed by.

25
00:01:54,760 --> 00:01:56,040
Run so.

26
00:01:57,260 --> 00:01:58,820
Now the subway is up and running.

27
00:01:59,300 --> 00:02:03,770
Let's visit this particular you are so let me open up the Web browser.

28
00:02:05,790 --> 00:02:12,390
And now let's actually go ahead and visit the you are so right now.

29
00:02:13,330 --> 00:02:20,410
As you can see, we get nothing over here and now if we go to localhost board eight thousand slash admin,

30
00:02:20,950 --> 00:02:26,830
as you can see, you are going to get this in from right over here in which you can log in with the

31
00:02:26,830 --> 00:02:27,540
credentials.

32
00:02:27,550 --> 00:02:33,570
So here I'll type in my name and then I'll type in the password as super user.

33
00:02:34,300 --> 00:02:39,630
And when I go ahead and log in, as you can see, we are now logged into this admin panel.

34
00:02:39,910 --> 00:02:43,870
And now, as you can see, it has some detail here.

35
00:02:43,900 --> 00:02:46,480
So first of all, it's a site administration.

36
00:02:46,480 --> 00:02:51,430
Then it has the authentication and authorization for the different users and different groups.

37
00:02:51,920 --> 00:02:57,490
But as you might have noticed here, we were expected to find our database over here or the database

38
00:02:57,490 --> 00:02:58,860
table, which is item.

39
00:02:59,410 --> 00:03:02,530
But as you can see, there is no item table right over here.

40
00:03:02,530 --> 00:03:07,550
And now we cannot go ahead and actually modify the contents of the item table.

41
00:03:08,200 --> 00:03:15,100
So how exactly do we go ahead and include that item table or the item model so that that item is going

42
00:03:15,100 --> 00:03:16,300
to be displayed over here?

43
00:03:17,050 --> 00:03:21,760
So for that, what you need to do is that you need to simply hop back to your code editor.

44
00:03:21,880 --> 00:03:28,960
And in here, first of all, if you go to the admin dot file, which is this file right here, as you

45
00:03:28,960 --> 00:03:33,670
can see, it actually gives you an option to register your models here.

46
00:03:33,850 --> 00:03:35,690
And what do we actually mean by that?

47
00:03:36,190 --> 00:03:43,210
So let's say if your site has five models and out of those five models, you only want to display three

48
00:03:43,210 --> 00:03:47,220
models of your main project in the admin panel.

49
00:03:47,770 --> 00:03:53,800
So whatever model you actually want to display inside your admin panel, you simply go ahead and register

50
00:03:53,800 --> 00:03:55,180
that model up over here.

51
00:03:55,750 --> 00:04:03,610
So right now, we want to register the model, which is item so I can simply type in admin, dot site,

52
00:04:03,610 --> 00:04:07,830
dot register and I can type an item over here.

53
00:04:08,590 --> 00:04:11,250
But right now the item model is not important.

54
00:04:11,320 --> 00:04:13,540
So let's go ahead and import that as well.

55
00:04:13,550 --> 00:04:17,860
So I'll type in from DOT models import item.

56
00:04:18,970 --> 00:04:25,090
So now once we go ahead and do that and once you go back over here and hit refresh, now as you can

57
00:04:25,090 --> 00:04:28,660
see, you have this model, which is items.

58
00:04:29,410 --> 00:04:34,750
So now if we go ahead and click that, as you can see, we also get the two items over here, which

59
00:04:34,750 --> 00:04:36,760
is burger and pizza.

60
00:04:36,940 --> 00:04:41,230
And now you also have the functionality to go ahead and edit or delete them.

61
00:04:41,770 --> 00:04:47,530
So now if you want to actually go ahead and delete them, simply open this thing up and you can click

62
00:04:47,530 --> 00:04:49,870
this delete option and click on.

63
00:04:49,870 --> 00:04:50,840
Yes, I'm sure.

64
00:04:50,860 --> 00:04:55,030
And as you can see, the item will be deleted from the database.

65
00:04:55,450 --> 00:04:59,470
And whenever you want to add an item, you simply click on add item.

66
00:05:00,220 --> 00:05:03,520
You add the item name, let's say a burrito.

67
00:05:04,570 --> 00:05:07,410
Let's say the description is fluffy burrito.

68
00:05:08,080 --> 00:05:10,720
Let's see the item price is thirty dollars.

69
00:05:11,080 --> 00:05:12,160
Click on Save.

70
00:05:12,160 --> 00:05:16,290
And as you can see, that item is now going to be added up over here.

71
00:05:16,900 --> 00:05:22,960
And let's say if you want to edit the item for some reason, you can simply go ahead, click on that.

72
00:05:23,110 --> 00:05:28,510
And let's say if you want to change the price to, let's say fifteen dollars, simply go ahead and click

73
00:05:28,510 --> 00:05:31,720
save and now the price will be updated right over here.

74
00:05:32,170 --> 00:05:39,730
So this is how you can go ahead and use the admin panel to kind of manage your entire site's data,

75
00:05:39,730 --> 00:05:46,180
because the Django admin panel gives you a functionality to add data to your database tables, modify

76
00:05:46,180 --> 00:05:48,710
that and also delete any sort of data.

77
00:05:48,940 --> 00:05:50,570
So that's it for this lecture.

78
00:05:50,710 --> 00:05:56,850
Hopefully you guys were able to understand what is Django admin and how to access the admin panel.

79
00:05:57,160 --> 00:06:03,550
So whenever you want to access the admin panel, you simply need to go ahead, go to localhost 40000

80
00:06:03,550 --> 00:06:06,310
slash admin and it will take you to the admin panel.

81
00:06:06,700 --> 00:06:08,980
Just make sure that your server is up and running.

82
00:06:09,310 --> 00:06:13,930
And also you need to create a super user before you access the admin panel.

83
00:06:14,320 --> 00:06:17,230
Now you need to create the super user only once.

84
00:06:17,230 --> 00:06:23,200
And once the user is created, you can go ahead and access the admin panel any time when required.

85
00:06:23,230 --> 00:06:30,100
So now you can simply log out and then you can simply go ahead and again log in by visiting this particular

86
00:06:30,100 --> 00:06:34,720
you are all type in your username and password and you should be good to go.

87
00:06:35,680 --> 00:06:41,920
So that's it about the admin panel and in the upcoming lecture we will move into the more advanced stuff.

88
00:06:42,280 --> 00:06:48,340
So what we will do in the upcoming lectures that will go ahead and write a few more views and we'll

89
00:06:48,340 --> 00:06:50,230
see how that thing is going to work.

90
00:06:50,260 --> 00:06:52,150
So thank you very much for watching.

91
00:06:52,150 --> 00:06:54,280
And I'll see you guys next time.

92
00:06:54,580 --> 00:06:55,180
Thank you.

