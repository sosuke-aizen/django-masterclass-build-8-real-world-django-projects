1
00:00:00,300 --> 00:00:05,910
Hello and welcome to this lecture and in this lecture, let's go ahead and see if we can go ahead and

2
00:00:05,910 --> 00:00:08,220
write another view and make it work.

3
00:00:08,820 --> 00:00:12,720
So let's go ahead and create a new view and let's call it as items.

4
00:00:12,850 --> 00:00:18,360
So in order to create a view, let's go in here and let's define a function, because a view is nothing

5
00:00:18,360 --> 00:00:19,900
but just a python function.

6
00:00:20,520 --> 00:00:24,710
So this is going to be def let's see item.

7
00:00:24,720 --> 00:00:26,430
Let's call this view as item.

8
00:00:27,770 --> 00:00:34,760
And now it's going to accept the same request over here and now it's going to return a response, so

9
00:00:34,760 --> 00:00:38,390
it's going to return and HTP response and let's say it returns.

10
00:00:38,390 --> 00:00:44,040
A response would say something like this is an item veto.

11
00:00:44,450 --> 00:00:50,000
OK, so now once we have this for you, we already know that we need to connect disp with the particular.

12
00:00:50,000 --> 00:00:52,460
You are all particular, you are Pashtun.

13
00:00:53,030 --> 00:00:59,510
So now we go into the general pattern for the food up and in here we simply type in BAB.

14
00:01:00,690 --> 00:01:07,530
And let's see, the usual pattern for this is going to be, let's see item, so you need to type an

15
00:01:07,530 --> 00:01:14,010
item here and then give a slash and now you just need to give a comma and you need to attach the view,

16
00:01:14,010 --> 00:01:17,580
which is item over here so you can type in view.

17
00:01:17,580 --> 00:01:18,570
Dot item.

18
00:01:20,240 --> 00:01:26,810
Gomaa name equals Eitam, then just make sure to give a comma over here and you should be good to go

19
00:01:27,140 --> 00:01:28,770
and this should actually be used.

20
00:01:28,790 --> 00:01:32,750
And now once we go ahead and save the code, they should actually work fine.

21
00:01:32,760 --> 00:01:38,600
And remember, now, you don't need to attach this thing with the mean you Alstott profile because that

22
00:01:38,600 --> 00:01:40,790
is actually already included over here.

23
00:01:41,750 --> 00:01:47,120
So now let's make sure that the server is up and running and yeah, it's up and running.

24
00:01:47,690 --> 00:01:52,850
And now if you go back over here and now if you just type in food.

25
00:01:54,280 --> 00:02:01,550
You will get the hello world, and if you type in food slash item, you will get the result as this

26
00:02:01,550 --> 00:02:02,690
is an item view.

27
00:02:02,990 --> 00:02:06,630
So that's how you essentially go ahead and create a new view here.

28
00:02:07,190 --> 00:02:12,080
So just as a practice, what you can do is that you can just go ahead and create a bunch of your views

29
00:02:12,080 --> 00:02:17,550
in here and try to attach those different views to different you all patterns in here.

30
00:02:17,690 --> 00:02:20,660
So that's actually going to be a good practice for you to do so.

31
00:02:21,130 --> 00:02:26,810
So if you have already understood these concepts, you can simply post this particular video and you

32
00:02:26,810 --> 00:02:30,830
can start writing your own views and start connecting them to different parts.

33
00:02:30,890 --> 00:02:32,930
So this will give you a general practice.

34
00:02:33,980 --> 00:02:39,650
So now one more interesting thing which I would like to mention here is that you can not only passing

35
00:02:39,980 --> 00:02:46,550
plain simple text here, but you can also pass an HTML in this particular HTP response.

36
00:02:47,030 --> 00:02:52,880
So, for example, let's say if you want to pass this particular thing as a heading, you can also include

37
00:02:52,880 --> 00:03:00,100
some e-mail tags in here so you can type in each one over here and you can have something like slash

38
00:03:00,200 --> 00:03:02,100
each one over here as the closing tag.

39
00:03:02,420 --> 00:03:04,640
And now let's see what exactly happens.

40
00:03:04,880 --> 00:03:13,040
So now if I go back over here and hit refresh, as you can see now we get a response as an e-mail instead

41
00:03:13,040 --> 00:03:14,870
of the plain simple text.

42
00:03:16,460 --> 00:03:23,540
So that means we can actually pass an entire HTML files as a response in a particular view, and this

43
00:03:23,540 --> 00:03:25,670
is actually not a proper way to do so.

44
00:03:25,940 --> 00:03:33,440
Instead, Jianguo actually has a functionality to use templates so that we can have entire HTML files

45
00:03:33,440 --> 00:03:38,030
in a different directory called as templates and later use them over here.

46
00:03:38,810 --> 00:03:44,080
So we are actually going to discuss about templates in the upcoming sections or upcoming lectures.

47
00:03:44,390 --> 00:03:49,820
But in the next lecture, we are going to start learning about a database and models because that is

48
00:03:49,820 --> 00:03:53,360
one of the most important reasons why we use Django.

49
00:03:54,320 --> 00:03:58,460
So thank you very much for watching and I'll see you guys next time.

50
00:03:58,730 --> 00:03:59,360
Thank you.

