1
00:00:00,720 --> 00:00:06,360
So now, in order to connect this view with you all, we need to define you are the patterns over here

2
00:00:06,360 --> 00:00:08,970
as well, just as they have defined over here.

3
00:00:09,750 --> 00:00:13,380
So what I will do here is that I'll just copy this particular code.

4
00:00:13,770 --> 00:00:14,880
Wasted an hour here.

5
00:00:15,180 --> 00:00:17,280
And don't worry, we are not going to use that.

6
00:00:17,880 --> 00:00:22,120
So we'll just get rid of this code and write our very own path from scratch.

7
00:00:22,770 --> 00:00:28,170
So in here, I'll type in the path as let's see blank.

8
00:00:28,530 --> 00:00:35,190
So let's see when a user actually visits a blank pad, he should be redirected to this particular view.

9
00:00:36,140 --> 00:00:42,920
Which is index, so how do we connect this index view to this particular path right here?

10
00:00:43,730 --> 00:00:47,670
So for that very purpose, first of all, we need to import views.

11
00:00:48,260 --> 00:00:52,250
So what you can do here is that you can type in from dot.

12
00:00:52,640 --> 00:00:55,010
And Dot actually means the current directory.

13
00:00:55,040 --> 00:00:58,850
So from the current directly, we first need to import views.

14
00:00:59,750 --> 00:01:04,150
So this is actually going to just go ahead and import the views which we have in here.

15
00:01:06,060 --> 00:01:14,070
And now what we can do here is that we can add the view, which is the index view, so in here you just

16
00:01:14,070 --> 00:01:22,110
need to type in views dot index, comma, name equals index, and then you just need to give a coal

17
00:01:22,110 --> 00:01:22,680
mine here.

18
00:01:23,130 --> 00:01:30,210
So what this does is that it attaches this particular view with this particular part, which is empty

19
00:01:30,210 --> 00:01:30,650
for now.

20
00:01:31,110 --> 00:01:34,140
And now, as you can see, it also shows an arrow here.

21
00:01:34,140 --> 00:01:36,240
It says undefined variable path.

22
00:01:36,750 --> 00:01:41,510
And that's because you need to import Padthaway here, just as they have done over here.

23
00:01:41,880 --> 00:01:46,100
So you just need to copy that and you need to piece it up over here.

24
00:01:46,290 --> 00:01:48,220
And that's going to work just fine.

25
00:01:49,020 --> 00:01:52,770
So now we have the you are all configured for this particular view.

26
00:01:53,910 --> 00:01:58,600
But there is one issue with this particular you are stoppie file.

27
00:01:59,010 --> 00:02:05,820
So as you can see now we have to you Alstott, profile the Alstott profile of our main project, which

28
00:02:05,820 --> 00:02:10,350
is my site, and this is a unstopping file of our food app.

29
00:02:10,710 --> 00:02:17,790
Now, the problem is Django only looks at this when you are stored by file and it does not have any

30
00:02:17,790 --> 00:02:20,070
obligation to look at this particular file.

31
00:02:20,580 --> 00:02:25,770
So now how exactly can we tell Django that it also needs to look up to this particular file?

32
00:02:26,340 --> 00:02:32,610
So for that very purpose, what we simply do is that we attach or include this file in this particular

33
00:02:32,610 --> 00:02:33,420
file right here.

34
00:02:34,200 --> 00:02:35,900
So how exactly can you do that?

35
00:02:36,480 --> 00:02:40,500
We can do that by just going in here and I can type in.

36
00:02:42,610 --> 00:02:48,820
Over here and in here, I can type in a path such as, uh, let's see, food slash.

37
00:02:49,300 --> 00:02:55,900
So this actually means that whenever the user types in the you are less food instead of Admon, he should

38
00:02:55,900 --> 00:02:59,320
be redirected to this particular view, which is this view right here.

39
00:02:59,920 --> 00:03:07,180
So now we want to tell Django that, OK, whenever it encounters this point right here, we actually

40
00:03:07,180 --> 00:03:11,320
want to include a file, which is this food dodgeballs file.

41
00:03:12,860 --> 00:03:19,460
So you can just go ahead and type in include and to this particular method, you just need to pass in

42
00:03:19,460 --> 00:03:22,900
this file right here, which is present in the foods app.

43
00:03:22,910 --> 00:03:26,510
So you type in food, don't you?

44
00:03:26,510 --> 00:03:31,370
Are or you are, and simply make sure to have a coal mine here.

45
00:03:32,000 --> 00:03:36,860
And now, as you can see, this thing gives you an error because you need to import include.

46
00:03:37,250 --> 00:03:38,690
So you just need to type in.

47
00:03:39,570 --> 00:03:42,960
Include over here, give a comma and you should be good to go.

48
00:03:43,380 --> 00:03:45,870
So now once this thing is done, we are good to go.

49
00:03:46,080 --> 00:03:52,500
And now this line of code basically means that we have connected this particular you are Lizotte profile

50
00:03:52,500 --> 00:03:54,120
with this particular file right here.

51
00:03:54,990 --> 00:03:58,560
Now, let's understand how all these three things actually work.

52
00:03:58,750 --> 00:04:05,070
And even before telling you that, I need to now go ahead and run the server and let's see what exactly

53
00:04:05,070 --> 00:04:07,440
happens when you type in the you are as food.

54
00:04:08,070 --> 00:04:09,330
So let me just go ahead.

55
00:04:09,480 --> 00:04:11,550
And the server is already up and running.

56
00:04:11,560 --> 00:04:18,630
So if your server is not running, make sure to run your server, go to your app and here you simply

57
00:04:18,630 --> 00:04:19,500
need to type in.

58
00:04:21,910 --> 00:04:29,710
Localhost bought thousand slash food, so when I go ahead and hit enter, as you can see, we successfully

59
00:04:29,710 --> 00:04:31,920
get a view, which is Hello world.

60
00:04:32,530 --> 00:04:35,080
So how exactly have we done that?

61
00:04:35,560 --> 00:04:38,870
So now let's understand the working behind this.

62
00:04:39,040 --> 00:04:46,060
So what happens in here is that whenever you type in the you are as localhost thousand 8000 food, Jianguo

63
00:04:46,060 --> 00:04:51,210
is actually going to look up into the ultimate profile and it's going to look up for the you are all

64
00:04:51,220 --> 00:04:54,560
patterns which matches up that you are all which you have entered.

65
00:04:54,970 --> 00:04:57,640
So you have actually entered the world as food.

66
00:04:57,940 --> 00:05:01,780
So this thing is actually going to match up with this particular pattern right here.

67
00:05:02,410 --> 00:05:08,650
So now, after looking at this particular pattern, Jianguo actually knows that it actually has to look

68
00:05:08,650 --> 00:05:10,820
at the food dodgeballs file.

69
00:05:11,260 --> 00:05:16,390
So now the control actually goes over here and it's actually going to look for these patterns over here.

70
00:05:16,540 --> 00:05:19,930
So right now, the pattern over here is empty.

71
00:05:20,270 --> 00:05:26,910
That basically means that in front of food, you don't have anything over here.

72
00:05:27,610 --> 00:05:32,170
So that's going to match up with this particular path or this particular pattern over here.

73
00:05:32,530 --> 00:05:37,850
And hence is going to now look at the view which is connected with this particular pattern.

74
00:05:38,380 --> 00:05:45,370
So the view which is connected with this pattern is the index view and hence it's going to go into the

75
00:05:45,370 --> 00:05:46,750
view start profile.

76
00:05:47,080 --> 00:05:52,620
It's going to look for The View, which is called an index, and it's going to execute this function.

77
00:05:52,630 --> 00:05:56,560
And the job of this function is to return a response, which is Hello world.

78
00:05:56,860 --> 00:06:00,920
And that's why we get the response as Hello Waldo here.

79
00:06:01,330 --> 00:06:04,850
So that's how all the things work internally in Shango.

80
00:06:04,870 --> 00:06:10,390
So whenever you want to return a response, you just simply go ahead and write in a view, connect that

81
00:06:10,390 --> 00:06:14,360
view with an appropriate you are a pattern and you should be good to go.

82
00:06:14,680 --> 00:06:21,520
So that means whenever you go ahead and type in some, you are well, you will get that particular response

83
00:06:21,760 --> 00:06:23,080
for that particular you are.

84
00:06:23,890 --> 00:06:25,840
So now you can also configure this.

85
00:06:25,840 --> 00:06:26,840
You are little differently.

86
00:06:27,010 --> 00:06:29,740
So let's say, for example, we want to get this.

87
00:06:29,740 --> 00:06:33,310
Hello world on typing slash food slash.

88
00:06:33,310 --> 00:06:33,730
Hello.

89
00:06:34,030 --> 00:06:35,860
So how exactly can you do that?

90
00:06:36,490 --> 00:06:40,620
So in order to do that, you simply need to go ahead and type in.

91
00:06:41,440 --> 00:06:43,150
Hello, slash over here.

92
00:06:44,020 --> 00:06:48,610
So now if you go ahead, save the code and if you go back over here.

93
00:06:49,670 --> 00:06:55,220
And now if you simply go ahead and type food, you won't get any response over here and said you will

94
00:06:55,220 --> 00:06:56,020
get an error.

95
00:06:56,270 --> 00:07:01,820
But now if you go ahead and if you type in slash food slash hello, you will get the response as hello

96
00:07:01,820 --> 00:07:06,890
world, because we have configured the pattern in a way that it basically goes ahead.

97
00:07:07,010 --> 00:07:12,530
It's going to take food and then in front of that, it's going to open this hello right here.

98
00:07:12,680 --> 00:07:16,510
So for now, let's get rid of that and just make it as it was before.

99
00:07:16,880 --> 00:07:24,020
So hopefully you guys be able to understand how to connect views with a particular you are so that we

100
00:07:24,020 --> 00:07:25,970
can get an appropriate response.

101
00:07:26,660 --> 00:07:30,590
So thank you very much for watching and I'll see you guys next time.

102
00:07:31,010 --> 00:07:31,610
Thank you.

