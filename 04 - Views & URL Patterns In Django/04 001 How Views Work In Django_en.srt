1
00:00:00,060 --> 00:00:06,210
Hello and welcome to this lecture and in this lecture will go ahead and learn how jangle apps actually

2
00:00:06,210 --> 00:00:06,580
work.

3
00:00:06,990 --> 00:00:12,630
So before writing any kind of code in here, first of all, we will understand how Django works and

4
00:00:12,900 --> 00:00:16,260
how the code which we are going to write, is going to get executed.

5
00:00:16,760 --> 00:00:20,820
Let's first discuss about what exactly is an app in Django.

6
00:00:21,300 --> 00:00:27,270
So let's say if you're building a website so this particular website can have multiple sections, for

7
00:00:27,270 --> 00:00:32,540
example, it can have the block section, the forum section and the comment section.

8
00:00:33,000 --> 00:00:40,590
So an entire huge website can be split up into multiple things and these things can be called as apps

9
00:00:40,590 --> 00:00:41,290
in Django.

10
00:00:41,460 --> 00:00:48,060
So whenever you are developing a big website or a big project in Django, it can be comprised of multiple

11
00:00:48,060 --> 00:00:48,480
apps.

12
00:00:48,720 --> 00:00:55,110
And these multiple apps combined together is going to be your entire website or your entire Django project.

13
00:00:55,140 --> 00:01:00,600
Now, these particular sections or these particular chunks of your website are called as apps.

14
00:01:00,960 --> 00:01:07,410
And in each Django project, you can have as many number of apps as you want and now you can just go

15
00:01:07,410 --> 00:01:10,110
ahead and write code for each one of those individual.

16
00:01:10,270 --> 00:01:14,340
So now let's go ahead and learn how apps actually look like in Django.

17
00:01:15,180 --> 00:01:19,710
So an app in Django is nothing, but it's a collection of a bunch of other files.

18
00:01:19,920 --> 00:01:25,120
So if you have a look over here, these are the different file, which a particular app actually has.

19
00:01:25,500 --> 00:01:31,410
So whenever you go ahead and create an app in Django, by default, these many files are going to be

20
00:01:31,410 --> 00:01:33,570
contained in that particular app folder.

21
00:01:33,600 --> 00:01:38,310
Now, whenever you go ahead and create an app, you will get it directly with that particular app name.

22
00:01:38,340 --> 00:01:43,740
So, for example, if you create an app named as food, you will get a food directory, which is going

23
00:01:43,740 --> 00:01:46,170
to have these many number of files in there.

24
00:01:46,680 --> 00:01:52,350
And the collection of all these files together in a particular directory is nothing but your Django

25
00:01:52,350 --> 00:01:52,680
app.

26
00:01:52,830 --> 00:01:58,160
And amongst these files right here, the most important file which we are going to talk about first

27
00:01:58,200 --> 00:01:59,730
is the views that people file.

28
00:02:00,210 --> 00:02:03,940
Now, let's understand what exactly is the view stored by file.

29
00:02:04,110 --> 00:02:10,500
So the view stored by a file or view is nothing, but it's actually something which processes the user

30
00:02:10,500 --> 00:02:11,940
requests in Django.

31
00:02:12,210 --> 00:02:18,150
So, for example, let's say this is a user and let's say this user sends a request to the browser.

32
00:02:18,150 --> 00:02:24,960
Let's say it sends the request of Facebook dot com slash X, Y, Z of X, Y, Z is the name of a particular

33
00:02:24,960 --> 00:02:25,890
Facebook user.

34
00:02:26,550 --> 00:02:32,520
So now jingoes job is to actually go ahead and process this particular user request.

35
00:02:32,880 --> 00:02:38,430
And the processing of this particular user request is done by something which is called as the view,

36
00:02:38,430 --> 00:02:40,090
which we have on the left hand side.

37
00:02:40,620 --> 00:02:46,380
So the job of this view is to take the request, analyze this particular request and then send back

38
00:02:46,380 --> 00:02:47,700
an appropriate response.

39
00:02:48,120 --> 00:02:53,620
Now, in this case, this user is requesting for Facebook page of X is the user.

40
00:02:53,640 --> 00:03:00,300
So the job of this view is to return a response back to the user by giving out X, Y, Z, Facebook

41
00:03:00,300 --> 00:03:00,690
page.

42
00:03:01,200 --> 00:03:03,580
So this is how the view should actually work.

43
00:03:04,050 --> 00:03:09,510
So a view in Zynga is nothing, but it's something which can actually go ahead and process the user's

44
00:03:09,510 --> 00:03:12,210
request and send back an appropriate response.

45
00:03:12,240 --> 00:03:17,970
So now let's learn how exactly can you write a view so we already know that a view is nothing but of

46
00:03:17,970 --> 00:03:19,150
user profile.

47
00:03:19,410 --> 00:03:24,060
And now let's learn how exactly we can write in some code inside that particular view.

48
00:03:24,570 --> 00:03:30,580
So a view is generally written as a python function, and this python function can be written inside

49
00:03:30,580 --> 00:03:32,020
the views that by file.

50
00:03:33,090 --> 00:03:38,070
So whenever you want to write a view, what you simply do is that you create a simple python function.

51
00:03:38,340 --> 00:03:44,220
And as usual, just as you give a name to a particular function, you can go ahead, create a function,

52
00:03:44,220 --> 00:03:45,330
name that something.

53
00:03:45,330 --> 00:03:48,630
And that name is going to be nothing but the name of your view.

54
00:03:48,660 --> 00:03:51,220
So this is what a view usually looks like.

55
00:03:51,240 --> 00:03:56,010
So the DEA is actually present in capitals, but that should be actually present in a small letter.

56
00:03:56,430 --> 00:03:59,040
So here we have a normal function.

57
00:03:59,310 --> 00:04:02,670
And as this is a view, it's going to accept a request over here.

58
00:04:02,850 --> 00:04:07,050
So in this case, this request parameter is nothing but the user request.

59
00:04:07,320 --> 00:04:12,450
So now the job of this function is to take the request and depending upon the request, it needs to

60
00:04:12,450 --> 00:04:14,130
return an appropriate response.

61
00:04:14,490 --> 00:04:18,300
So this is the basic structure of what exactly a view looks like.

62
00:04:18,329 --> 00:04:24,300
Now, once we know what our apps and what our views in the next lecture, let's go ahead and actually

63
00:04:24,300 --> 00:04:31,230
create a Django app and actually write a view inside that particular app, which processes a user request

64
00:04:31,230 --> 00:04:33,370
and sends back an appropriate response.

65
00:04:33,870 --> 00:04:38,160
So thank you very much for watching and I'll see you guys in the next lecture.

66
00:04:38,610 --> 00:04:39,150
Thank you.

