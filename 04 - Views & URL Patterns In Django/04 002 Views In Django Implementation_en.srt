1
00:00:00,700 --> 00:00:06,970
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to create a Django

2
00:00:06,970 --> 00:00:07,310
app.

3
00:00:08,140 --> 00:00:11,050
Now you might think what exactly is an app?

4
00:00:11,650 --> 00:00:16,170
Now, we have already created this project, which is the myside project.

5
00:00:16,480 --> 00:00:22,040
And in Django, the project is comprised of multiple apps combined together.

6
00:00:22,570 --> 00:00:29,590
So let's say, for example, if you're building a huge website, so that website might contain multiple

7
00:00:29,590 --> 00:00:30,290
sections.

8
00:00:30,310 --> 00:00:36,460
So, for example, your website could have a shopping section, a command section, a community section

9
00:00:36,460 --> 00:00:38,260
and so on and so forth.

10
00:00:38,530 --> 00:00:43,360
And these each different sections are called as individual apps.

11
00:00:43,930 --> 00:00:49,720
So in order to define these sections individually, you create individual apps for them.

12
00:00:50,110 --> 00:00:53,770
So right now now my side project, we don't have any app yet.

13
00:00:54,100 --> 00:00:57,560
So now we need to make sure that we go ahead and create one.

14
00:00:58,120 --> 00:01:03,640
So in order to create an app, you can go ahead, open up terminal and you make sure that you are into

15
00:01:03,640 --> 00:01:04,930
the myside project.

16
00:01:05,740 --> 00:01:07,530
And I want to enter this project.

17
00:01:07,960 --> 00:01:14,350
There is a specific command which you need to use to create an app, and that command is called as the

18
00:01:14,710 --> 00:01:15,370
up command.

19
00:01:15,790 --> 00:01:19,060
So in order to type that thing in, you need to type in Python.

20
00:01:19,190 --> 00:01:27,640
And as I'm using Python free and open, Python three managed Torpy and then you need to type in start

21
00:01:27,670 --> 00:01:34,240
up and ahead of the start up, you need to type in the name of the app which you want to build.

22
00:01:34,390 --> 00:01:39,400
So let's assume we are building a food app which is going to display information of different kinds

23
00:01:39,400 --> 00:01:39,970
of foods.

24
00:01:40,300 --> 00:01:44,560
I just need my app as food over here and now.

25
00:01:44,560 --> 00:01:49,720
Once I go ahead and hit enter, it is automatically going to create the app for me.

26
00:01:50,260 --> 00:01:56,500
So if you want to make sure if this app is created, you can go back to your idea or text editor.

27
00:01:56,980 --> 00:02:02,020
And as you can see now, we have a new folder over here, which is called as Food.

28
00:02:02,050 --> 00:02:08,139
Now, if you open up this directly or food, as you can see, it's going to have a bunch of other files

29
00:02:08,139 --> 00:02:08,560
as well.

30
00:02:09,280 --> 00:02:11,730
So right now, we have the inner supply.

31
00:02:11,770 --> 00:02:18,340
And the purpose of an adoption is to basically tell the python that this particular directly, which

32
00:02:18,340 --> 00:02:20,390
is food, is actually a python package.

33
00:02:20,410 --> 00:02:22,920
Now, there are other files over here as well.

34
00:02:23,200 --> 00:02:29,410
But for now, we are going to just talk about the abuse stockpile file, which is this file right here.

35
00:02:29,530 --> 00:02:33,820
Now, if you have a look over here in this file, it says create your views here.

36
00:02:34,600 --> 00:02:37,380
So now you can go ahead and create your views over here.

37
00:02:37,600 --> 00:02:41,730
But first of all, you need to understand what exactly do we mean by views?

38
00:02:41,740 --> 00:02:48,370
So as the name of the file suggests, a view is nothing but what content your user is going to view.

39
00:02:48,670 --> 00:02:53,510
So let's assume you want to display some kind of a message like Hollowell to the user.

40
00:02:54,010 --> 00:02:57,610
You need to go ahead and write a view for that particular thing.

41
00:02:57,760 --> 00:02:59,380
So let's take a simple example.

42
00:02:59,830 --> 00:03:01,630
Let's say you want to display HelloWallet.

43
00:03:01,630 --> 00:03:06,760
So for displaying that to the user, you need to write a specific view in here so you cannot directly

44
00:03:06,760 --> 00:03:07,970
just go ahead and type in.

45
00:03:08,320 --> 00:03:11,930
Hello, world over here, because that's not going to work.

46
00:03:12,460 --> 00:03:19,270
Instead, there is actually a proper method to write a view sort of view can be written as a python

47
00:03:19,270 --> 00:03:19,800
function.

48
00:03:20,350 --> 00:03:23,380
So as you all know, Python function looks something like this.

49
00:03:23,620 --> 00:03:29,800
You have def, then you have the function name over here and then you go ahead and just define a bunch

50
00:03:29,800 --> 00:03:30,550
of code in there.

51
00:03:30,730 --> 00:03:33,490
So we are going to do the same thing to write of you here.

52
00:03:34,060 --> 00:03:36,310
So let's go ahead and type in def.

53
00:03:37,930 --> 00:03:42,760
To state that this is actually a function and now as we need to name this function as something, I'll

54
00:03:42,760 --> 00:03:49,570
type an index here and whatever name you give to this particular function actually becomes the name

55
00:03:49,570 --> 00:03:50,300
of your view.

56
00:03:50,980 --> 00:03:56,540
So even though this is technically just a python function, we call it as a view in Django.

57
00:03:57,010 --> 00:04:00,690
Now, just like a normal function, it's also going to accept a parameter.

58
00:04:01,120 --> 00:04:07,320
And in this case, the parameter which this view is going to accept is going to be the request.

59
00:04:08,830 --> 00:04:16,180
And now what this view does is that it takes up this particular request and it sends back a response.

60
00:04:16,660 --> 00:04:19,060
So I'll type in return.

61
00:04:19,630 --> 00:04:25,000
And the response which we need to send here is going to be the HTP response.

62
00:04:25,000 --> 00:04:26,590
So I'll type in HTP.

63
00:04:29,180 --> 00:04:35,900
Response and now to this particular response, we can pass some text like hello.

64
00:04:37,010 --> 00:04:43,100
What and I don't know, for some reason, we do have some error here, so now once we go ahead and save

65
00:04:43,100 --> 00:04:45,020
this code, everything is fine.

66
00:04:45,500 --> 00:04:52,070
But there is one main issue with this particular view, and that is we have not yet defined the response.

67
00:04:52,790 --> 00:04:58,220
So now we need to empower the HTP response from Shango Dot HDP.

68
00:04:58,280 --> 00:05:05,250
So I'll type in from Shango dot HDP import HTP response.

69
00:05:05,870 --> 00:05:10,390
Now, you might be a little bit confused about what exactly is happening over here.

70
00:05:11,060 --> 00:05:18,290
So if you recall, I earlier mentioned that Django is actually used to program so was so that they can

71
00:05:18,290 --> 00:05:22,430
handle the incoming request and send back an appropriate response.

72
00:05:22,940 --> 00:05:25,260
So this is exactly what's happening over here.

73
00:05:25,790 --> 00:05:31,460
We are defining a view which is nothing but a function, and that function is going to accept the incoming

74
00:05:31,460 --> 00:05:32,330
user request.

75
00:05:33,080 --> 00:05:38,960
And it's going to send back a particular response, which is going to be an HTTP response.

76
00:05:39,470 --> 00:05:43,580
And whatever we pass in here is going to return back as a response.

77
00:05:44,950 --> 00:05:51,280
Now, this thing right here is called as a view, and you can have multiple views over here in your

78
00:05:51,280 --> 00:05:58,030
views stored by file, so all the views of your food app are going to be contained in this particular

79
00:05:58,030 --> 00:05:58,420
file.

80
00:05:59,020 --> 00:06:06,250
So now we have successfully defined this particular view, but this view now needs to be attached with

81
00:06:06,250 --> 00:06:06,780
the you are.

82
00:06:07,000 --> 00:06:12,520
So, for example, we have written this for you, but how exactly can we access this particular view

83
00:06:12,520 --> 00:06:13,450
on our website?

84
00:06:13,600 --> 00:06:15,850
So you need to open up your website.

85
00:06:16,090 --> 00:06:20,350
You need to type in some you are URL and depending upon that particular, you are.

86
00:06:20,890 --> 00:06:21,250
Hello.

87
00:06:21,250 --> 00:06:24,080
What should be displayed over here on your Web page?

88
00:06:24,400 --> 00:06:26,220
So how exactly can we do that?

89
00:06:26,740 --> 00:06:32,530
So now we need to make sure that we link this particular view to a particular.

90
00:06:32,530 --> 00:06:32,860
You are.

91
00:06:32,860 --> 00:06:34,370
It makes sense.

92
00:06:34,420 --> 00:06:36,790
So now let's go ahead and learn how to do that.

93
00:06:36,820 --> 00:06:42,920
So now how exactly do we tell the server that we need to attach to you are with this particular view.

94
00:06:43,030 --> 00:06:49,620
So for that, we simply go ahead and create a new file in the food directly and call it as you are Alstott

95
00:06:49,690 --> 00:06:50,390
profile.

96
00:06:51,210 --> 00:06:55,180
So as you can see this, you are also profile currently empty.

97
00:06:55,690 --> 00:06:58,750
But if you'll notice that we also have a U.

98
00:06:58,750 --> 00:07:00,950
Alstott profile over here as well.

99
00:07:01,930 --> 00:07:06,830
So if you go over here, as you can see, it actually has a bunch of code in here.

100
00:07:06,970 --> 00:07:09,250
So these are the comments and you can ignore that.

101
00:07:09,610 --> 00:07:12,240
But this is the main code which we will be talking about.

102
00:07:13,150 --> 00:07:17,610
So if you have a look over here, we have something which is called as a you are a pattern.

103
00:07:18,400 --> 00:07:25,570
So what does your URL pattern does is that it matches up this particular path with the incoming request.

104
00:07:26,140 --> 00:07:30,340
So now here we have a path which is called as admon.

105
00:07:31,550 --> 00:07:37,880
So what this means is that whenever you open up a browser and whenever you type Admon, something is

106
00:07:37,880 --> 00:07:39,170
going to eventually happen.

107
00:07:39,850 --> 00:07:46,460
So in order to test that, let's go ahead and let's set up a server by typing in Python, manage the

108
00:07:46,460 --> 00:07:50,810
API run server, and now let's go to a server here.

109
00:07:51,020 --> 00:07:56,060
So right now we are on to a server and now if I go ahead and type in slash admin.

110
00:07:58,010 --> 00:08:03,930
And enter, as you can see, we are redirected to this particular page, which is the Django Admins

111
00:08:04,040 --> 00:08:04,800
login page.

112
00:08:05,240 --> 00:08:08,390
So how exactly did this particular thing happen?

113
00:08:09,020 --> 00:08:14,560
This happened because the you are on a path for admin as actually configured.

114
00:08:15,140 --> 00:08:21,830
So now what we need to do is that we also need to configure such a path for this particular view as

115
00:08:21,830 --> 00:08:22,090
well.

116
00:08:23,160 --> 00:08:25,390
So how exactly can we do that?

117
00:08:25,440 --> 00:08:30,020
So what we will do here is that will go ahead and have a bunch of code in here.

118
00:08:30,780 --> 00:08:33,220
That is, we will define you all patterns in here.

119
00:08:33,240 --> 00:08:39,120
So in the upcoming lecture, we will go ahead and write in some real patterns in this particular file

120
00:08:39,120 --> 00:08:41,260
and connected with this particular view.

121
00:08:41,640 --> 00:08:43,650
So thank you very much for watching.

122
00:08:43,650 --> 00:08:45,780
And I'll see you guys next time.

123
00:08:46,260 --> 00:08:46,860
Thank you.

