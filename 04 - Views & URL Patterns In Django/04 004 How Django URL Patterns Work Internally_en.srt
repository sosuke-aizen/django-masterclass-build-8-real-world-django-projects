1
00:00:00,090 --> 00:00:05,460
Now that we know how you can actually go ahead, create a view and associate that view with the you

2
00:00:05,460 --> 00:00:08,560
are pattern, let's actually learn how jangle you are.

3
00:00:08,560 --> 00:00:09,950
A pattern actually works.

4
00:00:10,680 --> 00:00:15,540
So let's understand the internal workings, because when you go ahead and understand the internal workings

5
00:00:15,540 --> 00:00:21,240
of how Jianguo exactly works, when it receives a user request, you will be able to learn Zango in

6
00:00:21,240 --> 00:00:26,430
a much more better way and you will have an in-depth understanding of the subject instead of just scratching

7
00:00:26,430 --> 00:00:27,030
the surface.

8
00:00:27,630 --> 00:00:30,550
So let's go ahead and learn how exactly Django works.

9
00:00:31,380 --> 00:00:37,800
So what Django does is that Django actually looks for the incoming request and when it receives the

10
00:00:37,800 --> 00:00:42,480
incoming request, first of all, it looks for something which is called as the root.

11
00:00:42,480 --> 00:00:47,550
You are Alarcon's and this variable is actually located in the settings dogpile file.

12
00:00:48,030 --> 00:00:54,180
So right now, if you open up your website or if you open up your Web project, you will have a file

13
00:00:54,180 --> 00:00:56,200
which is called the Settings Dot Pi file.

14
00:00:56,580 --> 00:01:02,700
And in that particular file, if you search for the root and the school, you are a variable.

15
00:01:02,730 --> 00:01:05,730
You will have a variable over there and in there.

16
00:01:05,730 --> 00:01:12,000
As you can see, that particular variable currently points to the mine site that you are a file.

17
00:01:12,780 --> 00:01:19,170
So that means Django Nortel's that whenever there is some incoming user request, that is whenever a

18
00:01:19,170 --> 00:01:25,560
user is entering some sort of a new URL, what Django needs to do is Django force needs to go and look

19
00:01:25,560 --> 00:01:26,280
for the root.

20
00:01:26,280 --> 00:01:27,600
You are all configuration.

21
00:01:28,050 --> 00:01:34,200
So now as the root, you are configuration points to my side, not you are, as Django now needs to

22
00:01:34,200 --> 00:01:38,040
go ahead and look for the URL patterns inside this particular file.

23
00:01:38,910 --> 00:01:44,480
So after that, Django actually goes to the you are sought by file and when it goes to the you are also

24
00:01:44,540 --> 00:01:45,300
by file.

25
00:01:45,540 --> 00:01:51,120
It's going to compare the you are Pashtuns which are present in the you are Stoppie by file, which

26
00:01:51,120 --> 00:01:53,410
is nothing but the myside dodgeballs file.

27
00:01:53,760 --> 00:01:58,980
So it's going to compare the request which was sent by the user to the you are all Pashtuns which are

28
00:01:58,980 --> 00:01:59,760
present in this.

29
00:01:59,760 --> 00:02:01,250
You are all stock by file.

30
00:02:01,530 --> 00:02:08,520
And when it finds that there is a match of a particular pattern with the incoming request, it's simply

31
00:02:08,520 --> 00:02:14,220
going to go ahead and call that particular view, which is associated with that particular you are a

32
00:02:14,220 --> 00:02:14,700
pattern.

33
00:02:15,390 --> 00:02:19,660
Now, if this sounds confusing to you, let's go ahead and take an example.

34
00:02:20,040 --> 00:02:25,350
So as an example, we will take the example which we have completed in the previous lecture wherein

35
00:02:25,350 --> 00:02:29,120
we have one view associated with the particular you are.

36
00:02:29,550 --> 00:02:35,060
So in the previous lecture, what we have done is that we had entered this particular order that is

37
00:02:35,060 --> 00:02:37,530
slash food, slash hello into the you are the.

38
00:02:38,130 --> 00:02:40,770
And now let's understand how Django works from here.

39
00:02:41,460 --> 00:02:46,170
So now when we enter that particular you are first of all, Django looks for the root.

40
00:02:46,170 --> 00:02:51,630
You are a variable and it's going to see that variable from the setting start by file.

41
00:02:52,200 --> 00:02:57,340
And as you can see, this actually points to the side, which is my side, not you are us.

42
00:02:58,080 --> 00:03:03,900
So now Django knows it has to look for the you are Pashtuns in the myside that you are in this file.

43
00:03:04,800 --> 00:03:10,200
So it's going to open up that file and it's going to find that the you are Pashtuns, which are present

44
00:03:10,200 --> 00:03:10,740
over here.

45
00:03:11,190 --> 00:03:17,400
And now it's going to match this particular request, which is slash food slash hello with these patterns

46
00:03:17,400 --> 00:03:17,920
right here.

47
00:03:18,540 --> 00:03:20,910
So first of all, it's going to check for this pattern.

48
00:03:20,910 --> 00:03:22,980
So Admon does not match up with food.

49
00:03:23,280 --> 00:03:24,810
So it's going to skip that.

50
00:03:24,810 --> 00:03:28,890
Then it's going to move on to the next part, which is this food right over here.

51
00:03:29,340 --> 00:03:34,500
And now it's going to find out that, OK, this pot actually matches up with this particular part right

52
00:03:34,500 --> 00:03:34,760
here.

53
00:03:35,310 --> 00:03:38,050
And henceforth it's going to see what's next.

54
00:03:38,520 --> 00:03:44,580
So the next thing which it notices is that we have included another you are Taupo file, which is food

55
00:03:44,580 --> 00:03:45,680
Daudt, you are it.

56
00:03:46,230 --> 00:03:51,210
So then it's going to jump on to that particular file, which is food, dodgeballs.

57
00:03:51,630 --> 00:03:57,180
And upon going to this particular file, it's going to again look for that pattern and compare it with

58
00:03:57,180 --> 00:03:59,160
the users and request.

59
00:03:59,740 --> 00:04:05,160
So it's going to find that the request actually matches up with the pattern over here for this path.

60
00:04:05,640 --> 00:04:09,870
Henceforth, it's now going to go into The View, which is Disp right here.

61
00:04:10,230 --> 00:04:17,100
And hence now Django understands that as Yusa has requested this particular you are all it now needs

62
00:04:17,100 --> 00:04:20,100
to execute this view, which is nothing but the index view.

63
00:04:20,670 --> 00:04:26,220
So then it's going to load up the index view from the user profile, which is this view right here,

64
00:04:26,220 --> 00:04:28,500
which is nothing but a plain, simple function.

65
00:04:28,500 --> 00:04:31,740
And henceforth it's going to execute this particular function.

66
00:04:32,250 --> 00:04:38,370
So upon executing this function, this function simply returns and HDB response, which is Hello world,

67
00:04:38,370 --> 00:04:42,720
and therefore we get Hollowell on our browser screen as an output.

68
00:04:43,470 --> 00:04:45,540
So this is how Django works internally.

69
00:04:45,870 --> 00:04:50,370
So whenever Django receives a user request, Django, first of all, looks for the root.

70
00:04:50,370 --> 00:04:57,600
You are a config variable and then it finds out that that variable is going to point to a certain file

71
00:04:57,600 --> 00:04:59,700
which contains the You are Paton's then.

72
00:04:59,760 --> 00:05:05,130
It's simply going to match up the usual pattern with the user request, and when it finds out that particular

73
00:05:05,130 --> 00:05:10,560
pattern present over here, then it's going to look for the view, which it needs to load up, and then

74
00:05:10,560 --> 00:05:15,950
it's going to simply go ahead and execute that particular view, which is nothing but a python function.

75
00:05:15,990 --> 00:05:18,020
So that's it for this lecture.

76
00:05:18,030 --> 00:05:24,630
And hopefully you guys be able to understand how Shango works internally when you go ahead and type

77
00:05:24,630 --> 00:05:28,090
in some requests and say that you are all part of your Web browser.

78
00:05:28,770 --> 00:05:30,280
So that's it for this lecture.

79
00:05:30,330 --> 00:05:35,940
If you have any doubts or queries, feel free to post in the Q&A section and I will be there to help

80
00:05:35,940 --> 00:05:36,470
you out.

81
00:05:36,780 --> 00:05:38,670
So thank you very much for watching.

82
00:05:38,670 --> 00:05:40,620
And I'll see you guys next time.

83
00:05:40,890 --> 00:05:41,460
Thank you.

