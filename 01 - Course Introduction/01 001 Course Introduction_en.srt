1
00:00:00,060 --> 00:00:03,400
So congratulations on finally enrolling in the course.

2
00:00:03,450 --> 00:00:06,830
So in this lecture, we will go through the course introduction.

3
00:00:07,110 --> 00:00:10,380
So yes, exactly what we will learn in this course.

4
00:00:10,410 --> 00:00:14,920
So in the very first section, we will have a brief introduction to Jianguo.

5
00:00:15,030 --> 00:00:21,810
So even before we start making Django apps, we will first go ahead and learn and understand what exactly

6
00:00:21,810 --> 00:00:27,630
Django is, why it is used and how it can be used to build the back end of any webapp.

7
00:00:28,650 --> 00:00:32,500
Then we will learn about how to install Python and Django.

8
00:00:32,670 --> 00:00:38,820
So in order to use Django, you need to have Python installed on your computer and you also need to

9
00:00:38,820 --> 00:00:40,770
go ahead and install Django as well.

10
00:00:41,160 --> 00:00:47,640
So in this section, we will go through the installation steps for Python and Django, for both Mac

11
00:00:47,640 --> 00:00:48,900
and Windows users.

12
00:00:49,050 --> 00:00:56,030
Then we will start off by building a food menu app, which is going to be the very first Django application,

13
00:00:56,040 --> 00:01:02,160
which we will be building now, will start making this app right from scratch and we will learn the

14
00:01:02,160 --> 00:01:08,770
basics of Django in this app, like of use models, template and authentication.

15
00:01:09,390 --> 00:01:16,050
So this app is going to go ahead and teach you Django right from basics, cover all the basics like

16
00:01:16,050 --> 00:01:17,880
views, models and templates.

17
00:01:18,030 --> 00:01:24,330
And gradually the level of this application is going to go up and we will learn about advanced concepts

18
00:01:24,330 --> 00:01:27,300
like authentication using this app.

19
00:01:28,210 --> 00:01:34,600
Moving along, we will go ahead and learn about a rest API in which we will cover the Januaries framework,

20
00:01:34,600 --> 00:01:36,950
which will allow us to build a rest API.

21
00:01:37,240 --> 00:01:42,190
So this section is all about creating an API, using an Shango application.

22
00:01:42,850 --> 00:01:47,730
So we will make a simple rest API in Shango using the Changaris framework.

23
00:01:47,740 --> 00:01:51,310
The next section, Kawas pagination search and filtering.

24
00:01:51,520 --> 00:01:58,620
So we will build a simple app which covers these three major features of Shango that is pagination.

25
00:01:58,630 --> 00:02:03,670
That is how to split up content across multiple web pages such as nothing.

26
00:02:03,670 --> 00:02:07,060
But it allows you to search content throughout your triangle web app.

27
00:02:07,060 --> 00:02:09,370
And filtering is a feature similar to search.

28
00:02:10,860 --> 00:02:16,500
In the next section, we start building our e-commerce site, and this is not going to be just another

29
00:02:16,500 --> 00:02:23,280
simple e-commerce website, but we are going to build a very complex e-commerce website which has features

30
00:02:23,310 --> 00:02:30,210
like a shopping cart in which you can add items which works exactly like the card which you use on the

31
00:02:30,210 --> 00:02:31,840
popular e-commerce websites.

32
00:02:32,220 --> 00:02:37,620
It's also going to have a feature like Checkout, which will go ahead and display all the items added

33
00:02:37,620 --> 00:02:38,280
to the card.

34
00:02:38,280 --> 00:02:44,640
And their total will also have a search feature built into our e-commerce site, which will allow you

35
00:02:44,640 --> 00:02:46,580
to search for different kinds of products.

36
00:02:46,590 --> 00:02:51,710
And the final feature, which we will build in this particular section is order placement, wherein

37
00:02:51,720 --> 00:02:54,200
the user can actually go ahead and place an order.

38
00:02:54,390 --> 00:03:00,120
Then in the next section, we will learn about admin panel customization and which we will learn how

39
00:03:00,120 --> 00:03:06,300
exactly you can customize the admin panel of your jungle website to provide more features to the site

40
00:03:06,300 --> 00:03:06,760
admin.

41
00:03:08,180 --> 00:03:17,430
Then we will go ahead and make a very advanced Web based CV or resume generator, which actually accepts

42
00:03:17,550 --> 00:03:18,240
data.

43
00:03:18,240 --> 00:03:22,740
That is the personal data from the end user and out of that data.

44
00:03:22,920 --> 00:03:29,970
This application is going to dynamically generate a resume or a CV in a downloadable PDA format.

45
00:03:29,970 --> 00:03:34,680
And then finally we conclude the course by building a Web based link.

46
00:03:34,680 --> 00:03:41,000
Skripal, which is an advanced web application which scrapes all the links on a Web page.

47
00:03:41,490 --> 00:03:46,020
So these are the levels of applications which we will be building in this course.

48
00:03:46,320 --> 00:03:52,530
However, I would like to remind you that even though we covered these advanced concepts, we start

49
00:03:52,530 --> 00:03:58,530
right from basics and we go through the course in a very slow pace so that you understand each and every

50
00:03:58,530 --> 00:03:59,860
detail throughout the course.

51
00:03:59,880 --> 00:04:05,430
So even though the curriculum seems a little bit complex, you don't need to worry about it as we will

52
00:04:05,430 --> 00:04:09,300
learn tango right from basics and Motörhead at a very slow pace.

53
00:04:10,350 --> 00:04:12,450
So that's it for this lecture.

54
00:04:12,480 --> 00:04:13,850
From the next lecture on what?

55
00:04:13,860 --> 00:04:15,870
Let's go ahead and start learning Chango.

