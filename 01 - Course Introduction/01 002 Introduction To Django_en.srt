1
00:00:00,150 --> 00:00:06,320
Hello and welcome to this lecture, and in this lecture, we will have a brief overview about Django.

2
00:00:06,900 --> 00:00:09,150
So what exactly is Django?

3
00:00:09,660 --> 00:00:14,900
So to understand, Django, let's take an example of a social networking website.

4
00:00:15,240 --> 00:00:21,960
So let's say if you want to access a social networking website like Facebook, so what you essentially

5
00:00:21,960 --> 00:00:28,920
do is that you open up your browser and you send a request to the browser, for example, you open up

6
00:00:28,920 --> 00:00:30,750
the browser, you type in some.

7
00:00:30,750 --> 00:00:34,430
You are like Facebook dot com, slash the username.

8
00:00:34,920 --> 00:00:41,340
So let's say, for example, you want to extract the information of a user from a social media website.

9
00:00:41,580 --> 00:00:47,610
If you want to access information of a particular user, you type in Facebook, dot com, slash the

10
00:00:47,610 --> 00:00:49,400
name of that particular user.

11
00:00:50,040 --> 00:00:52,800
So you type this thing in into your browser.

12
00:00:53,040 --> 00:00:57,030
And what your browser does is that the browser takes this.

13
00:00:57,060 --> 00:01:00,330
You are a request and it sends it to the server.

14
00:01:00,720 --> 00:01:03,900
So this right here is let's assume Facebook's server.

15
00:01:04,379 --> 00:01:08,670
So now the job of the server is to take your request.

16
00:01:08,980 --> 00:01:10,560
It's going to read your request.

17
00:01:10,890 --> 00:01:17,430
And depending upon what data you have requested, it's going to look at that data and then it's going

18
00:01:17,430 --> 00:01:22,340
to find a data of user one and it's going to send back a response.

19
00:01:22,800 --> 00:01:28,800
So this response is going to be nothing but the profile of that particular user.

20
00:01:29,130 --> 00:01:36,030
So, for example, let's say if you type in Facebook dot com, slash your username, you will get your

21
00:01:36,060 --> 00:01:37,120
user profile.

22
00:01:37,830 --> 00:01:43,990
So this is the entire process which happens when you request for a particular webpage from a server.

23
00:01:44,010 --> 00:01:46,410
So this entire thing is nothing.

24
00:01:46,410 --> 00:01:51,180
But what happens when you access any kind of website on the Internet?

25
00:01:51,210 --> 00:01:57,390
So what essentially happens is that you type in a you are in your browser and now that browser is actually

26
00:01:57,390 --> 00:01:58,230
going to send that.

27
00:01:58,230 --> 00:02:01,630
You are added to the specific server and request back some data.

28
00:02:02,310 --> 00:02:09,210
Now, the servers are programmed in such a way that they can handle your request and return an appropriate

29
00:02:09,210 --> 00:02:09,840
response.

30
00:02:10,199 --> 00:02:12,150
So this is exactly what happened here.

31
00:02:12,330 --> 00:02:19,200
But now the main question is how exactly does this particular server know which kind of page or what

32
00:02:19,200 --> 00:02:21,900
kind of data it needs to return back to the browser?

33
00:02:21,900 --> 00:02:28,080
So the server actually knows this stuff because the server is actually programmed to handle these client

34
00:02:28,080 --> 00:02:30,810
requests and send back appropriate response.

35
00:02:31,530 --> 00:02:34,700
Now, how exactly can we program these servers?

36
00:02:35,010 --> 00:02:37,480
So that's where Django comes into picture.

37
00:02:37,710 --> 00:02:39,330
So what exactly is Django?

38
00:02:39,870 --> 00:02:43,560
Django is a free and open source web application framework.

39
00:02:44,010 --> 00:02:51,270
So whatever you want to build a Web application, we use Django and Django is nothing but a framework

40
00:02:51,270 --> 00:02:57,520
and a framework is nothing but a collection of multiple components which we can use to build a Web application.

41
00:02:58,800 --> 00:03:04,860
So as I also mentioned, a framework is nothing but a collection of components which allows us to build

42
00:03:04,920 --> 00:03:05,940
sites faster.

43
00:03:06,390 --> 00:03:12,700
So, as I earlier mentioned, we need to program servers and we Django, we can do exactly the same.

44
00:03:12,720 --> 00:03:19,230
So Django is called as a backend framework because Django is used for programming the servers and not

45
00:03:19,230 --> 00:03:20,110
at the front end.

46
00:03:20,430 --> 00:03:26,700
So this is the very specific reason why we call Django as a backend framework, because it is used to

47
00:03:26,700 --> 00:03:29,880
program the backend, that is the servers.

48
00:03:31,500 --> 00:03:36,930
Now, Django also has an automatic admin interface which supports crat.

49
00:03:37,980 --> 00:03:43,710
Now, if you build any kind of a website, so let's say, for example, you're building an e-commerce

50
00:03:43,710 --> 00:03:44,300
website.

51
00:03:44,310 --> 00:03:49,290
So for that very specific site, you also need to build an admin panel as well.

52
00:03:49,560 --> 00:03:55,440
So, for example, there is going to be an admin, which is nothing but the main person who is going

53
00:03:55,440 --> 00:04:02,850
to handle that entire site and that admin should be allowed to create, read, update and delete all

54
00:04:02,850 --> 00:04:04,050
the data from its site.

55
00:04:04,560 --> 00:04:09,510
So for that very purpose, you need to provide an admin panel for a particular website.

56
00:04:09,660 --> 00:04:16,500
But with Django, Django actually has a feature which provides an automatic admin interface so that

57
00:04:16,500 --> 00:04:18,399
you don't have to design it from scratch.

58
00:04:18,779 --> 00:04:22,200
So this is one of the features of Django, which we are going to look into.

59
00:04:22,350 --> 00:04:26,460
So that was a brief introduction about what exactly Django is.

60
00:04:26,790 --> 00:04:32,340
So the only thing which you need to keep in mind for now is that Django is used for programming the

61
00:04:32,340 --> 00:04:37,650
servers so that they can return an appropriate response back to the client.

62
00:04:38,040 --> 00:04:43,260
And Django is nothing but a framework, which is a backend framework and a framework is nothing but

63
00:04:43,260 --> 00:04:44,640
a collection of components.

64
00:04:45,210 --> 00:04:52,620
So a framework is nothing, but it's essentially a bunch of code which we can reuse to build sites faster.

65
00:04:53,310 --> 00:04:58,590
So without wasting any time in the upcoming lecture, we will start building a Web application with

66
00:04:58,590 --> 00:04:59,130
Django.

