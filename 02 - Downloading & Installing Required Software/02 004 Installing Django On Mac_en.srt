1
00:00:00,570 --> 00:00:06,900
So in this lecture, we will go ahead and learn how to install Chango on Makk, so the very first thing

2
00:00:06,900 --> 00:00:09,460
which you need to do is that you need to open up a terminal.

3
00:00:09,660 --> 00:00:17,510
So if you're on Mac, simply press come on space and search for terminal and open up the terminal.

4
00:00:17,850 --> 00:00:20,830
And once you open up the terminal, this is what you get.

5
00:00:21,390 --> 00:00:28,230
So in order to install Shango on your Mac, you first need to go ahead and install something which is

6
00:00:28,230 --> 00:00:29,370
called as BEP.

7
00:00:29,850 --> 00:00:34,300
So Pip is actually a tool which will be using to download and install Shango.

8
00:00:34,320 --> 00:00:39,420
So first of all, in order to install PIP on your machine, you need to type in pseudo.

9
00:00:41,600 --> 00:00:50,150
Easy and a school install and then type in BEP, so simply go ahead and hit enter and as you can see,

10
00:00:50,150 --> 00:00:54,790
it did not download for me as I already have pip installed on my machine.

11
00:00:54,800 --> 00:01:00,560
But if you are installing PIP for the very first time, it will actually go ahead and install PIP for

12
00:01:00,560 --> 00:01:00,830
you.

13
00:01:01,310 --> 00:01:07,100
And chances are when you type in pseudo easy install PIP, it's actually going to go ahead and ask you

14
00:01:07,100 --> 00:01:08,090
for your password.

15
00:01:08,090 --> 00:01:14,400
So simply go ahead, enter the password for your super user and it will actually install PIP for you.

16
00:01:14,870 --> 00:01:21,290
So now once you have Pip, you now need to go ahead and make sure to download and install Zango.

17
00:01:21,590 --> 00:01:27,280
And whenever you want to download and install Shango as we actually want to use Python three, which

18
00:01:27,350 --> 00:01:37,190
angle you simply need to go ahead and type in PIP three, install Django and then you can simply go

19
00:01:37,190 --> 00:01:43,910
ahead and also mentioned the version of Django which you want to install so you can type in Django equal

20
00:01:43,920 --> 00:01:50,090
equals and you know, you can specify dewpoint two so you can either download two point or two point

21
00:01:50,090 --> 00:01:51,400
one or two point two.

22
00:01:51,410 --> 00:01:54,130
So I'll go ahead and download two point two over here.

23
00:01:54,710 --> 00:01:56,740
So simply go ahead and hit enter.

24
00:01:57,380 --> 00:02:02,990
And as you can see, it's going to go ahead and download and install Django two point two for you.

25
00:02:04,390 --> 00:02:11,200
And the crucial step you need to remember here is that use poop three and not just in order to install

26
00:02:11,200 --> 00:02:11,700
Zango.

27
00:02:12,310 --> 00:02:16,940
So as you can see now, we have successfully installed Shango right over here.

28
00:02:17,140 --> 00:02:23,260
So if you have any issues downloading Python or jangle, do let me know in the Q&A section and I will

29
00:02:23,260 --> 00:02:24,550
be there to help you out.

