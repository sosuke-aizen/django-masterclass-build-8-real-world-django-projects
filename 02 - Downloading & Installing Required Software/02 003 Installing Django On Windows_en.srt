1
00:00:00,790 --> 00:00:05,770
Now, let's go ahead and learn how to install Shango on your Windows computer.

2
00:00:06,220 --> 00:00:09,370
So, first of all, if you open up the terminal.

3
00:00:10,500 --> 00:00:16,379
Or the command prompt, and then here, let me just go ahead and increase the font a little bit so that

4
00:00:16,379 --> 00:00:19,080
you'll be able to see the text clearly.

5
00:00:20,580 --> 00:00:26,730
OK, so now here, in order to install Chango, what you need to do is that you need to type in PIP

6
00:00:26,880 --> 00:00:27,660
install.

7
00:00:28,890 --> 00:00:35,670
Django and hit Enter, and as you can see, it's going to start installing Django for you now.

8
00:00:35,670 --> 00:00:39,510
It's going to take a while depending upon the Internet connection speed.

9
00:00:39,990 --> 00:00:46,260
So give it a little bit of time if you have a slow Internet connection and your download will soon be

10
00:00:46,260 --> 00:00:46,590
ready.

11
00:00:46,620 --> 00:00:50,600
OK, so now it's actually installing the required packages.

12
00:00:50,610 --> 00:00:53,710
And now, as you can see, Django has been successfully installed.

13
00:00:54,120 --> 00:00:59,370
Now, once Django is installed, you now need to go ahead and also set up the pad for Django.

14
00:00:59,850 --> 00:01:06,300
So in order to set up the pad for Django, you actually need to first find out where exactly Django

15
00:01:06,300 --> 00:01:07,200
is present.

16
00:01:07,470 --> 00:01:13,140
So for that, what you need to do is that, first of all, you need to go ahead and open up the python

17
00:01:13,140 --> 00:01:16,740
shell and you need to open up the location where this is actually present.

18
00:01:17,130 --> 00:01:20,270
So simply open up the file location for Shell.

19
00:01:20,640 --> 00:01:22,800
And now here, these are the shortcuts.

20
00:01:22,830 --> 00:01:27,000
So again, right, click on the shortcut for Python and open the file location.

21
00:01:27,660 --> 00:01:32,750
And in here, as you can see, this is exactly the path the python is actually present.

22
00:01:32,760 --> 00:01:38,340
So if you go to scripts, as you can see, we have Django admin over here, which means that this is

23
00:01:38,340 --> 00:01:39,480
the path for Django.

24
00:01:39,810 --> 00:01:46,350
So simply copy this particular path and now open up the computer, which is this PC.

25
00:01:47,390 --> 00:01:54,560
So simply click on that and then hear if you actually right, click on this PC and if you click on properties,

26
00:01:55,040 --> 00:01:58,410
you will get an option for advanced system settings.

27
00:01:58,460 --> 00:02:04,020
So simply click on the advanced system settings option and go to environment variables.

28
00:02:04,040 --> 00:02:07,490
And here you can actually add or edit the path variable.

29
00:02:07,510 --> 00:02:16,190
So simply select that and click on edit the path and it's going to give you an option to add the new

30
00:02:16,190 --> 00:02:17,020
Padthaway here.

31
00:02:17,030 --> 00:02:21,110
So simply click on new and paste in the path which we have just copied.

32
00:02:21,830 --> 00:02:23,490
So now let's go ahead click.

33
00:02:23,490 --> 00:02:24,800
OK, click on.

34
00:02:24,800 --> 00:02:26,300
OK, click on.

35
00:02:26,300 --> 00:02:33,680
OK, and now you should be good to go now in order to make sure of Chango is actually installed properly.

36
00:02:33,680 --> 00:02:38,000
Just type in PBI Dash M space Shango.

37
00:02:39,300 --> 00:02:40,550
Double dash version.

38
00:02:41,400 --> 00:02:46,920
So when you type that thing in, as you can see, you get the Django vision as two point two point one,

39
00:02:46,920 --> 00:02:51,250
which means that Django has been now successfully installed on your computer.

40
00:02:51,750 --> 00:02:53,310
So that's it for this lecture.

41
00:02:53,310 --> 00:02:55,440
And I'll see you guys in the next one.

42
00:02:55,800 --> 00:02:56,430
Thank you.

