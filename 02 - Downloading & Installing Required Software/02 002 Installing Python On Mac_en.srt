1
00:00:00,240 --> 00:00:07,950
So in this lecture, we will go ahead and learn how to install Python on Mac, so the very first thing

2
00:00:07,950 --> 00:00:16,710
which you need to do is that you need to go to Google and search for download Python and then you need

3
00:00:16,710 --> 00:00:22,050
to go to the first link, which pops up, which is going to be from the official Python Web site.

4
00:00:22,080 --> 00:00:23,520
So simply click on that.

5
00:00:25,190 --> 00:00:31,520
And then, as you can see, you have a bunch of options over here to choose from, and the option which

6
00:00:31,520 --> 00:00:35,650
I recommend you guys to download is Python three point six point eight.

7
00:00:35,660 --> 00:00:38,540
So simply go here and click on download.

8
00:00:41,050 --> 00:00:46,840
And now if you scroll down a little bit on this particular Web page, you will actually get different

9
00:00:46,840 --> 00:00:48,130
installation options.

10
00:00:48,490 --> 00:00:54,420
So as you can see, we have an option over here, which is a Mac OS 64 bit installer.

11
00:00:54,790 --> 00:00:56,540
So simply click over here.

12
00:00:56,590 --> 00:01:00,570
And as you can see, it has started downloading Python for you.

13
00:01:00,700 --> 00:01:04,330
So once the download is finished, you simply need to open this thing up.

14
00:01:05,980 --> 00:01:09,300
And you need to continue with the installation steps over here.

15
00:01:09,370 --> 00:01:13,240
So simply click, continue, click on continue.

16
00:01:14,440 --> 00:01:23,260
Continue to agree with the license agreement, click on install, then you need to type in the password

17
00:01:23,260 --> 00:01:28,540
for your admin, so I'll type in mine and then I'll click on install software.

18
00:01:33,130 --> 00:01:39,500
And as you can see, it says, congratulations, Python has been successfully installed for Mac OS.

19
00:01:39,560 --> 00:01:40,460
Ten point nine.

20
00:01:40,900 --> 00:01:47,470
So now you can simply exit out of this and you can move the installation file to crash soil, simply

21
00:01:47,470 --> 00:01:50,160
move the installation file to crash.

22
00:01:50,680 --> 00:01:56,770
And now in order to check if Python has been successfully installed, you just need to go ahead.

23
00:01:56,870 --> 00:02:00,320
You need to open up the terminal, which is this terminal right here.

24
00:02:00,340 --> 00:02:06,880
So if you don't have the terminal window here, you can simply hold command space and you can type in

25
00:02:07,270 --> 00:02:09,660
terminal over here to open up the terminal.

26
00:02:10,120 --> 00:02:16,810
And in this terminal, you just need to go ahead and you need to make sure if Python is actually installed.

27
00:02:17,110 --> 00:02:21,490
So the Python version, which we have installed on our Mac is Python three.

28
00:02:21,550 --> 00:02:27,550
So in order to make sure if you have Python three installed on your machine, you type in Python three

29
00:02:27,880 --> 00:02:28,900
and hit enter.

30
00:02:29,410 --> 00:02:35,220
And as you can see, when I type in Python three and hit enter, I actually enter into Python Shell.

31
00:02:35,410 --> 00:02:42,730
So after typing in Python three, if you get these three arrows means that the python shell has started

32
00:02:42,730 --> 00:02:47,740
and US access will be able to download and install Python on your Mac.

33
00:02:48,220 --> 00:02:52,480
So now in order to exit out of the shell, you need to type an exit.

34
00:02:55,930 --> 00:02:59,130
And hit enter and now you're out of the Python show.

35
00:02:59,920 --> 00:03:03,700
So in the next lecture, we will learn how to install a Shango on my.

