1
00:00:00,670 --> 00:00:02,440
Hello and welcome to this lecture.

2
00:00:02,590 --> 00:00:05,600
So in this lecture, we learn how to download Python.

3
00:00:06,130 --> 00:00:09,970
So just go to Google and search for download Python.

4
00:00:11,130 --> 00:00:16,880
And the first language pops up is going to be from the python's official website, so click on that.

5
00:00:16,890 --> 00:00:23,400
And in here, if you scroll down a little bit, as you can see, download the Python three point six

6
00:00:23,400 --> 00:00:23,910
version.

7
00:00:24,150 --> 00:00:26,930
So click on that and scroll down again.

8
00:00:27,540 --> 00:00:34,560
And then after scrolling down here, as you can see for Windows, you need to actually install the X

9
00:00:34,560 --> 00:00:36,950
86 64 executable installer.

10
00:00:37,230 --> 00:00:38,580
So simply click on that.

11
00:00:39,330 --> 00:00:42,150
And now that is going to be downloaded for you.

12
00:00:42,180 --> 00:00:46,940
And now once it's finished downloading, you can now simply go ahead and open that thing up.

13
00:00:48,750 --> 00:00:55,290
And now here, as you can see, we have two options, so here we have installed another option and here

14
00:00:55,290 --> 00:00:57,110
we have customized installation.

15
00:00:57,450 --> 00:01:00,200
So select the first option, which is installed now.

16
00:01:00,210 --> 00:01:02,210
And first of all, click, add BAP.

17
00:01:03,750 --> 00:01:11,040
So now the installation has started and now, as you can see, you can click close and now just go to

18
00:01:11,040 --> 00:01:16,910
the search bar and search for Python and you open up Python three point six.

19
00:01:17,550 --> 00:01:20,700
So simply click on that and it will open up the terminal window.

20
00:01:21,150 --> 00:01:24,120
So this means Python is now successfully installed.

21
00:01:24,120 --> 00:01:31,550
So now if you go to the command prompt and now if you type in Python, double dash was over here.

22
00:01:32,460 --> 00:01:37,440
As you can see, you get the Python version and three point six point eight, which means that Python

23
00:01:37,440 --> 00:01:40,340
has been successfully installed on your computer.

24
00:01:40,830 --> 00:01:42,340
So that's it for this lecture.

25
00:01:42,510 --> 00:01:47,540
Hopefully you guys be able to understand how to download and install Python on Windows.

26
00:01:47,850 --> 00:01:51,810
So thank you very much for watching and I'll see you guys next time.

27
00:01:52,050 --> 00:01:52,650
Thank you.

