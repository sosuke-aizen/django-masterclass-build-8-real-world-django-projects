1
00:00:00,240 --> 00:00:07,320
In this lecture, we will learn how to download and install Visual Studio Code on Mac, so Visual Studio

2
00:00:07,320 --> 00:00:11,400
Code is actually the code editor, which will be using throughout this course.

3
00:00:11,700 --> 00:00:17,130
And if you already have a visual studio code or any other kind of code editor, download it, then you

4
00:00:17,130 --> 00:00:20,080
can go ahead and completely skip this lecture.

5
00:00:20,490 --> 00:00:25,710
So in order to download Visual Studio code, simply search for visual studio code on Google.

6
00:00:26,100 --> 00:00:32,159
And the first link which pops up is going to be the official, a link to download and install a visual

7
00:00:32,159 --> 00:00:32,910
studio code.

8
00:00:32,940 --> 00:00:34,650
So simply open that thing up.

9
00:00:35,370 --> 00:00:42,240
And now if you are downloading Visual Studio code for Mac, you just need to go ahead and click on download

10
00:00:42,240 --> 00:00:42,750
for Mac.

11
00:00:43,080 --> 00:00:48,990
And if you're a Windows, users simply select Windows and download the suitable file for Windows.

12
00:00:53,440 --> 00:00:59,200
So I'll simply click on download, and as you can see, the download for Visual Studio Code has been

13
00:00:59,200 --> 00:00:59,690
started.

14
00:01:00,280 --> 00:01:05,680
So once the download is completed, I simply need to go ahead and go through the installation steps

15
00:01:05,680 --> 00:01:10,150
for Visual Studio Code, and it will actually install visual studio code for us.

16
00:01:10,330 --> 00:01:13,080
So now, as you can see, the download is complete.

17
00:01:13,090 --> 00:01:15,280
So I can simply open this thing up.

18
00:01:15,760 --> 00:01:21,250
And as you can see now you have visual studio code and your download folder and you simply can open

19
00:01:21,250 --> 00:01:23,910
it up and just click on Open.

20
00:01:24,670 --> 00:01:27,840
And as you can see, Visual Studio Code has now started.

21
00:01:28,120 --> 00:01:33,910
So I already have visual studio code installed on my machine and that's the reason why I actually get

22
00:01:33,910 --> 00:01:34,980
this particular file.

23
00:01:35,260 --> 00:01:40,540
But if you are opening Visual Studio code for the very first time, you will get a completely new and

24
00:01:40,540 --> 00:01:41,450
blank project.

25
00:01:41,770 --> 00:01:46,000
So that's how you can go ahead and install visual studio code on your machine.

26
00:01:46,390 --> 00:01:50,230
So thank you very much for watching and I'll see you guys next time.

27
00:01:50,500 --> 00:01:51,040
Thank you.

