1
00:00:00,060 --> 00:00:06,180
So congratulations on finally making it to the end of the course, I hope you guys had a great time

2
00:00:06,180 --> 00:00:12,090
learning Shenango from this course and I hope you guys gained a lot of skills in order to be able to

3
00:00:12,090 --> 00:00:14,750
build your very own Django applications.

4
00:00:15,390 --> 00:00:22,200
So now, once you have completed the scores, I would like to mention a few tips on how to build your

5
00:00:22,200 --> 00:00:23,370
very own projects.

6
00:00:23,820 --> 00:00:30,390
So obviously now you do have those skills and you know how exactly Django applications are built and

7
00:00:30,390 --> 00:00:31,160
how they work.

8
00:00:31,530 --> 00:00:36,780
But let's suppose if you still want to build an application all on your own, here's how you can build

9
00:00:36,780 --> 00:00:37,070
that.

10
00:00:37,770 --> 00:00:43,020
So whenever you are building your Django application, you need to understand the three things that

11
00:00:43,020 --> 00:00:45,110
is the model you view and the template.

12
00:00:45,510 --> 00:00:48,720
So Django actually follows the architecture.

13
00:00:48,930 --> 00:00:55,420
So you just need to remember that the model is something which actually handles the entire database

14
00:00:55,440 --> 00:00:55,920
part.

15
00:00:56,070 --> 00:01:02,640
And using this model, you can actually create the schema of your database and whatever front and logic

16
00:01:02,970 --> 00:01:09,810
which you need to write in Python, like, for example, data processing, data manipulation, data

17
00:01:09,810 --> 00:01:11,610
analysis, everything like that.

18
00:01:11,790 --> 00:01:15,950
You can actually place all of that Django code in your views.

19
00:01:15,960 --> 00:01:16,980
Dot a file.

20
00:01:17,980 --> 00:01:24,460
And for accepting data from the user, you can simply go ahead, simply create an HTML form, submit

21
00:01:24,460 --> 00:01:30,760
that form with a post request and in the view you can actually go ahead and check if the request is

22
00:01:30,760 --> 00:01:36,960
equal to post and if the request is equal to post, you can simply go ahead and get the data from there

23
00:01:37,270 --> 00:01:43,930
and then you can go ahead, use that user data and you can perform manipulation on that data using any

24
00:01:43,930 --> 00:01:46,480
kind of python code which you want in your view.

25
00:01:47,020 --> 00:01:52,750
And obviously when you want to return the result, when you're returning those results, you can simply

26
00:01:52,750 --> 00:01:59,050
go ahead and return that result as a context and parcel to a particular HTML template.

27
00:01:59,740 --> 00:02:04,840
So this is just a brief overview of how you can build any kind of application.

28
00:02:05,050 --> 00:02:10,600
So I hope this actually gives you an idea of how to actually proceed through building a certain Web

29
00:02:10,600 --> 00:02:13,170
application and a certain functionality.

30
00:02:13,450 --> 00:02:18,970
So let's say, for example, if you're a building certain functionality or a certain Web application,

31
00:02:19,210 --> 00:02:25,120
first of all, understand what are going to be the inputs of that particular application, how you are

32
00:02:25,120 --> 00:02:27,260
going to accept the input from the user.

33
00:02:27,310 --> 00:02:32,890
So when you have that in mind, you can design your forms in that particular manner, then you need

34
00:02:32,890 --> 00:02:38,510
to understand how the data is going to be manipulated inside that particular app.

35
00:02:38,950 --> 00:02:44,290
So when you accept the user data from the form now, you need to go ahead and perform some operations

36
00:02:44,290 --> 00:02:45,100
on that data.

37
00:02:45,520 --> 00:02:52,030
And you can do that in the views and then you can use the Jambos Warren object relational mapping to

38
00:02:52,030 --> 00:02:58,600
actually see that data into the database by just simply creating an object of that model and using this

39
00:02:58,810 --> 00:02:59,260
method.

40
00:02:59,320 --> 00:03:05,260
So now once you have completed the course, you can just go ahead and start building your own Shango

41
00:03:05,260 --> 00:03:07,970
project and keep on exploring new things in Django.

42
00:03:08,320 --> 00:03:13,120
And one more important resource, which I would like to mention here, is that if you want to learn

43
00:03:13,360 --> 00:03:19,360
in much more depth about how Django exactly works, please refer to the official Django documentation,

44
00:03:19,360 --> 00:03:25,150
which is one of the best resources you should be looking at if you actually want to understand in depth

45
00:03:25,150 --> 00:03:27,140
how exactly things work in Django.

46
00:03:27,160 --> 00:03:27,990
So that's it.

47
00:03:28,000 --> 00:03:29,680
And thank you very much for watching.

