1
00:00:00,660 --> 00:00:06,510
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to basically

2
00:00:06,510 --> 00:00:09,030
load up this particular project on a sailboat.

3
00:00:09,450 --> 00:00:14,580
So right now, we obviously don't have a server on which we can connect to the Internet.

4
00:00:14,850 --> 00:00:20,400
But Janger actually gives us a server on which we can test our Web applications.

5
00:00:20,760 --> 00:00:22,910
So right now, we do have a project in here.

6
00:00:23,040 --> 00:00:26,290
And let's suppose you want to run this project on a server.

7
00:00:26,400 --> 00:00:28,120
So how exactly can you do that?

8
00:00:29,400 --> 00:00:31,850
So first of all, you need to open up the terminal.

9
00:00:31,890 --> 00:00:37,440
So right now, we have already opened up the terminal and we are currently into the new directorate.

10
00:00:37,980 --> 00:00:44,510
So now what you need to do is that you need to navigate to the project directory, which is my site.

11
00:00:44,880 --> 00:00:48,870
So let's go into my site by typing KDDI, my site.

12
00:00:49,500 --> 00:00:56,760
And once we enter this particular folder, we can now go ahead and use the managed dorper command.

13
00:00:57,180 --> 00:01:03,240
So whenever you want to start up your server, you simply type in Python managed by.

14
00:01:04,599 --> 00:01:06,440
Run so.

15
00:01:06,590 --> 00:01:10,690
So now when you go ahead and execute this command, it's going to start up your.

16
00:01:11,890 --> 00:01:18,100
But now in my case, I actually have both the versions of Python install on my machine.

17
00:01:18,100 --> 00:01:21,160
That is, I have Python two as well as Python three.

18
00:01:21,580 --> 00:01:28,570
So I need to explicitly go ahead and mention Python three over here every time I use Python, because

19
00:01:28,570 --> 00:01:34,380
if I go ahead and just simply type in Python here, my computer knows that Python means Python too.

20
00:01:35,140 --> 00:01:40,870
So if you're using both versions of Python, if you have them on your computer, then you need to explicitly

21
00:01:40,870 --> 00:01:42,280
mention Python three here.

22
00:01:42,760 --> 00:01:48,520
But if you only have Python three installed, you can just go ahead and type in Python here and it's

23
00:01:48,520 --> 00:01:49,450
going to work fine.

24
00:01:49,900 --> 00:01:57,220
So I'll just go ahead and type in Python three managed by Rienzo and when I go ahead and hit enter.

25
00:01:57,250 --> 00:02:02,620
As you can see, it says Starting the development Serbo at this particular you are.

26
00:02:03,490 --> 00:02:08,970
So now if you want to see what exactly is present on the server, you just need to copy that.

27
00:02:10,240 --> 00:02:17,080
And what I will do here is that I'll open up my Web browser and in here you just need to piece this

28
00:02:17,080 --> 00:02:17,920
particular you out.

29
00:02:18,430 --> 00:02:24,760
Now remember that this you are Playskool as a localhost address and this address is nothing but the

30
00:02:24,760 --> 00:02:26,340
address of your local machine.

31
00:02:26,980 --> 00:02:32,920
And this thing right here, which is eight thousand, is actually the number on which this application

32
00:02:32,920 --> 00:02:33,340
runs.

33
00:02:33,940 --> 00:02:39,670
So Osoba has multiple ports and you can have multiple applications over there on different ports.

34
00:02:40,240 --> 00:02:45,520
So right now, by default, the number for Zango applications is 8000.

35
00:02:45,880 --> 00:02:52,720
So when I go ahead and hit enter, as you can see, we get a triangle site over here, which is that

36
00:02:52,720 --> 00:02:54,640
the install work successfully.

37
00:02:54,650 --> 00:02:55,720
Congratulations.

38
00:02:56,680 --> 00:02:59,350
So right now our site is up and running on our.

39
00:02:59,430 --> 00:03:05,410
So that means we have successfully set up the project and now we can start running our applications

40
00:03:05,410 --> 00:03:05,800
in here.

41
00:03:06,610 --> 00:03:11,500
Now, one more interesting thing which you can do here is that instead of this address right over here,

42
00:03:12,220 --> 00:03:13,240
you can type in.

43
00:03:14,850 --> 00:03:17,950
Localhost eight thousand.

44
00:03:18,210 --> 00:03:22,260
And when you go ahead and hit enter, this is going to work exactly the same.

45
00:03:22,530 --> 00:03:27,910
So you can now go ahead and test your zango web application on the localhost.

46
00:03:28,020 --> 00:03:30,760
So this is how you can actually go ahead and run the server.

47
00:03:31,200 --> 00:03:33,820
And now let's assume you want to stop the server.

48
00:03:33,870 --> 00:03:38,290
You simply need to press control, see, and it's going to automatically stop your server.

49
00:03:38,910 --> 00:03:45,420
So when your server is stopped and when you go ahead and hit refresh now, as you can see, it says

50
00:03:45,420 --> 00:03:49,990
that this site can be reached because the server is not up and running yet.

51
00:03:50,610 --> 00:03:55,560
So, again, if you want to restart the server, you again need to type in the command, which is Python

52
00:03:55,560 --> 00:03:57,960
three managed or be wire and server.

53
00:03:58,410 --> 00:04:01,500
And you what is going to be up and running once again?

54
00:04:01,740 --> 00:04:03,720
So now if I go ahead, hit refresh.

55
00:04:03,720 --> 00:04:09,030
As you can see, we have our site or Web application loaded up on the server.

56
00:04:10,910 --> 00:04:12,370
So that's it for this lecture.

57
00:04:12,560 --> 00:04:17,779
Hope you guys were able to understand how to start and stop a SO in the upcoming lecture.

58
00:04:17,810 --> 00:04:21,490
We will go ahead and learn something about the Django app.

59
00:04:22,100 --> 00:04:26,180
So thank you very much for watching and I'll see you guys next time.

60
00:04:26,420 --> 00:04:27,020
Thank you.

