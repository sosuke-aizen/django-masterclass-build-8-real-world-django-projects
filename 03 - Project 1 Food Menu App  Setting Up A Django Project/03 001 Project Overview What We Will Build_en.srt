1
00:00:00,150 --> 00:00:05,620
So this is what exactly we will be building in this specific section.

2
00:00:05,640 --> 00:00:13,170
So this site is completely designed using Chingo and this is a food menu site where you can actually

3
00:00:13,170 --> 00:00:19,830
go ahead, have a bunch of food items, its image, the details of those items, and you can actually

4
00:00:19,830 --> 00:00:20,310
go ahead.

5
00:00:20,340 --> 00:00:25,930
Also, click on these details and actually get a detailed view of that specific item.

6
00:00:26,370 --> 00:00:31,830
So if you go ahead and click on any item over here, you are going to get the image, its name, its

7
00:00:31,830 --> 00:00:33,080
detail and the price.

8
00:00:33,570 --> 00:00:39,420
And one more additional thing which you can do with this is that if you visit any particular details

9
00:00:39,420 --> 00:00:42,600
page over here, you will also have a delete button over here.

10
00:00:42,690 --> 00:00:48,570
So if you click on that specific delete button, it's going to ask you for a confirmation to delete

11
00:00:48,570 --> 00:00:49,310
that item.

12
00:00:49,770 --> 00:00:54,540
And if you click on confirm, it's actually going to go ahead and believe that item.

13
00:00:54,570 --> 00:00:58,230
So, as you can see, the Bergel has now been deleted.

14
00:00:58,230 --> 00:01:03,900
And one more additional feature which we have built into the site is that we have built in the login

15
00:01:03,900 --> 00:01:04,379
feature.

16
00:01:04,709 --> 00:01:06,560
So I can simply log in over here.

17
00:01:06,780 --> 00:01:09,690
So I have already created a login for the site.

18
00:01:10,080 --> 00:01:14,280
So I'll just type in my name over here and I'll type in the password.

19
00:01:17,440 --> 00:01:25,120
And now when I click on login, as you can see now I'm logged in as the user and now I can actually

20
00:01:25,120 --> 00:01:30,250
go ahead and now as I am logged in, I can go ahead and add a item.

21
00:01:30,260 --> 00:01:36,820
So if I click on add item now, I can actually go ahead and add in some items like, let's see, item

22
00:01:36,820 --> 00:01:37,300
one.

23
00:01:37,900 --> 00:01:42,550
Let's say the description is this is a food item one.

24
00:01:43,330 --> 00:01:49,870
Let's see the item price is twelve dollars and let's say the item link is actually equal of some image.

25
00:01:50,290 --> 00:01:53,140
So I already have copied a specific image.

26
00:01:53,560 --> 00:01:58,890
And now if I click on ADD, as you can see, that item is now added up over here.

27
00:01:58,900 --> 00:02:06,250
And now if you go to the homepage, as you can see, if you scroll down, you will get that we have

28
00:02:06,250 --> 00:02:12,520
this item, which is the item name, you have the item description, you have the item price.

29
00:02:12,520 --> 00:02:17,710
And you also have that this item has been added by this specific user.

30
00:02:18,520 --> 00:02:27,460
Now you can go ahead and log out and it says you have been logged out and you can now go ahead and also

31
00:02:27,700 --> 00:02:33,180
register a new user by going to register and let's create a new user in here.

32
00:02:33,580 --> 00:02:36,730
So I'll give this a name as new user.

33
00:02:36,940 --> 00:02:37,810
One hundred.

34
00:02:38,680 --> 00:02:45,250
And let's say the email is nothing but demo at demo dot com.

35
00:02:46,850 --> 00:02:55,640
And let's say the password is best test for five six test test.

36
00:02:56,670 --> 00:03:02,900
Four, five, six, I can now sign up as well, and it says, Welcome, new user 100.

37
00:03:02,910 --> 00:03:04,240
Your account is created.

38
00:03:04,320 --> 00:03:07,530
So now I can log in, so I will see you.

39
00:03:07,530 --> 00:03:14,560
User one hundred as my login and the password is best, best four, five, six.

40
00:03:15,030 --> 00:03:19,150
So when I log in, as you can see now, I am logged in as a new user.

41
00:03:19,530 --> 00:03:22,260
And now let's try to add another item over here.

42
00:03:22,630 --> 00:03:27,330
So I click on ADD item and I'll create a new item.

43
00:03:27,420 --> 00:03:29,370
So I'll see a new item too.

44
00:03:30,210 --> 00:03:31,470
And let's see the item.

45
00:03:31,470 --> 00:03:34,440
Description is this is new item two.

46
00:03:34,830 --> 00:03:36,480
I can see the item prices.

47
00:03:36,480 --> 00:03:37,110
Let's see.

48
00:03:37,740 --> 00:03:38,760
Twenty dollars.

49
00:03:39,120 --> 00:03:42,750
And you can also leave this item image default.

50
00:03:43,230 --> 00:03:47,100
So I'll just keep the image as default and I'll click on ADD.

51
00:03:47,790 --> 00:03:52,110
And as you can see, it says new item two and this is new item two.

52
00:03:52,110 --> 00:03:55,430
That means this item has now been added on our site.

53
00:03:55,860 --> 00:04:02,850
So if I go to add item or let's say if I go to food and if I scroll down to the bottom, as you can

54
00:04:02,850 --> 00:04:04,470
see, it says new item.

55
00:04:04,500 --> 00:04:07,140
This is new item and it's added by a new user.

56
00:04:07,140 --> 00:04:07,750
One hundred.

57
00:04:08,430 --> 00:04:12,740
So this is the kind of site which will be building throughout this entire section.

58
00:04:13,230 --> 00:04:17,670
That is we will design this entire functionality in Chingo.

59
00:04:17,790 --> 00:04:22,910
So in the next lecture, let's go ahead and start building the site entirely from scratch.

