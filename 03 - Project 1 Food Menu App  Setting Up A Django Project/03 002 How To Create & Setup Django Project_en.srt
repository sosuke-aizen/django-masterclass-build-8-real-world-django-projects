1
00:00:00,330 --> 00:00:06,000
Hello and welcome to this lecture and from this lecture onwards, let's actually go ahead and start

2
00:00:06,000 --> 00:00:08,160
building Shango application.

3
00:00:08,670 --> 00:00:13,500
So the first thing we need to do is then you need to know how to create a Django project.

4
00:00:13,870 --> 00:00:18,400
So what we will do in this case is that will create a folder on your desktop.

5
00:00:18,810 --> 00:00:25,890
So let me just go ahead and create a new folder and let me just name this folder as our new you can

6
00:00:25,890 --> 00:00:28,020
actually call this folder as anything.

7
00:00:28,290 --> 00:00:33,990
But for now, I'm just going to name this thing as new and this is going to contain all of our Django

8
00:00:33,990 --> 00:00:34,590
projects.

9
00:00:35,220 --> 00:00:41,160
So now, once you have this particular folder, in order to create a Django project, you first need

10
00:00:41,160 --> 00:00:43,740
to open up the command prompt or terminal.

11
00:00:44,310 --> 00:00:51,840
So if you're on Mac, like I am so simply press command space and search for terminal.

12
00:00:52,080 --> 00:00:56,610
And if you're on windows, you can search for the command prompt ACMD.

13
00:00:57,520 --> 00:01:03,460
So once this terminal window is opened up, what do you have to do is that you have to navigate to the

14
00:01:03,460 --> 00:01:04,120
desktop.

15
00:01:04,690 --> 00:01:09,630
So now if you're on Windows, you need to follow the exact same steps as well.

16
00:01:09,940 --> 00:01:16,530
So in order to navigate to the desktop, you need to type in code and you need to type in a desktop.

17
00:01:16,990 --> 00:01:20,080
So right here means change directly.

18
00:01:20,080 --> 00:01:24,310
And by changing directly, we need to go into the directory, which is desktop.

19
00:01:25,120 --> 00:01:32,350
So when I go ahead hit enter now we are into the desktop and now we actually want to go into this particular

20
00:01:32,350 --> 00:01:33,200
new folder.

21
00:01:33,490 --> 00:01:38,160
So in order to go there again to open KDDI, new hit Enter.

22
00:01:38,680 --> 00:01:45,460
And as you can see, it now shows new here, which means that we are into this particular folder now

23
00:01:45,460 --> 00:01:46,650
for Windows users.

24
00:01:47,110 --> 00:01:52,690
When you open up the command prompt, make sure that you are into your users folder here and from the

25
00:01:52,690 --> 00:01:56,150
user's folder, you need to navigate to the desktop.

26
00:01:56,350 --> 00:02:02,800
So once it shows users here, you need to navigate to the desktop and then you need to navigate down

27
00:02:02,830 --> 00:02:04,330
to this particular new folder.

28
00:02:05,530 --> 00:02:10,220
OK, so now once we are here, let's go ahead and learn how to create a project.

29
00:02:10,930 --> 00:02:17,050
So in order to create a project, there is a special command in Django, which is called as a Django

30
00:02:17,050 --> 00:02:18,670
admin style project.

31
00:02:19,090 --> 00:02:27,670
So if you want to start a project, you type in Django Dash admin, then type and start a project and

32
00:02:27,670 --> 00:02:30,310
then you need to name your project as something.

33
00:02:30,850 --> 00:02:37,690
So right now, let's assume we call our project, ask something like, let's say my site.

34
00:02:38,020 --> 00:02:41,380
So I'll type in my site over here and hit enter.

35
00:02:42,430 --> 00:02:47,590
And now, as soon as I hit Enter the Myside Project is going to be created.

36
00:02:47,980 --> 00:02:53,370
Now, if you want to check if this project is actually created, you can actually open the folder up.

37
00:02:53,380 --> 00:02:59,410
So if you open up this new folder, as you can see, we now have a myside folder in here, which is

38
00:02:59,410 --> 00:03:01,180
nothing but Azango project.

39
00:03:02,180 --> 00:03:08,380
Now, once we have this project over here, it's going to have a bunch of files and we are actually

40
00:03:08,380 --> 00:03:11,550
going to find out what these files are and what they do.

41
00:03:12,220 --> 00:03:19,450
So in order to find that out, let's first go ahead and open up this folder in an idea or in a code

42
00:03:19,450 --> 00:03:19,930
editor.

43
00:03:20,240 --> 00:03:22,030
So I'll be using voice code.

44
00:03:22,780 --> 00:03:25,120
So let's simply open up a visual studio code.

45
00:03:25,120 --> 00:03:33,940
And now let me just go to file and I need to open the current project, which is let's go to desktop.

46
00:03:35,290 --> 00:03:37,690
Let's go to New.

47
00:03:38,930 --> 00:03:44,360
And let's open this thing up, so now as I open this thing up, as you can see, we have this particular

48
00:03:44,360 --> 00:03:45,770
myside project in here.

49
00:03:46,640 --> 00:03:49,850
So now let's understand the significance of these files right here.

50
00:03:50,090 --> 00:03:57,650
So this outermost directly, which is my site, actually acts as a container to hold all your project

51
00:03:57,650 --> 00:03:58,630
related files.

52
00:03:59,180 --> 00:04:03,260
So as you can see, this is our project and these are the project related files.

53
00:04:03,560 --> 00:04:07,130
And the myside directly actually holds each one of those files.

54
00:04:07,880 --> 00:04:12,080
Now, let's talk about the other file, which is the managed profile.

55
00:04:12,110 --> 00:04:15,690
So if you have a look over here, it has a bunch of code in here.

56
00:04:16,130 --> 00:04:22,160
And what this file essentially does is that it's actually a file which allows you to interact with your

57
00:04:22,160 --> 00:04:23,080
Django project.

58
00:04:23,600 --> 00:04:29,360
So let's say, for example, if you want to start up the SERBO for the existing project, you can do

59
00:04:29,360 --> 00:04:32,120
the same by using this managed or by file.

60
00:04:33,380 --> 00:04:37,580
Now, let's go and talk about in IDOT profile, which is this file right here.

61
00:04:37,820 --> 00:04:41,540
Now, if you have a look at this file, this file is completely empty.

62
00:04:41,960 --> 00:04:49,040
And the job of this file is to tell Python that the current directorate, which means the myside directory,

63
00:04:49,040 --> 00:04:51,500
should be considered as a python package.

64
00:04:51,650 --> 00:04:58,310
So the sole purpose of this file is to tell Python that the myside directory is actually a python package.

65
00:04:59,280 --> 00:05:05,070
Now, let's move on to the settings, our profile and the settings, our profile is actually going to

66
00:05:05,070 --> 00:05:09,540
contain all the settings and configuration for the current and Shango project.

67
00:05:09,960 --> 00:05:14,520
So, for example, it has information like what kind of database we are using.

68
00:05:15,480 --> 00:05:21,570
So if you scroll down a little bit over here, as you can see, it's going to state that we are using

69
00:05:21,570 --> 00:05:23,980
a database which is escalatory.

70
00:05:24,450 --> 00:05:29,840
So if you want to choose any other database, we can make changes to this particular configuration here.

71
00:05:30,330 --> 00:05:36,810
And that's the main purpose of the settings DOT profile, that is to store the configuration of existing

72
00:05:36,810 --> 00:05:37,860
Shango project.

73
00:05:37,890 --> 00:05:43,780
Now, once we know about the settings or profile, let's talk about the you are all start by file.

74
00:05:44,520 --> 00:05:50,610
So this particular you are a spy file will contain all the you are old patterns which are supposed to

75
00:05:50,610 --> 00:05:52,140
be matched with the incoming.

76
00:05:52,140 --> 00:05:53,180
You are a request.

77
00:05:53,520 --> 00:05:59,400
So if you recall from the previous lecture, as I also mentioned, we need to handle the incoming client

78
00:05:59,400 --> 00:05:59,970
request.

79
00:06:00,000 --> 00:06:04,100
So for example, Facebook dot com slash user one.

80
00:06:04,380 --> 00:06:08,370
So we are going to receive such kind of request and Django project.

81
00:06:08,640 --> 00:06:14,250
And in order to handle those requests, we need a file which is going to match up those requests with

82
00:06:14,250 --> 00:06:15,540
a particular pattern.

83
00:06:15,640 --> 00:06:17,940
And that job is actually done by this.

84
00:06:17,940 --> 00:06:19,400
You all start by file.

85
00:06:19,890 --> 00:06:24,810
So this is actually going to contain the pattern which is going to match up with the incoming request.

86
00:06:24,840 --> 00:06:30,180
So right now, as you can see, we have something which is called, as you are, old patterns over here,

87
00:06:30,480 --> 00:06:32,670
and it contains a bunch of patterns in here.

88
00:06:33,000 --> 00:06:35,210
So right now, it only has one pattern.

89
00:06:35,220 --> 00:06:40,910
But as we go ahead and start building the site, we are going to have more patterns in here.

90
00:06:42,370 --> 00:06:48,850
Now, this information might seem a little bit overwhelming and you might be confused with all of the

91
00:06:48,850 --> 00:06:55,300
terminologies and everything like that, but as we dive deeper into Chango, all these things are going

92
00:06:55,300 --> 00:06:56,510
to start making sense.

93
00:06:57,100 --> 00:07:02,680
So if you are learning Zango for the very first time, don't get intimidated by all the terminologies.

94
00:07:03,010 --> 00:07:10,750
And this might seem a little bit confusing, but if you give time to let things sink in, you will eventually

95
00:07:10,750 --> 00:07:16,410
find that Django is extremely easy to use and extremely easy to build websites.

96
00:07:16,450 --> 00:07:22,300
Now, once we know what are the significance of these files, let's actually go ahead and learn how

97
00:07:22,300 --> 00:07:23,440
to start up yourself.

98
00:07:24,130 --> 00:07:29,310
So in the next lecture, we will go ahead and learn how to start up the server, which is provided by

99
00:07:29,310 --> 00:07:32,030
a triangle so that we can test our application.

100
00:07:32,500 --> 00:07:36,520
So thank you very much for watching and I'll see you guys next time.

101
00:07:37,000 --> 00:07:37,630
Thank you.

