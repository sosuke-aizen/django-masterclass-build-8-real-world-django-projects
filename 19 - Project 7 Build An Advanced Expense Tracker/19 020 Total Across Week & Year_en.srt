1
00:00:00,090 --> 00:00:01,230
In this particular lecture.

2
00:00:01,230 --> 00:00:06,140
Let's go ahead and calculate the monthly sum, the weekly sum, so on and so forth.

3
00:00:06,150 --> 00:00:12,390
So in order to calculate the sum across the specific days, we actually need to consider the dates on

4
00:00:12,390 --> 00:00:14,180
which the expenses were made.

5
00:00:14,190 --> 00:00:19,920
So, for example, let's say if you have to find out the expenses made in the last seven days, in that

6
00:00:19,920 --> 00:00:23,600
particular case, you have to go back to the date of the last seven days.

7
00:00:23,610 --> 00:00:28,530
Consider all of those expenses on that particular date and then sum them up together.

8
00:00:28,530 --> 00:00:33,390
And in order to do that, we need to find the difference between the current dates and the dates of

9
00:00:33,390 --> 00:00:35,460
the expenses which we have up over here.

10
00:00:35,730 --> 00:00:37,440
So if you already have those dates.

11
00:00:37,440 --> 00:00:41,520
But now the question is how exactly would you find differences between the dates?

12
00:00:41,520 --> 00:00:45,870
So we will be able to do that when we make use of the date time object.

13
00:00:45,930 --> 00:00:51,930
And the date time object is going to help us to calculate the difference between or the number of days

14
00:00:51,930 --> 00:00:55,210
between the current day and that expense date.

15
00:00:55,230 --> 00:00:57,120
So let's go ahead and implement that.

16
00:00:57,120 --> 00:01:00,480
So first of all, I'll go ahead and import date time here inside views.

17
00:01:00,780 --> 00:01:03,210
So I would say import and date time.

18
00:01:03,870 --> 00:01:11,220
And for now, let's say you want to calculate the expenses which are made in the last, let's say 365

19
00:01:11,220 --> 00:01:12,630
days or a year.

20
00:01:13,200 --> 00:01:18,420
So in order to calculate that, you have to go inside this index view and you have to go after the total

21
00:01:18,420 --> 00:01:19,290
expenses.

22
00:01:19,710 --> 00:01:22,140
And it's actually better if you delete this.

23
00:01:22,410 --> 00:01:29,220
And now I would add a comment here saying logic to calculate.

24
00:01:30,880 --> 00:01:35,140
365 days expenses.

25
00:01:35,990 --> 00:01:36,300
Okay.

26
00:01:36,350 --> 00:01:41,990
And here what we currently do is that, first of all, we get the current date time for today.

27
00:01:41,990 --> 00:01:44,450
So we make use of the date time object.

28
00:01:44,750 --> 00:01:47,690
So we say date time, dot, date, dot.

29
00:01:47,690 --> 00:01:49,340
And we want today's date.

30
00:01:49,340 --> 00:01:50,570
So we say today.

31
00:01:50,870 --> 00:01:56,960
So this kind of gives us access to the existing date and we want to go back to the date for the past

32
00:01:56,960 --> 00:01:58,160
365 days.

33
00:01:58,160 --> 00:02:00,260
So I say date time.

34
00:02:01,640 --> 00:02:03,770
Dot time, Delta time.

35
00:02:03,770 --> 00:02:09,919
Delta basically means you want to go back a couple of days and we want to go back 365 days.

36
00:02:09,919 --> 00:02:11,630
So I would say three, six, five.

37
00:02:12,110 --> 00:02:17,540
And the difference is going to give us the entire last three, 6 to 5 days, which is nothing but let's

38
00:02:17,540 --> 00:02:18,350
say last year.

39
00:02:18,350 --> 00:02:21,470
So I would say last underscore year.

40
00:02:23,050 --> 00:02:24,370
Equals this.

41
00:02:25,290 --> 00:02:32,400
Now, once we have the expenses or all the expenses in our database, we kind of make use of this last

42
00:02:32,400 --> 00:02:37,750
year to find out expenses which were actually made greater than last year, which is the current year.

43
00:02:37,770 --> 00:02:46,680
So here we say expense dot objects, dot filter, and we want to filter out the expenses and we want

44
00:02:46,680 --> 00:02:53,190
to say that the date is double underscore g t g t means greater than and we want the date greater than

45
00:02:53,190 --> 00:02:57,660
the last year, which we have just calculated, which means the last 365 days.

46
00:02:58,050 --> 00:03:04,890
And then this is going to give us access to all the objects which follow this particular condition.

47
00:03:04,890 --> 00:03:08,670
So let's see if those particular objects into a variable called as data.

48
00:03:09,450 --> 00:03:14,790
And now what we do is that once we have all of those expenses, that is the expenses made in the last

49
00:03:14,790 --> 00:03:21,120
365 days, we kind of go ahead and perform the same kind of aggregation as we have performed here.

50
00:03:21,390 --> 00:03:27,270
So here we have said expenses, dot aggregate sum, the amount, and we are going to be doing the same

51
00:03:27,270 --> 00:03:28,500
thing over here as well.

52
00:03:28,500 --> 00:03:35,100
So here I'll be set up over here and instead of saying expenses, now we want to make that query on

53
00:03:35,130 --> 00:03:36,360
the data.

54
00:03:36,360 --> 00:03:38,280
So I would say data equals this.

55
00:03:39,300 --> 00:03:44,760
Now this is going to give us the sum of expenses across a year, and therefore I'm going to save it

56
00:03:44,760 --> 00:03:47,760
into a variable called as yearly sum.

57
00:03:47,940 --> 00:03:49,740
So yearly sum equals this.

58
00:03:50,580 --> 00:03:55,710
Now, once we have this yearly sum, you simply have to pass a yearly sum as context over here.

59
00:03:56,520 --> 00:04:01,200
But even before that, let's actually try printing it up and let's see the kind of result we would get.

60
00:04:01,200 --> 00:04:02,130
So print.

61
00:04:02,840 --> 00:04:03,860
Yearly sum.

62
00:04:04,340 --> 00:04:07,190
And once we do that, we are pretty much good to go.

63
00:04:07,370 --> 00:04:08,900
So now let's go back here.

64
00:04:08,930 --> 00:04:10,520
Let's make a guest request.

65
00:04:10,970 --> 00:04:15,740
And if we go to the terminal, I should be able to get the yearly sum, which is this.

66
00:04:15,740 --> 00:04:21,470
And the yearly sum is going to be the same because all the expenses which we have made actually are

67
00:04:21,470 --> 00:04:22,730
in the same month.

68
00:04:22,760 --> 00:04:28,850
Now, if you actually want to check if this thing is working correctly, what you could do is that we

69
00:04:28,850 --> 00:04:32,330
have expenses across, I guess, three days.

70
00:04:32,480 --> 00:04:38,630
So let's actually minimize it to, let's say one and let's see the kind of result we would get.

71
00:04:38,870 --> 00:04:45,530
So if I say these equals one, if I make a get request again, and if I go to the terminal now, as

72
00:04:45,530 --> 00:04:52,280
you can see, it only gives us expense for today, which is $123,000, which is this expense right here.

73
00:04:52,310 --> 00:04:57,560
Now, in order to again make another check, I would change the days to two.

74
00:04:58,130 --> 00:05:06,710
And this time, if I go back here and make a get request, I should be able to get the $333 plus this

75
00:05:06,710 --> 00:05:07,250
expense.

76
00:05:07,250 --> 00:05:12,150
So if I go to the terminal, as you can see, that's exactly what we get here.

77
00:05:12,170 --> 00:05:17,630
That means this logic of actually calculating the expense across different days is working.

78
00:05:18,020 --> 00:05:20,270
So here I could see 365.

79
00:05:20,390 --> 00:05:23,870
So that kind of gives us data for 365 days.

80
00:05:24,170 --> 00:05:29,870
If I want to calculate the same kind of expenses over the past 30 days, I could make use of the same

81
00:05:29,870 --> 00:05:30,590
logic here.

82
00:05:31,100 --> 00:05:32,510
So I could delete this.

83
00:05:33,890 --> 00:05:37,820
Copy this entire snippet of code pick set up over here.

84
00:05:38,540 --> 00:05:40,790
Let's calculate it for 30 days now.

85
00:05:40,790 --> 00:05:44,570
And instead of saying last year, I would say last month.

86
00:05:46,460 --> 00:05:47,840
This is going to be the same.

87
00:05:47,840 --> 00:05:51,980
The only thing that's going to change is let's change the number of days to 30.

88
00:05:52,550 --> 00:05:54,280
This is going to be the same.

89
00:05:54,290 --> 00:05:57,440
This is going to change to last month.

90
00:05:58,290 --> 00:06:00,900
This is going to change to monthly some.

91
00:06:03,160 --> 00:06:07,960
And the same thing could be done to calculate the weekly expenses as well.

92
00:06:08,410 --> 00:06:10,990
So here again, copy the same thing.

93
00:06:11,020 --> 00:06:12,730
Change it to seven days.

94
00:06:13,570 --> 00:06:14,740
Change this thing to.

95
00:06:14,750 --> 00:06:15,520
Let's see.

96
00:06:18,700 --> 00:06:20,260
Last week.

97
00:06:22,610 --> 00:06:25,160
This is going to be seven days instead of 30.

98
00:06:26,510 --> 00:06:29,540
This is also going to be changed to last week as well.

99
00:06:31,870 --> 00:06:34,120
And this is going to change to.

100
00:06:35,590 --> 00:06:36,520
Weekly sum.

101
00:06:36,550 --> 00:06:42,190
Okay, So now once we have this, now we simply go ahead, take all of these objects which we have here

102
00:06:42,190 --> 00:06:46,270
and kind of parse those objects here just as we have passed in the total expenses.

103
00:06:46,540 --> 00:06:49,630
So here we pass in yearly sum, monthly sum and weekly sum.

104
00:06:49,630 --> 00:06:51,190
So let's do that real quick.

105
00:06:51,700 --> 00:06:57,970
So I would say yearly some pass it as context.

106
00:06:58,000 --> 00:06:58,960
Yearly sum.

107
00:06:59,870 --> 00:07:07,730
Do the same thing with weekly, some weekly, underscore some as weekly some.

108
00:07:07,730 --> 00:07:10,820
And then finally we have the monthly sum.

109
00:07:10,820 --> 00:07:11,660
So.

110
00:07:12,490 --> 00:07:17,050
Monthly sum is going to be, let's say, monthly some.

111
00:07:18,020 --> 00:07:23,660
Okay, So now once we have past this context, we could finally go ahead, retrieve all of that context

112
00:07:23,660 --> 00:07:27,100
and display that up over here inside the indexed HTML.

113
00:07:27,320 --> 00:07:30,560
But we are not going to simply display that just as it is.

114
00:07:30,560 --> 00:07:36,680
Instead, we are actually going to create a card like format to display the expenses across different

115
00:07:36,680 --> 00:07:37,250
days.

116
00:07:37,610 --> 00:07:39,910
So let's learn how to do that in the next one.

