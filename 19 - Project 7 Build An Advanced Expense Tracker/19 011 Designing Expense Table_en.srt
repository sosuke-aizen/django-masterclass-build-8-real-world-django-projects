1
00:00:00,090 --> 00:00:05,490
Now, let's actually go ahead and let's display all of this expense information in a card like or a

2
00:00:05,490 --> 00:00:06,350
tabular format.

3
00:00:06,360 --> 00:00:07,800
So I'll go right up over here.

4
00:00:07,800 --> 00:00:12,780
And for now, we are going to leave this code as it is, and we actually going to go ahead and create

5
00:00:12,780 --> 00:00:16,740
a div, which is going to act as a container for the table which we are creating.

6
00:00:16,920 --> 00:00:20,690
So inside this particular div right here, we are going to create another div.

7
00:00:20,700 --> 00:00:25,220
So the first div which we are going to be creating here is going to be the header.

8
00:00:25,230 --> 00:00:30,720
So in the header we want to display a heading like expense name, the expense amount, the category

9
00:00:30,720 --> 00:00:31,330
and date.

10
00:00:31,350 --> 00:00:36,540
So for creating that header, I would add another different side of this div and I would call this thing

11
00:00:36,540 --> 00:00:37,430
as expense header.

12
00:00:37,440 --> 00:00:40,290
So I would say expense header.

13
00:00:41,610 --> 00:00:47,400
Okay, so now for the outermost div, which is this div, I'll give a shadow so that we could identify

14
00:00:47,640 --> 00:00:49,360
the boundaries of that particular div.

15
00:00:49,380 --> 00:00:53,070
So I would say shadow dash lg.

16
00:00:53,190 --> 00:00:56,670
If I go back and out here, let's see, what do we get?

17
00:00:57,090 --> 00:01:01,510
So we don't get anything up over here as of now because this does not have any content.

18
00:01:01,560 --> 00:01:04,950
So for now, let's simply add some content in there.

19
00:01:05,099 --> 00:01:11,700
So right inside this div as this is the expense header, I would add span tags and I would add the headers

20
00:01:11,700 --> 00:01:13,800
as name, category, date and amount.

21
00:01:13,800 --> 00:01:18,720
So name this is going to be, let's see, amount category.

22
00:01:18,720 --> 00:01:22,050
And then finally, let's say we have something like data.

23
00:01:22,080 --> 00:01:27,450
So now once I have this, I hit refresh and as you are able to see now we have that container with a

24
00:01:27,450 --> 00:01:29,700
shadow with the header displayed.

25
00:01:29,760 --> 00:01:32,490
So now, as you can see, it's kind of stuck to the left.

26
00:01:32,490 --> 00:01:34,260
So let's add some margin here.

27
00:01:34,470 --> 00:01:40,950
So in order to add up some margin, I would add the margin not to this div but inside to the outermost,

28
00:01:41,040 --> 00:01:41,970
which we have.

29
00:01:42,120 --> 00:01:48,440
So I would say I want the margin of ten from all the sites and also I want the container to be rounded,

30
00:01:48,450 --> 00:01:50,280
so I would say rounded LG.

31
00:01:50,670 --> 00:01:52,290
And we should be good to go.

32
00:01:52,290 --> 00:01:57,450
So if I had to refresh, as you can see, we now have a rounded sort of container and it also has some

33
00:01:57,450 --> 00:01:59,130
margin from all the sides as well.

34
00:01:59,160 --> 00:01:59,520
Okay.

35
00:01:59,520 --> 00:02:04,470
So now once we have this, let's work on putting up these expenses inside of this one.

36
00:02:04,650 --> 00:02:10,860
So right now, just as we have the expense header, let's create an expense table here right after this

37
00:02:10,860 --> 00:02:11,160
one.

38
00:02:11,160 --> 00:02:16,440
So for that, first of all, as in that particular table, we have to display all of those expenses.

39
00:02:16,560 --> 00:02:23,160
So right after we have this div over here, I will create another div up over here and this step is

40
00:02:23,160 --> 00:02:26,000
actually going to be called as expense table.

41
00:02:26,010 --> 00:02:33,240
So here I would say class expense or rather let's call it as expense row, because this is going to

42
00:02:33,240 --> 00:02:34,110
be a row.

43
00:02:34,470 --> 00:02:37,230
And each row is going to contain one expense.

44
00:02:37,470 --> 00:02:45,030
So here, as we want to display multiple expenses, I'll get the four from here, paste it up over here,

45
00:02:46,080 --> 00:02:49,490
take the and from from here and paste it up after this div.

46
00:02:49,500 --> 00:02:54,930
So that means now we get as many number of rows as many expenses, which we actually have.

47
00:02:55,260 --> 00:03:00,810
So here, if I had to refresh, as you can see, the expenses from here disappeared and now I have to

48
00:03:00,810 --> 00:03:03,410
please these expense categories in there.

49
00:03:03,420 --> 00:03:06,890
So let's also create span tags in there to actually place them.

50
00:03:06,900 --> 00:03:13,800
So I would say span, let's assign a class of font bold to this one because we want to display that

51
00:03:13,800 --> 00:03:15,060
information in bold.

52
00:03:15,270 --> 00:03:19,440
And now let's simply cut this expense dot name and paste it up over here.

53
00:03:20,130 --> 00:03:21,450
So now let's go back.

54
00:03:21,450 --> 00:03:26,760
And as you can see now, we have the name like iMac iPhone, which is the name of the product, which

55
00:03:26,760 --> 00:03:28,020
is what exactly we want.

56
00:03:28,050 --> 00:03:31,050
Now let's also get the amount category and date as well.

57
00:03:31,470 --> 00:03:35,670
So I'll copy this paste over here a couple times.

58
00:03:35,700 --> 00:03:40,740
Let's change this thing to amount, change this thing to date.

59
00:03:41,570 --> 00:03:43,850
Change this thing to category.

60
00:03:44,870 --> 00:03:46,130
If I hit refresh.

61
00:03:46,130 --> 00:03:50,150
As you can see now, we also have those displayed up over here as well.

62
00:03:50,420 --> 00:03:53,030
Now, you'll be able to see that in the header.

63
00:03:53,300 --> 00:03:58,700
These names are kind of stuck to each other and we want to have some amount of space in between them.

64
00:03:58,880 --> 00:04:05,060
So in order to add space, there's one thing that could be done, and that is if you take a look at

65
00:04:05,060 --> 00:04:10,340
the div here, which is the expense header, I could actually make this thing flex and I could make

66
00:04:10,340 --> 00:04:12,140
this thing flex wrap.

67
00:04:12,590 --> 00:04:17,480
If I do this now, there's absolutely no space in between them.

68
00:04:17,480 --> 00:04:22,780
But then whenever you declare a particular div into a flex, we need to have certain space in there.

69
00:04:22,790 --> 00:04:25,870
So here I would type in space on the x axis.

70
00:04:25,880 --> 00:04:27,780
Let's say I want it to be 40.

71
00:04:27,800 --> 00:04:34,250
And now if I go back here to refresh, as you can see, these are now kind of spread out and that's

72
00:04:34,250 --> 00:04:38,360
the same exact thing which we need to do in this case as well for the expense row.

73
00:04:38,570 --> 00:04:45,530
So here I would say I want this to be flex wrap and I want the padding on the x axis to be, let's say,

74
00:04:45,530 --> 00:04:48,830
20 and padding on the Y axis to be, let's say five.

75
00:04:49,310 --> 00:04:53,150
So if I had to refresh, this is what we are able to get now.

76
00:04:53,150 --> 00:04:55,940
Still, the data up over here is not formatted well.

77
00:04:55,940 --> 00:05:01,910
And that's because as we want to display or we want to create a table here, we now need to go ahead

78
00:05:01,910 --> 00:05:07,790
and assign some custom CSS classes to this, which actually overrides the basic tailbone classes, which

79
00:05:07,790 --> 00:05:08,750
we already have.

80
00:05:09,200 --> 00:05:15,380
So in order to add our own custom CSS styling to this one, I'll create another file in here inside

81
00:05:15,380 --> 00:05:18,800
the static directory and I will call it a style dot CSS.

82
00:05:19,190 --> 00:05:24,890
And now I want to say that this style of CSS actually overwrites the styling which is present and styles

83
00:05:24,890 --> 00:05:25,880
dot CSS.

84
00:05:26,180 --> 00:05:29,960
And in order to do that, I would go inside the base dot HTML.

85
00:05:30,320 --> 00:05:36,680
And in here, just as we have associated this stylesheet after this one, I would now associate a new

86
00:05:36,680 --> 00:05:42,650
stylesheet here, so I'll just copy paste this and just change this thing to style dot CSS.

87
00:05:42,650 --> 00:05:48,410
So that means whatever CSS classes which we actually apply here, those particular scissors classes

88
00:05:48,410 --> 00:05:50,690
are going to be applied to the table, which we have.

89
00:05:51,050 --> 00:05:58,100
So in here what we want to do is for this particular expense rule, which we have, and for this particular

90
00:05:58,100 --> 00:06:02,570
expense header, which we have, we want to set the display type of them to be table.

91
00:06:02,570 --> 00:06:09,020
So here I would say get the development and from the div I actually want to get the element which is

92
00:06:09,020 --> 00:06:12,860
nothing but the expense header and the expense row.

93
00:06:13,610 --> 00:06:23,600
So I would go back here, I would say div dot expense header as well as get the div dot expense row.

94
00:06:24,710 --> 00:06:28,730
I want to get them both, and then I want to apply a certain kind of styling to it.

95
00:06:28,730 --> 00:06:32,270
So I want to say that I want to set the display type of this thing to table.

96
00:06:32,840 --> 00:06:39,200
I want to set the width of this thing to 100%, and I want to add a table layout and I want to set the

97
00:06:39,200 --> 00:06:40,790
table layout to be fixed.

98
00:06:41,150 --> 00:06:46,020
So once this thing is done, if I had refresh now, you won't notice any change here.

99
00:06:46,040 --> 00:06:51,530
That's because we also have to individually go ahead and style up these span tags as well.

100
00:06:51,800 --> 00:06:58,490
So these span tags which we have up over here inside the expense header and the expense row, they could

101
00:06:58,490 --> 00:07:00,640
be styled by making use of CSS.

102
00:07:00,650 --> 00:07:04,820
So let's go back here and I will simply copy this.

103
00:07:06,160 --> 00:07:07,540
Paste it up over here.

104
00:07:07,540 --> 00:07:12,580
And as we want to style these pan tags which are present in the expense header, I just need to say

105
00:07:12,580 --> 00:07:19,180
span here and if I want to style up these spans inside the expense row, I simply add a span over here.

106
00:07:19,690 --> 00:07:26,290
And now this thing is not a table, but instead the spans which we have, they are the cells of the

107
00:07:26,290 --> 00:07:26,820
table.

108
00:07:26,830 --> 00:07:28,840
So I would say table cell.

109
00:07:29,440 --> 00:07:31,330
So let's delete the rest of the things.

110
00:07:31,330 --> 00:07:32,740
Go back and refresh.

111
00:07:33,010 --> 00:07:36,880
Now, as you can see, they act as a cell and now they are aligned properly.

112
00:07:37,030 --> 00:07:39,880
And also we want to align them to the center.

113
00:07:39,910 --> 00:07:42,430
So I would also say text align as center.

114
00:07:43,090 --> 00:07:47,310
Go back here, hit refresh, and now the text is kind of aligned to the center.

115
00:07:47,320 --> 00:07:53,080
Now, along with this, for every expense which we have, we also want to add a button here to edit

116
00:07:53,080 --> 00:07:55,420
and delete the expenses, which we have.

117
00:07:55,990 --> 00:07:59,020
Therefore, let's go ahead and add them up over here as well.

118
00:07:59,290 --> 00:08:04,810
So for now, we are going to treat them as simple buttons or simple links for the time being, but later

119
00:08:04,810 --> 00:08:08,110
we are going to modify them into fully fledged forms.

120
00:08:08,110 --> 00:08:13,660
So just as we have these pants here, I'll copy this, paste it up over here, and instead of saying

121
00:08:13,900 --> 00:08:18,970
expense dot date, I'll actually replace it with an active tag, which is going to say edit.

122
00:08:19,300 --> 00:08:21,790
And the same is the case with this one as well.

123
00:08:21,820 --> 00:08:24,970
I will replace it with let's see, delete.

124
00:08:25,360 --> 00:08:27,370
Go back here, hit refresh.

125
00:08:27,760 --> 00:08:32,020
Now we have edit and delete here and let's also change the headers as well.

126
00:08:32,200 --> 00:08:35,679
So here I would say edit and here I would say delete.

127
00:08:35,770 --> 00:08:38,140
Let's actually correct the typo here.

128
00:08:38,350 --> 00:08:39,909
Go back, hit refresh.

129
00:08:39,909 --> 00:08:43,030
And as you can see, this is what the table looks like as of now.

130
00:08:43,030 --> 00:08:45,820
And the date and the category are kind of misaligned.

131
00:08:45,820 --> 00:08:47,470
So let's fix that as well.

132
00:08:51,030 --> 00:08:55,140
Okay, so now this seems correct, but still it kind of looks misaligned.

133
00:08:55,170 --> 00:08:56,580
We can fix that later.

134
00:08:56,580 --> 00:08:59,580
But for now, let's go ahead and let's do a couple of things here.

135
00:08:59,580 --> 00:09:05,010
So the very first thing which I need to do is, uh, in order to distinguish between each row, I would

136
00:09:05,010 --> 00:09:06,800
add a horizontal line in there.

137
00:09:06,810 --> 00:09:12,510
So we want to add a horizontal line at the top so as to differentiate between the header as well as

138
00:09:12,510 --> 00:09:12,980
the rows.

139
00:09:12,990 --> 00:09:15,290
So I'll add an air tag here.

140
00:09:15,300 --> 00:09:20,010
And after adding the air tag, if I go back now we have a horizontal line.

141
00:09:20,460 --> 00:09:23,670
Let's associate some sort of a margin for that one.

142
00:09:23,670 --> 00:09:29,070
So I would say air class, let's say margin to be ten.

143
00:09:29,930 --> 00:09:30,070
Okay.

144
00:09:30,200 --> 00:09:35,450
So as you can see now, this table looks pretty much ready and we have the expenses over here as well.

145
00:09:35,510 --> 00:09:39,020
So now let's try adding another expense like iPad.

146
00:09:40,210 --> 00:09:42,160
Let's see, it's 500 bucks.

147
00:09:42,190 --> 00:09:44,650
Category is, let's say, personal.

148
00:09:45,310 --> 00:09:49,950
If I add that, as you can see now, iPad would be added up over here without any issues.

149
00:09:49,960 --> 00:09:55,540
So now the table looks pretty much ready, but we still have this edit and delete buttons which do not

150
00:09:55,540 --> 00:09:56,560
look like a button.

151
00:09:56,560 --> 00:10:01,810
So in the next lecture, let's fix that and let's actually turn them into image buttons so as to make

152
00:10:01,810 --> 00:10:03,160
them look more appropriate.

153
00:10:03,160 --> 00:10:07,220
So thank you very much for watching and I'll see you guys in the next one.

154
00:10:07,240 --> 00:10:08,020
Thank you.

