1
00:00:00,480 --> 00:00:01,800
In this particular lecture.

2
00:00:01,800 --> 00:00:07,890
Let's go ahead and let's make use of charges to visually represent this data which we have inside our

3
00:00:07,890 --> 00:00:08,430
table.

4
00:00:08,640 --> 00:00:14,790
So in order to draw charts here on our Web page, we will be making use of a library called us charges.

5
00:00:14,940 --> 00:00:19,880
So simply go to Google and search for charges and click on the first link, which pops up.

6
00:00:19,890 --> 00:00:22,740
And this is going to redirect you to this page.

7
00:00:22,740 --> 00:00:25,230
And here we are interested in documentation.

8
00:00:25,230 --> 00:00:26,760
So let's go inside there.

9
00:00:27,120 --> 00:00:30,090
And in here there are a couple of versions of charges.

10
00:00:30,090 --> 00:00:32,430
So the charges two, three and four.

11
00:00:32,490 --> 00:00:37,410
So two is a lot older and four is quite a bit new.

12
00:00:37,410 --> 00:00:40,650
So we will be sticking with version three as of now.

13
00:00:40,650 --> 00:00:45,060
So in here for the documentation version, simply select the version, which is this one.

14
00:00:45,450 --> 00:00:50,990
And now here it's going to give you the code which you could use to set up charges on your website.

15
00:00:51,000 --> 00:00:56,460
So in here, as you can see, you could either install charges from NPM or you could use the CD.

16
00:00:56,670 --> 00:01:01,200
So for now we will be using CD N, So let's click on the charges CD and link.

17
00:01:01,200 --> 00:01:06,630
So in here, while selecting the charge CD and version, make sure that you make use of the version,

18
00:01:06,630 --> 00:01:07,890
which is the current version.

19
00:01:07,890 --> 00:01:15,240
So the current version, as we all know, it's actually version 3.0.2, which we have selected.

20
00:01:15,240 --> 00:01:21,630
So make sure that you select the same thing over here, just 3.0..

21
00:01:22,580 --> 00:01:23,140
Two.

22
00:01:24,110 --> 00:01:28,520
And then let's simply copy this, which is copy HTML.

23
00:01:29,090 --> 00:01:35,510
I could go back here to VTS code, and as we want to use this in indexed HTML, I will load that particular

24
00:01:35,510 --> 00:01:37,130
script tag up over here.

25
00:01:38,040 --> 00:01:40,780
And this script tag is going to be four charges.

26
00:01:40,800 --> 00:01:45,750
And again, in order to implement charts, we are going to add a script tag here, which is going to

27
00:01:45,750 --> 00:01:49,200
contain the JavaScript code, which is going to control our chart.

28
00:01:49,230 --> 00:01:50,370
So now let's go ahead.

29
00:01:50,370 --> 00:01:53,250
Let's get this code which is going to power up our chart.

30
00:01:53,550 --> 00:01:56,470
So first of all, to draw a chart, we need a canvas.

31
00:01:56,490 --> 00:01:58,830
So let's copy the canvas code from here.

32
00:01:59,550 --> 00:02:06,060
And as we want to display that particular canvas right below this table, what we will do is we'll go

33
00:02:06,060 --> 00:02:10,949
right where the divisions, which is this div right here again, create another div which is going to

34
00:02:10,949 --> 00:02:12,210
be obviously flex.

35
00:02:12,210 --> 00:02:18,690
And inside this again create another div with a class which sees the width is going to be one by two.

36
00:02:19,770 --> 00:02:23,880
Let's add a shadow LG to this to identify the boundaries.

37
00:02:24,480 --> 00:02:28,130
Let's say the margin on the left is ten and let's do the same thing.

38
00:02:28,140 --> 00:02:32,220
Let's add another view which is going to span another half for other chart.

39
00:02:32,550 --> 00:02:39,540
So this is going to be w-1 by two shadow, LG and ML ten and inside this one.

40
00:02:39,600 --> 00:02:45,630
Now, first of all, I would have a heading which would say something like, Oh, let's say this one

41
00:02:45,630 --> 00:02:48,480
needs to calculate the expense spread across categories.

42
00:02:48,480 --> 00:02:54,510
So I would say expense spread across categories.

43
00:02:55,080 --> 00:03:00,780
And then after this H one tag, I would go ahead and add the canvas, which is nothing but our actual

44
00:03:00,780 --> 00:03:01,290
chart.

45
00:03:01,500 --> 00:03:05,230
So we won't set any width or height for this chart, so let's delete that.

46
00:03:05,250 --> 00:03:08,400
Instead, I would add a class and set the margin to ten.

47
00:03:08,910 --> 00:03:11,470
Do the same thing with this H one as well.

48
00:03:11,490 --> 00:03:13,140
That is set the margin to ten.

49
00:03:13,530 --> 00:03:14,070
Okay.

50
00:03:14,520 --> 00:03:20,340
So now once we have this canvas, if I go back to the browser, you shouldn't be able to see anything

51
00:03:20,340 --> 00:03:22,530
here, just a blank canvas.

52
00:03:22,770 --> 00:03:28,140
But now, in order to actually add a chart, you would need this particular code which powers up our

53
00:03:28,140 --> 00:03:28,560
chart.

54
00:03:29,040 --> 00:03:33,900
So let's get this code, which is the JavaScript code, and you will be able to see that this code is

55
00:03:33,900 --> 00:03:35,220
inside the script tag.

56
00:03:35,670 --> 00:03:42,000
So let's go inside the script tag which we have created and kind of paste this code up over here so

57
00:03:42,090 --> 00:03:45,090
we won't be getting into the details of this code as of now.

58
00:03:45,090 --> 00:03:52,250
But just remember that this JavaScript code get access to my chart by making use of the ID of this canvas.

59
00:03:52,260 --> 00:03:58,440
Once it gets the access to that chart, it gets the context, it creates a new chart, passes in that

60
00:03:58,440 --> 00:04:02,760
context over here, and then it sets up the different parameters for that chart.

61
00:04:03,180 --> 00:04:09,720
So now if I go back and hit refresh, as you can see now, we have a chart added up over here and modifying

62
00:04:09,720 --> 00:04:11,940
this particular chart is quite simple.

63
00:04:12,630 --> 00:04:18,180
Now, as we want to display the expenses across all the categories, the type of chart which we want

64
00:04:18,180 --> 00:04:19,470
here is a pie chart.

65
00:04:19,500 --> 00:04:26,380
So in order to change the type of this chart from bar to pie, I would simply say buy here, go back,

66
00:04:26,380 --> 00:04:27,000
hit refresh.

67
00:04:27,000 --> 00:04:29,400
And as you can see now, we have a pie chart.

68
00:04:30,330 --> 00:04:35,100
Now, you'll also be able to see that there are different labels here, like the red, blue, green,

69
00:04:35,100 --> 00:04:35,640
yellow.

70
00:04:35,640 --> 00:04:41,430
And these labels, which we have here, we want to replace them with these categories, which we have,

71
00:04:41,430 --> 00:04:45,520
which is business, food, personal or any other category which we add.

72
00:04:45,540 --> 00:04:52,440
So we basically want to go ahead and kind of change the labels to dynamic one and get the values from

73
00:04:52,440 --> 00:04:52,980
here.

74
00:04:53,070 --> 00:04:59,100
And also the distribution of these is actually dependent on this order.

75
00:04:59,130 --> 00:05:02,770
So, for example, a red value is showing up to be 12.

76
00:05:02,790 --> 00:05:05,520
So if I hover over red, it says 12.

77
00:05:05,730 --> 00:05:07,950
So let's say if I make this 120.

78
00:05:08,900 --> 00:05:11,000
It's going to span a lot more area.

79
00:05:11,390 --> 00:05:16,670
So instead of having this random set of values, we want to get the values from here, which are nothing

80
00:05:16,670 --> 00:05:17,840
but the expenses.

81
00:05:18,440 --> 00:05:24,170
That means now we have to write our very own JavaScript code to get the value from this table and kind

82
00:05:24,170 --> 00:05:26,390
of put them up over here inside this chart.

83
00:05:26,780 --> 00:05:29,600
So let's learn how to do that in the next lecture.

