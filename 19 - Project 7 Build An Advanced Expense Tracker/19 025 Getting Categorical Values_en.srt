1
00:00:00,120 --> 00:00:05,100
In this particular lecture, let's learn how you could go ahead and get the values which are present

2
00:00:05,100 --> 00:00:07,939
here and kind of display them up over here.

3
00:00:07,950 --> 00:00:12,570
So first of all, we will work with getting the values for the labels which we have here.

4
00:00:12,870 --> 00:00:19,500
So in JavaScript, whenever you have to get access to values on the template or on the page, you always

5
00:00:19,500 --> 00:00:26,340
go ahead and inspect a particular element which you want to access and kind of take a look at the name

6
00:00:26,340 --> 00:00:28,950
or ID associated with that particular table.

7
00:00:29,220 --> 00:00:34,860
So this table, which we have up over here, we have not named it anything yet, so let's go ahead and

8
00:00:34,860 --> 00:00:35,820
let's do that.

9
00:00:36,210 --> 00:00:41,060
So I would go back, go inside the tables, which we have just created.

10
00:00:41,070 --> 00:00:46,050
So these are the tables, I guess, which we have created right up over here.

11
00:00:46,050 --> 00:00:49,320
And for this table, we have named this thing as a 30 day table.

12
00:00:49,320 --> 00:00:52,170
And we have also named this one as 30 day table as well.

13
00:00:52,170 --> 00:00:56,160
But in reality, this is categorical table.

14
00:00:56,250 --> 00:00:59,250
Or we could say that this is a category, some table.

15
00:00:59,250 --> 00:01:01,350
So let's replace the name of this with.

16
00:01:02,010 --> 00:01:04,590
Got some table.

17
00:01:04,590 --> 00:01:07,860
And remember that we have named the second table as at some table.

18
00:01:07,860 --> 00:01:10,140
The first one is still named 30 day table.

19
00:01:10,500 --> 00:01:15,030
And once we have named this particular table, now, we will go ahead and try to get access to that

20
00:01:15,030 --> 00:01:17,940
particular table inside JavaScript.

21
00:01:18,060 --> 00:01:25,860
So here, if I go back to the browser, let's inspect this one more time, which is the entire table

22
00:01:25,860 --> 00:01:26,340
here.

23
00:01:26,760 --> 00:01:30,750
And I'm not sure if you are able to view this, so I'll zoom in a little bit.

24
00:01:31,820 --> 00:01:35,960
And let's view this entire table up over here, which we have.

25
00:01:35,990 --> 00:01:37,550
So this is the left table.

26
00:01:38,280 --> 00:01:39,860
The content of that left table.

27
00:01:39,870 --> 00:01:42,210
So let's collab this left one.

28
00:01:42,300 --> 00:01:44,160
Let's take a look at this right one.

29
00:01:44,940 --> 00:01:46,110
If I hit refresh.

30
00:01:46,110 --> 00:01:51,600
As you can see, it says that this table which we have up over here is cat some table, and it kind

31
00:01:51,600 --> 00:01:53,410
of is showing up two dashes here.

32
00:01:53,430 --> 00:01:54,630
I'm not sure why.

33
00:01:54,680 --> 00:01:55,020
Okay.

34
00:01:55,020 --> 00:01:59,680
So now that problem is fixed and now it sees that this is a cut, some table.

35
00:01:59,700 --> 00:02:03,990
So now inside this cut some table, we could open this thing up.

36
00:02:03,990 --> 00:02:07,230
And here you'll be able to see that we have multiple divs here.

37
00:02:07,230 --> 00:02:12,000
So we want to get the cat some table and we want to get all the divs which we have in there.

38
00:02:12,300 --> 00:02:18,150
So in order to get that in JavaScript, I would go back to VTS code, go inside the script stack, which

39
00:02:18,150 --> 00:02:24,230
we have just created and right before where we have this chart logic, I would add my own script here.

40
00:02:24,240 --> 00:02:32,700
So I would say document dot, get element by ID and I want to get the cat some table.

41
00:02:33,910 --> 00:02:37,540
So yeah, I would say cut some table.

42
00:02:38,140 --> 00:02:43,990
And once I have access to this table in there, I want to get the divs which we have in there.

43
00:02:44,170 --> 00:02:46,620
So we have multiple divs in this table.

44
00:02:46,630 --> 00:02:47,950
So here I would say.

45
00:02:49,410 --> 00:02:57,390
Get elements by tag name, because as we want to get the elements, we will specify the tag as live.

46
00:02:57,840 --> 00:03:01,030
And now this is going to give me a whole bunch of list of divs.

47
00:03:01,050 --> 00:03:03,890
So let's save this in a variable.

48
00:03:03,900 --> 00:03:09,120
So I would say const cat, some div equals this.

49
00:03:09,930 --> 00:03:15,270
So now once we have those particular divs, let's actually console.log those divs and see the kind of

50
00:03:15,270 --> 00:03:16,350
result we have in there.

51
00:03:16,380 --> 00:03:21,300
So console.log, let's say I want to console.log the cat some div, which we have.

52
00:03:21,330 --> 00:03:22,710
So I'll go back here.

53
00:03:23,690 --> 00:03:25,940
Go to console, hit refresh.

54
00:03:25,940 --> 00:03:29,450
And as you can see, we have those particular divs in here.

55
00:03:30,290 --> 00:03:36,350
And now out of those divs, if I again go back to the inspector and again inspect this table.

56
00:03:39,940 --> 00:03:43,020
We have the cat some table inside these divs.

57
00:03:43,030 --> 00:03:51,220
We again have a span and then we have a div and again we have one span which actually shows up the category

58
00:03:51,220 --> 00:03:56,590
and then we have another div here which kind of shows up the actual expense which we have.

59
00:03:56,590 --> 00:04:02,410
So we have a div span which contains a business and then we have div span which contains this value

60
00:04:02,440 --> 00:04:02,830
here.

61
00:04:03,010 --> 00:04:08,520
So we want to get these two different values and save them and to do different set of arrays.

62
00:04:08,530 --> 00:04:10,630
So here I would create two brand new arrays.

63
00:04:10,630 --> 00:04:13,090
So the first one is going to be for saving the categories.

64
00:04:13,090 --> 00:04:16,570
So I would say cons cats, which is categories.

65
00:04:16,570 --> 00:04:19,450
So I would say this is going to be an empty array for now.

66
00:04:19,990 --> 00:04:27,250
And now once we have the cat sums, we are going to extract the actual name values like business, personal

67
00:04:27,250 --> 00:04:29,800
and everything from that particular cat, some div.

68
00:04:30,280 --> 00:04:32,760
So that means we have to loop through the cat some div.

69
00:04:32,770 --> 00:04:34,180
So I would add a for loop.

70
00:04:34,180 --> 00:04:42,100
So for I equals zero i less than cat some div length, which means the length of the entire array I

71
00:04:42,100 --> 00:04:42,970
plus plus.

72
00:04:43,600 --> 00:04:49,660
And the very first thing which we have here is that whenever the index value is zero, that is whenever

73
00:04:49,660 --> 00:04:54,160
the index value is even, we are going to get the category values.

74
00:04:54,160 --> 00:04:58,330
And whenever the index values are odd, we are going to get the expense.

75
00:04:58,450 --> 00:04:59,980
So here I'll see if.

76
00:05:01,060 --> 00:05:07,240
I morta, which means that if you take the index value and divide it by two, if this is equal to one,

77
00:05:07,240 --> 00:05:08,980
that means the number is an odd number.

78
00:05:08,980 --> 00:05:15,010
And if the number is odd number, that is going to give us the value for the expense or else if the

79
00:05:15,010 --> 00:05:19,810
number is one, it's actually going to give me this value here, which is the category value.

80
00:05:19,810 --> 00:05:22,560
And the category values have to be stored in cats.

81
00:05:22,570 --> 00:05:25,150
So here I would say cats don't push.

82
00:05:25,960 --> 00:05:33,550
And in order to get the value from this div, I would say cat some div, I want to get the zeroth element

83
00:05:33,550 --> 00:05:39,550
in this case and therefore I will specify I here because the initial value of I would be zero and this

84
00:05:39,550 --> 00:05:41,050
is actually going to give me.

85
00:05:41,950 --> 00:05:45,280
The span time which I have here, which is the entire spine tag.

86
00:05:45,280 --> 00:05:50,110
But instead I want the inner value, which is food or let's say, business in this case.

87
00:05:50,110 --> 00:05:56,140
So here I would say not inner text because we are interested in the text which is inside of it.

88
00:05:56,800 --> 00:06:00,670
Now, once we have this, this value gets saved into cats.

89
00:06:01,120 --> 00:06:07,450
Now, to test this, you could also console.log cats and see the kind of result you would get so console.log

90
00:06:07,480 --> 00:06:08,320
cats.

91
00:06:09,330 --> 00:06:12,450
If I go back, go to the console, hit refresh.

92
00:06:12,630 --> 00:06:16,560
As you can see, we get the categories as business, food and personal.

93
00:06:17,220 --> 00:06:22,050
Now, once we have these categories saved in there, we could finally pass those categories to this

94
00:06:22,050 --> 00:06:24,180
particular chart, which we have.

95
00:06:24,750 --> 00:06:30,570
So parsing that is actually quite simple in here for the labels which we have.

96
00:06:30,930 --> 00:06:37,170
Instead of seeing labels, I could simply go ahead and replace this entirely with array, which is cats.

97
00:06:37,410 --> 00:06:39,900
So if I do that, it refresh.

98
00:06:40,230 --> 00:06:45,300
Now we have business, food and personal, and the values are not filled up properly here because we

99
00:06:45,300 --> 00:06:47,930
have not assigned the other values for the data.

100
00:06:47,940 --> 00:06:49,560
But we are going to do that soon.

101
00:06:49,560 --> 00:06:51,480
But now we have the categories here.

102
00:06:52,200 --> 00:06:57,030
Now, just as we have gotten access to the categories in the next lecture, let's also get access to

103
00:06:57,030 --> 00:06:57,990
the data here.

104
00:06:58,320 --> 00:07:00,630
And getting access to the data is quite simple.

105
00:07:00,630 --> 00:07:05,840
We just have to go ahead and instead of getting the even values, we get the odd values here.

106
00:07:05,850 --> 00:07:07,830
So let's do that in the next lecture.

