1
00:00:00,090 --> 00:00:06,090
Now let's work on this delete functionality so that we could actually delete any expense which we want.

2
00:00:06,420 --> 00:00:11,400
So the delete functionality is going to be slightly different from the edit one, because in the edit

3
00:00:11,400 --> 00:00:17,250
one you click on this edit form, you are going to be redirected to a new template and that template

4
00:00:17,250 --> 00:00:22,110
is actually going to create a post request to post this data into the database.

5
00:00:22,140 --> 00:00:27,600
However, in case of delete, we want the delete functionality to be in a way that whenever you click

6
00:00:27,600 --> 00:00:32,700
a delete button, the post request should be submitted then and there itself and the data should be

7
00:00:32,700 --> 00:00:34,000
directly gone from here.

8
00:00:34,020 --> 00:00:38,100
So we do not want any kind of an extra template or something like that over here.

9
00:00:38,520 --> 00:00:43,470
So in order to implement this particular delete functionality, first of all, you have to create a

10
00:00:43,470 --> 00:00:50,670
view, create a URL pattern, and then somehow associate that particular URL with the front end, which

11
00:00:50,670 --> 00:00:51,580
you have up over here.

12
00:00:51,600 --> 00:00:53,790
So let's first work on creating that view.

13
00:00:53,970 --> 00:00:55,550
So let's go back to Vsco.

14
00:00:55,560 --> 00:00:58,590
Let's go to view start P VI and create a view for delete.

15
00:00:59,160 --> 00:01:00,840
So I would say def delete.

16
00:01:00,870 --> 00:01:06,210
Now obviously whenever you are deleting an expense, you need a request as well as the idea of the item

17
00:01:06,210 --> 00:01:07,350
which you want to delete.

18
00:01:07,860 --> 00:01:10,410
And deleting logic is quite simple.

19
00:01:10,410 --> 00:01:11,730
You just take the expense.

20
00:01:11,730 --> 00:01:15,600
Find out that expense from the ID and just say expense to delete.

21
00:01:15,690 --> 00:01:19,080
So here I would say if request start method.

22
00:01:19,080 --> 00:01:24,960
So whenever we have a post request in that particular case, let's simply go ahead, take the expense

23
00:01:24,960 --> 00:01:26,080
with an ID as ID.

24
00:01:26,100 --> 00:01:33,570
So I would say expense equals expense dot objects, dot get and get an expense with an ID where the

25
00:01:33,570 --> 00:01:38,640
ID is going to be equal to ID and then I simply want to delete that expense.

26
00:01:38,640 --> 00:01:40,200
So expense dot delete.

27
00:01:40,930 --> 00:01:41,500
That's it.

28
00:01:41,500 --> 00:01:43,780
That's how simple the deletion logic is.

29
00:01:44,020 --> 00:01:49,810
And once the deletion is complete, let's say I want to return redirects to a specific URL, which is

30
00:01:49,810 --> 00:01:51,400
index, which is the home page.

31
00:01:52,270 --> 00:01:53,340
Which is all fine and good.

32
00:01:53,350 --> 00:01:57,970
So once we have this view created, let's associate it with the URL pattern.

33
00:01:58,270 --> 00:02:03,850
So the URL pattern for the delete is going to be exactly same as the one which we have for added.

34
00:02:03,940 --> 00:02:09,460
So I would simply copy the same thing, paste it up over here and change this thing.

35
00:02:09,460 --> 00:02:13,120
To delete the ID is going to be the same thing.

36
00:02:13,330 --> 00:02:17,200
Change the view to delete and let's change the name to delete as well.

37
00:02:17,590 --> 00:02:20,250
Once this thing is done, we are pretty much good to go.

38
00:02:20,260 --> 00:02:27,850
So now what happens is whenever I go to some URL like let's say forward, slash, delete, forward slash

39
00:02:27,850 --> 00:02:29,440
one, let's see what happens.

40
00:02:29,590 --> 00:02:32,560
So if I go over, then nothing's happening here.

41
00:02:33,250 --> 00:02:38,470
And that's because we are making a great request and we should make a post request in order to be able

42
00:02:38,470 --> 00:02:39,670
to delete this one.

43
00:02:40,000 --> 00:02:45,430
So here I would go inside the next HTML and for the delete link, which we have here.

44
00:02:45,940 --> 00:02:48,040
I simply want to make a post request.

45
00:02:48,040 --> 00:02:51,310
So that means I cannot have an ref tag here.

46
00:02:51,400 --> 00:02:56,230
Instead, I need to have a fully fledged submit button or a form here.

47
00:02:56,740 --> 00:03:02,980
So here, instead of having a link inside of this one, I would create a fully fledged form in the inside

48
00:03:02,980 --> 00:03:03,780
the span tag.

49
00:03:03,790 --> 00:03:05,200
So I would create a form.

50
00:03:05,620 --> 00:03:09,400
And then I want to say that I want to make a post request.

51
00:03:09,400 --> 00:03:11,320
So method is going to be post.

52
00:03:11,710 --> 00:03:17,350
However, along with making this post request, we also want to go to the URL as well.

53
00:03:17,350 --> 00:03:19,420
So there are two things which we are doing here.

54
00:03:19,750 --> 00:03:21,730
First is we are visiting a URL.

55
00:03:21,760 --> 00:03:26,540
For that we need an active tag and we also want to make a post request on that URL.

56
00:03:26,560 --> 00:03:31,400
That means we need a method as post, but we cannot include an h ref tag here.

57
00:03:31,420 --> 00:03:36,010
Therefore, we are going to specify the link in the action or the form action.

58
00:03:36,250 --> 00:03:40,810
So in here I want to say that I want to use the URL which is delete.

59
00:03:40,810 --> 00:03:47,470
So here, just as we have the URL for edit, I would add a URL for delete and this is also going to

60
00:03:47,470 --> 00:03:50,860
get the expense ID for the expense which we want to delete.

61
00:03:50,890 --> 00:03:53,830
Therefore, I'm going to say expense and dot ID.

62
00:03:55,140 --> 00:03:59,760
And as this is a form, a form always needs to have x rf token as well.

63
00:04:00,060 --> 00:04:06,930
So I would see CSS, RF underscore token, and then inside this form we are going to create a button

64
00:04:06,930 --> 00:04:09,050
which is going to be a actual delete button.

65
00:04:09,060 --> 00:04:11,430
So the type of this thing is going to be.

66
00:04:12,580 --> 00:04:13,300
Submit.

67
00:04:13,300 --> 00:04:15,940
And here we are going to add an image later.

68
00:04:16,180 --> 00:04:19,450
But for now, I'm just going to see delete over here.

69
00:04:21,100 --> 00:04:25,150
Now, there's one problem, though, with this approach, and you will soon be able to identify that

70
00:04:25,150 --> 00:04:28,060
problem as soon as we try deleting something.

71
00:04:28,060 --> 00:04:30,160
So let's first fix the typo here.

72
00:04:30,160 --> 00:04:31,390
This should be delete.

73
00:04:31,660 --> 00:04:33,990
And now let's see if this actually works.

74
00:04:34,000 --> 00:04:35,320
So I'll go back here.

75
00:04:36,250 --> 00:04:40,160
First of all, visit this URL and now let's try deleting something.

76
00:04:40,180 --> 00:04:45,880
Now it's being deleted here, but the problem is that this delete functionality is working.

77
00:04:45,880 --> 00:04:52,970
But now on the same page, which is this page we have to post request.

78
00:04:52,990 --> 00:04:57,700
So we have a post request for this form and we also have a post request for delete as well.

79
00:04:58,330 --> 00:05:04,960
Now inside our views we want to explicitly distinguish between them because if the request method is

80
00:05:04,960 --> 00:05:06,790
post, then this is actually happening.

81
00:05:06,790 --> 00:05:12,460
And if the request method is post in this case, then we are making a submit request and to actually

82
00:05:12,460 --> 00:05:18,010
identify them inside the view itself or in order to avoid any kind of bugs.

83
00:05:18,010 --> 00:05:19,720
There's one thing which we could do.

84
00:05:20,490 --> 00:05:26,910
You could go to this index form right here and you could associate some sort of a name to the form which

85
00:05:26,910 --> 00:05:27,810
you are submitting.

86
00:05:28,080 --> 00:05:35,160
So right now, if I go to the index dot HTML, I could go inside this button here and I could see that

87
00:05:35,160 --> 00:05:37,920
the name is going to be delete.

88
00:05:39,330 --> 00:05:44,520
And I could actually make a check if the request which is coming or the post request which is coming,

89
00:05:44,520 --> 00:05:49,860
if that's actually coming from the delete button by just going inside the view.

90
00:05:50,160 --> 00:05:54,330
And I would say if request method is post and.

91
00:05:55,030 --> 00:06:00,160
If we have delete in the request dot post.

92
00:06:01,000 --> 00:06:05,860
So what this does is that it also checks for the delete keyword in the request and only if we have the

93
00:06:05,860 --> 00:06:09,530
delete keyword in the request, it's going to go ahead and execute this code.

94
00:06:09,580 --> 00:06:12,880
And right now we do have named this button as delete.

95
00:06:12,940 --> 00:06:15,700
That means our request method actually contains delete.

96
00:06:16,150 --> 00:06:18,840
So let's hit refresh and let's see if that's working.

97
00:06:18,850 --> 00:06:23,380
So as you can see, it's working absolutely fine now, and this should be working as well.

98
00:06:23,530 --> 00:06:31,210
So I could type an iPad, I could type in some amount, I could add a category, add that and this is

99
00:06:31,210 --> 00:06:32,170
working as well.

100
00:06:32,650 --> 00:06:35,860
So now the delete functionality is working absolutely fine.

101
00:06:35,860 --> 00:06:37,100
And in the next lecture.

102
00:06:37,120 --> 00:06:42,410
Now, let's go ahead and let's add some image icons to these particular links and buttons which we have

103
00:06:42,410 --> 00:06:45,760
up over here so as to improve the look and feel of our website.

104
00:06:45,940 --> 00:06:47,830
So let's do that in the next one.

