1
00:00:00,090 --> 00:00:04,590
In this particular lecture, let's learn how we could set up tailwind for our Django project.

2
00:00:04,590 --> 00:00:10,290
So whenever you have to set up tailwind for your Django project, you first have to go ahead and download

3
00:00:10,290 --> 00:00:12,740
Node.js and install it on your machine.

4
00:00:12,750 --> 00:00:19,590
And the reason why you need Node.js is because typically for installing HTML when we need NPM, which

5
00:00:19,590 --> 00:00:20,820
is Node package manager.

6
00:00:20,820 --> 00:00:26,310
So I already have node install on my machine, so I'm not going to repeat the same steps one more time,

7
00:00:26,310 --> 00:00:31,800
but if you do not have node install then I would highly recommend that you go ahead and install Node.js.

8
00:00:31,800 --> 00:00:33,330
The setup is quite simple.

9
00:00:33,330 --> 00:00:39,060
You just have to visit Node GSIS official website and download the long term supported version and you

10
00:00:39,060 --> 00:00:40,080
should be good to go.

11
00:00:40,260 --> 00:00:45,630
So once a node is installed, you then have to go ahead, go into the terminal and you have to first

12
00:00:45,630 --> 00:00:51,000
stop the server and then you need to go into the directory, which is nothing but the current project

13
00:00:51,000 --> 00:00:53,150
directory, which is in this case my site.

14
00:00:53,160 --> 00:00:58,290
So we are already in there, but still to demonstrate in a much more better way, I'll actually go back

15
00:00:58,290 --> 00:01:03,150
to the desktop and I'll show you how to navigate from there to the main project directory.

16
00:01:03,330 --> 00:01:05,310
So right now we are enter the desktop.

17
00:01:05,310 --> 00:01:10,680
So first of all, you have to go into the expense tracker and the expense tracker is actually the container

18
00:01:10,680 --> 00:01:14,580
which actually contains the virtual environment as well as the Django project.

19
00:01:14,580 --> 00:01:20,760
But in there we actually have to go inside our actual project directory, which is in this case my site.

20
00:01:20,760 --> 00:01:27,510
So I would type in CD my site and now once we enter this, my site directory here, first of all, we

21
00:01:27,510 --> 00:01:30,170
actually need to set up a package start JSON file.

22
00:01:30,180 --> 00:01:35,400
Now, this package, a JSON file is going to contain all the JavaScript dependencies, which our project

23
00:01:35,430 --> 00:01:36,030
uses.

24
00:01:36,180 --> 00:01:40,860
So in order to create a package JSON file here, you need to say NPM.

25
00:01:43,100 --> 00:01:43,940
In it.

26
00:01:43,970 --> 00:01:44,770
That's why.

27
00:01:44,780 --> 00:01:47,900
And you need to do this even before you start installing tailwind.

28
00:01:47,990 --> 00:01:49,340
So hit enter.

29
00:01:49,340 --> 00:01:52,730
And that's actually going to create a package JSON file for you.

30
00:01:52,880 --> 00:01:56,720
So if you open up VTS code, you should be able to see that inside my site.

31
00:01:56,750 --> 00:01:59,000
Now we have this package JSON file.

32
00:01:59,780 --> 00:02:03,950
Now once we have this file, we could finally go ahead and install Telvin.

33
00:02:04,040 --> 00:02:07,730
So here, in order to install Telvin, you need to say NPM install.

34
00:02:08,590 --> 00:02:10,090
Tailwind CSS.

35
00:02:10,660 --> 00:02:15,160
And here you need to mention the Virgin as 2.2.16.

36
00:02:15,550 --> 00:02:20,260
Now, there might be latest tailwind versions out there, but I would highly recommend that you stick

37
00:02:20,260 --> 00:02:21,280
with this version.

38
00:02:21,280 --> 00:02:26,490
Just because a newer version is available doesn't mean that the older one is rendered useless.

39
00:02:26,500 --> 00:02:32,410
So go ahead and install the exact same version which is specified here or else you might face some issues

40
00:02:32,410 --> 00:02:33,670
while using tailwind.

41
00:02:33,850 --> 00:02:39,580
So once you go ahead and install tailwind, if you go back to the package JSON file, you should be

42
00:02:39,580 --> 00:02:44,750
able to see that tailwind is now added as a dependency here after tailwind has been installed.

43
00:02:44,770 --> 00:02:51,040
We now need to add it to our Shango Project and the way in which we add it is that you first go ahead,

44
00:02:51,040 --> 00:02:57,460
go inside your app, which is my app, and in there you need to create a new folder called the static.

45
00:02:57,790 --> 00:03:02,080
And then static again, create a new folder which is going to be called as my app.

46
00:03:02,440 --> 00:03:09,270
And the reason why we do this is because Tilman works on the basis of a CSS file which is generated

47
00:03:09,280 --> 00:03:14,180
and typically in Django, you always save the CSS files and do static directories.

48
00:03:14,200 --> 00:03:16,720
Therefore, we create a directory structure for static.

49
00:03:17,050 --> 00:03:21,790
And then here, now I'm actually going to create a new file and I'm going to name it as.

50
00:03:22,570 --> 00:03:24,070
Start CSS.

51
00:03:24,400 --> 00:03:29,920
And the reason why I'm naming this file as source is because this file is going to contain the source

52
00:03:29,920 --> 00:03:35,950
code and tailwind is actually going to make use of this file and create an output file, which is going

53
00:03:35,950 --> 00:03:41,140
to be a CSS file, which is going to contain the actual CSS classes, which Tillman uses.

54
00:03:41,320 --> 00:03:46,690
So in this particular file, we are going to add some tailwind directives and these are going to be

55
00:03:46,690 --> 00:03:49,990
then translated or compiled into actual CSS classes.

56
00:03:49,990 --> 00:03:56,080
So I would add an ADD tailwind base add tailwind.

57
00:03:57,090 --> 00:03:58,220
Utilities.

58
00:03:58,230 --> 00:04:02,190
And then finally, let's add a tailwind components.

59
00:04:02,790 --> 00:04:05,700
Once this thing is done, we are pretty much good to go.

60
00:04:05,880 --> 00:04:11,620
So now we need to take this source file and we want to generate an output file out of it using tailwind.

61
00:04:11,640 --> 00:04:14,550
And in order to do that, you have to execute a command.

62
00:04:15,030 --> 00:04:17,850
But you cannot execute that command directly.

63
00:04:17,850 --> 00:04:23,430
Instead, you have to specify that command and the package start JSON file inside this script stack,

64
00:04:23,430 --> 00:04:24,270
which we have.

65
00:04:24,570 --> 00:04:28,500
So in here we will create another command or script called as Build.

66
00:04:28,620 --> 00:04:34,770
And this particular command is going to say something like Tailwind build and we want to build from

67
00:04:34,770 --> 00:04:35,760
the source file.

68
00:04:35,880 --> 00:04:39,150
And the source file is currently present inside the my app.

69
00:04:39,150 --> 00:04:47,190
So I would say my app forward slash static forward slash my app and the name of the file is nothing

70
00:04:47,190 --> 00:04:48,990
but source dots.

71
00:04:48,990 --> 00:04:49,710
Yes.

72
00:04:50,130 --> 00:04:53,120
And then we want to say that we want to generate an output.

73
00:04:53,130 --> 00:04:54,660
Therefore I would type in dash.

74
00:04:54,660 --> 00:05:00,060
Oh, and the output should be generated inside the same directory which we have.

75
00:05:00,060 --> 00:05:03,660
So therefore I will type in my app static.

76
00:05:04,820 --> 00:05:05,600
My app.

77
00:05:05,600 --> 00:05:10,640
And let's see the name of the source file needs to be Styles dot CSS.

78
00:05:10,640 --> 00:05:13,430
So I'll add that up over here and save this.

79
00:05:13,850 --> 00:05:20,240
So now once we have this script written up over here, we actually now have to run the script so that

80
00:05:20,240 --> 00:05:22,100
the output file is generated.

81
00:05:22,130 --> 00:05:29,240
So in order to run that script, I need to go back to the terminal and say, NPM, run, build.

82
00:05:30,260 --> 00:05:30,500
Hit.

83
00:05:30,500 --> 00:05:31,160
Enter.

84
00:05:31,220 --> 00:05:33,650
It's going to take a while for that file to generate.

85
00:05:33,650 --> 00:05:38,120
And as you can see now, the file is generated in around 4 seconds.

86
00:05:38,570 --> 00:05:43,820
So if you take a look at this now, you have a newly created file called a style dot CSS.

87
00:05:43,940 --> 00:05:49,010
And if you actually open it up, as you can see, it contains a whole bunch of tailwind classes, which

88
00:05:49,010 --> 00:05:49,760
we could use.

89
00:05:49,790 --> 00:05:55,420
Now, once this file is generated, now we have to link this particular file to the indexed HTML.

90
00:05:55,430 --> 00:06:00,060
And as soon as we do that, the styling of this particular page is going to change.

91
00:06:00,080 --> 00:06:01,880
So let's do that real quick.

92
00:06:02,180 --> 00:06:06,800
So here I'll say the link rel is going to be stylesheet.

93
00:06:06,830 --> 00:06:10,610
The H ref for this one is going to be static.

94
00:06:10,790 --> 00:06:15,200
My app forward slash styles dot CSS.

95
00:06:15,900 --> 00:06:21,540
And as soon as we do that and if we run the server now, let's see what happens.

96
00:06:23,640 --> 00:06:25,450
So the server is now up and running.

97
00:06:25,470 --> 00:06:27,390
If I go back here, I hit refresh.

98
00:06:28,140 --> 00:06:31,750
Okay, we do get an error because we forgot to load the static tag.

99
00:06:31,770 --> 00:06:38,280
So whenever you're using static here or the static keyword, you always need to load that particular

100
00:06:38,280 --> 00:06:40,020
tag at the top.

101
00:06:40,020 --> 00:06:44,730
So if I say load static, this should fix that error.

102
00:06:44,760 --> 00:06:46,830
So if I go back here, hit refresh.

103
00:06:47,040 --> 00:06:52,170
As you can see now, the styling of this particular web page has changed and now it sees that this is

104
00:06:52,170 --> 00:06:57,390
a template, but you'll be able to see that the font here is different, which means that the tailwind

105
00:06:57,390 --> 00:07:01,310
styling has now been applied to this indexed HTML page.

106
00:07:01,320 --> 00:07:06,690
But now the problem here is that if you create multiple templates here, you would have to include the

107
00:07:06,690 --> 00:07:09,020
same link in all of the files which you have.

108
00:07:09,030 --> 00:07:15,000
And in order to solve that issue, a better way to handle this is that you create a base template and

109
00:07:15,000 --> 00:07:20,610
include this link in the base template and then make the index HTML inherit from that base template.

110
00:07:20,880 --> 00:07:24,360
So let's go ahead and generate the base template in the next lecture.

