1
00:00:00,210 --> 00:00:01,410
In this particular lecture.

2
00:00:01,410 --> 00:00:07,440
Let's go ahead and let's display all of these sums which we have up over here inside the index HTML.

3
00:00:07,440 --> 00:00:10,170
So we have some additional code here which is not required.

4
00:00:10,170 --> 00:00:12,030
So let's first of all delete that.

5
00:00:12,210 --> 00:00:16,560
And then here, right after this event, I'll create another div which is going to be of the type flex.

6
00:00:16,560 --> 00:00:20,970
So I'll create a here and the class of this thing is going to be flex.

7
00:00:20,970 --> 00:00:23,940
And inside this day we are going to create three cards.

8
00:00:24,120 --> 00:00:27,540
So I'll create another div here and this is going to be a card.

9
00:00:27,570 --> 00:00:35,160
So in order to show it up as a card, I would add a shadow LG to this and then I would add some margin

10
00:00:35,160 --> 00:00:36,930
from the left as ten.

11
00:00:37,730 --> 00:00:42,500
And I would make this particular card as round it surrounded the LG.

12
00:00:43,220 --> 00:00:49,040
Now, once we have this div inside this div, we are going to actually have a heading which would say

13
00:00:49,040 --> 00:00:51,230
expense for the last 365 days.

14
00:00:51,230 --> 00:00:54,560
So here I would say last.

15
00:00:55,960 --> 00:00:57,460
365.

16
00:00:58,360 --> 00:01:04,930
DS And then after this, let's actually add a heading tag, which is going to show the actual expenses

17
00:01:04,930 --> 00:01:05,810
in a green color.

18
00:01:05,830 --> 00:01:07,360
So I'd add each one.

19
00:01:07,840 --> 00:01:10,390
I'll add some padding and margin to this.

20
00:01:10,540 --> 00:01:12,670
So let's do that later.

21
00:01:12,670 --> 00:01:14,530
But for now, let's display the sum.

22
00:01:14,920 --> 00:01:20,770
So here, in order to display the yearly sum, the context which we have passed here is a yearly sum.

23
00:01:21,040 --> 00:01:22,780
So let's copy this context.

24
00:01:22,810 --> 00:01:31,570
Go right inside here and inside the template syntax, I would say yearly sum, dot amount, double underscore,

25
00:01:31,600 --> 00:01:37,900
sum, because that's actually the name of the value here inside that object.

26
00:01:38,050 --> 00:01:41,120
And then also, I want to humanize this as well.

27
00:01:41,140 --> 00:01:46,930
So in order to humanize this, I would say I and comma.

28
00:01:48,490 --> 00:01:51,300
And I guess we have added that up over here as well.

29
00:01:51,310 --> 00:01:53,350
But that thing was removed for some reason.

30
00:01:53,350 --> 00:01:56,320
So let's go back here at Refresh.

31
00:01:56,560 --> 00:02:01,510
And as you can see, it stays the expense for the last 365 days where these.

32
00:02:01,840 --> 00:02:04,720
So now this is showing up, that particular expense.

33
00:02:04,750 --> 00:02:07,810
Now, let's go ahead and style this thing up in a little bit better way.

34
00:02:07,840 --> 00:02:15,250
So for this one, as we want to display the expenses for three different periods of time, that is a

35
00:02:15,250 --> 00:02:20,590
year, week and a month, I would actually go ahead and make this particular div span one third of the

36
00:02:20,590 --> 00:02:21,240
entire width.

37
00:02:21,250 --> 00:02:24,300
So I would say w-1 by three.

38
00:02:24,330 --> 00:02:27,460
And now let's style up this particular class, which we have.

39
00:02:27,580 --> 00:02:31,070
So here I would say the margin on the left should be ten.

40
00:02:31,090 --> 00:02:34,780
I want the font to be bold.

41
00:02:36,580 --> 00:02:41,140
I want the text to be gray with the shade of 500.

42
00:02:42,220 --> 00:02:46,560
And for this one right here, I would actually go ahead and add some classes as well.

43
00:02:46,570 --> 00:02:49,060
So I want the margin left to be ten.

44
00:02:49,360 --> 00:02:51,250
I want the margin y to be fi.

45
00:02:51,280 --> 00:02:54,040
I want the text to be two times Excel.

46
00:02:55,470 --> 00:03:02,490
I want the text to be green with a shade of, let's say 500, and I want the font to be bold.

47
00:03:02,940 --> 00:03:05,040
So let's see how this kind of looks like.

48
00:03:05,040 --> 00:03:06,570
So I'll go ahead, hit refresh.

49
00:03:06,570 --> 00:03:09,440
And as you can see, this is what it looks like.

50
00:03:09,450 --> 00:03:13,140
I could add a dollar sign in front of it to make it look even better.

51
00:03:14,300 --> 00:03:18,380
So as you can see now, we have expenses for the last 365 days.

52
00:03:18,650 --> 00:03:23,120
Now, similarly, we could display the expenses for the weeks and months as well.

53
00:03:23,120 --> 00:03:28,190
So I simply need to copy the same which I have pasted up over here.

54
00:03:28,280 --> 00:03:32,510
Replace this with last 30 days instead of saying yearly sum.

55
00:03:32,510 --> 00:03:34,940
I simply need to say monthly sum.

56
00:03:34,970 --> 00:03:36,430
Go back here, hit refresh.

57
00:03:36,440 --> 00:03:40,610
Now we have the sum of expenses for the last 30 days as well.

58
00:03:40,820 --> 00:03:45,280
Now let's finally go ahead and add the expenses for last week as well.

59
00:03:45,290 --> 00:03:52,220
So I again have to copy this, paste it up over here, change it to seven days.

60
00:03:53,260 --> 00:03:55,950
And change it to weekly sum.

61
00:03:55,960 --> 00:03:57,820
Go back here, hit refresh.

62
00:03:58,090 --> 00:04:02,030
And as you can see now, we have the sun for the last seven days as well.

63
00:04:02,050 --> 00:04:03,850
So these are the values which you get.

64
00:04:03,850 --> 00:04:09,340
And currently all the values are showing up to be same because we have made all the expenses in the

65
00:04:09,340 --> 00:04:10,600
past seven days itself.

66
00:04:10,600 --> 00:04:13,830
But if you make a new expense, these values are going to change.

67
00:04:13,840 --> 00:04:20,260
So after displaying these expenses in the next lecture, let's go ahead and let's calculate the expenses

68
00:04:20,260 --> 00:04:21,370
for every day.

69
00:04:21,610 --> 00:04:28,240
And what I mean by that is, so let's say if I make another expense, like let's say salary, let's

70
00:04:28,240 --> 00:04:34,000
say I'm paying a salary of $5,000 and this is obviously going to be a business expense.

71
00:04:34,240 --> 00:04:41,210
And if I make this expense on the same date, as you can see, the expense for today would be $123,000,

72
00:04:41,260 --> 00:04:44,110
plus 5000 would be 128,000.

73
00:04:45,280 --> 00:04:51,700
So I also want to be able to go ahead and display the expenses for every day for the past 30 days.

74
00:04:51,700 --> 00:04:57,850
So it should actually calculate the sum of expenses every day and display it in some sort of a table.

75
00:04:58,390 --> 00:05:04,690
And also, let's say you want to categorize your entire expenses in terms of categories as well, like

76
00:05:04,690 --> 00:05:09,520
how much you have spent on personal, how much you are spent on business, and if you have some other

77
00:05:09,520 --> 00:05:12,000
categories that should also display them as well.

78
00:05:12,010 --> 00:05:18,070
So let's learn how we could calculate the sum across the days as well as categories in the next lecture.

