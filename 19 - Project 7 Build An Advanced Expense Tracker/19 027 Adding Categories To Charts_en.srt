1
00:00:00,090 --> 00:00:05,640
So now let's work on adding a second chart here, which shows the 30 day expenses.

2
00:00:05,640 --> 00:00:10,410
And doing that is quite simple because we just have to use the code which we have used here and just

3
00:00:10,410 --> 00:00:12,870
make a couple of changes in that code itself.

4
00:00:13,230 --> 00:00:16,190
So let's first go ahead and let's create a chart here.

5
00:00:16,200 --> 00:00:18,540
And creating chart here is quite simple.

6
00:00:18,780 --> 00:00:24,060
So if you go take a look at the code here, we have not created any kind of canvas.

7
00:00:24,510 --> 00:00:30,390
So I'll copy the heading as well as the canvas from here, paste it up over here and only make a couple

8
00:00:30,390 --> 00:00:31,260
of changes.

9
00:00:31,620 --> 00:00:42,000
So expense spread across categories, changes to expense for the past 30 days, or I would rather name

10
00:00:42,000 --> 00:00:43,500
it as daily expenses.

11
00:00:43,500 --> 00:00:46,920
So daily expense.

12
00:00:46,950 --> 00:00:48,000
Let's see some.

13
00:00:49,150 --> 00:00:53,860
And now this canvas has an ID my chart and this one has my chart as well.

14
00:00:53,860 --> 00:00:58,720
So if you hit refresh, this isn't going to work because my chart is already there.

15
00:00:58,720 --> 00:01:00,280
So let's change it to my chart.

16
00:01:00,280 --> 00:01:00,730
One.

17
00:01:00,730 --> 00:01:06,610
It's not a good naming convention, but to keep things simple, I'm going to name this as my chart one.

18
00:01:07,000 --> 00:01:13,060
Now, just as we have a context which powers up the first chart, which is Ctcs, we need to create

19
00:01:13,060 --> 00:01:15,670
another context which powers up our second chart.

20
00:01:15,910 --> 00:01:18,100
So that means I have to copy this.

21
00:01:19,380 --> 00:01:22,920
Entire thing pasted up over here within the script tag.

22
00:01:23,460 --> 00:01:25,590
Name this thing as context one.

23
00:01:25,710 --> 00:01:27,450
This gets the chart one.

24
00:01:28,080 --> 00:01:31,740
Let's replace the variable name to match at one.

25
00:01:32,010 --> 00:01:34,080
The rest of the things remain the same.

26
00:01:34,530 --> 00:01:37,530
And now let's head back and see the kind of result it produces.

27
00:01:37,530 --> 00:01:41,670
So if I refresh and it's not showing up over here.

28
00:01:42,030 --> 00:01:45,330
And that's because we need to pass in context one here.

29
00:01:45,930 --> 00:01:51,150
Now, it's kind of showing up over here, but it's taking the data from this one because we are passing

30
00:01:51,150 --> 00:01:54,840
in the label as well as the data as cats and cats.

31
00:01:55,080 --> 00:01:57,300
Instead, we want to add something different here.

32
00:01:57,570 --> 00:02:02,880
But first of all, let's change the type of this chart to a line chart, so I'll change it to line.

33
00:02:03,980 --> 00:02:05,480
This is going to change that.

34
00:02:05,750 --> 00:02:12,680
And now, let's say instead of getting categories here, we want to get the dates which we have up over

35
00:02:12,680 --> 00:02:13,100
here.

36
00:02:13,700 --> 00:02:16,300
Now, the question is, how will you get the date values?

37
00:02:16,310 --> 00:02:22,610
So as the structure of this table and this table was the same, we could make use of the same code which

38
00:02:22,610 --> 00:02:25,430
we had used for getting the values from here.

39
00:02:25,940 --> 00:02:28,070
So for getting the values from here.

40
00:02:28,100 --> 00:02:32,330
First of all, we had created two set of arrays cat and cat sum.

41
00:02:32,480 --> 00:02:38,510
So in a similar fashion as here, we want to get the date and the sum.

42
00:02:38,510 --> 00:02:43,270
I would create two variables cost dates and cost sums.

43
00:02:43,280 --> 00:02:47,540
And our job is to go ahead and get the values just as we have gotten from here.

44
00:02:48,210 --> 00:02:53,400
So again, just as we have created this cat, some dev copy the same thing.

45
00:02:55,330 --> 00:02:59,080
Be set up over here and make a couple of changes.

46
00:02:59,710 --> 00:03:05,560
So instead of saying cat some dev, I will rename it to daily some div.

47
00:03:06,310 --> 00:03:08,980
This is going to get the data from the 30 day table.

48
00:03:08,980 --> 00:03:13,060
So instead of cad sum I would see 30 day table.

49
00:03:13,300 --> 00:03:17,320
We want to get the same elements here and here we are getting those values.

50
00:03:17,950 --> 00:03:20,340
And now let's make changes to this as well.

51
00:03:20,350 --> 00:03:25,690
So instead of saying cut some div, I would replace it with daily some div, and now instead of pushing

52
00:03:25,690 --> 00:03:30,770
up the values into cat sums and cats, I would push those values into dates and sums.

53
00:03:30,790 --> 00:03:32,800
Now the naming is quite confusing.

54
00:03:33,040 --> 00:03:36,910
So here I would see sums and here I would see dates.

55
00:03:36,910 --> 00:03:39,460
And let's also replace this with daily, some div.

56
00:03:39,460 --> 00:03:40,570
Daily some div.

57
00:03:41,430 --> 00:03:43,230
And let's delete this.

58
00:03:43,680 --> 00:03:48,600
And there's a high chance that you would make a mistake here if you miss any one of the names.

59
00:03:48,600 --> 00:03:50,550
So make sure that you are careful there.

60
00:03:50,850 --> 00:03:57,030
And now, once we have this dates and sums, we simply have to pass those values or arrays to the new

61
00:03:57,030 --> 00:03:58,500
chart which we have created.

62
00:03:58,980 --> 00:04:01,650
So the label here is going to be for dates.

63
00:04:02,750 --> 00:04:04,670
And the data here is going to be the sum.

64
00:04:04,670 --> 00:04:05,250
Sorry.

65
00:04:05,270 --> 00:04:09,350
So if I do that, if I go back here at refresh.

66
00:04:10,450 --> 00:04:16,110
As you can see, we get the expense for these two dates.

67
00:04:16,120 --> 00:04:21,040
I'm not sure why it's not able to extract the other dates, but currently it's showing up the expense

68
00:04:21,040 --> 00:04:22,690
for these two dates itself.

69
00:04:22,810 --> 00:04:25,420
So let's see what exactly went wrong here.

70
00:04:26,020 --> 00:04:30,700
So first of all, I would log the values for the dates as well as the sums.

71
00:04:31,500 --> 00:04:33,000
So here I would see.

72
00:04:33,950 --> 00:04:35,450
Console, dot log.

73
00:04:35,990 --> 00:04:40,370
Let's console log the dates and let's console log the.

74
00:04:41,680 --> 00:04:42,730
Sums as well.

75
00:04:42,910 --> 00:04:44,500
So if I had back.

76
00:04:45,780 --> 00:04:47,020
Inspect this.

77
00:04:47,040 --> 00:04:49,650
Open up the console and refresh.

78
00:04:51,110 --> 00:04:58,460
So we get all the dates here, which is 19 November 21st, November 22nd and 23rd.

79
00:04:59,400 --> 00:05:01,580
And we get the some values as well.

80
00:05:01,590 --> 00:05:08,380
So the dates are actually showing up pretty well and the expenses are not being shown up over here.

81
00:05:08,400 --> 00:05:08,770
Okay.

82
00:05:08,790 --> 00:05:15,480
And that's because we have added commas here to these expenses and we need to get rid of those commas.

83
00:05:15,960 --> 00:05:21,900
So in order to get rid of those commas, I would again go right where we have used the humanized filter

84
00:05:21,900 --> 00:05:23,970
for comma, which is for daily sum.

85
00:05:24,510 --> 00:05:26,010
Let's get rid of that.

86
00:05:26,010 --> 00:05:29,430
And if I do that at refresh now the commas are gone.

87
00:05:29,760 --> 00:05:35,940
And now, as you can see, we now get a nice graph of the past expenses for the sum of expenses which

88
00:05:35,940 --> 00:05:37,860
we have made on each and every date.

89
00:05:38,430 --> 00:05:44,190
So that means now our chart is pretty much ready and our expense tracker app is ready as well.

90
00:05:44,220 --> 00:05:47,400
So now let's change this thing to some of expenses.

91
00:05:47,400 --> 00:05:55,350
So I will go inside the label for this one and I would see some of daily expenses.

92
00:05:56,270 --> 00:05:57,570
Go back, hit refresh.

93
00:05:57,590 --> 00:06:00,190
Now it will kind of go ahead and display that.

94
00:06:00,200 --> 00:06:03,770
So let's make an expense for today, which is going to be a bigger expense.

95
00:06:03,770 --> 00:06:07,490
Let's say let's say the expenses, repairs.

96
00:06:07,520 --> 00:06:09,020
Let's see, the amount is.

97
00:06:09,820 --> 00:06:13,720
$14,000 category is, let's say, business.

98
00:06:14,290 --> 00:06:15,700
If I click on ADD.

99
00:06:16,420 --> 00:06:21,780
As you can see, the expense for today went way up for 23rd November.

100
00:06:21,790 --> 00:06:24,870
And even the dynamics of this chart changed as well.

101
00:06:24,880 --> 00:06:31,000
So that means our expense tracker app is fully functional and it's working absolutely fine without any

102
00:06:31,000 --> 00:06:36,400
issues and it's kind of performing all the operations without any bugs or errors.

103
00:06:36,400 --> 00:06:38,740
And now this expense tracker app is completed.

104
00:06:38,740 --> 00:06:45,340
But as a homework, I would give you one task and that task is to go ahead, add a in, log out and

105
00:06:45,340 --> 00:06:52,510
register functionality to this app and along with this also add a functionality so that a user, when

106
00:06:52,510 --> 00:06:57,910
they add their own expenses, they are only able to see their own expenses and not the expenses for

107
00:06:57,910 --> 00:06:59,020
some other users.

108
00:06:59,200 --> 00:07:05,950
So that means you now need to associate expense with the user ID of each user who's logged in and get

109
00:07:05,950 --> 00:07:07,480
those expenses accordingly.

110
00:07:07,600 --> 00:07:13,630
So a user who logged in into his own account should only be able to see his or her own expenses, and

111
00:07:13,630 --> 00:07:16,810
they should not be able to see the expenses made by the other user.

112
00:07:17,110 --> 00:07:22,660
So this is the task which you need to complete and let me know if you have any difficulties implementing

113
00:07:22,660 --> 00:07:22,900
that.

114
00:07:22,900 --> 00:07:25,990
So this completes the expense tracker section.

115
00:07:25,990 --> 00:07:31,420
If you have any feedback or if you want me to add any more features in this particular app, do let

116
00:07:31,420 --> 00:07:34,480
me know and I'll make sure to consider your suggestions.

