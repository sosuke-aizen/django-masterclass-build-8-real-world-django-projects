1
00:00:00,090 --> 00:00:00,840
In this lecture.

2
00:00:00,840 --> 00:00:05,370
Let's get the expense values and push them into a new array.

3
00:00:05,610 --> 00:00:07,900
So here I would create another array.

4
00:00:07,920 --> 00:00:11,570
Let's call this array as, let's say, sums.

5
00:00:11,580 --> 00:00:17,280
So constant cat sums, which is categorical sums equals an empty array.

6
00:00:17,280 --> 00:00:21,530
And then we take the cat sum, sorry, and push those values in there.

7
00:00:21,540 --> 00:00:23,070
So cat sums don't push.

8
00:00:23,490 --> 00:00:28,710
And just as we are pushing the values over here, we are going to do the same thing over there as well.

9
00:00:28,740 --> 00:00:30,300
So I'll copy this.

10
00:00:31,080 --> 00:00:38,280
Paste it up over here and now, instead of logging the cat's value, I would log in cat sums and see

11
00:00:38,280 --> 00:00:39,480
the result we would get.

12
00:00:40,050 --> 00:00:43,230
So I would go back here, go to the console, hit refresh.

13
00:00:43,230 --> 00:00:46,020
And now, as you can see, we have the expense values here.

14
00:00:46,860 --> 00:00:52,620
But now, out of the expense values of what we want to do is we don't want to get the commas and the

15
00:00:52,620 --> 00:00:53,310
dollars here.

16
00:00:53,310 --> 00:00:56,220
Instead, we want to have pure integer values.

17
00:00:56,520 --> 00:01:01,100
So let's replace the dollar sign with an empty string.

18
00:01:01,110 --> 00:01:04,739
So in here what I would do is that I'm pushing in these values.

19
00:01:04,739 --> 00:01:05,400
That's okay.

20
00:01:05,400 --> 00:01:10,410
But I want to replace the dollar sign with nothing.

21
00:01:11,540 --> 00:01:17,810
And also let's go ahead and let's remove the end comma filter from here as well.

22
00:01:18,320 --> 00:01:23,450
And if I had refreshed now, this is going to give me pure integer values, which I could finally pass

23
00:01:23,450 --> 00:01:27,440
in to this chart, which we have.

24
00:01:27,470 --> 00:01:29,080
So let's do that real quick.

25
00:01:29,090 --> 00:01:34,010
So once we have the chasms, let's see data is going to be.

26
00:01:36,010 --> 00:01:37,360
Cut sums.

27
00:01:38,990 --> 00:01:47,030
And now let's change the label as well to something like expense across categories.

28
00:01:47,960 --> 00:01:49,880
If I do that, head back here.

29
00:01:50,420 --> 00:01:52,850
Let's close this phone now because we don't need it.

30
00:01:52,880 --> 00:01:53,600
Head to refresh.

31
00:01:53,600 --> 00:01:57,470
And as you can see, we have business, food and personal.

32
00:01:57,470 --> 00:02:00,200
So on business, we have spent the most amount on personal.

33
00:02:00,200 --> 00:02:02,090
We have spent this and on food.

34
00:02:02,210 --> 00:02:06,440
It's not actually showing up over here because we have spent an insignificant amount.

35
00:02:06,440 --> 00:02:11,150
And let's say if you delete one of the expenses here, the chart is going to update real time.

36
00:02:11,150 --> 00:02:13,280
So let's delete this rent expense.

37
00:02:13,280 --> 00:02:20,030
So if I delete this now, as you can see, the distribution here changed and you could also delete even

38
00:02:20,030 --> 00:02:21,710
more business expenses.

39
00:02:21,860 --> 00:02:28,370
Let's see, we delete a big one and that's going to give you this evenly distributed chart with all

40
00:02:28,370 --> 00:02:30,770
the categorical spend added up over here.

41
00:02:31,340 --> 00:02:35,780
So that means this particular chart is working absolutely fine.

42
00:02:35,780 --> 00:02:41,450
So in the next lecture, let's go ahead and let's design this chart, which is going to show the past

43
00:02:41,450 --> 00:02:43,370
30 day expenses which we have here.

44
00:02:43,370 --> 00:02:45,050
So let's do that in the next one.

