1
00:00:00,090 --> 00:00:00,870
In this lecture.

2
00:00:00,870 --> 00:00:03,960
Let's work on making this form functional.

3
00:00:04,470 --> 00:00:09,480
So right now we are only handling the get requests for this particular form, which means that whenever

4
00:00:09,480 --> 00:00:13,650
the user actually requests for the page, the page and the form is being rendered.

5
00:00:13,650 --> 00:00:18,480
Well, because inside the view by default, the code which you write in here is for the get request.

6
00:00:18,480 --> 00:00:21,750
So this is what exactly happens when you have a get request.

7
00:00:21,840 --> 00:00:24,600
But what if you actually have a post request?

8
00:00:24,630 --> 00:00:29,310
That means what happens whenever a user actually submits the data from this form?

9
00:00:29,310 --> 00:00:31,770
That data should be added to the database.

10
00:00:31,770 --> 00:00:36,940
So in that particular case, a post request is submitted and we need to now handle the logic for that.

11
00:00:36,960 --> 00:00:44,850
So here I would say if request dot method, if this is equal equal to post in that particular case,

12
00:00:44,850 --> 00:00:46,020
we have to do this.

13
00:00:46,230 --> 00:00:50,640
And what we have to do here is we have to get the data from the expense form.

14
00:00:50,640 --> 00:00:55,410
So in order to get the data from the expense form, I would say expense form, and we have to get the

15
00:00:55,410 --> 00:00:56,820
data from the post request.

16
00:00:56,820 --> 00:00:58,980
So here I would say a request dot post.

17
00:00:58,980 --> 00:01:02,400
So we get the data from there and see we're into something.

18
00:01:02,400 --> 00:01:04,560
So I would save it and do expense.

19
00:01:04,560 --> 00:01:10,350
So I would say expense equals expense form and then accept the post request data.

20
00:01:10,740 --> 00:01:14,330
Once we have this data, we now need to check if that data is valid.

21
00:01:14,340 --> 00:01:21,690
So here I would say if expense DOT is underscore valid, if the data is valid, in that case, we take

22
00:01:21,690 --> 00:01:24,820
the expense object which we have and then we simply save it.

23
00:01:24,840 --> 00:01:30,570
So what this does is that it takes the data, whatever data we have in the form, and it actually saves

24
00:01:30,570 --> 00:01:33,270
it into the database and that's it.

25
00:01:33,270 --> 00:01:36,000
Once we are done with this, we should be good to go.

26
00:01:36,270 --> 00:01:38,610
So let's see if this logic works.

27
00:01:38,610 --> 00:01:39,930
So I'll go back here.

28
00:01:39,930 --> 00:01:41,850
So I would go to the admin and log in.

29
00:01:42,000 --> 00:01:45,930
Right now if you go to expenses, we only have one expense which is iMac.

30
00:01:46,200 --> 00:01:49,110
And now let's try adding one more expense using this.

31
00:01:49,110 --> 00:01:51,720
So let's say I buy an iPhone.

32
00:01:54,560 --> 00:01:59,860
$4,000 category is, let's say, personal.

33
00:01:59,870 --> 00:02:03,570
If I click on add, the form data kind of disappeared.

34
00:02:03,590 --> 00:02:05,510
Let's see if that is submitted here.

35
00:02:05,780 --> 00:02:11,420
So if I had to refresh, as you can see now, we have this iPhone added up over here along with the

36
00:02:11,420 --> 00:02:13,080
amount category and name.

37
00:02:13,100 --> 00:02:17,320
So that means this form is now fully functional and it's working.

38
00:02:17,330 --> 00:02:22,280
So once we have created this form in the next lecture, let's go ahead and let's learn how we could

39
00:02:22,280 --> 00:02:27,470
take all the expenses which we have made and display them up over here inside of a table.

40
00:02:27,770 --> 00:02:30,110
So let's learn how to do that in the next one.

