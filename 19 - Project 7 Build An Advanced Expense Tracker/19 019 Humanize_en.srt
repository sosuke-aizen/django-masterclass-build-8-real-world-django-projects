1
00:00:00,090 --> 00:00:04,890
In this particular lecture, let's actually learn how to add commas to these particular values, which

2
00:00:04,890 --> 00:00:05,520
we have.

3
00:00:05,820 --> 00:00:12,150
So in order to add commas here, there's a special library, or you could say an app in Django, which

4
00:00:12,150 --> 00:00:14,390
you could use, which is called as Humanise.

5
00:00:14,400 --> 00:00:20,050
So it comes built in with Django and you don't have to use Pips to kind of go ahead and install it.

6
00:00:20,070 --> 00:00:26,220
So in order to install humanise, you would need to go inside settings dot p file off your project.

7
00:00:26,370 --> 00:00:31,860
And in here inside the settings or P vi file, you simply have to include humanise inside the installed

8
00:00:31,860 --> 00:00:32,189
apps.

9
00:00:32,189 --> 00:00:39,270
So right up over here right after the my apps you need to see django dot contrib dot and just as we

10
00:00:39,270 --> 00:00:42,360
have this admin auth and other apps here I would see.

11
00:00:43,160 --> 00:00:44,180
Humanise.

12
00:00:44,570 --> 00:00:50,630
And what this humanise does is that it kind of humanizes the data which we have on our application.

13
00:00:50,630 --> 00:00:55,880
So for example, if you want to add commas to certain numbers, you could do that using humanise.

14
00:00:55,880 --> 00:01:00,830
So once you have added that, then you need to go to the file where you want to use humanise.

15
00:01:00,830 --> 00:01:05,180
So in this case we want to use humanise in the index dot HTML file.

16
00:01:05,180 --> 00:01:06,690
So I'll go right up over here.

17
00:01:06,710 --> 00:01:13,460
So inside the index dot html right after the load static tag, I have to load the humanise tag by saying

18
00:01:13,460 --> 00:01:14,300
load.

19
00:01:16,100 --> 00:01:17,210
Humanize.

20
00:01:18,530 --> 00:01:21,080
Once we do that, we are pretty much good to go.

21
00:01:21,470 --> 00:01:26,930
And now, once we have loaded the humanise tag, we simply have to go ahead and go to the value which

22
00:01:26,930 --> 00:01:27,890
we want to humanise.

23
00:01:27,890 --> 00:01:32,530
So in this particular case, we want to humanise these values, which are the amount values.

24
00:01:32,540 --> 00:01:36,100
So for humanizing them, you just have to go inside those values.

25
00:01:36,110 --> 00:01:43,220
So I'll go to expense amount and in here I have to use this perpendicular line and I have to say end

26
00:01:43,670 --> 00:01:44,290
comma.

27
00:01:44,300 --> 00:01:48,910
And what this does is that it's actually going to add commas to the integer value which we have.

28
00:01:48,920 --> 00:01:51,020
So now if I go back and hit refresh.

29
00:01:52,240 --> 00:01:54,010
For some reason it's not working.

30
00:01:54,010 --> 00:01:56,410
So let me check what exactly happens.

31
00:01:56,450 --> 00:02:00,580
Okay, so let's debug this error, which is no module named Humanise.

32
00:02:00,610 --> 00:02:02,910
Let's take a look at this and the browser.

33
00:02:02,920 --> 00:02:05,110
So let's search for this error in a new tab.

34
00:02:05,500 --> 00:02:07,840
And let's see what exactly went wrong.

35
00:02:10,050 --> 00:02:10,820
Okay.

36
00:02:10,830 --> 00:02:16,530
I think we made a silly mistake of not adding a comma after this one, so I need to add a comma.

37
00:02:16,530 --> 00:02:19,800
And once we do that, the server is back up and running.

38
00:02:20,310 --> 00:02:25,240
I could hit refresh and as you can see now, the comma is added up over here for $1,000.

39
00:02:25,260 --> 00:02:30,460
So let's try adding an expense which costs around, let's say $100,000.

40
00:02:30,480 --> 00:02:37,140
So let's say the expenses rent amount is $123,000.

41
00:02:38,010 --> 00:02:40,080
Category is business.

42
00:02:41,210 --> 00:02:47,090
If I click on ADD, as you can see, that amount is added up over here and the comma is added automatically.

43
00:02:47,090 --> 00:02:53,090
So just as we have added a comma here, we could also add a comma to the final total amount as well.

44
00:02:53,420 --> 00:02:59,480
So in order to add the comma to the total amount, I simply have to repeat the steps which I have done

45
00:02:59,480 --> 00:03:00,350
for.

46
00:03:01,350 --> 00:03:02,140
This one.

47
00:03:02,160 --> 00:03:06,840
So I just have to say and comma for that value as well, which is the sum value.

48
00:03:06,870 --> 00:03:08,520
So I'll add that up over here.

49
00:03:09,030 --> 00:03:10,980
Go back to the browser, hit refresh.

50
00:03:12,170 --> 00:03:14,710
Okay, so I guess we need to visit the URL.

51
00:03:14,720 --> 00:03:18,250
And as you can see now, we have a comma up over here as well.

52
00:03:18,260 --> 00:03:21,590
That means the values which we have here are now humanized.

53
00:03:21,830 --> 00:03:24,400
Now, you could also humanize the date values as well.

54
00:03:24,410 --> 00:03:29,570
Like, for example, instead of getting the exact same date, you could use the humanized library to

55
00:03:29,570 --> 00:03:35,070
kind of say that to this expense was made two days before, three days before, so on and so forth.

56
00:03:35,090 --> 00:03:39,440
By adding different filters over here, just as we have added the end comma filter.

57
00:03:39,950 --> 00:03:42,500
But for now, we are going to leave it as it is.

58
00:03:42,500 --> 00:03:45,380
So once this thing is done, we are pretty much good to go.

59
00:03:45,410 --> 00:03:47,870
We have also calculated the total as well.

60
00:03:47,960 --> 00:03:54,330
But now let's say you want to display the values which is going to be the total for the entire week,

61
00:03:54,350 --> 00:03:58,700
the total for the past 30 days and the total for the past one year.

62
00:03:59,000 --> 00:04:02,510
That is the total expense for all of these periods of time.

63
00:04:02,660 --> 00:04:07,670
In the next lecture, let's learn how we could do that in the backend inside Django and display them

64
00:04:07,670 --> 00:04:09,720
up over here inside this template.

65
00:04:09,740 --> 00:04:13,760
So thank you very much for watching and I'll see you guys in the next one.

66
00:04:13,790 --> 00:04:14,600
Thank you.

