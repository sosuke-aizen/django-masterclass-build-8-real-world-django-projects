1
00:00:00,150 --> 00:00:05,430
So in this particular lecture, let's work on retrieving all the expenses and displaying them up over

2
00:00:05,430 --> 00:00:06,510
here on this page.

3
00:00:06,540 --> 00:00:11,640
So in order to retrieve the expenses, first of all, we have to go inside the view, start by file,

4
00:00:11,640 --> 00:00:14,610
and kind of get all the expenses as objects.

5
00:00:14,610 --> 00:00:19,890
So right up over here in the index view, right now, we only have access to the expense form, but

6
00:00:19,890 --> 00:00:21,900
we don't have access to expenses.

7
00:00:22,230 --> 00:00:27,060
So in order to get access to expenses, there's one simple line of code which we need to write, and

8
00:00:27,060 --> 00:00:34,340
that is we simply need to say expenses equals expense, which is the expense model.

9
00:00:34,350 --> 00:00:38,130
So expense DOT objects.

10
00:00:39,210 --> 00:00:43,970
Dot all, and that's actually going to get all the expense objects from the expense model.

11
00:00:43,980 --> 00:00:48,270
But right now we also do not have the expense model imported here.

12
00:00:48,390 --> 00:00:50,880
So therefore, I would type in from DOT models.

13
00:00:50,880 --> 00:00:54,000
I would say I want to import the expense.

14
00:00:54,540 --> 00:00:59,010
So now once we have the expense model, we extracted all the objects from there.

15
00:00:59,010 --> 00:01:03,870
We have saved it into this expenses and we could finally pass in those expenses up over here.

16
00:01:04,050 --> 00:01:09,150
So here I would say expenses and then simply pass an expenses here.

17
00:01:09,360 --> 00:01:14,990
Now, once the expenses are passed as the context to the index HTML, let's go ahead and let's try to

18
00:01:15,000 --> 00:01:16,590
driving those expenses here.

19
00:01:17,100 --> 00:01:20,570
So right where this foments so this form starts here.

20
00:01:20,580 --> 00:01:21,450
It ends here.

21
00:01:21,720 --> 00:01:26,880
So just as we have a header over here, which is this is an index page, let's create another header

22
00:01:26,880 --> 00:01:31,800
up over here in the index dot HTML and let's see this thing, say something like expenses.

23
00:01:31,800 --> 00:01:36,030
So here I would create a div and I would say expenses.

24
00:01:36,720 --> 00:01:41,460
And in order to retrieve those actual expenses, I will loop through the expenses context.

25
00:01:41,460 --> 00:01:47,700
So here I would say for expense, that means a single expense in expenses.

26
00:01:47,700 --> 00:01:50,840
So an expenses is a list of objects which we have passed.

27
00:01:51,450 --> 00:01:58,530
Let's type an info and in here for now, let's just simply display the expenses and see the kind of

28
00:01:58,530 --> 00:01:59,550
results we would get.

29
00:01:59,550 --> 00:02:03,540
And here, if I say expense and if I go back to the browser and hit refresh.

30
00:02:05,800 --> 00:02:07,030
This is what I get.

31
00:02:07,030 --> 00:02:11,410
So I get the expenses which are made because if you go right up inside the models, you will be able

32
00:02:11,410 --> 00:02:16,330
to see that we have this SDB method which returns self dot name.

33
00:02:16,780 --> 00:02:23,350
And if I want to get the cost associated with that expense, I simply have to say expense dot amount.

34
00:02:23,470 --> 00:02:31,270
So in here inside the templates, if I say expense dot amount, if I go back to the browser, you'll

35
00:02:31,270 --> 00:02:35,860
be able to see that I now have the cost of those expenses here and now you could also go ahead and kind

36
00:02:35,860 --> 00:02:37,390
of print them up altogether.

37
00:02:37,390 --> 00:02:43,480
So I could say expense, dot name, expense dot amount, expense, dot category.

38
00:02:46,470 --> 00:02:49,200
And then finally, I could say expense dot.

39
00:02:50,040 --> 00:02:54,270
The date because date is nothing but when the expense was made.

40
00:02:54,750 --> 00:02:59,610
So now if I go back, as you can see now, I have all that info displayed up over here.

41
00:02:59,880 --> 00:03:05,070
Now our job is to go ahead and display this particular information in a tabular format or in a card

42
00:03:05,070 --> 00:03:07,470
like format up over here inside of a table.

43
00:03:07,500 --> 00:03:09,930
So let's learn how to do that in the next one.

