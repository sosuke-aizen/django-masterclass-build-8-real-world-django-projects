1
00:00:00,120 --> 00:00:00,900
In this lecture.

2
00:00:00,900 --> 00:00:06,160
Let's actually fix the alignment issue, which we have up over here inside this table.

3
00:00:06,180 --> 00:00:11,520
So the reason why we have this alignment issue is because if you actually take a look at the code here,

4
00:00:11,670 --> 00:00:19,080
you'll be able to see that for this particular rows which we have up over here, that is this rows here,

5
00:00:19,080 --> 00:00:23,940
we actually have set the padding X and padding Y, and we have not done the same thing with this div

6
00:00:23,940 --> 00:00:24,720
up over here.

7
00:00:24,840 --> 00:00:28,790
So we need to simply do that over here as well and it's going to align it properly.

8
00:00:28,800 --> 00:00:32,430
So padding x is 20 and padding y is five.

9
00:00:33,000 --> 00:00:34,590
If I do that, hit refresh.

10
00:00:34,590 --> 00:00:39,090
And now, as you can see, these things are aligned properly right next to each other.

11
00:00:39,300 --> 00:00:44,400
Now there's one more issue in the buttons which we have up over here, which are the image buttons.

12
00:00:44,490 --> 00:00:48,570
So let's fix the alignment issues with those particular buttons as well.

13
00:00:48,870 --> 00:00:54,640
So for all the spans which we have here, that is this span which is going to be for the image and this

14
00:00:54,640 --> 00:00:55,530
span as well.

15
00:00:55,980 --> 00:01:01,710
We have actually set the font to bold, but as they no longer have fonts, we would simply remove them

16
00:01:01,830 --> 00:01:07,080
and instead we want to add some padding from the left so that these things are actually aligned at the

17
00:01:07,080 --> 00:01:07,710
center.

18
00:01:07,770 --> 00:01:11,040
So let's say padding on the left is 24.

19
00:01:11,100 --> 00:01:15,000
Let's do the same thing with this as well, plus 24.

20
00:01:15,030 --> 00:01:19,960
Once we do that at refresh, as you can see now, these are aligned properly as well.

21
00:01:19,980 --> 00:01:22,710
You could even reduce the padding on the delete one.

22
00:01:22,710 --> 00:01:27,060
And now, as you can see, it's actually aligned properly so that thing is fixed.

23
00:01:27,090 --> 00:01:32,190
Now, let's also go ahead and style up this particular expenses as well in a better way.

24
00:01:32,280 --> 00:01:38,910
So let's make this font a little bit bolder and let's also align it right next to this table right here.

25
00:01:39,180 --> 00:01:43,950
So in order to do that, I would go right where we have typed in expenses, which is here.

26
00:01:44,310 --> 00:01:48,240
Let's give it a class and let's add margin.

27
00:01:49,080 --> 00:01:50,700
Ten from all the sides.

28
00:01:50,820 --> 00:01:53,580
Let's say the font is going to be bold.

29
00:01:54,800 --> 00:01:56,900
And we should be good to go.

30
00:01:57,290 --> 00:02:00,830
So if I go back here, as you can see now, it's aligned properly.

31
00:02:01,100 --> 00:02:04,450
Now, there's one more thing missing, and that's actually a navigation bar.

32
00:02:04,460 --> 00:02:07,560
So we simply have something which says that this is the index page.

33
00:02:07,580 --> 00:02:10,610
Now, instead of that, let's add in navigation bar.

34
00:02:10,610 --> 00:02:16,400
And whenever we have to add a navigation bar, we always do that in the base template, which we have.

35
00:02:16,550 --> 00:02:20,780
So let's go inside the base dot HTML and let's see the kind of code we have in there.

36
00:02:21,110 --> 00:02:26,110
So right now we are simply having the body here and we are not having anything else.

37
00:02:26,120 --> 00:02:28,400
So let's add a nav here.

38
00:02:29,420 --> 00:02:31,940
And this NAV is going to let's see how shadow.

39
00:02:31,940 --> 00:02:37,280
So I would say nav class is shadow LG, which means a larger shadow.

40
00:02:37,970 --> 00:02:40,310
We won't set any background color for this one.

41
00:02:40,310 --> 00:02:41,450
Let's keep it white.

42
00:02:41,570 --> 00:02:46,520
Now let's create a container which is actually going to hold the items inside this particular NAV.

43
00:02:46,790 --> 00:02:50,660
So I'll assign it a class as container.

44
00:02:50,810 --> 00:02:55,520
I will make it flex and make it flex wrap to span the entire width.

45
00:02:55,580 --> 00:03:03,680
And I would also add a property which is just to fly between and then items center and margin.

46
00:03:03,680 --> 00:03:05,730
On the x axis, I would set it to auto.

47
00:03:05,750 --> 00:03:12,350
So once we have this div now let's actually create a logo and this logo needs to be wrapped up inside

48
00:03:12,350 --> 00:03:15,680
an ref tag so that that website logo is clickable.

49
00:03:15,860 --> 00:03:17,780
So I would type in a ref.

50
00:03:18,560 --> 00:03:21,410
Let's set the ref to hash for now.

51
00:03:21,770 --> 00:03:24,070
I will also make this thing flex as well.

52
00:03:24,080 --> 00:03:25,640
And then I'll add an image here.

53
00:03:25,640 --> 00:03:28,600
And this image is going to be the icon of our website.

54
00:03:28,610 --> 00:03:33,050
And after the icon, I would add the name of our website in a span tag.

55
00:03:33,140 --> 00:03:37,460
So let's say we call our website or app as expense.

56
00:03:38,760 --> 00:03:40,650
Cracker, I would add that.

57
00:03:40,890 --> 00:03:47,790
Now, let's add some image in here, and in order to add the image as VI and expense track a website,

58
00:03:47,790 --> 00:03:49,570
I would go back to Icon Finder.

59
00:03:49,980 --> 00:03:57,390
I would search for expense or let's search for wallet because that also signifies an expense.

60
00:03:57,390 --> 00:03:59,550
So let's click on free.

61
00:03:59,580 --> 00:04:01,200
Okay, we have already selected that.

62
00:04:01,200 --> 00:04:02,990
Let's choose this icon right here.

63
00:04:03,750 --> 00:04:05,040
Download this one.

64
00:04:07,150 --> 00:04:09,040
Open up Vesco real quick.

65
00:04:09,160 --> 00:04:10,760
Take that particular icon.

66
00:04:10,780 --> 00:04:12,190
Put it up over here.

67
00:04:12,220 --> 00:04:14,650
Let's rename this thing to something like.

68
00:04:15,570 --> 00:04:16,410
Wallet.

69
00:04:17,350 --> 00:04:19,300
Let's go back to based on the HTML.

70
00:04:19,300 --> 00:04:24,190
And in here, I want to see that the image source is going to be static.

71
00:04:24,980 --> 00:04:30,560
My app forward slash images, forward slash wallet, dot bag.

72
00:04:30,560 --> 00:04:34,910
And as soon as I add that, I also need to ensure that I have load statics.

73
00:04:34,910 --> 00:04:39,020
So we already have load static tag in here, which means we are good to go.

74
00:04:39,290 --> 00:04:42,530
So if I go back here, hit refresh, that logo is added.

75
00:04:42,560 --> 00:04:44,540
We need to reduce the size of that thing.

76
00:04:45,140 --> 00:04:49,340
So let's add a class and make the height of this thing to ten.

77
00:04:49,370 --> 00:04:52,190
Now, as you can see, we have the logo and the nav bar.

78
00:04:52,220 --> 00:04:55,700
The only thing is we are having some alignment issues.

79
00:04:55,790 --> 00:04:57,290
So let's fix that.

80
00:04:57,320 --> 00:05:00,620
Let's add some margin on the right hand side for the image.

81
00:05:00,980 --> 00:05:07,400
So here I would say for the image, I want the margin right to be, let's see, three.

82
00:05:08,420 --> 00:05:09,900
I could go back here to refresh.

83
00:05:09,920 --> 00:05:11,450
Now, it has some margin here.

84
00:05:11,480 --> 00:05:15,980
And for the NAV itself, the entire nav bar is kind of stuck here.

85
00:05:16,130 --> 00:05:18,370
So let's add some padding as well.

86
00:05:18,380 --> 00:05:20,720
So let's say padding a file from all the sides.

87
00:05:20,720 --> 00:05:22,700
So this should be p-5.

88
00:05:22,850 --> 00:05:24,400
Go back here, hit refresh.

89
00:05:24,410 --> 00:05:28,220
As you can see now, we have some padding as well from all the sides.

90
00:05:28,250 --> 00:05:31,010
Now, this text is not aligned to the center.

91
00:05:31,160 --> 00:05:36,590
So for this span here, I would add a class and see, I want to self center this one.

92
00:05:37,340 --> 00:05:38,920
Self center the span.

93
00:05:38,930 --> 00:05:45,930
I want the text to be extra large and I want the fonts to be, let's say, semi bold for the site name.

94
00:05:45,950 --> 00:05:51,720
So once this thing is done, I could go back here to refresh and here we have it and nav pie is ready.

95
00:05:51,740 --> 00:05:54,950
So let's get rid of this index page here instead.

96
00:05:54,950 --> 00:05:57,200
Let's see, add an expense.

97
00:05:57,590 --> 00:06:05,630
So in order to add that, I will again go back to index dot HTML, take this expenses from here.

98
00:06:07,030 --> 00:06:09,430
Replace this h one tag with this one.

99
00:06:10,650 --> 00:06:13,140
And I would say add expense.

100
00:06:13,380 --> 00:06:16,440
Let's add an emoji here to keep things simple.

101
00:06:16,470 --> 00:06:19,020
So I would use this add imager.

102
00:06:20,750 --> 00:06:23,780
And let's use one more thing over here as well.

103
00:06:23,780 --> 00:06:28,040
So in here, let's use currency.

104
00:06:28,070 --> 00:06:30,140
Use a currency like that.

105
00:06:30,710 --> 00:06:31,220
Okay.

106
00:06:31,250 --> 00:06:37,490
If I go back and refresh now, as you can see, the layout of our site looks much better.

107
00:06:37,490 --> 00:06:38,840
So the layout is ready.

108
00:06:38,840 --> 00:06:39,950
Everything is ready.

109
00:06:39,980 --> 00:06:45,560
We also are able to perform the basic CRUD operations, like adding an expense, editing an expense,

110
00:06:45,560 --> 00:06:48,830
deleting an expense as well, and reading the expenses as well.

111
00:06:49,010 --> 00:06:55,250
Now, in the next lecture, let's actually go ahead and derive some statistics out of the expenses which

112
00:06:55,250 --> 00:06:55,680
we have.

113
00:06:55,700 --> 00:07:00,980
So for example, let's see, we want to display the total amount which is spent, total amount of sum

114
00:07:00,980 --> 00:07:03,510
spent across categories, so on and so forth.

115
00:07:03,530 --> 00:07:06,260
So let's learn how to do that in the next lecture.

