1
00:00:00,120 --> 00:00:05,320
So in this particular lecture, let's go ahead and let's start working on creating our application.

2
00:00:05,340 --> 00:00:11,490
So as our applications job is to go ahead and track all the expenses, that means we need to save these

3
00:00:11,490 --> 00:00:14,660
particular expenses and some sort of a database.

4
00:00:14,670 --> 00:00:19,890
So that means now we actually need to create a model for the expense.

5
00:00:20,100 --> 00:00:25,320
So let's go inside the my app and then let's go inside the model start py for my app.

6
00:00:25,320 --> 00:00:30,570
And here is where exactly we will be creating the model for saving up our expenses.

7
00:00:30,600 --> 00:00:33,840
So in here let's create a model called as expense.

8
00:00:33,840 --> 00:00:40,410
So here I would say class, let's say expense, and this particular thing is going to be a model, so

9
00:00:40,410 --> 00:00:42,750
it's going to inherit from model start model.

10
00:00:43,080 --> 00:00:48,480
Now here we need to define the fields like what All fields are going to define our expense.

11
00:00:48,480 --> 00:00:51,090
So obviously the expense which you create.

12
00:00:51,090 --> 00:00:57,900
So for example, let's say you go to shopping and you shop for an electronic gadget for your business.

13
00:00:57,900 --> 00:01:02,760
So that particular shopping expense is going to have some name.

14
00:01:02,760 --> 00:01:08,070
So let's say you have bought an iMac, so the name of that particular expense would be iMac.

15
00:01:08,070 --> 00:01:12,270
Then you will also associate an amount with that particular expense as well.

16
00:01:12,270 --> 00:01:14,820
Like how much the amount actually costs.

17
00:01:14,820 --> 00:01:20,490
Then there's going to be a category for that expense, like whether this is a personal expense or this

18
00:01:20,490 --> 00:01:25,440
is some business expense or it's related to food, so on and so forth.

19
00:01:25,440 --> 00:01:28,350
So we also need to track the category as well.

20
00:01:28,350 --> 00:01:33,690
And then again, the most important thing with an expense is that you also need to be able to define

21
00:01:33,690 --> 00:01:38,070
a date on an expense so that you know when exactly you have spent that amount.

22
00:01:38,730 --> 00:01:41,190
So these are the fields which we want for now.

23
00:01:41,190 --> 00:01:44,870
And if you want any other additional fields, you could add them as well.

24
00:01:44,880 --> 00:01:47,670
So let's go ahead and let's start creating those fields.

25
00:01:47,670 --> 00:01:51,810
So first of all, I would create name this is going to be a character field.

26
00:01:51,810 --> 00:01:58,080
So I would say model start car field and let's set up a max length for this one as 100.

27
00:01:58,530 --> 00:02:03,690
After that, let's set the amount, which is nothing but the cost of that particular product or that

28
00:02:03,690 --> 00:02:04,530
particular expense.

29
00:02:04,530 --> 00:02:07,680
So this is going to be model start integer field.

30
00:02:07,680 --> 00:02:14,130
So let's say we want to track our expenses in terms of integers and we want to avoid decimal points.

31
00:02:14,220 --> 00:02:16,650
And then let's specify the category.

32
00:02:16,680 --> 00:02:22,500
Now the category could be another model as well if you want to create a complex category.

33
00:02:22,500 --> 00:02:28,230
But for now, let's keep things simple and let's simply make this a character field with a max length

34
00:02:28,230 --> 00:02:29,730
of, let's say, 50.

35
00:02:30,390 --> 00:02:34,140
And then finally, we need to associate a date with that expense.

36
00:02:34,140 --> 00:02:39,000
So I would say date equals, let's say model start date field.

37
00:02:39,000 --> 00:02:45,840
And whenever we create this date field, one thing which we want to do is that we don't want the user

38
00:02:45,840 --> 00:02:47,130
to manually set the date.

39
00:02:47,130 --> 00:02:53,940
Instead, what we wish to do is the date should automatically be populated whenever an expense is created.

40
00:02:53,940 --> 00:03:00,630
And for that very purpose, we set the auto now for this thing, which is a parameter to true.

41
00:03:00,660 --> 00:03:06,540
So what this does is that whenever you create an expense, you only have to add the name amount and

42
00:03:06,540 --> 00:03:12,570
category, and Django is automatically going to set the date to the current date, which is for today.

43
00:03:13,020 --> 00:03:16,860
So once we have created this model, we are pretty much good to go.

44
00:03:16,950 --> 00:03:21,390
So now let's actually make migrations by going inside the terminal.

45
00:03:21,540 --> 00:03:25,080
So first of all, I'll stop the server clear up the screen.

46
00:03:25,290 --> 00:03:35,400
I would say Python manage, start by make migrations and then Python managed by migrate.

47
00:03:36,210 --> 00:03:41,330
So now once the migrations are created, this table should be created at the back end.

48
00:03:41,340 --> 00:03:45,540
And now in order to actually populate this table, we will need a super user.

49
00:03:45,570 --> 00:03:51,210
So in the next lecture, let's go ahead, let's create a super user and let's add some dummy values

50
00:03:51,210 --> 00:03:54,360
into the expenses model, which we have just created.

