1
00:00:00,180 --> 00:00:04,980
So in this particular lecture, let's learn how we could calculate the sum of the total amount which

2
00:00:04,980 --> 00:00:06,540
is being spent up over here.

3
00:00:06,780 --> 00:00:11,670
So right now we have all the expenses and let's say we want to calculate the total of those expenses

4
00:00:11,670 --> 00:00:13,920
and display it up over here inside the table.

5
00:00:14,040 --> 00:00:18,510
Now, one way to do this is you could actually implement this using JavaScript.

6
00:00:18,510 --> 00:00:24,330
So in JavaScript you could get access to the elements by the IDs or something like that, and then just

7
00:00:24,330 --> 00:00:27,560
sum these values up and then display the sum up over here.

8
00:00:27,570 --> 00:00:31,830
But another way to do this would be to go ahead and do this on the back end.

9
00:00:31,830 --> 00:00:34,460
That is to this inside Django itself.

10
00:00:34,470 --> 00:00:36,930
So let's learn how exactly we could implement that.

11
00:00:36,930 --> 00:00:42,600
So I'll go back here to Vsco code and I will go to the views, which is rendering this particular table

12
00:00:42,600 --> 00:00:43,230
right here.

13
00:00:43,530 --> 00:00:46,260
So the view responsible for this is index view.

14
00:00:46,260 --> 00:00:52,890
And inside this particular view now we need to calculate the sum of the total of all the expenses.

15
00:00:53,130 --> 00:00:58,950
So the way in which we do that is we first go ahead and take all the expenses or the expense objects.

16
00:00:58,950 --> 00:01:04,500
And once we have those particular expense objects, what we do is that we simply try to calculate the

17
00:01:04,500 --> 00:01:05,310
sum of them.

18
00:01:05,310 --> 00:01:12,180
So here we already have the expense objects and we have passed those particular expense objects as expenses

19
00:01:12,180 --> 00:01:13,470
as context here.

20
00:01:13,890 --> 00:01:22,980
Now, in order to get the sum, what I would do is that I would say something like total expenses equals

21
00:01:23,190 --> 00:01:27,720
and this is actually going to go ahead take all the objects, which is expenses.

22
00:01:27,720 --> 00:01:31,500
And after getting the expenses, we are going to aggregate them.

23
00:01:31,500 --> 00:01:35,040
And by aggregation we mean that we take all of them together.

24
00:01:35,040 --> 00:01:36,930
So we say expenses don't.

25
00:01:38,410 --> 00:01:39,460
Aggregate.

26
00:01:39,970 --> 00:01:45,640
And then we say that after aggregating those expense objects, we want to calculate the sum of those

27
00:01:45,640 --> 00:01:46,210
expenses.

28
00:01:46,210 --> 00:01:49,990
So I would say sum and then pass in an amount here.

29
00:01:50,110 --> 00:01:53,080
So once this thing is done, we are pretty much good to go.

30
00:01:53,080 --> 00:01:56,290
And this actually calculates the sum of all the expenses here.

31
00:01:56,530 --> 00:01:59,950
Now we have the sum of expenses saved in total expenses.

32
00:01:59,950 --> 00:02:04,710
And our job now is to simply go ahead and pass those particular expenses here.

33
00:02:04,720 --> 00:02:10,210
But even before passing those expenses, let's actually try printing up the value of the total expenses

34
00:02:10,210 --> 00:02:12,500
and see the kind of results we would get.

35
00:02:12,520 --> 00:02:15,640
So here I would simply print up the total expenses.

36
00:02:15,640 --> 00:02:17,500
And now let's go back.

37
00:02:18,340 --> 00:02:19,270
To the browser.

38
00:02:19,870 --> 00:02:25,150
And now let's make a fresh get request to this so that the print statement is actually triggered.

39
00:02:25,600 --> 00:02:29,730
Okay, So we got an error here which says that some is not defined.

40
00:02:29,740 --> 00:02:30,130
Okay.

41
00:02:30,130 --> 00:02:35,260
So as we have used some over there, we actually need to import some.

42
00:02:35,260 --> 00:02:40,270
So some is actually a method which is present inside the Django models.

43
00:02:40,270 --> 00:02:42,250
So we need to import that up over here.

44
00:02:42,250 --> 00:02:46,030
So I need to say from Django dot db dot models.

45
00:02:46,030 --> 00:02:52,930
So as we are performing these summation on the database values, we need to import the some method from

46
00:02:52,930 --> 00:02:53,140
that.

47
00:02:53,140 --> 00:02:55,360
So we actually forgot to do that.

48
00:02:55,450 --> 00:02:57,450
And as soon as you do that it refresh.

49
00:02:57,460 --> 00:03:01,270
Now this page is rendered and the print method would be triggered as well.

50
00:03:01,270 --> 00:03:06,910
So if you actually go to the terminal here as we actually printing up the total expenses, the total

51
00:03:06,910 --> 00:03:09,490
expenses object gave us this result.

52
00:03:09,490 --> 00:03:13,900
So in that particular object, we have this value which is called us amount sum.

53
00:03:14,470 --> 00:03:18,850
That means the total amount summit's displaying is 1333.

54
00:03:18,940 --> 00:03:24,880
So if you actually go back here and if you try to calculate the sum of these values, that's obviously

55
00:03:24,880 --> 00:03:25,900
the correct answer.

56
00:03:26,350 --> 00:03:32,050
So that means it's working fine and that means we simply have to pass in the total expenses here.

57
00:03:32,500 --> 00:03:37,870
So I would give a comma and also pass in total expenses.

58
00:03:38,560 --> 00:03:41,410
As total expenses here.

59
00:03:41,410 --> 00:03:47,020
And once we have passed that thing in, I would go inside the index and let's say you want to display

60
00:03:47,020 --> 00:03:52,480
that up over here right after this sell, that is when all the rows are actually completed.

61
00:03:52,630 --> 00:03:55,240
So for that we go right where the four ends.

62
00:03:55,240 --> 00:03:57,070
So the four actually ends here.

63
00:03:57,960 --> 00:04:01,050
And right after the fall ends, we will display the total.

64
00:04:01,050 --> 00:04:03,000
So here I would say something like.

65
00:04:04,560 --> 00:04:06,870
Displaying total.

66
00:04:08,030 --> 00:04:10,490
Let's get that piece set up over here.

67
00:04:10,550 --> 00:04:13,610
And in there, I will actually create a span tag to display that.

68
00:04:13,610 --> 00:04:19,850
I will say that the class of this thing is going to be font bold, and I want to show it in a green

69
00:04:19,850 --> 00:04:20,180
color.

70
00:04:20,180 --> 00:04:26,240
So text green with a shade of, let's say 500, and to display the actual amount, I would display the

71
00:04:26,240 --> 00:04:35,000
actual amount and template syntax and I would say total and let's go expenses dot And here inside total

72
00:04:35,000 --> 00:04:38,060
expenses, you actually want to get the value which is amount sum.

73
00:04:38,480 --> 00:04:40,670
So in there I would say.

74
00:04:41,600 --> 00:04:44,450
Amount, double underscore some.

75
00:04:44,750 --> 00:04:45,170
Okay.

76
00:04:45,170 --> 00:04:47,420
So now if I go back here, hit refresh.

77
00:04:47,570 --> 00:04:50,480
As you can see, that value is being displayed up over here.

78
00:04:50,480 --> 00:04:53,690
And I actually want to display it right up over here.

79
00:04:53,990 --> 00:04:57,110
And in order to do that, I will actually create a div here.

80
00:04:58,010 --> 00:05:01,830
And get this entire span and place it inside that div.

81
00:05:01,850 --> 00:05:03,110
So I'll cut it from here.

82
00:05:03,140 --> 00:05:04,460
Paste it up over here.

83
00:05:05,570 --> 00:05:13,310
And then I would see that the class of this thing is going to be p x 72, which adds a padding of 72.

84
00:05:13,430 --> 00:05:16,160
And we also want to add some padding from the top as well.

85
00:05:16,160 --> 00:05:19,850
So p y equals five and hopefully that should align it over here.

86
00:05:20,030 --> 00:05:22,250
So it aligned it kind of over here.

87
00:05:22,550 --> 00:05:27,710
If you want to give more padding, you could make it something like 80.

88
00:05:28,250 --> 00:05:32,090
And here I could also add some text which say something like.

89
00:05:33,120 --> 00:05:33,750
Total.

90
00:05:34,890 --> 00:05:35,400
Okay.

91
00:05:36,390 --> 00:05:37,680
So it's not a line properly.

92
00:05:37,680 --> 00:05:39,540
We are going to fix that later, obviously.

93
00:05:39,540 --> 00:05:43,200
But for now we get the sum here and that's exactly what we want.

94
00:05:43,290 --> 00:05:47,610
Now, one more thing that could be done here is that you could actually add a dollar sign to this sum

95
00:05:47,610 --> 00:05:50,820
so that this sum looks more natural and its own unit.

96
00:05:50,850 --> 00:05:56,370
So we could add a dollar sign here and we could also add dollar signs to these sum values as well,

97
00:05:56,370 --> 00:05:57,150
which we have.

98
00:05:57,390 --> 00:06:02,050
So right now, these are just showing up simple values without any currency.

99
00:06:02,070 --> 00:06:04,470
So let's add currency to them as well.

100
00:06:04,590 --> 00:06:09,120
So in here I would say dollar and then the actual amount.

101
00:06:09,820 --> 00:06:11,740
So this is what it looks like.

102
00:06:11,800 --> 00:06:15,790
And one more thing that could be done here is that now let's see.

103
00:06:16,720 --> 00:06:20,020
You have this amount here and you don't have any kind of commas.

104
00:06:20,020 --> 00:06:24,940
So if the value is bigger, it actually becomes unreadable or difficult to use.

105
00:06:25,210 --> 00:06:30,720
Therefore, what we could do is that we could add commas right where we actually require them.

106
00:06:30,730 --> 00:06:35,680
So now the question is how exactly would you add those commas, what kind of code you would write.

107
00:06:35,890 --> 00:06:41,260
Now the best thing about Django is that it actually comes in with a library, and that library actually

108
00:06:41,260 --> 00:06:45,490
helps us to automatically add commas to the numerical values which we have.

109
00:06:45,880 --> 00:06:49,150
So we are going to learn how to implement that in the next lecture.

110
00:06:49,150 --> 00:06:53,020
So thank you very much for watching and I'll see you guys in the next one.

111
00:06:53,050 --> 00:06:53,740
Thank you.

