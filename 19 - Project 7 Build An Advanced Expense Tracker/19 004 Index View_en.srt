1
00:00:00,120 --> 00:00:06,660
So in this particular lecture, let's actually go ahead and let's start creating the form to serve this

2
00:00:06,660 --> 00:00:10,620
particular model so that we could accept data for that model from the user.

3
00:00:10,830 --> 00:00:17,190
So the very first thing that needs to be done is we have to create a view to actually render out a particular

4
00:00:17,190 --> 00:00:17,620
template.

5
00:00:17,640 --> 00:00:20,020
So let's create that particular view right away.

6
00:00:20,040 --> 00:00:26,560
So a name this view as index, because we want to render the form inside our index page itself.

7
00:00:26,580 --> 00:00:30,690
So this is going to accept a request and then we are going to render some templates.

8
00:00:30,690 --> 00:00:37,630
So return and render request as well as the template which we are going to create.

9
00:00:37,650 --> 00:00:42,190
So let's now go ahead and let's create a template inside my apps directory.

10
00:00:42,210 --> 00:00:48,060
So here inside there I'll create a new folder called as templates, and then I'll again create a new

11
00:00:48,060 --> 00:00:50,010
folder in there called as my app.

12
00:00:50,430 --> 00:00:56,140
So now once we have this my app folder created, let's actually go ahead and create a template in there.

13
00:00:56,160 --> 00:00:59,250
So let's name this template as and next to HTML.

14
00:00:59,670 --> 00:01:04,800
And for now, let's have some simple HTML code in here or with the hair and the body tag.

15
00:01:04,800 --> 00:01:08,790
And let's say for now, we want to say something like each one.

16
00:01:08,790 --> 00:01:10,980
This is a template.

17
00:01:11,100 --> 00:01:11,640
Okay?

18
00:01:12,450 --> 00:01:17,850
So now once we have this, our next job is to go ahead and render this particular template inside this

19
00:01:17,850 --> 00:01:18,220
view.

20
00:01:18,240 --> 00:01:19,920
So doing that is quite simple.

21
00:01:19,920 --> 00:01:24,720
I would say render the template, which is my app forward slash indexed or HTML.

22
00:01:25,590 --> 00:01:30,100
Now let's associate this particular index view with a URL pattern.

23
00:01:30,120 --> 00:01:36,660
Now you could directly associate this view inside the URL start pattern file of this my site, but let's

24
00:01:36,660 --> 00:01:38,580
create a URL start by for this.

25
00:01:38,580 --> 00:01:39,750
My app itself.

26
00:01:39,900 --> 00:01:41,820
So in here I'll create a new file.

27
00:01:41,850 --> 00:01:43,650
I'll call it as you are.

28
00:01:43,650 --> 00:01:44,610
Will start by.

29
00:01:45,060 --> 00:01:51,810
And in there we are going to take the URL patterns code from here, pasted up over here.

30
00:01:52,350 --> 00:01:57,960
And then for the index page, I'm going to say that I want to associate this with a view, which is

31
00:01:57,960 --> 00:01:58,910
the index view.

32
00:01:58,920 --> 00:02:03,960
So here I would say view start index and the name is going to be index.

33
00:02:03,960 --> 00:02:06,570
So now we have this URL pattern created.

34
00:02:06,630 --> 00:02:11,710
Now we need to associate this URLs dot py file with the main url start py file.

35
00:02:11,730 --> 00:02:17,670
So here I would import include so as to include the URL, which is the URL for my app.

36
00:02:17,730 --> 00:02:19,200
And I would say path.

37
00:02:19,200 --> 00:02:22,830
And then here I would actually include the path.

38
00:02:23,610 --> 00:02:26,190
Which is the my app dot URL.

39
00:02:26,220 --> 00:02:29,550
So here I would say I want to include the path, which is.

40
00:02:31,050 --> 00:02:32,610
My app dot.

41
00:02:34,090 --> 00:02:34,810
You are.

42
00:02:34,810 --> 00:02:38,140
Let's give a comma and we should be good to go.

43
00:02:38,860 --> 00:02:41,360
So now let's head back to the URLs.

44
00:02:41,360 --> 00:02:44,230
Start by file here and we are all set to go.

45
00:02:44,680 --> 00:02:51,010
And as we have used views dot index here, we also have to import views from the current directory itself.

46
00:02:51,040 --> 00:02:55,660
So here I would say from dot import views.

47
00:02:56,440 --> 00:02:58,250
So now we are all set.

48
00:02:58,270 --> 00:03:04,210
So let's go back to the browser and let's see what happens if I visit the index page.

49
00:03:04,210 --> 00:03:10,600
So if I visit the index page right now, as you can see, it sees this is a template which is all fine

50
00:03:10,600 --> 00:03:11,040
and good.

51
00:03:11,050 --> 00:03:16,450
So that means now we are able to render the template and here we could render the form as well to accept

52
00:03:16,450 --> 00:03:17,350
the expenses.

53
00:03:17,530 --> 00:03:22,360
But now, as you'll be able to see that there is currently no styling at all applied to this particular

54
00:03:22,360 --> 00:03:23,020
template.

55
00:03:23,020 --> 00:03:29,110
And what we need to do is we actually need to go ahead and we want to associate some sort of a styling

56
00:03:29,110 --> 00:03:31,360
and we will be using tailwind for that.

57
00:03:31,750 --> 00:03:36,790
So in the next lecture, let's go ahead and let's learn how to set up tailwind for our Django project

58
00:03:36,790 --> 00:03:39,040
by implementing just a couple of steps.

59
00:03:39,050 --> 00:03:43,060
So thank you very much for watching and I'll see you guys in the next one.

60
00:03:43,150 --> 00:03:43,900
Thank you.

