1
00:00:00,090 --> 00:00:03,190
Now let's work on making this edit form functional.

2
00:00:03,210 --> 00:00:08,340
So the first and foremost thing that needs to be done here is that we first of all, need to add a submit

3
00:00:08,340 --> 00:00:12,150
button, which actually submits that particular form for the edit.

4
00:00:12,480 --> 00:00:15,780
So in order to add a submit button, doing that is quite simple.

5
00:00:15,780 --> 00:00:20,730
I just have to go inside the form here, which is for edit and inside the form tag.

6
00:00:20,730 --> 00:00:28,470
Here I need to add a button and I want to set the type of this thing to submit and let's see this thing

7
00:00:28,470 --> 00:00:32,970
says edit data or make edits anything you'd like to call it.

8
00:00:33,150 --> 00:00:37,080
And then I need to say the form method needs to be post.

9
00:00:38,220 --> 00:00:40,590
And this is going to make a post request.

10
00:00:40,680 --> 00:00:43,020
And let's see how this kind of works out.

11
00:00:43,990 --> 00:00:49,360
So whenever a post request is submitted, this view is going to get triggered, which is the edit view.

12
00:00:49,570 --> 00:00:54,220
And whenever this view is triggered now we need to handle the post request for the edit view as well.

13
00:00:54,460 --> 00:01:00,520
So inside the post request we now need to go ahead and get the data from the expense form and kind of

14
00:01:00,520 --> 00:01:04,340
save that data or edit that data inside the database.

15
00:01:04,360 --> 00:01:11,320
So here I would say if request method, if this is equally equal to post in that particular case, do

16
00:01:11,320 --> 00:01:12,040
something.

17
00:01:12,340 --> 00:01:17,140
And the thing which we want to do is that we want to get the expense with an ID.

18
00:01:17,170 --> 00:01:22,360
So I would say expense equals expense dot objects, dot get ID.

19
00:01:22,390 --> 00:01:25,390
So essentially we want the same code which we have up over here.

20
00:01:25,390 --> 00:01:29,850
So instead of typing this thing one more time, I could simply take that up over here.

21
00:01:29,860 --> 00:01:33,910
Or you could also use this expense form directly as well if you need to.

22
00:01:33,910 --> 00:01:39,640
But in order to keep the logic simple, I'm kind of repeating a line of code here so as to make this

23
00:01:39,640 --> 00:01:40,810
code understandable.

24
00:01:41,200 --> 00:01:43,090
I could also remove this as well.

25
00:01:43,120 --> 00:01:45,940
But just for the sake of simplicity, I would do that.

26
00:01:46,150 --> 00:01:48,930
Now, similarly, I would also get the form as well.

27
00:01:48,940 --> 00:01:54,070
So I would say form equals expense form, get that particular expense form.

28
00:01:54,280 --> 00:01:58,360
And we are getting the form because we want the new data which is being passed here.

29
00:01:58,390 --> 00:02:04,660
So here I would say request dot post and also get the instances.

30
00:02:04,900 --> 00:02:08,590
So instance equals expense.

31
00:02:09,310 --> 00:02:12,960
And once we get this, as usual, we make it check if the form is valid.

32
00:02:12,970 --> 00:02:19,690
So if a form is underscore valid in that particular case, I simply want to form dot save.

33
00:02:20,510 --> 00:02:25,580
And then I want to return redirect to the index V, which we have.

34
00:02:25,790 --> 00:02:26,560
That's it.

35
00:02:26,570 --> 00:02:28,760
That's the only thing which we want.

36
00:02:29,150 --> 00:02:35,420
Now, we have not imported the redirect here, so let's import redirect from Django Shortcuts and we

37
00:02:35,420 --> 00:02:36,510
should be good to go.

38
00:02:36,530 --> 00:02:40,340
So now let's try editing a particular expense and see if that works.

39
00:02:40,350 --> 00:02:41,310
So I'll go back here.

40
00:02:41,330 --> 00:02:42,260
Hit refresh.

41
00:02:42,560 --> 00:02:44,630
Let's try editing the cost of an iPad.

42
00:02:44,660 --> 00:02:45,400
Let's see.

43
00:02:45,410 --> 00:02:47,750
We want to make it $300.

44
00:02:48,420 --> 00:02:51,540
And let's change the category to business.

45
00:02:52,140 --> 00:02:58,590
Now, if I click on edit Data, okay, we got an error because I think we forgot to add extra token

46
00:02:58,590 --> 00:03:01,860
here, so always make sure to add a CSR if token.

47
00:03:02,920 --> 00:03:06,410
Also see as RF underscore token.

48
00:03:07,460 --> 00:03:09,960
Let's try editing this data one more time.

49
00:03:09,980 --> 00:03:11,690
So I would again go back here.

50
00:03:11,690 --> 00:03:14,360
First of all, make sure that the data is not edited.

51
00:03:14,870 --> 00:03:15,880
Let's edit it.

52
00:03:15,890 --> 00:03:20,660
Change this thing to 300, Change it to business.

53
00:03:21,410 --> 00:03:22,580
Click on added data.

54
00:03:22,580 --> 00:03:26,000
And as you can see, the data for iPad has been changed.

55
00:03:26,180 --> 00:03:32,240
So it says that the new cost of an expense of iPad is 300 and it is a business expense.

56
00:03:32,570 --> 00:03:36,110
So that means the edit functionality is working absolutely fine.

57
00:03:36,110 --> 00:03:38,870
But now this form kind of looks dull and boring.

58
00:03:38,870 --> 00:03:44,360
So in the next lecture, let's actually work on styling up this form and kind of make it look a little

59
00:03:44,360 --> 00:03:45,080
bit better.

60
00:03:45,080 --> 00:03:49,310
So thank you very much for watching and I'll see you guys in the next one.

61
00:03:49,520 --> 00:03:50,000
Thank you.

