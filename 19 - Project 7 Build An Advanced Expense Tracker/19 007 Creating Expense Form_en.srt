1
00:00:00,090 --> 00:00:05,730
In this particular lecture, let's work on creating a form to save the data for this model, which means

2
00:00:05,730 --> 00:00:11,110
that whenever we have to save the data for a model via a form, we need to create a model form.

3
00:00:11,130 --> 00:00:16,890
So whenever you have to create a form for Django, you always go inside the app, which is in this case

4
00:00:16,890 --> 00:00:20,940
my app, and create a new file called as Form Stop Pie.

5
00:00:20,970 --> 00:00:26,700
And once you have this file created, let's go ahead and let's create a form for the expense model.

6
00:00:26,700 --> 00:00:30,710
So here I would call this form as expense form.

7
00:00:30,720 --> 00:00:34,700
And now this is not just a simple class, but it's actually a model form.

8
00:00:34,710 --> 00:00:37,200
So here I would say model form.

9
00:00:37,500 --> 00:00:41,580
Now you also need to import model form as well from Django Forms.

10
00:00:41,580 --> 00:00:47,250
So I would say from Django Dot forms, I want to import the model form.

11
00:00:47,250 --> 00:00:48,900
So once this is imported.

12
00:00:48,930 --> 00:00:51,960
Now we could go ahead and start adding some content to this form.

13
00:00:52,020 --> 00:00:56,820
So in this form, first of all, we need to mention that we are using the expense model.

14
00:00:56,820 --> 00:01:01,140
So we have to import the expense model as well from the current directory.

15
00:01:01,140 --> 00:01:05,519
So from models, I want to import the expense model.

16
00:01:06,230 --> 00:01:09,860
And now let's specify that model up over here in a class.

17
00:01:09,860 --> 00:01:11,540
So class matter.

18
00:01:11,570 --> 00:01:15,390
I want to say that the model which this is using is going to be expense.

19
00:01:15,410 --> 00:01:20,210
And now once we have specified the model, we also need to specify the fields which we want to accept

20
00:01:20,210 --> 00:01:21,080
from the user.

21
00:01:21,470 --> 00:01:27,260
So if you take a look at the model here, the model has the fields like name amount, category and date.

22
00:01:27,560 --> 00:01:32,750
Out of these fields we only want to accept the name amount and category and we are not concerned with

23
00:01:32,750 --> 00:01:33,430
the date here.

24
00:01:33,440 --> 00:01:35,610
So write up inside the form start p.

25
00:01:35,810 --> 00:01:43,580
I could specify the fields which I want by saying fields equals and then I want the name, I want the

26
00:01:43,580 --> 00:01:47,690
amount, and then I want the category.

27
00:01:48,080 --> 00:01:50,640
Once this is added, we are pretty much good to go.

28
00:01:50,660 --> 00:01:52,700
So now this form is created.

29
00:01:52,730 --> 00:01:58,550
Now we simply have to go ahead and somehow render this particular form on this particular index page,

30
00:01:58,550 --> 00:01:59,360
which we have.

31
00:01:59,780 --> 00:02:03,890
And the way in which we do that is we take this form and pass it to the view.

32
00:02:03,890 --> 00:02:08,520
And from the view we pass that particular form to the template which we have.

33
00:02:08,539 --> 00:02:10,490
So let's get the form in here first.

34
00:02:10,490 --> 00:02:17,810
So in order to get that form, I would say from dot forms, I want to import the expense form.

35
00:02:18,140 --> 00:02:22,880
And once we have the expense form, you have to create an instance of that particular expense form.

36
00:02:22,880 --> 00:02:28,220
Because remember that if you go to form sort pie, this expense form is nothing but a class.

37
00:02:28,220 --> 00:02:32,720
So that means you kind of have to go ahead and create an instance or an object out of it.

38
00:02:33,140 --> 00:02:39,110
So in here I would say expense form equals expense form.

39
00:02:39,900 --> 00:02:46,320
Now, once you have created that instance, you now have to pass that instance up over here as context.

40
00:02:46,320 --> 00:02:53,610
So here to add context, I would say expense underscore form as expense form.

41
00:02:54,730 --> 00:02:57,730
Once this thing is done, we are pretty much good to go.

42
00:02:57,910 --> 00:03:03,310
So now let's go to the file, which is index dot HTML and let's try it rendering this form.

43
00:03:03,400 --> 00:03:09,730
So we all know that whenever we have to render a specific form, we first add the form tag and then

44
00:03:09,730 --> 00:03:11,900
we add a CSR of token as well.

45
00:03:11,920 --> 00:03:18,400
So CSR of underscore token, and this time let's simply pass in the context here and render the form

46
00:03:18,400 --> 00:03:18,940
as SP.

47
00:03:18,970 --> 00:03:24,130
So here the name of the form which we are passing here is expense underscore form.

48
00:03:24,430 --> 00:03:26,920
And I would say dot as underscore p.

49
00:03:27,130 --> 00:03:30,330
So let's see what's going to be rendered on the index page now.

50
00:03:30,340 --> 00:03:32,530
So let's go back to the browser, hit refresh.

51
00:03:32,530 --> 00:03:37,780
And as you can see now, we have the expense form rendered up over here and we are good to go.

52
00:03:38,230 --> 00:03:43,300
Now, the problem with this form is that although this form is rendered, there is no proper styling

53
00:03:43,330 --> 00:03:45,430
to this form, even after using tailwind.

54
00:03:46,380 --> 00:03:49,710
And that's because we have rendered this entire form speech.

55
00:03:49,740 --> 00:03:54,630
And in order to style this particular form up, we actually need to go ahead and render each and every

56
00:03:54,630 --> 00:03:59,160
element which we have inside of the form and then add the styling to it.

57
00:03:59,250 --> 00:04:04,050
So in the next lecture, even before making this form functional, let's go ahead and let's style up

58
00:04:04,050 --> 00:04:05,910
this form in the way which we want.

59
00:04:05,910 --> 00:04:09,900
So thank you very much for watching and I'll see you guys in the next one.

60
00:04:10,140 --> 00:04:10,740
Thank you.

