1
00:00:00,090 --> 00:00:05,380
Now let's add some image icons to the buttons which we have up over here for the edit and delete.

2
00:00:05,400 --> 00:00:11,400
So in order to get those images which could be used as button icons, you simply go to Google search

3
00:00:11,400 --> 00:00:11,580
for.

4
00:00:11,580 --> 00:00:17,040
I can find the go to I can find a dot com and here you could search for the different icons which you

5
00:00:17,040 --> 00:00:17,550
want.

6
00:00:17,640 --> 00:00:23,520
So if I search for edit here and if I go on free, I should be able to get all the icons which are free.

7
00:00:23,850 --> 00:00:27,090
And out of this, we are going to, let's say, select this one.

8
00:00:27,090 --> 00:00:29,970
So I'll select this download this particular icon.

9
00:00:30,720 --> 00:00:34,290
And once that is downloaded now, I could go ahead.

10
00:00:35,210 --> 00:00:40,940
Take the icon, which is actually present in that particular folder and we have to paste it up in VTS

11
00:00:40,940 --> 00:00:42,700
code inside the static folder.

12
00:00:42,710 --> 00:00:48,380
So inside static and inside my app, let's create another folder called Last Images.

13
00:00:48,380 --> 00:00:52,310
And now let me get that particular icon which we have just copied from here.

14
00:00:52,310 --> 00:00:59,300
So let's get that and let's drag it from here and let's drop it right inside the images.

15
00:00:59,300 --> 00:01:02,030
So as you can see, we have the edit icon over here.

16
00:01:02,210 --> 00:01:07,700
Now, similarly, let's also go ahead and download an icon for delete as well.

17
00:01:07,700 --> 00:01:09,530
So I would search for delete.

18
00:01:10,340 --> 00:01:12,740
And you'll get a bunch of icons in here.

19
00:01:12,770 --> 00:01:15,860
So out of delete, let's say I want to choose this one.

20
00:01:16,340 --> 00:01:17,860
So let's choose this icon.

21
00:01:17,870 --> 00:01:19,760
Let's download this and P&G.

22
00:01:19,790 --> 00:01:26,960
Once that is downloaded, I could again open up VZ code, open this icon up, drag it, drop it right

23
00:01:26,960 --> 00:01:27,740
up over here.

24
00:01:27,890 --> 00:01:31,490
And now let's rename these particular icons to edit and delete.

25
00:01:31,790 --> 00:01:33,770
So I'll rename this one to edit.

26
00:01:34,490 --> 00:01:39,140
So this becomes red dot PNG and let's rename this thing to delete.

27
00:01:39,290 --> 00:01:39,830
Okay.

28
00:01:40,250 --> 00:01:45,590
So once we have these two icons, let's add them up over here for the edit as well as delete.

29
00:01:46,160 --> 00:01:49,420
So the edit here is nothing, but it's an H ref.

30
00:01:49,550 --> 00:01:55,490
So what we could do is that inside the ref tag, instead of saying edit, I want to add an image tag.

31
00:01:55,490 --> 00:01:57,500
So I would say image source.

32
00:01:57,950 --> 00:02:01,940
And as this is actually present in the static directory, I would say static.

33
00:02:02,660 --> 00:02:04,790
And it's present in the my app folder.

34
00:02:04,790 --> 00:02:10,880
So my app forward slash images forward slash the name of the image is added dot PNG.

35
00:02:11,450 --> 00:02:18,170
Now if I add that and if I go back to the browser, go back to the app, hit refresh or let's visit

36
00:02:18,170 --> 00:02:18,860
this page.

37
00:02:18,860 --> 00:02:24,920
Now you get an error because right now we are using static tag, but we also need to load static as

38
00:02:24,920 --> 00:02:25,250
well.

39
00:02:25,250 --> 00:02:27,980
So right here at the top I would simply say.

40
00:02:29,670 --> 00:02:30,690
Load.

41
00:02:31,260 --> 00:02:32,340
Static.

42
00:02:32,490 --> 00:02:34,220
Let's see if that works.

43
00:02:34,230 --> 00:02:36,870
So if I had a refresh, as you can see, we have that edit.

44
00:02:36,870 --> 00:02:38,190
I can add it up over here.

45
00:02:38,430 --> 00:02:41,680
Now, the size of this edit icon is quite a lot.

46
00:02:41,700 --> 00:02:48,060
So let's fix that by going inside that image source and let's set a class for this one.

47
00:02:48,330 --> 00:02:51,030
And let's see, the height of this thing is going to be five.

48
00:02:51,360 --> 00:02:54,870
Now, if I had refresh, as you can see now, the icon becomes small.

49
00:02:55,110 --> 00:02:57,300
Let's do that with the delete button as well.

50
00:02:57,930 --> 00:03:02,580
So for the delete button, we have the button here.

51
00:03:02,610 --> 00:03:08,910
So right inside the button instead of delete, I would again say image and the source is going to be

52
00:03:08,910 --> 00:03:12,570
the same one as this one, except for the image name.

53
00:03:13,020 --> 00:03:14,820
So let's copy this one.

54
00:03:15,180 --> 00:03:17,010
Let's paste the source here.

55
00:03:18,100 --> 00:03:20,080
Change this thing to delete.

56
00:03:21,180 --> 00:03:22,290
Dot PNG.

57
00:03:23,030 --> 00:03:25,530
Let's also set in the height to five.

58
00:03:25,550 --> 00:03:28,340
So h5fi Now go back.

59
00:03:28,370 --> 00:03:29,240
Hit refresh.

60
00:03:29,390 --> 00:03:32,260
As you can see now, we have icons for both of these.

61
00:03:32,270 --> 00:03:35,560
So this is how you could go ahead and add these icons up over here.

62
00:03:35,570 --> 00:03:41,540
So once these icons are added in the next lecture, let's go ahead and let's actually fix the alignments

63
00:03:41,540 --> 00:03:45,470
and also add some styling to the entire web page.

64
00:03:45,470 --> 00:03:47,200
So let's do that in the next one.

