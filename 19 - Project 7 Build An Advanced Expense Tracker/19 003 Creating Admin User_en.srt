1
00:00:00,090 --> 00:00:00,870
In this lecture.

2
00:00:00,870 --> 00:00:06,330
Let's go ahead and let's create a super user and populate some values in the expenses table.

3
00:00:06,480 --> 00:00:12,240
So in order to create a super user, you have to go inside the terminal and say, Python, manage,

4
00:00:12,240 --> 00:00:17,100
start by, create a super user hit enter.

5
00:00:17,100 --> 00:00:20,460
And that's actually going to ask you for the user name for super user.

6
00:00:20,760 --> 00:00:27,750
So I'm going to type in my name here and then let's type in a dummy email like demo at gmail.com.

7
00:00:28,170 --> 00:00:33,510
Let's type in a password as super user ID enter.

8
00:00:33,540 --> 00:00:41,770
Let's type in the password again as super user hit enter and the super user is now successfully created.

9
00:00:41,790 --> 00:00:45,480
So let's clear up the screen and let's run the server now.

10
00:00:45,480 --> 00:00:49,980
So I would say Python manage dot by run server.

11
00:00:50,010 --> 00:00:54,480
Now let's open up the link in the browser and let's go to the admin.

12
00:00:54,480 --> 00:01:01,290
So here I actually need to go to the URL forward slash admin.

13
00:01:01,770 --> 00:01:05,470
If I go over there, I will be presented with this login screen.

14
00:01:05,489 --> 00:01:10,320
So here let's type in the username as the name which I have currently entered.

15
00:01:11,550 --> 00:01:14,550
Let's type in the password as super user.

16
00:01:14,580 --> 00:01:20,370
Let's log in and as soon as you log in, you won't be able to see that particular model up over here.

17
00:01:20,370 --> 00:01:22,600
And there's a specific reason for that.

18
00:01:22,620 --> 00:01:27,360
And that's because whenever you create a model, you actually have to register that particular model

19
00:01:27,360 --> 00:01:29,220
and the admin dot py file.

20
00:01:29,460 --> 00:01:32,670
So the admin dot py file for my app is this one.

21
00:01:32,670 --> 00:01:35,040
So here you need to import the expense model.

22
00:01:35,040 --> 00:01:42,360
So here you need to say from DOT models, I need to import the expense model and then you need to register

23
00:01:42,360 --> 00:01:46,680
that model by saying admin dot ci dot register.

24
00:01:47,610 --> 00:01:50,280
And you're simply passing the expense model.

25
00:01:50,310 --> 00:01:53,710
So once that model is registered, it could go back here.

26
00:01:53,730 --> 00:01:57,990
Hit refresh and you should be able to see the expense model here as expenses.

27
00:01:58,380 --> 00:02:02,520
So now I could go to that model and I could add a brand new expense here.

28
00:02:02,910 --> 00:02:05,970
So here, let's say I have purchased an iMac.

29
00:02:06,000 --> 00:02:07,620
So I would see iMac.

30
00:02:08,070 --> 00:02:11,460
Let's say I bought it for around $2,000.

31
00:02:11,760 --> 00:02:13,590
So I would type in 2000 here.

32
00:02:13,590 --> 00:02:17,940
And as this is a business expense, I would say that the category is business.

33
00:02:18,300 --> 00:02:24,060
And now you'll be able to see that we no longer have to actually go ahead and set the date for this

34
00:02:24,060 --> 00:02:25,110
particular expense.

35
00:02:25,110 --> 00:02:29,760
And Jango is automatically going to take the current date and it's actually going to put it up over

36
00:02:29,760 --> 00:02:30,210
here.

37
00:02:30,390 --> 00:02:33,310
So let's try saving this and let's see what happens.

38
00:02:33,330 --> 00:02:37,660
So if I save this now, we have this expense object one created.

39
00:02:37,680 --> 00:02:43,230
So here, if I open this thing up, as you can see now, we have the name amount and category, and

40
00:02:43,230 --> 00:02:45,370
we also actually have the data as well.

41
00:02:45,390 --> 00:02:47,800
So we are going to display the date later.

42
00:02:47,820 --> 00:02:52,860
But for now, you'll be able to see that when I go to expenses, it says expense, object one.

43
00:02:52,950 --> 00:02:58,800
And instead of this, I actually want to show the actual name of that particular item which has been

44
00:02:58,800 --> 00:03:00,360
bought for that expense.

45
00:03:00,540 --> 00:03:06,050
So in order to do that, I could go to VTS code and write where I have actually defined my model.

46
00:03:06,060 --> 00:03:08,430
I could create a STR method.

47
00:03:08,430 --> 00:03:13,260
So I would say def double underscore str double underscore.

48
00:03:13,530 --> 00:03:19,690
Let's pass in self as a parameter and we want to return the name of that particular expense.

49
00:03:19,710 --> 00:03:21,960
So here I would say return the.

50
00:03:23,210 --> 00:03:25,100
Self self-taught name.

51
00:03:25,100 --> 00:03:31,220
And as soon as I do that and if I go back here it refresh now I will be able to see the name of that

52
00:03:31,220 --> 00:03:33,980
particular expense, which is meat, which is all fine and good.

53
00:03:34,100 --> 00:03:41,300
But now what we wish to do is we wish to actually go ahead and instead of adding the expense via this

54
00:03:41,300 --> 00:03:47,390
particular admin panel, what we want to do is create a form on our front ends to actually add this

55
00:03:47,390 --> 00:03:48,140
expense.

56
00:03:48,380 --> 00:03:51,560
So we are going to learn how to do that in the next lecture.

57
00:03:51,560 --> 00:03:56,540
And even before creating the form, we are going to learn how to set up tailwind for our current Django

58
00:03:56,540 --> 00:03:59,450
project so that we could properly style up that form.

59
00:03:59,540 --> 00:04:02,220
So let's learn how to do that in the next one.

