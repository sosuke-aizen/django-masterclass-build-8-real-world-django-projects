1
00:00:00,150 --> 00:00:00,930
In this lecture.

2
00:00:00,930 --> 00:00:03,800
Let's go ahead and style up this form which we have.

3
00:00:03,810 --> 00:00:08,940
So in order to style up this form, we will be making use of certain tailwind CSS classes, which we

4
00:00:08,940 --> 00:00:09,360
have.

5
00:00:09,780 --> 00:00:14,190
So let's first start off with the form tag itself and let's remove the action from here.

6
00:00:14,190 --> 00:00:18,720
And instead we want to say that the form method of this thing is going to be post.

7
00:00:18,720 --> 00:00:24,660
And now inside this form, right after the CSS RAF token, let's add a div and this div is going to

8
00:00:24,660 --> 00:00:26,680
act as a container for our form.

9
00:00:26,700 --> 00:00:31,200
So I'll name this thing as, let's say form container.

10
00:00:31,410 --> 00:00:35,810
And once we have added this, we are going to place the entire form elements in there.

11
00:00:35,820 --> 00:00:41,850
So for creating the form elements, what we will do is that we'll actually go ahead and create multiple

12
00:00:41,850 --> 00:00:43,860
divs for each element which we have.

13
00:00:44,070 --> 00:00:48,530
So for example, right now I now have to go ahead and add a label here.

14
00:00:48,540 --> 00:00:50,640
So for the label I will create a div.

15
00:00:50,670 --> 00:00:56,700
This is going to hold up our label, so I'll add a label here and let's say we remove this form and

16
00:00:56,700 --> 00:01:03,000
let's see, this label says expense name, because that's the first thing which we want to add here.

17
00:01:03,000 --> 00:01:07,710
And right after the label I will add another div and this div is actually going to contain the input

18
00:01:07,710 --> 00:01:09,570
field for the expense name.

19
00:01:09,960 --> 00:01:16,920
So therefore inside this div here, instead of saying expense form dot ASP, I would say expense form

20
00:01:16,920 --> 00:01:17,520
dot name.

21
00:01:17,520 --> 00:01:21,870
So I could simply cut this thing from here, paste this inside this div.

22
00:01:21,870 --> 00:01:26,510
And over here I would say name because the name of this field is name.

23
00:01:26,520 --> 00:01:33,000
So if you inspect this you'll be able to see that the name of this particular field is name.

24
00:01:33,000 --> 00:01:36,960
So you can actually reference that particular thing by saying something like this.

25
00:01:37,230 --> 00:01:42,570
So as soon as I do that, hit refresh, As you can see now we have expense name and we have then put

26
00:01:42,570 --> 00:01:43,560
field up over here.

27
00:01:43,560 --> 00:01:47,790
So once we have this, let's go ahead and let's start styling up this particular input field.

28
00:01:48,330 --> 00:01:53,400
So in order to style up this input field, I'll go ahead, go inside this div and here I would add a

29
00:01:53,400 --> 00:01:58,680
class and in order to add some styling I would use a class called Last model.

30
00:02:00,390 --> 00:02:03,870
And as you can see now, this particular input field has a border.

31
00:02:03,900 --> 00:02:09,060
Now, the border actually spans across the entire width here, but we are going to fix that very soon.

32
00:02:09,060 --> 00:02:15,330
But for now, the input field is ready and we are also going to remove this on focus effect of having

33
00:02:15,330 --> 00:02:16,400
a blue shade here.

34
00:02:16,410 --> 00:02:18,520
So we are going to learn how to remove that as well.

35
00:02:18,540 --> 00:02:21,120
But for now, let's focus on designing the form.

36
00:02:21,120 --> 00:02:25,960
So now let's go ahead and let's add some margin and padding to the form itself.

37
00:02:25,980 --> 00:02:31,770
So let's go inside the form and let's add a class and let's say the form needs to have some shadow.

38
00:02:31,770 --> 00:02:37,230
So I would say shadow dash log, which is a class which adds a larger shadow to the form.

39
00:02:37,410 --> 00:02:42,270
So if I hit refresh, as you can see, the form now has a certain amount of shadow to it.

40
00:02:42,300 --> 00:02:46,680
Now let's say I want a margin of ten from all the sites for this form.

41
00:02:46,680 --> 00:02:48,120
I would say ten.

42
00:02:48,540 --> 00:02:52,830
If I do that now, as you can see now, we have a margin of ten from all the sites.

43
00:02:52,860 --> 00:02:56,100
Now, let's actually make the edges of this thing to be rounded.

44
00:02:56,100 --> 00:03:00,360
And in order to do that, I would see rounded dash log.

45
00:03:00,630 --> 00:03:02,520
If I do that, go back.

46
00:03:02,910 --> 00:03:06,450
Now, the edges of this form are going to be slightly rounded.

47
00:03:06,480 --> 00:03:10,980
Now, once this form is style, let's go ahead and style the inner div, which is nothing but this form

48
00:03:10,980 --> 00:03:11,640
container.

49
00:03:11,670 --> 00:03:16,980
Now the form container is going to be inside the form and as you can see, the elements are stuck right

50
00:03:16,980 --> 00:03:17,870
to the left here.

51
00:03:17,880 --> 00:03:21,000
So let's add some padding from the x and y axis.

52
00:03:21,270 --> 00:03:25,170
So in here I would say the padding on the x axis should be ten.

53
00:03:25,290 --> 00:03:30,090
If I do that now, as you can see, it kind of shifted you a little bit, right?

54
00:03:30,090 --> 00:03:33,180
And from the right hand side, this shifted a little bit to the left.

55
00:03:33,720 --> 00:03:36,310
And now let's add some padding at the top as well.

56
00:03:36,330 --> 00:03:38,430
So padding top is, let's see, ten.

57
00:03:39,240 --> 00:03:43,120
And if you see that now we have padding from all the sides.

58
00:03:43,140 --> 00:03:46,200
Now, also, there's no space between the label and then put fields.

59
00:03:46,200 --> 00:03:48,740
So let's also add some margin at the bottom here.

60
00:03:48,750 --> 00:03:53,000
So in order to add some margin to the label, I would go ahead style up this div.

61
00:03:53,010 --> 00:03:57,820
I would say that I want the margin on the bottom to be five.

62
00:03:57,840 --> 00:04:03,330
So if I do that head refresh now, as you can see, there's some space here now along with the expense

63
00:04:03,330 --> 00:04:03,660
name.

64
00:04:03,660 --> 00:04:06,150
We also need to accept the amount and category.

65
00:04:06,150 --> 00:04:09,030
And let's say I want to place those particular input fields here.

66
00:04:09,030 --> 00:04:14,580
So in this case I would take this label and expense and I would add them inside another div.

67
00:04:14,880 --> 00:04:16,980
So I'll create one for each one.

68
00:04:16,980 --> 00:04:23,160
So I'll cut this from here, paste it in there, format it in a little bit better way.

69
00:04:24,150 --> 00:04:25,830
So as to avoid confusion.

70
00:04:25,830 --> 00:04:32,700
And once we are done with this, I would simply copy this, create another suggestive for the amount

71
00:04:32,700 --> 00:04:34,320
and another one for category.

72
00:04:34,590 --> 00:04:42,120
So here, instead of saying expense name, I would say amount and here I would say category and I would

73
00:04:42,120 --> 00:04:43,970
also change the input fields as well.

74
00:04:43,980 --> 00:04:48,510
So let's change this thing to amount because that's the field name inside the model.

75
00:04:48,510 --> 00:04:51,300
And here this should be category.

76
00:04:51,540 --> 00:04:56,640
Once we do that, hit refresh, as you can see now we have amount and category fields added up over

77
00:04:56,640 --> 00:04:57,060
here.

78
00:04:57,540 --> 00:05:03,960
But now you'll be able to see that right now, these particular fields which we have, they are actually

79
00:05:03,960 --> 00:05:08,290
placed on top of each other and we actually want to place them next to each other.

80
00:05:08,310 --> 00:05:14,390
So in order to do that, I should now go here and turn this outermost form container into a flex.

81
00:05:14,400 --> 00:05:20,520
So whenever you turn a drive into a flex, the child elements which we have up over here, they will

82
00:05:20,520 --> 00:05:23,510
be aligned next to each other and not on top of each other.

83
00:05:23,520 --> 00:05:29,940
So if I refresh now, as you can see, these elements are now aligned next to each other, but there's

84
00:05:29,940 --> 00:05:31,470
no space in between them.

85
00:05:31,680 --> 00:05:33,120
So let's add that.

86
00:05:33,120 --> 00:05:37,340
So these divs actually contain this entire unit.

87
00:05:37,350 --> 00:05:42,470
So I'll add a class which says I want a margin on the x axis to be ten.

88
00:05:42,480 --> 00:05:46,560
So let's add the same thing to all the other divs, which we have.

89
00:05:47,210 --> 00:05:50,500
And that should actually give us margin between them.

90
00:05:50,510 --> 00:05:53,420
So as you can see, it's working absolutely fine.

91
00:05:53,990 --> 00:05:59,180
Now, the final thing which is remaining is we now need to have a add button here, which actually allows

92
00:05:59,180 --> 00:06:01,970
us to add all of these to the database.

93
00:06:01,970 --> 00:06:05,460
So let's add the add button or the submit button at the end.

94
00:06:05,480 --> 00:06:08,900
So for that, I would go right where this events.

95
00:06:08,900 --> 00:06:10,220
I'll create another div.

96
00:06:11,100 --> 00:06:15,180
As usual, this is also going to have a margin X as ten.

97
00:06:15,420 --> 00:06:18,210
We are going to add some classes later if we need to.

98
00:06:18,630 --> 00:06:22,290
Let's add a button for now and let's see this thing sees ADD.

99
00:06:22,320 --> 00:06:26,160
So if I hit refresh, as you can see, that button is present up over here.

100
00:06:26,190 --> 00:06:27,960
Now we need to style it a little bit.

101
00:06:28,230 --> 00:06:31,530
So let's say the background of this thing needs to be green.

102
00:06:31,530 --> 00:06:35,310
So I would say class to add a green background.

103
00:06:35,460 --> 00:06:36,630
I would see.

104
00:06:38,030 --> 00:06:43,390
Big green with a shade of, let's say, 500.

105
00:06:43,400 --> 00:06:45,080
Now the background color is added.

106
00:06:45,440 --> 00:06:49,010
Now, as you can see, the name ad is kind of stuck to the button.

107
00:06:49,010 --> 00:06:50,890
So let's add some padding as well.

108
00:06:50,900 --> 00:06:54,920
So p x is going to be five and p Y is going to be two.

109
00:06:55,430 --> 00:06:58,670
I want to make the button rounded, surrounded LG.

110
00:06:59,210 --> 00:07:02,330
I want the text for the button to be white.

111
00:07:02,900 --> 00:07:08,000
So I would say text white and I want the fonts to be bold.

112
00:07:08,090 --> 00:07:12,440
So as soon as I do that, as you can see, the button is now added up over here.

113
00:07:12,980 --> 00:07:15,620
But still, there's actually no margin from the top.

114
00:07:15,620 --> 00:07:17,450
So let's add that as well.

115
00:07:17,570 --> 00:07:21,920
So here for this automotive, I would say margin on the top needs to be ten.

116
00:07:22,610 --> 00:07:26,480
If I had refresh now, the button is kind of aligned a little bit to the bottom.

117
00:07:26,900 --> 00:07:30,350
And it's even better if we make the margin to be eight.

118
00:07:30,830 --> 00:07:32,530
And this seems perfect.

119
00:07:32,540 --> 00:07:35,240
So as you can see, the form is almost ready.

120
00:07:35,240 --> 00:07:40,970
And we could now go ahead and add some data in here and we could click on submit.

121
00:07:40,970 --> 00:07:47,000
But right now, this form won't work because we have not handled the logic so as to what happens when

122
00:07:47,000 --> 00:07:48,170
the form is submitted.

123
00:07:48,410 --> 00:07:53,900
So let's make this particular form functional in the next lecture by handling the post request.

124
00:07:54,050 --> 00:07:58,040
So thank you very much for watching and I'll see you guys in the next one.

125
00:07:58,160 --> 00:07:58,820
Thank you.

