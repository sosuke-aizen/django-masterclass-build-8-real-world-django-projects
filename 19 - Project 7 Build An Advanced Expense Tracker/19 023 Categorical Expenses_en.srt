1
00:00:00,090 --> 00:00:06,000
In this lecture, Let's calculate the expenses across all the categories which we have up over here.

2
00:00:06,000 --> 00:00:08,880
Sum them up and show them inside a table here.

3
00:00:08,910 --> 00:00:10,590
So doing that is quite simple.

4
00:00:10,620 --> 00:00:13,950
First of all, we have to calculate the sum across all the categories.

5
00:00:13,980 --> 00:00:16,920
That means we have to go inside the views first.

6
00:00:16,920 --> 00:00:19,320
So let's go inside the view, start p file.

7
00:00:19,740 --> 00:00:22,500
And there's nothing different you need to do here.

8
00:00:22,620 --> 00:00:29,640
So just as we have calculated the sum across the past 30 days here, I simply need to copy the same

9
00:00:29,640 --> 00:00:30,180
thing.

10
00:00:30,180 --> 00:00:36,580
And instead of filtering as put the date, I now have to filter and order as per the category.

11
00:00:36,600 --> 00:00:40,770
So category is the field using which we want to filter out this data.

12
00:00:41,130 --> 00:00:47,040
So, uh, let's change this thing to something like a categorical.

13
00:00:48,260 --> 00:00:52,790
Sums, and let's use the same variable name over here as well.

14
00:00:52,940 --> 00:00:54,740
Hopefully there's no typo there.

15
00:00:54,980 --> 00:00:59,270
And now I simply have to replace this date with category.

16
00:00:59,270 --> 00:01:03,250
And we also want to order this data by category as well.

17
00:01:03,260 --> 00:01:06,060
So let's add category over here and over here.

18
00:01:06,080 --> 00:01:12,620
So now I'll remove the print here and we only want to test the categorical sum if that's actually correct

19
00:01:12,620 --> 00:01:13,970
and see if it works.

20
00:01:13,970 --> 00:01:17,030
So now I could go back, make a guest request here.

21
00:01:17,900 --> 00:01:20,990
After making a guest request, I could go back to the terminal.

22
00:01:20,990 --> 00:01:25,190
And here, as you can see, the sum of business expenses.

23
00:01:25,190 --> 00:01:32,750
128 The sum of food expense is 35 bucks and the sum of personal expenses one, three, three, three.

24
00:01:32,750 --> 00:01:35,600
And that's actually correct if you calculate that.

25
00:01:35,900 --> 00:01:38,400
So let's try making one more expense here.

26
00:01:38,420 --> 00:01:40,850
Let's say we want to make a business expense.

27
00:01:41,210 --> 00:01:44,860
Let's say we buy a car and include it as a business expense.

28
00:01:44,870 --> 00:01:50,330
Let's say the car costs $25,000 category is, let's say business.

29
00:01:50,900 --> 00:01:51,920
Add that up.

30
00:01:52,940 --> 00:01:55,850
And now we have another business expense here.

31
00:01:55,880 --> 00:01:57,170
Let's head back here.

32
00:01:57,560 --> 00:02:05,570
And here you'll be able to see that the sum of business expense has now increased to $153,000, which

33
00:02:05,570 --> 00:02:08,419
means that functionality is working absolutely fine.

34
00:02:08,780 --> 00:02:14,270
Now, the only thing that needs to be done here is that we need to take that particular expense, which

35
00:02:14,270 --> 00:02:18,530
we have just printed up and then just pass it as context to the template.

36
00:02:18,560 --> 00:02:21,050
So let's take the categorical sums.

37
00:02:22,340 --> 00:02:25,580
Go to the long list of contexts which we are passing here.

38
00:02:25,820 --> 00:02:27,860
So I'll add that up over here.

39
00:02:29,200 --> 00:02:35,920
And then we simply have to do the same thing as we have done here with the daily sums which we have.

40
00:02:36,160 --> 00:02:41,920
So we simply have to go ahead, go across all the categorical sums and display them up over here.

41
00:02:42,220 --> 00:02:49,390
So here I would simply copy this div, which is actually this left more stable.

42
00:02:49,420 --> 00:02:51,370
I would simply copy paste that.

43
00:02:51,970 --> 00:02:54,530
So copy this, paste it inside this div.

44
00:02:54,550 --> 00:03:02,410
Make sure that it's inside the flex div and then just replace it with categorical expenses.

45
00:03:03,240 --> 00:03:05,790
Pardon me of the spelling of this is wrong.

46
00:03:06,120 --> 00:03:09,600
And then I simply have to loop through the categories here.

47
00:03:09,840 --> 00:03:13,050
On the basis of this context, which I pass here.

48
00:03:13,500 --> 00:03:14,760
So I'll copy this.

49
00:03:14,790 --> 00:03:15,780
Go back here.

50
00:03:15,900 --> 00:03:17,280
Replace this with.

51
00:03:18,290 --> 00:03:21,440
Categorical sum and categorical sums.

52
00:03:22,350 --> 00:03:24,870
And see categorical some dot.

53
00:03:24,870 --> 00:03:31,860
And this time the name is actually nothing but it's actually category and some.

54
00:03:31,860 --> 00:03:33,900
So here I would say this is.

55
00:03:35,030 --> 00:03:36,410
Category instead of date.

56
00:03:36,410 --> 00:03:41,180
And this as it will remain some, but replace this with categorical sum.

57
00:03:41,840 --> 00:03:44,690
So once we are done with this, hopefully this should work.

58
00:03:44,690 --> 00:03:47,420
So I'll go back to the browser, make a get request.

59
00:03:47,420 --> 00:03:52,730
And as you can see now, we have the expenses which are actually made category wise.

60
00:03:52,730 --> 00:03:57,440
So that means the expense across categories and the daily expense sum is calculated.

61
00:03:57,440 --> 00:04:03,500
And now in the next lecture, let's go ahead and let's learn how we could visualize this data, which

62
00:04:03,500 --> 00:04:09,230
we have across categories and across 30 day expense show up in a visual format.

63
00:04:09,230 --> 00:04:12,020
So we are going to do that using charges.

64
00:04:12,020 --> 00:04:15,980
So thank you very much for watching and I'll see you guys in the next one.

65
00:04:16,100 --> 00:04:16,820
Thank you.

