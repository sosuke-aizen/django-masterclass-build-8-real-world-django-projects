1
00:00:00,060 --> 00:00:06,390
In this particular lecture, let's set up a basic template for adding the stylesheet link in the base

2
00:00:06,390 --> 00:00:07,020
template.

3
00:00:07,050 --> 00:00:12,390
So here, let's go ahead and let's create a new file and let's call it us based on HTML.

4
00:00:12,780 --> 00:00:17,970
And now we are simply going to take all the code which we actually have in the indexed HTML, simply

5
00:00:17,970 --> 00:00:21,480
cut it from here and paste all the code inside based on HTML.

6
00:00:21,510 --> 00:00:25,530
Now, this index, the HTML is actually going to inherit from the base template.

7
00:00:25,560 --> 00:00:32,250
So in here I'll delete the H one tag and I would add the block body and end block tag so that the inheriting

8
00:00:32,250 --> 00:00:35,490
template could actually place the code up inside these two tags.

9
00:00:35,490 --> 00:00:41,340
So here I would say block body, which means that the block body of that template would start here and

10
00:00:41,340 --> 00:00:42,600
it would end here.

11
00:00:42,870 --> 00:00:48,030
So I would see N block and I would go inside the indexed HTML.

12
00:00:48,030 --> 00:00:54,900
And first of all, as we want to inherit from the base template here, I would say I want to extend

13
00:00:54,900 --> 00:01:02,550
so extends from my app forward slash B stored HTML, and then I would add a block body tag.

14
00:01:03,460 --> 00:01:06,190
So block body.

15
00:01:07,090 --> 00:01:11,620
And then I would say and BLOCK okay.

16
00:01:11,800 --> 00:01:14,860
So now for a sample, let's see this thing.

17
00:01:14,890 --> 00:01:18,850
See, something like this is the index.

18
00:01:20,460 --> 00:01:21,360
Beach.

19
00:01:21,360 --> 00:01:25,370
And let's see if this and now let's see the kind of output we would get.

20
00:01:25,380 --> 00:01:27,870
So if I go to the browser, hit refresh.

21
00:01:28,440 --> 00:01:32,910
Now, as you can see, it sees this is the index page, which is this page being rendered.

22
00:01:32,910 --> 00:01:38,430
But you'll also notice that this index page now also has the styling, which is the tail when styling

23
00:01:38,430 --> 00:01:39,840
which is added up over here.

24
00:01:40,200 --> 00:01:45,300
Now, the advantage of using this base template is that no matter what other template you create here,

25
00:01:45,330 --> 00:01:49,230
that's always going to contain the tail CSS code, which we could use.

26
00:01:49,620 --> 00:01:54,690
And also you could add some components like a navigation bar to the base template, which is going to

27
00:01:54,690 --> 00:01:57,530
be added to each and every template which you create here.

28
00:01:57,540 --> 00:02:01,390
So once this base template is created, we are pretty much good to go.

29
00:02:01,410 --> 00:02:07,770
So now our job is to go ahead, take this particular model and create a particular form so that the

30
00:02:07,770 --> 00:02:12,210
data for this particular model could be accepted and stored into a database.

31
00:02:12,210 --> 00:02:16,740
So from the next lecture on what's let's work on creating a form for this model.

