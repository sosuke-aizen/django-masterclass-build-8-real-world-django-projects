1
00:00:00,090 --> 00:00:01,350
In this particular lecture.

2
00:00:01,350 --> 00:00:07,689
Let's go ahead and let's learn how we could calculate the sum of the expenses across all the dates.

3
00:00:07,710 --> 00:00:14,220
So the way in which we are going to calculate the sum is, let's say on this date, which is 22nd November,

4
00:00:14,340 --> 00:00:20,130
we have made these two expenses and the expense for that particular date should be combined and should

5
00:00:20,130 --> 00:00:23,730
be shown up together as the total expense for that particular date.

6
00:00:23,820 --> 00:00:26,130
So let's learn how to calculate that.

7
00:00:26,130 --> 00:00:31,320
So whenever we have to calculate total, we usually go inside the view start p file, which is this

8
00:00:31,320 --> 00:00:32,400
file right up over here.

9
00:00:32,400 --> 00:00:35,910
And in this, just as we have calculated the total sum up over here.

10
00:00:35,910 --> 00:00:38,960
So the total sum for was for all the expenses.

11
00:00:38,970 --> 00:00:45,630
But here we want to get all the expenses, but we only want to add expenses as per the date on which

12
00:00:45,630 --> 00:00:46,310
they are made.

13
00:00:46,320 --> 00:00:49,160
So let's go ahead and let's implement that logic up over here.

14
00:00:49,170 --> 00:00:50,850
So first of all, I'll get the data.

15
00:00:50,850 --> 00:00:55,560
So I would say data is going to be equal to nothing but expense.

16
00:00:56,860 --> 00:01:01,780
Dot objects, dot filter.

17
00:01:02,290 --> 00:01:08,440
And here we are basically going to get all the values and then I'm going to say get the values on the

18
00:01:08,440 --> 00:01:09,290
basis of data.

19
00:01:09,310 --> 00:01:12,040
So this is actually going to fill down the basis of date.

20
00:01:12,220 --> 00:01:19,450
And once we have these values or the date values, we then go ahead and order these values up by date.

21
00:01:19,570 --> 00:01:26,560
So along with filtering those values by date, I also want to order the values by the date.

22
00:01:27,160 --> 00:01:31,600
And here what we have done is that we have calculated the sum of all the expenses.

23
00:01:31,600 --> 00:01:34,410
That is, we have aggregated all the expenses.

24
00:01:34,420 --> 00:01:39,190
But here in this particular case, instead of aggregation, we want to perform annotation.

25
00:01:39,190 --> 00:01:44,410
And what annotation does is that it kind of filters up the expenses, which we have made as per the

26
00:01:44,410 --> 00:01:45,330
date which we have.

27
00:01:45,340 --> 00:01:49,360
So here we see dot and note eight and then.

28
00:01:50,310 --> 00:01:53,970
Just as we have passed in some here, I would do the same thing.

29
00:01:53,970 --> 00:02:01,110
So I would say I want to call this thing as some and I would say some equals some of the amount, because

30
00:02:01,110 --> 00:02:04,670
amount is actually the total expenses which we have made.

31
00:02:04,680 --> 00:02:10,380
So once we have this particular data, let's save it into some sort of other variable like daily sums.

32
00:02:10,380 --> 00:02:18,240
So instead of saying data, I would say daily sums, and now this actually becomes an object list and

33
00:02:18,240 --> 00:02:22,950
let's see the kind of results we would get on actually printing up this object list.

34
00:02:22,950 --> 00:02:28,020
So I'll save this and let's print this thing up and see daily sums.

35
00:02:28,020 --> 00:02:31,640
So now let's hit the get request and see the kind of results we would get.

36
00:02:31,650 --> 00:02:34,640
So I have made a get request.

37
00:02:34,650 --> 00:02:36,360
Let's go to the terminal here.

38
00:02:36,900 --> 00:02:39,660
So if I go to the terminal here, as you can see.

39
00:02:40,820 --> 00:02:46,490
It kind of calculated the expenses made on those particular dates, and it has shown up those things

40
00:02:46,490 --> 00:02:47,090
as some.

41
00:02:47,340 --> 00:02:52,790
Now, the reason why this thing saves some here is because we have explicitly specified that we want

42
00:02:52,790 --> 00:02:55,490
to call it some in this particular case.

43
00:02:55,490 --> 00:02:59,060
And this one, it was showing up some other value name.

44
00:02:59,420 --> 00:02:59,760
Okay.

45
00:02:59,780 --> 00:03:08,180
So for the date 22nd November, the total which is showing up over here is this.

46
00:03:08,180 --> 00:03:18,890
So here, as you can see, we have date as 22nd November 2022, and some of the amount is $128,000.

47
00:03:18,890 --> 00:03:20,420
And that's actually correct.

48
00:03:20,420 --> 00:03:23,330
If you sum these values up, that's exactly what you get.

49
00:03:23,750 --> 00:03:26,690
And it has also calculated the sum for these as well.

50
00:03:26,690 --> 00:03:31,790
But as they only have one expenses, it kind of shows these values themselves.

51
00:03:31,790 --> 00:03:37,220
So now once we have these daily sum values, let's go ahead and display these daily sum values inside

52
00:03:37,250 --> 00:03:41,000
of a table inside our index HTML template.

53
00:03:41,000 --> 00:03:44,030
So in order to do that, I would go to index dot HTML.

54
00:03:44,450 --> 00:03:48,950
I will go right where the div for this one ends and I'll create another div.

55
00:03:48,950 --> 00:03:50,620
This is also going to be flex.

56
00:03:50,620 --> 00:03:57,740
So div class is flex and in this I would create one day which is going to span half of the width because

57
00:03:57,740 --> 00:04:03,740
we only want to show the 30 day sums on half of the width of this website.

58
00:04:03,830 --> 00:04:10,760
So here I would say this is going to be w-1 by two, which means it spans half.

59
00:04:11,180 --> 00:04:16,010
I want to add a shadow LG and I want to add a margin of ten.

60
00:04:16,519 --> 00:04:19,459
And in this I would again go ahead, create another div.

61
00:04:19,459 --> 00:04:22,070
This is going to be flex again.

62
00:04:22,070 --> 00:04:27,380
So div class is going to be flex, this is going to be flex wrap.

63
00:04:27,620 --> 00:04:35,240
Let's add some space X as 40 and let's make the font to be bold.

64
00:04:36,430 --> 00:04:41,080
Let's make the padding X to V 20 and let's make the padding y to be five.

65
00:04:41,980 --> 00:04:48,160
So once we have this now and this here we are going to add a spine tag and this is going to say something

66
00:04:48,160 --> 00:04:53,650
like past 30 days, some expenses.

67
00:04:54,720 --> 00:04:55,560
That's it.

68
00:04:55,590 --> 00:05:01,800
Now, once we have this or once we basically have this header after this, I would have an air tag.

69
00:05:01,800 --> 00:05:06,930
And after the air tag I would create another div up over here and that there is actually going to contain

70
00:05:06,930 --> 00:05:07,950
the 30 D table.

71
00:05:07,950 --> 00:05:12,630
So I would say a div, let's give it an ID as let's say 30.

72
00:05:13,840 --> 00:05:15,790
The table.

73
00:05:15,820 --> 00:05:17,830
We have not assigned a class for this one.

74
00:05:18,160 --> 00:05:23,680
And in this particular 30 day table, we basically want to display all the expenses which we have gotten

75
00:05:23,680 --> 00:05:25,390
from here, which is daily sums.

76
00:05:25,510 --> 00:05:30,220
So to get daily sums, I first have to pass in daily sums as contexte over here.

77
00:05:30,220 --> 00:05:33,040
And we have passed in a lot of context objects here.

78
00:05:33,460 --> 00:05:35,120
So let's pass one more.

79
00:05:35,140 --> 00:05:37,480
So daily sums as daily sums.

80
00:05:38,320 --> 00:05:43,600
So once we have passed this thing in, now let's go ahead and get the daily sum from the daily summary,

81
00:05:43,600 --> 00:05:44,440
which we have.

82
00:05:44,980 --> 00:05:54,590
So here, in order to get that, I would add a for loop so far daily sum in daily sums, I would say.

83
00:05:54,610 --> 00:05:56,410
And I would end for here.

84
00:05:57,220 --> 00:05:58,390
So and for.

85
00:05:59,220 --> 00:06:04,710
And in this we will create a div and the class for this one is going to be flex.

86
00:06:05,600 --> 00:06:12,980
Flex wrap, and I want the padding on the x axis to be 20 padding on the y axis to be five.

87
00:06:13,280 --> 00:06:16,220
And then inside this I would add a span.

88
00:06:16,220 --> 00:06:19,280
So we are going to do just as we have done up over here.

89
00:06:19,340 --> 00:06:24,860
And inside this particular span, I want to get the daily sums date and display the date first.

90
00:06:24,860 --> 00:06:33,200
So I would say daily, some date and after this or right after this day events, I have to create another

91
00:06:33,200 --> 00:06:33,740
div.

92
00:06:33,860 --> 00:06:41,270
So we will be having two divs here and even this div is going to have a class of flex and flex wrap

93
00:06:41,270 --> 00:06:44,090
as well and I would add a span.

94
00:06:45,440 --> 00:06:46,040
Class.

95
00:06:46,040 --> 00:06:50,570
I would make the text to be green with a shade of 500.

96
00:06:50,990 --> 00:06:55,400
And after showing up the date, I want to show up the sum in this case.

97
00:06:55,400 --> 00:07:01,160
So here, I'll copy this, paste it up over here, and simply replace this with some.

98
00:07:02,160 --> 00:07:05,040
So let's see the kind of result this kind of provides.

99
00:07:05,070 --> 00:07:07,150
So if I go back here, hit refresh.

100
00:07:07,170 --> 00:07:13,230
If I go down here, as you can see now, we have the expenses as well as the dates here, which is the

101
00:07:13,230 --> 00:07:15,510
past 30 days, some expenses.

102
00:07:15,690 --> 00:07:19,590
It's kind of misaligned because we have not added padding and margin here.

103
00:07:19,590 --> 00:07:23,370
So p x is 20 and P by is five.

104
00:07:23,670 --> 00:07:24,000
Okay.

105
00:07:24,060 --> 00:07:25,590
So once we have fixed that.

106
00:07:26,250 --> 00:07:28,350
Now let's head back here, hit refresh.

107
00:07:28,380 --> 00:07:31,020
Now the expenses are shown up properly over here.

108
00:07:32,550 --> 00:07:34,650
You could also add a dollar sign here.

109
00:07:35,100 --> 00:07:40,260
And another thing which could be done here is that for these sums which you have, you could humanise

110
00:07:40,260 --> 00:07:46,410
them by adding a filter and you could see something like end comma, go back.

111
00:07:46,920 --> 00:07:51,330
Now you have the expenses displayed up over here along with the respective commas.

112
00:07:51,840 --> 00:07:58,050
So once we have this leftmost table, which kind of displays the past 30 days some, we could now go

113
00:07:58,050 --> 00:08:03,300
ahead and create a table on the right hand side, which shows the sum of amounts spans across all these

114
00:08:03,300 --> 00:08:04,110
categories.

115
00:08:04,740 --> 00:08:07,560
But even before that, let's test if this actually works.

116
00:08:07,560 --> 00:08:09,600
So I'll add an expense here.

117
00:08:09,600 --> 00:08:12,600
For example, let's say I would say pizza.

118
00:08:13,710 --> 00:08:16,680
Let's see, the amount is just $20.

119
00:08:16,710 --> 00:08:18,860
Let's say this falls in the food category.

120
00:08:18,870 --> 00:08:20,040
I would add that.

121
00:08:20,460 --> 00:08:26,910
So now we have pizza and food here, and that expense is added for today, which adds up to $20.

122
00:08:27,420 --> 00:08:30,210
Now, I could go ahead, add another expense like coffee.

123
00:08:30,240 --> 00:08:30,690
Let's see.

124
00:08:30,690 --> 00:08:32,460
This is also a food expense.

125
00:08:33,120 --> 00:08:33,659
Let's see.

126
00:08:33,659 --> 00:08:35,140
That is $15.

127
00:08:35,159 --> 00:08:36,179
Add that.

128
00:08:36,210 --> 00:08:37,380
Go back here.

129
00:08:37,409 --> 00:08:41,220
As you can see now, the total shows up to be $35 for today.

130
00:08:41,610 --> 00:08:44,430
That means this is working absolutely fine.

131
00:08:45,780 --> 00:08:51,330
Now, in the next lecture, let's actually sum up the expenses on the basis of their categories and

132
00:08:51,330 --> 00:08:53,910
display them up over here on the right hand side table.

133
00:08:54,000 --> 00:08:55,740
So let's do that in the next one.

