1
00:00:00,210 --> 00:00:01,020
In this lecture.

2
00:00:01,020 --> 00:00:03,700
Let's work on making this edit button work.

3
00:00:03,719 --> 00:00:09,570
So whenever you click a particular edit button, technically what should happen is you should be able

4
00:00:09,570 --> 00:00:15,030
to open up this particular expense on a new page or on a new form, and you should be able to edit the

5
00:00:15,030 --> 00:00:19,260
details for that particular expense, like the name amount and category.

6
00:00:19,380 --> 00:00:24,600
So in order to do that, first of all, you have to create a brand new view call as added.

7
00:00:24,600 --> 00:00:27,270
So let's open up Vsco right up over here.

8
00:00:27,300 --> 00:00:32,060
Go inside the view, start p file and let's create a new view here called Last Edit.

9
00:00:32,070 --> 00:00:33,840
So I would say def edit.

10
00:00:34,050 --> 00:00:39,750
And here this is going to accept a request and this is going to return and render a particular template

11
00:00:39,750 --> 00:00:41,220
which we are going to pass here.

12
00:00:41,220 --> 00:00:49,110
But for now, let's simply say my app slash edit dot, HTML and we have to work on creating that particular

13
00:00:49,110 --> 00:00:49,770
template.

14
00:00:50,130 --> 00:00:55,860
So this edit template, which we are going to have, it's simply going to render up the form which allows

15
00:00:55,860 --> 00:00:58,150
us to edit a particular expense.

16
00:00:58,170 --> 00:01:03,030
That means in order to implement the edit functionality, we first of all need to create a form just

17
00:01:03,030 --> 00:01:04,950
as we have this form up over here.

18
00:01:05,099 --> 00:01:10,560
Now, one best thing about Django is that you could still make use of the expense form which you have

19
00:01:10,560 --> 00:01:11,310
created here.

20
00:01:11,310 --> 00:01:15,630
So the expense form has created in form start pie, which is this form right here.

21
00:01:15,900 --> 00:01:22,560
And as of now, we are already using that particular expense form up over here and we are getting the

22
00:01:22,560 --> 00:01:25,130
data and kind of posting it to the database.

23
00:01:25,140 --> 00:01:29,910
Now, the same expense form which is being used here could also be used here as well.

24
00:01:30,270 --> 00:01:37,110
So in order to do that, you simply have to go ahead and say expense and let's go form and get the expense

25
00:01:37,110 --> 00:01:37,530
form.

26
00:01:37,560 --> 00:01:44,040
Now let's take this particular expense form and pass it to the HTML and see the kind of results we would

27
00:01:44,040 --> 00:01:44,490
get.

28
00:01:44,670 --> 00:01:47,790
So let's pass in the expense form here as context.

29
00:01:47,790 --> 00:01:49,740
So expense underscore form.

30
00:01:50,720 --> 00:01:52,730
I would say expense form.

31
00:01:52,880 --> 00:01:53,330
Okay.

32
00:01:53,330 --> 00:01:59,150
So now once we have passed in that expense form, let's go to my app and create a new template like

33
00:01:59,180 --> 00:02:00,530
edit dot, HTML.

34
00:02:01,070 --> 00:02:04,510
And as usual, this template is going to extend from the base template.

35
00:02:04,520 --> 00:02:09,169
So I would say extends my app forward slash based on HTML.

36
00:02:10,130 --> 00:02:13,910
And then I would have the usual block body and n block tag.

37
00:02:13,930 --> 00:02:16,160
So block body.

38
00:02:16,520 --> 00:02:19,250
And then finally, let's have the end block tag.

39
00:02:19,250 --> 00:02:20,960
So end block.

40
00:02:21,050 --> 00:02:21,560
Okay.

41
00:02:22,190 --> 00:02:26,750
So once we are done with this, now let's render up the expense form which we have up over here.

42
00:02:26,750 --> 00:02:31,790
So first of all, I would say form we won't add any kind of method here as of now.

43
00:02:31,790 --> 00:02:34,130
And now let's render the expense form.

44
00:02:34,130 --> 00:02:37,910
So I would say expense, underscore form.

45
00:02:38,180 --> 00:02:40,730
Now we have passed in the expense form.

46
00:02:40,730 --> 00:02:45,670
We have created this view as well, but now we need a URL pattern for edit.

47
00:02:45,680 --> 00:02:52,100
So let's temporarily set up a URL pattern for edit and for that I will actually go inside the URL start

48
00:02:52,100 --> 00:02:54,080
by file, which is this file right here.

49
00:02:54,380 --> 00:02:59,900
And just as we have a path for index, I will also create a path for edit as well.

50
00:03:00,230 --> 00:03:04,220
So let's see, this thing sees edit and we won't pass any ID here.

51
00:03:04,220 --> 00:03:09,140
As of now, I would simply save used edit and then name equals edit.

52
00:03:09,410 --> 00:03:11,300
We'll keep it simple as of now.

53
00:03:11,780 --> 00:03:17,000
Now let's see what happens if I go up over here and if I say forward slash edit.

54
00:03:17,540 --> 00:03:21,020
So if I see this, I get a form here which is all fine and good.

55
00:03:21,020 --> 00:03:23,270
And that's exactly what we wanted as well.

56
00:03:23,600 --> 00:03:29,720
But now this form which we get here, this form is empty, but whenever you are editing something out,

57
00:03:30,080 --> 00:03:34,620
technically what should happen is that you should be able to get the data which are editing as well.

58
00:03:34,640 --> 00:03:39,230
Now this kind of looks like a regular submit form, which we have here to submit a new expense.

59
00:03:39,230 --> 00:03:45,080
But if you want to edit the current expense, that means the expense which we are currently editing.

60
00:03:45,080 --> 00:03:47,630
That data should be populated up over here.

61
00:03:48,020 --> 00:03:53,090
So which means if I click on the edit button for iPhone, I should be redirected to this page with the

62
00:03:53,090 --> 00:03:59,900
data for iPhone filled up, which means that we now need to go ahead and get the particular expense

63
00:03:59,900 --> 00:04:01,640
on the basis of the ID.

64
00:04:02,210 --> 00:04:03,730
So let's do that as well.

65
00:04:03,740 --> 00:04:10,160
So that means I now have to return back to The View, which I have, which is in this case, this view

66
00:04:10,160 --> 00:04:13,970
right up over here, and I need to make this view except an ID as well.

67
00:04:14,630 --> 00:04:18,930
So now this ID belongs to the expense ID, which we want to edit.

68
00:04:18,950 --> 00:04:23,690
That means we now need to get that ID and get the expense from that ID.

69
00:04:23,720 --> 00:04:27,350
Now you would think from where exactly are you going to get that ID?

70
00:04:27,860 --> 00:04:31,400
So we want to get that ID from the URL pattern, which we have.

71
00:04:31,400 --> 00:04:34,760
So this right here is nothing but the URL pattern, which we are looking for.

72
00:04:35,150 --> 00:04:41,000
So the URL pattern should be designed in a way that it should be able to accept an ID of that particular

73
00:04:41,000 --> 00:04:42,680
expense, which we want to edit.

74
00:04:42,830 --> 00:04:48,800
Therefore, I would say forward slash and I want to make this thing accept an ID and the type of ID

75
00:04:48,830 --> 00:04:50,000
should be integer.

76
00:04:50,240 --> 00:04:54,830
So I would say int id give a slash and we should be good to go.

77
00:04:55,400 --> 00:05:02,510
So that means now whenever I need to edit an expense, I cannot simply just go up over here like that.

78
00:05:02,510 --> 00:05:07,250
Instead, I would say edit forward, slash one or let's say edit forward slash two.

79
00:05:07,250 --> 00:05:13,040
So if I say edit forward slash two, that's going to present me a form which allows me to edit the expense

80
00:05:13,040 --> 00:05:13,640
number two.

81
00:05:13,640 --> 00:05:19,610
So now once we have that ID from the URL pattern, we could take that particular ID and kind of get

82
00:05:19,610 --> 00:05:24,980
a specific expense from that particular ID and fill this form with that particular expense.

83
00:05:25,220 --> 00:05:30,380
And the way in which you would do that is you would go inside the view here and get the expense.

84
00:05:30,380 --> 00:05:37,910
So I would say expense equals take the expense model, get the objects and instead of saying objects

85
00:05:37,910 --> 00:05:43,820
dot all, I would now say object, start, get because I want to get an expense where the idea of the

86
00:05:43,820 --> 00:05:47,810
expense is equal to nothing but this ID which we have gotten from here.

87
00:05:47,810 --> 00:05:52,460
So this ID is nothing but the ID which you get from the URL pattern.

88
00:05:53,000 --> 00:05:58,910
So for example, if I type in one here, this one is now going to be passed up over here to this particular

89
00:05:58,910 --> 00:05:59,660
function.

90
00:05:59,660 --> 00:06:05,510
That one is going to be passed up over here, and this will get the expense where the ID of that expense

91
00:06:05,510 --> 00:06:06,200
is one.

92
00:06:06,860 --> 00:06:12,470
Now, once we have this expense, we will pass that expense as an instance to this form.

93
00:06:12,830 --> 00:06:18,440
So even if we get that expense, that expense now needs to be passed up over here to this form so that

94
00:06:18,440 --> 00:06:20,960
these form fields would be automatically filled.

95
00:06:21,470 --> 00:06:23,150
So in here, I want to say.

96
00:06:24,210 --> 00:06:27,660
The instance is going to be expense.

97
00:06:27,690 --> 00:06:29,150
Now let's see the magic.

98
00:06:29,160 --> 00:06:33,000
What happens if I see added forward slash one if I had to refresh?

99
00:06:33,330 --> 00:06:38,820
Now, as you can see, this form is now filled up with the first expense, which is iMac.

100
00:06:39,120 --> 00:06:45,120
Now the expense ID for iPad is three, which means if I go ahead and say edit forward slash three,

101
00:06:45,840 --> 00:06:48,390
I would get the edit form for iPad.

102
00:06:48,720 --> 00:06:50,680
And that's exactly what we have wanted.

103
00:06:50,700 --> 00:06:56,340
But now the thing is we are manually going ahead and we are manually typing in these IDs here.

104
00:06:56,640 --> 00:06:59,220
But instead we want to automate that process.

105
00:06:59,220 --> 00:07:04,950
And basically, when we click on one of these edit buttons here, we want to be redirected to those

106
00:07:04,950 --> 00:07:05,610
URLs.

107
00:07:05,610 --> 00:07:07,560
So doing that is quite simple.

108
00:07:07,980 --> 00:07:12,480
I just need to go inside the index dot html where we have the edit button.

109
00:07:12,480 --> 00:07:14,730
So here is where we have the edit button.

110
00:07:14,820 --> 00:07:20,700
And inside these edit buttons, I simply have to say that okay, whenever somebody clicks an edit button,

111
00:07:20,700 --> 00:07:23,760
I want to go to that specific URL here.

112
00:07:23,760 --> 00:07:26,700
The URL, which we want to go for is.

113
00:07:27,500 --> 00:07:30,620
Forward, slash, edit, forward, slash the idea of that expense.

114
00:07:30,620 --> 00:07:34,280
So in here to specify that URL, I'll make use of template syntax.

115
00:07:34,280 --> 00:07:41,660
And I would say I want a URL which is going to be nothing but the URL which we have just created, which

116
00:07:41,660 --> 00:07:44,930
is this URL and we have already named this URL as edit.

117
00:07:45,650 --> 00:07:52,340
That means I could now use that name up over here by saying edit and along with this edit the next parameter

118
00:07:52,340 --> 00:07:58,220
the URL needs to accept as actually the ID, which means I should also be able to get the expense ID

119
00:07:58,220 --> 00:07:58,730
as well.

120
00:07:58,730 --> 00:08:05,180
So just as we have gotten the expense name amount and date, I would say expense dot ID and this will

121
00:08:05,180 --> 00:08:06,560
give me the expense ID.

122
00:08:07,760 --> 00:08:12,650
So now once we have this, this URL is now formulated so I could save this.

123
00:08:12,680 --> 00:08:13,610
Go back here.

124
00:08:14,480 --> 00:08:16,250
Go back to the home page.

125
00:08:16,880 --> 00:08:18,260
Let's hit refresh.

126
00:08:18,800 --> 00:08:23,930
And now, if I inspect one of these elements or one of these edit buttons, you'll be able to see that

127
00:08:23,930 --> 00:08:27,110
they now have a specific URL associated with them.

128
00:08:27,590 --> 00:08:32,960
And now if I click on them, as you can see, if I click on the edit button for iMac, I get an edit

129
00:08:32,960 --> 00:08:34,130
form for iMac.

130
00:08:34,370 --> 00:08:39,909
If I click on the edit button for iPhone, I would get the edit form for iPhone, so on and so forth.

131
00:08:39,919 --> 00:08:41,780
And that's exactly what we wanted.

132
00:08:42,289 --> 00:08:48,260
Now the only thing that needs to be done here is that we should be able to change this and submit this

133
00:08:48,260 --> 00:08:53,210
particular form and the data should be submitted so the form should be submitted and the data should

134
00:08:53,210 --> 00:08:53,990
be edited.

135
00:08:54,380 --> 00:08:57,140
So let's work on that part in the next lecture.

