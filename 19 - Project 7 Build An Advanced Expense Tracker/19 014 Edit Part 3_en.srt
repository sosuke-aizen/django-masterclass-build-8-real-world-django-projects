1
00:00:00,150 --> 00:00:00,960
In this lecture.

2
00:00:00,960 --> 00:00:04,530
Let's work on styling up the expense form which we have.

3
00:00:04,680 --> 00:00:11,070
So in order to style this thing up, I have to go inside the HTML and it's already extending from the

4
00:00:11,070 --> 00:00:11,850
base template.

5
00:00:11,850 --> 00:00:16,720
But now I actually have to go ahead and dissect each and every field which we have up over here.

6
00:00:16,740 --> 00:00:19,860
So inside this form, first of all, I have to create a div.

7
00:00:19,950 --> 00:00:24,990
So I would say div and I want to assign some kind of padding and margin to it.

8
00:00:24,990 --> 00:00:28,080
So let's see, the padding on the x axis needs to be ten.

9
00:00:28,200 --> 00:00:30,750
The padding on y axis need to be ten as well.

10
00:00:31,080 --> 00:00:34,200
And now let's place all the input field inside the div.

11
00:00:34,770 --> 00:00:36,750
So I'll create a div.

12
00:00:36,780 --> 00:00:42,990
Each div is going to have a label, so I'll create a div for label and I would create another def for

13
00:00:42,990 --> 00:00:44,550
placing the actual input field.

14
00:00:44,550 --> 00:00:49,050
So this one is going to be for label and this one is going to be for the input field.

15
00:00:49,050 --> 00:00:54,930
So right inside this one I would add a label, so I would add a label, I would remove the fall from

16
00:00:54,930 --> 00:00:55,350
here.

17
00:00:55,350 --> 00:00:59,670
And for now let's say the first label is expense name.

18
00:00:59,670 --> 00:01:06,900
So I would say expense name here I would assign a class and make this label semi bold.

19
00:01:06,900 --> 00:01:10,950
So font dash, semi bold.

20
00:01:11,370 --> 00:01:15,750
And then for the next which we have, this is going to contain the input field.

21
00:01:15,930 --> 00:01:20,040
So we want to get the input field from the expense form which we have here.

22
00:01:20,040 --> 00:01:27,090
So here I would say expense form, expense, underscore form dot name, because that's actually the

23
00:01:27,090 --> 00:01:28,530
name of our first field.

24
00:01:28,620 --> 00:01:34,560
So let's get rid of this for now and let's actually see how the first label kind of looks like.

25
00:01:34,560 --> 00:01:36,570
So this is what it looks like as of now.

26
00:01:37,680 --> 00:01:42,870
So let's also add some shadow to the form as well so that the form kind of looks separated from the

27
00:01:42,870 --> 00:01:43,980
rest of the background.

28
00:01:43,980 --> 00:01:48,330
So shadow, let's make it a larger shadow.

29
00:01:49,870 --> 00:01:50,880
We have the shadow.

30
00:01:50,890 --> 00:01:54,700
Let's also add some margin from all the sites that say margin ten.

31
00:01:55,420 --> 00:01:57,000
This is what the form looks like.

32
00:01:57,010 --> 00:02:00,190
And let's also assign some classes to these deals as well.

33
00:02:00,370 --> 00:02:06,280
So for each label, which we have, I want to have a margin bottom of five because the label is kind

34
00:02:06,280 --> 00:02:08,620
of now stuck to this particular input field.

35
00:02:08,800 --> 00:02:12,880
So here I would say class, and I want to have a margin bottom of five.

36
00:02:13,670 --> 00:02:19,760
And for this div, which is for the label and the input field combine, I want the margin of x axis

37
00:02:19,760 --> 00:02:20,300
to be ten.

38
00:02:20,300 --> 00:02:22,160
So x is going to be ten.

39
00:02:22,940 --> 00:02:27,980
So if I do that now, it kind of has sufficient margin and padding from all the sides.

40
00:02:27,980 --> 00:02:31,250
And also for this input field, which we have, let's add some border.

41
00:02:31,640 --> 00:02:38,030
So I would say border and also let's assign a padding of one from all the sides.

42
00:02:38,980 --> 00:02:42,130
Okay, So now it kind of has a border as well.

43
00:02:42,460 --> 00:02:47,470
So now let's copy the same div, which we have for the rest of the field as well.

44
00:02:47,500 --> 00:02:52,780
So this one is going to be fun name this one is going to be for expense and category.

45
00:02:53,680 --> 00:02:58,840
So I would say expense and I would rename the last one to category.

46
00:03:00,390 --> 00:03:02,760
So here I would say expense amount.

47
00:03:03,030 --> 00:03:05,760
I would say expense category.

48
00:03:05,760 --> 00:03:10,830
And this thing should be I guess the filename is amount.

49
00:03:10,830 --> 00:03:12,990
So let's change that to amount as well.

50
00:03:12,990 --> 00:03:15,890
So expense from dot amount.

51
00:03:15,900 --> 00:03:16,320
Okay.

52
00:03:16,320 --> 00:03:20,820
So if I had refresh now, as you can see, it's looking absolutely fine.

53
00:03:20,820 --> 00:03:24,660
And we want to remove the blue borders here, but we are going to do that later.

54
00:03:25,410 --> 00:03:29,070
And let's also edit the button as well, which is a submit button.

55
00:03:29,370 --> 00:03:32,760
So here I'll go back a lot, some sort of classes here.

56
00:03:32,760 --> 00:03:38,070
But even before that, let's also include this thing in a div so that we could add some margin and padding.

57
00:03:38,400 --> 00:03:43,320
So class Mex, let's see, ten and empty is going to be, let's say ten as well.

58
00:03:43,320 --> 00:03:46,500
And then let's also style up this button by adding classes.

59
00:03:46,620 --> 00:03:50,350
So I want the background of this thing to be green with a shade of 500.

60
00:03:50,370 --> 00:03:55,140
I want the padding on the excess five padding on the y axis as two.

61
00:03:55,290 --> 00:04:00,270
I want the text to be white and I want the font to be bold.

62
00:04:00,930 --> 00:04:03,330
Okay, so now I could go back here, hit refresh.

63
00:04:03,330 --> 00:04:05,280
And this is what the button looks like.

64
00:04:06,140 --> 00:04:11,840
And also, let's see, you actually want to align these fields next to each other instead of having

65
00:04:11,840 --> 00:04:13,940
them aligned on top of each other.

66
00:04:14,270 --> 00:04:20,000
So there's one simple thing which you could do, and that thing is you could actually simply take these

67
00:04:20,000 --> 00:04:28,220
divs, which are the inner divs, cut them from here, paste them up over here inside like that.

68
00:04:28,640 --> 00:04:30,530
So I'll cut them all from here.

69
00:04:31,600 --> 00:04:34,380
Piece them inside the main door, which we have.

70
00:04:34,960 --> 00:04:36,760
Get rid of these two Divs.

71
00:04:38,110 --> 00:04:41,040
Place this button inside this one.

72
00:04:41,050 --> 00:04:45,730
And let's also get this div inside the first div combined.

73
00:04:47,200 --> 00:04:54,040
And once we have this, let's turn the outermost divide to a flex and see how that kind of works.

74
00:04:54,400 --> 00:04:56,880
So I would say flex it, refresh.

75
00:04:56,890 --> 00:04:59,890
And as you can see, this is what it looks like as of now.

76
00:05:00,040 --> 00:05:05,260
So this is what the form looks like and let it form, I guess looks pretty decent.

77
00:05:05,260 --> 00:05:09,370
And now our added functionality is complete and this form is fully functional.

78
00:05:09,370 --> 00:05:11,320
So I could add a new price here.

79
00:05:11,320 --> 00:05:13,210
I could add a new category.

80
00:05:14,170 --> 00:05:18,070
If I click on edit, that is going to work absolutely fine.

81
00:05:18,760 --> 00:05:24,310
So this completes the added functionality and in the next lecture, let's work on the delete functionality

82
00:05:24,310 --> 00:05:25,060
for this form.

83
00:05:25,330 --> 00:05:29,230
So thank you very much for watching and I'll see you guys in the next one.

84
00:05:29,260 --> 00:05:29,950
Thank you.

