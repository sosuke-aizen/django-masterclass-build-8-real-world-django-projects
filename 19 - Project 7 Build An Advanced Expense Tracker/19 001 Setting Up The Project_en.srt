1
00:00:00,590 --> 00:00:06,990
So from this lecture onwards, let's actually go ahead and start creating our expense track application.

2
00:00:07,010 --> 00:00:12,230
So the very first thing which needs to be done is we need to go ahead and create a container which is

3
00:00:12,230 --> 00:00:16,129
going to hold the virtual environment as well as our Django project.

4
00:00:16,340 --> 00:00:21,590
So let's create a new folder here on the desktop, and I'm actually going to name this thing as, let's

5
00:00:21,590 --> 00:00:23,940
say, expense tracker.

6
00:00:24,650 --> 00:00:30,290
And now let's open up this particular folder and terminal so that we could create the virtual environment

7
00:00:30,290 --> 00:00:32,030
as well as the Django project.

8
00:00:32,150 --> 00:00:38,870
So I'll open up a terminal here and I would go into the desktop by saying, See the desktop?

9
00:00:39,260 --> 00:00:41,960
And here let's go into the expense tracker.

10
00:00:41,960 --> 00:00:44,630
So see the expense tracker.

11
00:00:45,380 --> 00:00:51,260
So once we are in there here, we first of all need to create a virtual environment because after creating

12
00:00:51,260 --> 00:00:55,640
a virtual environment, we need to install Django and then create a Django project.

13
00:00:55,760 --> 00:00:57,350
So what shall the envy?

14
00:00:57,350 --> 00:00:58,070
Envy.

15
00:00:58,610 --> 00:01:03,110
And now, once the virtual environment is created, you'll be able to see that up over here.

16
00:01:03,110 --> 00:01:04,849
So let's clear up the screen.

17
00:01:05,390 --> 00:01:10,550
Now, the steps to activate virtual environment on windows are going to be a little bit different and

18
00:01:10,550 --> 00:01:12,410
they are covered in the previous sections.

19
00:01:12,410 --> 00:01:16,330
So if you are confused about that, make sure to take a look at that.

20
00:01:16,340 --> 00:01:22,370
But on a mac, what you need to do is once you have created the virtual environment, you have to go

21
00:01:22,370 --> 00:01:30,290
inside that environment by saying CD, E and V, and then you have to say source bean forward, slash,

22
00:01:30,290 --> 00:01:31,220
activate.

23
00:01:31,220 --> 00:01:34,610
And that's actually going to activate the virtual environment for you.

24
00:01:34,880 --> 00:01:38,720
So now let's go back to the main project directory, which is expense tracker.

25
00:01:38,990 --> 00:01:46,100
And now once the virtual environment is activated, let's install Django by saying PIP install Django

26
00:01:46,340 --> 00:01:49,580
hit enter, and that's going to install Django for you.

27
00:01:50,030 --> 00:01:52,430
So let me clear up the screen one more time.

28
00:01:52,550 --> 00:01:57,710
And now once Django is installed, we could finally start creating our Django project.

29
00:01:57,950 --> 00:02:01,070
So here I would see Django.

30
00:02:02,750 --> 00:02:06,500
Dash Admin Start project.

31
00:02:06,500 --> 00:02:09,740
And let's see, the name of our project is going to be my site.

32
00:02:09,860 --> 00:02:12,440
So I would simply type in my site here.

33
00:02:12,920 --> 00:02:14,000
Let's add enter.

34
00:02:14,030 --> 00:02:17,270
That's actually going to create a project called as my site.

35
00:02:17,360 --> 00:02:22,220
So if I type in LS, I will be able to see that we have the my site project here.

36
00:02:23,030 --> 00:02:25,010
Now let's go inside my site.

37
00:02:25,190 --> 00:02:29,340
And now here we actually need to create our very first Django app.

38
00:02:29,360 --> 00:02:36,410
So here I have to say Django Dash Admin Start app.

39
00:02:36,410 --> 00:02:39,360
And the name of our app is going to be, let's say my app.

40
00:02:39,380 --> 00:02:45,800
So to keep things simple, we will call the project as my site and we will call the app as my app.

41
00:02:46,040 --> 00:02:46,790
Hit enter.

42
00:02:46,790 --> 00:02:49,280
And that will create the my app for you.

43
00:02:49,730 --> 00:02:55,970
So let's open this entire project up and vsco and let's take a look at the directory structure.

44
00:02:56,240 --> 00:03:01,700
So I'll simply open up VTS code and then here I'll open up the expense tracker app which we have just

45
00:03:01,700 --> 00:03:02,460
created.

46
00:03:02,480 --> 00:03:09,290
So I'll open up the entire container and in here you'll be able to see that we have this virtual environment

47
00:03:09,290 --> 00:03:14,510
and we also have this my site directory in which we have my app as well as my site.

48
00:03:14,540 --> 00:03:20,660
Now, the very first thing that needs to be done is whenever you have created a new Django application,

49
00:03:20,780 --> 00:03:25,580
you have to include that particular application in the settings dot pie file up over here.

50
00:03:25,910 --> 00:03:32,270
So as we have created the my app, we need to include it inside the installed apps directory.

51
00:03:32,270 --> 00:03:33,770
So here I need to say.

52
00:03:34,470 --> 00:03:35,430
My app.

53
00:03:36,240 --> 00:03:39,320
Once that thing is done, we should be good to go.

54
00:03:39,330 --> 00:03:42,960
So now let's actually test our app by running the server.

55
00:03:42,960 --> 00:03:47,160
So python managed dot by run server.

56
00:03:48,450 --> 00:03:54,540
Now it seems that we have certain applied migrations, so we need to apply them by saying python manage

57
00:03:54,540 --> 00:03:55,950
dot p migrate.

58
00:03:55,950 --> 00:03:57,420
So let's do that as well.

59
00:03:57,420 --> 00:04:04,110
So I'll clear up the screen and I would say Python managed by migrate.

60
00:04:04,230 --> 00:04:07,860
Once I've done this, as you can see, the migrations are now made.

61
00:04:08,280 --> 00:04:14,940
So now once the migrations are made, let's run the server again by saying Python manage dot B by run

62
00:04:14,940 --> 00:04:15,510
server.

63
00:04:16,050 --> 00:04:21,079
Now the server is up and running, so let's open up this particular link in a browser.

64
00:04:21,089 --> 00:04:26,880
So here, as you can see, I've actually opened the Django project inside of a web browser and this

65
00:04:26,880 --> 00:04:28,080
is what it looks like.

66
00:04:28,080 --> 00:04:34,250
So that means now Django is up and running and we could start creating the functionality for our project.

67
00:04:34,260 --> 00:04:39,360
So from the next lecture onwards, let's go ahead and let's start working on the expense tracker app

68
00:04:39,360 --> 00:04:40,980
by creating a model.

69
00:04:40,980 --> 00:04:44,730
So thank you very much for watching and I'll see you guys in the next one.

