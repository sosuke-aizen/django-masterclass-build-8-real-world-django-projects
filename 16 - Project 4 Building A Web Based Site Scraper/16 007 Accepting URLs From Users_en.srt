1
00:00:00,090 --> 00:00:06,120
So in this lecture, let's actually go ahead and add up a formal here so that the user can submit his

2
00:00:06,120 --> 00:00:11,610
own request for a specific, you are so adding form over here is quite simple.

3
00:00:11,610 --> 00:00:15,510
You just need to go ahead, go into this template and add up a form over here.

4
00:00:15,990 --> 00:00:21,810
So even before we start creating a form up here, let's go ahead and actually create a proper bootstrap

5
00:00:21,810 --> 00:00:27,170
container, have the row and column inside it and please the table inside that container.

6
00:00:27,810 --> 00:00:32,110
So I'll see div class equals container.

7
00:00:32,369 --> 00:00:35,340
And in that container, let's actually have the row and column.

8
00:00:35,350 --> 00:00:37,100
So div class equals row.

9
00:00:38,160 --> 00:00:40,560
And then in there let's have the column in here.

10
00:00:40,570 --> 00:00:41,970
So the class equals.

11
00:00:41,970 --> 00:00:44,910
Let's call Dash and dash, let's see it.

12
00:00:45,690 --> 00:00:46,740
This should be formed.

13
00:00:47,790 --> 00:00:53,680
And now inside this let's actually place entire table which we have right here.

14
00:00:54,390 --> 00:00:58,050
So let's get that from here and place it inside over here.

15
00:00:58,890 --> 00:01:05,069
So now once we have this table, if you actually go back and hit refresh, as you can see, a table

16
00:01:05,069 --> 00:01:06,810
is placed in a much more better way.

17
00:01:07,650 --> 00:01:14,940
So now if I go ahead and want to create a form, I will go inside this container and then I'll create

18
00:01:14,940 --> 00:01:17,910
another div that is another room.

19
00:01:18,090 --> 00:01:24,730
So I'll type and of class equals two and then I can create a column inside here.

20
00:01:24,760 --> 00:01:29,620
So div class equals called Alghamdi Dash.

21
00:01:29,640 --> 00:01:32,610
Let's see for.

22
00:01:33,960 --> 00:01:39,570
So now in here, let's actually go ahead, create a form which has the input feel, which accepts the

23
00:01:39,570 --> 00:01:44,460
link from the user, so I can simply say something like form.

24
00:01:45,150 --> 00:01:52,180
And the method for this form is going to be equal to post and the action is going to be just a slash

25
00:01:52,200 --> 00:01:55,780
because we want to visit the home page back when the form is submitted.

26
00:01:56,190 --> 00:02:03,920
And now let's go ahead and add X out of token in here, so I'll type and see us out of underscore token.

27
00:02:04,290 --> 00:02:09,509
And now let's actually start creating the input class, soil type and input class equals.

28
00:02:10,020 --> 00:02:11,740
This is going to be form control.

29
00:02:11,760 --> 00:02:13,410
So this is nothing but the.

30
00:02:15,380 --> 00:02:25,310
Bootstrap class and the type of this is going to be taxed and the idea of this is going to be side and

31
00:02:25,310 --> 00:02:28,130
let's say the placeholder is going to be something like.

32
00:02:28,160 --> 00:02:30,950
And the site address.

33
00:02:33,450 --> 00:02:40,470
And the name of this thing is also going to be equal to sight, so I'll see name equals sight as well.

34
00:02:41,580 --> 00:02:44,610
So now let me just go ahead, close this particular tag.

35
00:02:44,910 --> 00:02:49,570
And after that, let's go ahead and close this specifically right here.

36
00:02:49,800 --> 00:02:53,880
So this the which we have created should actually end up over here itself.

37
00:02:55,030 --> 00:03:02,500
So now, once we end this debate, let's go ahead and create another column over here so I'll see live

38
00:03:02,500 --> 00:03:05,800
class equals call dash and dash one.

39
00:03:06,190 --> 00:03:08,920
And in here we are actually going to have the submit button.

40
00:03:09,640 --> 00:03:15,160
So we are still inside the form, but we are just separating out of tax so as to format this in a better

41
00:03:15,160 --> 00:03:15,390
way.

42
00:03:15,940 --> 00:03:17,610
So let's create a button in here.

43
00:03:17,740 --> 00:03:23,800
So the button class is going to be beaten Betty and dash primary.

44
00:03:25,370 --> 00:03:33,260
And the type of this button is going to be submit because this is going to submit a form and then let's

45
00:03:33,260 --> 00:03:39,260
name this button as scrap because it's going to scrape up for the links in a particular webpage.

46
00:03:39,890 --> 00:03:44,440
So now once this form ends, we'll go right here and create another column over here.

47
00:03:44,450 --> 00:03:49,750
So I'll say this class equals all dash and dash and I'll say seven.

48
00:03:51,140 --> 00:03:56,690
And in here I'll again create another button, which is the button to actually delete all the links

49
00:03:56,690 --> 00:03:58,400
which are present in our database.

50
00:03:58,400 --> 00:04:05,960
So it'll see e class equals button, button, dash warning.

51
00:04:07,250 --> 00:04:11,920
And the each for this is going to be, let's say, slash, delete.

52
00:04:11,930 --> 00:04:16,760
So we have not yet created the usual patterns for this, but we are going to create that pretty soon.

53
00:04:17,029 --> 00:04:18,820
So let's name this thing as delete.

54
00:04:19,370 --> 00:04:24,910
And now once we have this specific form, let's go ahead and see if we have that form up over here.

55
00:04:24,920 --> 00:04:29,220
So if I hit refresh, as you can see, we do have that form over here.

56
00:04:29,690 --> 00:04:33,070
And as you can see, it's there's not much margin over here.

57
00:04:33,080 --> 00:04:38,870
And in order to add up the margin to this form, I can simply go ahead and make use of the bootstrap

58
00:04:38,870 --> 00:04:39,320
class.

59
00:04:39,320 --> 00:04:43,760
So I'll see M five to add a margin of five on either side.

60
00:04:44,600 --> 00:04:46,090
I'll use this for the scroll.

61
00:04:46,100 --> 00:04:48,080
And now if I go ahead, hit refresh.

62
00:04:48,320 --> 00:04:51,940
As you can see, we still have some formatting issues over there.

63
00:04:52,130 --> 00:04:58,130
So let's actually go ahead, make this two and let's try to make this six and see if this thing works

64
00:04:58,130 --> 00:04:58,420
out.

65
00:04:58,820 --> 00:05:01,100
So now, as you can see, it looks much more better.

66
00:05:01,730 --> 00:05:07,850
So now once we are done with that, let's actually go ahead and make this one functional by modifying

67
00:05:07,850 --> 00:05:08,930
the view which we have.

68
00:05:09,470 --> 00:05:14,690
So currently, the view is only designed so as to go ahead and create these links right here.

69
00:05:14,690 --> 00:05:21,080
But now, instead of actually having to hardcourt this lingo here, we now actually want to get the

70
00:05:21,080 --> 00:05:22,490
link from the form.

71
00:05:23,030 --> 00:05:28,790
So in order to get anything from the form, when the form is submitted, we first need to make sure

72
00:05:28,790 --> 00:05:30,920
and check if the request method is post.

73
00:05:31,070 --> 00:05:36,890
So that means that whenever the form is submitted, we actually want to get the link which is submitted

74
00:05:36,890 --> 00:05:38,450
to this particular input field.

75
00:05:39,080 --> 00:05:44,930
So in here I need to type in if request dot method.

76
00:05:47,520 --> 00:05:50,490
Is equal, equal to post.

77
00:05:52,480 --> 00:05:59,200
Which means I now need to go ahead and actually get the site and the site is nothing but the parameter

78
00:05:59,200 --> 00:06:02,350
which the user has entered, so I'll type in site equals.

79
00:06:03,560 --> 00:06:12,710
Request dot post, dot get, and the name of the input field is nothing but site, soil type and site

80
00:06:12,710 --> 00:06:15,560
in here and the default for this is going to be empty.

81
00:06:16,430 --> 00:06:22,280
So now once we have the site, we now need to make sure to go ahead and pass this site over here to

82
00:06:22,280 --> 00:06:22,970
this page.

83
00:06:23,000 --> 00:06:27,700
So now you also need to go ahead and actually and then this code over here.

84
00:06:27,710 --> 00:06:31,310
So make sure that you go ahead and then this over here.

85
00:06:32,740 --> 00:06:40,120
So and then each and every line of code like that and in here now to this particular page, instead

86
00:06:40,120 --> 00:06:43,170
of passing this, I can now simply pass in sight.

87
00:06:44,590 --> 00:06:46,600
So this thing actually remains the same.

88
00:06:46,600 --> 00:06:52,220
We will still be using the Malpaso will still actually go ahead, loop through these elements and save

89
00:06:52,240 --> 00:06:52,970
them over here.

90
00:06:53,620 --> 00:07:01,000
But the one thing that changes over here is that once we have actually scraped the entire thing, we

91
00:07:01,000 --> 00:07:06,640
actually now want to go ahead and return back to the Web page, which is the current home page.

92
00:07:06,940 --> 00:07:14,170
So in order to do that, I can type in a return and we will use HDB response redirect, because obviously

93
00:07:14,170 --> 00:07:18,570
when the former submitted, we want to redirect the user back to the home page.

94
00:07:19,060 --> 00:07:23,260
So I need to first go ahead and import HTP response redirect.

95
00:07:23,290 --> 00:07:32,470
So I'll type in from Django dot htp import htp response redirect.

96
00:07:32,920 --> 00:07:39,400
So now once I have imported that I can just go ahead and make use of that over here as EDP response

97
00:07:39,400 --> 00:07:42,710
redirect and we actually want to redirect them to the home page.

98
00:07:42,820 --> 00:07:48,520
So we have written this blog over here and now we also want to write the ELDs blog as well.

99
00:07:48,670 --> 00:07:56,530
So I need to type in LS and if the request method is not equal to post, that means if we are just requesting

100
00:07:56,530 --> 00:08:02,620
for the home page and we have not submitted any data in that case, we actually want to go ahead and

101
00:08:02,620 --> 00:08:08,380
execute this code over here, which gets all the objects from our database and displays them on our

102
00:08:08,380 --> 00:08:08,950
home page.

103
00:08:09,130 --> 00:08:12,380
So we will have this particular code inside or else block.

104
00:08:12,400 --> 00:08:13,810
So that's all fine and good.

105
00:08:14,020 --> 00:08:17,060
We now have this view design completely.

106
00:08:17,170 --> 00:08:19,750
So now let's go ahead and see if this thing works.

107
00:08:21,130 --> 00:08:26,230
So if I go ahead, save the code and if we go back over here and now let's go ahead and first of all,

108
00:08:26,230 --> 00:08:38,039
hit refresh and now let's open HTP, colon or colon double forward slash w w w dot wiki padia dot org.

109
00:08:38,620 --> 00:08:43,659
And now if I click on scrape and now if I click on scrape as you can see.

110
00:08:45,420 --> 00:08:50,800
We will get the Wikipedia page script, but it will be present at the end over here.

111
00:08:50,820 --> 00:08:57,670
So as you can see now, we have links to Wikipedia all here and the different names of all those links.

112
00:08:59,310 --> 00:09:03,960
So now we also need to make sure that we make this delete button functional.

113
00:09:04,530 --> 00:09:09,190
So now, in order to make this delete button functional, first of all, let's go ahead and write a

114
00:09:09,190 --> 00:09:10,710
review for the delete method.

115
00:09:11,220 --> 00:09:19,530
So in here, I need to type in def and let's name this view as clear and it's going to accept a request.

116
00:09:19,890 --> 00:09:22,170
And the job of this view is simple.

117
00:09:22,170 --> 00:09:28,020
It just needs to go ahead and delete all the link objects so I can simply type and link dot objects.

118
00:09:29,170 --> 00:09:33,980
Not all, and I can use the delete method to delete all of those objects.

119
00:09:34,000 --> 00:09:35,350
It's as simple as that.

120
00:09:35,620 --> 00:09:38,110
And then I need to simply return a response.

121
00:09:38,110 --> 00:09:44,920
So I'll simply go ahead and render the template, which is nothing but existing HTML template so I can

122
00:09:44,920 --> 00:09:53,800
see my app slash Resiled dot html and that's it.

123
00:09:53,810 --> 00:09:56,100
You don't need to pass in any context over here.

124
00:09:57,080 --> 00:10:03,410
And also, this should be actually request, so now once we have that, the only thing which remains

125
00:10:03,410 --> 00:10:08,740
is that we need to now go ahead and actually create a real pattern for this.

126
00:10:08,840 --> 00:10:12,530
So if you go to you all start by filing here.

127
00:10:13,040 --> 00:10:18,410
And if you type in path and just type in the path, which is delete.

128
00:10:19,130 --> 00:10:24,610
So you need to type in delete, slash away here and you need to also mention the view.

129
00:10:24,620 --> 00:10:31,670
So if you start clear and the name of this view also is going to be let's be clear.

130
00:10:34,520 --> 00:10:40,340
So now once we go ahead and let's it refresh and when I click on the delete button, as you can see

131
00:10:40,640 --> 00:10:43,790
all the contents inside, ask people actually get deleted.

132
00:10:44,330 --> 00:10:45,170
So now let's see.

133
00:10:45,170 --> 00:10:52,580
I want to scrap any site soil type in https calling double forward, slash Google dot com.

134
00:10:52,940 --> 00:10:59,150
So when I click on scrape, as you can see it actually scrape all the links which we have and when I

135
00:10:59,150 --> 00:11:03,950
want to delete, I can simply click on this delete button and it will delete all the links which are

136
00:11:03,950 --> 00:11:04,850
present over here.

137
00:11:05,000 --> 00:11:11,950
And one last thing which I want to do here is that I just want to go ahead and give our site a header.

138
00:11:13,130 --> 00:11:14,990
So let's go ahead and do that real quick.

139
00:11:15,570 --> 00:11:16,640
So let's go ahead.

140
00:11:16,640 --> 00:11:20,020
And in here, let's create a row.

141
00:11:20,030 --> 00:11:20,780
So div.

142
00:11:22,070 --> 00:11:23,540
Plus equals through.

143
00:11:25,970 --> 00:11:33,650
And in here, let's create a column so div class equals, let's call dash and dash.

144
00:11:35,740 --> 00:11:36,190
Well.

145
00:11:37,180 --> 00:11:38,290
And let's see.

146
00:11:38,380 --> 00:11:45,220
This is going to also have a margin of five and in here, let's simply go ahead and create a header

147
00:11:45,220 --> 00:11:48,600
by using the H1 tag and let's see if this thing is linked.

148
00:11:48,610 --> 00:11:52,010
Let's just name it link collector.

149
00:11:52,240 --> 00:11:58,570
And now if you go ahead and hit refresh, as you can see, you now have the link collector over here,

150
00:11:58,570 --> 00:12:02,170
which looks pretty simple, but it performs the V, which you want.

151
00:12:02,980 --> 00:12:04,480
So that's it for this section.

152
00:12:04,480 --> 00:12:11,260
And I hope you guys enjoyed creating this link Skripal or Link Collector, so you can actually go ahead

153
00:12:11,260 --> 00:12:14,660
and modify the look and feel of the functionality of links.

154
00:12:14,660 --> 00:12:18,400
SKRIPAL As you want, so you can either tweak the design of this link.

155
00:12:18,400 --> 00:12:23,920
Skripal Or if you want, you can actually go ahead and make changes to the functionality of this link.

156
00:12:23,920 --> 00:12:26,840
Skripal in the user profile right here.

157
00:12:27,400 --> 00:12:31,120
So thank you very much for watching and I'll see you guys next time.

158
00:12:31,360 --> 00:12:31,960
Thank you.

