1
00:00:00,060 --> 00:00:06,540
So in this lecture, let's go ahead and learn how exactly can we execute this same code which we have

2
00:00:06,540 --> 00:00:14,890
typed in the console inside the actual app, which is nothing but the app, which is my app right over

3
00:00:14,910 --> 00:00:15,190
here.

4
00:00:15,540 --> 00:00:17,440
So let's go ahead and learn how to do that.

5
00:00:17,820 --> 00:00:20,420
So first of all, let's exit out of this console.

6
00:00:20,640 --> 00:00:27,090
So in order to exit out of this console, simply to open, exit and enter and you will be out of the

7
00:00:27,090 --> 00:00:27,690
console.

8
00:00:29,370 --> 00:00:37,380
So now let's go ahead and go inside the actual app and in here, let's go inside The View by and what

9
00:00:37,380 --> 00:00:43,440
our Python code, which you actually want to execute in your Django app, actually goes inside The View's

10
00:00:43,440 --> 00:00:44,400
top UI file.

11
00:00:44,820 --> 00:00:50,700
So here in order to execute any kind of code, first of all, you need to go ahead and create a view.

12
00:00:51,270 --> 00:00:56,010
And in this particular view, you can just go ahead and write in the code, which we have just written

13
00:00:56,010 --> 00:00:56,980
inside the shell.

14
00:00:57,780 --> 00:01:02,940
So even before we write some code in here, first of all, we need to import the libraries, which is

15
00:01:02,940 --> 00:01:07,280
nothing but the beautiful super library and the requests library.

16
00:01:07,620 --> 00:01:10,320
So I'll simply go ahead and type an import.

17
00:01:12,350 --> 00:01:21,950
Requests and then I can simply type in from wasteful import, that's going to be beautiful soup.

18
00:01:22,700 --> 00:01:28,220
So once we have these two things imported, we can now go ahead and create a function in here which

19
00:01:28,220 --> 00:01:32,000
will include the actual code to scrape those links.

20
00:01:32,000 --> 00:01:39,210
So I'll type in Dev and let's name this function as scrip and as with every function.

21
00:01:39,230 --> 00:01:41,420
This is also going to accept the request.

22
00:01:41,870 --> 00:01:46,160
And now in here we can directly go ahead and start writing a Python code.

23
00:01:46,520 --> 00:01:53,330
So first of all, we actually use the request library to get a particular webpage soil type and request

24
00:01:53,330 --> 00:01:58,090
that we get and then pass a Web page in here.

25
00:01:58,100 --> 00:02:00,960
So I'll simply go ahead and pass and Google over here.

26
00:02:01,640 --> 00:02:08,150
So HTP Escalon, double forward slash w w w dot Google dot com.

27
00:02:08,690 --> 00:02:13,780
And also let's make sure to actually see this page into something.

28
00:02:14,210 --> 00:02:15,770
So I'll see this in page.

29
00:02:16,070 --> 00:02:22,310
And now let's go ahead and use beautiful soup so that we can pass this page.

30
00:02:22,460 --> 00:02:27,880
So I'll type in Soup equals Beautiful Soup and Jespersen Page.

31
00:02:27,890 --> 00:02:35,410
And in order to actually get the text from this page, I'll type in page text and then passing the parser

32
00:02:35,420 --> 00:02:35,920
in here.

33
00:02:36,020 --> 00:02:39,370
So I'll type in a similar parser in here.

34
00:02:39,860 --> 00:02:44,510
And now once we have this specific thing, we are now actually good to go.

35
00:02:45,200 --> 00:02:50,690
So now the only thing which we need to do is that we need to go ahead and loop through the soup which

36
00:02:50,690 --> 00:02:52,340
we have, and get those links.

37
00:02:52,520 --> 00:03:04,250
So I can say for Link in soup, dot find and let's go all and let's find the links with e-TAG.

38
00:03:04,700 --> 00:03:08,660
And in here you can actually go ahead and print those links over here.

39
00:03:08,750 --> 00:03:13,750
But for now we don't want to print those links, but instead we actually want to display them on our

40
00:03:13,750 --> 00:03:14,180
Web page.

41
00:03:15,510 --> 00:03:21,900
So in order to display them on our Web page, let's actually go ahead and create a list over here and

42
00:03:21,900 --> 00:03:24,810
actually append those links to that particular list.

43
00:03:25,230 --> 00:03:31,350
So in here, let me just go ahead and create a list and let's see that list name is or let's see link

44
00:03:32,310 --> 00:03:33,090
address.

45
00:03:33,640 --> 00:03:38,910
And this list is going to be currently empty and we are going to fill that list up over here.

46
00:03:39,450 --> 00:03:45,900
So now, in order to append these links to this link address, you can type in a link address.

47
00:03:47,540 --> 00:03:56,960
Not a bend and then simply passing link, don't get and then get the eight Streiff.

48
00:03:58,370 --> 00:04:04,220
So now what this will do is that it will get all the link addresses and it will store those link addresses

49
00:04:04,220 --> 00:04:07,460
and to this particular list, which we have just created.

50
00:04:08,210 --> 00:04:14,030
Now, once we have these items in this list, we can actually pass in this particular list to the template

51
00:04:14,360 --> 00:04:17,579
so that we can render that template along with the list items.

52
00:04:17,899 --> 00:04:19,930
So let's go ahead and do that over here.

53
00:04:19,940 --> 00:04:29,960
So I'll type and return Rindo, then pass and request and then I'll simply go ahead and create a template.

54
00:04:30,440 --> 00:04:33,880
And that template is going to be present in my app Slash.

55
00:04:33,920 --> 00:04:43,700
And let's see, the template name is nothing, but let's see a result dot each shyamal and then I can

56
00:04:43,700 --> 00:04:52,640
pass in the context as nothing but the link address which we have so I can see a address as Lync address

57
00:04:52,820 --> 00:04:54,830
and then we are basically good to go.

58
00:04:55,610 --> 00:05:01,910
So now once we have actually passed this thing in, OK, so we do have an error over here and that error

59
00:05:01,910 --> 00:05:10,310
is we have used requests so here, which should actually be request because requests is actually a library.

60
00:05:10,580 --> 00:05:13,850
And this request which we use here, is the actual request.

61
00:05:14,690 --> 00:05:16,640
So make sure that you don't get confused.

62
00:05:17,210 --> 00:05:19,730
So once we have that, we are pretty much good.

63
00:05:19,970 --> 00:05:26,570
And now let's go ahead and actually create the template, which is nothing but result dot HDMI.

64
00:05:27,320 --> 00:05:29,570
So let's go ahead, go to my app.

65
00:05:29,810 --> 00:05:35,320
Let's create a directory which is called as templates.

66
00:05:35,330 --> 00:05:39,200
Let's create another directly in here, which is called as my app.

67
00:05:39,590 --> 00:05:46,250
And in there, let's go ahead, create a new file which is called As a result dot html.

68
00:05:47,210 --> 00:05:53,330
And now in here let's add the HTML tanks and in here I can simply go ahead and loop through the list,

69
00:05:53,330 --> 00:05:56,390
which is nothing but the list of link addresses.

70
00:05:56,810 --> 00:06:07,770
So I can see something like oh four link in the list which is nothing but link address and let's end

71
00:06:07,770 --> 00:06:08,900
the four over here.

72
00:06:11,500 --> 00:06:19,930
So I'll say and for Ananya, I can just go ahead and print out the link so I can see Linko here and

73
00:06:19,930 --> 00:06:23,740
let's use a breaktime over here to separate out all those links.

74
00:06:23,770 --> 00:06:29,200
OK, so now once we have this template and once we have associated this template with the CPU, the

75
00:06:29,200 --> 00:06:34,780
only thing which remains is that we now actually need to go ahead and associate Disp with the you are

76
00:06:34,780 --> 00:06:35,410
the pattern.

77
00:06:35,740 --> 00:06:37,750
So let's go ahead and do that as well.

78
00:06:37,990 --> 00:06:40,240
So let's go to the mine sites.

79
00:06:40,240 --> 00:06:41,560
You are a Stoppie.

80
00:06:42,100 --> 00:06:45,760
Let's import the view here so I'll see from.

81
00:06:47,840 --> 00:06:56,440
My amp import views, and once we have imported views, let's create a dual pattern for the same.

82
00:06:57,090 --> 00:07:01,330
So I'll see something like let's say we want to display this on our home page.

83
00:07:01,340 --> 00:07:05,330
So I leave this thing as empty and I'll see you start.

84
00:07:05,450 --> 00:07:11,000
And the name of of you is nothing but scrape we use to scrape.

85
00:07:11,240 --> 00:07:16,610
So the name of this is going to be also equal to, let's see, scrip.

86
00:07:17,000 --> 00:07:19,580
Simply give a comma and you are now good to go.

87
00:07:20,520 --> 00:07:27,240
So now let's see if we actually get those links on our home page, so I'll simply go ahead, save this

88
00:07:27,240 --> 00:07:30,480
code and make sure that the server is up and running.

89
00:07:30,480 --> 00:07:33,480
So I'll type in Python three managed at.

90
00:07:34,870 --> 00:07:41,810
Why run a server at Enter and now let me just open up the Web browser.

91
00:07:42,430 --> 00:07:49,300
So now as you can see, if I actually open up the Web browser, I do get all the links from Google over

92
00:07:49,300 --> 00:07:49,580
here.

93
00:07:50,200 --> 00:07:55,270
So as you can see, these are the specific links which are actually present on the Google's homepage.

94
00:07:55,840 --> 00:07:57,660
So now let's do one thing.

95
00:07:57,670 --> 00:07:59,850
Let's actually try to tweak this a little bit.

96
00:07:59,980 --> 00:08:07,360
So let's go ahead and let's change this thing to let's see Facebook dot com and let's see if we get

97
00:08:07,360 --> 00:08:08,800
a different set of links now.

98
00:08:09,250 --> 00:08:15,040
So now, if I go ahead and hit refresh, as you can see now, we got all the links which were actually

99
00:08:15,040 --> 00:08:17,240
present on Facebook's homepage.

100
00:08:17,650 --> 00:08:19,080
So that's all fine and good.

101
00:08:19,090 --> 00:08:21,000
We are actually getting these links here.

102
00:08:21,340 --> 00:08:27,550
But what we want to do is that we want to make an app where instead of actually hard coding this Google

103
00:08:27,550 --> 00:08:34,030
dot com or Facebook dot com over here, we actually want to ask the user to submit their own links so

104
00:08:34,030 --> 00:08:38,200
that we can actually skip that particular Web page for other links.

105
00:08:39,039 --> 00:08:42,260
So we are going to go ahead and do that in the upcoming elections.

106
00:08:42,490 --> 00:08:47,590
So in the upcoming lecture, what we will do is that, first of all, we will go ahead and try to save

107
00:08:47,590 --> 00:08:50,890
all these links inside our model.

108
00:08:50,890 --> 00:08:55,400
So we'll create a model so we will see if these particular links inside our model.

109
00:08:55,450 --> 00:09:00,670
And then after saving them in the model, we will actually go ahead and split them up over here.

110
00:09:01,450 --> 00:09:03,370
So thank you very much for watching.

111
00:09:03,370 --> 00:09:05,350
And I'll see you guys next time.

112
00:09:05,590 --> 00:09:06,160
Thank you.

