1
00:00:00,360 --> 00:00:07,080
So in this lecture, let's make our Web page look a little bit better by adding bootstrap and actually

2
00:00:07,080 --> 00:00:10,900
placing these links and the links name inside a bootstrap table.

3
00:00:11,460 --> 00:00:19,770
So in order to do the same, first of all, let's search for, download, bootstrap for and we actually

4
00:00:19,770 --> 00:00:22,860
want to get the bootstrap CDN link.

5
00:00:23,080 --> 00:00:28,170
So simply open up the official bootstrap site and just copy the bootstrap CDN.

6
00:00:28,650 --> 00:00:29,780
So I'll just go ahead.

7
00:00:29,820 --> 00:00:30,660
Copy that.

8
00:00:31,170 --> 00:00:37,520
And let's go back over here and let's go inside the template and paste that CVN link up over here.

9
00:00:40,390 --> 00:00:45,730
So now, once we have bootstrap, our site is going to look a little bit better than the previous version.

10
00:00:45,760 --> 00:00:51,730
So if I had to refresh, as you can see, the font improved a little bit, but now we actually want

11
00:00:51,730 --> 00:01:00,340
to go ahead and we actually want to use tables in here so I can simply search for people in here and

12
00:01:00,340 --> 00:01:02,620
let us see what kind of table options do we have.

13
00:01:03,100 --> 00:01:07,840
So now, if you scroll down a little bit, as you can see, we have different kinds of table options

14
00:01:07,840 --> 00:01:09,020
which we can choose from.

15
00:01:09,400 --> 00:01:13,530
So let's say you want to display the information in this specific format.

16
00:01:13,930 --> 00:01:18,970
So let's go ahead and actually copy this entire table, which we have.

17
00:01:19,390 --> 00:01:20,980
So let me just copy this.

18
00:01:21,180 --> 00:01:30,040
I let me go back to our code and in here, let's go ahead and paste this specific table right in here.

19
00:01:30,340 --> 00:01:37,330
And now what we want to do is that we actually want to go ahead and make use of this for loop to actually

20
00:01:37,330 --> 00:01:39,550
loop through this particular table.

21
00:01:39,970 --> 00:01:46,510
So the very first thing which we do is that, as you can see, we have these rules in here, so we'll

22
00:01:46,510 --> 00:01:48,610
get rid of the extra rules which we have.

23
00:01:48,730 --> 00:01:50,530
So let me just go ahead, delete that.

24
00:01:51,310 --> 00:01:54,600
And I guess the table is actually copied multiple times.

25
00:01:54,610 --> 00:01:57,910
So let me just also go ahead and get rid of that as well.

26
00:01:58,420 --> 00:01:58,780
OK.

27
00:01:58,810 --> 00:02:05,830
So now once we have this table with the table, had the table body and one rule, we are good to go.

28
00:02:05,860 --> 00:02:07,690
So this is what exactly we want.

29
00:02:08,259 --> 00:02:13,710
So now let's replace this with ID because we actually want to display the ID of our link here.

30
00:02:14,200 --> 00:02:19,990
And this should actually display the name of our link and this should actually have the actual link.

31
00:02:20,920 --> 00:02:24,160
And let's get rid of the fourth heading, which we have in here.

32
00:02:25,120 --> 00:02:33,160
And now the next thing which we need to do is that we have this table in here and in this specific table

33
00:02:33,160 --> 00:02:33,550
head.

34
00:02:33,700 --> 00:02:36,490
Let's go ahead and get rid of this table head over here.

35
00:02:36,610 --> 00:02:43,480
And now, instead of these three over here, we will actually go ahead and make use of the link in the

36
00:02:43,480 --> 00:02:45,000
linked name and link address.

37
00:02:45,520 --> 00:02:53,200
So in here I can simply type in link dot ID and in here I'll type in.

38
00:02:55,190 --> 00:03:05,780
Link, dot name and the last one which will use you is going to be link dot address, so I'll see link

39
00:03:05,780 --> 00:03:11,950
dot address and now we actually have access to these over here.

40
00:03:11,960 --> 00:03:16,390
But now the question arises, where exactly are we going to use this for loop?

41
00:03:16,970 --> 00:03:21,750
So in order to use this for loop, you actually need to place it right before the table.

42
00:03:22,670 --> 00:03:26,540
So please sit right over here before the table.

43
00:03:26,540 --> 00:03:33,110
Rootie actually starts and end the for loop exactly where the table row ends.

44
00:03:33,380 --> 00:03:35,150
So you have the table, Rohtak ends.

45
00:03:35,160 --> 00:03:41,210
So please the end for over here because we actually want to have multiple of these table rules inside

46
00:03:41,210 --> 00:03:41,760
our table.

47
00:03:42,380 --> 00:03:44,830
And now let's finally get rid of this thing.

48
00:03:44,840 --> 00:03:52,040
And now if you save the code and if you go back over here and hit refresh, as you can see now, you

49
00:03:52,040 --> 00:03:54,320
actually have all these links saved over here.

50
00:03:54,740 --> 00:03:58,430
So now, as you can see, our Web page actually looks much more better.

51
00:03:58,880 --> 00:04:04,640
But there's one issue with our current Web page is that if you keep on refreshing this Web page, these

52
00:04:04,640 --> 00:04:10,700
same links are going to be added up over and over because we are constantly visiting Google dot com

53
00:04:10,700 --> 00:04:13,320
and we are saving those same links in our database.

54
00:04:13,940 --> 00:04:19,220
So in the upcoming lecture, what we will do is that will actually go ahead and provide the input field

55
00:04:19,220 --> 00:04:24,860
over here so that user can actually enter a new you are a lawyer, which he wants to crawl.

56
00:04:25,220 --> 00:04:30,680
And we will also provide a delete button over here, which will actually delete the existing links which

57
00:04:30,680 --> 00:04:32,070
are present in our database.

58
00:04:32,570 --> 00:04:37,610
So if the user does not want any kind of old links in his database, he can just go ahead.

59
00:04:37,610 --> 00:04:43,860
And with a simple click of a button, all the old links inside a database will be deleted.

60
00:04:43,970 --> 00:04:46,820
So we will go ahead and learn that in the next lecture.

61
00:04:47,330 --> 00:04:49,220
So thank you very much for watching.

62
00:04:49,220 --> 00:04:51,170
And I'll see you guys next time.

63
00:04:51,590 --> 00:04:52,190
Thank you.

