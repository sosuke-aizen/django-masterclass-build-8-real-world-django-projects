1
00:00:00,240 --> 00:00:06,300
Hello and welcome to this lecture, and from this lecture onwards and this section, we will start building

2
00:00:06,390 --> 00:00:12,180
the Web Skripal, which is going to scrape a particular Web page and find those links.

3
00:00:12,840 --> 00:00:18,440
So even before we start with the project, you first need to go ahead, create a folder on your desktop.

4
00:00:18,450 --> 00:00:23,430
So I have created a folder which is named as Skripal here and in there.

5
00:00:23,430 --> 00:00:26,110
We will go ahead and set up the Shango project.

6
00:00:26,550 --> 00:00:32,490
So in this particular lecture, we will go through setting up the Shango project and we will also install

7
00:00:32,580 --> 00:00:38,550
the packages which we will actually need to use in that particular web scraper project.

8
00:00:38,820 --> 00:00:42,910
So let's go ahead, open up our terminal and create a virtual environment.

9
00:00:43,020 --> 00:00:45,180
So let me just open up the terminal.

10
00:00:45,810 --> 00:00:48,970
And in here, let's go to desktop saucily.

11
00:00:49,230 --> 00:00:51,060
Desktop slash.

12
00:00:52,460 --> 00:01:00,140
Scraper and in here, first of all, let's go ahead and create the virtual environment, so I'll see

13
00:01:00,140 --> 00:01:03,110
something like what Shuli and we N.V..

14
00:01:06,340 --> 00:01:12,010
And now once the virtual environment is created, we will go ahead, activate the virtual environment

15
00:01:12,310 --> 00:01:13,990
and install Django in it.

16
00:01:14,200 --> 00:01:18,250
So if I type in Ellis, as you can see, we have the virtual environment.

17
00:01:18,700 --> 00:01:20,800
So let's go to the virtual environment.

18
00:01:22,570 --> 00:01:24,550
And so, Ben.

19
00:01:25,610 --> 00:01:29,630
Ben Slash activate to activate the virtual environment.

20
00:01:29,690 --> 00:01:35,890
Okay, so now once the virtual environment is activated, our next job is to actually go ahead and install

21
00:01:35,900 --> 00:01:36,470
Shango.

22
00:01:36,500 --> 00:01:44,380
So let's first get out of the environment and let's dive and install Shango.

23
00:01:44,390 --> 00:01:46,930
And it's actually going to install Shango for us.

24
00:01:47,600 --> 00:01:56,510
And after installing Zango, the two packages which will be using in our application requests and beautiful

25
00:01:56,510 --> 00:01:56,860
soup.

26
00:01:57,020 --> 00:02:01,730
So let's first let it install Shango and we will discuss about the packages later.

27
00:02:01,980 --> 00:02:08,990
Okay, so now once Django is successfully installed, let's go ahead and let's create a Django project.

28
00:02:09,020 --> 00:02:10,789
So right now we're into the script.

29
00:02:11,030 --> 00:02:18,020
So let's create a Django project by typing in Django admin, start a project.

30
00:02:18,280 --> 00:02:20,410
Let's name this project as my site.

31
00:02:21,020 --> 00:02:23,230
And now let's add into that project.

32
00:02:23,240 --> 00:02:28,540
So I'll type in my site and now let's go ahead and create an API.

33
00:02:28,730 --> 00:02:32,560
So I'll see Django admin start up.

34
00:02:32,930 --> 00:02:35,000
Let's see my app.

35
00:02:36,560 --> 00:02:41,420
So now once we have created this app and once we have set up the project, let me just go ahead and

36
00:02:41,420 --> 00:02:42,930
open up a visual studio code.

37
00:02:43,040 --> 00:02:48,530
So now in Visual Studio could all simply open up the project, which we have just created, which is

38
00:02:48,530 --> 00:02:51,500
nothing but the project inside our script.

39
00:02:51,950 --> 00:02:55,790
So once I open this thing up, we are pretty much good to go.

40
00:02:56,760 --> 00:03:03,060
So now the very first thing which we need to do here is that we need to go to the mine site.

41
00:03:04,090 --> 00:03:08,980
And in here, in these settings, not be where you actually need to go ahead and add my app or here,

42
00:03:09,130 --> 00:03:12,730
so let me type in my app, give a comma and we are good to go.

43
00:03:13,670 --> 00:03:19,470
So now this is just basic triangle project creation and the Apsara process.

44
00:03:19,850 --> 00:03:22,040
Now as we are creating a Web scraper.

45
00:03:22,220 --> 00:03:25,130
We now need to learn how to install the packages.

46
00:03:25,640 --> 00:03:29,340
So in order to install the packages which we need, we will be using paper.

47
00:03:29,780 --> 00:03:35,410
So the very first package which we need for our Web Skripal is nothing but request.

48
00:03:36,110 --> 00:03:42,830
So requests is actually a package which allows us to send requests in our python code.

49
00:03:42,920 --> 00:03:48,650
So, for example, let's say if you actually want to access a Web page, so let's say if you want to

50
00:03:48,650 --> 00:03:53,090
access Google dot com in your Jianguo code or in your Python code.

51
00:03:53,210 --> 00:03:58,250
So for doing the same, what you can actually do is that you can go ahead and install requests and that

52
00:03:58,250 --> 00:04:04,660
requests package is going to help you to actually request for any kind of Web page which you want.

53
00:04:04,940 --> 00:04:13,160
So in order to install that particular package in here, I'll type in PIP install requests it enter

54
00:04:13,460 --> 00:04:17,089
and it's actually going to install the requests package for us.

55
00:04:21,649 --> 00:04:27,590
And as you can see, the request package has now been installed and another package which will be using

56
00:04:27,590 --> 00:04:30,200
over here is called as beautiful soup.

57
00:04:30,590 --> 00:04:37,500
So Beautiful Soup is actually a python library for pulling data out of HTML and XML files.

58
00:04:37,910 --> 00:04:43,970
So this request package is actually going to go ahead and extract the Web page contents or the HTML

59
00:04:43,970 --> 00:04:44,510
for us.

60
00:04:44,900 --> 00:04:52,010
But after that, you actually need to go ahead and get the data or pull out some data out of those HTML

61
00:04:52,010 --> 00:04:52,430
code.

62
00:04:52,430 --> 00:04:57,510
That is, you want to get the links, you want to get the different elements, so on and so forth.

63
00:04:57,890 --> 00:05:02,830
So for doing the same, you actually need to go ahead and also install beautiful soup.

64
00:05:03,200 --> 00:05:09,140
So in order to install beautiful soup, the beautiful super version which we will be installing is beautiful

65
00:05:09,140 --> 00:05:09,910
soup for.

66
00:05:10,040 --> 00:05:12,770
So you need to type and pip install.

67
00:05:15,040 --> 00:05:22,310
Beautiful soup for it enter, and it's actually going to install beautiful soup for you.

68
00:05:22,330 --> 00:05:28,660
So now in the upcoming lecture, even before we start writing some code in our Trango project, we will

69
00:05:28,660 --> 00:05:33,820
first learn how to use these two packages so as to get a particular Web page.

70
00:05:33,850 --> 00:05:35,350
So that's it for this lecture.

71
00:05:35,350 --> 00:05:37,300
And I'll see you guys in the next one.

72
00:05:37,600 --> 00:05:38,170
Thank you.

