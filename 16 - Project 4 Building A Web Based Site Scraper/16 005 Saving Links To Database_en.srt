1
00:00:00,420 --> 00:00:05,939
So now, in order to actually go ahead and see these links in our database, let's go ahead and create

2
00:00:05,939 --> 00:00:06,450
a model.

3
00:00:06,689 --> 00:00:09,150
So first of all, let's go ahead and stop the server.

4
00:00:09,390 --> 00:00:12,930
Then we can actually go ahead, go to the model, start by file.

5
00:00:13,350 --> 00:00:17,610
And in here I can just go ahead and start creating the model to save our links.

6
00:00:17,640 --> 00:00:20,550
So let's go ahead and type in class and let's see.

7
00:00:20,550 --> 00:00:25,800
We want to name this model as link because we are storing the link related information.

8
00:00:26,860 --> 00:00:33,520
So in the in this particular model, this is going to inherit from model store model and in there let's

9
00:00:33,520 --> 00:00:34,990
create only two fields.

10
00:00:34,990 --> 00:00:41,200
So let's say we want to field, which is nothing but the address, which is the address of a specific

11
00:00:41,200 --> 00:00:41,680
link.

12
00:00:41,710 --> 00:00:53,050
So let's type in model start Coffield and let's add the max length to, let's say a thousand and let's

13
00:00:53,050 --> 00:00:59,470
say the null value equals true, which means that the value of this particular field can be null.

14
00:00:59,620 --> 00:01:04,840
And the reason why we are setting this null equals two and blank equals true is because whenever we

15
00:01:04,840 --> 00:01:10,210
are scrolling the web page for certain links, certain links might not have a proper address.

16
00:01:10,210 --> 00:01:16,990
And that's why in order to avoid any errors, we have to actually mention null and blank as true for

17
00:01:16,990 --> 00:01:18,160
this field over here.

18
00:01:18,640 --> 00:01:21,280
And the same goes with the name field as well.

19
00:01:21,310 --> 00:01:24,610
So now we'll go ahead and create the name fillable here.

20
00:01:25,120 --> 00:01:30,910
And the same rules apply for this field as well, that, as you said, the max length equals 2000.

21
00:01:30,910 --> 00:01:34,270
And now you also said the null and blank equals true.

22
00:01:35,080 --> 00:01:41,290
And once we have that, let's go ahead, save the code and also let's have a string representation of

23
00:01:41,380 --> 00:01:42,450
this particular thing.

24
00:01:42,460 --> 00:01:44,680
So I'll see Def Astill.

25
00:01:46,600 --> 00:01:55,760
Self, and then we want to return self-taught name, and the reason why we have this is because whenever

26
00:01:55,780 --> 00:02:01,720
you actually refer to this object, you actually get the name of the link instead of getting just the

27
00:02:01,720 --> 00:02:03,250
object of link.

28
00:02:04,210 --> 00:02:08,289
So now once we go ahead and create this model, we need to make migrations.

29
00:02:08,289 --> 00:02:20,770
So I'll type in Python three managed by Make Migration's and then Python three matched up by my great.

30
00:02:21,880 --> 00:02:27,850
So as you can see, these migrations have been applied and now we need to go inside our view and we

31
00:02:27,850 --> 00:02:33,760
need to make sure that these links with The View actually Krall's, it needs to save all those links

32
00:02:33,760 --> 00:02:34,930
up into our model.

33
00:02:34,960 --> 00:02:38,810
So how exactly can we go ahead and save those links inside our model?

34
00:02:38,980 --> 00:02:44,640
So right now we are only extracting the link address using link dot cat.

35
00:02:44,800 --> 00:02:50,470
So now, instead of appending this to this specific list, you can actually go ahead, get rid of that.

36
00:02:51,970 --> 00:02:58,180
And instead, steer it into something which is called let's link address, also make sure that you get

37
00:02:58,180 --> 00:03:01,930
rid of this list right here and you can see something like.

38
00:03:03,890 --> 00:03:13,190
Link and let's go address equals link, don't get each ref and then you can type and link and the school

39
00:03:13,580 --> 00:03:20,240
text in order to get the text from that specific link, you can type and link dot string.

40
00:03:20,960 --> 00:03:27,470
And not once we have these two values, we can now go ahead and create an object out of these two values

41
00:03:27,470 --> 00:03:28,800
using our model.

42
00:03:28,850 --> 00:03:31,090
So first of all, you need to import this model.

43
00:03:31,550 --> 00:03:36,020
So you need to type in from models import.

44
00:03:36,380 --> 00:03:38,420
And the name of our model is Link.

45
00:03:38,750 --> 00:03:45,860
And now in order to create the object of that particular model, you need to type and link dot objects,

46
00:03:45,860 --> 00:03:47,150
dot create.

47
00:03:47,150 --> 00:03:49,540
And this will actually create the objects for you.

48
00:03:49,970 --> 00:03:52,550
And in here you just need to pass in the parameters.

49
00:03:52,850 --> 00:03:56,770
So you need to pass in the parameters, which is nothing but address and name.

50
00:03:56,780 --> 00:04:04,520
So you're the address is going to be nothing but Lincolnesque address and then the next parameter is

51
00:04:04,520 --> 00:04:05,630
the name.

52
00:04:05,900 --> 00:04:08,120
So you need to type and name equals.

53
00:04:09,290 --> 00:04:10,760
That's going to be nothing, but.

54
00:04:12,010 --> 00:04:13,520
Link and Lasco text.

55
00:04:13,540 --> 00:04:19,070
So now this will actually go ahead, create the objects for you and it will see it into your database.

56
00:04:20,440 --> 00:04:23,950
So now once you go ahead and save those objects, you're not good to go.

57
00:04:24,400 --> 00:04:30,820
But now the only thing which you need to take care of is that as now you have actually removed this

58
00:04:30,820 --> 00:04:36,340
link address right over here, you actually need to go ahead and you need to pass in some context in

59
00:04:36,340 --> 00:04:36,590
here.

60
00:04:37,150 --> 00:04:42,430
So you need to actually now go ahead and extract the data from the database even before you actually

61
00:04:42,430 --> 00:04:43,360
pass it over here.

62
00:04:43,930 --> 00:04:45,770
So let's go ahead and do that as well.

63
00:04:45,790 --> 00:04:53,350
So let's go out of this for loop and type in data to get the data and make use of the same model, which

64
00:04:53,350 --> 00:04:57,560
is link and get all the objects from that particular link object.

65
00:04:57,580 --> 00:04:59,050
So you need to type and link.

66
00:05:00,900 --> 00:05:10,800
Not objects at all, so you got all the objects from the link people and now you can pass on this specific

67
00:05:10,800 --> 00:05:13,090
data over here as the context.

68
00:05:13,110 --> 00:05:18,870
So you need to type in detail here and also personal data over here as well.

69
00:05:19,500 --> 00:05:25,020
Now, this data on now this particular object now has two things saved in it.

70
00:05:25,350 --> 00:05:30,540
So we not only have just the link address, but we also have the link text as well.

71
00:05:31,080 --> 00:05:36,960
So now once we have these two things, we also need to go ahead and now make modifications so here as

72
00:05:36,960 --> 00:05:37,230
well.

73
00:05:37,530 --> 00:05:43,260
So now, instead of having link in link address, I can now go ahead and type in for link in data.

74
00:05:44,070 --> 00:05:50,520
And now instead of just typing in link, you can now go ahead and access these two parameters, which

75
00:05:50,520 --> 00:05:51,910
is the address and name.

76
00:05:52,590 --> 00:05:58,040
So let's say you type and link dot name and right in front of it.

77
00:05:58,230 --> 00:06:05,220
Let's go ahead and also extract the address as well so I can type and link dot address.

78
00:06:06,240 --> 00:06:11,000
So this will now print both the name and address of each link on our template.

79
00:06:11,700 --> 00:06:14,190
So let's go ahead and see if this thing works out.

80
00:06:14,640 --> 00:06:20,750
So I'll just go ahead and let's try to visit the Web page for Google dot com itself.

81
00:06:20,760 --> 00:06:21,810
So I'll go ahead.

82
00:06:23,270 --> 00:06:30,890
Let me change this thing to Google dot com, and now if I save the code and hit refresh, first of all,

83
00:06:30,890 --> 00:06:32,510
I need to start the server.

84
00:06:32,570 --> 00:06:37,040
So Python three managed by one server.

85
00:06:37,310 --> 00:06:42,830
And we do have another way here, which is no module name models.

86
00:06:44,660 --> 00:06:51,470
That's because if you actually go to the views, they should actually be from DOT models.

87
00:06:51,650 --> 00:06:55,010
And once we fix that, the server should be up and running.

88
00:06:55,700 --> 00:06:57,920
And now let's go ahead and refresh.

89
00:07:01,010 --> 00:07:06,980
And as you can see now, we have all the links and we have them in a specific format, like we have

90
00:07:06,980 --> 00:07:13,040
the linked meme first, then we have this arrow and then we have the link address and we have this format

91
00:07:13,040 --> 00:07:15,260
for each one of the items over here.

92
00:07:16,070 --> 00:07:21,060
Now, let's see if we actually have these things saved in the back end.

93
00:07:21,080 --> 00:07:24,860
So in order to check that, let's create a super user.

94
00:07:25,070 --> 00:07:33,590
So it'll simply go ahead and type in Python three, managed by create super user.

95
00:07:34,940 --> 00:07:43,850
I leave it to my current thing, I'll add in some dummy email, let's say a demo at Gmail dot com.

96
00:07:44,180 --> 00:07:50,360
Let's see, the password is super user, super user.

97
00:07:50,750 --> 00:07:57,140
And then let's just go ahead, run the server, go back over here, go to admin.

98
00:07:58,980 --> 00:07:59,790
Just logon.

99
00:08:03,460 --> 00:08:09,010
And now we don't have the model over here because we need to register that in the admin, not by file.

100
00:08:09,010 --> 00:08:14,250
So I'll type in from not models import link.

101
00:08:14,800 --> 00:08:17,340
And now let's register this model over here.

102
00:08:17,350 --> 00:08:23,590
So I'll see admen dot, dot, dot register and then just pass and link over here.

103
00:08:24,280 --> 00:08:32,230
So now if I see that and now if I go here, hit refresh, if I go to lengths, as you can see now we

104
00:08:32,230 --> 00:08:34,450
have all these links saved over here.

105
00:08:34,990 --> 00:08:41,650
So now if I go to privacy, as you can see, we have the link for the privacy and the name of that link

106
00:08:41,650 --> 00:08:42,120
as well.

107
00:08:42,460 --> 00:08:47,560
And we have names of all these links and the links actually stored in there.

108
00:08:47,590 --> 00:08:52,870
So now once we have stored all these links in the back end, now in the upcoming lecture, let's actually

109
00:08:52,870 --> 00:08:59,050
go ahead and style this Web page in a much more better way so that these links are displayed in a proper

110
00:08:59,050 --> 00:09:00,100
tabular format.

111
00:09:00,640 --> 00:09:06,520
And after doing that, will actually add a input field over here, which will allow the user to actually

112
00:09:06,520 --> 00:09:07,660
enter the U.

113
00:09:07,660 --> 00:09:09,400
R, which he wants to sleep.

114
00:09:10,150 --> 00:09:13,840
So thank you very much for watching and I'll see you guys next time.

115
00:09:14,170 --> 00:09:14,710
Thank you.

