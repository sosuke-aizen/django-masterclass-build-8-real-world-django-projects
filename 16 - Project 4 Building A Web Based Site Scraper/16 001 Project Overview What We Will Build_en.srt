1
00:00:00,710 --> 00:00:06,170
So hello and welcome to this new section, so let's go ahead and have a look at what exactly are we

2
00:00:06,170 --> 00:00:08,510
going to build in this specific section.

3
00:00:08,520 --> 00:00:14,090
So in this section, we will build a link collector tool, which is actually a tool which will crawl

4
00:00:14,090 --> 00:00:16,700
up any webpage which you actually specify.

5
00:00:16,760 --> 00:00:22,050
And it's actually going to go ahead, collect all the links on that given webpage along with their names.

6
00:00:22,430 --> 00:00:26,400
So let's go ahead and see a quick demo of how this tool exactly works.

7
00:00:26,690 --> 00:00:33,770
So if I actually go ahead and enter a site address in here, so let's see if I want to put all the links

8
00:00:33,770 --> 00:00:35,180
on the Facebook homepage.

9
00:00:35,180 --> 00:00:39,040
I'll go ahead and type in the exact you are for that specific page.

10
00:00:39,470 --> 00:00:45,320
And now if I go ahead and click on script, it's actually going to go ahead and scrape all the data,

11
00:00:46,130 --> 00:00:49,420
all the links on that webpage along with their names.

12
00:00:49,430 --> 00:00:55,880
So as you can see, these are nothing but the names of those specific links and these are the exact

13
00:00:55,880 --> 00:00:56,350
links.

14
00:00:56,360 --> 00:01:01,900
So it will go ahead and crawl all those links up over here and save this information in the back end.

15
00:01:02,330 --> 00:01:06,080
And now let's see if I actually want to go ahead and delete this data.

16
00:01:06,080 --> 00:01:10,590
I can simply do that by clicking the single button and delete the entire data.

17
00:01:10,760 --> 00:01:16,840
And now if I want to call some other site, I can simply go ahead, click on that specific site.

18
00:01:16,850 --> 00:01:19,370
So let's see if I want to scroll Wikipedia.

19
00:01:19,370 --> 00:01:22,090
I can simply select that click on screen.

20
00:01:22,490 --> 00:01:27,080
And as you can see, it has created the Web page for Wikipedia as well.

21
00:01:27,350 --> 00:01:33,140
So as you can see, we have the names of the links as well as the exact links on the right hand side.

22
00:01:33,200 --> 00:01:37,200
So this is a tool which we will be building in this specific section.

23
00:01:37,220 --> 00:01:42,850
So in the next lecture, let's actually go ahead and start building this link collector to right away.

