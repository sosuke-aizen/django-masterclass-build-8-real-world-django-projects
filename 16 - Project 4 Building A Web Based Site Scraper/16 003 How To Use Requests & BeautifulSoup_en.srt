1
00:00:00,330 --> 00:00:06,510
So now once we have these packages installed, let's go ahead and learn how these packages actually

2
00:00:06,510 --> 00:00:06,880
work.

3
00:00:07,620 --> 00:00:14,700
So even before we actually go ahead and learn about these packages, let's learn how exactly can we

4
00:00:14,700 --> 00:00:19,800
write in some Python code inside the Django app, which we have right now.

5
00:00:20,430 --> 00:00:26,370
So right now we have created a Django project and we can actually open up visual studio code and we

6
00:00:26,370 --> 00:00:30,300
can actually open up that particular project and visual studio code.

7
00:00:30,510 --> 00:00:36,450
But even before that, I want to mention one thing over here, and that is how exactly can we go ahead

8
00:00:36,750 --> 00:00:41,670
and execute some python code in this particular terminal within our Django app.

9
00:00:42,240 --> 00:00:45,780
So as you can see, we are into the myside directory.

10
00:00:45,780 --> 00:00:51,540
And now if I just print out the contents of the directory, we have the managed by a file.

11
00:00:51,930 --> 00:00:57,960
And in here, if you actually want to write in and tell some python code, what you can simply do is

12
00:00:57,960 --> 00:01:01,440
that you can type in Python three managed by Shell.

13
00:01:02,070 --> 00:01:07,680
And when you go ahead and hit end, as you can see, it's going to start up the Python interactive shell

14
00:01:07,680 --> 00:01:08,160
for you.

15
00:01:08,640 --> 00:01:13,320
And in here you can actually go ahead and write in a bunch of code which you can execute.

16
00:01:13,590 --> 00:01:18,410
And this shell actually works just like any normal python shell would work.

17
00:01:18,900 --> 00:01:25,050
So in order to learn about the beautiful soup and the requests library, we are actually going to make

18
00:01:25,050 --> 00:01:26,270
use of these same shell.

19
00:01:26,880 --> 00:01:32,730
So you before we make use of those two libraries in our project, let's go ahead and learn those libraries

20
00:01:32,730 --> 00:01:34,020
using this python shell.

21
00:01:34,590 --> 00:01:38,360
So let's first learn about the requests library.

22
00:01:38,370 --> 00:01:46,220
So in order to use the requests library, you first need to go ahead and type in import import requests.

23
00:01:46,230 --> 00:01:52,470
And now once we have this imported, we can now go ahead and make use of this request library.

24
00:01:53,010 --> 00:01:58,560
So the request library is a library which allows us to request any web page which we want.

25
00:01:58,920 --> 00:02:06,240
So let's say, for example, you want to request Google dot com, you can simply type request doGet

26
00:02:06,720 --> 00:02:09,690
and then you can pass in the webpage, which you want.

27
00:02:09,690 --> 00:02:11,280
So I can type in over here.

28
00:02:11,580 --> 00:02:18,050
HTP Holen double forward slash w w w dot Google dot com.

29
00:02:18,450 --> 00:02:23,760
And now in order to actually store the result into something else, simply go ahead and type in page

30
00:02:23,760 --> 00:02:24,970
equals request.

31
00:02:25,470 --> 00:02:29,670
So the result of this particular function gets stored in the page.

32
00:02:30,000 --> 00:02:35,640
So now once I go ahead, hit enter and when I type in page, as you can see we get a response.

33
00:02:35,640 --> 00:02:36,210
Two hundred.

34
00:02:36,990 --> 00:02:43,260
So now all the contents of our Web page, which is the web page, Google dot com, are now stored in

35
00:02:43,260 --> 00:02:43,780
the page.

36
00:02:44,130 --> 00:02:52,710
So if I do page dot text and enter, as you can see, we got a bunch of each HTML code in here, which

37
00:02:52,710 --> 00:02:55,080
is nothing but the content of our Web page.

38
00:02:55,260 --> 00:03:01,770
Now, if you have a look over here, this HTML is quite messy and you won't make any sense out of this

39
00:03:01,770 --> 00:03:03,210
particular HDMI code.

40
00:03:03,480 --> 00:03:09,450
And henceforth, in order to obtain some important information from this particular Web page, we actually

41
00:03:09,450 --> 00:03:12,600
make use of another library, which is called as beautiful soup.

42
00:03:12,990 --> 00:03:19,560
So Beautiful Soup is actually a python library for pulling data out of HTML and XML files.

43
00:03:19,710 --> 00:03:24,090
So in here we already have a HTML page, which we have extracted.

44
00:03:24,360 --> 00:03:30,070
And now in order to actually go ahead and make sense of that page will make use of the beautiful soup

45
00:03:30,090 --> 00:03:30,720
library.

46
00:03:30,750 --> 00:03:32,870
So let's first import beautiful soup.

47
00:03:33,360 --> 00:03:38,040
So in order to import beautiful soup, you type in from B is for.

48
00:03:39,150 --> 00:03:41,430
Import, that's going to be.

49
00:03:42,990 --> 00:03:45,330
Beautiful soup.

50
00:03:46,230 --> 00:03:51,510
So now once you have this library imported, you can simply go ahead and make use of it so you can see

51
00:03:51,510 --> 00:03:58,890
beautiful soup and possin page dot text, which is nothing but the actual e-mail, which is the HTML,

52
00:03:58,890 --> 00:04:00,510
which we have about right now.

53
00:04:00,870 --> 00:04:06,930
And then to this particular beautiful soup, you also need to pass in the parcel, which you want to

54
00:04:06,930 --> 00:04:07,350
use.

55
00:04:07,500 --> 00:04:14,430
So as you of you want to pass the HTML file, pass in HTML not parser over here.

56
00:04:15,450 --> 00:04:22,680
So now what this will do is that it will actually go ahead, dig this HTML and use the term El Paso

57
00:04:22,680 --> 00:04:24,780
to pass that particular HDMI.

58
00:04:25,290 --> 00:04:28,680
And now let's also go ahead and see the result into something.

59
00:04:28,680 --> 00:04:34,590
So I'll type soup equals so that we have the result inside scoop.

60
00:04:35,460 --> 00:04:39,360
So now let me just go ahead and execute this code and see what happens.

61
00:04:39,630 --> 00:04:45,350
So when it opens soup, as you can see, we again get the same e-mail in here.

62
00:04:45,690 --> 00:04:51,720
But now if I actually want to go ahead and print it out in a specific format, I can say print.

63
00:04:52,290 --> 00:04:54,180
Then I'll type in Sudan.

64
00:04:55,910 --> 00:05:02,180
Prettify, so prettify is actually a method which actually format's this HTML code in a much more better

65
00:05:02,180 --> 00:05:02,490
way.

66
00:05:02,540 --> 00:05:10,060
So when I go ahead and hit enter and if we scroll up a little bit, as you can see now, we have HDMI

67
00:05:10,070 --> 00:05:12,380
Lynnae much more better and readable format.

68
00:05:12,530 --> 00:05:17,120
Now let's see if I actually want to go ahead and out of this ASML code.

69
00:05:17,120 --> 00:05:21,920
I just want to find all the elements which are nothing but links.

70
00:05:21,950 --> 00:05:26,240
So let's say I want to find all the linked tags and the from the about HTML.

71
00:05:26,240 --> 00:05:35,480
I can simply type in Sudan find and let's go all and in here I can simply passing the tag which I want

72
00:05:35,480 --> 00:05:35,970
to find.

73
00:05:35,990 --> 00:05:38,690
So I want to find all the elements with the attack.

74
00:05:38,690 --> 00:05:40,240
So I go ahead, hit enter.

75
00:05:40,580 --> 00:05:47,090
And as you can see, we now have all the linked tags over here which are actually represented in a form

76
00:05:47,090 --> 00:05:47,870
of a list.

77
00:05:48,170 --> 00:05:53,270
So we can go ahead, use a Follow-Up to loop through all the list items and print this in a much more

78
00:05:53,270 --> 00:05:53,780
better way.

79
00:05:54,320 --> 00:06:03,650
So in order to write a for loop over here, I can type in for link in so dot find all and just passing

80
00:06:05,050 --> 00:06:13,430
you over here and then in order to actually go to the next line I can simply go ahead, pull the shift

81
00:06:13,430 --> 00:06:14,860
key and hit enter.

82
00:06:14,870 --> 00:06:20,210
And as you can see, it won't execute this line of code, but instead the Python channel will allow

83
00:06:20,210 --> 00:06:21,780
us to write code on the next line.

84
00:06:21,800 --> 00:06:30,050
So here I'll use indentation and I'll simply try to print out the value of link and let's see what exactly

85
00:06:30,050 --> 00:06:30,550
happens.

86
00:06:30,560 --> 00:06:35,320
And now once you want to execute the code, simply hit, enter and hit, enter one more time.

87
00:06:35,540 --> 00:06:42,020
And as you can see now, we have all the links in a specific format, which we have where we have one

88
00:06:42,020 --> 00:06:43,170
link on each line.

89
00:06:43,580 --> 00:06:47,030
So as you can see, this is the link for Google Images.

90
00:06:47,540 --> 00:06:50,750
It's actually an entire link tag for Google Images.

91
00:06:50,750 --> 00:06:56,600
So we have the iTrust attribute, we have the name of that attribute, so on and so forth.

92
00:06:56,600 --> 00:07:00,400
And we have all the links which were actually present on the Google homepage.

93
00:07:00,440 --> 00:07:01,820
So that's all fine and good.

94
00:07:02,300 --> 00:07:07,290
But let's say if you only want to get the content inside the each attribute.

95
00:07:07,370 --> 00:07:09,630
So how exactly are you going to get that?

96
00:07:10,310 --> 00:07:13,730
So we will actually go ahead, make use of the same logic.

97
00:07:13,730 --> 00:07:22,130
That is, we will use the same for loop to get the e-TAG elements shift and shift to the next line in

98
00:07:22,130 --> 00:07:24,770
here type and print link.

99
00:07:25,190 --> 00:07:29,210
And instead of just printing out the link, let's type and link dot get.

100
00:07:29,570 --> 00:07:37,400
And in here, let's specify that we want the Streiff attribute and once we go had mentioned that and

101
00:07:37,400 --> 00:07:38,110
hit enter.

102
00:07:38,510 --> 00:07:45,140
As you can see now we only get all the links over here, which is nothing but the content of the each

103
00:07:45,380 --> 00:07:46,020
attribute.

104
00:07:46,130 --> 00:07:48,960
So this is what exactly we want to perform.

105
00:07:49,100 --> 00:07:56,300
So for example, for any given Web page, we actually want to get all the links on that particular Web

106
00:07:56,300 --> 00:07:56,710
page.

107
00:07:56,750 --> 00:08:03,670
So in this specific case, we have actually extracted all the links from the Google dot com homepage.

108
00:08:04,040 --> 00:08:06,650
So doing this is quite easy.

109
00:08:06,650 --> 00:08:12,680
But now in the upcoming lectures, we are going to learn how exactly can we use or mention this specific

110
00:08:12,680 --> 00:08:19,320
code inside our language application so as to go ahead and extract links from any given webpage.

111
00:08:19,340 --> 00:08:20,930
So that's it for this lecture.

112
00:08:20,930 --> 00:08:26,040
And in the next lecture, let's try to go ahead and integrate this code in our Shango application.

113
00:08:26,450 --> 00:08:28,370
So thank you very much for watching.

114
00:08:28,370 --> 00:08:30,290
And I'll see you guys next time.

115
00:08:30,500 --> 00:08:31,070
Thank you.

