1
00:00:00,180 --> 00:00:06,370
So now here, let's go ahead and define the string method, so I'll go ahead and type in def Double

2
00:00:06,390 --> 00:00:11,300
and Kostia and Will Posehn self as the parameter.

3
00:00:11,730 --> 00:00:18,960
And here what we want to do is that instead of having this movie's object over here, we actually want

4
00:00:18,960 --> 00:00:24,320
to go ahead and display the name of the particular movie right here.

5
00:00:24,360 --> 00:00:27,980
And henceforward, what we will do is that will Dipen return?

6
00:00:28,440 --> 00:00:34,560
And now in order to get access to this particular name, we type in self-taught name.

7
00:00:34,980 --> 00:00:37,140
So now once we go ahead, save the code.

8
00:00:37,560 --> 00:00:43,860
And if we go back over here and hit refresh, as you can see now, we have the name of the movie over

9
00:00:43,860 --> 00:00:46,920
here instead of actually having movies object.

10
00:00:47,430 --> 00:00:52,910
OK, so now once we have access to this particular movies, object in the admin panel.

11
00:00:53,010 --> 00:00:59,280
Now let's go ahead and learn how to create a view to list out all the movies which are available over

12
00:00:59,280 --> 00:01:00,670
here inside the model.

13
00:01:01,590 --> 00:01:08,070
So in order to create a view, we actually go ahead and go to The View by file, which is this file

14
00:01:08,070 --> 00:01:08,630
right here.

15
00:01:09,120 --> 00:01:14,670
And the first thing which we do is that we, first of all, input the model which we have just created,

16
00:01:14,670 --> 00:01:16,110
which is the movies model.

17
00:01:16,560 --> 00:01:18,990
So let's go ahead and type in from.

18
00:01:20,600 --> 00:01:29,470
Not models in both movies, and once we have imported that, we can now actually go ahead and fetch

19
00:01:29,470 --> 00:01:31,960
objects from this particular movie model.

20
00:01:32,530 --> 00:01:34,750
So let's go ahead and define a view here.

21
00:01:34,900 --> 00:01:40,810
So we type in depth and let's say this view is actually going to list out all the movies.

22
00:01:40,810 --> 00:01:44,470
And henceforth we call this view as movie list.

23
00:01:44,890 --> 00:01:48,100
So I'll type in movie and the school list.

24
00:01:48,640 --> 00:01:53,580
And as usual, this view is going to accept a parameter, which is a request.

25
00:01:53,590 --> 00:01:58,860
And now we go ahead and extract all the objects from this particular movie's model.

26
00:01:59,290 --> 00:02:04,000
So we type in movies, dot objects dot all.

27
00:02:04,810 --> 00:02:11,380
And now in order to save these objects into something, we see it into movies.

28
00:02:13,520 --> 00:02:13,950
Object.

29
00:02:14,110 --> 00:02:18,020
So now once we have that, we now have access to all of these objects.

30
00:02:18,440 --> 00:02:23,520
Now let's go ahead and parse this movie's object as a context to a template.

31
00:02:23,540 --> 00:02:29,000
So now, in order to list out all of the movies, we now need to go ahead and create a template and

32
00:02:29,390 --> 00:02:33,860
in movies object as the context to that particular template.

33
00:02:33,920 --> 00:02:37,230
So I can type and return render.

34
00:02:38,030 --> 00:02:45,710
And in here I can type in request because that's what you posehn do, the render function and then you

35
00:02:45,710 --> 00:02:46,790
pass in the template.

36
00:02:47,180 --> 00:02:53,240
So right now we don't have any sort of template right here, but still we are going to type in the template

37
00:02:53,240 --> 00:02:56,500
path as a let's see, the app name.

38
00:02:56,510 --> 00:02:59,120
So our app name is nothing but a new app.

39
00:02:59,120 --> 00:03:06,560
So I'll type in new app slash and let's say the name of the template is going to be a movie and a school

40
00:03:06,980 --> 00:03:08,590
list DOT HDMI.

41
00:03:09,380 --> 00:03:17,390
And also let's pass in the context as well, which is movies object and let's name this thing as, let's

42
00:03:17,390 --> 00:03:23,930
say, movie objects, because that name actually sounds appropriate because this is going to have the

43
00:03:23,930 --> 00:03:29,330
objects of movies and let's pass in that context over here as well.

44
00:03:29,450 --> 00:03:33,710
So I'll type in movie and the school.

45
00:03:34,970 --> 00:03:38,760
Objects as movie objects.

46
00:03:39,560 --> 00:03:42,520
OK, so once we do that, we are pretty much good to go.

47
00:03:42,620 --> 00:03:45,610
Now we need to go ahead and create this template right here.

48
00:03:46,280 --> 00:03:50,420
So you all know how exactly do we create a template inside an app.

49
00:03:50,450 --> 00:03:53,260
So we want to create a template inside the new app.

50
00:03:53,630 --> 00:03:59,990
So we go ahead and first of all, create a new folder called This Thing as templates.

51
00:04:00,860 --> 00:04:08,180
And inside those templates directly, we again go ahead and create a new directory and named as new

52
00:04:08,180 --> 00:04:08,570
app.

53
00:04:09,440 --> 00:04:15,330
And in here, we finally go ahead and create a new file and call it a template, which is nothing but

54
00:04:15,330 --> 00:04:20,120
a movie and a school list dot each.

55
00:04:21,649 --> 00:04:25,370
So once we go ahead and do that, now we have a template right here.

56
00:04:26,090 --> 00:04:32,840
So now as we have passed in the movie objects as the context to this particular template, now you can

57
00:04:32,840 --> 00:04:34,820
go ahead and make use of that over here.

58
00:04:35,330 --> 00:04:41,680
So what we do is that we loop through each one of those objects which we have in the movie list.

59
00:04:42,200 --> 00:04:48,110
So in order to loop through, we use a follow up here so I can type in for movie.

60
00:04:49,570 --> 00:04:50,200
Item.

61
00:04:51,540 --> 00:05:00,480
In a movie and the school objects, which means now we want to access a single movie item inside the

62
00:05:00,480 --> 00:05:05,130
movie objects, and we also end this photo here by typing in.

63
00:05:06,350 --> 00:05:15,560
And for and now in here, let's go ahead and loop through them so I can type and movie underscore item,

64
00:05:16,490 --> 00:05:23,150
and now I actually want to access the name of that particular item so I can type in that name over here,

65
00:05:23,840 --> 00:05:26,030
which is nothing but this name right over here.

66
00:05:26,510 --> 00:05:31,240
So now once we go ahead and do that, we are pretty much done creating the template.

67
00:05:32,120 --> 00:05:37,160
And now once we have the model, once we have the view, the next thing which we need to do is that

68
00:05:37,160 --> 00:05:42,990
we need to define the usual patterns for this particular view, which is what we list.

69
00:05:43,430 --> 00:05:45,330
So let's go ahead and do that as well.

70
00:05:45,770 --> 00:05:52,730
So in the Alstott profile right here, first of all, we go ahead and we need to import this particular

71
00:05:52,730 --> 00:05:56,880
view because we want to associate this particular view with the other.

72
00:05:57,320 --> 00:06:00,200
And I guess I have misspelled Moviola here.

73
00:06:00,210 --> 00:06:04,790
That should actually be M or we I.

74
00:06:04,790 --> 00:06:05,180
E.

75
00:06:06,470 --> 00:06:16,130
So once we fix that, we can go ahead and impose that over here so I can type in from new app import,

76
00:06:16,580 --> 00:06:25,280
I can just import Musea or I can also type in from new app DOT views import movie list so you can do

77
00:06:25,280 --> 00:06:25,830
either way.

78
00:06:26,090 --> 00:06:30,440
So I'm just going to import Musea and then I'm going to use the.

79
00:06:31,890 --> 00:06:41,640
Movie list in here, so I can type in bad and let's see the you are all for this is going to be movies

80
00:06:41,640 --> 00:06:46,650
so I can type in movie slash and then I need to pass and view.

81
00:06:46,770 --> 00:06:51,990
So in order to pass on you, I will make use of this views right here, which I have just imported.

82
00:06:52,350 --> 00:07:01,650
And from this views I will actually use the movie list view so I can type in views dot movie list.

83
00:07:01,860 --> 00:07:04,170
And the name is going to be.

84
00:07:05,260 --> 00:07:11,350
Movie and school list, so once we go ahead and do that, we are pretty much good to go.

85
00:07:12,160 --> 00:07:19,510
So now that means we have successfully completed the setting up of the model, the view and the template

86
00:07:19,510 --> 00:07:19,980
as well.

87
00:07:20,290 --> 00:07:26,010
And we also have a U-Haul in place to actually give us access to that particular view.

88
00:07:26,650 --> 00:07:28,600
So let's see if this thing works fine.

89
00:07:28,780 --> 00:07:31,500
So let's make sure that the subway is up and running.

90
00:07:32,260 --> 00:07:40,300
And now let's open up the Web browser and let's go to slash movies.

91
00:07:40,450 --> 00:07:44,880
So when I go ahead and hit and as you can see, it sees my movie over here.

92
00:07:45,550 --> 00:07:49,370
And now let's go ahead and try to add some more movies in here.

93
00:07:49,840 --> 00:07:52,420
So let's go ahead and add in another movie like.

94
00:07:52,570 --> 00:07:53,410
Let's see.

95
00:07:55,210 --> 00:08:03,700
Second movie, let's see, the reading is four point seven and let's click on Save and as you can see,

96
00:08:03,700 --> 00:08:06,930
we have my movie and second movie over here.

97
00:08:07,330 --> 00:08:13,500
And now let's go back to slash movies and let's see what we get.

98
00:08:14,050 --> 00:08:17,960
So when we go to movies, as you can see, we have the second movie as well.

99
00:08:18,430 --> 00:08:23,800
That means now we have a view which actually lists out all the movies in our database.

100
00:08:24,730 --> 00:08:26,230
So that's it for this lecture.

101
00:08:26,380 --> 00:08:33,520
And I hope you guys were able to understand how to set up the model, you and the template for our project.

102
00:08:33,850 --> 00:08:37,299
And in the upcoming lecture, we are going to learn about pagination.

103
00:08:37,299 --> 00:08:44,110
That is how to exactly go ahead and limit the amount of movies you can have on one page, which means

104
00:08:44,110 --> 00:08:49,990
that let's say if you have five movies and let's say you only want to display three on this page and

105
00:08:49,990 --> 00:08:54,320
you want to display the rest of the two on the next page, you can do that using pagination.

106
00:08:54,790 --> 00:08:56,680
So thank you very much for watching.

107
00:08:56,680 --> 00:08:58,660
And I'll see you guys next time.

108
00:08:58,780 --> 00:08:59,350
Thank you.

