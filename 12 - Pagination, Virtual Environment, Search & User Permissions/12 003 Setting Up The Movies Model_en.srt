1
00:00:00,980 --> 00:00:06,530
Hello and welcome to this lecture and in this lecture, we will actually go ahead and we will set up

2
00:00:06,530 --> 00:00:12,680
the model and our project so that we can learn the concepts like pagination site sitemap search and

3
00:00:12,680 --> 00:00:14,470
filtering, so on and so forth.

4
00:00:14,900 --> 00:00:19,370
So the very first thing which we will do is that we will go into the myside directory.

5
00:00:19,880 --> 00:00:23,240
And as you can see, we don't have any app or here as of now.

6
00:00:23,780 --> 00:00:27,650
So we only have this myside directory and we don't have any app.

7
00:00:27,650 --> 00:00:30,250
So let's go ahead and create that app right over here.

8
00:00:30,740 --> 00:00:32,920
So I'll first make sure to stop the server.

9
00:00:33,530 --> 00:00:36,620
And now let's go ahead and open Shango.

10
00:00:38,090 --> 00:00:46,700
Admen start up and let's name this app as a new app, which is going to be a dummy app, so now once

11
00:00:46,700 --> 00:00:52,610
this app is created, we can now go ahead, go into the go into visual studio code.

12
00:00:53,210 --> 00:00:56,650
And here, as you can see, we should actually have an app here.

13
00:00:56,810 --> 00:00:59,140
So as you can see, we have new app over here.

14
00:00:59,720 --> 00:01:04,920
So let's go to the model Stoppie file and let's create a dummy model right over here.

15
00:01:05,090 --> 00:01:08,090
So let's go ahead and type in class and let's see.

16
00:01:08,120 --> 00:01:14,040
We want to use this app to store movie data that is the name of the movie and rating of the movie.

17
00:01:14,060 --> 00:01:18,080
And if you want, you can actually go ahead and create the model of your own choice.

18
00:01:18,350 --> 00:01:23,910
But I would highly recommend you guys to stay the course and just follow along and create the movie's

19
00:01:23,930 --> 00:01:24,350
model.

20
00:01:24,560 --> 00:01:25,960
So I'll type in movies here.

21
00:01:27,380 --> 00:01:30,430
And this is going to inherit from Model Start model.

22
00:01:31,580 --> 00:01:35,350
So I hope you guys got the hang of it, of creating the model.

23
00:01:35,510 --> 00:01:41,420
So if you can just post the video and create the model on your own just for practice.

24
00:01:42,110 --> 00:01:46,280
So type name equals that's going to be modeled on.

25
00:01:47,880 --> 00:01:50,880
Garfield, the max.

26
00:01:52,190 --> 00:01:54,560
Lent is going to be let's see.

27
00:01:56,180 --> 00:01:56,840
200.

28
00:01:58,810 --> 00:02:06,820
Then we have the rating, which is going to be integer field, so that's going to be modeled on instead

29
00:02:06,820 --> 00:02:07,380
of a..

30
00:02:07,390 --> 00:02:08,530
Let's keep it flawed.

31
00:02:08,560 --> 00:02:10,199
So this is going to be a flawed field.

32
00:02:10,210 --> 00:02:16,150
And now once we have this model, the very first thing which you do after having your model and after

33
00:02:16,150 --> 00:02:22,990
creating a new app is that you go into the settings dot profile of your myside, which is your main

34
00:02:22,990 --> 00:02:28,180
project, and then you have to mention the app which you have just created.

35
00:02:28,630 --> 00:02:35,410
So I'll type in New Apple where he'll give a comma and you can also go ahead and type in my myside dot

36
00:02:35,410 --> 00:02:38,760
apps, dot new apps config and that works as well.

37
00:02:38,770 --> 00:02:41,390
But this is just a simpler way of doing it.

38
00:02:41,920 --> 00:02:47,050
So once you go ahead and add this app and once you have this model right here now, the next thing which

39
00:02:47,050 --> 00:02:49,600
we do is that we go ahead and make migration's.

40
00:02:49,780 --> 00:02:52,210
So you type in Python three.

41
00:02:53,340 --> 00:02:57,270
Managed by MK Migration's.

42
00:02:58,760 --> 00:03:03,410
And then you type in Python three managed IPY.

43
00:03:04,610 --> 00:03:11,180
Migrate and hit enter, and as you can see, all the migrations have been applied, which means that

44
00:03:11,180 --> 00:03:15,800
we actually now have a model at the back end in which we can add data.

45
00:03:15,860 --> 00:03:21,070
So now, in order to add some dummy data, let's go ahead and create a super user.

46
00:03:21,350 --> 00:03:28,700
So I'll type in Python three managed by the super user.

47
00:03:29,930 --> 00:03:37,010
And I'll keep my super username as my name itself, the email address is some dummy e-mail address like

48
00:03:37,010 --> 00:03:38,030
Demel at.

49
00:03:39,280 --> 00:03:45,400
Gmail dot com password is going to be super user.

50
00:03:47,110 --> 00:03:51,670
Super user, and as you can see, the super user has been created.

51
00:03:52,120 --> 00:03:55,180
So now let's go back and log in as admin.

52
00:03:55,270 --> 00:04:00,060
So I'll type in Python three managed IPY run server.

53
00:04:00,670 --> 00:04:05,350
And now let's go ahead and open up browser.

54
00:04:06,190 --> 00:04:07,710
Go to slash admin.

55
00:04:07,720 --> 00:04:08,530
Let's login.

56
00:04:12,450 --> 00:04:15,490
And now let's go ahead and add in some dummy data over here.

57
00:04:15,660 --> 00:04:21,300
But as you can see, in order to add dummy data, we don't see a model right here, and that's because

58
00:04:21,300 --> 00:04:25,650
we have not registered our model into the admin side.

59
00:04:26,040 --> 00:04:31,660
So in order to do so, simply go to the admin, not file, import your model here.

60
00:04:31,710 --> 00:04:34,650
So from DOT models, that's going to be.

61
00:04:35,980 --> 00:04:42,760
Import and we are going to import the model, which is nothing but movies, so once we have imported

62
00:04:42,760 --> 00:04:45,390
that, let's register that on our admin side.

63
00:04:45,400 --> 00:04:49,290
So we dipen admin side, not register.

64
00:04:49,540 --> 00:04:50,770
And let's pass an.

65
00:04:52,130 --> 00:04:53,120
Movies over here.

66
00:04:54,290 --> 00:04:59,660
So once you go ahead and make the change and if you go ahead, hit refresh, as you can see, you will

67
00:04:59,660 --> 00:05:02,000
have the movies model right over here.

68
00:05:02,240 --> 00:05:10,100
So when I go ahead and click on ADD, as you can see, I can add a movie here so I can type in my movie.

69
00:05:10,160 --> 00:05:13,350
Let's see, the rating is five click on Save.

70
00:05:13,370 --> 00:05:15,710
So here we have the movies object.

71
00:05:16,010 --> 00:05:23,960
And in order to actually have some name over here instead of just movies object one, you can go back

72
00:05:24,200 --> 00:05:26,390
and you always know what to do.

73
00:05:26,390 --> 00:05:32,120
In such cases, you need to define a string method in here in the models class, which is in this case,

74
00:05:32,120 --> 00:05:32,600
movies.

