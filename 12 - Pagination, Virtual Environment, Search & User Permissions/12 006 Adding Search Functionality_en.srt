1
00:00:00,090 --> 00:00:01,790
Hello and welcome to this lecture.

2
00:00:01,890 --> 00:00:06,650
And in this lecture, we are going to learn about search and filtering in general.

3
00:00:07,290 --> 00:00:12,720
So we were actually supposed to learn side maps in this particular lecture, but we will learn about

4
00:00:12,720 --> 00:00:14,510
site map in the upcoming lectures.

5
00:00:14,520 --> 00:00:17,790
But for now, let's jump into search and filtering.

6
00:00:18,240 --> 00:00:25,250
So now let's see if you have thousands of movies on your movie website right over here.

7
00:00:25,620 --> 00:00:30,840
And let's say if you want to have a search bar right here to search one of these movies right here.

8
00:00:31,140 --> 00:00:33,510
So how exactly can you go ahead and do that?

9
00:00:34,170 --> 00:00:39,560
So in order to go ahead and search movies in here, we are going to make use of filters and Shango.

10
00:00:40,140 --> 00:00:42,810
So using filters and Django is pretty simple.

11
00:00:42,810 --> 00:00:48,300
You just need to go ahead and filter out the objects which you actually get on retrieving a particular

12
00:00:48,300 --> 00:00:48,690
model.

13
00:00:49,200 --> 00:00:54,360
So now what we want to do is that we want to have a search bar right here and it should have the search

14
00:00:54,360 --> 00:00:54,810
button.

15
00:00:55,140 --> 00:00:59,790
And whenever we type in some movie name in here and when we click on that search button, we should

16
00:00:59,790 --> 00:01:02,900
actually get the movie, which we have just typed in here.

17
00:01:03,570 --> 00:01:05,820
So how exactly can we go ahead and do that?

18
00:01:06,030 --> 00:01:10,860
So first of all, let's go ahead and add the search bar and you will understand how we can connect that

19
00:01:10,860 --> 00:01:13,300
search part, your database and everything like that.

20
00:01:13,920 --> 00:01:19,380
So let's go ahead and define a search bar in here at the top of our template.

21
00:01:19,410 --> 00:01:20,310
So I'll go ahead.

22
00:01:20,610 --> 00:01:23,870
And in order to have a search bar, I'll go ahead and create a form here.

23
00:01:24,090 --> 00:01:29,550
So the forms action is going to be nothing, but the method is actually going to be kept.

24
00:01:30,270 --> 00:01:33,480
And in here we are simply going to create a input field.

25
00:01:33,480 --> 00:01:36,150
So the input type is going to be search.

26
00:01:36,870 --> 00:01:40,810
And the name is the most important thing which we are going to give here.

27
00:01:41,760 --> 00:01:47,160
So the name of this input type is going to be let the movie and the school name.

28
00:01:47,970 --> 00:01:51,570
And we don't actually want the idea of this particular field right now.

29
00:01:52,410 --> 00:01:54,800
So we are going to simply go ahead, delete that.

30
00:01:54,810 --> 00:01:58,100
And now let's actually have a submit button in here.

31
00:01:58,350 --> 00:02:05,340
So I'll type in button and let's define the type of this button to submit because we are using a form

32
00:02:05,340 --> 00:02:08,940
here and let's say this button says such.

33
00:02:09,660 --> 00:02:13,240
Okay, so now once we have the search button, we are pretty much good to go.

34
00:02:13,380 --> 00:02:15,120
So if I go ahead, hit refresh.

35
00:02:15,120 --> 00:02:18,630
As you can see, we have a nice looking search button up over here.

36
00:02:19,500 --> 00:02:24,570
So now once we have this button now, let's go ahead and make this button functional.

37
00:02:24,960 --> 00:02:31,290
So now the first thing which we do is that as soon as the user clicks the search button, we will actually

38
00:02:31,290 --> 00:02:34,780
want to fetch in the movie, which was entered by the user.

39
00:02:34,950 --> 00:02:37,170
So how exactly can we go ahead and do that?

40
00:02:37,710 --> 00:02:44,370
So first of all, we actually need a we to get this particular movie's name, which we have just typed

41
00:02:44,370 --> 00:02:46,250
in here in this particular input field.

42
00:02:46,740 --> 00:02:52,730
So in order to get that, we use the get method, which we have also used in pagination.

43
00:02:52,740 --> 00:02:59,160
So if we actually go to the views of PWA, as you can see, we actually use the get method to actually

44
00:02:59,160 --> 00:03:00,530
get the page number.

45
00:03:00,840 --> 00:03:06,390
Now we are going to use the same thing for getting the movie name over here from this particular input

46
00:03:06,390 --> 00:03:06,750
field.

47
00:03:07,260 --> 00:03:10,190
So let's see how exactly can we go ahead and do that?

48
00:03:10,710 --> 00:03:17,130
So leave the page, need a code aside and we'll start search and filtering code right from here.

49
00:03:17,440 --> 00:03:22,740
First of all, Liben movie and a school name, because we want to get access to that movie name.

50
00:03:23,160 --> 00:03:25,620
So we go ahead and get that from the request.

51
00:03:25,860 --> 00:03:28,140
Don't get, don't get.

52
00:03:28,620 --> 00:03:33,150
And in here you need to type in the name of the film, which is the input file right here.

53
00:03:33,180 --> 00:03:36,130
And the name of that input field is nothing but movie name.

54
00:03:36,750 --> 00:03:39,580
So simply copy that and pasted right over here.

55
00:03:39,930 --> 00:03:45,480
So that means now we just have access to the movie name and now once we have access to the movie name

56
00:03:45,660 --> 00:03:51,600
and once we have all the movie objects, we want to get the movie object, which has the name as the

57
00:03:51,600 --> 00:03:53,800
movie name as simple as that.

58
00:03:54,270 --> 00:03:59,250
So we just go ahead and make a check over here so we see if movie name.

59
00:03:59,520 --> 00:04:03,030
So first of all, we want to make sure that the movie name is not empty.

60
00:04:03,360 --> 00:04:13,880
So we type F movie name is not equal to empty and the movie name is not none.

61
00:04:14,100 --> 00:04:16,720
So it should not be empty and it should not be none.

62
00:04:17,130 --> 00:04:23,850
So if that is the case, then we actually can go ahead and use these movie objects and we can filter

63
00:04:23,850 --> 00:04:31,350
out the objects and we can only have that object which has that particular movie name so we can type

64
00:04:31,350 --> 00:04:31,590
in.

65
00:04:32,700 --> 00:04:39,760
Movie objects equals movie objects, dot filter.

66
00:04:40,410 --> 00:04:46,140
So we are filtering upon the movie objects, which we currently have, and we want the object where

67
00:04:46,140 --> 00:04:48,850
the name Sunim is nothing but this feel right here.

68
00:04:49,140 --> 00:04:56,190
So the name should be equal to nothing but the movie name, which we just got from the input field so

69
00:04:56,190 --> 00:04:57,060
I can type in.

70
00:04:58,150 --> 00:04:59,530
Moving them right over here.

71
00:05:00,350 --> 00:05:05,260
So now once we go ahead and do that, we are pretty much done creating a filter.

72
00:05:05,890 --> 00:05:09,880
So now what this thing simply does is that it takes all the movie objects.

73
00:05:10,180 --> 00:05:16,630
It's going to filter them in a way that the name of that particular movie Object matches up the movie

74
00:05:16,630 --> 00:05:21,010
name, which is nothing but the name of the movie, which we have entered in the search bar.

75
00:05:21,550 --> 00:05:28,150
And it's going to return back the result to movie objects, which means that if it finds that particular

76
00:05:28,150 --> 00:05:32,050
movie object, it's going to simply pass that thing as the context.

77
00:05:32,050 --> 00:05:35,020
And we are going to get that on our main page.

78
00:05:35,140 --> 00:05:37,810
So now let's go ahead and see if this thing actually works.

79
00:05:38,440 --> 00:05:47,990
So let me switch back to the browser and now let's search for my movie.

80
00:05:48,010 --> 00:05:51,400
So when I go ahead and click search, as you can see, we have my movie.

81
00:05:51,410 --> 00:05:57,340
Now, there is one problem with this particular approach, and that is basically let's see if you have

82
00:05:57,340 --> 00:06:00,650
a movie which says My space movie.

83
00:06:00,700 --> 00:06:06,240
So let's go ahead and add a movie, which is my and has some space and then it says movie.

84
00:06:06,250 --> 00:06:07,940
Let's see what happens in that case.

85
00:06:08,470 --> 00:06:10,960
So let's go to Admon.

86
00:06:12,070 --> 00:06:17,110
And in here, let's add a movie and let's name this thing as my.

87
00:06:18,200 --> 00:06:26,450
Movie, let's see, the beating is five click on Save, and now if we actually hop back to movies.

88
00:06:27,650 --> 00:06:35,810
That should be movies, and now if we go to the last page, as you can see, you have my movie.

89
00:06:36,290 --> 00:06:41,220
So if you actually just search for a movie, you should actually get these two movies right here.

90
00:06:41,240 --> 00:06:45,950
But now, if I go ahead and actually search, as you can see, I get no result at all.

91
00:06:46,580 --> 00:06:52,460
So how exactly can we fix this problem so we can fix this problem by using something we just called

92
00:06:52,460 --> 00:06:53,630
as I canteen's.

93
00:06:54,170 --> 00:07:01,490
So what our current query or current filter is doing is that it is actually looking for the exact matching

94
00:07:01,490 --> 00:07:01,940
words.

95
00:07:02,420 --> 00:07:07,760
So it needs to make sure that the name is going to be equal to the exact name which we are passing in

96
00:07:07,760 --> 00:07:08,500
the input field.

97
00:07:09,020 --> 00:07:15,320
And in order to change that, I can simply go ahead and instead of just having name over here, I can

98
00:07:15,320 --> 00:07:20,270
type a name double and the score I contains.

99
00:07:21,500 --> 00:07:28,250
So what this does is that it relaxes our search a little bit so that we also get if certain particular

100
00:07:28,250 --> 00:07:30,970
wood of our movies title matches up.

101
00:07:31,700 --> 00:07:34,190
So just make sure to add double underscores.

102
00:07:34,220 --> 00:07:39,680
So this is not a single underscore and said we have two the scores and then type and I canteen's.

103
00:07:39,920 --> 00:07:44,230
So once we go ahead and do that, let's see how a search works now.

104
00:07:44,870 --> 00:07:46,580
So let's first go to movies.

105
00:07:47,030 --> 00:07:53,630
And now as you can see on the last page, we have fifth movie and my movie and let's search for a movie.

106
00:07:53,960 --> 00:07:59,120
So when I click on Search, as you can see, everything appears right over here because each one of

107
00:07:59,120 --> 00:08:01,880
these fields actually have a movie in their name.

108
00:08:02,240 --> 00:08:04,190
Let's go ahead and add another movie.

109
00:08:04,430 --> 00:08:10,010
And let's see that movie's name is something different, which does not have movie in it.

110
00:08:10,020 --> 00:08:17,210
Or I can type in ABC as the movie name and the rating is, let's say five click on Save.

111
00:08:17,510 --> 00:08:25,190
And now if I go to slash movies and now if you go to Lask, as you can see, you have EBC right here

112
00:08:25,520 --> 00:08:29,390
and now if I type in ABC and had such.

113
00:08:30,710 --> 00:08:37,370
As you can see, I only have EBC over here and now let's see if I type in thought and if I click, such

114
00:08:37,370 --> 00:08:39,380
as you can see, I get the third movie.

115
00:08:39,770 --> 00:08:45,100
If I go ahead and type in second and click search, I get the second movie over here.

116
00:08:45,980 --> 00:08:51,090
So that's how you can actually go ahead and implement the search functionality in Django.

117
00:08:51,170 --> 00:08:54,830
So the only thing which you need to use is that you need to use a form.

118
00:08:55,310 --> 00:09:01,910
Then in order to access or get the input field data from the form, you simply need to go ahead and

119
00:09:01,940 --> 00:09:09,200
use the get method from the request and get that particular forms filled using the form filename, which

120
00:09:09,200 --> 00:09:10,610
is movie name in this case.

121
00:09:11,150 --> 00:09:16,540
Then you simply get that movie name and you check if that movie name actually matches up with the name.

122
00:09:16,940 --> 00:09:22,700
And in order to ease up the search a little bit, you use the variable name Double and Dusko I canteen's

123
00:09:23,000 --> 00:09:25,750
and the search is going to work in a much more better way.

124
00:09:26,540 --> 00:09:28,060
So that's it for this lecture.

125
00:09:28,130 --> 00:09:32,590
I hope you guys were able to understand how search and filtering works in Django.

126
00:09:32,990 --> 00:09:37,670
So in the next lecture, we are going to learn something about permissions and groups.

127
00:09:38,090 --> 00:09:42,030
So thank you very much for watching and I'll see you guys next time.

128
00:09:42,380 --> 00:09:42,950
Thank you.

