1
00:00:00,060 --> 00:00:05,850
Hello and welcome to this lecture and in this lecture, we will go ahead and learn something about permissions

2
00:00:05,850 --> 00:00:06,720
and groups.

3
00:00:07,290 --> 00:00:10,230
So let's see if you actually want to build a site.

4
00:00:10,230 --> 00:00:13,400
And obviously you are going to be the admin of the site.

5
00:00:13,770 --> 00:00:16,770
But as the site grows, you need to hire employees.

6
00:00:16,770 --> 00:00:22,470
You need to hire a sub admin or maybe just an employee to actually have a look at your data, which

7
00:00:22,470 --> 00:00:25,860
is present in your database, and take some action upon the data.

8
00:00:26,040 --> 00:00:31,830
So you obviously need to grant access to some sort of data to your employees so that they have enough

9
00:00:31,830 --> 00:00:32,870
data to work with.

10
00:00:33,090 --> 00:00:38,100
But let's say if you don't want to give them access to, actually go ahead and edit the data.

11
00:00:38,280 --> 00:00:40,470
So what would you do in that particular case?

12
00:00:41,340 --> 00:00:46,530
So in that particular case, you cannot directly go ahead and give admin access to your employees,

13
00:00:46,530 --> 00:00:52,500
because if you do so, then what would happen is that they will eventually be able to edit these particular

14
00:00:52,500 --> 00:00:53,370
things right here.

15
00:00:53,700 --> 00:00:56,270
And let's see, for some reason, you don't want to do that.

16
00:00:56,700 --> 00:01:03,180
So how exactly can you go ahead and give employees the permission only to view the data in the database,

17
00:01:03,180 --> 00:01:05,530
but not actually edit the data?

18
00:01:06,060 --> 00:01:11,430
So in that case, we can use something which is called as the user permissions and user groups.

19
00:01:11,740 --> 00:01:18,170
So what does Shango admin allows you to do is that they actually allow you to create a new user.

20
00:01:18,240 --> 00:01:23,640
So as you can see, if you go to the admin panel, you will be able to see this option, which is something

21
00:01:23,640 --> 00:01:24,670
like users.

22
00:01:24,720 --> 00:01:30,150
So if you click on users right here, as you can see right now, we have this admin user, which is

23
00:01:30,150 --> 00:01:31,130
my name right here.

24
00:01:31,650 --> 00:01:35,000
And as you can see, it actually has a staff status right here.

25
00:01:35,010 --> 00:01:41,490
So staff status basically means that this particular user is an employee of the company who runs this

26
00:01:41,490 --> 00:01:42,540
particular website.

27
00:01:42,570 --> 00:01:48,570
So what we can do here is that we can also go ahead and add a new user here so I can simply click on

28
00:01:48,570 --> 00:01:54,360
ADD User and I can see something like my employee or let's say my staff as the user name.

29
00:01:54,360 --> 00:01:57,840
I can set the password to our staff.

30
00:01:57,990 --> 00:02:06,240
User can form the password as staff user and I can click on Save and the user is now going to be created

31
00:02:06,240 --> 00:02:06,770
for us.

32
00:02:07,110 --> 00:02:12,660
And as you can see, if you scroll down a little bit, you also now have the option of actually going

33
00:02:12,660 --> 00:02:16,160
ahead and editing the personal information of the staff as well.

34
00:02:16,170 --> 00:02:19,920
So you can have a first name, last name, email address, so on and so forth.

35
00:02:19,950 --> 00:02:24,510
And also, as you can see here, we have a bunch of permissions which we can assign to this particular

36
00:02:24,510 --> 00:02:25,000
user.

37
00:02:25,020 --> 00:02:30,080
So let's say if you want to deactivate the user's account, you can simply uncheck this option.

38
00:02:30,090 --> 00:02:35,570
It will actually deactivate the account, not delete the account, which you can reactivate once again.

39
00:02:35,610 --> 00:02:38,420
Then you have this other option, which is staff status.

40
00:02:38,430 --> 00:02:43,620
So if you actually click on this option means that the user, which we have right now, which is my

41
00:02:43,620 --> 00:02:47,770
staff, is actually assigned a status of a staff user.

42
00:02:47,790 --> 00:02:51,870
So now you if we scroll down a little bit, we also have the super user status.

43
00:02:51,870 --> 00:02:54,690
So we don't want to make this user as a super user.

44
00:02:54,690 --> 00:02:56,510
So we will keep that unchecked.

45
00:02:57,000 --> 00:03:02,210
And also, if you scroll down a little bit, as you can see, we have permissions over here as well.

46
00:03:02,850 --> 00:03:04,950
So if you scroll down a little bit.

47
00:03:06,440 --> 00:03:13,040
As you can see, we also have the Apple Watch here, which is new app, and here you can see the model

48
00:03:13,040 --> 00:03:13,730
name as well.

49
00:03:13,880 --> 00:03:16,220
So here the model name is Maurice.

50
00:03:16,670 --> 00:03:23,810
And you, as you can see, this thing basically says that he can add movies to this particular model,

51
00:03:23,810 --> 00:03:25,910
which is movies modeled in the new app.

52
00:03:26,420 --> 00:03:30,910
So you can make sure to assign different permissions to this particular user.

53
00:03:30,920 --> 00:03:37,430
So let's say if you only want him to be able to view movies and cannot add or delete or change movies.

54
00:03:37,430 --> 00:03:40,920
So you can select this permission right here, click on this arrow.

55
00:03:40,940 --> 00:03:43,700
And now this permission is going to be added up over here.

56
00:03:44,480 --> 00:03:48,470
So now if you scroll down a little bit, you have the option to save.

57
00:03:48,500 --> 00:03:53,650
So once you click on that, these user settings will be saved for this user, which is my stuff.

58
00:03:54,290 --> 00:03:59,580
So I can now click on Save and now I will log out as a super user.

59
00:03:59,870 --> 00:04:04,910
And now let's log in using the credentials of a staff.

60
00:04:05,000 --> 00:04:08,330
So I'll type in my staff over here.

61
00:04:08,330 --> 00:04:09,800
And the password is.

62
00:04:11,120 --> 00:04:12,780
Staff use it.

63
00:04:12,950 --> 00:04:19,339
So when I log in, as you can see now, I can see the movies table over here and I can see the detail

64
00:04:19,339 --> 00:04:21,320
which is in there, as you can see.

65
00:04:21,320 --> 00:04:24,040
I can see the details of that particular movie as well.

66
00:04:24,350 --> 00:04:29,640
But I can no longer go ahead and edit one of these options or one of these movies right here.

67
00:04:30,800 --> 00:04:36,500
So that's how you can actually go ahead and control the different permissions for different set of users

68
00:04:36,500 --> 00:04:37,910
on your website.

69
00:04:37,970 --> 00:04:39,440
So that's it for this lecture.

70
00:04:39,620 --> 00:04:45,500
Hopefully you guys be able to understand how to create different kinds of stuff users for your website

71
00:04:45,500 --> 00:04:47,640
and give them different kinds of permissions.

72
00:04:47,870 --> 00:04:53,390
So this is specifically useful when you want to give some sort of permissions to some users and restrict

73
00:04:53,390 --> 00:04:54,040
the others.

74
00:04:54,050 --> 00:04:57,880
So thank you very much for watching and I'll see you guys next time.

