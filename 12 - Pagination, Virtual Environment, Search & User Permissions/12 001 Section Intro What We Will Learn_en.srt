1
00:00:00,060 --> 00:00:05,850
So hello and welcome to this new section, and in this section, we are going to call if you advance

2
00:00:05,850 --> 00:00:14,220
Concepts and Chango, so we are going to learn about pagination such virtual environments and user permissions

3
00:00:14,220 --> 00:00:15,450
and each app.

4
00:00:15,870 --> 00:00:18,690
So let's first go ahead and start with pagination.

5
00:00:18,910 --> 00:00:26,700
So what exactly is pagination pagination is actually the process of separating print or digital content

6
00:00:26,700 --> 00:00:27,990
into discrete pages.

7
00:00:28,200 --> 00:00:34,290
So if you have ever used any website with lots of content on it, you will be able to see these kinds

8
00:00:34,290 --> 00:00:40,330
of things on the bottom of the Web page, which actually allow you to navigate through your entire website.

9
00:00:40,800 --> 00:00:44,520
So let's say, for example, your browsing and e-commerce website.

10
00:00:44,820 --> 00:00:51,090
And if you actually go ahead search for a particular product, they are actually going to be a thousand

11
00:00:51,090 --> 00:00:52,540
products in that category.

12
00:00:53,070 --> 00:00:59,040
So in order to scroll all these products, it's not actually possible to load up all the products on

13
00:00:59,040 --> 00:01:00,020
a single Web page.

14
00:01:00,030 --> 00:01:04,640
And it's also not a good practice to load all the products on a single page.

15
00:01:04,830 --> 00:01:10,170
And that's why we use pagination to kind of split up all those products on the front pages.

16
00:01:10,680 --> 00:01:15,510
So let's say, for example, on a single page, you only want to display 10 products.

17
00:01:15,880 --> 00:01:19,160
So using pagination, you can actually go ahead and do that.

18
00:01:19,470 --> 00:01:25,130
That is, you can limit the amount of products or limit the amount of content which you actually show

19
00:01:25,170 --> 00:01:31,260
onto a single Web page and you can split the remaining all the rest of the contents on different pages.

20
00:01:31,890 --> 00:01:38,790
So we are going to learn how exactly can we implement pagination in Shango by creating a very simple

21
00:01:38,790 --> 00:01:40,140
and a very basic app.

22
00:01:40,530 --> 00:01:45,300
But we are actually going to make use of this concept in the next section, which is the e-commerce

23
00:01:45,300 --> 00:01:45,810
section.

24
00:01:46,560 --> 00:01:48,080
Now, let's talk about search.

25
00:01:48,720 --> 00:01:54,300
So any website which you are building should have a search functionality into it.

26
00:01:54,700 --> 00:01:58,650
That means let's see if your site is listing up a bunch of products.

27
00:01:58,890 --> 00:02:04,230
You actually need to give a search bar, which looks something of this sort, and it should allow your

28
00:02:04,230 --> 00:02:07,570
users to search for some content on your site.

29
00:02:07,590 --> 00:02:14,430
So let's say, for example, you have an e-commerce website and you want to allow users to search for

30
00:02:14,430 --> 00:02:17,910
products, then you must build the search functionality in your site.

31
00:02:18,600 --> 00:02:24,300
So in the section, we are going to design a simple app which is actually going to make you understand

32
00:02:24,300 --> 00:02:30,240
how exactly you can build a search functionality in your Chanko app to search for certain items.

33
00:02:31,570 --> 00:02:37,000
Then the most important thing which I should have actually covered earlier is virtual environment.

34
00:02:37,570 --> 00:02:39,760
So what exactly is virtual environment?

35
00:02:40,000 --> 00:02:45,490
So virtual environment is not going to be your app feature, but instead it's actually something which

36
00:02:45,490 --> 00:02:48,250
you used during Django app development process.

37
00:02:48,970 --> 00:02:55,750
So virtual environment is a tool in Python, which helps in creating new virtual environments for your

38
00:02:55,750 --> 00:02:56,320
projects.

39
00:02:56,470 --> 00:03:02,170
So what will EMV is actually a tool which helps you to create virtual environments, but what exactly

40
00:03:02,170 --> 00:03:03,320
are Warshel environments?

41
00:03:03,700 --> 00:03:10,570
So a watchful environment is something which provides an isolated environment which you can use for

42
00:03:10,570 --> 00:03:11,410
each project.

43
00:03:11,620 --> 00:03:14,800
So you may create different virtual environments for each project.

44
00:03:15,010 --> 00:03:18,610
So now why exactly do you need to use virtual environment?

45
00:03:19,240 --> 00:03:24,450
Each virtual environment may have a different set of packages, modules and libraries installed.

46
00:03:24,850 --> 00:03:27,910
So let's see if you want to create a specific project.

47
00:03:28,240 --> 00:03:34,720
And let's say you only want to install libraries for that project itself and you don't actually want

48
00:03:34,720 --> 00:03:37,820
those libraries install on your computer globally.

49
00:03:38,290 --> 00:03:44,560
So in that case, using virtual environment, you can create an isolated environment for that specific

50
00:03:44,560 --> 00:03:50,860
project and only install the packages and libraries which are specific to that project.

51
00:03:52,210 --> 00:03:55,390
This is what exactly what your environments allows you to do.

52
00:03:55,540 --> 00:04:01,330
So you can also actually create a virtual environment and have different versions of Python and Django

53
00:04:01,330 --> 00:04:01,840
install.

54
00:04:02,350 --> 00:04:08,110
So let's say if you actually want to build a Django project and for that particular Django project,

55
00:04:08,110 --> 00:04:13,060
let's say for some reason you actually want to use Python, you and Django one point eleven.

56
00:04:13,390 --> 00:04:16,890
So it does not make much sense to actually use those versions.

57
00:04:16,899 --> 00:04:20,130
But let's say for some reason you want to use those versions.

58
00:04:20,529 --> 00:04:26,950
So what you can do is that without even installing Python two and Django one on your machine, you can

59
00:04:26,950 --> 00:04:33,010
actually create an isolated environment using virtually N.V. and you can actually install Python to

60
00:04:33,010 --> 00:04:40,330
end Django one point eleven on that environment and actually use that environment to run your application.

61
00:04:41,590 --> 00:04:46,870
And in a similar fashion, by using the same virtual environment, you can create a brand new social

62
00:04:46,870 --> 00:04:50,120
environment which uses Python three and Django two point two.

63
00:04:50,980 --> 00:04:53,980
So this is the main advantage of using virtual environment.

64
00:04:54,160 --> 00:05:00,320
That is, it provides an isolated development environment for each individual project.

65
00:05:00,730 --> 00:05:05,620
So we are going to learn about how to create a virtual environment while starting up a project.

66
00:05:06,760 --> 00:05:09,820
Then we will go ahead and go through user permissions as well.

67
00:05:10,960 --> 00:05:13,530
So what exactly are user permissions?

68
00:05:14,020 --> 00:05:19,420
So whatever data which you have in your Web app should not actually be accessible to everyone.

69
00:05:19,780 --> 00:05:25,530
So let's see if you are using or if you have created a banking application for people.

70
00:05:25,690 --> 00:05:30,640
So all the content inside that application should obviously be not visible to everyone.

71
00:05:30,650 --> 00:05:36,400
That is, if you are a bank employee, you should be only able to view people's accounts balances and

72
00:05:36,400 --> 00:05:39,600
you should not be able to see any other confidential information.

73
00:05:40,490 --> 00:05:47,810
Now, some data and your Web, as I mentioned, might be confidential, and that data must only be revealed

74
00:05:47,810 --> 00:05:55,700
to certain specific set of users and not all users should have permission to perform crud, which means

75
00:05:55,700 --> 00:06:02,720
not all users should have the permission to perform the operations like create, read, update and delete.

76
00:06:03,260 --> 00:06:09,140
So if you are building your banking software, you should not actually allow the bank employee to update

77
00:06:09,140 --> 00:06:10,230
or delete your data.

78
00:06:10,250 --> 00:06:16,940
And even if a end user is using it, the end user should only be able to actually go ahead and read

79
00:06:16,940 --> 00:06:19,160
his data or read his account balance.

80
00:06:19,160 --> 00:06:23,780
And he shouldn't be actually able to go ahead and update or delete that data.

81
00:06:23,990 --> 00:06:27,200
So we are going to learn about user permissions as well.

82
00:06:27,260 --> 00:06:34,130
That is how exactly you can go ahead and use teaching or admin to set certain user permissions for all

83
00:06:34,130 --> 00:06:35,600
users for your site.

84
00:06:36,440 --> 00:06:40,580
So these are the four features which are going to learn in this section.

85
00:06:40,580 --> 00:06:46,370
And for the very same reason, we are actually going to develop a very small and basic looking application,

86
00:06:46,370 --> 00:06:49,380
which is going to cover all those four important features.

87
00:06:49,430 --> 00:06:54,770
So from the next lecture onwards, let's actually go ahead and start building this app, which has this

88
00:06:54,770 --> 00:06:55,400
features.

