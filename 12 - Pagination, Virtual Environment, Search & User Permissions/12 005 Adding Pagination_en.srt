1
00:00:00,180 --> 00:00:01,890
Hello and welcome to this lecture.

2
00:00:02,009 --> 00:00:05,120
So in this lecture, we are going to learn about pagination.

3
00:00:05,580 --> 00:00:09,900
So if you don't know what exactly pagination is, pagination is nothing.

4
00:00:09,900 --> 00:00:14,790
But while browsing some websites, you might have seen some numbers over here at the bottom of the Web

5
00:00:14,790 --> 00:00:17,930
page, which is page number one, two, three, four and five.

6
00:00:18,360 --> 00:00:20,100
And this is a common scenario.

7
00:00:20,100 --> 00:00:26,130
While you are using some sort of an e-commerce website, let's say if you search for a product, multiple

8
00:00:26,130 --> 00:00:29,220
products appear on our Web page, on a single page.

9
00:00:29,430 --> 00:00:35,370
And if you keep on scrolling down, scrolling down the list of the product ends and it actually gives

10
00:00:35,370 --> 00:00:41,220
you an option to switch to page number two, where you have other set of products so that one, two,

11
00:00:41,220 --> 00:00:46,500
three, four, five, or the page numbers or previous Onex, which we have at the bottom of the Web

12
00:00:46,500 --> 00:00:53,010
page, is nothing but pagination, which actually allow us to split up the products on our page from

13
00:00:53,010 --> 00:00:54,890
a single page to multiple pages.

14
00:00:55,590 --> 00:00:57,780
So we are going to learn exactly that.

15
00:00:58,020 --> 00:01:03,780
So right now, as you can see, we only have two movies in here, which is my movie and second movie.

16
00:01:04,080 --> 00:01:08,700
And let's say for some reason you only want to display three movies on a single page.

17
00:01:08,850 --> 00:01:17,280
So right now, if we actually go to admen and if we keep on adding these movies in here, so let's say

18
00:01:17,280 --> 00:01:22,980
I type in third movie, let's give some random rating.

19
00:01:23,310 --> 00:01:24,640
Let's add a few more.

20
00:01:25,590 --> 00:01:28,020
Let's see what movie.

21
00:01:30,510 --> 00:01:31,590
Let's see.

22
00:01:34,410 --> 00:01:35,910
Fifth movie.

23
00:01:39,380 --> 00:01:43,790
So once we have these bunch of movies and now if we go to.

24
00:01:45,770 --> 00:01:53,030
Movies, as you can see, we have all these movies in here, so we have one, two, three, four and

25
00:01:53,030 --> 00:01:53,830
five movies.

26
00:01:54,230 --> 00:02:00,020
So let's say for some reason you only want to display four movies in here instead of actually displaying

27
00:02:00,020 --> 00:02:00,420
five.

28
00:02:00,440 --> 00:02:02,150
So how exactly can we do that?

29
00:02:02,480 --> 00:02:04,700
We can do that by using pagination.

30
00:02:04,910 --> 00:02:09,430
So let's go ahead and learn how to use pagination in your Django site.

31
00:02:10,070 --> 00:02:14,900
So the very first thing which we do is that let's go ahead and fix this template a little bit.

32
00:02:15,290 --> 00:02:22,250
So let's add a bleak tag in here so that the movies are actually listed on top of each other.

33
00:02:22,460 --> 00:02:25,620
OK, so once we have that, let's now start with pagination.

34
00:02:26,240 --> 00:02:33,080
So in order to use pagination, you actually need to modify this particular view right here, because

35
00:02:33,080 --> 00:02:38,240
this view is something which defines how your page is going to fetch the data.

36
00:02:38,630 --> 00:02:44,370
So in case of pagination, what we want to do is that we first want to get the page number.

37
00:02:44,720 --> 00:02:50,960
So this page number one and four page number one, what we want to do is that instead of fetching all

38
00:02:50,960 --> 00:02:54,850
of these five movies over here, we only want to fetch four of them.

39
00:02:55,460 --> 00:03:02,600
So we want a way to actually go ahead and fetch the first four movies for the first page and for the

40
00:03:02,600 --> 00:03:03,260
second page.

41
00:03:03,260 --> 00:03:07,920
We want to go ahead and fetch the next four movies, so on and so forth.

42
00:03:08,240 --> 00:03:11,380
So pagination allows us to do exactly that.

43
00:03:11,630 --> 00:03:17,450
So let's go back here in The View and the very first thing which you need to do in order to use pagination

44
00:03:17,450 --> 00:03:19,460
is that you need to import pagination.

45
00:03:19,880 --> 00:03:26,210
So you type in from Django dot cool dot pazhani to import vaccinator.

46
00:03:26,600 --> 00:03:32,180
So once we have the opportunity, we go ahead and create the instance of the page you need right over

47
00:03:32,180 --> 00:03:32,470
here.

48
00:03:33,260 --> 00:03:43,130
So I can go ahead, go right over here and I can type in Virginia and make sure that you check the key

49
00:03:43,160 --> 00:03:44,510
sensitivity over here.

50
00:03:44,630 --> 00:03:49,580
This should be small P, not the capital P and now we go ahead and create the instance of the opportunity

51
00:03:49,580 --> 00:03:50,810
we have just imported.

52
00:03:51,170 --> 00:03:57,440
And we tell the spy agency that we actually want to split up the movie objects, which we have right

53
00:03:57,440 --> 00:04:04,610
over here so I can pass in moving objects over here and then I need to define the number of items I

54
00:04:04,610 --> 00:04:06,450
actually want on a single page.

55
00:04:06,470 --> 00:04:09,830
So we want four objects of all items on a single page.

56
00:04:09,830 --> 00:04:11,420
So I pass info here.

57
00:04:12,260 --> 00:04:18,110
So now once we have defined that to the budget naito, now we need to go ahead and we need to get the

58
00:04:18,110 --> 00:04:19,279
current page number.

59
00:04:19,730 --> 00:04:23,360
So how exactly can you go ahead and get the current page number?

60
00:04:24,020 --> 00:04:29,720
We can go ahead and get that by using the request, which is the request right here.

61
00:04:29,750 --> 00:04:33,290
So this request actually contains a lot of information.

62
00:04:33,470 --> 00:04:38,660
And one such piece of information here is nothing but the page number of the current page, which we

63
00:04:38,660 --> 00:04:39,110
are on.

64
00:04:39,590 --> 00:04:48,350
So in order to get access, I type in page equals request dot get so get is actually a method or you

65
00:04:48,350 --> 00:04:55,670
all know and this is actually going to get the data and we type and get together and in here we actually

66
00:04:55,670 --> 00:04:57,140
want the page number.

67
00:04:57,140 --> 00:05:00,090
Hence I'm going to type in page and single quotes.

68
00:05:00,740 --> 00:05:04,280
So now this is going to give us access to the page number.

69
00:05:04,370 --> 00:05:10,370
And now once we get the page number and once we have defined opportunity and once we have passed in

70
00:05:10,370 --> 00:05:17,120
the number of objects we want, we now actually go ahead and get the items on that particular page.

71
00:05:17,690 --> 00:05:25,730
So in order to do that, we again go ahead and type in movie objects or you can create a new variable

72
00:05:25,730 --> 00:05:26,650
over here as well.

73
00:05:26,990 --> 00:05:33,020
But I'm going to just go ahead and use moving objects equals that's going to be pajarito, which is

74
00:05:33,020 --> 00:05:39,620
nothing but this patinated instance which we have just created and we type in dot get and the school

75
00:05:39,630 --> 00:05:45,910
page and then we it the page number over here so I can type in page over here.

76
00:05:46,790 --> 00:05:52,610
And this is actually going to fetch the objects which are present on the current page and not all of

77
00:05:52,610 --> 00:05:53,890
the objects right over here.

78
00:05:54,410 --> 00:05:59,690
And now this is actually going to fetch the objects and it's going to pass it in over here and the movie

79
00:05:59,690 --> 00:06:00,340
objects.

80
00:06:00,350 --> 00:06:05,150
And if you go ahead and name this something else, you just need to make sure that you pass on that

81
00:06:05,150 --> 00:06:07,950
as a context over here and not movie objects.

82
00:06:08,210 --> 00:06:14,330
So now once we go ahead and do that, we are pretty much done modifying the view right here and now.

83
00:06:14,330 --> 00:06:20,720
If we actually hop back over here and if we hit refresh, as you can see, we only have four movies

84
00:06:20,720 --> 00:06:26,210
over here and we don't actually have access to the first movie because right now we are on page number

85
00:06:26,210 --> 00:06:26,510
one.

86
00:06:27,230 --> 00:06:31,760
So how exactly can you go ahead and jump on to page number two in this case?

87
00:06:31,970 --> 00:06:36,230
So in order to do that, you just go here and you type in question.

88
00:06:36,230 --> 00:06:40,240
Mark Page equals two and hit enter.

89
00:06:40,550 --> 00:06:43,910
And as you can see now, you have access to the second page.

90
00:06:44,580 --> 00:06:51,210
Now, let's see what happens if I do beach equals three, as you can see it still this fifth movie because

91
00:06:51,210 --> 00:06:53,450
we don't really have page number three.

92
00:06:54,240 --> 00:07:00,180
And now if I want to switch back to page number one, I can type in page equals one and I will get the

93
00:07:00,180 --> 00:07:01,000
page number one.

94
00:07:01,710 --> 00:07:03,330
So that's all fine and good.

95
00:07:03,660 --> 00:07:07,620
We can actually access all the movies by visiting different pages.

96
00:07:07,920 --> 00:07:11,370
But how is the user supposed to go ahead and do this?

97
00:07:11,400 --> 00:07:12,260
You are right here.

98
00:07:12,960 --> 00:07:18,000
So we don't actually want the user to enter the you are here, but we want to provide some sort of an

99
00:07:18,000 --> 00:07:23,420
option of buttons over here which allow the user to navigate through the entire website.

100
00:07:24,030 --> 00:07:28,560
So we actually need to go ahead and modify this particular template right here.

101
00:07:28,950 --> 00:07:31,220
So how exactly can we go ahead and do that?

102
00:07:31,920 --> 00:07:37,770
So in order to do that, we need to go to the template and make a few changes in here.

103
00:07:37,980 --> 00:07:41,700
We already have this list here and we don't want to do anything with that.

104
00:07:42,120 --> 00:07:46,970
But the thing which we want to do is that we want to go ahead, add the navigation over here.

105
00:07:47,160 --> 00:07:52,020
So the way in which we are going to have this navigation is that we are going to have the first page,

106
00:07:52,020 --> 00:07:55,770
which is the link to the first page, and we'll call it as page one.

107
00:07:56,460 --> 00:07:59,010
Then we will have the link to the previous page.

108
00:07:59,400 --> 00:08:05,140
So we will say something like previous so that previous is going to actually give access to the previous

109
00:08:05,160 --> 00:08:05,370
piece.

110
00:08:05,400 --> 00:08:07,360
So let's go ahead and define that over here.

111
00:08:07,950 --> 00:08:14,640
So the very first thing which we need to check is that we need to check if movie objects actually has

112
00:08:14,640 --> 00:08:15,770
a previous object.

113
00:08:15,930 --> 00:08:19,740
So we type in every movie, underscore objects.

114
00:08:19,740 --> 00:08:24,370
DOT has underscore previous.

115
00:08:24,390 --> 00:08:31,590
So has previous is actually a method which is going to tell us if this actually has previous items and

116
00:08:31,590 --> 00:08:33,960
we also make sure to end the if over here.

117
00:08:33,960 --> 00:08:37,289
So I'll type end over here and in here.

118
00:08:37,530 --> 00:08:44,280
What we want to do is that if it has the previous item, we actually want to get the page number of

119
00:08:44,280 --> 00:08:45,580
that particular item.

120
00:08:45,600 --> 00:08:47,700
So in here I'll find a link.

121
00:08:47,700 --> 00:08:51,120
So I'll open a history of equals.

122
00:08:51,120 --> 00:08:55,670
And for now, let's keep this thing as empty and we'll see something like previous.

123
00:08:55,680 --> 00:09:00,600
And once we have that now, we go ahead and find another if statement over here.

124
00:09:01,200 --> 00:09:06,840
So now, just as we have checked, if the object has some previous object, we also need to go ahead

125
00:09:06,840 --> 00:09:11,550
and make sure that if a particular object also has item next to it.

126
00:09:11,610 --> 00:09:20,160
So it open another if statement over here and we type in if movie objects dot and just as we have used

127
00:09:20,160 --> 00:09:22,350
has previous we use has.

128
00:09:22,350 --> 00:09:24,310
And let's go next over here.

129
00:09:25,110 --> 00:09:33,900
So once we have that we again go ahead and type in and therefore we're here and we again go ahead and

130
00:09:33,900 --> 00:09:37,830
define the tag, which is the link for this particular page.

131
00:09:38,400 --> 00:09:41,340
So we type in this as next.

132
00:09:42,940 --> 00:09:48,790
And now once we have the person next now we'll go ahead and also add the links to the first and the

133
00:09:48,790 --> 00:09:55,720
last page as well so I can go ahead and type in each riff and in here I can type in something like post.

134
00:09:56,860 --> 00:10:00,010
And in here, I can go ahead and type in.

135
00:10:01,000 --> 00:10:04,980
It's Trev and this is going to be last.

136
00:10:05,890 --> 00:10:08,960
OK, so once I have these things, we are pretty much good to go.

137
00:10:09,070 --> 00:10:11,780
Now, let's fill up the places for links here.

138
00:10:11,890 --> 00:10:18,460
So for the first page, the link is going to be nothing but questionmark piece equals one because that's

139
00:10:18,460 --> 00:10:20,800
what you actually want to visit the first page.

140
00:10:21,400 --> 00:10:27,730
Now, how exactly are you going to get the page number of the previous page so you can do that by using

141
00:10:28,300 --> 00:10:30,410
the movie objects, which we have right here.

142
00:10:30,970 --> 00:10:34,240
So we type in questionmark page equals.

143
00:10:34,780 --> 00:10:40,510
And instead of defining a hardcoded number in here, I can just go ahead and type and movie and the

144
00:10:40,510 --> 00:10:45,880
school objects dot and I can type in previous.

145
00:10:49,890 --> 00:10:52,910
And the school page and the school, no.

146
00:10:53,730 --> 00:11:01,080
So this is actually going to give me access to the previous page number of movies object, and in the

147
00:11:01,080 --> 00:11:07,110
same way, we are actually going to go ahead and use the same thing for the next as well.

148
00:11:07,260 --> 00:11:11,630
So I can just copy this particular thing, be set for the next as well.

149
00:11:12,330 --> 00:11:19,350
And in here, instead of having previous page number, I will just go ahead and use the next page number

150
00:11:19,530 --> 00:11:21,230
so I can type in next over here.

151
00:11:23,180 --> 00:11:28,730
And for the last page, in order to get access to the last page, obviously we do not know what the

152
00:11:28,730 --> 00:11:30,030
last page is going to be.

153
00:11:30,470 --> 00:11:32,000
So instead, we type in.

154
00:11:33,670 --> 00:11:40,420
Question mark Paige, movies object, and inside the movies object, we can get access to the last page

155
00:11:40,780 --> 00:11:45,250
and to get access to the last page, we just need to know the number of pages we have.

156
00:11:45,440 --> 00:11:51,070
So here I can simply type in numbers and the school pages.

157
00:11:51,280 --> 00:11:57,310
Now, this is going to give me access to the number of pages which the site has, and that number of

158
00:11:57,310 --> 00:12:00,650
pages is nothing but the number of our last page.

159
00:12:01,330 --> 00:12:04,300
So once we go ahead and do that, we are pretty much good to go.

160
00:12:04,900 --> 00:12:07,110
Now we have access to the first page.

161
00:12:07,120 --> 00:12:08,940
We have access to the previous page.

162
00:12:08,950 --> 00:12:12,010
We have access to the next page and the last page as well.

163
00:12:12,370 --> 00:12:17,910
Now, the only thing which we need to define is that we need to define which page are we currently on.

164
00:12:18,340 --> 00:12:21,400
And in order to do that, I'll simply type in page here.

165
00:12:21,820 --> 00:12:28,480
And we want to see page one of two of page five of six of page ten of hundred.

166
00:12:28,900 --> 00:12:30,630
So here I'll type in page.

167
00:12:30,700 --> 00:12:35,980
So in order to get access to the current page, I again type in moving objects.

168
00:12:35,980 --> 00:12:37,710
Dot No.

169
00:12:37,780 --> 00:12:42,630
So it's going to give me access to the current page and will type in off.

170
00:12:44,320 --> 00:12:53,620
And here I'll type in movie objects, dot number of pages to get access to the number of pages which

171
00:12:53,620 --> 00:12:55,760
we actually have on our web app.

172
00:12:56,410 --> 00:12:59,530
So once we go ahead and do that, we are pretty much good to go.

173
00:12:59,830 --> 00:13:03,890
Now let's see if this thing actually works on our homepage.

174
00:13:03,920 --> 00:13:11,890
So if I go ahead and hit refresh, as you can see, it says page one of and we don't have any number

175
00:13:11,890 --> 00:13:12,310
right here.

176
00:13:12,390 --> 00:13:19,450
OK, so the problem here is that in order to access the number of pages, we need to type in movie objects

177
00:13:19,450 --> 00:13:21,640
dot pazhani to.

178
00:13:23,380 --> 00:13:27,220
Number of pages, so the same thing applies here as well.

179
00:13:27,250 --> 00:13:32,640
So you want movie objects d'art as you need to draw number of pages.

180
00:13:32,860 --> 00:13:34,570
So once we go ahead, fix that.

181
00:13:34,930 --> 00:13:38,740
And now if I click refresh, as you can see, it's page one of two.

182
00:13:38,890 --> 00:13:42,520
So when I click next, it says page two of two.

183
00:13:42,700 --> 00:13:44,410
So now I can go to first.

184
00:13:44,890 --> 00:13:46,120
I can go to last.

185
00:13:47,020 --> 00:13:54,280
I can also go to next and I can also go to previous, so now as you can see, we can navigate through

186
00:13:54,280 --> 00:13:57,960
the number of Web pages which we actually have on our website.

187
00:13:58,750 --> 00:14:04,660
So this might look a little bit boring over here, but we can actually go ahead silat up using bootstrap.

188
00:14:04,950 --> 00:14:07,300
So we are going to do that in the upcoming lecture.

189
00:14:07,310 --> 00:14:11,290
But for now, you just need to remember how the generator actually works.

190
00:14:11,920 --> 00:14:15,640
So the opportunities working in the viewport, it's quite simple.

191
00:14:15,940 --> 00:14:22,690
You just import pajarito, then you simply go ahead and create an instance of the generator and give

192
00:14:22,690 --> 00:14:24,820
it the object, which we have.

193
00:14:25,240 --> 00:14:30,610
And then you simply pass in the number of items which you want to display on the Web page, on a single

194
00:14:30,610 --> 00:14:30,970
page.

195
00:14:31,000 --> 00:14:34,490
So in this case, we wanted for you can modify this as well.

196
00:14:34,510 --> 00:14:38,920
And then the first thing which you need to do is that you need to get access to the page.

197
00:14:39,040 --> 00:14:45,340
So we type in page equals request dot, get dot, get page to get access to the current page.

198
00:14:45,820 --> 00:14:51,160
And once we have that access to the single page, we simply pass that thing to the page NATO and we

199
00:14:51,160 --> 00:14:57,880
get the movie objects for that particular page and then we simply pass that thing as the context.

200
00:14:58,360 --> 00:15:03,640
Now the most important thing which we need to remember here is how to actually get access to the different

201
00:15:03,640 --> 00:15:04,880
page numbers over here.

202
00:15:05,500 --> 00:15:11,290
So these things might actually look big and confusing, but these things are nothing but the page number.

203
00:15:11,710 --> 00:15:14,730
So this thing actually gives us access to the previous page.

204
00:15:14,740 --> 00:15:18,670
So let's see if you on page number two, this is going to see one.

205
00:15:18,700 --> 00:15:21,540
If you on page number three, this is going to see two.

206
00:15:22,420 --> 00:15:28,450
So in order to get access to the previous page, you simply use the same movies object and to get access

207
00:15:28,450 --> 00:15:31,840
to the actual page number, you use the previous page number method.

208
00:15:31,870 --> 00:15:35,170
Now we do the same thing for the next page number as well.

209
00:15:35,320 --> 00:15:38,160
So we do have one movie objects dot next page number.

210
00:15:38,800 --> 00:15:44,380
And in order to get access to the number of pages, we simply type in movie objects.

211
00:15:44,380 --> 00:15:50,870
That is nothing but the objects, dot, pajarito, dot the number of pages.

212
00:15:50,920 --> 00:15:52,410
So that's it for this lecture.

213
00:15:52,780 --> 00:15:56,440
And I hope you guys be able to understand how pagination works.

214
00:15:56,800 --> 00:16:00,750
So in the upcoming lecture, we are going to learn something about sitemap.

215
00:16:00,790 --> 00:16:04,840
So thank you very much for watching and I'll see you guys next time.

216
00:16:05,470 --> 00:16:06,040
Thank you.

