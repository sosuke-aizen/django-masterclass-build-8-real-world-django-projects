1
00:00:00,420 --> 00:00:05,939
Hello and welcome to this news section, and in this section, we will actually go ahead and learn a

2
00:00:05,939 --> 00:00:08,820
few more important concepts in Django.

3
00:00:09,300 --> 00:00:16,020
So in this particular section, we are going to learn about what social environment site maps pagination

4
00:00:16,230 --> 00:00:20,220
search and filtering and groups and permissions in Django.

5
00:00:20,820 --> 00:00:27,240
So these are some special features which can be used in Django to make your application much more better.

6
00:00:27,900 --> 00:00:34,050
So for this specific section, we are actually going to make a completely different Web app and this

7
00:00:34,050 --> 00:00:40,830
is actually going to be just a sample Web app to give you guys an idea about these following features.

8
00:00:41,250 --> 00:00:46,920
So the first thing which I would recommend you guys to do is that I would recommend you do create a

9
00:00:46,920 --> 00:00:50,930
new Huldah and we are actually going to create a new project in here.

10
00:00:51,060 --> 00:00:56,640
And the very first thing which we are going to learn in this section is we are going to learn about

11
00:00:56,640 --> 00:00:59,220
something which is called as a watch environment.

12
00:00:59,670 --> 00:01:05,099
So right now, I actually have a folder which is called as Newbrough on my desktop.

13
00:01:05,310 --> 00:01:12,210
And what I will do here is that I'll navigate down to desktop slash newgroup.

14
00:01:15,060 --> 00:01:21,270
And in here, what we will do is that will create our very own Shango project, but this time, instead

15
00:01:21,270 --> 00:01:27,270
of directly creating the Shango project, we're actually going to use something which is called as a

16
00:01:27,270 --> 00:01:28,550
watchful environment.

17
00:01:28,590 --> 00:01:31,530
So what exactly is a virtual environment?

18
00:01:32,010 --> 00:01:38,490
So a virtual environment is nothing, but it's something which allows us to create an isolated environment

19
00:01:38,490 --> 00:01:39,950
to create projects.

20
00:01:40,830 --> 00:01:42,900
So what do you exactly mean by that?

21
00:01:43,470 --> 00:01:50,250
So let's say if you want to create a project and let's say if you want to run that project in multiple

22
00:01:50,250 --> 00:01:51,250
Django versions.

23
00:01:51,540 --> 00:01:57,430
So currently right now we have a version which is Shango to install on our computer.

24
00:01:57,990 --> 00:02:05,130
So let's say if you want to install another version of Django on your computer for this specific project.

25
00:02:05,520 --> 00:02:12,120
So in that case, what we can do is that we can create an environment and we can install any version

26
00:02:12,120 --> 00:02:18,360
of Django which we want in that environment, and then we can create a project and use that environment

27
00:02:18,360 --> 00:02:23,700
on that project and we can run the project using any Django version.

28
00:02:24,300 --> 00:02:31,170
Social environment is nothing but an isolated environment which we can create for a project in which

29
00:02:31,170 --> 00:02:36,870
we can install any version of Django or any version of any software for that matter.

30
00:02:37,590 --> 00:02:41,720
So let's now learn how exactly can we go ahead and use virtual environment.

31
00:02:42,270 --> 00:02:47,820
So the very first thing which you need to do is that you need to go ahead and install virtual environments

32
00:02:47,970 --> 00:02:50,580
in order to install virtual environment.

33
00:02:50,590 --> 00:02:55,920
The first thing which you need to do is that you need to type in BEP install.

34
00:02:56,940 --> 00:03:03,930
What you'll envy and when you go ahead and hit enter the is actually going to install virtual environment

35
00:03:03,930 --> 00:03:04,380
for you.

36
00:03:04,590 --> 00:03:10,680
So as you can see in my case now the virtual environment is installing and it seems that successfully

37
00:03:10,680 --> 00:03:17,190
installed what your environment was, sixteen point seven point three now that the virtual environment

38
00:03:17,190 --> 00:03:17,950
is installed.

39
00:03:18,270 --> 00:03:22,170
Let's now learn how you can go ahead and create a virtual environment.

40
00:03:22,860 --> 00:03:28,470
So whenever you want to create a virtual environment, first of all, you navigate down to the project

41
00:03:28,470 --> 00:03:31,450
where you actually want to start up your Shango project.

42
00:03:31,470 --> 00:03:34,950
So I actually have this new project on my desktop.

43
00:03:35,400 --> 00:03:43,500
And now in order to create a virtual environment, you type in what I really envy and then you actually

44
00:03:43,500 --> 00:03:46,610
need to name this virtual environment as something.

45
00:03:47,070 --> 00:03:53,280
So let's say I named this environment as EMV itself, so I type in envy and hit enter.

46
00:03:54,810 --> 00:04:00,300
And now it's actually going to create the virtual environment for us, as you can see now, the virtual

47
00:04:00,300 --> 00:04:01,850
environment has been created.

48
00:04:02,250 --> 00:04:10,020
And now if you actually have a look at this project folder, as you can see, we now have this ESV directly

49
00:04:10,020 --> 00:04:10,800
right over here.

50
00:04:11,640 --> 00:04:18,269
So now in order to use this virtual environment, you first need to go ahead and activate it.

51
00:04:18,750 --> 00:04:22,580
So how exactly can you go ahead and activate the social environment?

52
00:04:22,890 --> 00:04:29,460
So the steps to activate this virtual environment for Windows and for Mac users is different.

53
00:04:29,880 --> 00:04:33,150
So I'll show you guys how to do it on Mac, so on.

54
00:04:33,150 --> 00:04:39,630
Mac, what you do is that once you enter this project folder, you first CD into the environment.

55
00:04:39,750 --> 00:04:41,630
So we'll type in CD N.V..

56
00:04:42,510 --> 00:04:51,360
So when you go into this EMV folder, you simply open source spaceborne slash activate.

57
00:04:51,510 --> 00:04:57,540
So when I go ahead and hit enter, as you can see now, you can see this environment or the virtual

58
00:04:57,540 --> 00:05:03,180
environment name inside of parenthesis right here on the left hand side, which means that the virtual

59
00:05:03,180 --> 00:05:05,430
environment has been now activated.

60
00:05:06,000 --> 00:05:11,910
So now how exactly can you activate the virtual environment and windows, so on windows in order to

61
00:05:11,910 --> 00:05:13,620
activate the virtual environment?

62
00:05:13,650 --> 00:05:16,070
First of all, CD into the environment.

63
00:05:16,080 --> 00:05:22,830
So type and CD N.V. And after going into the environment, you simply go ahead, go into the scripts

64
00:05:22,830 --> 00:05:29,470
folder so you type in CD scripts and then when you into the scripts folder simply type and activate.

65
00:05:29,880 --> 00:05:38,540
So for Windows simply to open CD and then you type in CD scripts and then you type in activate.

66
00:05:38,870 --> 00:05:41,070
So this is how you do it for windows.

67
00:05:41,520 --> 00:05:47,820
So in my case, as I'm using Mac, I have followed these steps for Mac and now once the environment

68
00:05:47,820 --> 00:05:50,430
is activated, you will get this sign right here.

69
00:05:50,430 --> 00:05:53,100
That is, you'll have the environment name in parenthesis.

70
00:05:53,160 --> 00:05:58,800
OK, so now once it is activated, let's now go ahead and go back to the machine directly.

71
00:05:58,830 --> 00:06:04,710
So currently we are into the EMV directly and you can actually create a project in this Ianuzzi directory

72
00:06:04,710 --> 00:06:05,220
as well.

73
00:06:05,550 --> 00:06:07,960
But it's recommended that you don't do so.

74
00:06:08,460 --> 00:06:09,570
So let's go back.

75
00:06:09,840 --> 00:06:11,440
So CD dot dot.

76
00:06:12,480 --> 00:06:14,220
So we go back to the new Broyhill.

77
00:06:14,760 --> 00:06:20,610
And one more thing which you need to learn over here is how exactly can you go ahead and deactivate

78
00:06:20,670 --> 00:06:22,340
this Wortzel environment?

79
00:06:22,380 --> 00:06:28,320
So in order to deactivate the virtual environment on Mac as well as Windows, you can simply type in

80
00:06:28,320 --> 00:06:30,960
the activate and hit enter.

81
00:06:31,380 --> 00:06:35,220
And as you can see, the virtual environment is now deactivated.

82
00:06:35,730 --> 00:06:39,270
So again, to activate it back, you can again go to CD.

83
00:06:40,360 --> 00:06:45,070
And B, then I can simply open source Ben.

84
00:06:47,460 --> 00:06:55,610
Slash activist and the virtual environment is going to be activated now once the environment is activated,

85
00:06:55,620 --> 00:07:00,550
let's learn how exactly can we go ahead and install Shango using this environment.

86
00:07:01,110 --> 00:07:06,360
So first and foremost thing, which you need to do is that you need to go back to your main directory,

87
00:07:06,360 --> 00:07:09,470
which is new over here.

88
00:07:09,780 --> 00:07:13,250
And to do so, you simply type in seed, dot, dot.

89
00:07:13,710 --> 00:07:16,890
And once you are here, you can create your triangle project here.

90
00:07:17,520 --> 00:07:23,190
So even before you create a triangle project, you now need to understand that this is a new environment,

91
00:07:23,220 --> 00:07:28,060
which means that Django is not going to be installed by default in this environment.

92
00:07:28,500 --> 00:07:32,960
So now you need to go ahead and install Django right from scratch.

93
00:07:33,420 --> 00:07:42,330
So here we type in PIP, install Django, and now, as you can see, Django is going to be successfully

94
00:07:42,330 --> 00:07:44,060
installed for you from scratch.

95
00:07:44,790 --> 00:07:46,860
So we are using Django to point to.

96
00:07:48,910 --> 00:07:52,220
And as you can see, Django is now successfully installed.

97
00:07:52,810 --> 00:07:57,250
So now once Django is installed, we can now go ahead and create a Django project.

98
00:07:57,520 --> 00:08:02,380
So, as usual, we type in the command as Django admen.

99
00:08:03,750 --> 00:08:12,870
Start a project and we will name our project as my site hit, enter and then click into the my site

100
00:08:13,380 --> 00:08:15,420
and let's see what do we have here?

101
00:08:15,750 --> 00:08:18,690
So if you go here and if you type in Python.

102
00:08:20,020 --> 00:08:27,820
This should actually be Python three, manage the IPY run server and hit enter.

103
00:08:29,330 --> 00:08:36,110
And as you can see, the Sylwia is now up and running, so let me just open up the browser and if we

104
00:08:36,110 --> 00:08:43,340
go here and hit refresh, as you can see on Shango project is now up and running on the browser, which

105
00:08:43,340 --> 00:08:48,600
means we have successfully created a single project using the virtual environment.

106
00:08:49,220 --> 00:08:50,800
So that's it for this lecture.

107
00:08:50,930 --> 00:08:57,410
And I hope you guys were able to install virtual environment, activate the virtual environment, install

108
00:08:57,410 --> 00:09:01,760
Django using virtual environment and eventually run your project on the Web browser.

109
00:09:02,240 --> 00:09:07,580
So if you have any issues or problems regarding installing virtual environment, let me know in the

110
00:09:07,580 --> 00:09:10,440
Q&A section and I will be there to help you out.

111
00:09:11,030 --> 00:09:12,850
So that's it for this lecture.

112
00:09:12,890 --> 00:09:18,890
And in the next lecture, we are going to actually go ahead and create a dummy model and we are going

113
00:09:18,890 --> 00:09:20,780
to learn something about pagination.

114
00:09:21,080 --> 00:09:25,100
So thank you very much for watching and I'll see you guys next time.

115
00:09:25,580 --> 00:09:26,180
Thank you.

