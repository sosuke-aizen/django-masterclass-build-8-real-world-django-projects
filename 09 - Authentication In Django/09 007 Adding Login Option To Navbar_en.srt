1
00:00:00,360 --> 00:00:02,160
Hello and welcome to this lecture.

2
00:00:02,310 --> 00:00:08,670
So in this lecture, we will learn how to go ahead and add some things over here so that we know that

3
00:00:08,670 --> 00:00:10,320
the user is logged in.

4
00:00:10,860 --> 00:00:13,920
So right now, we don't know if the user is logged in or not.

5
00:00:14,070 --> 00:00:20,640
And hence, our aim is to add something to this particular navigation bar right here so that whenever

6
00:00:20,640 --> 00:00:25,650
we are logged in, it should say something like log out and whenever we are logged out, it should say

7
00:00:25,650 --> 00:00:27,090
something like log in.

8
00:00:27,540 --> 00:00:30,060
So how exactly can we go ahead and do that?

9
00:00:30,300 --> 00:00:36,700
So we all know that this particular navigation bar is actually designed in the be stored e-mail file.

10
00:00:36,780 --> 00:00:43,080
So in order to go ahead and make some changes over here, we need to go ahead and go to be stored.

11
00:00:43,100 --> 00:00:48,780
HTML file, which is this file right here, which is nothing but the file, which contains a navigation

12
00:00:48,780 --> 00:00:49,030
bar.

13
00:00:49,260 --> 00:00:51,700
So as you can see, this is an right here.

14
00:00:52,260 --> 00:00:58,710
So in this particular Nappa, we need to add a log out option whenever the user is logged in and whenever

15
00:00:58,710 --> 00:01:01,750
the user is logged out, we need to display the log in option.

16
00:01:02,070 --> 00:01:07,060
So how exactly do we know when the user is logged in and when the user is locked out?

17
00:01:07,470 --> 00:01:14,370
So in order to do that, Shango actually provides us with a variable which contains the current user,

18
00:01:14,400 --> 00:01:21,120
and it also contains the information about the current user, that is, if the current user is authenticated

19
00:01:21,120 --> 00:01:21,670
or not.

20
00:01:22,140 --> 00:01:27,360
So using that particular variable, we can identify the user is logged in or locked out.

21
00:01:27,810 --> 00:01:30,740
So what we do here is that we add a conditional.

22
00:01:30,930 --> 00:01:38,880
So I'm just going to type in the syntax, which is the syntax right here, and then I'm going to type

23
00:01:38,880 --> 00:01:44,190
in if and then we need to access the user variable, which Django provides.

24
00:01:44,400 --> 00:01:50,710
So I'll type in if user DOT is underscore authenticated.

25
00:01:51,090 --> 00:01:57,840
So this is authenticated attribute of the user is going to tell us if the current user is authenticated

26
00:01:57,840 --> 00:02:00,690
or not and if the current users authenticated.

27
00:02:00,930 --> 00:02:03,410
We want to display the log out option.

28
00:02:03,420 --> 00:02:05,370
So I'll type in each ref.

29
00:02:06,300 --> 00:02:09,479
That's going to be the you are all for log out.

30
00:02:09,960 --> 00:02:17,010
So in here the URL for logout is you are all and then the name of that you are simply log out.

31
00:02:17,010 --> 00:02:21,200
So I'll type that thing over here and let's name this thing as a out.

32
00:02:22,320 --> 00:02:28,170
So when the user is logged then that is the user is authenticated, then we want to display this or

33
00:02:28,170 --> 00:02:36,270
else we want to display log in, which means that if the user is not authenticated, then we want to

34
00:02:36,270 --> 00:02:38,010
display the log in option.

35
00:02:38,370 --> 00:02:45,750
So here I'll type in health and then in the else block who will go ahead and type in each address and

36
00:02:45,750 --> 00:02:49,110
the URL for login, which is nothing but log in itself.

37
00:02:49,110 --> 00:02:50,040
So I'll type in.

38
00:02:50,070 --> 00:02:57,020
You all log in and I'll simply type in log in as the display name of the zero.

39
00:02:57,720 --> 00:03:05,820
And now finally we end this particular block by typing in and if so, once we go ahead and do that,

40
00:03:05,820 --> 00:03:07,440
we are pretty much good to go.

41
00:03:07,470 --> 00:03:10,920
So now let's go back over here and now hit refresh.

42
00:03:10,920 --> 00:03:16,680
And as you can see, as the user is currently not logged, then it's showing us the log in option.

43
00:03:16,710 --> 00:03:22,200
So when I go ahead and click login, it's going to ask me to enter the username and password.

44
00:03:22,200 --> 00:03:23,670
So I'll go ahead and do that.

45
00:03:23,700 --> 00:03:25,500
So let me just go ahead and log in.

46
00:03:28,720 --> 00:03:34,770
So now, once I'm logged in, as you can see now, this thing shows the option to log out, so when

47
00:03:34,780 --> 00:03:40,960
I can go back and click on Log Out and when we are logged out and now when I try to go ahead and access

48
00:03:40,960 --> 00:03:46,790
this particular page now as I am logged out, it's going to again show me back the option to log in.

49
00:03:46,900 --> 00:03:52,510
So that means now we at least know that when we actually logged into a website, logged in and to our

50
00:03:52,510 --> 00:03:53,980
web and when we are not.

51
00:03:54,010 --> 00:03:59,770
So that's how you can go ahead and identify if the user is logged in or locked out by using the user

52
00:03:59,770 --> 00:04:01,810
variable, which is the user.

53
00:04:01,820 --> 00:04:06,760
And then in that, we are actually going to access the attribute which is is authenticated.

54
00:04:07,150 --> 00:04:09,220
So this is authenticated attribute.

55
00:04:09,610 --> 00:04:14,260
Lets us know if the current user is logged in or locked out and making use of the same.

56
00:04:14,530 --> 00:04:18,269
We have put up a conditional statement over here and the navigation bar.

57
00:04:18,279 --> 00:04:22,120
So whenever the user is logged, then it's going to show us the log out option.

58
00:04:22,390 --> 00:04:26,100
And whenever the user is logged out, it's going to show us the log in option.

59
00:04:26,110 --> 00:04:27,610
So that's it for this lecture.

60
00:04:27,760 --> 00:04:31,870
And hopefully you guys be able to understand how to add this functionality.

61
00:04:32,260 --> 00:04:37,660
Now, the only thing which remains is that using this particular log in and log out functionality,

62
00:04:37,890 --> 00:04:44,200
we need to put certain restrictions on certain routes, which means that currently, if you go to this

63
00:04:44,200 --> 00:04:50,020
page right here, as you can see, this page is accessible by anyone, which means that even if you

64
00:04:50,020 --> 00:04:52,950
are locked out, you can still access this particular page.

65
00:04:53,230 --> 00:04:59,630
But let's say if you want to restrict access to certain pages to only the logged in users.

66
00:04:59,650 --> 00:05:02,840
So in the next lecture, we will learn how exactly we can do that.

67
00:05:03,100 --> 00:05:08,440
That is how to restrict access to certain pages to just the logged in users.

68
00:05:08,710 --> 00:05:10,450
So thank you very much for watching.

69
00:05:10,450 --> 00:05:12,460
And I'll see you guys next time.

70
00:05:12,970 --> 00:05:13,600
Thank you.

