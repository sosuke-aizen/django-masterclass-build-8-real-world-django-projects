1
00:00:00,330 --> 00:00:02,080
Hello and welcome to this lecture.

2
00:00:02,130 --> 00:00:07,630
And in this lecture, let's work on adding the profile image to our profile page right here.

3
00:00:08,189 --> 00:00:10,610
So right now, we don't have any profile image right here.

4
00:00:10,620 --> 00:00:13,500
We just have the user of the profile.

5
00:00:13,950 --> 00:00:19,110
And now in order to add an image over here, what you simply have to do is that you have to go to the

6
00:00:19,110 --> 00:00:21,360
template, which is profile dot shyamal.

7
00:00:21,630 --> 00:00:25,020
And in here you simply need to add the image tag.

8
00:00:25,050 --> 00:00:26,370
So I'll type and image.

9
00:00:26,370 --> 00:00:35,190
And the source for this image is going to be equal to nothing but user dot profile dot image dot you

10
00:00:35,190 --> 00:00:35,340
are.

11
00:00:35,950 --> 00:00:43,170
So if you remember how in the previous lecture we were able to access the image of a profile, you simply

12
00:00:43,170 --> 00:00:50,700
type in user because that's a user model and inside the user, we want to access the profile, so we

13
00:00:50,700 --> 00:00:52,630
want to access the profile of the user.

14
00:00:52,650 --> 00:00:55,800
And in that profile, we want to access the image feel.

15
00:00:56,160 --> 00:01:00,410
And in here specifically, we want the wall of that image.

16
00:01:00,420 --> 00:01:01,290
So I'll type in.

17
00:01:01,290 --> 00:01:02,300
You are all over here.

18
00:01:03,720 --> 00:01:10,530
OK, so once you have this, your image will be loaded up, however, if you now go to the template

19
00:01:10,530 --> 00:01:15,210
and if you hit refresh, as you can see, you are going to get an error over here.

20
00:01:16,290 --> 00:01:22,030
And that's because there is one issue with using the images stored on our computer.

21
00:01:22,620 --> 00:01:29,160
So as I mentioned, these images which we are using right here, are not actually stored into the database,

22
00:01:29,160 --> 00:01:35,310
but they are actually stood on the server or in this case, they are stored on our computer, which

23
00:01:35,310 --> 00:01:41,300
means that these images, which we are using or here are actually the static files.

24
00:01:41,820 --> 00:01:47,190
So now whenever you are working with static files, there is some modification which you need to do

25
00:01:47,220 --> 00:01:53,760
with your code, and that is to basically go ahead and include the you are all of the directly which

26
00:01:53,760 --> 00:01:56,290
actually has those static files.

27
00:01:56,790 --> 00:01:59,190
So how exactly can you go ahead and do that?

28
00:01:59,190 --> 00:02:05,280
And where exactly do you need to add the you are all for those static files so that what you simply

29
00:02:05,280 --> 00:02:11,250
do is that you go to Google and you simply go ahead and search for something like uploading.

30
00:02:12,220 --> 00:02:20,590
Static files and jangle So this is going to give you a guide to work with the static files which are

31
00:02:20,590 --> 00:02:21,160
uploaded.

32
00:02:21,320 --> 00:02:26,890
So in here, if you scroll down a little bit, as you can see, it says serving files uploaded by a

33
00:02:26,890 --> 00:02:28,780
user during development.

34
00:02:29,140 --> 00:02:34,780
So which means that this is the code which you need to include in your usual patterns to be able to

35
00:02:34,780 --> 00:02:36,310
use those static files.

36
00:02:36,340 --> 00:02:40,450
So now what I will simply do here is that Al simply copy this entire code.

37
00:02:40,690 --> 00:02:42,250
So just copy that.

38
00:02:42,250 --> 00:02:49,240
And now you need to go ahead, go to your code editor, go into the Alstott profile of your main project,

39
00:02:49,480 --> 00:02:51,490
which is my site.

40
00:02:52,060 --> 00:02:55,020
So the mine site in the URL stop by you.

41
00:02:55,090 --> 00:02:57,430
We actually need to go ahead and paste these.

42
00:02:57,430 --> 00:03:03,580
You are old patterns and let's just simply get this imports from here, paste them up over here.

43
00:03:04,120 --> 00:03:08,790
And now, as this is also you are a pattern and we want to use these.

44
00:03:08,800 --> 00:03:10,800
You are old patterns with these patterns.

45
00:03:10,810 --> 00:03:17,890
What we simply do is that we make it your all patterns plus equal to which means that these patterns

46
00:03:17,890 --> 00:03:19,550
are going to be added up over here.

47
00:03:20,360 --> 00:03:24,980
Okay, so now once we go ahead and fix that, let's see if the image is loaded up now.

48
00:03:25,000 --> 00:03:30,910
So if I go ahead and hit refresh, as you can see now, we actually have our image over here.

49
00:03:31,310 --> 00:03:35,580
So that's how you can go ahead and add your image up to your profile page.

50
00:03:35,590 --> 00:03:41,050
So the only thing which we have done here is that we have actually access the image up over here using

51
00:03:41,050 --> 00:03:42,030
the image, you are told.

52
00:03:42,280 --> 00:03:48,010
And the next most important thing which we have done here is that we have simply added this particular

53
00:03:48,010 --> 00:03:54,430
you are a pattern which basically gives us access to the static files which are uploaded by the user.

54
00:03:54,610 --> 00:04:00,490
So whenever you are using a user uploaded static files, you always need to make sure that you include

55
00:04:00,580 --> 00:04:08,320
this particular code and you all start by file so that Django knows that you are using some static images

56
00:04:08,320 --> 00:04:10,810
and your website which are uploaded by the user.

57
00:04:11,780 --> 00:04:18,529
So that's it for this lecture and in the next lecture will go ahead and we will learn about having the

58
00:04:18,560 --> 00:04:24,800
default profile image, which means that what happens if we create some user who does not have a profile

59
00:04:24,800 --> 00:04:29,360
picture and in that case, we want to display a particular default profile picture.

60
00:04:29,990 --> 00:04:31,940
So thank you very much for watching.

61
00:04:31,940 --> 00:04:33,950
And I'll see you guys next time.

62
00:04:34,220 --> 00:04:34,820
Thank you.

