1
00:00:00,210 --> 00:00:06,810
Hello and welcome to this lecture, and in this lecture, we will learn about authentication and authentication

2
00:00:06,810 --> 00:00:10,530
simply means how to register, log in and log out users.

3
00:00:10,980 --> 00:00:17,070
Now, in case you need to design a website where any external users can add some data to your website,

4
00:00:17,310 --> 00:00:23,010
then in that case, you need to make sure that you authenticate the user's first even before you have

5
00:00:23,010 --> 00:00:29,010
or even before you give them access to your data on your website and to authenticate users.

6
00:00:29,190 --> 00:00:31,920
First of all, you need to register them on your Web site.

7
00:00:32,130 --> 00:00:38,100
And what exactly do I mean by registration as that you simply go ahead, accept the username passwords,

8
00:00:38,100 --> 00:00:41,460
email, and simply store the data onto your website.

9
00:00:41,650 --> 00:00:48,180
So that means when the next time they visit your website, you can simply go ahead and make them login

10
00:00:48,180 --> 00:00:53,250
with the same credentials which they have previously used to register themselves on your site.

11
00:00:54,000 --> 00:01:01,140
Now, in order to just go ahead and implement all this authentication functionality in Django, what

12
00:01:01,140 --> 00:01:06,780
we need to do first is that we need to create a completely new application now in the main project,

13
00:01:06,780 --> 00:01:12,630
which is the myside, we currently have a food app over here and now we also need to create a new app

14
00:01:12,630 --> 00:01:14,520
which is called as, let's say, users.

15
00:01:15,410 --> 00:01:19,500
So in order to create that app, let's simply open up the command prompt.

16
00:01:19,500 --> 00:01:22,470
And first of all, I'm going to stop myself over here.

17
00:01:22,800 --> 00:01:29,160
And in order to create a new app, you always go ahead and use a command, which is Django admin up

18
00:01:29,160 --> 00:01:29,690
users.

19
00:01:30,060 --> 00:01:35,040
So let's go ahead and type in Django Dash admin.

20
00:01:37,160 --> 00:01:42,710
Startup and the name of that app is users.

21
00:01:43,680 --> 00:01:47,650
So now let's go ahead and enter, and the app should be created for us.

22
00:01:48,210 --> 00:01:53,100
So now if you have a look over here, as you can see now we have the user's app over here.

23
00:01:53,640 --> 00:01:59,640
And the first thing which you always do when you have created the app is that you simply go ahead and

24
00:01:59,640 --> 00:02:02,130
go into the App Store by file.

25
00:02:02,520 --> 00:02:09,300
You simply copy the user's config from here or which is the app name config from here, and then you

26
00:02:09,300 --> 00:02:12,300
go to the settings that profile of your main site.

27
00:02:12,330 --> 00:02:13,350
So let's go here.

28
00:02:13,590 --> 00:02:19,350
And first of all, we need to go ahead and add that particular created app to this particular install

29
00:02:19,350 --> 00:02:20,070
apps list.

30
00:02:20,080 --> 00:02:22,870
So let's go ahead and add that up over here as well.

31
00:02:23,220 --> 00:02:27,990
So that's going to be app name, which is users dot.

32
00:02:27,990 --> 00:02:29,100
App Dot.

33
00:02:30,350 --> 00:02:36,860
Users can fake, so now once we have added that to the install apps, now we are free to go ahead and

34
00:02:36,860 --> 00:02:39,800
actually implement the authentication functionality.

35
00:02:40,550 --> 00:02:47,150
Now, first of all, as now, we want to register the users, we now need to go ahead and create a view

36
00:02:47,300 --> 00:02:50,120
which is going to help the users to get registered.

37
00:02:50,390 --> 00:02:56,990
So now, in order to create a view, we just go ahead and go into the user profile of our new app,

38
00:02:56,990 --> 00:02:58,040
which is users.

39
00:02:58,280 --> 00:03:03,350
So this is the user's app and this is the view stored by file for the user's app.

40
00:03:03,950 --> 00:03:09,640
Now in here, how exactly can we create a view which is going to accept some data from the user?

41
00:03:09,950 --> 00:03:12,800
So, as usual, we need to go ahead and create a form.

42
00:03:13,280 --> 00:03:21,440
Now, we have created a form to actually accept data that is the form data like we have accepted the

43
00:03:21,440 --> 00:03:25,580
item details of the food item, details from the user by creating a form.

44
00:03:25,910 --> 00:03:31,780
But in case of authentication over here, what we are going to do is that we are going to use the inbuilt

45
00:03:31,790 --> 00:03:37,270
forms which come with Django, which automatically create the registration form for us.

46
00:03:37,670 --> 00:03:39,560
And that's the best part about Django.

47
00:03:39,560 --> 00:03:43,160
That is, you don't have to build the authentication logic from scratch.

48
00:03:43,310 --> 00:03:45,250
You don't have to build a form either.

49
00:03:45,560 --> 00:03:48,770
Everything is actually set up and provided by Django.

50
00:03:48,780 --> 00:03:51,770
You just need to go ahead and use that into your application.

51
00:03:52,400 --> 00:03:55,070
So now how exactly can we go ahead and do that?

52
00:03:56,000 --> 00:04:02,390
So first of all, in order to use a form which is the user creation form in Django, first of all,

53
00:04:02,390 --> 00:04:07,400
you need to import that user creation form and to import that you type in from.

54
00:04:07,880 --> 00:04:10,490
That's going to be Django Dot Cantrip.

55
00:04:11,850 --> 00:04:12,420
Dr..

56
00:04:13,730 --> 00:04:22,450
Or dart forms and from the forms, we specifically import the user creation form, so we type in import,

57
00:04:22,790 --> 00:04:27,440
that's going to be user creation form, not the change form.

58
00:04:27,440 --> 00:04:29,050
That should be creation form.

59
00:04:29,540 --> 00:04:36,290
And now once we have this form, we can now simply go ahead, create a particular view and use that

60
00:04:36,290 --> 00:04:37,250
form over here.

61
00:04:37,580 --> 00:04:40,040
So now let's go ahead and create a view over here.

62
00:04:40,040 --> 00:04:43,520
And let's say we call that view as, oh, let's say register.

63
00:04:43,520 --> 00:04:44,960
And now to this register view.

64
00:04:45,080 --> 00:04:47,420
We are now simply going to pass in a request.

65
00:04:48,770 --> 00:04:55,400
So as we always do with a particular view, we pass in a request and now to create a form out of this

66
00:04:55,400 --> 00:04:57,650
user creation form, you simply type in.

67
00:04:58,860 --> 00:04:59,370
Form.

68
00:05:00,590 --> 00:05:04,410
Equals and that's going to be user creation form.

69
00:05:05,750 --> 00:05:08,730
And now once we have this form, we now go ahead.

70
00:05:08,900 --> 00:05:14,410
We simply need to render a template so we type and return render.

71
00:05:14,450 --> 00:05:16,670
And that's going to be a request.

72
00:05:17,390 --> 00:05:20,150
And then let's pass a template name over here.

73
00:05:20,180 --> 00:05:25,610
So right now, we have not yet created the template, but we are still going to pass it over here.

74
00:05:25,670 --> 00:05:31,850
So let's say the name of the template to register a user is going to be register dot HTML.

75
00:05:32,890 --> 00:05:38,800
And now what we do is that to this particular template, we simply passing the user creation form,

76
00:05:38,800 --> 00:05:41,650
which we have created right over here as a context.

77
00:05:42,100 --> 00:05:46,510
So here I'll just type in form, koolan form.

78
00:05:48,320 --> 00:05:54,020
That means we have now created a view and we have created a form inside that view and we have passed

79
00:05:54,020 --> 00:05:58,550
that form to the template, which is register.com.

80
00:05:59,330 --> 00:06:02,810
Now, let's go ahead and create the template which is registered on each jemal.

81
00:06:03,260 --> 00:06:08,870
And now, as we have created the new app, which is users right here, in order to create a template

82
00:06:08,870 --> 00:06:13,450
into any app, first of all, we need to go ahead and create a template directory.

83
00:06:13,460 --> 00:06:15,230
So let's go ahead and type in.

84
00:06:16,180 --> 00:06:21,490
Templates over here and in this template directly, let's go ahead and add another directory, which

85
00:06:21,490 --> 00:06:29,020
is nothing but users and in this particular user's directory, let's now go ahead and add in the actual

86
00:06:29,020 --> 00:06:31,270
template, which is registered on each HTML.

87
00:06:31,660 --> 00:06:32,830
So that's going to be.

88
00:06:34,030 --> 00:06:41,380
Register dot each, and in here we are actually going to create the form, so for now I'll just go ahead

89
00:06:41,380 --> 00:06:47,350
and type in form and the method for this form is going to be post as usual.

90
00:06:48,070 --> 00:06:54,660
And now he'll be forced at us a six out of Tolkan, as we always do with Forms and Django.

91
00:06:54,670 --> 00:06:58,240
So see as a sort of underscore token.

92
00:06:58,810 --> 00:07:04,660
And now what we do is that we simply go ahead and use this context, which is form right here, and

93
00:07:04,660 --> 00:07:07,860
the form is going to be automatically added up right over here.

94
00:07:07,870 --> 00:07:10,550
So I'll type in form and we are good to go.

95
00:07:10,870 --> 00:07:17,620
Now we simply need to add a submit button over here, so I'll type in button.

96
00:07:18,800 --> 00:07:26,060
And let's see, the tape is going to be submitted and let's say this button says something like.

97
00:07:27,060 --> 00:07:31,350
Sign up and now once we have added all that, we are almost good to go.

98
00:07:31,560 --> 00:07:36,990
So now we have a working view here and we also have a template which is connected to this particular

99
00:07:36,990 --> 00:07:40,620
view, which is actually going to render a registration form.

100
00:07:41,100 --> 00:07:46,500
Now, the only thing which remains is that we need to create a new pattern so that we can actually go

101
00:07:46,500 --> 00:07:49,850
ahead and access this particular registration form.

102
00:07:50,720 --> 00:07:58,010
So now, instead of adding you all Stoppie file in the user's app, what I'm directly going to do is

103
00:07:58,010 --> 00:08:05,150
that I'm directly going to jump on to the myside directly and indirectly going to use to mean you are

104
00:08:05,330 --> 00:08:12,490
Torpy file, which is this file right here now in here, and directly add a path to register a user.

105
00:08:12,650 --> 00:08:18,590
So I'll type in path and in here I'll type in register as the name of the path.

106
00:08:18,950 --> 00:08:20,750
So I'll register slash.

107
00:08:21,090 --> 00:08:26,000
And here now we need to access the view, which is the view which we have just created.

108
00:08:26,000 --> 00:08:30,240
And that is nothing but this view right here, which is the register you.

109
00:08:30,740 --> 00:08:34,850
So first of all, we need to actually go ahead and import that register of you.

110
00:08:35,120 --> 00:08:40,760
So in order to impose that view, I'll simply go ahead and type in from users.

111
00:08:41,330 --> 00:08:50,870
I'll type in import views, and I'm actually going to import all the views as, let's say, user underscore.

112
00:08:51,890 --> 00:08:58,940
Views, so now in order to access the register of you now, you need to type in views which we have

113
00:08:58,940 --> 00:09:00,140
just imported right here.

114
00:09:00,230 --> 00:09:01,670
And this should actually be.

115
00:09:03,080 --> 00:09:04,430
From not far from.

116
00:09:05,950 --> 00:09:09,790
So now, once we have imported the views, we now go ahead and type in.

117
00:09:11,120 --> 00:09:18,970
User and the views, because we have important views as user views and now in order to access the register

118
00:09:18,980 --> 00:09:20,820
view from there, we depend on.

119
00:09:21,960 --> 00:09:29,550
Register and now we also go ahead and name the school as, let's say, register, so I'll type and name

120
00:09:29,550 --> 00:09:32,920
equals, and that's going to be registered.

121
00:09:33,780 --> 00:09:37,360
So now let's go ahead, save the code and we are almost good to go.

122
00:09:37,500 --> 00:09:46,110
So now the way in which this thing works is that if you go to the localhost 48000 register, this is

123
00:09:46,110 --> 00:09:50,750
actually going to look for a view which is called Register of You in the user's app.

124
00:09:51,180 --> 00:09:55,800
So the register of you in the user's app is nothing but this view right here.

125
00:09:55,890 --> 00:09:57,800
So let me just open up that for you.

126
00:09:58,800 --> 00:10:02,980
So you have a bunch of files in here so it becomes difficult to navigate through them.

127
00:10:03,360 --> 00:10:08,760
OK, so now it goes to the register of you, then it's actually going to go ahead and create a user

128
00:10:08,760 --> 00:10:12,090
creation, form it into this particular instance.

129
00:10:12,360 --> 00:10:18,240
And then we are actually passing in this form over here as a context to the template which is registered

130
00:10:18,240 --> 00:10:19,100
on HTML.

131
00:10:19,440 --> 00:10:21,850
And this is the template which we are talking about.

132
00:10:21,870 --> 00:10:27,510
So this is actually going to get rendered and this simply has a form and a submit button which is going

133
00:10:27,510 --> 00:10:33,000
to submit the form which is going to eventually load up the user registration form, which is enabled

134
00:10:33,000 --> 00:10:33,750
by triangle.

135
00:10:34,170 --> 00:10:40,320
So now once we know how this thing works, let's run the application by typing in Python.

136
00:10:41,950 --> 00:10:45,310
Managed by run so.

137
00:10:46,400 --> 00:10:55,090
So now once the subway is up and running, let's actually go ahead and go to localhost or thousand cash

138
00:10:55,730 --> 00:10:56,520
register.

139
00:10:56,930 --> 00:11:02,650
And now when I go ahead and hit enter, it simply does not exist and register.

140
00:11:02,810 --> 00:11:07,430
And that means we do have some sort of an issue here of a template.

141
00:11:07,580 --> 00:11:13,570
OK, so the issue of the arrow is that this should actually be users, not user.

142
00:11:13,910 --> 00:11:16,010
So that was a mistake from my site.

143
00:11:16,730 --> 00:11:19,100
And now hopefully everything should work fine.

144
00:11:19,190 --> 00:11:24,390
So now if you go to register, as you can see now we have a login form or sign of form over here.

145
00:11:24,830 --> 00:11:28,430
Now, as you can see, we have a form right here.

146
00:11:28,430 --> 00:11:30,820
And let's go ahead, try to add some detail here.

147
00:11:31,190 --> 00:11:33,260
So I'll just go ahead and type in my name.

148
00:11:33,260 --> 00:11:35,930
And let's say I type in some dummy password over here.

149
00:11:36,590 --> 00:11:42,310
And let's say the password is something like, let's see, admin user.

150
00:11:43,040 --> 00:11:44,510
Let's see the password.

151
00:11:44,520 --> 00:11:47,060
Confirmation is also admin user.

152
00:11:48,260 --> 00:11:54,260
So when I go ahead and click on Sign Up, as you can see, nothing actually happened in here because

153
00:11:54,260 --> 00:11:57,710
we have just written a code to create a form and nothing else.

154
00:11:57,710 --> 00:12:01,830
That means we have not yet written the code to actually handle the data from the form.

155
00:12:02,570 --> 00:12:08,960
So what we want to do here now is that whenever you click assign a button right over here, you should

156
00:12:08,960 --> 00:12:13,160
actually get some sort of a message like, let's say the user has been created.

157
00:12:13,580 --> 00:12:18,800
And if the form data which you have submitted, like the username or password, let's save the password,

158
00:12:18,800 --> 00:12:24,470
do not match, then the form needs to give you out some warning that, OK, the user is now not created.

159
00:12:24,670 --> 00:12:29,900
Instead you have entered and matching passwords or user name, which is already created.

160
00:12:30,590 --> 00:12:37,370
Now in the next lecture will go ahead and learn how to exactly validate form and how to display a message

161
00:12:37,550 --> 00:12:38,630
when they sign up.

162
00:12:38,630 --> 00:12:39,440
A successful.

163
00:12:39,800 --> 00:12:43,620
So thank you very much for watching and I'll see you guys next time.

164
00:12:44,000 --> 00:12:44,570
Thank you.

