1
00:00:00,390 --> 00:00:06,060
Hello and welcome to this lecture, and in this lecture, we are going to implement two functionalities.

2
00:00:06,390 --> 00:00:13,170
One is how to redirect the registered users to the login page, and the next one is the logout functionality.

3
00:00:13,740 --> 00:00:18,620
So now let's go ahead and learn how to redirect the registered users to the login page.

4
00:00:19,020 --> 00:00:21,090
So doing that is actually quite simple.

5
00:00:21,090 --> 00:00:24,180
You just have to go into the register of right here.

6
00:00:24,300 --> 00:00:30,540
And right now, as you can see, we are actually redirecting the registered users to the index page

7
00:00:30,540 --> 00:00:32,409
of our food app right over here.

8
00:00:32,520 --> 00:00:38,070
You simply have to redirect them to the you are allowed for login and the URL for login is nothing but

9
00:00:38,400 --> 00:00:40,980
this you are right here, which we have just created.

10
00:00:41,190 --> 00:00:45,730
And the name of this you are has nothing but login, which is stated right over here.

11
00:00:46,290 --> 00:00:52,560
So in order to make that change, you simply have to go ahead, open up the you start by file for the

12
00:00:52,560 --> 00:00:56,910
users and simply go ahead, replace this with login.

13
00:00:57,810 --> 00:01:01,240
And now you can see this and you are pretty much good to go.

14
00:01:01,560 --> 00:01:04,010
And now let's see if this thing works out fine.

15
00:01:04,050 --> 00:01:06,600
So let's just go ahead and register a user.

16
00:01:07,140 --> 00:01:13,800
So I'll type in a new user for let's say the e-mail is a new.

17
00:01:14,760 --> 00:01:25,590
For at Gmail dot com, let's say the password is best test for five six, the password confirmation

18
00:01:25,590 --> 00:01:28,080
as test test four, five, six.

19
00:01:28,080 --> 00:01:32,980
And when I click on Sign Up, as you can see now, we are redirected to the login page.

20
00:01:33,300 --> 00:01:37,000
So this is the natural flow which takes place in any website.

21
00:01:37,020 --> 00:01:42,540
So if you have visited any website, the first ask you to register and once you register, obviously

22
00:01:42,540 --> 00:01:45,270
they should prompt you to login into their site.

23
00:01:45,600 --> 00:01:47,760
So now let's go ahead and try to login.

24
00:01:47,760 --> 00:01:53,340
So the user name is a new user full and the password was.

25
00:01:54,380 --> 00:01:57,300
Test, test, four, five, six, seven.

26
00:01:57,370 --> 00:02:03,650
Click on login, as you can see now, we are logged in over here and now we also need to go ahead and

27
00:02:03,650 --> 00:02:04,820
change this message.

28
00:02:04,820 --> 00:02:07,580
So we need to say something like welcome new user.

29
00:02:08,240 --> 00:02:13,520
Instead of seeing your account is created, you can see something like, oh, you are currently logged

30
00:02:13,520 --> 00:02:16,590
in so you can make changes for this particular thing right here.

31
00:02:16,610 --> 00:02:17,870
There is no issue in that.

32
00:02:17,870 --> 00:02:22,670
But for now, let's go ahead and concentrate on making the logout functionality.

33
00:02:23,420 --> 00:02:28,700
So now let's go ahead and go to the template, which is the logo or e-mail.

34
00:02:28,750 --> 00:02:31,040
So let me just go ahead and go there.

35
00:02:31,370 --> 00:02:34,820
And actually, we have not yet created the logo template.

36
00:02:34,820 --> 00:02:41,240
So let me just go ahead, go to the user's app, go to the templates, and here I'll create a new template

37
00:02:41,240 --> 00:02:44,270
and I'll call it as logout dot HDMI.

38
00:02:44,540 --> 00:02:49,900
And in this particular logo template, you don't have to create any sort of a formal way here.

39
00:02:49,910 --> 00:02:56,120
You simply need to display a message that you have been locked out so I can type in it heading over

40
00:02:56,120 --> 00:03:02,390
here and I can see something like you have been locked out.

41
00:03:03,880 --> 00:03:07,240
So now, once you go ahead and save that, we are pretty much good to go.

42
00:03:07,840 --> 00:03:13,570
So now let's actually go ahead and now, as you all know, that we are locked in.

43
00:03:14,260 --> 00:03:21,940
But now if I go ahead and type in out, as you can see, it says you have been locked out.

44
00:03:22,450 --> 00:03:26,590
But how exactly can we know if a particular user is logged in or log out?

45
00:03:26,800 --> 00:03:31,030
And how exactly can we be sure that the login functionality is working fine?

46
00:03:31,060 --> 00:03:36,880
So for that, let's actually go ahead and try to access the admin panel and first of all, log in as

47
00:03:36,880 --> 00:03:37,500
an admin.

48
00:03:37,870 --> 00:03:44,580
So I'll go ahead and I'll go to the login page and I'll try to log in as admin.

49
00:03:44,590 --> 00:03:46,300
So that's going to be my name.

50
00:03:46,390 --> 00:03:48,190
The super user credentials.

51
00:03:50,650 --> 00:03:55,070
So when I go ahead and log in, so right now we will be logged in as admin.

52
00:03:55,450 --> 00:04:00,490
And now let's go ahead and type in log out so and I type in log out.

53
00:04:00,790 --> 00:04:02,440
It says we have been locked out.

54
00:04:02,440 --> 00:04:08,410
And now if we try to access this admin dashboard, as you can see, it's going to give you the login

55
00:04:08,410 --> 00:04:13,990
page, which means that we have successfully logged out and the logged functionality has been successfully

56
00:04:13,990 --> 00:04:14,730
implemented.

57
00:04:14,860 --> 00:04:16,490
So that's it for this lecture.

58
00:04:16,510 --> 00:04:22,360
Hopefully you guys be able to understand how to redirect the registered users to the login page and

59
00:04:22,360 --> 00:04:25,440
also how to implement the login functionality.

60
00:04:25,900 --> 00:04:31,150
So in the next lecture, what we will do is that we will go ahead and we will learn how we can actually

61
00:04:31,150 --> 00:04:36,770
go ahead and have something over here which is going to state that the user is currently logged in.

62
00:04:36,790 --> 00:04:41,340
So, for example, when the user is logged, then it should show a logged option over here.

63
00:04:41,710 --> 00:04:47,260
And if the user is not currently logged, then it should show a log in option right over here.

64
00:04:47,500 --> 00:04:50,350
So we are going to implement that in the upcoming lecture.

65
00:04:50,530 --> 00:04:52,450
So thank you very much for watching.

66
00:04:52,450 --> 00:04:54,430
And I'll see you guys next time.

67
00:04:54,880 --> 00:04:55,510
Thank you.

