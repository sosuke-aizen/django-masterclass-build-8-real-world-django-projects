1
00:00:00,150 --> 00:00:06,390
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how you can add the

2
00:00:06,390 --> 00:00:09,280
email field, do the user registration form.

3
00:00:09,840 --> 00:00:16,050
So right now, the user registration form does not have any sort of an email field and we need to add

4
00:00:16,050 --> 00:00:18,420
an additional feel to the form itself.

5
00:00:18,630 --> 00:00:23,760
Now, if you want to look how exactly we have created the form, then you can go to the register, dot

6
00:00:23,760 --> 00:00:31,170
each HTML page, and here you will see that while creating this particular form right here, we directly

7
00:00:31,170 --> 00:00:36,990
have imported or we directly have used this particular form right here, which is an inbuilt form,

8
00:00:37,710 --> 00:00:44,130
and we have simply added a submit button right at the end in order to make the user submit the form.

9
00:00:44,550 --> 00:00:51,690
So now we cannot directly go ahead and add an input type over here as an email, because that's obviously

10
00:00:51,690 --> 00:00:52,710
not going to work.

11
00:00:53,010 --> 00:00:59,330
So now let's learn how exactly can we add an additional field to and build a Django form?

12
00:01:00,090 --> 00:01:06,360
So whenever you actually want to add an additional feel to a particular form, what you need to do is

13
00:01:06,360 --> 00:01:10,620
that you actually need to go ahead and extend that particular form.

14
00:01:10,980 --> 00:01:14,430
So what exactly do I mean by extending a particular form?

15
00:01:15,620 --> 00:01:21,650
So if you talk about this form right here, which is the form which we have created, we have actually

16
00:01:21,650 --> 00:01:27,200
created this form inside the Vieuxtemps profile for the users.

17
00:01:27,590 --> 00:01:32,000
So if you actually go to the users, you start profile.

18
00:01:32,000 --> 00:01:38,900
As you can see, we have created this form from the user creation form, which means that we already

19
00:01:38,900 --> 00:01:42,350
have all the fields of that form in the user creation form.

20
00:01:43,100 --> 00:01:48,680
Now, there is one thing which you need to remember that is you cannot directly go ahead and add any

21
00:01:48,680 --> 00:01:50,920
different feel to the user creation form.

22
00:01:51,230 --> 00:01:58,700
But what we can do here is that we can create a completely new form, inherit the user creation form

23
00:01:58,700 --> 00:02:03,650
in that particular form, and also add the email feel to it.

24
00:02:04,280 --> 00:02:09,560
So what we essentially do here is that we create a completely new form which is going to be created

25
00:02:09,560 --> 00:02:10,250
by us.

26
00:02:10,720 --> 00:02:14,630
And in that form, we will inherit the user creation form.

27
00:02:15,110 --> 00:02:21,860
And then we will also add the email field to the new form, which already has the user creation form.

28
00:02:23,090 --> 00:02:26,740
So now let's see how exactly can we go ahead and do that.

29
00:02:26,750 --> 00:02:33,470
So in order to do that, we actually go ahead and create our very own forms, not by file in the user's

30
00:02:33,470 --> 00:02:33,830
app.

31
00:02:34,190 --> 00:02:41,290
So if you go to the user's app, you just need to go ahead and create a new file and carlita's form

32
00:02:41,310 --> 00:02:42,150
stockpile.

33
00:02:42,440 --> 00:02:47,210
And this forms the profile is going to contain all the user build forms.

34
00:02:48,160 --> 00:02:53,750
So whenever if you want to create your very own form, you can go ahead and do that in this particular

35
00:02:53,750 --> 00:02:54,470
file right here.

36
00:02:55,190 --> 00:02:58,660
So first of all, you need to go ahead and import forms here.

37
00:02:58,670 --> 00:03:01,400
So I'll type in from Django.

38
00:03:02,390 --> 00:03:04,640
That's going to be import forms.

39
00:03:06,230 --> 00:03:12,950
And now we also need to go ahead and import the user model as well, because we are going to create

40
00:03:12,950 --> 00:03:14,720
a form related to users.

41
00:03:15,020 --> 00:03:18,860
So I'll type in from Django Dot Cantrip.

42
00:03:20,250 --> 00:03:21,210
Dawidoff.

43
00:03:23,150 --> 00:03:26,420
The models I'm going to import the.

44
00:03:27,800 --> 00:03:34,660
Use a model, and the reason why we are importing use a model from here is because user is not actually

45
00:03:34,900 --> 00:03:39,350
the model which we have created, but instead it's actually inbuilt into Zango.

46
00:03:39,610 --> 00:03:42,760
So you need to import that from here and now.

47
00:03:42,850 --> 00:03:48,940
You also need to go ahead and import the user creation form as well, because you need to inherit from

48
00:03:48,940 --> 00:03:50,100
that particular form.

49
00:03:50,110 --> 00:03:53,530
So you type in from Django Dot Cantrip.

50
00:03:54,890 --> 00:03:57,470
Dot, dot, that should be off.

51
00:03:58,670 --> 00:03:59,810
That forms.

52
00:04:01,140 --> 00:04:05,300
Import, that's going to be user creation fault.

53
00:04:06,030 --> 00:04:11,020
OK, so now once we have all these imports, let's go ahead and create our very own form.

54
00:04:11,490 --> 00:04:16,560
So in order to create a very own form, we create that in the form of a glass.

55
00:04:16,899 --> 00:04:23,310
So you type in class and in here you name this particular form as, let's say, register form.

56
00:04:25,110 --> 00:04:31,080
So now this is the new form which we will be using instead of user creation form, and this particular

57
00:04:31,080 --> 00:04:35,370
register form is going to inherit from the user creation form.

58
00:04:35,730 --> 00:04:42,030
So now whenever you want to make a particular class inherit something from some other class, then what

59
00:04:42,030 --> 00:04:47,770
you do is that you go ahead and pass that class as a parameter to this particular class right here.

60
00:04:48,240 --> 00:04:54,600
So as now we want to inherit from the user creation form, I'll simply type in user creation form here.

61
00:04:55,960 --> 00:04:59,330
And now once we go ahead and do that, we are pretty much good to go.

62
00:04:59,830 --> 00:05:05,830
So now what we want to do is that to this particular legislative form, we want to add an additional

63
00:05:05,830 --> 00:05:07,810
field, which is the e-mail field.

64
00:05:08,170 --> 00:05:14,110
So in order to add an additional field, you type in the field name over here and that's going to be

65
00:05:14,110 --> 00:05:16,410
equal to form start.

66
00:05:16,870 --> 00:05:21,850
That's going to be e-mail field, meaning that we are adding a particular field.

67
00:05:21,880 --> 00:05:27,940
We are naming that field as e-mail and the type of that field is nothing but e-mail field.

68
00:05:28,900 --> 00:05:35,980
So now once we actually have this particular class for the register form, now we go ahead and define

69
00:05:35,980 --> 00:05:38,180
a middle class for this particular class.

70
00:05:38,740 --> 00:05:41,620
So what exactly do I mean by a middle class?

71
00:05:41,980 --> 00:05:48,020
So a middle class is nothing but the class which provides information about this class itself.

72
00:05:48,190 --> 00:05:55,960
So, for example, now we want to state that we want to add this particular field to the register form

73
00:05:56,200 --> 00:06:03,610
so we can do that by having a middle class over here so I can type in class matter, which means that

74
00:06:03,640 --> 00:06:09,100
this particular class is going to hold information about this particular class, which is the registered

75
00:06:09,100 --> 00:06:09,440
form.

76
00:06:10,120 --> 00:06:14,910
So now in here we specify two things in this particular matter class.

77
00:06:15,130 --> 00:06:20,020
The first thing is that we specify the user model, which this form is going to be using.

78
00:06:20,650 --> 00:06:25,240
So Jianguo actually knows that you have created this particular form right here.

79
00:06:25,270 --> 00:06:28,930
It does not know to which particular model this form belongs to.

80
00:06:29,470 --> 00:06:31,900
So this form actually belongs to the user model.

81
00:06:31,900 --> 00:06:35,090
So will type and model equals.

82
00:06:35,110 --> 00:06:36,330
That's going to be user.

83
00:06:37,180 --> 00:06:43,810
And now along with this, there is also one more field which you need to specify, and that is fields.

84
00:06:44,240 --> 00:06:49,670
So you also need to specify which all fields are going to be present in this particular form.

85
00:06:49,690 --> 00:06:53,900
So for that, you simply type in fields and you create fields over here.

86
00:06:54,310 --> 00:06:57,970
So the fields which you want is that you want the user name.

87
00:06:58,660 --> 00:07:00,850
You want the email.

88
00:07:02,510 --> 00:07:09,860
Which is this field which we have created just right now, and then you need a password over here,

89
00:07:09,950 --> 00:07:17,030
so you type in password and this is going to be password one and you actually need another field, which

90
00:07:17,030 --> 00:07:17,390
is.

91
00:07:18,600 --> 00:07:19,490
Password two.

92
00:07:19,950 --> 00:07:23,740
So these are nothing, but these are the fields to confirm the password.

93
00:07:23,760 --> 00:07:29,100
So if you actually go to the register form, you will notice that we actually have two inputs for the

94
00:07:29,100 --> 00:07:29,750
password.

95
00:07:29,970 --> 00:07:34,860
The first one is to actually accept the password, and the second one is to confirm the password.

96
00:07:35,460 --> 00:07:38,500
So that's the significance of those fields over here.

97
00:07:39,050 --> 00:07:45,470
And now, once this form is created, we can now go ahead and use this particular form and register

98
00:07:45,480 --> 00:07:45,760
view.

99
00:07:46,080 --> 00:07:47,910
So let's go back over here.

100
00:07:48,360 --> 00:07:53,970
And right now, as you can see, we are using the user creation form here, but now we can go ahead

101
00:07:53,970 --> 00:07:56,650
and replace that and use the register form.

102
00:07:57,120 --> 00:08:02,530
So even before you go ahead and use the register form, you need to go ahead and import that form up

103
00:08:02,580 --> 00:08:10,590
over here so you can type in from dot forms, import the register form.

104
00:08:11,490 --> 00:08:18,030
And now what you can simply do is that you can replace the user creation form with the register form.

105
00:08:18,300 --> 00:08:20,220
So I'll replace that over here.

106
00:08:20,490 --> 00:08:25,650
This is going to be register form and this thing is going to be register form as well.

107
00:08:26,670 --> 00:08:33,059
OK, so now once we do that, we can also go ahead and delete the user creation form from here because

108
00:08:33,059 --> 00:08:35,140
we no longer need it right over here.

109
00:08:36,600 --> 00:08:43,740
So once I go ahead, delete that, we should be good to go and now we should have the email fill in

110
00:08:43,740 --> 00:08:44,610
a register form.

111
00:08:45,030 --> 00:08:47,190
So let's go ahead and see if we get that.

112
00:08:47,430 --> 00:08:50,060
So let me just go to register.

113
00:08:50,700 --> 00:08:56,850
And as you can see now, we not only have the user name feel, but we also have the email file right

114
00:08:56,850 --> 00:08:57,330
over here.

115
00:08:58,020 --> 00:09:00,950
So now let's go ahead and try to register a new user.

116
00:09:00,960 --> 00:09:02,010
So I'll type in.

117
00:09:03,500 --> 00:09:14,420
New user, let's say, new user one, let's say the email is new user one at G.M. dot com, let's say

118
00:09:14,420 --> 00:09:18,920
the password is test test four, five, six.

119
00:09:19,980 --> 00:09:28,080
Test test for physics, and now when I click on Sign Up, as you can see, the account is created.

120
00:09:28,350 --> 00:09:33,920
And now let's go ahead and make sure if the email feel for this particular user is added.

121
00:09:34,260 --> 00:09:42,230
And in order to check that, we simply go to the admin, I simply log in with my admin credentials here.

122
00:09:42,240 --> 00:09:49,200
So when I go ahead and log in and when we go to users, as you can see, we have a new user, one over

123
00:09:49,200 --> 00:09:52,740
here, and it also has the email address file as well.

124
00:09:52,770 --> 00:09:59,010
That means we have successfully added the email address feel for our users form, which we have created

125
00:09:59,010 --> 00:10:01,500
by inheriting from the user creation form.

126
00:10:01,690 --> 00:10:08,220
So that means we were successful in adding the email feel to the user creation form by inheriting the

127
00:10:08,220 --> 00:10:11,730
user creation form and creating our very own form right here.

128
00:10:12,120 --> 00:10:18,600
So whenever you have to create a new form, you can simply go ahead, create a form here and you can

129
00:10:18,600 --> 00:10:21,090
inherit from the inbuilt form from Shango.

130
00:10:21,120 --> 00:10:27,400
So in this case, this register form is the form which we have actually created and we have inherited

131
00:10:27,420 --> 00:10:30,180
from the inbuilt form, which is the user creation form.

132
00:10:30,570 --> 00:10:36,780
And then to actually add a field, we have simply made a field goal here and we have simply used the

133
00:10:36,780 --> 00:10:43,230
middle class over here and added up that we lower here along with the model which the form belongs to.

134
00:10:43,410 --> 00:10:49,710
And now once we know how to actually go ahead and register users in the upcoming lecture, we will learn

135
00:10:49,710 --> 00:10:52,150
how to log in and log out the users.

136
00:10:52,650 --> 00:10:56,400
So thank you very much for watching and I'll see you guys next time.

137
00:10:56,700 --> 00:10:57,360
Thank you.

