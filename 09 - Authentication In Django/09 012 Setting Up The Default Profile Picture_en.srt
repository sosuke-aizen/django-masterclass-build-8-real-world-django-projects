1
00:00:00,420 --> 00:00:06,730
Now, let's try to create a profile for a user and let's go ahead and leave the image feel as empty.

2
00:00:07,200 --> 00:00:09,840
So right now I am into the register page right here.

3
00:00:09,840 --> 00:00:14,160
And let's go ahead and create a user and I'll call it as new.

4
00:00:15,320 --> 00:00:22,880
Site, you use it and let's see, the e-mail is demo at Gmail dot com.

5
00:00:22,910 --> 00:00:26,870
Let's say we passed this test test for five six.

6
00:00:27,410 --> 00:00:29,150
Let's confirm the password as well.

7
00:00:29,190 --> 00:00:31,760
So test, test four, five, six.

8
00:00:32,940 --> 00:00:40,560
So now when I go ahead and click on Sign Up, it's asking me to log in so I'll type in new.

9
00:00:41,500 --> 00:00:42,280
Site.

10
00:00:43,590 --> 00:00:45,780
User and the password is.

11
00:00:47,110 --> 00:00:55,360
Test test for five, so when I click on login, as you can see, it basically locks us in and now if

12
00:00:55,360 --> 00:00:59,500
I click on Profile, it just is giving me new side user over here.

13
00:00:59,590 --> 00:01:04,840
So now once we have this user, let's actually go to the admin field and let's see what exactly happens

14
00:01:04,840 --> 00:01:05,200
in here.

15
00:01:05,349 --> 00:01:07,430
So a login with my admin credentials.

16
00:01:07,450 --> 00:01:12,910
So now if I go to profiles, as you can see, we only have the profile for the super user.

17
00:01:12,940 --> 00:01:17,500
Now, let's also go ahead and create a profile for the user, which we have created right now.

18
00:01:17,890 --> 00:01:20,280
So I'll select the user as new site user.

19
00:01:20,560 --> 00:01:26,470
And right now I'll just leave this image as empty and I'll simply add a location as, let's say, a

20
00:01:26,470 --> 00:01:27,120
country.

21
00:01:27,130 --> 00:01:29,140
And now let's go ahead and click save.

22
00:01:29,620 --> 00:01:31,870
And now the user profile should be created.

23
00:01:32,230 --> 00:01:35,050
Now, let me just go to slash.

24
00:01:37,530 --> 00:01:43,540
Logon and let me log in as the new site user soil type, a new site user.

25
00:01:44,220 --> 00:01:47,460
This should be a test test for five, six.

26
00:01:47,760 --> 00:01:55,320
And now when I log in and now when I go to profile, as you can see, as we have not uploaded any image

27
00:01:55,330 --> 00:02:01,320
over here, it's going to show us this particular symbol, which means that the Zango is not able to

28
00:02:01,320 --> 00:02:03,000
find an image for this user.

29
00:02:03,630 --> 00:02:09,300
So whenever someone creates a profile on your website and if he does not upload an image instead of

30
00:02:09,300 --> 00:02:14,040
having an image over here, it's going to show you this particular icon right here, which means that

31
00:02:14,070 --> 00:02:15,620
the image failed to load.

32
00:02:15,630 --> 00:02:19,800
And in this case, the image failed to load because there was no image uploaded.

33
00:02:20,190 --> 00:02:26,340
So in such situations, we actually want to have a placeholder image over here instead of getting this

34
00:02:26,610 --> 00:02:30,210
image, because this is going to spoil the look and feel of your Web site.

35
00:02:30,780 --> 00:02:36,330
So what we will do here is that we will go ahead and go to some Web site to download the default user

36
00:02:36,330 --> 00:02:36,700
image.

37
00:02:36,720 --> 00:02:41,280
So let's go to Google and in here, let's search for Icon Finder.

38
00:02:41,430 --> 00:02:46,560
And the first language pops up is going to be from I can find it so I can type in user over here.

39
00:02:48,440 --> 00:02:53,180
And as you can see, there are a bunch of images in here, and I'll go ahead and click on three right

40
00:02:53,180 --> 00:02:56,990
over here and now you can use any one of these images from here.

41
00:02:57,140 --> 00:03:04,190
So let's say for the user, I want to select this image so I can simply go ahead and download the Panji.

42
00:03:05,760 --> 00:03:12,390
So once the Pangea's downloaded, now what we can do is that we can use this particular image as a default

43
00:03:12,390 --> 00:03:13,290
profile picture.

44
00:03:13,320 --> 00:03:19,230
Now, how exactly can we go ahead and tell Zango that we want to use this picture as the default profile

45
00:03:19,230 --> 00:03:19,680
picture?

46
00:03:20,010 --> 00:03:26,400
So now if you just go ahead and if you view the page source, as you can see, Shango is actually looking

47
00:03:26,400 --> 00:03:32,640
for the profile picture as in this particular location, and it's looking for pictures like profile

48
00:03:32,640 --> 00:03:33,890
picture, dodgy back.

49
00:03:34,380 --> 00:03:37,900
So it's actually looking for the default profile picture here.

50
00:03:38,040 --> 00:03:42,560
So let's go to that particular directory, which is pictures, pictures, slash profile.

51
00:03:42,570 --> 00:03:45,420
So I'll simply open up that particular folder.

52
00:03:46,080 --> 00:03:49,290
So I'll go to the desktop, I'll go to our website.

53
00:03:49,440 --> 00:03:51,120
So we'll go to the mine site.

54
00:03:51,540 --> 00:03:54,680
Then we'll will actually go to the pictures directory.

55
00:03:54,690 --> 00:03:57,620
And in here it's looking for a profile pic Dorjee back.

56
00:03:58,200 --> 00:04:03,800
So right now, as we do not have that, let's actually go ahead and copy that image over here.

57
00:04:03,810 --> 00:04:08,930
So I'll simply go ahead, copy the image which I have downloaded, and I'll paste that image over here.

58
00:04:09,690 --> 00:04:14,880
And now, once we have this image, let's rename this image to Profile Pic Dorjee back.

59
00:04:14,910 --> 00:04:19,110
So I'll type in profile pic dot.

60
00:04:20,490 --> 00:04:28,410
Gypsy, I actually need to remove the extension and now I'll just say that I want to use this as JPEG

61
00:04:28,410 --> 00:04:31,390
and as soon as I upload that, we should be good to go.

62
00:04:31,950 --> 00:04:37,560
Now, if you go back to the profile and now if you hit refresh, as you can see, the default profile

63
00:04:37,560 --> 00:04:42,900
picture is going to appear over here, which is going to be the picture which we have uploaded right

64
00:04:42,900 --> 00:04:43,170
now.

65
00:04:44,010 --> 00:04:46,710
So in this way, now we have a default profile picture.

66
00:04:46,710 --> 00:04:52,470
And instead of giving us the error over here and now, what you can also do is that you can also set

67
00:04:52,470 --> 00:04:54,900
the height and weight of this image as well.

68
00:04:55,020 --> 00:04:59,760
So if your image is getting bigger, you can set the height and weight property over here.

69
00:04:59,760 --> 00:05:04,020
So I can set the width to be, let's say, 100 pixels.

70
00:05:04,350 --> 00:05:06,530
And let's see, I can also set the height.

71
00:05:06,570 --> 00:05:08,310
You want pixels as well.

72
00:05:08,850 --> 00:05:14,680
So now if you see that and if you hit refresh, as you can see now, the image is actually resized.

73
00:05:14,730 --> 00:05:19,220
So that's how you can go ahead and add the default user image in Django.

74
00:05:19,260 --> 00:05:25,280
So the only thing which you need to do is that you need to upload the default image to a specific location.

75
00:05:25,290 --> 00:05:29,800
And in our case, the location was actually pictures, last profile technology back.

76
00:05:30,060 --> 00:05:37,020
And the reason for that is because if you actually go to the model, which is the profile, as you can

77
00:05:37,020 --> 00:05:44,430
see, we have set the default field of this image to profile pic, dodgy pic, which means that if there

78
00:05:44,430 --> 00:05:51,570
is no picture uploaded by the user, it's by default, going to set the default profile image as whatever

79
00:05:51,570 --> 00:05:52,970
is mentioned right over here.

80
00:05:53,920 --> 00:05:57,400
So that's it for this lecture, and I'll see you guys in the next one.

81
00:05:57,790 --> 00:05:58,360
Thank you.

