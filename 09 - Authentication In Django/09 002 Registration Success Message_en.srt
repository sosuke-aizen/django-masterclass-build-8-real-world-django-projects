1
00:00:00,210 --> 00:00:05,700
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how exactly can we

2
00:00:05,700 --> 00:00:09,310
display a particular message when the users sign up a successful.

3
00:00:09,840 --> 00:00:15,870
So right now, when you go ahead and simply type in some credentials over here and if you type in some

4
00:00:15,870 --> 00:00:25,090
password like, let's say test four, five, six, and let's say test four, five, six, and when you

5
00:00:25,090 --> 00:00:27,570
could sign up, it actually does nothing.

6
00:00:27,570 --> 00:00:29,690
And the data or you actually disappears.

7
00:00:30,180 --> 00:00:32,980
So what exactly is happening up over here?

8
00:00:33,390 --> 00:00:39,380
So whenever you go ahead and submit the form data, the form data is going to be submitted by the post

9
00:00:39,390 --> 00:00:39,890
method.

10
00:00:39,900 --> 00:00:46,470
So if you actually go ahead and have a look into the form over here, which is the register form right

11
00:00:46,470 --> 00:00:51,120
over here, as you can see, the request method, which we have used for the form is the post method,

12
00:00:51,540 --> 00:00:56,970
which means that whenever you go ahead and submit the form data, the form letter is going to be submitted

13
00:00:57,210 --> 00:01:01,600
using the post method and the data actually has to be posted somewhere.

14
00:01:01,860 --> 00:01:04,200
So in our case, that's actually not happening.

15
00:01:04,390 --> 00:01:06,830
We are not submitting the data anywhere over here.

16
00:01:07,170 --> 00:01:14,010
So whenever we encounter that post method or whenever the data is submitted, what we wish to do here

17
00:01:14,010 --> 00:01:17,490
is that we wish to actually get some information from the data.

18
00:01:17,490 --> 00:01:21,610
And then we want to display a message that the sign up is successful.

19
00:01:21,990 --> 00:01:25,740
So what we want to do is start whenever the user successfully signs up.

20
00:01:25,940 --> 00:01:31,320
We want to redirect him to a particular page, let's say in this case, index, dot, html template.

21
00:01:31,590 --> 00:01:35,250
And over there on the top, we want to display some sort of message over there.

22
00:01:35,850 --> 00:01:38,360
So how exactly can we go ahead and do that?

23
00:01:38,910 --> 00:01:45,300
So in order to display a message, we need to make modifications to this particular register view right

24
00:01:45,300 --> 00:01:45,790
over here.

25
00:01:46,200 --> 00:01:50,130
So in order to display a message, first of all, you need to import messages.

26
00:01:50,580 --> 00:01:54,690
So in order to import messages, you import them from Shango Dot Cantrip.

27
00:01:54,690 --> 00:01:57,780
So you type in from Shango Dot Cantrip.

28
00:01:59,170 --> 00:02:05,010
Import, that's going to be messages, so once you have the messages, you're pretty much good to go.

29
00:02:05,230 --> 00:02:09,979
And now what we do is that we are going to modify this register you right here.

30
00:02:10,570 --> 00:02:16,690
So this particular thing or this particular code should be executed when we want to create a form.

31
00:02:17,080 --> 00:02:20,260
But what if we actually don't want to create a form?

32
00:02:20,260 --> 00:02:23,230
Instead, we want to submit, as in this case, right over here.

33
00:02:23,800 --> 00:02:29,620
So what usually is happening right now is that whenever you type in something away and can sign up,

34
00:02:29,620 --> 00:02:35,350
we are again getting back this form because when you click the submit button, it's actually going to

35
00:02:35,350 --> 00:02:37,450
execute the code inside this view again.

36
00:02:38,290 --> 00:02:43,960
So the form is going to be created once again and we are going to have the same exact page.

37
00:02:44,530 --> 00:02:50,410
So now what we want to do is that we want to identify when the user clicks, design a button.

38
00:02:50,870 --> 00:02:55,540
So when the user clicks the sign a button, the form is actually posting the data.

39
00:02:55,840 --> 00:03:01,690
So what we wish to do is that whenever there is a post request from the form, we simply want to go

40
00:03:01,690 --> 00:03:02,050
ahead.

41
00:03:02,230 --> 00:03:08,500
And instead of redirecting to the same page, we actually want to go ahead and display a message.

42
00:03:08,740 --> 00:03:15,340
So here what we do is that we check of the request method is the post method, so we type in if request.

43
00:03:16,360 --> 00:03:24,700
Not matter if this is equal, equal to post, then what we wish to do is that we wish to first go ahead

44
00:03:24,970 --> 00:03:30,350
and even before displaying the message, we only want to display the message of the form data is valid.

45
00:03:30,550 --> 00:03:36,490
So in order to check the validity of the form, first of all, you need to go ahead and actually get

46
00:03:36,490 --> 00:03:37,060
the form.

47
00:03:37,060 --> 00:03:38,350
So we dipen form.

48
00:03:39,560 --> 00:03:40,290
Equals.

49
00:03:40,370 --> 00:03:46,880
That's going to be user creation form and now this parameter will of the user creation form is not going

50
00:03:46,880 --> 00:03:47,760
to be empty.

51
00:03:47,810 --> 00:03:50,440
Instead, it's actually going to get the request.

52
00:03:50,750 --> 00:03:53,870
So we type in request dot post.

53
00:03:54,800 --> 00:03:59,360
So that means now the form is going to have all the data which is submitted from the post method.

54
00:04:00,730 --> 00:04:07,060
And now in order to check if this form data is valid, we simply use a method which is called Asla is

55
00:04:07,060 --> 00:04:07,870
valid method.

56
00:04:08,180 --> 00:04:13,490
So you don't have to write the code from scratch to check each and every feel for being valid.

57
00:04:13,760 --> 00:04:16,959
Instead, you can simply type an IV form.

58
00:04:19,160 --> 00:04:24,980
That is underscored valid, so this is the method which is going to check for the validity of your form

59
00:04:25,460 --> 00:04:29,270
and if the form is valid now, you can go ahead and display the message.

60
00:04:29,700 --> 00:04:35,330
So the message which we want to display is that we want to display something like, uh oh, welcome

61
00:04:35,510 --> 00:04:38,010
the user name, your account is created.

62
00:04:38,060 --> 00:04:43,350
So first of all, we need to extract the user name, which the user actually used to register.

63
00:04:43,700 --> 00:04:49,430
So in order to get that username, you type in user name equals, and then we need to extract that from

64
00:04:49,430 --> 00:04:50,000
the form.

65
00:04:50,360 --> 00:05:01,310
So we type and form DOT that's going to be cleaned data and we get the data by typing in dot get and

66
00:05:01,310 --> 00:05:04,440
here you need to specify the feel which you want to get.

67
00:05:04,460 --> 00:05:10,070
So we want to get the user name over here and once we do that now it's going to get the user name and

68
00:05:10,070 --> 00:05:11,420
store it right over here.

69
00:05:11,990 --> 00:05:15,760
Now let's make use of this user name by displaying a message.

70
00:05:15,770 --> 00:05:18,240
So we have already important messages over here.

71
00:05:18,320 --> 00:05:21,200
So here we simply type in messages, Dot.

72
00:05:23,100 --> 00:05:29,550
Success, so as we want to display a success message, we will use success over here and in here, you

73
00:05:29,550 --> 00:05:36,840
need to pass on the request and then to actually display your message, you type an F and then you display

74
00:05:36,840 --> 00:05:37,810
your message over here.

75
00:05:37,830 --> 00:05:42,990
So let's say you say something like welcome and then you need to display the username.

76
00:05:42,990 --> 00:05:44,370
So you type in username.

77
00:05:44,970 --> 00:05:48,480
Then you say something like your account is.

78
00:05:49,550 --> 00:05:53,550
Created, OK, so once we do that, we are pretty much good to go.

79
00:05:54,260 --> 00:06:00,410
So once the account is created and once we display this message, then we want to redirect the user

80
00:06:00,650 --> 00:06:03,410
to a page, which is nothing but the index page.

81
00:06:03,590 --> 00:06:06,890
So we dipen return redirect.

82
00:06:08,640 --> 00:06:12,510
And here simply Dipen Food Coolen Index.

83
00:06:13,430 --> 00:06:16,640
So now once we do that, the user will now be redirected.

84
00:06:17,060 --> 00:06:19,280
Now, as you can see, it's showing us an error.

85
00:06:19,280 --> 00:06:21,870
That's because we have not yet imported redirect.

86
00:06:21,890 --> 00:06:22,490
So simply.

87
00:06:22,490 --> 00:06:25,780
Go ahead, Dipen, redirect over here.

88
00:06:25,790 --> 00:06:28,100
Give a comma and you should be good to go.

89
00:06:28,880 --> 00:06:35,390
So now what we do is that we already haven't closed this particular thing in the IF block, which means

90
00:06:35,390 --> 00:06:40,280
that if the request method for the form is going to be paused, then we actually want to go ahead,

91
00:06:40,280 --> 00:06:45,410
validate the form and then display a message by extracting the user name from the form.

92
00:06:45,860 --> 00:06:52,250
Now, we already had this bunch of code right here, which usually needed to be executed so that we

93
00:06:52,250 --> 00:06:53,570
can actually get the form.

94
00:06:54,050 --> 00:07:00,180
So what we do is that we simply enclose this particular block of code and or else block.

95
00:07:00,440 --> 00:07:04,130
So this should actually be included in the else block.

96
00:07:04,140 --> 00:07:07,640
And once we go ahead and do that, we should be good to go.

97
00:07:08,180 --> 00:07:15,080
So now the way in which this thing will work is that whenever the register view comes up initially,

98
00:07:15,080 --> 00:07:19,170
it's actually going to go ahead and check if the request method is post.

99
00:07:19,520 --> 00:07:22,820
So when you simply go ahead and visit this, you are right here.

100
00:07:24,150 --> 00:07:30,040
The request method is not going to be post, instead it's going to be just a request to get the form.

101
00:07:30,060 --> 00:07:34,980
Hence, we will get the form right here, which means that the code and the else block is going to be

102
00:07:34,980 --> 00:07:35,670
executed.

103
00:07:35,670 --> 00:07:38,820
And we basically get the form right there on our webpage.

104
00:07:39,270 --> 00:07:44,130
Now for the second time, when you go ahead and just submit some form data over here and click on the

105
00:07:44,130 --> 00:07:44,990
sign a button.

106
00:07:45,300 --> 00:07:50,700
In that case, what would happen is that the request method is going to be post and henceforth this

107
00:07:50,700 --> 00:07:52,470
code is going to be executed.

108
00:07:53,130 --> 00:07:54,450
So now let's go ahead.

109
00:07:54,450 --> 00:07:57,870
And was it registered again?

110
00:07:58,290 --> 00:08:04,650
And now let me just go ahead and type in some new username, so I'll type in my name here.

111
00:08:05,650 --> 00:08:12,010
And let's see, the password is a test for five, six.

112
00:08:13,460 --> 00:08:17,730
That's going to be test for five, six as well.

113
00:08:17,750 --> 00:08:23,420
And now when I click on Sign Up, it says that the password is too short, which means that the validation

114
00:08:23,420 --> 00:08:24,900
part over here is working.

115
00:08:25,190 --> 00:08:30,740
And now let's go ahead and test the validation part one more time by typing in some and matching passwords.

116
00:08:30,980 --> 00:08:35,600
So I'll type in any random passwords here and any other random password over here.

117
00:08:35,929 --> 00:08:41,179
And now when I click on Sign Up, as you can see, it says the two password fields don't match.

118
00:08:41,720 --> 00:08:52,880
Now, let me just type in the password as let's see, test test for five, six and let's the test test

119
00:08:53,090 --> 00:08:54,150
for five, six.

120
00:08:54,650 --> 00:09:00,950
So when I click on Sign Up, as you can see, we are actually redirected to this page right here, but

121
00:09:00,950 --> 00:09:04,210
we don't have any sort of a message displayed right over here.

122
00:09:04,490 --> 00:09:06,140
So why exactly is that?

123
00:09:06,450 --> 00:09:12,500
And that is because we have actually passed in the message over here, but we have not included this

124
00:09:12,500 --> 00:09:15,030
message in any one of the Web pages.

125
00:09:15,500 --> 00:09:21,650
So what we wish to do now is that we wish to include those messages over here or display those messages

126
00:09:21,650 --> 00:09:24,420
over here in the index page, which is this page right here.

127
00:09:25,070 --> 00:09:32,210
So in order to include those messages over there, we simply go to the Bistrot HTML file, which is

128
00:09:32,210 --> 00:09:34,070
this file right here in our food app.

129
00:09:34,430 --> 00:09:41,510
And we are actually going to include the code to display the messages over here because our index file

130
00:09:41,540 --> 00:09:46,860
basically inherits the code, which is the navigation bar code from the Bistrot e-mail.

131
00:09:47,270 --> 00:09:51,460
So let's assume we want to display the message right below the navigation bar.

132
00:09:51,620 --> 00:09:55,410
So we will go ahead and write in the code to display the message over here.

133
00:09:56,600 --> 00:10:02,960
So in order to display the message, we first use the syntax over here and we type in if messages,

134
00:10:03,320 --> 00:10:08,210
which means that if we have any sort of a message, then we go ahead and loop through them.

135
00:10:08,750 --> 00:10:15,740
So I also need to include Andy over here and now in order to loop through all those messages we need

136
00:10:15,980 --> 00:10:16,900
for over here.

137
00:10:17,690 --> 00:10:24,170
So we type in for message, which means a single message and messages.

138
00:10:25,500 --> 00:10:34,770
And we also need info over here, so it's open and full and now to get one of those single messages,

139
00:10:34,770 --> 00:10:39,600
you simply go ahead and use double curly braces and type in message over here.

140
00:10:40,440 --> 00:10:44,790
So now once this thing is done, this should work pretty much fine.

141
00:10:44,820 --> 00:10:50,490
So now let's go to register and now let's type in the username.

142
00:10:52,490 --> 00:10:56,000
And now let's type in the password as best.

143
00:10:56,990 --> 00:10:58,850
Best four, five, six.

144
00:11:01,060 --> 00:11:07,830
Test, test, four, five, six, and when I click on Sign Up, as you can see, we get a message over

145
00:11:07,840 --> 00:11:10,570
here, which is a welcome username.

146
00:11:10,620 --> 00:11:16,150
Your account is created and for some reason it's actually displaying the message twice.

147
00:11:16,620 --> 00:11:20,020
So hopefully this does not happen when we log in once again.

148
00:11:20,290 --> 00:11:21,940
So now let's do one more thing.

149
00:11:21,940 --> 00:11:26,230
Let's actually try to modify or style this message a little bit.

150
00:11:26,540 --> 00:11:32,110
So in order to style that thing up, what I will do here is that I'll simply enclose this thing up in

151
00:11:32,110 --> 00:11:37,810
a div, so I'll type in the class and let's say this is going to be a lot.

152
00:11:38,360 --> 00:11:44,260
So we are going to use some bootstrap classes over here and I'll type an alert dash.

153
00:11:44,560 --> 00:11:52,440
And here in order to get the actual tag from the message, I'm simply going to type in message dot type.

154
00:11:52,480 --> 00:11:54,730
So that's going to get the information about tag.

155
00:11:55,120 --> 00:12:01,140
And now let's go ahead and put this message up over here and do so.

156
00:12:01,180 --> 00:12:05,410
Just get that pasted up over here and we should be good to go.

157
00:12:06,160 --> 00:12:08,710
So now let's go back to register again.

158
00:12:08,710 --> 00:12:13,290
And now let me type in the user name and password and let's click on Sign Up.

159
00:12:13,600 --> 00:12:17,260
And as you can see now, this thing is actually still a little bit.

160
00:12:17,560 --> 00:12:20,410
And we also have the message displayed over here.

161
00:12:20,410 --> 00:12:23,380
And now the message is not going to be displayed twice.

162
00:12:23,680 --> 00:12:29,860
And now if you just usually go ahead and visit this page, which is the index page that does by hitting

163
00:12:29,860 --> 00:12:33,720
refresh, as you can see, you won't have any messages displayed up over here.

164
00:12:34,210 --> 00:12:39,830
And that's because that message is only a one time message, which you get on signing up.

165
00:12:39,850 --> 00:12:41,770
So that's it for this lecture.

166
00:12:41,800 --> 00:12:47,470
And hopefully you guys be able to understand how to display a message when the user sign up is successful

167
00:12:47,470 --> 00:12:50,420
and how the validation works in case of signing up.

168
00:12:50,860 --> 00:12:56,620
So in the next lecture, we will go ahead and we will learn how to actually store the form data, which

169
00:12:56,620 --> 00:12:58,690
we have entered in the registration form.

170
00:12:59,230 --> 00:13:03,040
So thank you very much for watching and I'll see you guys next time.

171
00:13:03,340 --> 00:13:03,880
Thank you.

