1
00:00:00,330 --> 00:00:06,689
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how we can put restrictions

2
00:00:06,689 --> 00:00:07,950
on certain routes.

3
00:00:08,230 --> 00:00:14,100
That is how to make certain pages accessible only to the logged in or authenticated users.

4
00:00:14,400 --> 00:00:20,790
Now, the main purpose of having the log in and log out functionality, do you have restrictions on

5
00:00:20,790 --> 00:00:26,650
certain pages, which means that you want to restrict access to certain pages only to registered users?

6
00:00:27,120 --> 00:00:31,920
Now, in this particular lecture, what we will do here is that we will actually go ahead and create

7
00:00:31,920 --> 00:00:33,350
the user profile page.

8
00:00:33,360 --> 00:00:39,630
So if you go to profile right now, as you can see, there is no page which exists, but in this particular

9
00:00:39,630 --> 00:00:45,840
lecture will go ahead and create the user profile page, which the user can access, which is going

10
00:00:45,840 --> 00:00:48,290
to have the information about the logged in user.

11
00:00:48,630 --> 00:00:55,320
And we will also go ahead and also put up a restriction on that particular profile page so that only

12
00:00:55,320 --> 00:00:57,270
the registered users are logged in.

13
00:00:57,270 --> 00:01:00,540
Users will be able to access that particular page.

14
00:01:01,200 --> 00:01:04,560
Now, let's go ahead and start working on the profile page.

15
00:01:04,569 --> 00:01:10,410
So in order to go ahead and do that, first of all, we need to go into the user's app, which is this

16
00:01:10,410 --> 00:01:11,150
app right here.

17
00:01:11,490 --> 00:01:16,920
And in that particular application, we need to go into the views because we always build the functionality,

18
00:01:16,920 --> 00:01:18,240
starting up with The View.

19
00:01:18,690 --> 00:01:20,850
So right now we have the register of you here.

20
00:01:21,180 --> 00:01:26,580
And now let's go ahead and create another view which is going to power up our profile page.

21
00:01:26,970 --> 00:01:30,030
So I'll type in def profile.

22
00:01:31,080 --> 00:01:37,530
Page as the name of this particular view, which is going to accept a request, and then I'm just going

23
00:01:37,530 --> 00:01:42,210
to go ahead and here we are simply going to go ahead and render a particular template.

24
00:01:42,210 --> 00:01:44,010
So I'll type and return.

25
00:01:46,220 --> 00:01:51,050
And this is going to accept that request and let's say we are going to go ahead and create a template

26
00:01:51,050 --> 00:01:53,510
for this and the user's app.

27
00:01:53,720 --> 00:01:59,300
So I'll type in users and let's see the name of this template is profile.

28
00:02:00,740 --> 00:02:01,100
Dot.

29
00:02:02,360 --> 00:02:05,540
So once we go ahead and do that, we are pretty much good to go.

30
00:02:06,170 --> 00:02:09,580
Now let's go ahead and actually create this particular template.

31
00:02:09,590 --> 00:02:12,710
So I'll go to the user's app and the templates.

32
00:02:12,710 --> 00:02:19,850
I'll go ahead and create a new file and I'll name this thing as profile dot HDMI.

33
00:02:20,570 --> 00:02:22,370
And now let's go ahead.

34
00:02:22,610 --> 00:02:26,470
And now as we want to create the page for user.

35
00:02:26,570 --> 00:02:31,650
So it should have the basic information about the user, like the user name and something like that.

36
00:02:31,670 --> 00:02:37,350
So for now, what we will do is that we will just go ahead and try to access the user name of the user.

37
00:02:37,370 --> 00:02:43,250
So I'll have a H1 tag over here, the heading back, and then he will simply extract the user name.

38
00:02:43,730 --> 00:02:49,970
So in order to get the user name, what we will do is that we will go ahead and we will make use of

39
00:02:50,060 --> 00:02:53,030
the same variable which we had in the previous lecture.

40
00:02:53,120 --> 00:02:57,890
So in the previous lecture, we had made use of the user variable, which angle provides.

41
00:02:58,220 --> 00:03:05,540
So I'll type in user dot and to access the user name, I'll simply go ahead and type in user name over

42
00:03:05,540 --> 00:03:05,820
here.

43
00:03:06,740 --> 00:03:10,070
So once we go ahead and do that, we are pretty much good to go.

44
00:03:11,390 --> 00:03:17,930
So now let's go ahead say that so now we have the view, we also have the template as well.

45
00:03:18,200 --> 00:03:24,020
Now, the only thing which we need to do is that we need to add a wall so that we can access this particular

46
00:03:24,020 --> 00:03:24,310
view.

47
00:03:25,070 --> 00:03:29,930
So in order to add a view, we will go to the main projects.

48
00:03:29,930 --> 00:03:36,260
You are to start file, which is the you all start by a file of my site, which is this file right here

49
00:03:36,740 --> 00:03:37,310
and in here.

50
00:03:37,310 --> 00:03:43,760
I'll simply go ahead and add in a path for the profile view, so I'll go ahead and type in path.

51
00:03:44,270 --> 00:03:50,870
And this is going to be, let's see, profile slash and in here will simply go ahead and get access

52
00:03:50,870 --> 00:03:52,910
to the views using user views.

53
00:03:53,420 --> 00:03:55,280
So I'll type in user view start.

54
00:03:55,460 --> 00:03:59,000
And the name of the view is nothing but a profile page.

55
00:03:59,010 --> 00:04:01,010
So I'll go ahead and type that thing in.

56
00:04:01,670 --> 00:04:06,140
And let's say the name of this view is going to be nothing but profile.

57
00:04:06,480 --> 00:04:08,810
So simply give a comma and you're good to go.

58
00:04:09,500 --> 00:04:13,760
So now let's go ahead and try to access the profile page right here.

59
00:04:13,790 --> 00:04:19,959
So now if we go to forward slash profile, it's empty because we are not currently logged then.

60
00:04:20,440 --> 00:04:24,590
So if I go ahead and just log in, let's see what exactly happens.

61
00:04:29,250 --> 00:04:36,810
So when I go ahead and log in, and if we go to slash profile, as you can see, we get the username

62
00:04:36,810 --> 00:04:38,430
of the currently logged and user.

63
00:04:39,270 --> 00:04:46,260
Now, let's go ahead and try to add the link to the profile over here in this particular navigation

64
00:04:46,260 --> 00:04:46,570
bar.

65
00:04:46,980 --> 00:04:53,400
So what I will do over here is that I'll just go ahead, go to the base template, which we had accessed

66
00:04:53,880 --> 00:04:55,320
in the previous lecture.

67
00:04:55,590 --> 00:05:03,330
So right over here, what we want to do is that we want to display the profile page if the user is currently

68
00:05:03,330 --> 00:05:05,610
logged in so I can go ahead and type in.

69
00:05:06,730 --> 00:05:12,490
Yes, chef, and here will add a link to the profile, so I'll type in you all.

70
00:05:12,670 --> 00:05:18,720
And we have currently created the usual pattern for the profile and we have named it as profile.

71
00:05:18,730 --> 00:05:21,570
So let's go ahead and add that thing up over here.

72
00:05:22,090 --> 00:05:26,860
And let's also type in profile over here, which is the display name for this particular.

73
00:05:26,860 --> 00:05:27,190
You are.

74
00:05:28,820 --> 00:05:34,250
OK, so once we go ahead and do that, and now once we hit refresh, as you can see, we also get an

75
00:05:34,250 --> 00:05:35,790
option for profile over here.

76
00:05:36,350 --> 00:05:39,770
So when I click on Profile, we are redirected to this page right here.

77
00:05:40,490 --> 00:05:45,630
Now, let's see what happens when I go ahead and just simply log out from here.

78
00:05:45,830 --> 00:05:49,480
So when I click on Log Out, it says that you have been locked out.

79
00:05:49,910 --> 00:05:54,570
But let's see what happens if we try to access the previous profile page right over here.

80
00:05:54,860 --> 00:06:01,010
So when I go back, as you can see, we are getting an empty profile right over here and we actually

81
00:06:01,010 --> 00:06:02,060
don't want to do that.

82
00:06:02,450 --> 00:06:08,270
That is, we don't want to go ahead and display user blank profile page right here when the user is

83
00:06:08,270 --> 00:06:08,830
locked out.

84
00:06:09,200 --> 00:06:11,120
So let's think of that in this way.

85
00:06:11,540 --> 00:06:17,720
Whenever you're using a social media Web site and whenever you are into your home feed or the feed page,

86
00:06:17,720 --> 00:06:19,400
which you get on logging in.

87
00:06:19,810 --> 00:06:21,980
So what usually happens when you log out?

88
00:06:22,320 --> 00:06:28,130
So whenever you are locked out, you are not allowed to access your own feed that as you are going to

89
00:06:28,130 --> 00:06:34,130
get some sort of error or the social media website is going to prompt you to log in first if you want

90
00:06:34,130 --> 00:06:35,780
to access that particular page.

91
00:06:36,140 --> 00:06:40,730
So we want to do something similar with this particular profile page right here as well.

92
00:06:41,060 --> 00:06:47,270
We want to restrict access to the profile page, which means that if the user is logged out, he should

93
00:06:47,270 --> 00:06:50,030
not be able to access this particular profile page.

94
00:06:50,600 --> 00:06:52,480
So how exactly can we do that?

95
00:06:53,330 --> 00:06:59,660
So whenever you want to restrict access to a certain view in Django, what you simply do is that you

96
00:06:59,660 --> 00:07:03,050
go ahead and add a decorator to that particular view.

97
00:07:03,440 --> 00:07:05,210
So what I exactly mean by that.

98
00:07:05,840 --> 00:07:11,390
So in this particular case, as we want to restrict access to the profile page, so we'll go to the

99
00:07:11,390 --> 00:07:17,120
profile Poipu, which is Disp right here, and we will simply add a decorator, which is called as a

100
00:07:17,150 --> 00:07:18,160
login required.

101
00:07:18,560 --> 00:07:24,890
So in order to add a decorator, you simply will add sign right before the view where you want to add

102
00:07:24,890 --> 00:07:28,490
the decorator and then you need to type in login underscore.

103
00:07:29,830 --> 00:07:36,700
Required, so whenever you go ahead and add this particular login required decorator, it's going to

104
00:07:36,700 --> 00:07:40,640
restrict access to this particular view only to the logged in users.

105
00:07:41,170 --> 00:07:45,920
So now it's showing us another way here because login required is not yet defined.

106
00:07:46,030 --> 00:07:47,680
So let's go ahead and import that.

107
00:07:48,160 --> 00:07:56,740
So I'll type in from Shango dot cantrip dot or dot decorator's.

108
00:07:57,260 --> 00:08:00,190
That's going to be import.

109
00:08:01,230 --> 00:08:08,580
Logan and the school required, so once we go ahead and save that, as you can see, the error actually

110
00:08:08,580 --> 00:08:09,210
disappeared.

111
00:08:10,110 --> 00:08:13,020
Now let's go ahead and try to access the profile page.

112
00:08:13,230 --> 00:08:15,270
So now if we go back over here.

113
00:08:16,210 --> 00:08:18,010
And now currently we are locked out.

114
00:08:18,050 --> 00:08:23,620
So now if I go ahead and try to access the profile page, as you can see, we are going to get an error

115
00:08:23,620 --> 00:08:24,190
over here.

116
00:08:24,760 --> 00:08:30,820
So now that means it's not going to allow us to actually go ahead and access a particular page if we

117
00:08:30,820 --> 00:08:31,430
are locked out.

118
00:08:31,450 --> 00:08:37,030
And if you notice this particular triangle page, as you can see, Django is actually looking for a

119
00:08:37,030 --> 00:08:43,330
page, which is account slash login, which means that Django is trying to redirect us to the login

120
00:08:43,330 --> 00:08:43,770
page.

121
00:08:44,440 --> 00:08:50,050
Now, in case of Django, Django always looks for the login page in this particular path right here

122
00:08:50,050 --> 00:08:53,770
by default, which is flash account slash login.

123
00:08:54,460 --> 00:09:01,540
But right now the path for our login is not slash account slash login, but it simply slash login right

124
00:09:01,540 --> 00:09:02,020
over here.

125
00:09:02,500 --> 00:09:08,970
So now we need to tell Django that it has to look for the login page and the particular you are old

126
00:09:08,980 --> 00:09:11,320
pattern, which is just slash login.

127
00:09:11,650 --> 00:09:13,830
So how exactly can we go ahead and do that?

128
00:09:14,350 --> 00:09:20,950
So in order to do that, you need to go to the Settings Torpy File, which is the file and your main

129
00:09:20,950 --> 00:09:21,370
site.

130
00:09:22,150 --> 00:09:28,270
So let me just go ahead, go to the myside site and in the settings dot, why you need to add a you

131
00:09:28,270 --> 00:09:34,840
are lower here for login, so you need to mention login underscore.

132
00:09:36,150 --> 00:09:42,690
You are, and this year is going to be nothing but the URL for LOG-IN, which is the path for logging,

133
00:09:42,720 --> 00:09:44,650
which we have named as Log-in.

134
00:09:44,910 --> 00:09:50,430
So if you are confused with this, you simply go to the you are to stop by of our main app.

135
00:09:51,690 --> 00:09:57,420
Off our main site, which is the site right here, and this is the path for Log-in, which is actually

136
00:09:57,420 --> 00:10:03,480
named as Log-in, and henceforth in the settings that profile, we actually went ahead and named this

137
00:10:03,480 --> 00:10:04,650
thing as in.

138
00:10:05,010 --> 00:10:08,850
So now if we go ahead and see that, let's see how Django behaves now.

139
00:10:08,880 --> 00:10:15,660
So now if I try to go ahead and access the profile page, as you can see now, we actually redirected

140
00:10:15,900 --> 00:10:17,680
to the login page.

141
00:10:17,700 --> 00:10:21,030
So now when I go ahead and log in, let's see what happens.

142
00:10:21,330 --> 00:10:27,120
So when I go ahead and type in super user as the password, click login.

143
00:10:27,270 --> 00:10:30,990
As you can see now, I am redirected to the profile page.

144
00:10:31,980 --> 00:10:38,700
So that means now we have successfully protected a particular route by using the login required decorator,

145
00:10:38,700 --> 00:10:40,800
which is this decorator right here.

146
00:10:41,070 --> 00:10:47,220
And this decorator is going to make sure that the user is first logged in even before he's able to access

147
00:10:47,220 --> 00:10:48,210
this particular view.

148
00:10:48,540 --> 00:10:53,220
And if the user is not logged in, he's going to redirect us to the login page.

149
00:10:53,640 --> 00:11:01,140
And by default, the Jianguo actually looks for the login page and the part which accounts slash login.

150
00:11:01,470 --> 00:11:07,950
But we have modified that behavior by making changes to the settings that profile right here by mentioning

151
00:11:07,950 --> 00:11:10,680
the new login you URL which Angle should look for.

152
00:11:11,490 --> 00:11:12,880
So that's it for this lecture.

153
00:11:12,990 --> 00:11:16,830
Hopefully you guys be able to understand how to protect certain routes.

154
00:11:17,230 --> 00:11:23,220
So in the next lecture, what we will do is that will go ahead and learn about setting up the profile

155
00:11:23,220 --> 00:11:23,610
page.

156
00:11:23,640 --> 00:11:28,500
So right now, we don't have anything up over here on the profile page and in the upcoming lecture,

157
00:11:28,500 --> 00:11:33,990
we will go ahead and make a few changes over here, like adding up the user profile image and everything

158
00:11:33,990 --> 00:11:34,490
like that.

159
00:11:34,950 --> 00:11:38,880
So thank you very much for watching and I'll see you guys next time.

160
00:11:39,150 --> 00:11:39,750
Thank you.

