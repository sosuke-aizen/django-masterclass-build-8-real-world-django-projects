1
00:00:00,120 --> 00:00:06,120
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how exactly can we

2
00:00:06,120 --> 00:00:09,510
actually store the data submitted by the user in the form.

3
00:00:10,050 --> 00:00:14,760
So even before we learn how to do that, there's a slight correction from the previous lecture.

4
00:00:14,760 --> 00:00:19,760
And that is, uh, this particular thing should actually be message dirtbag's, not tag.

5
00:00:20,130 --> 00:00:25,690
So this is actually to get the color for this particular alert block right over here.

6
00:00:26,100 --> 00:00:30,900
So when you go ahead and fix that issue here and when you go to the sign up right here.

7
00:00:31,290 --> 00:00:36,300
So now when you click on Sign Up, as you can see, you are going to get a much more better styling

8
00:00:36,720 --> 00:00:38,580
for this particular message right here.

9
00:00:38,910 --> 00:00:42,730
OK, so once we are done with this, let's get back to saving the form data.

10
00:00:43,200 --> 00:00:46,470
So saving the form data and Django is actually quite simple.

11
00:00:46,650 --> 00:00:52,390
You just have to go ahead and just type in form that save and once you know that the form is valid.

12
00:00:52,410 --> 00:00:59,100
So if you go to the you start profile right here for the user's app, you just need to go ahead.

13
00:00:59,220 --> 00:01:04,890
And upon checking of the form data as valid, you simply need to type and formed �tzi right over here

14
00:01:04,890 --> 00:01:08,080
and the user is going to be successfully sued for you.

15
00:01:08,640 --> 00:01:15,030
Now, one more thing which you need to take care of is that let's say the user wants to register using

16
00:01:15,030 --> 00:01:15,950
the register page.

17
00:01:16,350 --> 00:01:22,010
So let's say a user registers by a user name, like something like user one to three.

18
00:01:22,440 --> 00:01:28,590
And upon registering, let's say, another user comes up and lets say he uses the same registration

19
00:01:28,590 --> 00:01:30,480
name, which is user one to three.

20
00:01:30,510 --> 00:01:36,150
So in that case, what we want to do is that we don't want to sign up the next user with the same user

21
00:01:36,150 --> 00:01:40,620
name because we want all the user names on our website to be unique.

22
00:01:41,010 --> 00:01:43,290
So how exactly can we handle that?

23
00:01:43,830 --> 00:01:47,910
So in order to handle that, you don't need to write any extra bit of code.

24
00:01:48,180 --> 00:01:52,050
This form that is valid method actually takes care of that as well.

25
00:01:52,170 --> 00:01:59,160
So if you try to register the user or the same user name twice, then it's going to show you some sort

26
00:01:59,160 --> 00:02:01,210
of an invalid error right over here.

27
00:02:01,770 --> 00:02:05,960
So in order to test that, let's actually go to the admin site right here.

28
00:02:05,970 --> 00:02:11,130
So I'll go to admin and I'll type in my name over here.

29
00:02:11,280 --> 00:02:15,400
And the password is, I suppose, super user.

30
00:02:15,570 --> 00:02:17,190
So when I go ahead and log in.

31
00:02:19,160 --> 00:02:22,430
I guess this is not the correct admin name.

32
00:02:22,460 --> 00:02:29,000
OK, so now once I log in and once I go to the users right over here, as you can see, uh, this is

33
00:02:29,000 --> 00:02:31,160
the only user which exists right now.

34
00:02:31,580 --> 00:02:37,130
So let's try to go ahead and try to register a new user with this username right here.

35
00:02:37,370 --> 00:02:43,140
Or in your case, you might have your own username, which you have used while creating the super user.

36
00:02:43,460 --> 00:02:48,300
So simply try to use that super user user name and see what actually happens.

37
00:02:48,320 --> 00:02:53,960
So I'm going to type in my name here, which is the super user name, and I'll type in some dummy password,

38
00:02:53,960 --> 00:02:57,080
as does test for five six.

39
00:02:58,060 --> 00:03:06,940
Test test for physics now when I go ahead and try to sign up, as you can see, you will get a message

40
00:03:06,940 --> 00:03:13,630
over here, which is that a user with that username already exist, which means we are also successfully

41
00:03:13,630 --> 00:03:20,490
validating the user's username feel in order to check if a user with that username already exists.

42
00:03:20,740 --> 00:03:25,930
And if that actually exists, then Django is actually going to throw up this error and it's going to

43
00:03:25,930 --> 00:03:28,950
validate that the username should be unique.

44
00:03:28,960 --> 00:03:32,140
So we don't have to worry about validating that separately.

45
00:03:32,140 --> 00:03:33,790
That's already taken care of.

46
00:03:34,480 --> 00:03:40,630
OK, so now once we know that, we can now simply go ahead and actually save the user by just simply

47
00:03:40,630 --> 00:03:44,140
typing in form dot safe and that's it.

48
00:03:44,140 --> 00:03:46,390
You don't have to do anything else apart from that.

49
00:03:46,420 --> 00:03:51,190
Now let's go ahead and try to create a user and let's see what exactly happens.

50
00:03:51,340 --> 00:03:53,570
So I'll create a new user over here.

51
00:03:53,710 --> 00:03:55,960
So let me just go ahead and refresh.

52
00:03:56,860 --> 00:04:04,870
Or let's maybe go to register from scratch and now I'll simply go ahead and submit some sort of a username

53
00:04:04,960 --> 00:04:09,910
or let's say I'll type in test user.

54
00:04:11,090 --> 00:04:16,130
And let's say you pass this test test for five, six.

55
00:04:17,649 --> 00:04:20,339
Test test for physics.

56
00:04:20,829 --> 00:04:26,380
So when I click on Sign Up, as you can see, it says, Welcome test user, your account is created.

57
00:04:26,860 --> 00:04:33,070
So now if you go back over here and hit refresh in the Django admin panel, as you can see, you will

58
00:04:33,070 --> 00:04:36,160
notice that the test user is created right over here.

59
00:04:36,520 --> 00:04:38,330
So here we have the user name Feel.

60
00:04:38,740 --> 00:04:43,600
We also have other fields as well, which is the email address, the first name, last name and the

61
00:04:43,600 --> 00:04:44,440
staff status.

62
00:04:44,740 --> 00:04:48,400
So these three fields over here are still empty, as you could see.

63
00:04:49,030 --> 00:04:52,760
So what if you actually want to add another over here as well?

64
00:04:52,780 --> 00:04:58,570
So let's say if you also want to accept the email address of the test user or the new user, which you

65
00:04:58,570 --> 00:05:01,180
are creating, how exactly can we do that?

66
00:05:01,630 --> 00:05:04,000
So we are going to learn that in the next lecture.

67
00:05:04,540 --> 00:05:09,970
Now, one more thing which you'll notice here is that under the staff status over here, we actually

68
00:05:09,970 --> 00:05:13,450
have a cross sign for the test user and for the super user.

69
00:05:13,450 --> 00:05:19,960
We have a right sign over here, which means that this user is actually a super user and this test user

70
00:05:19,960 --> 00:05:21,860
is not actually a super user.

71
00:05:22,360 --> 00:05:28,470
So that means we have now successfully implemented the functionality to sign up users on our website.

72
00:05:28,660 --> 00:05:35,050
But the only thing which we need to make sure is that we also get the email addresses along with the

73
00:05:35,050 --> 00:05:36,580
user name and password.

74
00:05:36,940 --> 00:05:41,450
So in the next lecture, we will learn how to add the email feel to the registration forms.

75
00:05:41,470 --> 00:05:47,380
So in order to accept the email address, you also need to make sure that you have the email address

76
00:05:47,380 --> 00:05:48,450
feel right over here.

77
00:05:49,330 --> 00:05:50,920
So that's it for this lecture.

78
00:05:50,950 --> 00:05:52,840
And I'll see you guys in the next one.

79
00:05:53,320 --> 00:05:53,890
Thank you.

