1
00:00:00,300 --> 00:00:06,220
So now let's go ahead and try to access the user profile model and try to add some profiles in here.

2
00:00:06,720 --> 00:00:11,610
So when you click on this particular ad profile button, as you can see, it's going to give you a form

3
00:00:11,610 --> 00:00:13,080
to add a user profile.

4
00:00:13,590 --> 00:00:17,770
Now, even before you add the user profile, you need to select a particular user.

5
00:00:17,940 --> 00:00:23,000
So let's say I'm going to add the user profile for my admin user, which is my name right here.

6
00:00:23,010 --> 00:00:24,300
So I simply select that.

7
00:00:24,750 --> 00:00:27,660
And now it's going to ask me to choose an image.

8
00:00:27,780 --> 00:00:28,880
So I'll click over here.

9
00:00:29,640 --> 00:00:34,490
And what I will do is that I'll simply select a particular image from my desktop.

10
00:00:34,500 --> 00:00:40,170
So I'll go to desktop and I'll select this particular image, which is my image right here.

11
00:00:40,350 --> 00:00:46,200
So I'll click on Open and it's going to upload the image for me and now location.

12
00:00:46,380 --> 00:00:49,100
So I'll simply type in my city location over here.

13
00:00:49,800 --> 00:00:54,960
And when I click on Save, it's actually going to go ahead and create the user profile for me.

14
00:00:55,740 --> 00:01:02,250
So now if I go ahead and click on my profile right here for my username, as you can see, I will have

15
00:01:02,250 --> 00:01:08,550
the information like the user name, the image and the location so I can go ahead, save this or even

16
00:01:08,550 --> 00:01:09,960
edit this if I want to.

17
00:01:10,350 --> 00:01:17,940
So now let's go ahead and learn where exactly the image is uploaded and my case for this particular

18
00:01:17,940 --> 00:01:18,820
profile user.

19
00:01:19,290 --> 00:01:24,960
So in order to know that what we will do is that will open up the terminal, we will stop the server

20
00:01:24,960 --> 00:01:28,590
for now and we will try to access the Python channel.

21
00:01:28,800 --> 00:01:34,570
So I'll type in Python three managed by Shell to get access to Shell.

22
00:01:35,160 --> 00:01:41,610
Now, once we enter this particular shell, what we will do is that we will access the inbuilt user

23
00:01:41,610 --> 00:01:41,940
model.

24
00:01:42,210 --> 00:01:44,280
So I'll dipen from.

25
00:01:45,520 --> 00:01:52,570
Chango Dort Cantrip dot art, not models, import user.

26
00:01:53,350 --> 00:01:59,650
So once we have the user model imported, let's try to go ahead and access the user, which we have

27
00:01:59,650 --> 00:02:01,850
currently created the profile for.

28
00:02:01,900 --> 00:02:10,060
So I'll type in user equals user dot and in order to get the object open objects and we are going to

29
00:02:10,060 --> 00:02:12,420
filter the object depending upon the user name.

30
00:02:12,430 --> 00:02:16,830
So I'll type and filter over here and we are going to filter by user name.

31
00:02:16,840 --> 00:02:19,830
So the user name is going to be my name.

32
00:02:20,920 --> 00:02:24,760
And in your case you might add your name over there and I'll type in DOT.

33
00:02:25,880 --> 00:02:29,490
First, to get the first result of this particular object.

34
00:02:30,110 --> 00:02:35,420
So when I go ahead and do that and when I type in user, as you can see, we get the user name over

35
00:02:35,420 --> 00:02:35,690
here.

36
00:02:36,350 --> 00:02:41,180
So now in order to get the profile, I can simply type in user dot profile.

37
00:02:42,570 --> 00:02:48,330
And when I go ahead and enter, I will also get the profile for my username, and now in order to access

38
00:02:48,330 --> 00:02:54,300
the image, we can go ahead and type in user dot profile, dot image.

39
00:02:55,050 --> 00:03:00,030
So when I go ahead and do that, as you can see, I will get the image filled file right here.

40
00:03:00,270 --> 00:03:05,760
And that file is present nowhere else, but it's actually present in the profile pictures directly.

41
00:03:06,090 --> 00:03:08,820
And the name of that image is my name Dorji.

42
00:03:09,450 --> 00:03:11,550
Now, I've actually named that image over here.

43
00:03:11,560 --> 00:03:14,160
That's why it's going to have my name over here.

44
00:03:14,190 --> 00:03:18,660
But in case if we have uploaded some other image, it's going to have a different name.

45
00:03:19,110 --> 00:03:25,420
So now we need to learn where exactly do we have this particular directly, which is the profile pictures

46
00:03:25,440 --> 00:03:25,980
directly.

47
00:03:26,040 --> 00:03:28,830
So now if you go ahead, open up your code editor.

48
00:03:29,070 --> 00:03:33,290
And if you scroll through the files right here, as you can see, you will have the profile pictures

49
00:03:33,300 --> 00:03:36,700
directly and you will also have your image up over here.

50
00:03:37,740 --> 00:03:44,460
So that means Django is actually storing the profile pictures right inside the machine directly, which

51
00:03:44,460 --> 00:03:49,050
is the directly which contains all the app and the myside project.

52
00:03:49,140 --> 00:03:52,070
But we don't actually want to go ahead and do it this way.

53
00:03:52,440 --> 00:03:58,290
Instead, we want to organize this particular image systematically in some sort of other folder.

54
00:03:58,650 --> 00:04:05,100
So now we need to tell Django that don't store these images up over here, instead store them in a different

55
00:04:05,100 --> 00:04:05,700
directory.

56
00:04:06,000 --> 00:04:09,400
So how exactly can we go ahead and make changes to that?

57
00:04:10,110 --> 00:04:16,180
So whenever you want to make changes to the part where these images are stored, we simply go to settings

58
00:04:16,180 --> 00:04:20,490
dot by file and we are going to define a variable which is called as media root.

59
00:04:21,089 --> 00:04:24,980
So type in media and the school route over here.

60
00:04:25,410 --> 00:04:30,750
And this is actually going to have the part where the images are actually stored.

61
00:04:31,140 --> 00:04:37,440
Now, in the previous cases, we have also defined the parts for the login redirect you are, the login

62
00:04:37,440 --> 00:04:38,970
you are and everything like that.

63
00:04:39,210 --> 00:04:43,290
And these were the actual URLs which were present on the website.

64
00:04:43,590 --> 00:04:47,970
But in this case, we are not actually going to store images on our website.

65
00:04:48,150 --> 00:04:50,550
Instead, we want to save those images on.

66
00:04:51,300 --> 00:04:53,170
So what exactly do I mean by that?

67
00:04:53,340 --> 00:04:58,790
So we do have the image below here, which is this field right here in our database model.

68
00:04:59,130 --> 00:05:05,250
But in reality, what is going to happen is that this image is not going to be stored in database because

69
00:05:05,250 --> 00:05:08,340
storing images in a database is highly inefficient.

70
00:05:08,580 --> 00:05:12,150
Instead, those images are actually present on ourselves.

71
00:05:12,510 --> 00:05:15,600
So in our case, Asaba is nothing but a computer.

72
00:05:15,990 --> 00:05:20,140
So in this case, we want to store those images up on our computer.

73
00:05:20,850 --> 00:05:27,090
So here in case of media route, we are not going to define the path which is going to be present on

74
00:05:27,090 --> 00:05:29,220
our website or our Web application.

75
00:05:29,250 --> 00:05:33,170
Instead, we actually want to store this onto our operating system.

76
00:05:33,210 --> 00:05:40,280
So I'll type in or start Pappe, which is going to get the current path of our directory or of our project.

77
00:05:40,980 --> 00:05:48,990
So we'll type an all star path DOT join and we are going to join the current path, which is dBASE directly.

78
00:05:49,380 --> 00:05:56,560
So I'll type in base here, over here and paste directly is nothing but the path of our current project.

79
00:05:56,580 --> 00:06:01,320
So we are going to use that and we are going to join it with the new directorate, which we want to

80
00:06:01,320 --> 00:06:01,780
create.

81
00:06:02,260 --> 00:06:07,650
So in here, instead of directly having the profile pictures directory, let's say you want to create

82
00:06:07,650 --> 00:06:13,290
another directory, which is going to contain the profile pictures directly and then contain the images.

83
00:06:13,690 --> 00:06:14,520
So let's see.

84
00:06:14,520 --> 00:06:17,760
That directly is going to be named as pictures.

85
00:06:17,760 --> 00:06:19,740
So I'll simply add pictures over here.

86
00:06:19,950 --> 00:06:25,410
Now, the way in which angle will work is that Shango will actually create the pictures directly in

87
00:06:25,410 --> 00:06:28,520
the main project and in the pictures directly.

88
00:06:28,530 --> 00:06:33,610
We are going to have another directory, which is profile pictures, which is going to save our images.

89
00:06:33,750 --> 00:06:40,200
Now, once we go ahead and do that, and once we want to access those saved images, what we also want

90
00:06:40,200 --> 00:06:45,660
to do is that we also want to define the are where the media of our application lies.

91
00:06:45,960 --> 00:06:49,800
So in order to do that, I love and media underscore.

92
00:06:50,680 --> 00:06:55,870
You all and that's going to be equal to nothing, but this particular picture's directly right over

93
00:06:55,870 --> 00:06:57,940
here, so I'll go ahead and type in slash.

94
00:06:59,180 --> 00:07:04,680
Pictures, and then we are going to go now once we go ahead and do that.

95
00:07:04,700 --> 00:07:07,800
The changes won't be reflected in our current user models.

96
00:07:07,910 --> 00:07:13,280
So if you go to the current user model, that particular image is still going to be present over here

97
00:07:13,280 --> 00:07:14,900
in the profile pictures directly.

98
00:07:14,960 --> 00:07:17,720
But now if we go ahead and start the server.

99
00:07:17,810 --> 00:07:25,340
So let me first exit this particular shell and then start the server by typing in Python three managed

100
00:07:25,340 --> 00:07:26,120
up by.

101
00:07:27,220 --> 00:07:33,460
Runs well and now runs the service up and running, I can hit refresh, I can access this particular

102
00:07:33,460 --> 00:07:38,390
thing, delete my profile and let's add a new profile for the same user.

103
00:07:38,650 --> 00:07:41,170
And now let's select my picture.

104
00:07:42,250 --> 00:07:44,700
Let's add my location as well.

105
00:07:44,710 --> 00:07:52,390
And when I click on Save now, if you go to the code editor, as you can see, you will have a new directly

106
00:07:52,420 --> 00:07:55,060
over here, which is called Pictures and None.

107
00:07:55,060 --> 00:07:59,200
Now, these pictures directly will have another directly, which is called as profile pictures.

108
00:07:59,200 --> 00:08:04,930
And that's eventually going to contain all the images, which means that we now have a systematic way

109
00:08:04,930 --> 00:08:10,120
to save images in a particular directory so that our project won't get cluttered.

110
00:08:11,360 --> 00:08:17,360
So that's it for this lecture and in the next lecture, what we will do is that now as we have the image

111
00:08:17,660 --> 00:08:23,720
and now as we have the other information about the profile as well, we can go ahead and start working

112
00:08:23,720 --> 00:08:28,770
on this profile page so that it also displays the image as well as the location.

113
00:08:28,790 --> 00:08:33,679
So in the next lecture, we will go ahead and we will learn how to access the image over here on the

114
00:08:33,679 --> 00:08:34,559
profile page.

115
00:08:34,760 --> 00:08:36,679
So thank you very much for watching.

116
00:08:36,679 --> 00:08:38,750
And I'll see you guys next time.

117
00:08:39,200 --> 00:08:39,830
Thank you.

