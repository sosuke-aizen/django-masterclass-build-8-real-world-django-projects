1
00:00:00,540 --> 00:00:02,430
Hello and welcome to this lecture.

2
00:00:02,460 --> 00:00:09,030
And in this lecture, we will go ahead and learn how to implement the login and a logout functionality.

3
00:00:09,570 --> 00:00:14,070
So in the previous lectures, we have actually created the registration functionality.

4
00:00:14,400 --> 00:00:20,040
And the way in which we have created that functionality is that first we have created this view right

5
00:00:20,040 --> 00:00:21,990
here, which was called the Register of You.

6
00:00:22,290 --> 00:00:26,280
Then we have actually created this template as well, which is nothing.

7
00:00:26,280 --> 00:00:28,530
But we have added a form and a submit button.

8
00:00:28,770 --> 00:00:35,130
And also we actually had to go ahead and go into the you Alstott profile of our main site, which is

9
00:00:35,130 --> 00:00:37,260
nothing but this file right here.

10
00:00:37,350 --> 00:00:40,990
And we have simply added up the part for the latest of you.

11
00:00:41,400 --> 00:00:46,560
So this is all we had to do in order to go ahead and implement the register functionality.

12
00:00:46,890 --> 00:00:52,560
But the way in which by implementing the login functionality is going to be slightly different.

13
00:00:52,590 --> 00:00:58,260
So in case of implementing the login functionality, you don't have to actually go ahead and write in

14
00:00:58,260 --> 00:00:58,620
a view.

15
00:00:59,130 --> 00:01:04,830
Instead, Janger actually provides you with an inbuilt view which is going to take care of the entire

16
00:01:04,830 --> 00:01:08,730
login functionality and you just have to create a small template for it.

17
00:01:08,850 --> 00:01:11,290
So now let's learn how exactly can we do that.

18
00:01:11,730 --> 00:01:17,790
So whenever you want to build something or some functionality, you always go ahead and start with either

19
00:01:17,790 --> 00:01:22,870
of you or you simply go ahead and start with the you are the pattern, which is this right here.

20
00:01:23,070 --> 00:01:30,570
So we are actually going to add the log out and log in functionality to the URL patterns of the main

21
00:01:30,570 --> 00:01:33,810
site, which is my site right here, or the main project.

22
00:01:34,170 --> 00:01:38,670
So now let's go ahead and actually have a path to create a login.

23
00:01:39,030 --> 00:01:44,900
So as I mentioned, the login functionality is going to use an inbuilt view from Shango.

24
00:01:45,240 --> 00:01:49,990
So even before we start creating the path, let's actually import that particular view.

25
00:01:50,490 --> 00:01:56,940
So in order to impose those views, you simply type from Django Dot Cantrip.

26
00:01:58,470 --> 00:01:59,400
Dot, dot.

27
00:02:01,180 --> 00:02:09,400
We need to import views now instead of simply importing views over here, you also name this particular

28
00:02:09,400 --> 00:02:14,920
import as something, because we have already imported certain kinds of views here and that might actually

29
00:02:14,920 --> 00:02:16,000
cause confusion's.

30
00:02:16,210 --> 00:02:19,770
So we have named the views from the users as users view.

31
00:02:20,080 --> 00:02:25,870
So in order to import views from the triangle, dot, dot, dot, dot, we will actually go ahead and

32
00:02:25,870 --> 00:02:30,040
name them as authentication views so I can simply type in as.

33
00:02:31,850 --> 00:02:39,590
Authentication and the school of use, so once we go ahead and do that, we are pretty much good to

34
00:02:39,590 --> 00:02:39,830
go.

35
00:02:40,130 --> 00:02:43,700
So now let's go ahead and create the path for the legal view.

36
00:02:44,270 --> 00:02:46,040
So I'll type in path over here.

37
00:02:46,190 --> 00:02:52,210
And let's say for login, you simply go ahead and type in login slash.

38
00:02:52,490 --> 00:02:58,940
And now here, in order to add views, we are going to make use of this particular views, which we

39
00:02:58,940 --> 00:02:59,960
have just imported.

40
00:02:59,960 --> 00:03:02,030
So I can type in authentication of use.

41
00:03:02,690 --> 00:03:11,320
And now in order to get access to the login view, you simply go ahead and type in dot login view.

42
00:03:11,990 --> 00:03:18,650
And now one more interesting thing about this inbuilt login view is that this is not just a normal regular

43
00:03:18,650 --> 00:03:19,000
view.

44
00:03:19,010 --> 00:03:21,540
Instead, they are actually Class-Based Peus.

45
00:03:21,980 --> 00:03:27,140
Now, we have not learned about Class-Based views yet, but soon we are going to do that in the upcoming

46
00:03:27,140 --> 00:03:27,670
lectures.

47
00:03:28,010 --> 00:03:33,950
But for now, you need to understand that whenever you are using a class based view and whenever you

48
00:03:33,950 --> 00:03:39,740
actually want to have access to them, you always go ahead and type in Dot ArcView.

49
00:03:40,850 --> 00:03:46,820
So now, once we are done with this, let's also add the name for the euro, so that's going to be,

50
00:03:47,150 --> 00:03:50,930
let's say Log-in, simply give a comma and we are good to go.

51
00:03:51,620 --> 00:03:57,200
So just as we have added the path for dialogue and view, let's also go ahead and create a path for

52
00:03:57,200 --> 00:03:58,440
the logout view as well.

53
00:03:58,880 --> 00:04:05,960
So you simply need to go ahead, copy that piece the over here and replace this with logout.

54
00:04:06,840 --> 00:04:14,730
And instead of view for the log functionality, we will go ahead and use the logout view and let's name

55
00:04:14,730 --> 00:04:17,040
this you are as logout.

56
00:04:17,810 --> 00:04:23,460
OK, so now once this thing is done, we have set up the actual real patterns for these parts right

57
00:04:23,460 --> 00:04:23,760
here.

58
00:04:24,480 --> 00:04:29,910
And once we have set up the parts, we always go ahead and make sure to create views.

59
00:04:29,910 --> 00:04:32,880
But in this case, we already have those views over here.

60
00:04:33,390 --> 00:04:39,450
So now the one important thing which remains is that you don't have templates for the log in and log

61
00:04:39,450 --> 00:04:39,900
out view.

62
00:04:40,140 --> 00:04:46,380
So the log in and log out we use are actually inbuilt, but still they require templates to actually

63
00:04:46,380 --> 00:04:46,980
function.

64
00:04:46,980 --> 00:04:53,160
And in order to confirm if that's true, let's actually go ahead, run the server and let's try to access

65
00:04:53,160 --> 00:04:55,470
a log in over here and let's see what do we get.

66
00:04:55,500 --> 00:05:01,050
So if I go ahead and type in slash login, I guess I misspelled this.

67
00:05:01,050 --> 00:05:02,550
There should be log in.

68
00:05:03,090 --> 00:05:09,600
And as you can see when I go ahead and type and log in, it says that template does not exist at large

69
00:05:09,630 --> 00:05:16,410
login, which means that Django is actually looking for the login template in the registrations directorate.

70
00:05:16,980 --> 00:05:22,500
So now what we want to do here is that we want to create a template for the log in view.

71
00:05:22,680 --> 00:05:28,530
But one more thing which you need to do now is that you need to tell Shango that don't look for the

72
00:05:28,530 --> 00:05:31,020
login template in the registration strategy.

73
00:05:31,290 --> 00:05:35,230
Instead, look for that in the user's directory.

74
00:05:35,430 --> 00:05:42,360
So in order to mention a new pad for the template, you actually go ahead, go into the ArcView over

75
00:05:42,360 --> 00:05:43,560
here and here.

76
00:05:43,560 --> 00:05:47,490
You can define the part where Django needs to look for the login template.

77
00:05:47,760 --> 00:05:55,110
So in order to add that part, you can type in template and the school name and you can type in this

78
00:05:55,110 --> 00:06:01,420
equals and then specify the actual path where Django needs to look for the login template.

79
00:06:01,440 --> 00:06:08,100
So we are going to place the login template in users and the name of that is going to be log in dot

80
00:06:08,100 --> 00:06:08,880
HTML.

81
00:06:10,710 --> 00:06:17,370
So we simply go ahead and type that thing in over here and now we also do the same thing with logout

82
00:06:17,370 --> 00:06:22,170
as well, that is wide open template and the school name is going to be equal to.

83
00:06:23,950 --> 00:06:27,630
User slash logout dot e-mail.

84
00:06:29,480 --> 00:06:32,580
OK, so once we do that, we are pretty much good to go.

85
00:06:32,930 --> 00:06:37,940
Now the only thing which we need to do is that we need to create these respective templates, which

86
00:06:37,940 --> 00:06:41,180
is the login, not H.M. and Logout or HTML.

87
00:06:41,600 --> 00:06:46,600
So let's go to the user's directory, which is the strategy right here and in the users.

88
00:06:46,620 --> 00:06:48,260
Let's go to templates again.

89
00:06:48,260 --> 00:06:55,280
Go to users and let's create a new file here and let's name this thing as log and dot its Gemalto.

90
00:06:56,150 --> 00:07:01,410
So once we have this log in, not each shamal, you can simply go ahead and create a form right here.

91
00:07:01,670 --> 00:07:07,280
So whenever you want to create a basic form, you simply go ahead, type in the form tag, then you

92
00:07:07,280 --> 00:07:07,790
go ahead.

93
00:07:07,970 --> 00:07:10,640
And first of all, at the same sort of token.

94
00:07:11,240 --> 00:07:18,170
So you type in see as all of underscore token and then you simply go ahead and type and form and the

95
00:07:18,170 --> 00:07:18,860
syntax.

96
00:07:19,400 --> 00:07:24,800
Then finally you need a submit button soil type and button type is going to be submit.

97
00:07:25,690 --> 00:07:32,440
And then you simply say something like Logan, and once we have created this form, we are pretty much

98
00:07:32,440 --> 00:07:33,040
good to go.

99
00:07:33,190 --> 00:07:37,130
And let me just make sure that if we have not left anything over here.

100
00:07:37,870 --> 00:07:44,710
OK, so one thing which we also need to add is that we also need to add the method as opposed to adhere

101
00:07:44,740 --> 00:07:46,120
to the login form.

102
00:07:46,510 --> 00:07:49,350
So you type in method equals post.

103
00:07:50,230 --> 00:07:55,150
OK, so once we are done with this, let's actually save the code and let's see if the form actually

104
00:07:55,150 --> 00:07:55,580
works.

105
00:07:56,260 --> 00:08:00,300
So let me just go ahead and let me open up the login form.

106
00:08:00,310 --> 00:08:04,780
And as you can see, when you go to the local hospital, it thousands log in.

107
00:08:05,050 --> 00:08:06,710
You should get this form right here.

108
00:08:07,270 --> 00:08:13,870
Now, even before testing this form, let's first go ahead and let's make sure if the admin user is

109
00:08:13,870 --> 00:08:14,460
locked in.

110
00:08:14,470 --> 00:08:17,440
So let's simply go to slash admin.

111
00:08:17,560 --> 00:08:20,400
And right now, as you can see, the admin user is logged in.

112
00:08:20,710 --> 00:08:25,810
So let's try to log out from here and let's see if we can log in as admin from here.

113
00:08:25,960 --> 00:08:28,930
So right now, if you go to the admin site.

114
00:08:30,210 --> 00:08:34,240
You will get this form right here, which means the admin is not logged in yet.

115
00:08:34,530 --> 00:08:37,880
So now let's go ahead and try to log in as admin from here.

116
00:08:37,919 --> 00:08:41,370
So I'll type in my admin or the super user credentials over here.

117
00:08:41,820 --> 00:08:43,710
So I'll type in my name then.

118
00:08:43,710 --> 00:08:46,250
The password is super user.

119
00:08:46,800 --> 00:08:48,870
And now when I go ahead and click login.

120
00:08:50,720 --> 00:08:57,230
As you can see, you actually get an arrow over here, which this page not found, and that's because

121
00:08:57,470 --> 00:08:59,780
if you have a look at this, you are right here.

122
00:09:00,080 --> 00:09:05,990
You will notice that Django is now trying to redirect us to this particular you are right here, which

123
00:09:05,990 --> 00:09:08,420
is the account slash profile.

124
00:09:09,360 --> 00:09:15,750
And this is nothing but the profile page of the user, which we have just created, but as we all know,

125
00:09:15,750 --> 00:09:18,980
we have not yet created any sort of profile page for users.

126
00:09:19,200 --> 00:09:23,900
And that's actually fine for now because we have not yet created this user profile page.

127
00:09:24,270 --> 00:09:28,830
But right now, let's see if we actually now have access to the admin panel.

128
00:09:29,220 --> 00:09:32,100
So now I'll go ahead and type in slash admin.

129
00:09:32,100 --> 00:09:34,040
And when I go ahead and hit enter.

130
00:09:34,410 --> 00:09:40,320
As you can see, we are now logged in and to the admin panel, which means that the login functionality

131
00:09:40,320 --> 00:09:41,620
was successful.

132
00:09:42,900 --> 00:09:49,620
So now once the login functionality is successful, we also need to change the way in which the login

133
00:09:49,620 --> 00:09:52,920
form actually directs us when we successfully login.

134
00:09:53,310 --> 00:09:56,220
So as I mentioned, now we are logged in.

135
00:09:56,370 --> 00:09:59,700
Whenever we log in, we are redirected to this particular UOL.

136
00:10:00,030 --> 00:10:03,090
But let's say we want to redirect to some other.

137
00:10:03,090 --> 00:10:03,450
You are.

138
00:10:03,690 --> 00:10:05,490
So how exactly can we do that?

139
00:10:06,030 --> 00:10:12,120
We can change this default Shango behavior by just going into these settings dot by file.

140
00:10:12,330 --> 00:10:19,110
So if you go to the settings or profile of your main app and if you scroll down over here, here, you

141
00:10:19,110 --> 00:10:22,040
can add your very own settings which you want.

142
00:10:22,440 --> 00:10:29,310
So in order to change the login redirect, you are all you need to simply go ahead and type in login,

143
00:10:29,310 --> 00:10:32,640
underscore, redirect, underscore you are.

144
00:10:32,850 --> 00:10:38,660
And here you can specify the part, which is the path for the index or the index e-mail template.

145
00:10:38,670 --> 00:10:42,640
So for that you can simply type in food colon index.

146
00:10:42,660 --> 00:10:46,080
So now once we are done with this, we can simply go ahead and save the code.

147
00:10:46,110 --> 00:10:49,510
So now let's see what exactly happens if we actually log in.

148
00:10:49,860 --> 00:10:54,400
So now let me just go ahead and log out from here, so I'll click on the log out button.

149
00:10:54,510 --> 00:10:57,930
And now let's go ahead and try to access the login page.

150
00:10:58,470 --> 00:11:01,350
So let's log in with the admin credentials over here.

151
00:11:01,350 --> 00:11:05,780
So I'll type in my name, then I'll type in super user as well.

152
00:11:05,790 --> 00:11:12,510
And when I click on Log In, as you can see now, we are actually redirected to the index page and not

153
00:11:12,510 --> 00:11:18,390
to the user profile page, which we had not created, which means that we have successfully implemented

154
00:11:18,390 --> 00:11:19,740
the login functionality.

155
00:11:20,130 --> 00:11:25,890
So in the next lecture, what we will do is that will go ahead and make sure that when the user actually

156
00:11:25,890 --> 00:11:29,340
registers, he is not redirected to the index page.

157
00:11:29,340 --> 00:11:36,150
But instead, upon successfully completing his registration, he is redirected to the login page, which

158
00:11:36,150 --> 00:11:37,660
is this page right here.

159
00:11:38,310 --> 00:11:40,170
So thank you very much for watching.

160
00:11:40,170 --> 00:11:42,180
And I'll see you guys next time.

161
00:11:42,450 --> 00:11:43,110
Thank you.

