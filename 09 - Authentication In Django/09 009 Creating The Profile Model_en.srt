1
00:00:00,150 --> 00:00:05,790
Hello and welcome to this lecture and in this lecture, let's go ahead and learn how to set up the user

2
00:00:05,790 --> 00:00:07,960
profile page, which is this page right here.

3
00:00:08,520 --> 00:00:14,580
So currently the user profile page only displays the user name of the logged in user.

4
00:00:14,580 --> 00:00:21,720
But let's say if you also want to add other fields like the profile image, the location of this particular

5
00:00:21,720 --> 00:00:22,140
user.

6
00:00:22,350 --> 00:00:24,050
So how exactly can we do that?

7
00:00:24,180 --> 00:00:29,520
So now, by default in the user model, which is the model which we're using to get this particular

8
00:00:29,520 --> 00:00:33,700
data, does not have the feel for adding a profile image.

9
00:00:33,900 --> 00:00:40,770
So what we need to do now is that we need to use the current user model and we need to extend that particular

10
00:00:40,770 --> 00:00:46,680
current user model and create another model and basically have some sort of relationship between those

11
00:00:46,680 --> 00:00:47,500
two models.

12
00:00:47,910 --> 00:00:53,970
So how exactly can we go ahead and do that, which means that we need to create a new model and extend

13
00:00:53,970 --> 00:00:56,010
from the current user model, which we have.

14
00:00:56,580 --> 00:00:57,740
So how exactly?

15
00:00:57,750 --> 00:01:00,300
And we go ahead and create a new user model.

16
00:01:00,690 --> 00:01:04,650
So we simply go ahead and do that by going into our users app.

17
00:01:04,860 --> 00:01:08,390
And in our app, we already have a file which is called as model.

18
00:01:09,300 --> 00:01:14,370
And in this particular model, Stoppie, we always have the user created models.

19
00:01:14,430 --> 00:01:18,160
So in brief, we are going to create our own user model right over here.

20
00:01:18,690 --> 00:01:24,300
So let's first go ahead and as we want to create a user model which extends the current model.

21
00:01:24,460 --> 00:01:25,320
So we'll go ahead.

22
00:01:25,320 --> 00:01:29,960
And first, and the existing model from Django dot country dot dot.

23
00:01:30,240 --> 00:01:34,770
So we'll type in from Shango dot contribute dot dot.

24
00:01:36,170 --> 00:01:37,460
DOT models.

25
00:01:38,860 --> 00:01:42,050
And we need to import the model, which is user.

26
00:01:42,400 --> 00:01:44,750
So once we have imported that, we are good to go.

27
00:01:45,370 --> 00:01:50,790
So now we need to create another model here, which is going to extend the current user model.

28
00:01:50,800 --> 00:01:57,910
So I'll just type in class and we will name this model as, let's say, profile, as this is going to

29
00:01:57,910 --> 00:01:59,140
be a user profile.

30
00:01:59,500 --> 00:02:01,110
So look at it this way.

31
00:02:01,600 --> 00:02:04,420
So we have a user model, which is an inbuilt model.

32
00:02:04,630 --> 00:02:10,810
And for each user, we want to create a profile, which means that every single user is going to have

33
00:02:10,810 --> 00:02:11,490
a profile.

34
00:02:12,070 --> 00:02:15,910
So user is going to be a model, profile is going to be a different model.

35
00:02:16,240 --> 00:02:21,640
And we are going to basically have a one to one relationship, meaning that one user is going to have

36
00:02:21,640 --> 00:02:25,960
one profile and one profile is going to be associated with one user.

37
00:02:26,590 --> 00:02:28,180
So now let's go ahead and do that.

38
00:02:28,780 --> 00:02:30,910
So this is going to be model start model.

39
00:02:32,350 --> 00:02:39,910
And now in order to associate yuzu with this particular model, dipen user equals, that's going to

40
00:02:39,910 --> 00:02:42,940
be model start one to one field.

41
00:02:43,450 --> 00:02:50,740
And here we are going to type in user, meaning that we are associating this particular model with the

42
00:02:50,740 --> 00:02:52,490
user model, which we have right here.

43
00:02:52,990 --> 00:02:54,700
So we'll type in user over here.

44
00:02:55,090 --> 00:02:59,550
And after that, we also need to mention the undelete parameter over here.

45
00:02:59,710 --> 00:03:04,270
So I'll typing undelete equals that's going to be model start.

46
00:03:05,800 --> 00:03:09,860
Cascade, now, what exactly is this thing right here?

47
00:03:10,570 --> 00:03:17,680
So that means on deleting the user, the profile of that user also needs to be deleted, which means

48
00:03:17,680 --> 00:03:22,890
that let's see if you are having a social networking website, you have created a user account in there.

49
00:03:23,230 --> 00:03:25,270
So they also give you your profile.

50
00:03:25,900 --> 00:03:31,090
So when you go ahead and delete your user account, it also needs to go ahead and delete your profile

51
00:03:31,090 --> 00:03:35,890
as well, because there's no point in having your profile page of the user account is deleted.

52
00:03:36,220 --> 00:03:42,280
So this line does the same exact thing that it's going to delete the profile if the user is going to

53
00:03:42,280 --> 00:03:43,030
be deleted.

54
00:03:43,870 --> 00:03:47,380
So now once we go ahead and do that, we are pretty much good to go.

55
00:03:47,920 --> 00:03:53,920
So now once we have associated this particular profile model with the user model, now we need to go

56
00:03:53,920 --> 00:03:56,750
ahead and add our very own fields over here.

57
00:03:57,220 --> 00:04:03,310
So let's see if I want to add the image, feel I can simply type an image equals that's going to be

58
00:04:03,820 --> 00:04:06,070
models dot image field.

59
00:04:06,400 --> 00:04:09,520
And in here we can specify a few parameters.

60
00:04:09,790 --> 00:04:15,940
So the first parameter is what's going to be a default image of this particular profile.

61
00:04:16,269 --> 00:04:21,279
So let's see if you go ahead and create this model and let's see if you don't assign any sort of an

62
00:04:21,279 --> 00:04:21,649
image.

63
00:04:21,670 --> 00:04:27,130
So in that case, it's actually going to need a default image, which is going to be present.

64
00:04:27,220 --> 00:04:30,430
So I'll type in default and the default image is going to be.

65
00:04:30,430 --> 00:04:31,120
Let's see.

66
00:04:32,340 --> 00:04:33,120
Profile.

67
00:04:34,270 --> 00:04:35,830
Big Dorce, big.

68
00:04:37,080 --> 00:04:43,770
And now we also need to mention the location to which particular directly this image needs to be uploaded.

69
00:04:44,010 --> 00:04:49,400
So this image needs to be uploaded to, let's say, a new directly, which is called let's profile pictures.

70
00:04:49,740 --> 00:04:56,370
So in order to mention that, you need to type an upload and let's go to that's going to be equal to

71
00:04:56,370 --> 00:04:57,620
the name of the directory.

72
00:04:58,020 --> 00:05:01,590
So let's name that directly as a profile underscore.

73
00:05:02,720 --> 00:05:09,440
Pictures, so once we go ahead and do that, let's also add another fellow here, so let's add a field

74
00:05:09,440 --> 00:05:14,270
which is called last location, which is going to be, let's see, model start characterful.

75
00:05:14,630 --> 00:05:19,480
And let's say the max length of this thing is going to be 100.

76
00:05:20,090 --> 00:05:24,580
So once we go ahead and once we create the profile model, we are almost good to go.

77
00:05:24,920 --> 00:05:30,770
But upon adding the image field right over here and if you go ahead and run the Serbo, you will get

78
00:05:30,770 --> 00:05:34,970
an error over here, which will see something like a pillow is not installed.

79
00:05:35,600 --> 00:05:41,660
So whenever you're using image field image fillers, not by default present and Django and for using

80
00:05:41,660 --> 00:05:45,730
an image feel, you always need to install something which is called as pillow.

81
00:05:46,220 --> 00:05:50,690
So in order to install pillow, you simply need to type in BIP.

82
00:05:51,580 --> 00:05:52,360
Install.

83
00:05:53,650 --> 00:05:57,020
Below, and that's actually going to install below for you.

84
00:05:57,040 --> 00:06:01,180
So when I go ahead and hit enter, it's going to install pillow for me.

85
00:06:01,360 --> 00:06:07,060
And right now, as you can see, it has actually denied installing pillow because I am not currently

86
00:06:07,060 --> 00:06:08,550
logged in as a super user.

87
00:06:09,010 --> 00:06:14,170
So in order to resolve this issue, if you're on Mac or Linux, you need to type in pseudo.

88
00:06:15,380 --> 00:06:15,830
Beppe.

89
00:06:16,560 --> 00:06:17,400
Install.

90
00:06:18,970 --> 00:06:24,760
Below and then it's going to ask you for your password, and when I go ahead and enter my password,

91
00:06:24,760 --> 00:06:26,950
it's going to start installing pillow for me.

92
00:06:27,430 --> 00:06:33,820
Now, if you're on windows and if you get the permission error while installing below, what you need

93
00:06:33,820 --> 00:06:38,430
to do is that you need to go ahead and open up your command prompt as administrator.

94
00:06:38,560 --> 00:06:41,770
So simply go ahead typing calmly on your search bar.

95
00:06:42,020 --> 00:06:42,400
Right.

96
00:06:42,400 --> 00:06:45,380
Click on CMD and click on Run as administrator.

97
00:06:45,580 --> 00:06:51,370
So when you go ahead and run CMD as an administrator, you won't have any permission issues while installing

98
00:06:51,370 --> 00:06:51,740
pillow.

99
00:06:51,940 --> 00:06:56,840
So once below is installed, we can now go ahead and work on the rest of the model.

100
00:06:57,460 --> 00:07:03,670
So once the fields in the models are completed now let's go ahead and add the string representation

101
00:07:03,670 --> 00:07:04,350
of this model.

102
00:07:04,720 --> 00:07:08,290
So in order to do that, we Dipen Def Asgeir.

103
00:07:10,210 --> 00:07:17,140
Pass self over here, and this is nothing, but this is what we always do when we want to access objects

104
00:07:17,140 --> 00:07:21,800
in that model, we just go ahead and define the representation of that particular model.

105
00:07:22,210 --> 00:07:28,300
So that means whenever we are trying to access any object from this model, we should get the user name.

106
00:07:28,330 --> 00:07:32,620
So we'll type in self-taught user dot.

107
00:07:34,750 --> 00:07:39,390
Username, so once we go ahead and do that, we are pretty much good to go.

108
00:07:39,420 --> 00:07:44,790
So now once this thing is done, once we have all those fields, once we have the method over here,

109
00:07:44,790 --> 00:07:46,030
we almost good to go.

110
00:07:46,050 --> 00:07:48,420
So this means our model is actually ready.

111
00:07:48,690 --> 00:07:55,200
And now whenever you go ahead and create a new model, what we always do is that we go ahead and make

112
00:07:55,200 --> 00:07:56,240
the migration's.

113
00:07:56,250 --> 00:08:00,330
So you need to type in Python managed by.

114
00:08:02,200 --> 00:08:07,960
Make migration's so when I go ahead and enter, it's going to create the model, which is profile.

115
00:08:08,590 --> 00:08:10,150
Now, again, you need to type in.

116
00:08:12,530 --> 00:08:13,250
Python.

117
00:08:14,290 --> 00:08:22,240
Managed by Margaret, so when I go ahead and enter, it's actually going to migrate and now we will

118
00:08:22,240 --> 00:08:24,460
have the table at the back end.

119
00:08:24,470 --> 00:08:31,720
And now in order to check if the profile model is actually added, we can simply go ahead, fire up

120
00:08:31,720 --> 00:08:32,260
ourselves.

121
00:08:32,500 --> 00:08:36,159
So let me just type in python managed by.

122
00:08:37,549 --> 00:08:38,760
Ron Silver.

123
00:08:39,230 --> 00:08:47,960
And now let's go to the admin side log-in as admin and as you can see currently we do not have the model

124
00:08:47,960 --> 00:08:48,500
over here.

125
00:08:48,860 --> 00:08:54,830
So in order to actually add the model to the admin side, we always need to go ahead, go to the admin

126
00:08:54,830 --> 00:08:57,380
side, and we need to register that model over here.

127
00:08:57,890 --> 00:09:00,320
So first of all, we will import the profile model.

128
00:09:00,320 --> 00:09:04,130
So we will type in from not models import.

129
00:09:05,060 --> 00:09:12,520
Profile and in order to register, the model will type in Admon dot site, dot register.

130
00:09:14,290 --> 00:09:17,250
And then simply passing profile over here.

131
00:09:18,210 --> 00:09:23,820
So once we go ahead, do that, save the code and once we go back over here and hit refresh, as you

132
00:09:23,820 --> 00:09:27,150
can see now, we have user profile model right over here.

133
00:09:27,180 --> 00:09:33,350
So now once we have this user profile model, we can now go ahead and actually add the user profiles

134
00:09:33,360 --> 00:09:33,880
over here.

135
00:09:33,900 --> 00:09:39,090
So currently, now, if you go ahead and go to the user profiles, as you can see, there are no currently

136
00:09:39,090 --> 00:09:40,870
existing user profiles right here.

137
00:09:41,340 --> 00:09:46,860
So in the next lecture will go ahead and we will learn how to exactly work with this particular user

138
00:09:46,860 --> 00:09:47,670
profile model.

139
00:09:48,000 --> 00:09:49,860
So thank you very much for watching.

140
00:09:49,860 --> 00:09:51,900
And I'll see you guys next time.

141
00:09:52,170 --> 00:09:52,740
Thank you.

