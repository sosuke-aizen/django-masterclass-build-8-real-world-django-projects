1
00:00:00,240 --> 00:00:05,400
So once we have actually designed the login functionality, let's now go ahead and design the logout

2
00:00:05,400 --> 00:00:06,710
functionality as well.

3
00:00:06,720 --> 00:00:12,360
So for the login functionality, what we have done is we have actually created a template called us

4
00:00:12,360 --> 00:00:12,850
Login.

5
00:00:12,870 --> 00:00:17,230
We have also created a form as well, which is this particular login form right here.

6
00:00:17,250 --> 00:00:22,860
And we have also created a complex view right here which actually renders out that particular template

7
00:00:22,860 --> 00:00:24,480
and logs the user in.

8
00:00:24,570 --> 00:00:30,660
However, there's one more easier way to actually process log in and log out actions, and that is by

9
00:00:30,660 --> 00:00:33,550
using the built in authentication views in Django.

10
00:00:33,570 --> 00:00:40,620
So Django actually has some in-built views, like the authentication views for logging in and logging

11
00:00:40,620 --> 00:00:41,420
out the user.

12
00:00:41,430 --> 00:00:47,380
So if you use those particular views, you don't have to write these complex views which we have written.

13
00:00:47,400 --> 00:00:52,830
So now in order to use this views, first of all, you need to understand that you don't have to create

14
00:00:52,830 --> 00:00:54,990
a view just as we have created here.

15
00:00:54,990 --> 00:00:59,180
So our aim is to go ahead and create a log out functionality.

16
00:00:59,190 --> 00:01:04,650
So in order to implement that functionality, as you don't need to create a view, you directly have

17
00:01:04,650 --> 00:01:09,330
to go ahead and associate that authentication view with a URL pattern.

18
00:01:09,330 --> 00:01:14,700
So we'll go to the URL start PY file here and here I'll create a view for logout.

19
00:01:14,700 --> 00:01:20,730
So here I'll say path log out forward, slash.

20
00:01:20,730 --> 00:01:27,090
And then now here we have used the view which we have created, but now we are going to make use of

21
00:01:27,090 --> 00:01:31,620
a built in view from a particular set of Django Library.

22
00:01:31,620 --> 00:01:40,740
So here I would import it from from Django dot contrib dot auth import views.

23
00:01:40,830 --> 00:01:47,190
So the logout view which we want is actually present in this particular library path right here.

24
00:01:47,370 --> 00:01:53,970
And then as we have imported views over here as well as over here, Django will get confused like which

25
00:01:53,970 --> 00:01:55,860
view is this and which one is this one?

26
00:01:55,860 --> 00:02:01,320
Therefore we are going to import this particular view as authentication views, so I'll import this

27
00:02:01,320 --> 00:02:04,890
particular views as auth view or authentication view.

28
00:02:04,890 --> 00:02:11,160
So I would say auth underscore view and I would simply add import this as auth view.

29
00:02:11,400 --> 00:02:16,980
So that means whenever we see on view, Django understands that we are referring to these views which

30
00:02:16,980 --> 00:02:19,740
we have in Django dot dot off.

31
00:02:20,010 --> 00:02:24,990
So once we have this particular URL, let's associate that particular auth view here.

32
00:02:25,320 --> 00:02:32,760
So here we would say auth view taught and this art view actually contains the login view, the log out

33
00:02:32,760 --> 00:02:33,060
view.

34
00:02:33,060 --> 00:02:35,840
It also contains a whole bunch of other views as well.

35
00:02:35,850 --> 00:02:38,120
As you can see there are other views as well.

36
00:02:38,130 --> 00:02:41,930
But what we are interested here is the log out view.

37
00:02:41,940 --> 00:02:47,970
So let's say log out view and as this is not a regular view, as this is a in-built view, we have to

38
00:02:47,970 --> 00:02:50,730
say dot as underscore view.

39
00:02:51,390 --> 00:02:54,570
Once we go ahead and do that, we are pretty much good to go.

40
00:02:54,970 --> 00:02:57,390
Just make sure you have a comma at the end.

41
00:02:57,390 --> 00:02:57,720
Okay.

42
00:02:57,720 --> 00:03:03,330
So now, once this particular view is set up with this URL pattern, let's see if that works.

43
00:03:03,420 --> 00:03:05,370
So right now, I'll go ahead.

44
00:03:05,790 --> 00:03:09,120
And as you can see, let's log in.

45
00:03:09,120 --> 00:03:13,560
So I will log in as a super user hit submit.

46
00:03:13,560 --> 00:03:15,780
And now it sees we are currently logged in.

47
00:03:16,080 --> 00:03:22,380
Now, if I have to log out in that particular case, I would simply go ahead and say forward, slash

48
00:03:22,380 --> 00:03:24,330
users, forward, slash log out.

49
00:03:24,750 --> 00:03:30,930
So if I type this thing in, as you can see, we get this particular template right here from Django

50
00:03:30,930 --> 00:03:34,680
administration, which sees that you have been logged out.

51
00:03:34,680 --> 00:03:39,090
So it says that thanks for spending some quality time with the website today.

52
00:03:39,180 --> 00:03:44,430
And we get this particular view because this is a inbuilt view which is being rendered.

53
00:03:44,460 --> 00:03:49,230
However, let's say instead of rendering this particular template right here, which is the inbuilt

54
00:03:49,230 --> 00:03:54,690
template, let's say we want to create our own log out page which is going to be displayed when the

55
00:03:54,690 --> 00:03:55,920
user is logged out.

56
00:03:56,040 --> 00:04:02,670
So in order to create that, I'll go back to the VTS code and as usual, as we create any template,

57
00:04:02,670 --> 00:04:10,380
we'll go ahead, go to templates, users, create a new template and name it as log out dot HTML.

58
00:04:11,100 --> 00:04:14,340
And in here let's simply create ah1 tag.

59
00:04:14,340 --> 00:04:17,220
Let's keep this simple for now and let's see.

60
00:04:17,880 --> 00:04:21,420
You have been logged out.

61
00:04:22,530 --> 00:04:28,890
Okay, so once we have this now we have to render this instead of this by default template right here.

62
00:04:28,890 --> 00:04:35,190
And for that we will go back to the URL start poi file because we are directly rendering that view over

63
00:04:35,190 --> 00:04:41,100
here and here in this particular as view, you could type in the name of the template which you want.

64
00:04:41,100 --> 00:04:44,760
So here I will mention a parameter which is template name.

65
00:04:44,760 --> 00:04:47,190
So I would say template underscore name.

66
00:04:47,550 --> 00:04:52,560
And here I would say I want to now use the template which is present in the user's app.

67
00:04:52,560 --> 00:04:59,400
And the name of that template is log out dot HTML.

68
00:04:59,980 --> 00:05:01,210
Once this thing is done.

69
00:05:01,210 --> 00:05:03,550
I could also associate a name to this.

70
00:05:03,550 --> 00:05:07,510
So let's say I want to call this particular URL pattern as logout.

71
00:05:08,320 --> 00:05:12,250
Okay, so once this thing is done, let's see how it works now.

72
00:05:12,460 --> 00:05:14,710
So here, let's log in again.

73
00:05:15,460 --> 00:05:18,040
So when I log in, it sees your logged in.

74
00:05:18,070 --> 00:05:19,660
Now let's try logging out.

75
00:05:19,660 --> 00:05:24,730
And when I log out this time, as you can see, it sees you have been locked out.

76
00:05:25,120 --> 00:05:30,330
So that means now the log out functionality is working absolutely fine.

77
00:05:30,340 --> 00:05:36,310
And now in the upcoming lecture, we are going to protect a particular view with the login functionality

78
00:05:36,310 --> 00:05:37,730
which we have just created.

79
00:05:37,750 --> 00:05:40,150
So let's learn how to do that in the next one.

