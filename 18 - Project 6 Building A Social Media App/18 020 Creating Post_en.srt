1
00:00:00,180 --> 00:00:06,100
Let's now work on making a form on our website so that users could actually submit a post.

2
00:00:06,120 --> 00:00:11,310
So in order to do that, let's go back to VTS code and in there inside the Post app, we now need to

3
00:00:11,310 --> 00:00:13,770
create a new file which is forms dot p y.

4
00:00:13,980 --> 00:00:15,690
So let's create a new file.

5
00:00:15,720 --> 00:00:18,360
Let's call it us forms dot pie.

6
00:00:18,990 --> 00:00:23,090
And in here let's create a form for the post model.

7
00:00:23,100 --> 00:00:27,600
So this is going to be a model form so we don't have to create the form manually.

8
00:00:27,600 --> 00:00:30,450
We could just specify the model and the fields which we want.

9
00:00:30,480 --> 00:00:37,440
So here I would say from DOT models, and I want to import the post model and now let's also import

10
00:00:37,440 --> 00:00:38,580
forms from Django.

11
00:00:38,580 --> 00:00:44,960
So from Django import forms, and now let's create a form.

12
00:00:44,970 --> 00:00:49,740
So here I would create a class which is going to be the class for creating post.

13
00:00:49,740 --> 00:00:52,950
So I will call it as post create form.

14
00:00:53,340 --> 00:01:00,330
This needs to inherit from forms dot model form because we are working on a model and then we simply

15
00:01:00,330 --> 00:01:02,580
need to provide some meta information.

16
00:01:02,580 --> 00:01:04,700
So here I would say class meta.

17
00:01:04,709 --> 00:01:09,990
The model which we are working with is the post model which we have imported above, and the fields

18
00:01:09,990 --> 00:01:14,420
which this form should contain is the title, the caption and the image.

19
00:01:14,430 --> 00:01:18,480
So here I would say title, which is one of the fields which we have here.

20
00:01:18,660 --> 00:01:21,300
So if you take a look at this, we have title.

21
00:01:21,750 --> 00:01:25,020
We also want the image and we want the caption.

22
00:01:25,020 --> 00:01:32,810
So title image and then caption Once this thing is done, we are pretty much good to go.

23
00:01:32,820 --> 00:01:36,330
So now once this form is created, we have to render out this form.

24
00:01:36,420 --> 00:01:41,550
That means this form needs to be passed to a view, and then that view is going to render a template

25
00:01:41,550 --> 00:01:44,960
and pass this form as the context to that particular template.

26
00:01:44,970 --> 00:01:47,370
So let's work on creating a view now.

27
00:01:47,370 --> 00:01:50,900
So we have to go to the view start py file for the post.

28
00:01:50,910 --> 00:01:55,230
So let's go here and first of all you need to import a couple of things here.

29
00:01:55,530 --> 00:02:00,930
So first of all I have to import the form which we have created so that form is present and forms.

30
00:02:00,930 --> 00:02:10,050
So I would say from dot forms import the post, create form and now let's create a view here.

31
00:02:10,050 --> 00:02:16,950
So I'll call this view as create, underscore, post or let's call this thing as post, underscore,

32
00:02:16,950 --> 00:02:17,700
create.

33
00:02:17,700 --> 00:02:20,430
This is going to accept a request as usual.

34
00:02:20,610 --> 00:02:24,480
And now first we handle the post method or the post request.

35
00:02:24,480 --> 00:02:32,700
So here we say if request dot method is equal equal to post and that particular case, we want to get

36
00:02:32,700 --> 00:02:35,640
the form data and actually create a post.

37
00:02:35,910 --> 00:02:38,100
So here let's get the form data.

38
00:02:38,100 --> 00:02:46,170
So in order to do that, I would say form equals post, create form and get the data by saying data

39
00:02:46,770 --> 00:02:49,610
equals request dot post.

40
00:02:49,620 --> 00:02:54,660
This will get the posted data from the form and we now check the validity of the form data.

41
00:02:54,660 --> 00:03:01,320
So we see if form DOT is underscore valid in that particular case, get the data.

42
00:03:01,320 --> 00:03:04,590
And once we have gotten the data, we simply saved that particular data.

43
00:03:04,590 --> 00:03:10,230
So now if you directly save the form, so if you say form dot save, and what would happen is that it

44
00:03:10,230 --> 00:03:14,400
will actually save the form, but the user field won't be added up over here.

45
00:03:14,730 --> 00:03:20,040
And hence in order to resolve that particular issue, what we first do is that instead of saving the

46
00:03:20,040 --> 00:03:25,680
form directly, we say form dot, save and set the commit to false.

47
00:03:25,680 --> 00:03:32,850
So we won't commit the data to the database as of now and we are going to save this thing in new item.

48
00:03:32,850 --> 00:03:35,070
So I would say new item equals this.

49
00:03:35,400 --> 00:03:40,770
And now I would set the user field for this particular new item as the logged in user.

50
00:03:40,770 --> 00:03:47,340
So I would say new underscore item dot user, which means that the user field for this one is going

51
00:03:47,340 --> 00:03:49,500
to be set to the currently logged in user.

52
00:03:49,710 --> 00:03:55,290
And in order to get the currently logged in user, I simply have to access the request object and I

53
00:03:55,290 --> 00:03:57,120
want to get the user from there.

54
00:03:57,210 --> 00:03:59,940
So this gives me the currently logged in use object.

55
00:03:59,970 --> 00:04:04,020
Now that's being saved here and now we have to save the new item.

56
00:04:04,020 --> 00:04:07,170
So I would say new underscore item dot save.

57
00:04:07,980 --> 00:04:10,880
Once this thing is done, we are pretty much good to go.

58
00:04:10,890 --> 00:04:13,920
Now we have to write an an ls part for this.

59
00:04:13,920 --> 00:04:17,190
So this ls part is actually going to handle the get request.

60
00:04:17,459 --> 00:04:23,190
So whenever we want to handle the get request, we say form equals this and the request data is going

61
00:04:23,190 --> 00:04:23,910
to be get.

62
00:04:23,910 --> 00:04:32,250
So here I'll get this and this is going to be request dot yet and then we have to return and render

63
00:04:32,250 --> 00:04:32,940
a template.

64
00:04:32,940 --> 00:04:37,470
So return render pass in the request object.

65
00:04:37,470 --> 00:04:43,350
And then let's say we want to create a template called last create dot HTML for creating the post.

66
00:04:43,470 --> 00:04:47,790
And that template is obviously going to be present in the post app.

67
00:04:47,790 --> 00:04:52,320
So here I would say post forward slash create dot HTML.

68
00:04:52,410 --> 00:04:58,230
So we have not created that template as of now, but we are going to create that very soon and then

69
00:04:58,230 --> 00:04:58,710
we want to.

70
00:04:58,850 --> 00:05:04,580
US in this form to that particular template so that the form would be rendered in that template.

71
00:05:04,580 --> 00:05:07,040
So let's pass in the form as context.

72
00:05:07,220 --> 00:05:10,370
So here I would say pass in form as form.

73
00:05:11,270 --> 00:05:13,880
Once that thing is done, we are good to go.

74
00:05:13,880 --> 00:05:19,700
So in the upcoming lecture, let's go ahead and let's create a template in the post snap.

