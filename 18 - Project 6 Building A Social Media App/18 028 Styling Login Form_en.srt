1
00:00:00,120 --> 00:00:04,270
In this lecture, let's learn how to design the login form which we have up over here.

2
00:00:04,290 --> 00:00:09,740
So right away you will be able to see that the tail when styling isn't applied to this login form.

3
00:00:09,750 --> 00:00:15,330
And that's because if you actually go to the page, which is login dot HTML, you'll be able to see

4
00:00:15,330 --> 00:00:18,780
that that particular page does not extend from the base template.

5
00:00:18,900 --> 00:00:24,120
So first of all, the first and foremost thing which needs to be done is that you actually need to go

6
00:00:24,120 --> 00:00:26,050
ahead and extend from the base template.

7
00:00:26,070 --> 00:00:27,820
So let's do that right away.

8
00:00:27,840 --> 00:00:33,560
So we'll go ahead and instead of directly having the form, I would have the extend stack.

9
00:00:33,570 --> 00:00:39,660
So I would say extends that's going to be from user's forward slash based on HTML.

10
00:00:39,870 --> 00:00:44,670
So whenever we extend from this particular template, you need to have the block body and end block

11
00:00:44,670 --> 00:00:48,930
tag as well, because that is the tag which we have included in the base template.

12
00:00:49,020 --> 00:00:53,340
So here I would say block body.

13
00:00:53,340 --> 00:00:56,370
And then also let's have the end block tag as well.

14
00:00:56,670 --> 00:00:59,070
So here I would say and.

15
00:00:59,430 --> 00:01:01,290
BLOCK okay.

16
00:01:01,290 --> 00:01:02,670
So once we have this.

17
00:01:02,700 --> 00:01:06,900
Now, if I go back to that page and hit refresh right away, you'll be able to see that.

18
00:01:06,900 --> 00:01:12,330
Now we have the photo app navigation bar added up over here, along with the different links, which

19
00:01:12,330 --> 00:01:13,010
we have.

20
00:01:13,020 --> 00:01:18,630
But you'll also notice that as soon as we apply the tailwind styling to this particular form, this

21
00:01:18,630 --> 00:01:23,460
form kind of fades away and we no longer actually have the input fields clearly visible over here.

22
00:01:23,550 --> 00:01:29,720
So in order to now style this thing up, let's head back over here to Vsco.

23
00:01:29,730 --> 00:01:36,510
And first of all, let's go ahead and let's style a heading or add a heading to the top even before

24
00:01:36,510 --> 00:01:38,540
we actually start creating a form.

25
00:01:38,550 --> 00:01:45,930
So here I would add a normal H one heading and this heading is going to say something like login user.

26
00:01:46,200 --> 00:01:49,080
We could also use an emoji over here if we want to.

27
00:01:49,110 --> 00:01:54,060
So as we are logging in a user, I could use an emoji of a person.

28
00:01:54,060 --> 00:01:58,890
So I'll search for person and I'll add a person here.

29
00:01:59,730 --> 00:02:03,880
And now if I go back, this is what it's going to look like.

30
00:02:03,900 --> 00:02:09,690
Now, this doesn't all look like a heading, so let's make it look like a heading by using Telvin.

31
00:02:09,690 --> 00:02:12,140
So let's make the font a little bit bigger.

32
00:02:12,150 --> 00:02:17,640
So here to this heading, I would add a class and let's make the text larger.

33
00:02:17,640 --> 00:02:24,360
So I would say text Dash two times Excel, which means that it's twice the extra large size which is

34
00:02:24,360 --> 00:02:25,610
available in tailwind.

35
00:02:25,620 --> 00:02:31,410
And if you want to see how this thing or how I have applied this class, you could simply go to Tailwind

36
00:02:31,410 --> 00:02:36,440
and you could search for font and you'll be able to see what kind of classes you need to apply.

37
00:02:36,450 --> 00:02:38,880
So as you can see, this is what the heading looks like.

38
00:02:39,030 --> 00:02:43,490
But you'll also notice that this does not have any kind of margin from either side.

39
00:02:43,500 --> 00:02:48,090
So let's add a margin from all the sides and let's say I want to add margin ten.

40
00:02:48,360 --> 00:02:49,890
I could add that hit refresh.

41
00:02:49,890 --> 00:02:54,210
And as you can see now, we have decent amount of margin from all the sides.

42
00:02:55,650 --> 00:02:56,940
Now after this.

43
00:02:57,180 --> 00:03:00,060
Let's go ahead and let's start designing the login page.

44
00:03:00,090 --> 00:03:06,030
So as far as the login page is concerned or the login form is concerned, we have to go ahead and we

45
00:03:06,030 --> 00:03:08,310
have to create a div container for that form.

46
00:03:08,310 --> 00:03:09,750
So I'll create a div.

47
00:03:10,080 --> 00:03:13,280
This is going to be the starting tag ending tag.

48
00:03:13,290 --> 00:03:19,080
Let's take the entire form, put up the entire form in this particular div except for the paragraph

49
00:03:19,170 --> 00:03:19,500
here.

50
00:03:19,530 --> 00:03:27,060
And for this div we are going to give it a class container and we want a padding of five on the y axis.

51
00:03:27,060 --> 00:03:29,730
So I would say py-5 here.

52
00:03:30,570 --> 00:03:36,540
If I go back now it has a padding of three from the top and now we could style up the form itself by

53
00:03:36,540 --> 00:03:38,130
adding different classes to it.

54
00:03:38,130 --> 00:03:42,090
So after the form method post or even before that, you could add a class.

55
00:03:42,240 --> 00:03:45,510
And let's say I want the background of this form to be white.

56
00:03:45,510 --> 00:03:47,310
So I would say BG White.

57
00:03:47,610 --> 00:03:51,660
Let's say I want this form to have a shadow, which is going to be a medium shadow.

58
00:03:51,660 --> 00:03:53,070
So I would say shadow.

59
00:03:53,100 --> 00:03:57,300
MD Let's say I want the margin to be ten from all the sides.

60
00:03:57,600 --> 00:04:01,920
Let's say I want the width to be 2/5 of the entire available space.

61
00:04:01,920 --> 00:04:04,650
So I would say w-2 by five.

62
00:04:04,650 --> 00:04:08,730
And let's say if I want the form to be rounded, I could add that as well.

63
00:04:09,000 --> 00:04:14,280
I want the padding on the x axis as eight padding on the top as six.

64
00:04:14,460 --> 00:04:18,720
Now all of these things are predefined and you don't have to do anything about them.

65
00:04:18,899 --> 00:04:26,250
And let's also add a padding at the bottom to be eight and then margin at the bottom to be, let's say

66
00:04:26,250 --> 00:04:26,760
four.

67
00:04:27,120 --> 00:04:29,140
As soon as I add all of these.

68
00:04:29,160 --> 00:04:30,770
Now let's see the magic.

69
00:04:30,780 --> 00:04:35,280
So if I had to refresh, as you can see now, this is what your form is going to look like.

70
00:04:35,430 --> 00:04:41,400
So now it appears in a card like format with all of these particular stylings applied to it.

71
00:04:42,300 --> 00:04:47,970
Now the form is actually styled up, but the fields or the form fields still look pretty dull.

72
00:04:47,970 --> 00:04:50,010
So let's style them up as well.

73
00:04:50,040 --> 00:04:56,250
So in order to style them up right up over here, instead of rendering the entire form all at once,

74
00:04:56,250 --> 00:04:59,400
let's say we want to render the form field by field.

75
00:04:59,430 --> 00:05:03,540
So the two fields which we need to work with here are the username and password.

76
00:05:03,690 --> 00:05:08,280
So instead of saying form ASP, I could say form dot username.

77
00:05:08,610 --> 00:05:09,900
And I could say.

78
00:05:11,470 --> 00:05:13,510
Form dot password.

79
00:05:13,690 --> 00:05:16,790
If I do that, it's going to give me one and the same result.

80
00:05:16,810 --> 00:05:21,430
So we have these two fields, but they are not currently visible because we have not styled them.

81
00:05:21,520 --> 00:05:27,310
And you have to now manually add the labels because you'll be able to see that the labels have now disappeared.

82
00:05:27,550 --> 00:05:29,560
So let's go ahead and style them up.

83
00:05:29,650 --> 00:05:35,310
So first of all, let's create a div for containing the label as well as the actual field.

84
00:05:35,320 --> 00:05:43,630
So first of all, I'll add a label and this label is going to be something called as username as this

85
00:05:43,630 --> 00:05:44,950
label is for username.

86
00:05:44,980 --> 00:05:50,080
Then I would create another div after this label and this is going to be for containing the username

87
00:05:50,080 --> 00:05:50,500
input.

88
00:05:50,500 --> 00:05:56,350
So div and I'll take the username input from here and paste it right inside of it.

89
00:05:56,350 --> 00:05:58,540
And now let's apply classes to this.

90
00:05:58,540 --> 00:06:01,390
So I would say class, let's see.

91
00:06:01,870 --> 00:06:06,160
BLOCK So this basically means that the type of this particular element is going to be.

92
00:06:06,160 --> 00:06:13,940
BLOCK I want the text inside of this to be gray with the shade of 700.

93
00:06:14,050 --> 00:06:17,140
I want the text inside this to be smaller.

94
00:06:17,170 --> 00:06:19,420
I want the font of this thing to be.

95
00:06:20,220 --> 00:06:23,310
Ball and I want the margin bottom to be too.

96
00:06:23,850 --> 00:06:29,550
So now if I had refresh, as you can see, it now sees username and now let's style up this particular

97
00:06:29,550 --> 00:06:32,840
input field, which currently is looking up to BMT.

98
00:06:32,850 --> 00:06:34,140
So I'll add a class.

99
00:06:34,140 --> 00:06:36,120
I will add a shadow to this.

100
00:06:36,120 --> 00:06:39,570
I would say appearance as none.

101
00:06:39,660 --> 00:06:46,860
I would add a border, I would make the border rounded and then I want the padding on the y axis to

102
00:06:46,860 --> 00:06:51,690
be to padding on the x axis to be three margin on the right to be ten.

103
00:06:52,620 --> 00:06:57,330
If I go back here to refresh now, as you can see, we have a nice looking input field right up over

104
00:06:57,330 --> 00:06:57,720
here.

105
00:06:57,720 --> 00:07:02,760
And let's say if I want to make the text of this thing to be of the same color as this, I simply could

106
00:07:02,760 --> 00:07:06,180
copy the class from here and paste it up over here.

107
00:07:06,210 --> 00:07:11,310
So now if I had refresh, if I type any text here that's going to have the same color as this one.

108
00:07:11,550 --> 00:07:16,260
Now, just as I have styled this particular label, I could do the same thing with the form password.

109
00:07:16,260 --> 00:07:23,850
So I'll simply copy this, paste it up over here, can deform password from here or simply change this

110
00:07:23,850 --> 00:07:25,380
thing to password.

111
00:07:26,590 --> 00:07:29,200
Let's say the label is password.

112
00:07:29,500 --> 00:07:31,140
If I go back, hit refresh.

113
00:07:31,150 --> 00:07:35,620
As you can see, we have username and password, but right now you'll be able to see that there's no

114
00:07:35,620 --> 00:07:37,080
margin at the bottom.

115
00:07:37,090 --> 00:07:39,790
So let's add margin bottom to both of these.

116
00:07:39,790 --> 00:07:46,120
So here for this live, I will add a class saying margin bottom is two.

117
00:07:46,750 --> 00:07:49,900
Let's do the same thing with the other, which we have.

118
00:07:49,930 --> 00:07:51,460
So I'll copy this.

119
00:07:52,240 --> 00:07:54,310
Paste it up over here as well.

120
00:07:54,880 --> 00:07:55,450
Okay.

121
00:07:56,170 --> 00:08:00,970
If I go back now, as you can see, it has a margin at the top as well as in the bottom.

122
00:08:01,270 --> 00:08:06,430
Now, let's tile up this submit button, because currently it looks pretty dull and boring.

123
00:08:06,670 --> 00:08:11,890
So in order to style the submit button, I would go right up over here and instead of saying input,

124
00:08:11,890 --> 00:08:15,100
I would actually change this thing to button.

125
00:08:15,970 --> 00:08:17,800
Type as submit.

126
00:08:19,050 --> 00:08:25,860
And then let's name this button as login and let's say the color of this button.

127
00:08:25,860 --> 00:08:31,020
I want the color to be white, which is the text color of the button to be white.

128
00:08:31,110 --> 00:08:35,460
So I would add a class, which is text dash white.

129
00:08:35,789 --> 00:08:39,870
And let's say I want the background color of this thing to be green.

130
00:08:39,870 --> 00:08:43,900
So BG, Dash, green, let's say with a shade of 500.

131
00:08:43,919 --> 00:08:46,680
So I don't want to be a darker green, but lighter green.

132
00:08:46,950 --> 00:08:51,810
So if I add that now this is what the button looks like, so it has no padding here.

133
00:08:52,050 --> 00:08:59,700
So in order to add a padding, I would say I want the padding on the x axis to be, let's say five.

134
00:08:59,820 --> 00:09:02,310
And the padding on the Y should be two.

135
00:09:03,060 --> 00:09:04,260
If that is done.

136
00:09:04,440 --> 00:09:06,450
This is what the login button looks like.

137
00:09:06,450 --> 00:09:08,250
And I think that looks pretty neat.

138
00:09:08,790 --> 00:09:13,980
Now, similar to this, let's also go ahead and let's also style up this text which we have, which

139
00:09:13,980 --> 00:09:15,030
is forgot password.

140
00:09:15,030 --> 00:09:16,200
Click here to reset.

141
00:09:16,800 --> 00:09:21,010
So in order to style this thing up, we have to style up this paragraph, which we have.

142
00:09:21,030 --> 00:09:23,550
So first of all, let's add a padding x to this.

143
00:09:23,550 --> 00:09:25,350
So padding x is going to be ten.

144
00:09:25,350 --> 00:09:27,630
Let's say padding y is also going to be ten.

145
00:09:27,630 --> 00:09:31,140
And we already have this text over here which says forgot password.

146
00:09:31,140 --> 00:09:34,490
And here we could also add some class to this ref as well.

147
00:09:34,500 --> 00:09:37,260
So let's say I want to make the text of a different color.

148
00:09:37,260 --> 00:09:42,360
So text, let's say this is going to be indigo with a shade of 500.

149
00:09:43,620 --> 00:09:44,430
I could see this.

150
00:09:44,430 --> 00:09:45,300
Go back here.

151
00:09:46,320 --> 00:09:49,320
As you can see, this is what it actually looks like.

152
00:09:49,320 --> 00:09:54,660
So you could customize this particular page as per your own needs if you want that certain elements

153
00:09:54,690 --> 00:09:56,820
need a certain amount of E property.

154
00:09:56,820 --> 00:10:02,130
So let's say, for example, if you think that this login button needs to have padding at the top,

155
00:10:02,130 --> 00:10:03,960
you could simply go ahead and do that.

156
00:10:03,960 --> 00:10:10,230
So for example, you could say something like, I want the margin on the top or on the Y axis to be,

157
00:10:10,230 --> 00:10:11,330
let's say ten.

158
00:10:11,340 --> 00:10:12,540
If I do that.

159
00:10:12,720 --> 00:10:15,720
Now, as you can see, that margin is added right up over here.

160
00:10:15,720 --> 00:10:20,130
So if you want to reduce it, you could do that as well by changing it to five.

161
00:10:20,460 --> 00:10:25,800
So as you can see, we have styled the login page and in the next lecture, let's go ahead and let's

162
00:10:25,800 --> 00:10:29,460
style up the registration page because it still looks pretty dull and boring.

163
00:10:29,460 --> 00:10:30,930
So if I go to register.

164
00:10:32,950 --> 00:10:35,760
As you can see, this is what the page currently looks like.

165
00:10:35,770 --> 00:10:40,060
So in the next lecture, let's style up the registration page as per our own needs.

166
00:10:40,060 --> 00:10:43,750
So thank you very much for watching and I'll see you guys in the next one.

167
00:10:44,020 --> 00:10:44,680
Thank you.

