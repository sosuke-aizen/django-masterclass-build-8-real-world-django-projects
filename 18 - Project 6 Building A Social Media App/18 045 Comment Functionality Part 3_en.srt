1
00:00:00,090 --> 00:00:06,480
So in this particular lecture, let's go ahead and let's actually create a form over here for each post,

2
00:00:06,480 --> 00:00:09,620
which we have so that a comment could be submitted.

3
00:00:09,630 --> 00:00:14,880
So right now we are actually able to submit a comment, but we are actually doing it via the admin panel.

4
00:00:14,880 --> 00:00:19,740
But we should provide the users a functionality so that they could add their own comments.

5
00:00:19,740 --> 00:00:21,450
So doing that is quite simple.

6
00:00:21,450 --> 00:00:26,430
So let's say you want to add a form to add a comment right beneath the list of the comments which you

7
00:00:26,430 --> 00:00:30,230
have, you could simply go ahead and do that in the feed dot HTML.

8
00:00:30,240 --> 00:00:35,520
So let's go inside the feed dot HTML page and let's go right after the div.

9
00:00:35,610 --> 00:00:37,410
The comment section actually ends.

10
00:00:37,410 --> 00:00:39,390
So this is the div it ends.

11
00:00:39,390 --> 00:00:43,560
So I'll create another view and let's add a padding and margin here.

12
00:00:43,560 --> 00:00:47,970
So p x is five and let's see, B, y is also five here.

13
00:00:47,970 --> 00:00:53,550
And once we have this particular div, let's create a form here and the action, we are not going to

14
00:00:53,550 --> 00:00:54,720
define any action here.

15
00:00:54,720 --> 00:00:59,880
We are just going to say that the form method is going to be post and as usual what we will do here

16
00:00:59,880 --> 00:01:05,880
is that the lady CSR of token because obviously while submitting any form you would need a CSR of token

17
00:01:05,880 --> 00:01:06,180
here.

18
00:01:06,180 --> 00:01:09,720
So I'll add in CSR ref and the school token.

19
00:01:09,990 --> 00:01:12,690
And now let's start adding some fields for the form.

20
00:01:12,690 --> 00:01:15,570
So here, as we only want to accept a comment here.

21
00:01:15,570 --> 00:01:20,880
So what we will technically do here is that we'll actually go ahead, go inside the views and actually

22
00:01:20,880 --> 00:01:23,700
pass in a particular comment form here.

23
00:01:23,700 --> 00:01:28,290
So first of all, let's actually create a common form so that we could pass that particular form to

24
00:01:28,290 --> 00:01:29,090
this template.

25
00:01:29,100 --> 00:01:34,500
So in order to create a comment form, I have to go to the form, start by file and remember that you

26
00:01:34,500 --> 00:01:37,740
have to go to the forms py file of the post app.

27
00:01:37,740 --> 00:01:41,970
So here I'll create another form here which is going to be the comment form.

28
00:01:41,970 --> 00:01:45,810
So I would say class, let's name this thing as common form.

29
00:01:45,960 --> 00:01:52,140
This is going to inherit from form, start model form because we are actually targeting the comment

30
00:01:52,140 --> 00:01:52,590
model.

31
00:01:52,590 --> 00:01:54,720
So I'll say this is going to be model form.

32
00:01:55,020 --> 00:01:57,720
Now here let's provide some class for information.

33
00:01:57,720 --> 00:02:03,090
So class meta, we are going to see that the model which this is going to be using is the comment model.

34
00:02:03,090 --> 00:02:04,860
So I'll add comment here.

35
00:02:05,070 --> 00:02:07,980
Let's also import comment from the models as well.

36
00:02:07,980 --> 00:02:12,590
And now once we have specified the model, let's also specify the fields here.

37
00:02:12,600 --> 00:02:17,550
So in this particular case, as we only want to accept the body of the comment.

38
00:02:17,550 --> 00:02:19,530
So if you take a look at the model here.

39
00:02:20,890 --> 00:02:22,120
Here for the comment.

40
00:02:22,120 --> 00:02:26,400
We are only interested in the common body and that's what we want to accept.

41
00:02:26,410 --> 00:02:29,440
So here I would mention body in single quotes.

42
00:02:30,580 --> 00:02:33,130
Give a comma and we should be good to go.

43
00:02:33,310 --> 00:02:39,250
Now, once we have this form, now we just have to pass this particular form to a view so that that

44
00:02:39,250 --> 00:02:42,740
view could actually pass this form to the template.

45
00:02:42,760 --> 00:02:44,500
So now let's go to The View.

46
00:02:45,540 --> 00:02:48,730
And let's see where exactly we could pass in that form.

47
00:02:48,750 --> 00:02:52,510
So in here we already have this view, which is the feed view.

48
00:02:52,530 --> 00:02:56,640
And right now the feed view is actually only accepting the code.

49
00:02:56,640 --> 00:03:01,140
That is what it needs to be doing when we get a cat request.

50
00:03:01,290 --> 00:03:07,770
But now we need to also go ahead and handle a post request over here for actually handling the form

51
00:03:07,770 --> 00:03:09,120
submission, which we have.

52
00:03:09,150 --> 00:03:14,790
So we want to say that whenever a request method is post, we actually want to handle the form submission

53
00:03:14,790 --> 00:03:15,390
of the comment.

54
00:03:15,390 --> 00:03:20,700
And that's because if you actually go to the feed page, which is this page right here, as you can

55
00:03:20,700 --> 00:03:26,790
see, we are not submitting any kind of data here through the post request, but now we will be actually

56
00:03:26,790 --> 00:03:28,430
adding a form to add a comment.

57
00:03:28,440 --> 00:03:33,000
And whenever we submit that data, that data should be submitted via the post request.

58
00:03:33,120 --> 00:03:37,710
That means now here we have to write in the code for the post request as well.

59
00:03:37,950 --> 00:03:41,690
So here I would see if request or method.

60
00:03:41,700 --> 00:03:48,000
So whenever a request or method is equal to post, in that particular case, we basically have to go

61
00:03:48,000 --> 00:03:49,740
ahead and get the data from the form.

62
00:03:49,860 --> 00:03:52,990
So here the form which we are interested in is the comment form.

63
00:03:53,010 --> 00:03:55,410
So I would say comment, underscore form.

64
00:03:55,650 --> 00:03:59,820
And we will actually be importing comment from from the form start p file.

65
00:03:59,850 --> 00:04:05,520
So here I would say form dot forms and I guess dot forms is already imported here.

66
00:04:05,520 --> 00:04:09,360
I just need to say I want to import the comment from form here.

67
00:04:10,420 --> 00:04:15,430
Okay, So once I've imported that, I could now go ahead and make use of that in the field view.

68
00:04:15,460 --> 00:04:18,829
So here I would say this is going to be comment form.

69
00:04:18,850 --> 00:04:23,120
Let's get the data here from the request dot post.

70
00:04:23,140 --> 00:04:29,350
So now once we have this data, now what we want to do is that we want to create a comment object so

71
00:04:29,350 --> 00:04:31,630
that we could actually save that comment object.

72
00:04:31,810 --> 00:04:38,110
So here I would create a new comment object by saying new underscore comment.

73
00:04:38,320 --> 00:04:42,810
And this is going to be equal to nothing but get the data from the comment form.

74
00:04:42,820 --> 00:04:45,430
Save it, but do not commit that data.

75
00:04:45,460 --> 00:04:47,470
Therefore, I'll say commit equals false.

76
00:04:47,500 --> 00:04:53,200
Now, the reason why we are setting commit equals false here is because every comment which you actually

77
00:04:53,200 --> 00:04:56,560
have that comment needs to be associated with the post.

78
00:04:56,710 --> 00:04:59,980
And right now this is only getting all the data from the form.

79
00:04:59,980 --> 00:05:04,600
And that data or the comment data is not going to be associated with any post.

80
00:05:04,660 --> 00:05:11,350
That means now we actually have to go ahead and get the post ID from every post, which we have and

81
00:05:11,350 --> 00:05:16,990
we are actually going to get the post ID from this particular form itself.

82
00:05:16,990 --> 00:05:21,760
So we are going to add a field here which is going to be associated with the post ID and we are going

83
00:05:21,760 --> 00:05:22,780
to make it hidden.

84
00:05:22,780 --> 00:05:29,050
And from there we are going to get the post ID And the reason why we want to get the post ID is because

85
00:05:29,050 --> 00:05:34,390
once we have the common data from the form, we want to attach that post ID with that particular comment

86
00:05:34,390 --> 00:05:37,120
so that the comment gets associated with the post.

87
00:05:37,240 --> 00:05:38,810
So let's get the post ID.

88
00:05:38,830 --> 00:05:44,110
So I would say post underscore ID is going to be equal to and we are basically going to get this from

89
00:05:44,110 --> 00:05:45,190
the request itself.

90
00:05:45,190 --> 00:05:50,860
So I would say request dot post, dot get and I would get the.

91
00:05:51,990 --> 00:05:53,940
Post underscore ID.

92
00:05:54,180 --> 00:06:00,090
So once we have this post ID, we could now go ahead and get the actual post from this particular post

93
00:06:00,090 --> 00:06:00,450
ID.

94
00:06:00,480 --> 00:06:06,540
So here I would say post equals get that particular post using get object or photo four.

95
00:06:06,570 --> 00:06:08,400
Get the post from the post model.

96
00:06:08,400 --> 00:06:14,040
And here we want to get the post where the idea of the post is going to be equal to post underscore

97
00:06:14,790 --> 00:06:15,600
ID.

98
00:06:16,200 --> 00:06:19,290
So once this thing is done, we are pretty much good to go.

99
00:06:19,650 --> 00:06:24,810
Now, once we have this post now we could attach that post to the new comment which we have.

100
00:06:24,840 --> 00:06:30,930
So here I could say new comment dot post and this new comment post is nothing.

101
00:06:30,930 --> 00:06:36,570
But if you go to the models here, as you can see, we are simply taking this field of that particular

102
00:06:36,570 --> 00:06:41,830
comment and attaching it to a post because the type of this thing is a foreign key.

103
00:06:41,850 --> 00:06:47,070
So here I would say new comment or post is going to be this post which we have gotten.

104
00:06:47,490 --> 00:06:50,220
So here I would simply say this equals post.

105
00:06:50,250 --> 00:06:55,080
Now the comment is actually attached with a post and that's exactly what we want.

106
00:06:55,110 --> 00:07:00,600
Now, once finally the comment is ready, we could now save that particular comment object into the

107
00:07:00,600 --> 00:07:01,170
database.

108
00:07:01,170 --> 00:07:03,420
So finally I could say a new comment.

109
00:07:04,270 --> 00:07:05,890
Dot save.

110
00:07:06,010 --> 00:07:07,420
And we should be good to go.

111
00:07:07,420 --> 00:07:09,760
And the comment would be stored in the database.

112
00:07:10,090 --> 00:07:14,390
Now, what happens if the method or the request method is not equal to post?

113
00:07:14,410 --> 00:07:19,450
That means whenever we have a get method on feed, we should always go ahead and actually get the comment

114
00:07:19,450 --> 00:07:25,510
form and simply pass that particular comment form as context over here to this template, which is feed

115
00:07:25,930 --> 00:07:26,360
HTML.

116
00:07:26,410 --> 00:07:32,770
So here I would say else if the request method is not post, in that case, get the comment form just

117
00:07:32,770 --> 00:07:35,050
as we have gotten up over here above.

118
00:07:35,440 --> 00:07:40,930
So once we have this comment form now, we simply have to pass in that comment form up over here as

119
00:07:40,930 --> 00:07:41,590
context.

120
00:07:41,590 --> 00:07:48,370
So I would say comment, underscore, form, pass this thing as comment, underscore, form.

121
00:07:49,030 --> 00:07:54,940
So once this comment form is passed now, we could finally go ahead and actually render that particular

122
00:07:54,940 --> 00:07:58,570
form up in the feed dot HTML app over here.

123
00:07:59,340 --> 00:08:02,190
So we are going to learn how to do that in the next one.

124
00:08:02,190 --> 00:08:03,960
And doing that is actually quite simple.

125
00:08:03,960 --> 00:08:09,180
You just have to go through the fields which are actually present in the farm and then finally add a

126
00:08:09,180 --> 00:08:10,750
submit button at the end.

127
00:08:10,770 --> 00:08:12,990
So let's do that in the next lecture.

