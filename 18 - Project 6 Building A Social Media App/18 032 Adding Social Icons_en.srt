1
00:00:00,090 --> 00:00:05,700
In this particular lecture, let's add some social media icons like the like comment and share buttons

2
00:00:05,700 --> 00:00:07,590
to the page, which we currently have.

3
00:00:07,620 --> 00:00:12,070
So in order to get these particular icons, you could simply go to icon Finder dot com.

4
00:00:12,090 --> 00:00:18,270
So here we are into I can find a and here I have to search for some icons like for the like button let's

5
00:00:18,270 --> 00:00:24,480
say if you want to have the hood icon I could simply go ahead select that you could choose a particular

6
00:00:24,480 --> 00:00:25,460
image which you want.

7
00:00:25,470 --> 00:00:27,840
So let's say I want this particular image.

8
00:00:27,930 --> 00:00:34,170
I could simply download this particular thing in a PNG format or SVG format, whatever I would like

9
00:00:34,170 --> 00:00:39,840
to get it in and whatever size I want to get it in, I could simply go ahead, download that particular

10
00:00:39,840 --> 00:00:41,730
thing into a PNG format.

11
00:00:42,000 --> 00:00:48,450
I could simply go back, go back to the project, go back to the folder where we have the images.

12
00:00:48,450 --> 00:00:54,990
So we have the images inside this, uh, images folder or the static folder inside the user's app.

13
00:00:54,990 --> 00:00:59,400
So I'll simply drag it and drop that particular image up in that folder.

14
00:00:59,640 --> 00:01:06,360
And let's say I want to change the name of this thing to something like, let's say like dot PNG, and

15
00:01:06,360 --> 00:01:11,790
once we have this particular like dot PNG in there, our next job is to go ahead and actually add that

16
00:01:11,790 --> 00:01:13,580
to the index page, which we have.

17
00:01:13,590 --> 00:01:20,880
So let's head back, let's go back to the indexed HTML and we want to add that particular thing right

18
00:01:20,880 --> 00:01:22,380
after our image ends.

19
00:01:22,380 --> 00:01:24,960
So the image which we have ends in this div.

20
00:01:24,960 --> 00:01:30,420
So let's create another div right up over here and this div is going to act as a container for holding

21
00:01:30,420 --> 00:01:31,690
all of our icons.

22
00:01:31,710 --> 00:01:38,400
So inside this div I'll create another div and I'll call this thing as I can container because this

23
00:01:38,400 --> 00:01:44,130
is going to hold all of our icons and this is simply going to be an image and the source for this thing

24
00:01:44,130 --> 00:01:52,770
is going to be the path which is static because currently the images folder is in the static directory

25
00:01:52,770 --> 00:01:54,570
for the user's app.

26
00:01:54,570 --> 00:02:00,240
So user slash images slash and the name of image should be like dot org.

27
00:02:01,560 --> 00:02:03,990
And let's enjoy if we have that name.

28
00:02:03,990 --> 00:02:11,400
So if I go over there, we have the images inside static users images and like dot org.

29
00:02:11,430 --> 00:02:16,490
So if I go ahead, let's test if the image would be added here.

30
00:02:16,500 --> 00:02:18,020
So if I had to refresh.

31
00:02:18,030 --> 00:02:21,070
Okay, So we have some sort of an error here.

32
00:02:21,090 --> 00:02:25,850
I'm not sure why this is because it says invalid block static.

33
00:02:25,860 --> 00:02:26,180
Okay.

34
00:02:26,190 --> 00:02:29,310
So this is because we have not yet loaded the static tag.

35
00:02:29,520 --> 00:02:36,570
So let's load this static tag at the top as well by saying load static because whenever we are using

36
00:02:36,570 --> 00:02:42,000
the static keyword, whenever we are referring to a URL for static assets, you always need to load

37
00:02:42,000 --> 00:02:42,920
the static tag.

38
00:02:42,930 --> 00:02:44,820
And now if I had to refresh.

39
00:02:46,150 --> 00:02:46,960
Okay, It's this.

40
00:02:46,960 --> 00:02:49,150
This should be the first tag.

41
00:02:50,200 --> 00:02:53,520
So let's make this tag as the first tag and the issue would be resolved.

42
00:02:53,530 --> 00:02:56,320
So always make sure that you have extends at the top.

43
00:02:56,500 --> 00:03:00,730
So now if I go back here, as you can see, we have this particular like icon right here.

44
00:03:00,730 --> 00:03:05,020
But as of now, you'll be able to see that the size of that icon is pretty big.

45
00:03:05,050 --> 00:03:10,720
So let's change that by going inside the image tag and let's assign it a class.

46
00:03:10,720 --> 00:03:15,490
And let's see, I want the width of this thing to be five and the height to be five as well.

47
00:03:16,300 --> 00:03:21,480
Now it will actually turn it into a much smaller like button, which we have here, as you can see.

48
00:03:21,490 --> 00:03:25,730
But now this thing does not have any kind of space from the top and from the left.

49
00:03:25,750 --> 00:03:31,930
So let's fix that by styling up this particular container, which we have, which is the outer container

50
00:03:31,930 --> 00:03:33,190
for the icon container.

51
00:03:33,340 --> 00:03:39,430
So I would say the class is going to add the padding on the x axis to be six and padding on the Y axis

52
00:03:39,430 --> 00:03:40,210
to be four.

53
00:03:40,540 --> 00:03:44,950
As soon as I add that now, as you can see, it's actually aligned pretty well.

54
00:03:45,310 --> 00:03:50,590
Now, just like this particular image which we have, which is the like button, let's also add the

55
00:03:50,590 --> 00:03:52,420
comment and share buttons as well.

56
00:03:52,660 --> 00:03:59,800
So here I could go to I can find the search for comment button and we will find a button which is similar

57
00:03:59,800 --> 00:04:00,970
to the previous one.

58
00:04:01,090 --> 00:04:08,170
So let's say we go with a button which looks something like this and some of the images over here are

59
00:04:08,170 --> 00:04:08,590
paid.

60
00:04:08,590 --> 00:04:14,140
But if you want free images, you could simply go ahead and you could toggle filters, change this thing

61
00:04:14,140 --> 00:04:14,770
to free.

62
00:04:15,100 --> 00:04:17,649
And for some reason, I'm not able to do that.

63
00:04:17,649 --> 00:04:19,480
So let me zoom out.

64
00:04:19,480 --> 00:04:23,500
And if I click on Free now, I'll only get those icons which are free.

65
00:04:23,620 --> 00:04:25,480
So let's use this for now.

66
00:04:25,600 --> 00:04:27,760
Let's download this thing in PNG.

67
00:04:27,790 --> 00:04:34,430
Let's repeat the same process of actually dragging it and dropping it inside this particular image directory.

68
00:04:34,450 --> 00:04:36,220
So let's get that.

69
00:04:36,520 --> 00:04:37,900
Put it up over here.

70
00:04:37,930 --> 00:04:39,910
Let's rename this thing to.

71
00:04:41,020 --> 00:04:42,640
Comment dot org.

72
00:04:42,640 --> 00:04:45,910
And let's also get a share button or share icon as well.

73
00:04:45,910 --> 00:04:51,520
So I could say share and let's use one of these things over here.

74
00:04:51,760 --> 00:04:55,450
So let's use this one because it looks sort of transparent.

75
00:04:55,450 --> 00:04:57,490
Let's get that in there as well.

76
00:04:58,240 --> 00:05:02,460
Let's rename this thing to share dot org and there we have it.

77
00:05:02,470 --> 00:05:10,120
I could now go back and here, let's go inside the index dot HTML and let's add the newly added icons.

78
00:05:10,120 --> 00:05:16,450
So I'll go to index dot HTML, write up over here and I simply have to copy the image tag, which we

79
00:05:16,450 --> 00:05:22,120
already have because we simply have to add images with just the different names.

80
00:05:22,270 --> 00:05:28,540
So I could change this thing to common dot PNG and this thing to share dot PNG.

81
00:05:29,020 --> 00:05:32,980
If I go back to the browser, as you can see, the buttons are now added.

82
00:05:32,980 --> 00:05:36,190
But now the problem is they are actually added on top of each other.

83
00:05:36,190 --> 00:05:42,610
So if I want to place them side by side, I simply have to add a flex here, save this, go back and

84
00:05:42,610 --> 00:05:44,920
now there's no space in between them.

85
00:05:44,920 --> 00:05:52,030
So let's fix that by adding a class gap three which basically adds a gap of three for all the flex elements

86
00:05:52,030 --> 00:05:53,760
which we have in a flex container.

87
00:05:53,770 --> 00:05:58,870
So if I had refresh now, as you can see, this is what it looks like and you'll be able to see that

88
00:05:58,870 --> 00:06:03,970
the heart button looks a little bit smaller because I think we have downloaded a smaller size for that

89
00:06:03,970 --> 00:06:04,480
one.

90
00:06:04,480 --> 00:06:09,700
So it could be fixed by simply using a little bit larger image, but we are not going to work on that

91
00:06:09,700 --> 00:06:10,450
as of now.

92
00:06:10,450 --> 00:06:13,240
We just want to make this thing fully functional first.

93
00:06:13,240 --> 00:06:20,410
Okay, So once we have these icons, the next two things which are remaining are to be added to the

94
00:06:20,410 --> 00:06:23,080
post are the post title and the post caption.

95
00:06:23,080 --> 00:06:28,470
So we are going to add the post title and post caption right after this particular div.

96
00:06:28,480 --> 00:06:31,660
So after this particular div let's add another div.

97
00:06:31,660 --> 00:06:37,360
So this div is going to be the container for the post caption as well as the post title.

98
00:06:37,360 --> 00:06:42,850
And let's use the same class as which we have used here, which actually adds margin and padding to

99
00:06:42,850 --> 00:06:44,470
this particular things as well.

100
00:06:44,530 --> 00:06:49,150
And the first of which we are going to have here is going to be for post title.

101
00:06:49,150 --> 00:06:56,950
So let's see, I simply add post dot title in here and for the second, which we have here, let's not

102
00:06:56,950 --> 00:07:04,420
add a live for the caption, Let's add a paragraph instead and let's see this thing says post dot caption.

103
00:07:06,390 --> 00:07:08,640
And now if you could looks and consistent.

104
00:07:08,640 --> 00:07:14,590
And if it does not have a proper structure, you could simply use an extension called less prettier.

105
00:07:14,610 --> 00:07:20,280
But right now I've actually disabled prettier because what prettier does is that it actually aligns

106
00:07:20,280 --> 00:07:26,370
the static tags and the block body and end block tags in such a way which is inconsistent with Django,

107
00:07:26,370 --> 00:07:28,170
and that might cause issues.

108
00:07:28,560 --> 00:07:34,020
So once you have written all the code to clean up your code, simply use prettier to format your code

109
00:07:34,020 --> 00:07:34,780
in a better way.

110
00:07:34,800 --> 00:07:37,320
So we are going to do that pretty soon.

111
00:07:37,320 --> 00:07:40,110
But for now, let's go ahead and let's style this thing up.

112
00:07:40,260 --> 00:07:44,930
So for this particular title tag here, I want the fonts to be bolder.

113
00:07:44,940 --> 00:07:47,190
So I would say font dash bold.

114
00:07:47,460 --> 00:07:52,100
Let's make the text to extra large and let's set the margin at the bottom to two.

115
00:07:52,110 --> 00:07:56,790
And here, as you can see, we have the title and the caption for this particular image set up.

116
00:07:56,790 --> 00:07:59,150
And now let's style up the caption as well.

117
00:07:59,160 --> 00:08:07,650
So here for the caption, I want to add a class which is going to make it text of a slighter darker

118
00:08:07,650 --> 00:08:08,310
gray shade.

119
00:08:08,310 --> 00:08:16,470
So text gray with the shade of, let's say 700 and I want to say text base, which means I want a basic

120
00:08:16,470 --> 00:08:19,180
text and I don't want anything fancy in there.

121
00:08:19,200 --> 00:08:22,020
So as you can see, this is what the Post currently looks like.

122
00:08:22,020 --> 00:08:27,390
So now we are actually done creating this particular page, and this page is going to render any new

123
00:08:27,390 --> 00:08:29,370
post which we are going to add here.

124
00:08:29,550 --> 00:08:35,760
And therefore let's get rid of the post use and post title from here and we are pretty much good to

125
00:08:35,760 --> 00:08:36,179
go.

126
00:08:36,480 --> 00:08:42,659
So once we have styled up this particular indexed HTML page or the home page for the user in the upcoming

127
00:08:42,659 --> 00:08:47,540
lecture, let's go ahead and let's style up all the remaining pages, which we actually have in our

128
00:08:47,580 --> 00:08:49,020
entire Django project.

129
00:08:49,020 --> 00:08:53,700
And if you don't want to go through the styling of these pages, I will also provide the source code

130
00:08:53,700 --> 00:08:56,420
for these pages in a different file.

131
00:08:56,430 --> 00:09:00,390
So you could simply go ahead and copy them as per your own requirements.

132
00:09:00,390 --> 00:09:04,470
So thank you very much for watching and I'll see you guys in the next one.

133
00:09:04,650 --> 00:09:05,340
Thank you.

