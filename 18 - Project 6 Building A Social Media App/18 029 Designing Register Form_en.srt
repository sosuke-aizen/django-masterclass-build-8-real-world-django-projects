1
00:00:00,120 --> 00:00:04,380
So in this particular lecture, let's start by styling up this registration form.

2
00:00:04,560 --> 00:00:09,630
So in order to style this thing up, we'll go back to VTS code and here we'll go to the file which is

3
00:00:09,630 --> 00:00:11,250
registered on HTML.

4
00:00:11,730 --> 00:00:15,090
So here is where I registered HTML files.

5
00:00:15,090 --> 00:00:20,280
So currently we are simply rendering up the form elements right here and we want to style it similar

6
00:00:20,280 --> 00:00:23,470
to how we have actually styled this log in HTML.

7
00:00:23,490 --> 00:00:28,710
So this is style using Tilman and we need to do the exact same thing over there as well.

8
00:00:28,800 --> 00:00:33,740
So I'll give you a simple trick which you could use while styling up your own web pages.

9
00:00:33,750 --> 00:00:39,450
So whenever you are designing any kind of app, first of all, you'll have the basic form structure

10
00:00:39,450 --> 00:00:42,060
in order to go ahead and test if that form works.

11
00:00:42,090 --> 00:00:48,120
Now, once you know that this particular form works, you could simply go ahead, take any other form

12
00:00:48,120 --> 00:00:49,410
which you have already styled.

13
00:00:49,410 --> 00:00:54,390
You could copy all the styling from there, and then you simply have to go ahead and paste it up over

14
00:00:54,390 --> 00:00:55,290
here at the top.

15
00:00:55,530 --> 00:00:58,710
So here, as you can see, I have paste all the code here.

16
00:00:58,830 --> 00:01:05,099
But now what happens is this already is extending from the base template and it already has a block

17
00:01:05,099 --> 00:01:06,090
body and an block.

18
00:01:06,270 --> 00:01:12,630
So you have to go ahead and take the elements which you have here and replace these elements up over

19
00:01:12,630 --> 00:01:13,110
here.

20
00:01:13,140 --> 00:01:18,930
So first of all, as we are already extending from the base template, we should get rid of this line

21
00:01:19,740 --> 00:01:22,230
as we are already having a blog body tag.

22
00:01:22,230 --> 00:01:26,850
You should also get rid of this as well and you should already get rid of and block as well.

23
00:01:27,090 --> 00:01:35,370
And then next step is to go ahead, go right before the end block tag and take your older form, cut

24
00:01:35,370 --> 00:01:36,930
it and paste it up over here.

25
00:01:36,960 --> 00:01:42,180
Now, there's a specific reason why we do that, and that's because when you place this form inside

26
00:01:42,180 --> 00:01:44,660
the end block, this form would be rendered as well.

27
00:01:44,670 --> 00:01:46,680
So now if I go to register.

28
00:01:47,840 --> 00:01:49,220
You'll be able to see that.

29
00:01:49,250 --> 00:01:49,640
Okay.

30
00:01:49,640 --> 00:01:55,400
Now, not just this form is being rendered, but even we have this form being rendered as well.

31
00:01:55,400 --> 00:02:01,730
And the reason why we actually need this is because we now need to get the names of these particular

32
00:02:01,730 --> 00:02:02,960
fields, which we have.

33
00:02:03,140 --> 00:02:06,980
So first of all, I'll go ahead, right click over here.

34
00:02:07,670 --> 00:02:13,610
If I inspect this particular field, I will be able to see that the name of this particular field is

35
00:02:13,610 --> 00:02:14,250
email.

36
00:02:14,270 --> 00:02:19,910
So that means if I have to render this particular field up over here in place of username, I simply

37
00:02:19,910 --> 00:02:20,990
have to go back.

38
00:02:21,260 --> 00:02:27,860
And here instead of saying form dot username, I have to say form dot email.

39
00:02:28,280 --> 00:02:32,570
And also you'll be able to notice here that the form which we have.

40
00:02:34,200 --> 00:02:37,870
It's actually called us user form and not just form.

41
00:02:37,890 --> 00:02:42,700
So here I have to replace this with user underscore form.

42
00:02:42,720 --> 00:02:48,390
And as soon as I do that and if I had refresh, you'll be able to see that when I inspect this field

43
00:02:48,390 --> 00:02:55,710
now, you'll be able to see that the type of this thing is email, name is email, and this now becomes

44
00:02:55,710 --> 00:02:56,820
an email field.

45
00:02:56,940 --> 00:03:00,120
So we simply now have to change the label of this thing.

46
00:03:00,150 --> 00:03:03,050
So I would type an email and there we have it.

47
00:03:03,060 --> 00:03:05,700
We have replicated one of the fields which we have here.

48
00:03:06,090 --> 00:03:12,030
Now let's say I need to find out what this username field is called so I could inspect this here.

49
00:03:12,030 --> 00:03:14,860
I'll be able to see that it's actually called us username.

50
00:03:14,880 --> 00:03:18,770
So I'll simply replace that particular field with username.

51
00:03:18,780 --> 00:03:20,090
So let's go back.

52
00:03:20,100 --> 00:03:22,050
So here I would say.

53
00:03:22,830 --> 00:03:23,910
Username.

54
00:03:23,970 --> 00:03:28,830
Let's replace this with form or user underscore form dot username.

55
00:03:29,220 --> 00:03:35,430
So I'll copy this, paste it up over here and replace email with username.

56
00:03:36,270 --> 00:03:36,830
Okay.

57
00:03:36,840 --> 00:03:41,160
So once that thing is done, let's also get the other fields as well.

58
00:03:41,160 --> 00:03:46,150
So the other fields which we now have over here are first name, password, confirm, password and register.

59
00:03:46,170 --> 00:03:52,450
So for first name, if I inspect this, it sees it's called as first underscore name.

60
00:03:52,470 --> 00:03:58,890
So I simply now have to go ahead, take a particular field like this along with the label.

61
00:03:59,220 --> 00:04:04,290
Paste it up over here, change this thing to first name.

62
00:04:04,950 --> 00:04:09,150
Let's also change this thing to user form Dot.

63
00:04:11,010 --> 00:04:14,790
First underscore name because that's actually the name of our field.

64
00:04:15,180 --> 00:04:17,550
Let's check what this password field is called.

65
00:04:17,579 --> 00:04:19,290
It's simply called last password.

66
00:04:19,320 --> 00:04:23,100
The second password field is actually called as password two.

67
00:04:23,340 --> 00:04:25,410
So let's add those fields as well.

68
00:04:25,440 --> 00:04:27,210
So again, go back.

69
00:04:28,130 --> 00:04:30,140
Copy the fields, which we have here.

70
00:04:30,290 --> 00:04:33,080
Paste it up over here and over here.

71
00:04:34,080 --> 00:04:36,510
Change the name of the first one to.

72
00:04:38,830 --> 00:04:40,000
Password.

73
00:04:40,540 --> 00:04:45,280
Second one, I would say confirm password.

74
00:04:46,370 --> 00:04:52,810
The first field is going to be changed to something like, let's say password and this is called as

75
00:04:52,820 --> 00:04:53,750
password to.

76
00:04:55,530 --> 00:04:57,360
Okay, so once this thing is done.

77
00:04:58,090 --> 00:04:59,690
We are pretty much good to go.

78
00:04:59,710 --> 00:05:01,480
Then we have this register button.

79
00:05:01,480 --> 00:05:02,980
So let's scroll down.

80
00:05:02,980 --> 00:05:05,500
Let's change this thing to register.

81
00:05:07,280 --> 00:05:12,200
Okay, So once we have this, if I hit refresh now, as you can see, we are actually able to go ahead

82
00:05:12,200 --> 00:05:13,250
and get these fields.

83
00:05:13,250 --> 00:05:17,840
And you'll also be able to notice that the form is not styled properly, because when we actually go

84
00:05:17,840 --> 00:05:22,730
ahead and click on a particular form field, there's a blue border which we get here.

85
00:05:22,760 --> 00:05:25,790
So we are going to resolve that issue very soon.

86
00:05:25,970 --> 00:05:31,190
But for now, as you can see, the registration form is pretty much ready and we have to actually change

87
00:05:31,190 --> 00:05:34,730
the heading over here because currently it's this log in form.

88
00:05:34,850 --> 00:05:37,250
So let's quickly change that as well.

89
00:05:37,760 --> 00:05:40,910
So I will say register user.

90
00:05:41,390 --> 00:05:47,720
And finally, once we are done with this form, which is this form right here, I could go ahead and

91
00:05:47,720 --> 00:05:48,530
get rid of it.

92
00:05:49,520 --> 00:05:54,920
Now, if I head back, I should be able to have this register user form here wherein I could go ahead

93
00:05:54,920 --> 00:05:56,190
and register a user.

94
00:05:56,210 --> 00:06:01,010
So if you want to test this particular form, you could fill up certain details and click on register

95
00:06:01,010 --> 00:06:02,720
and see if that actually works.

96
00:06:02,720 --> 00:06:08,540
And if it does not work, you simply have to go ahead and see what particular form field names have

97
00:06:08,540 --> 00:06:09,980
you set up incorrectly.

98
00:06:10,400 --> 00:06:15,500
So once we have this particular registration form set up in the next lecture, let's actually go ahead

99
00:06:15,500 --> 00:06:19,200
and let's set up the styling for the index page, which we have.

100
00:06:19,220 --> 00:06:24,030
So currently the index page is pretty dull and boring and we do not get anything out of it.

101
00:06:24,050 --> 00:06:28,400
So from the next lecture onwards, we are going to style this thing up in a proper manner.

102
00:06:28,550 --> 00:06:30,890
So let's go ahead and do that in the next one.

