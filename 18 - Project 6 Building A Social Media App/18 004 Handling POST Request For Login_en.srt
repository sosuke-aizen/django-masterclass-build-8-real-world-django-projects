1
00:00:00,090 --> 00:00:05,730
Okay, so now our job is to go ahead and make this form functional so that whenever we click on the

2
00:00:05,730 --> 00:00:07,980
submit button, the post request is handled.

3
00:00:07,980 --> 00:00:14,490
And once the Post request is handled, we essentially want to go ahead and simply log in the user in

4
00:00:14,490 --> 00:00:15,240
our Django app.

5
00:00:15,240 --> 00:00:18,240
So for that very reason, we have to handle the post request.

6
00:00:18,360 --> 00:00:24,530
So as I earlier mentioned, by default, we handle the get requests whenever we create a view.

7
00:00:24,540 --> 00:00:29,550
So that means whenever we get a get request, this is the logic which is going to be executed.

8
00:00:29,700 --> 00:00:33,570
However, when the form is submitted, we should be able to handle the post request.

9
00:00:33,570 --> 00:00:38,460
So here I would say if request dot method is equally equal to post.

10
00:00:38,460 --> 00:00:43,890
So here I would say post then do this and mine that.

11
00:00:43,890 --> 00:00:48,900
Whenever we say request dot method, we are actually getting that particular method from the request

12
00:00:48,900 --> 00:00:49,550
object.

13
00:00:49,560 --> 00:00:55,920
So essentially when you click the submit button as this particular form, So let me show you the form.

14
00:00:55,920 --> 00:01:03,390
So as this form actually submits a post request, when we submit that technically this request object

15
00:01:03,390 --> 00:01:06,840
which we get now, is going to contain the method as post.

16
00:01:07,290 --> 00:01:11,760
So we simply get access to the request object and then access the method of it.

17
00:01:11,760 --> 00:01:16,560
And then we know that the method is post and then we are going to do the following things.

18
00:01:16,560 --> 00:01:22,920
So once the request method is post, we are going to do a certain things or else we are simply going

19
00:01:22,920 --> 00:01:25,620
to create this particular form right here.

20
00:01:25,620 --> 00:01:29,220
So here I will put this particular block of code and else.

21
00:01:29,820 --> 00:01:34,620
And right now we are having an error here because we have not written any code inside the if block.

22
00:01:34,620 --> 00:01:37,970
So now let's try the logic of what happens when we submit the form.

23
00:01:37,980 --> 00:01:42,900
So when we submit the form, we essentially want to take the data, which is the username and password,

24
00:01:42,900 --> 00:01:48,000
and we simply have to log in the user using this particular credentials.

25
00:01:48,000 --> 00:01:53,490
So here, in order to get the form data, first of all, you need to get access to the form.

26
00:01:53,490 --> 00:01:56,610
So here I would get access to the login form.

27
00:01:57,030 --> 00:02:02,370
And now in order to get the data which is posted, we could get that data from the request object,

28
00:02:02,370 --> 00:02:03,030
which we have.

29
00:02:03,030 --> 00:02:09,509
So just as we gotten the access to request method, we could get access to the data as well.

30
00:02:09,509 --> 00:02:16,680
So here I would say request dot post and this is actually going to give us access to all the form data

31
00:02:16,680 --> 00:02:17,520
which we have.

32
00:02:17,760 --> 00:02:24,420
So here I would say form equals this and now we are getting an entire form which is filled up with the

33
00:02:24,420 --> 00:02:26,580
request data which has been posted.

34
00:02:26,910 --> 00:02:33,210
So once we have that particular data, we could check the validity of that particular data using an

35
00:02:33,210 --> 00:02:35,310
inbuilt method which is is valid.

36
00:02:35,310 --> 00:02:40,650
So here we say if form dot is underscore valid.

37
00:02:41,370 --> 00:02:47,820
So if this thing is valid only then we could go ahead and use that data to log in the user.

38
00:02:48,120 --> 00:02:52,860
So here I would say if the form data is valid and then do the following things.

39
00:02:53,100 --> 00:02:58,710
So first of all, when the form data is valid, we want to pass that form data to a method which is

40
00:02:58,710 --> 00:03:00,090
called us authenticate.

41
00:03:00,240 --> 00:03:05,550
And what the authenticate method does is that it accepts the request object which we have.

42
00:03:06,150 --> 00:03:12,060
It accepts the username and password as well and checks if that particular password is present in the

43
00:03:12,060 --> 00:03:12,660
database.

44
00:03:12,660 --> 00:03:16,590
So you don't have to manually create the database and query that database.

45
00:03:16,590 --> 00:03:19,560
It's automatically done by Django using the Aurum.

46
00:03:19,770 --> 00:03:26,400
And what that does is that the Authenticate method checks for the username and password in the database,

47
00:03:26,400 --> 00:03:33,450
and if it actually finds the user is present, then it returns a user object and then we save that particular

48
00:03:33,450 --> 00:03:35,430
user object and a user variable.

49
00:03:35,430 --> 00:03:39,000
So let's go ahead and do that and it will start making more sense.

50
00:03:39,150 --> 00:03:42,270
So first of all, you need to import the Authenticate method.

51
00:03:42,270 --> 00:03:46,770
So the authenticate method is actually present in the django contrib auth.

52
00:03:46,830 --> 00:03:55,380
So I would say from django dot contrib dot auth import the method which is authenticate.

53
00:03:55,890 --> 00:03:59,730
And now here I simply have to call that authenticate method.

54
00:03:59,730 --> 00:04:01,710
So here I will say authenticate.

55
00:04:04,000 --> 00:04:07,840
And this authenticate method, as you can see, takes a bunch of parameters.

56
00:04:07,840 --> 00:04:10,450
So first parameter it takes is the request.

57
00:04:11,290 --> 00:04:15,010
So let's pass in the request object, give a comma.

58
00:04:15,130 --> 00:04:21,010
The second parameter which we need to pass in here is we have to pass in the username and password.

59
00:04:21,670 --> 00:04:27,550
But instead of directly passing in the username and password, we first have to clean that particular

60
00:04:27,550 --> 00:04:31,360
data because we don't want anything fishy going on in there.

61
00:04:31,510 --> 00:04:36,130
So here, in order to clean up that data, I would take the data from the form.

62
00:04:36,130 --> 00:04:42,430
So here I would say form and then see cleaned data that's actually going to clean up the data for us.

63
00:04:42,430 --> 00:04:45,940
And let's see if the data into a variable called us data.

64
00:04:46,420 --> 00:04:53,650
And now in order to actually pass in the username here, I would say username equals and then in order

65
00:04:53,650 --> 00:05:00,430
to get access to the username which is saved in this data, I would say data of username and same as

66
00:05:00,430 --> 00:05:01,890
the case with password as well.

67
00:05:01,900 --> 00:05:04,000
I would say password equals.

68
00:05:04,000 --> 00:05:06,130
That's going to be data of.

69
00:05:07,470 --> 00:05:08,230
Password.

70
00:05:08,280 --> 00:05:13,740
So this is going to give me access to the cleaned username and password data, and that's going to be

71
00:05:13,740 --> 00:05:16,170
passed to this particular authenticate method.

72
00:05:16,260 --> 00:05:21,000
And now, as I earlier mentioned, this authenticate method, it's actually going to go through your

73
00:05:21,000 --> 00:05:25,350
database, check if the username is present with that username and password.

74
00:05:25,770 --> 00:05:30,450
If it finds out that the user is actually present, it's going to return a user object.

75
00:05:30,450 --> 00:05:35,640
So as this returns a user object, that user object has to be stored somewhere.

76
00:05:35,670 --> 00:05:41,280
So let's type in user equals this and now the user object is actually stored.

77
00:05:41,640 --> 00:05:46,380
And once the user object is stored, we could finally go ahead and do something with it.

78
00:05:46,890 --> 00:05:52,920
So if the user object is found, that means the login credentials are valid.

79
00:05:52,920 --> 00:05:57,520
And if the login credentials are valid, we simply want to log in that particular user.

80
00:05:57,540 --> 00:05:59,040
It's as simple as that.

81
00:05:59,520 --> 00:06:03,750
So if the user exists, that means if the user is not none.

82
00:06:04,050 --> 00:06:09,420
So we say if the user is not none.

83
00:06:10,700 --> 00:06:16,260
Which means that if this user object is not empty in that particular case, we want to log in the user.

84
00:06:16,280 --> 00:06:20,300
So in order to log in the user, we have a method which is called last login.

85
00:06:20,300 --> 00:06:25,970
So I have to import this method from the same package from where we have imported authenticate.

86
00:06:26,240 --> 00:06:34,790
And now once we have imported that, I simply have to say I want to log in the user and this login method

87
00:06:34,790 --> 00:06:37,720
actually takes in the request object.

88
00:06:37,730 --> 00:06:43,340
So I have to pass an request and along with it I have to type in the user object as well because we

89
00:06:43,340 --> 00:06:45,430
want to log in that specific user.

90
00:06:45,440 --> 00:06:47,840
Therefore the user object is obviously needed.

91
00:06:48,110 --> 00:06:53,960
So once this thing is done, we are pretty much good to go and if the user is logged in, we want to

92
00:06:53,960 --> 00:06:57,950
return some sort of a response, which is let's see, HTTP response.

93
00:06:57,950 --> 00:07:02,210
So we return and HTTP response.

94
00:07:02,210 --> 00:07:06,290
And here it's actually importing HTTP response from a different package.

95
00:07:06,290 --> 00:07:07,630
So we don't want that.

96
00:07:07,640 --> 00:07:10,790
Instead, we want to import it from Django dot http.

97
00:07:10,790 --> 00:07:20,840
So from Django dot http, we import HTTP response and here, let's use that instead.

98
00:07:20,840 --> 00:07:31,400
So HTTP response and we want to say something like a user authenticated and logged in.

99
00:07:31,970 --> 00:07:38,720
Or else if this is false, which means that if the user does not exist, that means that our authenticate

100
00:07:38,720 --> 00:07:44,900
method is not able to find any user with these credentials, which means that we want to say something

101
00:07:44,900 --> 00:07:46,520
like, let's say.

102
00:07:47,500 --> 00:07:48,550
Invalid login.

103
00:07:48,550 --> 00:07:59,020
So here I would go ahead and say return HTTP response, which would say something like invalid credentials

104
00:08:00,340 --> 00:08:01,540
or invalid login.

105
00:08:02,410 --> 00:08:02,860
Okay.

106
00:08:02,860 --> 00:08:05,920
So once we are done with this, we are pretty much good to go.

107
00:08:05,950 --> 00:08:08,650
Hopefully everything is correct and this should work.

108
00:08:08,680 --> 00:08:13,870
Now the problem here is that we have no way to actually test this particular login method, and that's

109
00:08:13,870 --> 00:08:19,450
because we do not have any user registered already in our site and therefore we are actually going to

110
00:08:19,450 --> 00:08:21,100
create a super user for that.

111
00:08:21,460 --> 00:08:27,340
But even before creating super user, first of all, make sure that you have used proper indentations

112
00:08:27,340 --> 00:08:32,590
over here or else you are going to have an error when you execute this particular view.

113
00:08:32,620 --> 00:08:32,980
Okay.

114
00:08:32,980 --> 00:08:38,380
So now in the next lecture, let's go ahead and let's create a super user so that we could test this

115
00:08:38,380 --> 00:08:40,140
particular login functionality.

116
00:08:40,150 --> 00:08:44,230
So thank you very much for watching and I'll see you guys in the next one.

117
00:08:44,410 --> 00:08:45,130
Thank you.

