1
00:00:00,090 --> 00:00:01,290
In this particular lecture.

2
00:00:01,290 --> 00:00:06,540
Let's go ahead and design the functionality to allow users to change their passwords.

3
00:00:06,630 --> 00:00:10,930
So in order to design any functionality, we obviously need a view.

4
00:00:10,950 --> 00:00:12,840
We need a URL pattern.

5
00:00:12,870 --> 00:00:14,280
We need a template.

6
00:00:14,310 --> 00:00:16,790
That's all we have to do in order to do that.

7
00:00:16,800 --> 00:00:23,970
And as I earlier mentioned, as we had the login and log out view which we had used from the Django

8
00:00:23,970 --> 00:00:30,210
dot com report oath, We also have some of the views from this particular package which are the password

9
00:00:30,210 --> 00:00:35,820
change view as well, which means that you don't have to go ahead and write a whole bunch of view in

10
00:00:35,820 --> 00:00:37,260
order to change your password.

11
00:00:37,260 --> 00:00:42,000
Instead, what you could simply do is that you could simply go ahead and make use of the password change

12
00:00:42,000 --> 00:00:44,200
view in order to change the password.

13
00:00:44,220 --> 00:00:46,970
So let's go ahead and let's learn how to implement that.

14
00:00:46,980 --> 00:00:53,370
So the password change view is actually present in the django dot contrib dot auth package and now you

15
00:00:53,370 --> 00:00:56,730
simply have to go ahead and make use of that particular view up over here.

16
00:00:56,940 --> 00:01:03,930
So let's say I want to create a path to change the password and let's say the user has to type in password

17
00:01:03,930 --> 00:01:06,900
underscore change in order to access that view.

18
00:01:06,900 --> 00:01:10,590
So I'll add this thing up over here and then I would say OD view.

19
00:01:10,890 --> 00:01:14,520
And from the OT view I want to use the password change view.

20
00:01:14,520 --> 00:01:19,650
So as soon as you type a dot here, you'll be able to see the password change view right up over here.

21
00:01:19,650 --> 00:01:22,050
So here it's a password change view.

22
00:01:22,080 --> 00:01:25,800
As usual, we will see this as underscore view.

23
00:01:27,030 --> 00:01:28,470
And we should be good to go.

24
00:01:28,500 --> 00:01:32,880
Now, let's also give it some name, so I'll give a comma and add a name here.

25
00:01:32,880 --> 00:01:36,120
And let's call this thing as password change itself.

26
00:01:36,120 --> 00:01:42,630
So password underscore, change, give a comma at the end.

27
00:01:42,930 --> 00:01:43,320
Okay.

28
00:01:43,320 --> 00:01:48,000
So now once this particular view is associated with this particular URL pattern.

29
00:01:48,120 --> 00:01:52,650
Now, you also actually need to go ahead and create a template for this as well.

30
00:01:52,650 --> 00:01:58,800
So right now what happens is that if you try visiting this URL, so let's try visiting that, which

31
00:01:58,800 --> 00:02:00,930
is password underscore change.

32
00:02:01,950 --> 00:02:07,830
As you can see, you will be redirected to the admin panel template right up over here and we don't

33
00:02:07,830 --> 00:02:08,430
want that.

34
00:02:08,430 --> 00:02:13,800
Instead, what we want to do is that we actually want to render our very own form with our very own

35
00:02:13,800 --> 00:02:14,490
theme.

36
00:02:14,490 --> 00:02:19,740
And therefore, in order to do that, I'll go ahead and create a new form which is called as password

37
00:02:19,740 --> 00:02:20,820
change form.

38
00:02:20,820 --> 00:02:28,860
So here I'll go ahead and go to the templates and the user's app, create a new file and call it as.

39
00:02:30,440 --> 00:02:35,960
Password underscore change, underscore form, dot html.

40
00:02:36,560 --> 00:02:41,510
And in this particular HTML file, I'll first of all, make this thing extend from the base template.

41
00:02:41,540 --> 00:02:44,750
So here I will add the regular template syntax.

42
00:02:44,750 --> 00:02:56,330
I would say this extends from users forward slash base dot HTML, then have the block body and block

43
00:02:56,330 --> 00:02:56,850
tags.

44
00:02:56,870 --> 00:03:00,190
And over here we simply have to go ahead and pass in the form.

45
00:03:00,200 --> 00:03:01,700
So here I'll create a form.

46
00:03:01,820 --> 00:03:05,240
And this form is going to have a method which is post.

47
00:03:05,240 --> 00:03:10,280
So I would say form method is going to be equal to post, and I simply have to render the form over

48
00:03:10,280 --> 00:03:10,600
here.

49
00:03:10,610 --> 00:03:16,820
So I would say form dot as underscore P and before that have a CSR F token.

50
00:03:16,970 --> 00:03:23,540
So here I would see CSS, CSR of underscore token and also we need to type in an input field as well.

51
00:03:23,840 --> 00:03:28,700
And here this input field is going to say something like input type equals submit.

52
00:03:29,480 --> 00:03:35,480
Okay, so now once this thing is done, let's go back here and to this I have to pass in the template

53
00:03:35,510 --> 00:03:36,170
name as well.

54
00:03:36,170 --> 00:03:44,060
So template underscore name is going to be nothing, but it's going to be users forward slash that's

55
00:03:44,060 --> 00:03:46,580
going to be password change, form dot HTML.

56
00:03:46,580 --> 00:03:51,590
So password change form, dot, html.

57
00:03:55,270 --> 00:04:01,210
So once I go back up over here, as you can see now, it will basically go ahead and present us with

58
00:04:01,210 --> 00:04:07,420
this particular form right here, which is our own form, which we could style as per our own needs.

59
00:04:07,720 --> 00:04:12,320
So once this thing is done, let's now try changing the password of the super user.

60
00:04:12,340 --> 00:04:16,600
So once we are here, let's type in the old password.

61
00:04:16,870 --> 00:04:20,709
So the old password is super user.

62
00:04:20,740 --> 00:04:26,080
Let's say the new password is test, test one, two, three.

63
00:04:26,110 --> 00:04:28,570
Let's type in the new password one more time.

64
00:04:28,960 --> 00:04:31,630
Test, test one, two, three.

65
00:04:32,050 --> 00:04:33,550
Now, if I had submit.

66
00:04:34,910 --> 00:04:40,160
Now, it will actually go ahead and give us an error here, because whenever the password change process

67
00:04:40,160 --> 00:04:44,060
is complete, we have to render another view, which we have to create.

68
00:04:44,060 --> 00:04:47,660
But first, let's ensure if the password has been changed.

69
00:04:47,750 --> 00:04:50,410
So let's try logging in with the older password.

70
00:04:50,420 --> 00:04:51,850
So let's try logging in.

71
00:04:51,860 --> 00:04:57,050
I will type in my older password here, which is super user.

72
00:04:57,920 --> 00:05:00,020
Now it sees invalid credentials.

73
00:05:00,050 --> 00:05:04,610
Now let's try logging in with a newer password, which is test, test.

74
00:05:04,610 --> 00:05:05,600
One, two, three.

75
00:05:06,140 --> 00:05:11,390
Click on submit and it sees user authenticated and logged in, which means that the password change

76
00:05:11,390 --> 00:05:13,130
functionality is working well.

77
00:05:13,160 --> 00:05:19,220
But now the only problem is that whenever we go ahead and whenever we change the password, the page

78
00:05:19,220 --> 00:05:21,830
which we are getting or the view which we are getting is not defined.

79
00:05:21,830 --> 00:05:24,170
So that view needs to be defined as well.

80
00:05:24,170 --> 00:05:29,420
And that view is nothing, but it's called as the password change done view.

81
00:05:29,660 --> 00:05:32,390
So let's set that thing up over here.

82
00:05:32,480 --> 00:05:40,100
So here I would say path and this path is going to be for password change and done.

83
00:05:40,100 --> 00:05:45,680
So I would say password change forward, slash, done.

84
00:05:45,710 --> 00:05:52,970
Let's say this is the view which we want to visit and then I would say odd view dot password change

85
00:05:52,970 --> 00:05:58,520
done view instead of password change view simply say as underscore view.

86
00:05:59,540 --> 00:06:00,800
Let's give it a name.

87
00:06:00,860 --> 00:06:04,940
Let's say a name is going to be nothing but password change done.

88
00:06:04,970 --> 00:06:10,820
So password change dot, give a comma and we should be good to go.

89
00:06:11,060 --> 00:06:12,710
Let's try changing the password.

90
00:06:12,710 --> 00:06:16,250
So let's say the old password is test, test.

91
00:06:16,250 --> 00:06:17,210
One, two, three.

92
00:06:17,240 --> 00:06:17,720
Let's see.

93
00:06:17,720 --> 00:06:20,840
The new password is super user.

94
00:06:20,840 --> 00:06:25,520
One, two, three, super user One, two, three.

95
00:06:25,550 --> 00:06:27,290
Let's try hitting submit.

96
00:06:27,830 --> 00:06:31,070
And as you can see now, it's is password change successful?

97
00:06:31,070 --> 00:06:38,000
And again, here we actually have this Django admin template and if you want to replace it with our

98
00:06:38,000 --> 00:06:41,560
own template, we have to actually go ahead and create our own template.

99
00:06:41,570 --> 00:06:42,920
So let's do that.

100
00:06:42,920 --> 00:06:50,870
So let's go back here, create a new template, call us password, underscore, change, underscore,

101
00:06:50,870 --> 00:06:52,490
done, dot, HTML.

102
00:06:52,970 --> 00:06:56,290
And in here we want to extend from the base template.

103
00:06:56,300 --> 00:06:59,570
So in order to keep things simple, I'll go ahead.

104
00:07:00,850 --> 00:07:04,150
And use the code which we have up over here.

105
00:07:05,940 --> 00:07:07,710
Which is in the index page.

106
00:07:07,710 --> 00:07:09,030
So I'll copy this.

107
00:07:09,030 --> 00:07:11,190
I'll go to the password change.

108
00:07:11,190 --> 00:07:11,730
Done.

109
00:07:11,880 --> 00:07:14,910
We sit up over here and I'll simply say.

110
00:07:16,570 --> 00:07:21,870
Password change successfully.

111
00:07:22,360 --> 00:07:23,230
Save this.

112
00:07:23,230 --> 00:07:28,720
And now, once we have created this particular template, we now have to pass this template to the View.

113
00:07:28,720 --> 00:07:32,080
And the view is actually defined in the URL start profile.

114
00:07:32,140 --> 00:07:35,410
So let's go to the URL start by file up over here.

115
00:07:35,560 --> 00:07:38,530
And here I have to pass in the template name.

116
00:07:39,820 --> 00:07:42,160
So template name has nothing but

117
00:07:44,470 --> 00:07:51,060
users forward slash password change don dot html.

118
00:07:51,070 --> 00:07:54,030
So if I do that, let's see if that functionality works.

119
00:07:54,040 --> 00:07:56,920
So here I'm actually onto the password change.

120
00:07:56,920 --> 00:07:59,020
So let me type in the old password.

121
00:08:04,980 --> 00:08:10,260
And let me type in the new password as super user.

122
00:08:11,240 --> 00:08:12,320
Three, two, one.

123
00:08:13,400 --> 00:08:16,760
Super user Three, two, one.

124
00:08:16,760 --> 00:08:22,640
And now if I click on submit, as you can see now, instead of getting the by default page from Django

125
00:08:22,640 --> 00:08:27,410
Admin, we instead get our very own custom template, which we have defined up over here.

126
00:08:27,410 --> 00:08:30,650
So this is how the password change functionality works.

127
00:08:30,920 --> 00:08:34,760
So that's it for this lecture and I'll see you guys in the next one.

128
00:08:34,909 --> 00:08:35,720
Thank you.

