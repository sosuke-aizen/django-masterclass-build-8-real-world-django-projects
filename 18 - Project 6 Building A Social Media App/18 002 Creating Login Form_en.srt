1
00:00:00,210 --> 00:00:04,910
So in this lecture, let's go ahead and start building the authentication functionality.

2
00:00:04,920 --> 00:00:09,800
So let's first start off by building the login functionality for our application.

3
00:00:09,810 --> 00:00:14,730
So in order to create the login functionality, obviously we will be using the user's app.

4
00:00:14,730 --> 00:00:19,690
And here in order to build that functionality, first of all, we need to build a form.

5
00:00:19,710 --> 00:00:23,130
Now there are multiple ways to build a log in functionality.

6
00:00:23,140 --> 00:00:25,230
One is you could create forms here.

7
00:00:25,230 --> 00:00:30,930
The other way is that you could manually create HTML forms and then submit the data using the post method.

8
00:00:30,930 --> 00:00:35,940
Or you could also use the built in authentication use, which comes with Django.

9
00:00:35,970 --> 00:00:41,520
However, here I will try a different approaches and see what works best for us.

10
00:00:41,520 --> 00:00:47,310
So the first approach which we are going to try is to create a login form using a simple form.

11
00:00:47,370 --> 00:00:53,100
So whenever you have to create forms in Django, you simply create a file called as Form start P, and

12
00:00:53,100 --> 00:00:59,880
you just create a form by defining your form in terms of a class and then specify the fields which you

13
00:00:59,880 --> 00:01:01,320
want in that particular form.

14
00:01:01,560 --> 00:01:03,930
So we are going to do exactly that.

15
00:01:03,930 --> 00:01:09,810
So in order to create a login form, first of all, we will create a file class form start pie and this

16
00:01:09,810 --> 00:01:10,800
user app.

17
00:01:11,070 --> 00:01:15,720
So let's go ahead, create a new file and say form start pie.

18
00:01:15,930 --> 00:01:21,780
So all the forms which you will be requiring for this particular user's app, you could actually create

19
00:01:21,780 --> 00:01:22,740
them over here.

20
00:01:22,950 --> 00:01:28,190
So as we need a login form here, we'll actually go ahead and start building that right away.

21
00:01:28,200 --> 00:01:31,080
So first of all, I'll import forms.

22
00:01:31,080 --> 00:01:36,780
So here I would say from Django import forms.

23
00:01:36,960 --> 00:01:37,320
Okay.

24
00:01:37,320 --> 00:01:42,090
So once we have imported that, it's showing us a warning because we have not yet used it.

25
00:01:42,180 --> 00:01:44,250
So let's go ahead and use that form.

26
00:01:44,250 --> 00:01:49,830
So here, in order to create a login form, I'll create a class and let's create a form called Last

27
00:01:49,860 --> 00:01:51,060
Login form.

28
00:01:51,480 --> 00:01:53,880
And here let's import from forms.

29
00:01:53,880 --> 00:01:56,790
So I would say form, start form.

30
00:01:57,150 --> 00:02:01,350
So this is actually going to inherit from the base form, which we have.

31
00:02:01,650 --> 00:02:07,680
And here, let's say in this particular form field, we want to have a username and password field.

32
00:02:07,680 --> 00:02:14,490
So I would say username and this is going to be form start that's going to be character filled because

33
00:02:14,490 --> 00:02:19,220
the username of a particular user is going to express the in terms of characters.

34
00:02:19,230 --> 00:02:21,420
Now let's create a password field as well.

35
00:02:21,420 --> 00:02:27,620
So I would say password equals that's also going to be form start character field as well.

36
00:02:27,630 --> 00:02:32,100
But now the widget which will be using for this is going to be the password widget.

37
00:02:32,100 --> 00:02:34,740
So here I will explicitly mention that.

38
00:02:34,800 --> 00:02:41,490
So here I would say widget equals that's going to be forms dot password input.

39
00:02:41,610 --> 00:02:46,320
So what this does is that it's actually going to mask all the characters which are going to be typed

40
00:02:46,320 --> 00:02:47,580
in that particular form.

41
00:02:47,580 --> 00:02:50,010
And still we are getting this particular warning.

42
00:02:50,010 --> 00:02:51,930
It says Django import.

43
00:02:51,930 --> 00:02:54,780
Django could not be resolved from Pinelands.

44
00:02:54,780 --> 00:02:56,670
So let's fix that as well.

45
00:02:57,650 --> 00:03:03,020
So now this particular issue, which we have, could be fixed in multiple ways, but I want to show

46
00:03:03,020 --> 00:03:09,680
you why exactly this issue happens and how to fix it once and for all so that you don't face such issues

47
00:03:09,680 --> 00:03:13,340
whenever you are working with certain Python projects and terminal.

48
00:03:13,520 --> 00:03:19,820
So first of all, whenever you are running Django applications at all, it's always actually better

49
00:03:19,820 --> 00:03:21,260
to use the built in terminal.

50
00:03:21,260 --> 00:03:27,260
But let's say if you're using an external terminal like I am over here, what you could do is that right

51
00:03:27,260 --> 00:03:31,640
now we have opened up the social project, which is our Django project.

52
00:03:31,640 --> 00:03:36,800
But instead of opening up this particular project folder, what you do is that you instead open up the

53
00:03:36,800 --> 00:03:41,870
main container folder, which we have, which actually includes the virtual environment.

54
00:03:41,870 --> 00:03:47,810
So right now the problem is that the version of Python or the Python interpreter, which we are using

55
00:03:47,810 --> 00:03:53,930
in our terminal to run our Django app and the project interpreter or Python interpreter, which is code

56
00:03:53,930 --> 00:03:58,340
is using, they are actually different and that's what causes this particular issue.

57
00:03:58,580 --> 00:04:07,100
So here, if I simply go ahead, open up a folder and instead of opening up the social project instead,

58
00:04:07,100 --> 00:04:12,530
if I open up the social project, which actually has our virtual environment, let's see what happens.

59
00:04:12,680 --> 00:04:17,300
Now, if I open this thing up, it's going to load up the virtual environment up over here.

60
00:04:17,300 --> 00:04:23,000
So as you can see, we have this env folder as well and now we as code is automatically going to pick

61
00:04:23,000 --> 00:04:25,850
up the python part for the virtual environment.

62
00:04:25,850 --> 00:04:31,520
So as you can see here, it says the python interpreter, which we are currently using, is actually

63
00:04:31,520 --> 00:04:33,590
present in the dot and V folder.

64
00:04:34,220 --> 00:04:38,890
So once we are done with this, we are pretty much good to go and the issue is fixed.

65
00:04:38,900 --> 00:04:44,480
So now once this form is created from the next lecture onwards, let's go ahead and let's render out

66
00:04:44,480 --> 00:04:46,370
this form by creating a view.

67
00:04:46,550 --> 00:04:50,720
So thank you very much for watching and I'll see you guys in the next one.

68
00:04:50,810 --> 00:04:51,470
Thank you.

