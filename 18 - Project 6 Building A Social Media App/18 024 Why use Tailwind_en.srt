1
00:00:00,030 --> 00:00:03,590
So in this particular lecture, let's work on styling up our web page.

2
00:00:03,600 --> 00:00:08,820
So in order to style up our web page, the very first thing which we are going to be doing here is that

3
00:00:08,820 --> 00:00:11,030
we will be using Dillwyn, CSS.

4
00:00:11,040 --> 00:00:16,219
So we all know that we could add CSS to our website so as to make it look attractive.

5
00:00:16,230 --> 00:00:23,730
But the problem with CSS is that CSS is pretty hard to master and obviously if you are not familiar

6
00:00:23,730 --> 00:00:25,920
with CSS, it can get quite tedious.

7
00:00:25,920 --> 00:00:29,240
You need to add a couple of classes, so on and so forth.

8
00:00:29,250 --> 00:00:33,270
You also need to set a different set of properties as well for different elements.

9
00:00:33,270 --> 00:00:39,660
But with tailwind, especially when you are using Tailwind with something like React or Django, whenever

10
00:00:39,660 --> 00:00:42,960
you have to design components or whenever you have to create templates.

11
00:00:42,960 --> 00:00:50,130
So in Django right now we are working with templates and let's say you only want to have inline styling

12
00:00:50,130 --> 00:00:56,460
and what do I mean by inline styling is that let's say you only want to write CSS or you only want to

13
00:00:56,460 --> 00:01:00,720
design your entire web page by adding some code to the template.

14
00:01:00,720 --> 00:01:04,019
And let's say you don't have to work with an external CSS file.

15
00:01:04,170 --> 00:01:07,510
In such cases, using tailwind is extremely crucial.

16
00:01:07,530 --> 00:01:13,230
Now, I know that this is not a one specific course, but I think tailwind is a must even for back end

17
00:01:13,230 --> 00:01:19,380
developers if they are working with Django, especially the Django templates, because obviously if

18
00:01:19,380 --> 00:01:23,570
you design a Django website and if you do not style it, it's going to look something like that.

19
00:01:23,580 --> 00:01:29,370
And the best part about Tailwind is that you don't need to put as much effort in into learning tailwind

20
00:01:29,400 --> 00:01:33,210
as opposed to whatever is required for learning CSS.

21
00:01:33,210 --> 00:01:36,900
And therefore we are going to be using tailwind throughout the course.

22
00:01:36,900 --> 00:01:40,130
And unlike other courses, I am not going to skip that part.

23
00:01:40,140 --> 00:01:45,150
I'm actually going to cover tailwind in a little bit detail so that even you could get started with

24
00:01:45,150 --> 00:01:46,650
Tailwind and start using it.

25
00:01:46,650 --> 00:01:51,150
And one of the best things about Tailwind is that you don't have to be a frontend expert.

26
00:01:51,150 --> 00:01:56,790
You could just use some of the tailwind classes, add them inline to your HTML templates right away

27
00:01:56,790 --> 00:01:58,140
and you would be up and running.

28
00:01:58,140 --> 00:02:02,550
So this is the reason why I recommend that you go through this particular section, even if you don't

29
00:02:02,550 --> 00:02:09,570
feel like it and you will be familiar with Tailwind in no time and you will be surprised to see how

30
00:02:09,570 --> 00:02:12,210
easier it is as compared to regular CSS.

31
00:02:12,210 --> 00:02:17,730
So from the next lecture onwards, let's go ahead and let's start setting up tailwind for our Django

32
00:02:17,730 --> 00:02:20,920
project and it's much more easier than you think.

33
00:02:20,940 --> 00:02:24,610
So that's it for this lecture, and I'll see you guys in the next one.

34
00:02:24,630 --> 00:02:25,200
Thank you.

