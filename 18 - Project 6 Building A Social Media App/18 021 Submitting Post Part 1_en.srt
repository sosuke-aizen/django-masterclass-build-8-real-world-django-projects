1
00:00:00,090 --> 00:00:03,930
Now let's work on creating this create HTML template.

2
00:00:03,930 --> 00:00:07,360
So we want to create that in the POSTS app, which we have.

3
00:00:07,380 --> 00:00:11,610
So in the Post app, let's create a new folder called as templates.

4
00:00:12,300 --> 00:00:17,970
And inside the templates folder, let's create another folder with the same name, which is posts.

5
00:00:18,750 --> 00:00:23,730
Once this is created now inside the Post app, I have to create that particular template, which is

6
00:00:23,730 --> 00:00:25,020
the actual template.

7
00:00:25,380 --> 00:00:27,060
Create dot html.

8
00:00:27,690 --> 00:00:28,110
Okay.

9
00:00:28,110 --> 00:00:33,720
So now this particular template also needs to inherit from the base template which we have in our users

10
00:00:33,720 --> 00:00:34,170
app.

11
00:00:34,230 --> 00:00:39,000
So in the users app, if we go to the templates, this is the template we want to inherit from.

12
00:00:39,180 --> 00:00:44,010
And as you can see, while inheriting from that template, we write this particular code.

13
00:00:44,040 --> 00:00:47,500
So that's exactly the thing which we are going to do over here as well.

14
00:00:47,520 --> 00:00:52,410
So in order to save time, you could actually take any form which you already have from here.

15
00:00:52,650 --> 00:00:58,770
So let's take a look at the register form so I could simply take up the register form from here, from

16
00:00:58,770 --> 00:01:00,250
registered HTML.

17
00:01:00,930 --> 00:01:07,920
Take that particular code, go to post, go to create dot HTML, simply pose that thing up over here.

18
00:01:08,190 --> 00:01:11,280
So this is also going to extend from the base template.

19
00:01:11,430 --> 00:01:13,110
It's going to have a blog body.

20
00:01:13,140 --> 00:01:15,840
It's going to have a CSR of token.

21
00:01:16,020 --> 00:01:21,750
But the only thing is, instead of user form, we simply have to say a form as P, and we also have

22
00:01:21,750 --> 00:01:23,190
the submit button as well.

23
00:01:23,430 --> 00:01:27,510
And instead of saying register, I would simply say post up over here.

24
00:01:27,690 --> 00:01:34,830
And also, as this particular post needs to post some image data, we have to set the entry type of

25
00:01:34,830 --> 00:01:37,470
this thing to multi part slash form data.

26
00:01:38,010 --> 00:01:43,770
Once that thing is done, we are pretty much good to go and the form will now be rendered without any

27
00:01:43,770 --> 00:01:50,010
issues because the form is actually passed as a context here and that form is simply being rendered

28
00:01:50,010 --> 00:01:50,460
here.

29
00:01:50,790 --> 00:01:56,270
Now the only thing which is remaining is to associate this particular view with the URL pattern.

30
00:01:56,280 --> 00:02:02,550
But even before that, you need to remember that while creating a post, only the users who are logged

31
00:02:02,550 --> 00:02:04,140
in should be able to create a post.

32
00:02:04,170 --> 00:02:07,260
That means now we need to log in, protect this root as well.

33
00:02:07,380 --> 00:02:11,550
And therefore we add a login required decorator here.

34
00:02:11,550 --> 00:02:19,950
So add login underscore required and we need to import that from Django dot dot dot decorators.

35
00:02:19,950 --> 00:02:29,670
So here from django dot contrib dot auth dot decorators import the login required decorator.

36
00:02:29,700 --> 00:02:36,660
Now let's go ahead and associate this thing with a URL pattern and remember that as we have created

37
00:02:36,660 --> 00:02:41,730
a new app here, we actually need to create a new url start py file in the post app.

38
00:02:42,030 --> 00:02:45,890
So let's go ahead, create a new file, call it as url start p.

39
00:02:45,890 --> 00:02:46,290
Why?

40
00:02:47,130 --> 00:02:53,820
Let's associate this particular URL dot py with the main URL stop by which we have in our main project.

41
00:02:54,090 --> 00:02:55,680
So let's go to the main project.

42
00:02:55,680 --> 00:03:02,580
So stop by and in here, just as we have included the URLs for the users, I would simply copy the same

43
00:03:02,580 --> 00:03:05,130
thing pasted up over here.

44
00:03:05,340 --> 00:03:13,530
And instead of users I will now see posts and I would say include the posts dot URLs.

45
00:03:13,800 --> 00:03:16,930
Once this thing is done, we are now pretty much good to go.

46
00:03:16,950 --> 00:03:22,980
Now, after that we now need to add some code to the URLs dot py of the post and for that you could

47
00:03:22,980 --> 00:03:28,110
actually copy the same code template which you have in your users app.

48
00:03:28,380 --> 00:03:30,690
So you could simply copy this.

49
00:03:30,720 --> 00:03:32,130
Go back to the.

50
00:03:33,960 --> 00:03:35,100
Bootstrap.

51
00:03:36,000 --> 00:03:38,550
Go to the URLs PWA of the posts app.

52
00:03:38,580 --> 00:03:39,840
Paste this thing up.

53
00:03:40,230 --> 00:03:44,910
And right now, instead of having so many paths, let's delete all of them except for one.

54
00:03:45,030 --> 00:03:52,640
So that we could modify that as per our own view and also remove the other imports which are not required.

55
00:03:52,650 --> 00:03:57,000
And in here we now have to associate the view.

56
00:03:59,670 --> 00:04:02,190
So let me close the rest of the things.

57
00:04:02,430 --> 00:04:06,480
So here we have to associate a view which is post, underscore, create.

58
00:04:06,600 --> 00:04:13,320
So here I would say views, dot, post, underscore, create, and let's name this thing as create.

59
00:04:13,530 --> 00:04:16,829
And also let's have a path for create.

60
00:04:16,829 --> 00:04:18,959
So let's say this thing C is create.

61
00:04:19,209 --> 00:04:19,640
Okay.

62
00:04:19,649 --> 00:04:21,240
So once this thing is done.

63
00:04:21,510 --> 00:04:26,660
Now whenever we visit posts forward, slash, create, we should be able to render this view.

64
00:04:26,670 --> 00:04:31,770
So in the next lecture, let's go ahead and let's test out this functionality and see if everything

65
00:04:31,770 --> 00:04:32,550
works well.

66
00:04:32,610 --> 00:04:36,390
So thank you very much for watching and I'll see you guys in the next one.

67
00:04:36,390 --> 00:04:36,860
Thank you.

