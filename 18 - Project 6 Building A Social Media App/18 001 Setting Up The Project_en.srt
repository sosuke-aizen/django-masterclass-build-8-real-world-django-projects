1
00:00:00,330 --> 00:00:06,730
So in this particular lecture, let's go ahead and let's get started with the Social Media Django Project.

2
00:00:06,750 --> 00:00:12,420
So first of all, in this lecture, we are going to set up a Django project by creating a virtual environment,

3
00:00:12,420 --> 00:00:15,310
installing Django, so on and so forth.

4
00:00:15,330 --> 00:00:18,260
So let's first go ahead and go to desktop.

5
00:00:18,270 --> 00:00:24,420
So I would go to desktop and here I've actually created a folder which is called as a social.

6
00:00:24,420 --> 00:00:30,720
So I would go to the social folder and here is where we will set up the virtual environment.

7
00:00:30,720 --> 00:00:35,850
So in order to set up virtual environment, first of all, and show that you already have a virtual

8
00:00:35,850 --> 00:00:36,900
enemy installed.

9
00:00:36,900 --> 00:00:44,670
So if you don't have virtually and we install you simply type in PIP and install virtual E and V, but

10
00:00:44,670 --> 00:00:49,160
since I already have that installed, I'm going to skip that step.

11
00:00:49,170 --> 00:00:57,120
So in order to create a virtual environment here, I would say virtual and v, e and V, once virtual

12
00:00:57,120 --> 00:00:59,970
environment is created, I would go into that environment.

13
00:00:59,970 --> 00:01:04,980
So I would say CD, E and V, and here I would activate that environment.

14
00:01:04,980 --> 00:01:08,760
So source been forward, slash, activate.

15
00:01:09,900 --> 00:01:12,810
Okay, so now the virtual environment is activated.

16
00:01:12,810 --> 00:01:14,460
So let me clear the screen.

17
00:01:14,610 --> 00:01:20,250
And here, once the virtual environment is activated, I have to go back to the directory, which is

18
00:01:20,250 --> 00:01:21,210
the social.

19
00:01:21,210 --> 00:01:25,250
So here I would go to CD, dot, dot.

20
00:01:25,680 --> 00:01:27,890
Now I'm back into the social directory.

21
00:01:27,900 --> 00:01:32,940
Now the social directory is actually going to act as a container for our Django project.

22
00:01:32,940 --> 00:01:37,950
And here in this particular project, container is where we will create our actual project.

23
00:01:38,070 --> 00:01:41,970
But even before that, we first have to go ahead and install Django.

24
00:01:41,970 --> 00:01:48,990
So I would say PIP install Django, that's going to go ahead and set up an install Django for us.

25
00:01:49,530 --> 00:01:54,120
So once a Django is installed now, we could go ahead, create a project.

26
00:01:54,120 --> 00:02:00,510
So again, clear up the screen and in order to create a Django project, I would say Django Dash admin

27
00:02:00,930 --> 00:02:02,900
start project.

28
00:02:02,910 --> 00:02:08,669
And here I'm actually going to name this thing as social project.

29
00:02:09,120 --> 00:02:16,050
So I'm specifically naming this particular project as social and the name as project, because this

30
00:02:16,050 --> 00:02:22,710
project is going to contain multiple small apps, which is going to together comprise of a single huge

31
00:02:22,710 --> 00:02:23,340
project.

32
00:02:23,340 --> 00:02:29,730
So in order to differentiate the project folder, well, I'm actually going to name this thing as social

33
00:02:29,730 --> 00:02:30,330
project.

34
00:02:30,480 --> 00:02:35,280
Now let's hit enter and that's actually going to go ahead and create a project for us.

35
00:02:35,430 --> 00:02:41,640
So now once that particular project is created, let's go ahead and open up that particular project

36
00:02:41,640 --> 00:02:42,680
in VTS code.

37
00:02:42,690 --> 00:02:46,100
So here I've opened up the project, which is social project.

38
00:02:46,110 --> 00:02:52,440
Here we have a managed dot by file, and now once we have this particular project set up, our next

39
00:02:52,440 --> 00:02:56,040
job is to actually go ahead and create a new app in there.

40
00:02:56,220 --> 00:03:01,920
So for this particular project, what we will be doing is that first of all, we will be designing the

41
00:03:01,920 --> 00:03:04,950
authentication functionality for our application.

42
00:03:04,950 --> 00:03:10,710
And the reason why we are doing this is because let's say in future, if you want to develop any kind

43
00:03:10,710 --> 00:03:17,490
of application, obviously any web app would require authentication, which is log in, log out, so

44
00:03:17,490 --> 00:03:18,420
on and so forth.

45
00:03:18,420 --> 00:03:24,450
So we are going to design that functionality first, create the basic login, log out functionality

46
00:03:24,450 --> 00:03:26,760
and build an app on top of it.

47
00:03:26,760 --> 00:03:32,520
So what that allows you to do is that it allows you to use this particular template for developing any

48
00:03:32,520 --> 00:03:37,470
kind of project which you want and you don't have to repeat all of those authentication steps.

49
00:03:37,470 --> 00:03:41,430
So now let's go ahead and let's start building that authentication app.

50
00:03:41,430 --> 00:03:43,300
So we'll go back to the terminal.

51
00:03:43,320 --> 00:03:47,780
So here, let's actually create a new app within this social project.

52
00:03:47,790 --> 00:03:50,730
So first of all, you have to go into the social project.

53
00:03:50,970 --> 00:03:53,940
So CDD Social project.

54
00:03:54,120 --> 00:03:58,200
And in here let's create an app called as Users.

55
00:03:58,200 --> 00:04:07,230
So here I would say Django Dash admin Start app that's going to be users and that's actually going to

56
00:04:07,230 --> 00:04:09,930
set up a user's app and our main project.

57
00:04:09,930 --> 00:04:14,430
So as you can see now, we have this user app with a whole bunch of files in there.

58
00:04:14,640 --> 00:04:20,550
So now once this Django app is created, we simply have to go ahead and include this particular app

59
00:04:20,550 --> 00:04:23,490
inside the settings start by of our main project.

60
00:04:23,640 --> 00:04:25,080
So let's go in there.

61
00:04:25,200 --> 00:04:28,380
And over here I simply have to add.

62
00:04:29,670 --> 00:04:32,810
The app name over here to install the apps.

63
00:04:32,820 --> 00:04:38,460
So here I would say users give a comma and we should be good to go.

64
00:04:38,640 --> 00:04:44,280
So once we have added the users app in there, the next thing that needs to be done is that we need

65
00:04:44,280 --> 00:04:47,060
to apply certain migrations.

66
00:04:47,070 --> 00:04:57,900
So in order to apply those migrations we say python manage to be bi migrate, hit enter and that's going

67
00:04:57,900 --> 00:04:59,940
to apply all the migrations for us.

68
00:04:59,940 --> 00:05:05,720
And now once that thing is done, let's actually try running our Django app and see if that works.

69
00:05:05,730 --> 00:05:11,820
So here I would say Python manage dot by run server.

70
00:05:12,690 --> 00:05:15,300
So now the server is up and running on this port.

71
00:05:15,300 --> 00:05:17,820
So let me open up this particular URL.

72
00:05:18,330 --> 00:05:22,620
So I'll copy this, open up a web browser and paste it in there.

73
00:05:22,890 --> 00:05:23,280
Okay.

74
00:05:23,280 --> 00:05:28,980
So as you can see, I've actually open up the app here and the app is up and running, which means that

75
00:05:28,980 --> 00:05:33,560
we have successfully set up Django Virtual environment, set up the project as well.

76
00:05:33,570 --> 00:05:39,630
So now from the next lecture onwards, let's go ahead and let's start designing the login functionality

77
00:05:39,630 --> 00:05:42,120
inside our users app.

78
00:05:42,180 --> 00:05:45,090
So we are going to start doing that from the next lecture.

79
00:05:45,090 --> 00:05:49,270
So thank you very much for watching and I'll see you guys in the next one.

80
00:05:49,290 --> 00:05:50,040
Thank you.

