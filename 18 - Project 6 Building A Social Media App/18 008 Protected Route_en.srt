1
00:00:00,090 --> 00:00:05,520
So in this particular lecture, let's actually create a home page and let's extend that home page from

2
00:00:05,520 --> 00:00:07,050
the base template, which we have.

3
00:00:07,320 --> 00:00:13,280
So a home page is nothing, but it's basically the page we should get when we visit forward slash users.

4
00:00:13,290 --> 00:00:18,390
So right now, if you visit users, as you can see, there's no URL pattern defined for it.

5
00:00:18,390 --> 00:00:22,380
And we want to make this thing sort of a home page for the user's app.

6
00:00:22,530 --> 00:00:26,070
So in order to set it up, first of all, let's go back to VTS code.

7
00:00:26,190 --> 00:00:33,240
And here, in order to actually create a particular page, I'll go to the templates and here I'll create

8
00:00:33,240 --> 00:00:36,240
a new file and call it as index dot HTML.

9
00:00:36,750 --> 00:00:41,970
And now this index dot HTML is actually going to extend from the base template, which we have just

10
00:00:41,970 --> 00:00:50,160
created because this index template should actually have this thing as the navigation bar of that template.

11
00:00:50,250 --> 00:00:53,490
So here I'll make this thing extend from the base template.

12
00:00:53,490 --> 00:01:00,660
So I would say extends from nothing but users forward slash base dot HTML.

13
00:01:01,260 --> 00:01:06,320
So once this extension is made, now we could start adding the code to this index page.

14
00:01:06,330 --> 00:01:12,090
So this index page, let's say we want to simply add a simple line like this is the home page.

15
00:01:12,090 --> 00:01:18,960
So here I will add the block body tag, which means that the block or the body of this particular template

16
00:01:18,960 --> 00:01:22,170
starts from here and end block means it ends here.

17
00:01:22,620 --> 00:01:31,410
So here I'll add a simple H one tag and call it as home page for users app.

18
00:01:31,410 --> 00:01:34,890
So now we need to render this template on the basis of a view.

19
00:01:34,890 --> 00:01:40,470
So let's go to the view start by file, which is this file right here and up over here.

20
00:01:40,470 --> 00:01:44,160
I want to create a view, so I'll call this view as index.

21
00:01:44,280 --> 00:01:53,610
So I would see def index as usual, it's going to accept a request and this is going to simply return

22
00:01:53,610 --> 00:01:55,800
and render the template which we have.

23
00:01:55,920 --> 00:02:02,070
So this is obviously going to accept the request object and also it's actually going to go ahead and

24
00:02:02,070 --> 00:02:10,229
render the template, which is users forward slash, which is nothing but index dot HTML.

25
00:02:10,949 --> 00:02:13,800
Once this thing is done, we are pretty much good to go.

26
00:02:13,800 --> 00:02:17,310
Now let's associate this particular view with a URL pattern.

27
00:02:17,580 --> 00:02:22,950
So we want to associate this with a URL pattern, which is nothing but just forward slash.

28
00:02:22,950 --> 00:02:28,290
And that's because if you take a look at this, this is the URL pattern we are targeting for.

29
00:02:28,320 --> 00:02:34,470
So let's go back to vsco, go to the URL start poi file, let's create a path.

30
00:02:34,920 --> 00:02:39,540
And here I would say the patch should be empty and the view should be indexed.

31
00:02:39,540 --> 00:02:45,800
So view start index and I would say the name should be also index.

32
00:02:45,810 --> 00:02:48,570
Give a comma at the end and we should be good.

33
00:02:48,930 --> 00:02:50,700
So let's see if that works.

34
00:02:50,710 --> 00:02:58,500
So if I go back here and if I visit this, it sees template does not exist at users, which is user

35
00:02:58,500 --> 00:03:00,390
forward slash base dot HTML.

36
00:03:00,390 --> 00:03:06,870
And this is because if you go to this template right here, we have to extend from users, not user.

37
00:03:07,050 --> 00:03:12,900
If I do that, hit refresh, as you can see, it says home page for users app.

38
00:03:12,900 --> 00:03:17,910
And you'll also be able to notice that here it will say login because currently the user is logged out.

39
00:03:17,910 --> 00:03:22,830
So if I log in now, let's try logging in with the super user credentials.

40
00:03:22,830 --> 00:03:27,210
If I had submit, as you can see it, these user authenticated and logged in.

41
00:03:27,450 --> 00:03:33,450
And now if I try visiting this homepage right here, as you can see, as the user is now logged in,

42
00:03:33,720 --> 00:03:39,390
it will say log out and when I click this, it will basically go ahead and log the user out.

43
00:03:39,390 --> 00:03:45,090
And now again, if I visit the home page, you'll be able to see that it will again say login here,

44
00:03:45,090 --> 00:03:49,080
which means that the login and log out functionality is completed.

45
00:03:49,740 --> 00:03:55,980
Now, one more thing which we could do here is that let's say we have this home page and we want the

46
00:03:55,980 --> 00:04:00,180
user to be able to access this home page only when the user is logged in.

47
00:04:00,180 --> 00:04:04,680
In that particular case, we need to add a login required decorator for that.

48
00:04:05,100 --> 00:04:07,260
So let's go back to the.

49
00:04:08,400 --> 00:04:10,740
View here, which is the index view.

50
00:04:10,740 --> 00:04:14,560
And let's say we want to protect this view with the login required decorator.

51
00:04:14,580 --> 00:04:17,250
So first of all, we need to import that decorator.

52
00:04:17,310 --> 00:04:27,240
So in order to import that you would say from django dot contrib dot auth dot decorators and import

53
00:04:27,240 --> 00:04:32,690
the log required decorator and once we have imported that we simply have to add it up over here.

54
00:04:32,700 --> 00:04:36,000
So I would say add login required.

55
00:04:37,350 --> 00:04:37,710
Okay.

56
00:04:37,710 --> 00:04:41,820
So now what this does is that it will protect this root with a login.

57
00:04:41,940 --> 00:04:49,230
And now if I go back up over here, if I try accessing this page, as you can see, it will not allow

58
00:04:49,230 --> 00:04:52,130
us to access that unless and until we are actually logged in.

59
00:04:52,140 --> 00:04:57,630
But now the problem is that instead of redirecting us to the login page, it's actually redirecting

60
00:04:57,630 --> 00:05:00,600
us to some random page up over here.

61
00:05:00,600 --> 00:05:05,720
And this is because we have to actually set up the login pads and the log out pads.

62
00:05:05,730 --> 00:05:10,980
And in order to set those parts up, I have to go back to the settings dot UI file.

63
00:05:12,440 --> 00:05:13,730
Of our main app.

64
00:05:13,730 --> 00:05:18,410
And here I need to clearly define the paths for log in and log out.

65
00:05:18,650 --> 00:05:22,540
So here, let's go at the very bottom and define those paths.

66
00:05:22,550 --> 00:05:31,910
So I would say the login you URL, I want the URL to be nothing but login, which is basically present

67
00:05:31,910 --> 00:05:33,050
in the user's app.

68
00:05:34,410 --> 00:05:42,430
And then I would say the logout and the school URL should be cool to log out once this thing is done.

69
00:05:42,450 --> 00:05:43,980
We are pretty much good to go.

70
00:05:44,010 --> 00:05:49,140
So now let's try going to use us.

71
00:05:49,500 --> 00:05:54,990
And now, as you can see, if I try accessing users instead of giving me that error page, now we are

72
00:05:54,990 --> 00:05:59,470
getting this proper login page because we have set up this login URL right here.

73
00:05:59,490 --> 00:06:01,970
And now let's see what happens when I try to log in.

74
00:06:01,980 --> 00:06:07,230
So when I log in, it basically says user authenticated and login.

75
00:06:07,230 --> 00:06:11,630
So that means the login and the log out functionality is working pretty well.

76
00:06:11,640 --> 00:06:17,640
And in the upcoming lecture, let's go ahead and let's create a functionality to be able to change passwords.

77
00:06:17,640 --> 00:06:22,770
So let's say for some reason, if the user has to change their password in that particular case, we

78
00:06:22,770 --> 00:06:24,420
have to provide them that feature.

79
00:06:24,420 --> 00:06:26,460
So let's implement that in the next one.

