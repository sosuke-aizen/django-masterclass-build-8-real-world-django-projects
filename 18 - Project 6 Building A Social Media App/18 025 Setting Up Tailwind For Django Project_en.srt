1
00:00:00,090 --> 00:00:05,400
So in this particular lecture, let's go ahead and let's learn how you could set up tailwind for your

2
00:00:05,430 --> 00:00:06,430
Django project.

3
00:00:06,450 --> 00:00:12,030
So in order to set up tailwind for Django, first of all, you need to make sure that you have installed

4
00:00:12,030 --> 00:00:14,250
tailwind in your project folder.

5
00:00:14,340 --> 00:00:18,990
So in order to do that, first of all, you need to make sure that your computer has node.

6
00:00:19,110 --> 00:00:21,530
So simply go to Node.js dot org.

7
00:00:21,540 --> 00:00:26,970
If you don't have no JS, simply download the Ltsc version, which is the long term supported version.

8
00:00:26,970 --> 00:00:32,549
And the reason why we need to download Node is because we need to use the node package manager to be

9
00:00:32,549 --> 00:00:34,980
able to install tailwind for our project.

10
00:00:35,010 --> 00:00:39,540
Now, once node is installed, the installation setup for node is quite simple.

11
00:00:39,540 --> 00:00:45,180
You simply have to download this, go through the installation wizard and once the installation is complete

12
00:00:45,210 --> 00:00:48,450
now you could actually use NPM to install tailwind.

13
00:00:48,900 --> 00:00:52,590
Now let's go ahead and let's set up tailwind for our project.

14
00:00:52,590 --> 00:00:54,240
So I'll open up VTS code here.

15
00:00:54,450 --> 00:00:59,460
And the very first thing which you need to understand here is you need to understand the project structure.

16
00:00:59,550 --> 00:01:04,050
So right now we have the project, which is social project.

17
00:01:04,050 --> 00:01:08,610
And what you need to do is that you need to go into this particular project directory.

18
00:01:08,620 --> 00:01:14,520
So in order to go up over here, I'll open up the terminal and let's stop the server and let me show

19
00:01:14,520 --> 00:01:16,340
you the exact path where you need to go.

20
00:01:16,350 --> 00:01:19,380
So here, let me clear up the screen.

21
00:01:19,380 --> 00:01:24,090
And right now, as you can see, we are into the social project, which is nothing but the container

22
00:01:24,090 --> 00:01:25,080
of our project.

23
00:01:25,200 --> 00:01:27,670
And that's where exactly you need to go.

24
00:01:27,690 --> 00:01:30,930
So if you type an LZ here, you should be able to see all the apps.

25
00:01:30,930 --> 00:01:34,230
So here, as you can see, you are able to see posts and users.

26
00:01:34,230 --> 00:01:36,570
So here is where exactly you need to go.

27
00:01:36,570 --> 00:01:39,630
And once you are in there, this is where you want to install tailwind.

28
00:01:39,630 --> 00:01:45,240
But even before installing tailwind, we have to set up a package start JSON file which is going to

29
00:01:45,240 --> 00:01:49,230
contain all the node modules which are going to be installed along with Tailwind.

30
00:01:49,230 --> 00:01:55,330
And in order to keep track of those specific files, we need the package to JSON file.

31
00:01:55,350 --> 00:02:02,490
So here, in order to create a package JSON file inside the social project, you would type in npm init

32
00:02:02,730 --> 00:02:03,540
dash y.

33
00:02:03,540 --> 00:02:09,600
So this is going to initialize a package start JSON file and dash y is a flag which is being set so

34
00:02:09,600 --> 00:02:11,310
as to set all the default options.

35
00:02:11,310 --> 00:02:17,760
So hit enter and let's head back to VTS code and here you should be able to see that if I go to social

36
00:02:17,760 --> 00:02:20,790
project, I have now the package JSON file.

37
00:02:21,060 --> 00:02:22,890
So this is what exactly we need.

38
00:02:23,340 --> 00:02:28,860
Now here, if I go to package dot JSON here, you will be able to see the different packages which are

39
00:02:28,860 --> 00:02:29,580
installed.

40
00:02:29,590 --> 00:02:35,490
So currently we do not have any packages installed, but now we need to install tailwind CSS two over

41
00:02:35,490 --> 00:02:35,940
here.

42
00:02:36,270 --> 00:02:38,550
So here let's hit clear.

43
00:02:38,550 --> 00:02:42,390
And the version of Tillman we want to install is tailwind CSS two.

44
00:02:42,510 --> 00:02:51,660
So here I would say NPM install tailwind CSS at 2.2.16.

45
00:02:51,660 --> 00:02:56,820
So make sure that you install the exact version of tailwind which is specified here or else you are

46
00:02:56,820 --> 00:02:58,440
going to face some issues.

47
00:02:58,710 --> 00:03:02,900
So simply hit enter and that's actually going to install tailwind for you.

48
00:03:02,910 --> 00:03:07,530
So it's going to take a while for tailwind to get set up and get installed.

49
00:03:07,530 --> 00:03:12,210
And as you can see now we have Tailwind CSS two added as a dependency here.

50
00:03:12,240 --> 00:03:16,720
Now, once tailwind is installed, let's go ahead and let's get started working with Tailwind.

51
00:03:16,740 --> 00:03:21,300
But before working with Tailwind, you really need to understand the way in which tailwind works.

52
00:03:21,300 --> 00:03:26,520
So the way in which tailwind works is that Tailwind has a bunch of utility classes and now you have

53
00:03:26,520 --> 00:03:29,430
to use this utility classes and you need to compile them.

54
00:03:29,430 --> 00:03:35,730
And when you compile those particular classes, they actually generate a CSS file and that CSS file

55
00:03:35,730 --> 00:03:41,820
has a whole bunch of CSS code with a whole bunch of CSS classes and you simply need to use those classes

56
00:03:41,820 --> 00:03:45,390
and assign it to the HTML elements which you have in your app.

57
00:03:45,390 --> 00:03:51,210
So your app could be a Django app, it could be a simple HTML page, or it could also be a React app,

58
00:03:51,210 --> 00:03:53,280
which contains multiple components.

59
00:03:53,400 --> 00:03:55,290
So let's go ahead and let's do that.

60
00:03:55,290 --> 00:03:58,980
So first of all, you need to understand that we need to have a source file.

61
00:03:59,010 --> 00:04:04,650
We compile that source file and tailwind, and then you get another output file and then you link that

62
00:04:04,650 --> 00:04:07,760
output file to your Django pages, which you have.

63
00:04:07,770 --> 00:04:13,200
So let's first go ahead and let's create a source directory and we are specifically going to create

64
00:04:13,200 --> 00:04:16,170
that particular directory and the users app.

65
00:04:16,170 --> 00:04:21,959
And the reason why we are going to do it in the user's app is because it's the users app, which has

66
00:04:21,959 --> 00:04:24,480
this base template from which we are extending.

67
00:04:24,480 --> 00:04:27,720
So here we will be creating the source folder and the user's app.

68
00:04:27,720 --> 00:04:32,630
So in there you have to go inside users and inside the user's app.

69
00:04:32,640 --> 00:04:35,430
First of all, we need to create a static directory.

70
00:04:35,430 --> 00:04:41,070
And the reason why we need a static directory is because as we are uploading a CSS file in there and

71
00:04:41,070 --> 00:04:44,340
that is a static asset, that means we need to create a static folder.

72
00:04:44,340 --> 00:04:50,700
And so let's go ahead, go inside the user's app, create a new folder called us static.

73
00:04:51,830 --> 00:04:57,620
And in this static directory, let's create another folder called as users.

74
00:04:57,890 --> 00:05:02,720
And in this particular users app is where we are actually going to create that particular the source

75
00:05:02,720 --> 00:05:03,740
file for tailwind.

76
00:05:03,770 --> 00:05:08,710
So let's name this particular source file as p w dot CSS.

77
00:05:08,720 --> 00:05:13,100
So I'll create a new file called it s d w dot CSS.

78
00:05:13,100 --> 00:05:15,080
So this is going to be our source file.

79
00:05:15,170 --> 00:05:18,770
And in the source file, you simply have to write three lines of code.

80
00:05:18,770 --> 00:05:29,420
So you have to write at Tailwind Base, which is the base utility class at Tailwind Utilities, and

81
00:05:29,420 --> 00:05:33,530
then finally add tailwind components.

82
00:05:34,070 --> 00:05:40,850
So once you have these particular base classes, now the tailwind has to build from these particular

83
00:05:40,850 --> 00:05:42,170
classes, which you have.

84
00:05:42,440 --> 00:05:48,380
So now once these classes are done now we actually need to go ahead and generate a dynamic file, which

85
00:05:48,380 --> 00:05:51,740
is going to be a file which contains a whole bunch of CSS code.

86
00:05:51,980 --> 00:05:56,660
And in order to generate that, you need to execute a long and lengthy command.

87
00:05:57,140 --> 00:06:02,870
And if you want to execute that command multiple times, it's actually better to save that command into

88
00:06:02,870 --> 00:06:04,500
something in order to do that.

89
00:06:04,520 --> 00:06:10,250
What we do is we go inside the package dot JSON file, which we have.

90
00:06:10,280 --> 00:06:15,510
Now keep in mind that we have two files here package log, JSON, and package JSON.

91
00:06:15,530 --> 00:06:20,570
So make sure that you don't touch this file and instead only work with package to JSON.

92
00:06:20,840 --> 00:06:26,360
And in here we create a scripts to build the one files from the source file.

93
00:06:26,360 --> 00:06:31,550
So give a comma at the end for the test and we will create a very own script here.

94
00:06:31,550 --> 00:06:33,770
And let's name that script as build.

95
00:06:34,100 --> 00:06:39,830
And once we have this build script, this is basically going to say tail when build.

96
00:06:40,310 --> 00:06:43,580
And then we want to build from the source file, which we have.

97
00:06:43,580 --> 00:06:45,620
And the source file is nothing but.

98
00:06:47,220 --> 00:06:49,610
The file, which is t w dot CSIS.

99
00:06:49,620 --> 00:06:53,550
And here we need to specify the path of this file.

100
00:06:53,550 --> 00:06:56,460
So this file is actually present in our users app.

101
00:06:56,460 --> 00:07:05,580
So I would say users forward slash static forward slash users forward slash D w dot CSS and we want

102
00:07:05,580 --> 00:07:07,380
to generate an output from this file.

103
00:07:07,380 --> 00:07:14,220
So I would say dash O and then we need to specify the source file which needs to be generated.

104
00:07:14,220 --> 00:07:17,820
So we want to generate the source file inside the user's app.

105
00:07:17,970 --> 00:07:24,060
So here I would say users and we want it to be present and static again users.

106
00:07:24,060 --> 00:07:28,230
And then I want to name that file as Styles dot CCIS.

107
00:07:28,230 --> 00:07:30,900
So once we do that, we are pretty much good to go.

108
00:07:31,170 --> 00:07:32,490
So let's save this.

109
00:07:32,490 --> 00:07:38,410
And once this thing is saved now whenever we execute this build command, the file which is styled dot

110
00:07:38,430 --> 00:07:40,770
CSS should be generated up over here.

111
00:07:41,130 --> 00:07:43,140
So let's see if that thing works out.

112
00:07:43,140 --> 00:07:45,570
So make sure that you enter the social project.

113
00:07:45,570 --> 00:07:51,090
And here you need to execute the build command, which is this command which we have just created.

114
00:07:51,270 --> 00:07:58,470
So you would simply say NPM, run, build, hit, enter, and that's actually going to build from the

115
00:07:58,470 --> 00:08:03,180
source file, which we have, and it's going to create this output file which is styles dot CSS.

116
00:08:03,360 --> 00:08:05,610
So let's see if that file is created.

117
00:08:06,380 --> 00:08:11,480
So it's going to take a while for this thing to happen and here it's is done.

118
00:08:11,990 --> 00:08:14,780
Now, if you go back here, you'll be able to see that.

119
00:08:14,780 --> 00:08:17,130
Now we have a styles dot CSS file.

120
00:08:17,150 --> 00:08:20,920
And this style dot CSS file has a whole bunch of CSS in here.

121
00:08:20,930 --> 00:08:26,300
So if you take a look at this, it has probably tens of thousands of lines of CSS code.

122
00:08:26,300 --> 00:08:32,360
And the CSS code, which you have here are pretty much the basic tailbone classes which we have.

123
00:08:32,780 --> 00:08:38,299
Now what you need to do is whenever you have to style a particular element, you simply have to use

124
00:08:38,299 --> 00:08:39,830
any one of these classes here.

125
00:08:40,460 --> 00:08:45,410
Now the final part, which is remaining, is that once you have this file created, now you need to

126
00:08:45,410 --> 00:08:48,200
link this particular file to all your Django project.

127
00:08:48,350 --> 00:08:54,280
And one of the best ways to do that is by simply linking it to the base HTML file, which you have.

128
00:08:54,290 --> 00:09:01,100
And that's because we are basically linking the basic HTML file to every template which we have in our

129
00:09:01,100 --> 00:09:01,610
app.

130
00:09:01,790 --> 00:09:07,370
So in the head tag right up over here, I simply need to add a link to the stylesheet.

131
00:09:07,370 --> 00:09:09,760
And the stylesheet is present in the user's app.

132
00:09:09,770 --> 00:09:11,720
It's present in the static folder.

133
00:09:11,840 --> 00:09:14,040
So here I'll make use of static.

134
00:09:14,060 --> 00:09:20,030
So here I would say static and then specify the path as users.

135
00:09:20,030 --> 00:09:24,190
Forward slash styles dot CSS.

136
00:09:24,200 --> 00:09:29,420
So as soon as I do that and if I go back here, if I had to refresh.

137
00:09:30,790 --> 00:09:34,060
We get an error, and that's because the project is not running.

138
00:09:34,060 --> 00:09:35,710
So let's run the project.

139
00:09:36,250 --> 00:09:38,530
Let's get back over here.

140
00:09:38,620 --> 00:09:42,970
If I had to refresh, as you can see now, the font over here appears to be different.

141
00:09:43,300 --> 00:09:49,180
And if you font appears to be different on the different pages here, this basically means that the

142
00:09:49,180 --> 00:09:54,760
tailbone has been loaded and the font on the login page did not change because we have not extended

143
00:09:54,760 --> 00:09:56,890
the login page to use the base template.

144
00:09:57,130 --> 00:10:02,500
But if you do that, like if you go to login and if you make this thing extend from the base template

145
00:10:03,100 --> 00:10:05,260
like the other pages which you have.

146
00:10:05,260 --> 00:10:10,960
So for example, if you go to the edit page, this actually inherits from base templates, so this will

147
00:10:10,960 --> 00:10:12,280
have the tail when styling.

148
00:10:12,640 --> 00:10:15,790
So let's get back here and go to edit.

149
00:10:15,790 --> 00:10:15,960
Yeah.

150
00:10:15,970 --> 00:10:21,640
As you can see now, this particular thing has the tail when styling, so that means we have successfully

151
00:10:21,640 --> 00:10:27,390
added elements to our entire Django project and only the pages which do not have that styling.

152
00:10:27,400 --> 00:10:29,980
We simply need to make them extend the base template.

153
00:10:30,340 --> 00:10:35,620
So once this thing is done from the next lecture onwards, we will go ahead and we will start styling

154
00:10:35,620 --> 00:10:37,720
up the index page from scratch.

155
00:10:37,720 --> 00:10:39,680
So this is the next page right here.

156
00:10:39,700 --> 00:10:45,280
So we are going to add a navigation bar to this index page and we are also going to add a nice layout

157
00:10:45,280 --> 00:10:48,250
to this particular user posts page.

158
00:10:48,460 --> 00:10:52,600
So thank you very much for watching and I'll see you guys in the next one.

159
00:10:52,630 --> 00:10:53,320
Thank you.

