1
00:00:00,510 --> 00:00:04,480
Okay, So now once we have this form, let's create a view to render this form.

2
00:00:04,500 --> 00:00:10,920
So in here and the users have we have this view start p vi file and in here is where exactly we will

3
00:00:10,920 --> 00:00:12,060
be building a view.

4
00:00:12,060 --> 00:00:17,070
And the view which we will be building is going to be a login view that's going to render the form which

5
00:00:17,070 --> 00:00:19,590
we have just created, which is this form right here.

6
00:00:19,890 --> 00:00:21,950
So let's go ahead and let's do that.

7
00:00:21,960 --> 00:00:25,530
So first of all, in order to create a view, I'll create a function.

8
00:00:25,530 --> 00:00:29,040
So let's name this thing as a user login.

9
00:00:29,460 --> 00:00:32,670
Obviously, this view is actually going to accept a request.

10
00:00:32,670 --> 00:00:35,040
So here I would say request.

11
00:00:35,040 --> 00:00:41,010
And now in order to render a form which we have just created, what we simply have to do is that we

12
00:00:41,010 --> 00:00:45,300
have to say form equals log in form.

13
00:00:45,510 --> 00:00:50,460
So that's actually going to go ahead, get the login form and we just have to render this form.

14
00:00:50,460 --> 00:00:55,380
So here I would say return, render and simply pass and request.

15
00:00:55,380 --> 00:01:01,610
And here, as we want to render this particular form, this form needs to be passed to a template.

16
00:01:01,620 --> 00:01:04,379
So here we have not yet created a template.

17
00:01:04,379 --> 00:01:11,620
So in order to create a template, let's create a template called us login HTML inside the users app.

18
00:01:11,640 --> 00:01:17,040
So here I need to specify the path as users forward slash login dot HTML.

19
00:01:17,250 --> 00:01:23,900
And then to this particular template I need to pass in the form which we have created as a context.

20
00:01:23,940 --> 00:01:29,110
So here I would simply say a form form.

21
00:01:29,130 --> 00:01:33,690
So here we are simply passing in the form, which is the login form as the context.

22
00:01:33,690 --> 00:01:39,120
And one more thing that needs to be taken care of is that you actually have to import the form.

23
00:01:39,120 --> 00:01:42,490
So here it has automatically imported that for us.

24
00:01:42,510 --> 00:01:48,810
However, instead of saying this, what I could see is that I could say something like from dot forms

25
00:01:48,810 --> 00:01:54,750
import the login form so that we don't have to write this long import statement.

26
00:01:54,750 --> 00:01:58,170
Now let's go ahead and create a template to render out this form.

27
00:01:58,350 --> 00:02:00,900
So let's go back to the file Explorer.

28
00:02:00,900 --> 00:02:05,840
And in order to create a template, you obviously need to set up a templates directory.

29
00:02:05,850 --> 00:02:10,740
So in the user's app, create a new folder called as templates and templates.

30
00:02:10,740 --> 00:02:14,280
Create a new folder again called as users.

31
00:02:14,370 --> 00:02:20,280
And then in this users, let's create a new file, which is nothing but log in dot HTML.

32
00:02:20,280 --> 00:02:26,310
And right now let's say we simply say something like this contains form.

33
00:02:26,550 --> 00:02:31,680
So we have not created any fancy template, nor we have rendered the form right here inside the login

34
00:02:31,680 --> 00:02:32,760
dot HTML.

35
00:02:32,790 --> 00:02:38,790
We have simply added some random text here so as to see if this thing is actually even functional.

36
00:02:39,030 --> 00:02:46,500
So now, once we have done that, we will now go ahead and associate this view with a URL pattern.

37
00:02:46,650 --> 00:02:51,270
So now in order to associate this view with a URL pattern, let's go ahead.

38
00:02:51,870 --> 00:02:56,070
And first of all, we have to create a URL start by file and users.

39
00:02:56,070 --> 00:03:04,830
So here I would say you URL start p y, and now we have to associate this URL, start p with the main

40
00:03:04,830 --> 00:03:08,540
URL, start p five which we have in this main project.

41
00:03:08,550 --> 00:03:10,740
So in here, let's go to the main project.

42
00:03:10,740 --> 00:03:16,940
Let's go to URL, start by and here I have to import something which is called us include.

43
00:03:16,950 --> 00:03:23,550
And what this include does is that it allows us to include a URL start p v file of the apps as well.

44
00:03:23,730 --> 00:03:30,000
So here, once we have done that, I simply have to create a path for our users app.

45
00:03:30,000 --> 00:03:36,780
So here I would say that I want to point out to the users you are start by file whenever the user types

46
00:03:36,780 --> 00:03:39,570
in a URL like users forward slash.

47
00:03:39,570 --> 00:03:48,060
So here I would say comma and include the path, which is nothing but users dot you URLs make sure that

48
00:03:48,060 --> 00:03:53,340
you have includes that in quotations and give a comma when you add this URL pattern.

49
00:03:53,340 --> 00:03:59,070
So now this will actually point out to the URL start py file, which is.

50
00:04:00,080 --> 00:04:04,820
The early start poi file of the users app, which is this file right here.

51
00:04:05,390 --> 00:04:10,900
So once we have done that, now let's start adding some code in the URL start py file here.

52
00:04:10,910 --> 00:04:14,330
So here I'll simply copy the code which we have here.

53
00:04:14,420 --> 00:04:19,970
Go back, piece it up over here and now let's remove a couple of imports.

54
00:04:19,970 --> 00:04:25,370
So I would remove the include, I would also remove the URL patterns which we have here.

55
00:04:25,400 --> 00:04:31,580
We obviously don't need an admin over here either, so let's remove the admin as well.

56
00:04:32,000 --> 00:04:37,410
And now we have to create a URL pattern which links with this particular view, which is the login view.

57
00:04:37,430 --> 00:04:40,520
So here I would first import views.

58
00:04:40,520 --> 00:04:48,440
So I would say from dot import views that's actually going to import the views file and then create

59
00:04:48,440 --> 00:04:50,410
a path which links with the login view.

60
00:04:50,420 --> 00:04:55,880
So here I would say path and let's see, the path for this thing is going to be something like login

61
00:04:55,880 --> 00:04:59,900
forward slash and this is going to be associated with the login view.

62
00:05:00,020 --> 00:05:02,060
So I would say view start.

63
00:05:02,860 --> 00:05:07,960
User underscore login and let's name this view as login as well.

64
00:05:09,920 --> 00:05:13,010
And at the end and show that you have a comma.

65
00:05:13,460 --> 00:05:16,670
Once we are done with this, we are pretty much good to go.

66
00:05:16,940 --> 00:05:20,310
So let's take a look at the flow in which this is going to work.

67
00:05:20,330 --> 00:05:24,140
So now when we are going to run our app, we are going to open up our browser.

68
00:05:24,170 --> 00:05:28,880
We are going to type in the path which is users forward slash log in.

69
00:05:28,880 --> 00:05:33,800
And that's because whenever we run our app, it's going to take a look at this file.

70
00:05:34,040 --> 00:05:36,290
So it's going to take a look at users.

71
00:05:36,290 --> 00:05:40,820
It's going to include users, dot URLs file, which is this file right here.

72
00:05:40,850 --> 00:05:44,240
Then when we type in log in, it's going to render the login view.

73
00:05:44,330 --> 00:05:50,900
The login view is nothing but this view, and this view renders this template along with the login form

74
00:05:50,900 --> 00:05:52,100
which we have created.

75
00:05:52,100 --> 00:05:55,820
The login form we already know is a Django form which we have created.

76
00:05:55,820 --> 00:06:01,220
Then it's going to point out to the log in HTML template, which currently does not contain any kind

77
00:06:01,220 --> 00:06:01,730
of form.

78
00:06:01,730 --> 00:06:05,120
So we are simply going to get this text over there.

79
00:06:05,300 --> 00:06:07,530
So let's see if that thing is functional.

80
00:06:07,550 --> 00:06:09,470
So I'll open up the browser.

81
00:06:10,430 --> 00:06:15,860
And here I would go to forward slash users forward slash login.

82
00:06:16,490 --> 00:06:19,520
So as you can see, it says this contains a form.

83
00:06:19,550 --> 00:06:21,950
However, we do not have any form in there.

84
00:06:22,100 --> 00:06:25,380
So now let's see how we could render out a form over there.

85
00:06:25,400 --> 00:06:29,450
So in order to render the form, we simply have to make a couple of changes.

86
00:06:29,690 --> 00:06:35,780
So here, instead of rendering this text out, instead, I'll create a form.

87
00:06:36,470 --> 00:06:38,750
So here I'll create a form.

88
00:06:38,840 --> 00:06:45,800
And inside this form, as I have to render the form which is being passed as context, which is this

89
00:06:45,800 --> 00:06:48,530
right here, I would simply say.

90
00:06:50,890 --> 00:06:53,800
Form inside of a template syntax.

91
00:06:54,160 --> 00:06:56,590
So make sure that you use template syntax here.

92
00:06:56,590 --> 00:06:59,770
And we want to render this format in terms of paragraph.

93
00:06:59,770 --> 00:07:05,380
So I would see dot as underscore p, which means as paragraphs.

94
00:07:05,380 --> 00:07:11,140
And also whenever you are creating a form, you also have to ensure that you're using a CSR F token.

95
00:07:11,140 --> 00:07:17,020
So here I would say C are F underscore token.

96
00:07:17,620 --> 00:07:24,700
And here instead of having an action, let's say we want a method of this form to be post, okay?

97
00:07:24,700 --> 00:07:27,790
So once this thing is done, we are pretty much good to go.

98
00:07:28,210 --> 00:07:31,260
So now if I go back to the browser, hit refresh.

99
00:07:31,270 --> 00:07:36,550
Now, as you can see, you have a login form right here, but we simply have the form and the form fields

100
00:07:36,550 --> 00:07:41,800
and we do not have a submit button here, so we have to add that submit button manually.

101
00:07:41,800 --> 00:07:46,840
So let's add input type is going to be, let's see, submit.

102
00:07:47,140 --> 00:07:50,950
So once we do that, now we have a submit button up over here as well.

103
00:07:50,980 --> 00:07:56,740
However, if I type in some username and some password here and click on Submit now, nothing would

104
00:07:56,740 --> 00:07:57,340
happen.

105
00:07:57,340 --> 00:07:58,810
So let's try that.

106
00:07:59,050 --> 00:08:01,930
So I have typed in some random username and password.

107
00:08:01,930 --> 00:08:05,410
When I click on submit, the form just disappears.

108
00:08:05,410 --> 00:08:11,920
And that's because whenever we are clicking on submit, technically a post request is going to be submitted.

109
00:08:11,920 --> 00:08:17,020
So when you request for this form, we are essentially making a get request and we are clicking on the

110
00:08:17,020 --> 00:08:17,830
submit button.

111
00:08:17,830 --> 00:08:19,480
We are making a post request.

112
00:08:19,480 --> 00:08:24,310
So the get request logic is already handled by this view right here.

113
00:08:24,310 --> 00:08:31,030
So by default, whenever you create a view, that view is for handling the get request, but we do not

114
00:08:31,030 --> 00:08:34,150
know what needs to be done whenever there's a post request.

115
00:08:34,150 --> 00:08:39,130
So whenever we have a post request, that is, whenever we are submitting the username and password,

116
00:08:39,280 --> 00:08:43,150
we want to log in the user into our Django app.

117
00:08:43,150 --> 00:08:49,600
And for that very reason we have to write logic to handle what happens when the request method is post.

118
00:08:50,050 --> 00:08:53,380
So we are going to learn how to do that in the next lecture.

119
00:08:53,380 --> 00:08:57,370
So thank you very much for watching and I'll see you guys in the next one.

120
00:08:57,490 --> 00:08:58,240
Thank you.

