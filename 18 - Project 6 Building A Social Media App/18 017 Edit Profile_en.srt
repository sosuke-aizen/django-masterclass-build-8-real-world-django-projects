1
00:00:00,090 --> 00:00:05,550
So now that we have created the profile model here, now we need to go ahead and create the forms to

2
00:00:05,550 --> 00:00:09,000
actually go ahead and edit that particular profile model.

3
00:00:09,000 --> 00:00:15,480
And the way in which we want the profile model to work is that whenever we go ahead and create a new

4
00:00:15,480 --> 00:00:19,040
user, the profile for that particular user should automatically create it.

5
00:00:19,050 --> 00:00:24,540
So whenever a new user is registered on our website, the profile for that particular user should be

6
00:00:24,540 --> 00:00:25,020
created.

7
00:00:25,020 --> 00:00:30,390
And also, once the profile is created, the user should actually be able to manually go ahead and create

8
00:00:30,390 --> 00:00:31,080
the profile.

9
00:00:31,080 --> 00:00:36,810
So in the previous case, we had created the profile from an admin panel, but we don't want that.

10
00:00:36,820 --> 00:00:41,880
Instead, we want the user to be able to go ahead and create their very own profile from the website

11
00:00:41,880 --> 00:00:42,370
instead.

12
00:00:42,390 --> 00:00:44,370
So let's learn how to go ahead and do that.

13
00:00:44,370 --> 00:00:49,440
So that means, first of all, we have to create a form so that users could actually use that particular

14
00:00:49,440 --> 00:00:51,000
form to create a profile.

15
00:00:51,030 --> 00:00:55,590
So let's go to the form start p file here and the user model.

16
00:00:55,590 --> 00:00:59,910
And in there, first of all, we have to go ahead and create two forms.

17
00:00:59,910 --> 00:01:05,610
So the first form is going to be for editing the user, and the second form is going to be for editing

18
00:01:05,610 --> 00:01:06,740
the user profile.

19
00:01:06,750 --> 00:01:09,420
So here, first of all, let's import the profile.

20
00:01:09,420 --> 00:01:11,640
So in there, let's import the profile model.

21
00:01:11,640 --> 00:01:17,550
So from dot model or dot models, import the profile model.

22
00:01:17,550 --> 00:01:22,030
And once we have imported the profile model, let's create that form at the top itself.

23
00:01:22,050 --> 00:01:26,790
So let's say we want to create a form for editing the user.

24
00:01:26,790 --> 00:01:33,470
So user edit form, this is going to be first form, So this is going to be a model form.

25
00:01:33,480 --> 00:01:36,330
So I would say form, start, model, form.

26
00:01:36,330 --> 00:01:39,870
And this basically is going to target the model, which is user.

27
00:01:40,170 --> 00:01:42,720
So let's pass some meta information.

28
00:01:42,720 --> 00:01:47,520
So we are going to say that the model this thing is going to target is going to be the user model.

29
00:01:47,520 --> 00:01:51,270
And now let's type in the fields which we want to target in this particular thing.

30
00:01:51,270 --> 00:01:59,010
So if you actually go to the user model, so if you go to your web browser, if you go to users, this

31
00:01:59,010 --> 00:02:00,480
is not running as of now.

32
00:02:00,480 --> 00:02:03,900
And I guess we need to restart the server for now.

33
00:02:06,640 --> 00:02:13,400
You go back here and as you can see right now, we have the username and email and we also have the

34
00:02:13,400 --> 00:02:15,170
first name and the last name as well.

35
00:02:15,170 --> 00:02:19,430
So let's see, we should be able to edit the first name, last name as well as email.

36
00:02:19,430 --> 00:02:25,220
So here I would say first underscore name as one of the field which we want to edit from the form.

37
00:02:25,220 --> 00:02:28,250
Then we also want to be able to edit the last name as well.

38
00:02:28,370 --> 00:02:32,160
And then finally, we want to be able to edit the email as well.

39
00:02:32,180 --> 00:02:36,080
Once this thing is done, that particular form is ready now, right away.

40
00:02:36,080 --> 00:02:38,870
Let's also create a form for profile as well.

41
00:02:38,870 --> 00:02:47,060
So in the profile we simply have to edit the photo so I could simply copy this, paste it up over here

42
00:02:47,060 --> 00:02:48,500
and just change a couple of things.

43
00:02:48,500 --> 00:02:53,360
So instead of user, I would say profile, edit, form, this is going to be a model form.

44
00:02:53,360 --> 00:02:56,960
The meta information is going to target the model, which is the profile model.

45
00:02:57,110 --> 00:03:00,470
And in here we only want to be able to edit the photo.

46
00:03:00,470 --> 00:03:02,390
So I would only say photo here.

47
00:03:02,720 --> 00:03:06,510
So once these two forms are created now, we are pretty much good to go.

48
00:03:06,530 --> 00:03:13,430
Now the way in which we want the flow to work is that let's say when I go ahead and when I register

49
00:03:13,430 --> 00:03:14,330
a new user.

50
00:03:14,990 --> 00:03:18,740
So right now it's giving me an error for some reason, okay?

51
00:03:18,740 --> 00:03:22,280
And that's because there should be a comma here, it seems.

52
00:03:26,230 --> 00:03:29,500
Okay, now it's running well, so make sure that you add a comma there.

53
00:03:29,540 --> 00:03:29,890
Okay.

54
00:03:29,890 --> 00:03:35,770
So once this thing is done now, the way in which we want everything to work is whenever you go ahead

55
00:03:35,770 --> 00:03:38,960
and register a new user, a new profile should be created.

56
00:03:38,980 --> 00:03:46,720
So if you go to the views here, the register is nothing but register as the code in which we basically

57
00:03:46,720 --> 00:03:48,130
go ahead and register a user.

58
00:03:48,130 --> 00:03:50,720
That is, we create a new user in our website.

59
00:03:50,740 --> 00:03:54,490
So here is where we actually go ahead, create a new user and save that.

60
00:03:54,490 --> 00:03:59,680
So as soon as a new user is saved, I also want to go ahead and create a new profile for that user as

61
00:03:59,680 --> 00:04:00,020
well.

62
00:04:00,040 --> 00:04:05,150
So in order to create a new profile here, I actually need to go ahead and import the profile model.

63
00:04:05,170 --> 00:04:12,430
So I would say from DOT models, import the profile model, and then in order to create a new profile,

64
00:04:12,430 --> 00:04:15,100
we first have to create an object of the type profile.

65
00:04:15,100 --> 00:04:23,320
So I would say profile DOT, and then I would say objects dot create, which means we have to create

66
00:04:23,320 --> 00:04:24,220
an object.

67
00:04:24,220 --> 00:04:30,760
And then for this I would take the new user which I have created and simply pass it to the profile model.

68
00:04:30,790 --> 00:04:34,300
So here I would say user equals new user.

69
00:04:34,300 --> 00:04:40,750
And we are passing this because if you actually take a look at the model here, this profile model actually

70
00:04:40,750 --> 00:04:42,100
needs to have a user.

71
00:04:42,100 --> 00:04:44,110
And that's what we are basically doing here.

72
00:04:44,110 --> 00:04:48,000
That is we are basically creating a profile object right up over here.

73
00:04:48,010 --> 00:04:52,990
So this particular line of code is going to create a new profile object, save it into a database,

74
00:04:52,990 --> 00:04:57,400
which means essentially we are creating a profile for that particular new user.

75
00:04:58,030 --> 00:04:58,450
Okay.

76
00:04:58,450 --> 00:05:01,550
So once this thing is done, we are pretty much good to go.

77
00:05:01,570 --> 00:05:06,790
Now, just as we have this register view, and that's actually registering this particular form right

78
00:05:06,790 --> 00:05:07,150
here.

79
00:05:07,180 --> 00:05:09,700
Now let's go ahead and let's create an edit view.

80
00:05:09,700 --> 00:05:15,690
And that edit view should allow us to edit both the user as well as the profile models.

81
00:05:15,700 --> 00:05:22,990
So here, in order to create that, I would go ahead and go at the bottom and see def edit.

82
00:05:23,020 --> 00:05:24,340
Let's type in request.

83
00:05:24,340 --> 00:05:29,410
And this particular edit view should be able to render out these two forms which we have created, which

84
00:05:29,410 --> 00:05:32,260
is the user edit form and the profile edit form.

85
00:05:32,380 --> 00:05:40,510
So here in order to render those out, I have to go back to the views and first import them.

86
00:05:40,510 --> 00:05:47,440
So here I would say from dot forms I want to import the user edit form.

87
00:05:47,860 --> 00:05:50,590
Let's also import the profile edit form as well.

88
00:05:50,590 --> 00:05:51,430
So profile.

89
00:05:51,430 --> 00:05:52,230
Edit form.

90
00:05:52,240 --> 00:05:58,200
Okay, so once that thing is done, I could go down here and let's start working on this view.

91
00:05:58,210 --> 00:06:01,810
So first of all, we are going to handle the post request as usual.

92
00:06:01,810 --> 00:06:09,310
So here I would say if request or method, if this is equally equal to post in that particular case,

93
00:06:09,310 --> 00:06:15,340
we basically want to get the form data and then post that data and create a user profile from that.

94
00:06:15,760 --> 00:06:19,600
So here, first of all, let's get access to the user form.

95
00:06:19,600 --> 00:06:25,690
So I would say user form is going to be equal to user edit form, which means that we want to be able

96
00:06:25,690 --> 00:06:27,670
to edit the data for the user.

97
00:06:27,670 --> 00:06:33,310
And in here the instance is going to be nothing but request start user because this particular thing

98
00:06:33,310 --> 00:06:38,380
needs to access the user object to be able to edit the details for the user.

99
00:06:38,650 --> 00:06:46,150
So here I would say instance equals request dot user and then the data which is going to get it's,

100
00:06:46,150 --> 00:06:47,890
it's going to get the data from the form.

101
00:06:47,890 --> 00:06:51,150
So the data is going to be request dot post.

102
00:06:51,160 --> 00:06:56,380
That means we are getting the post data saving and into the data and we are getting the user instance

103
00:06:56,380 --> 00:06:58,090
and saving it into instance.

104
00:06:58,240 --> 00:07:01,510
Now we do the same thing with the profile form as well.

105
00:07:01,510 --> 00:07:06,100
So here I would say profile form and this is going to be profile.

106
00:07:06,100 --> 00:07:06,850
Edit form.

107
00:07:07,360 --> 00:07:08,500
So profile.

108
00:07:08,500 --> 00:07:09,370
Edit form.

109
00:07:09,580 --> 00:07:14,020
And then similarly we pass in the instance as the request or the user.

110
00:07:14,020 --> 00:07:19,780
So we say request dot user here, that means we are getting the instance for the currently logged in

111
00:07:19,780 --> 00:07:20,310
user.

112
00:07:20,320 --> 00:07:22,660
So this actually gives us the user.

113
00:07:22,660 --> 00:07:26,950
But here, as this is a profile form, we want to get the profile of the log than user.

114
00:07:26,950 --> 00:07:30,280
So we say request dot user, dot profile.

115
00:07:31,470 --> 00:07:36,540
And then the data as usual, we are actually going to get it from request or post.

116
00:07:36,900 --> 00:07:40,680
So here I would say data is request dot post.

117
00:07:40,680 --> 00:07:46,440
So we are getting the data from the form and also as in this particular profile form, as we are working

118
00:07:46,440 --> 00:07:51,390
with an image, we also need to get access to the files as well, which is nothing but the image file.

119
00:07:51,450 --> 00:07:58,290
So here I would also add a files field and just like request dot post, I would simply type in a request

120
00:07:58,290 --> 00:08:01,140
dot that's going to be files.

121
00:08:01,830 --> 00:08:02,130
Okay.

122
00:08:02,160 --> 00:08:05,070
So once this thing is done, we are pretty much good to go.

123
00:08:05,340 --> 00:08:11,970
And as these two forms are getting the data from request or user, which which means that we are getting

124
00:08:11,970 --> 00:08:17,190
the data from the currently logged in user, that means this will only work when the user is logged

125
00:08:17,190 --> 00:08:17,580
in.

126
00:08:17,590 --> 00:08:21,120
And henceforth let's add a login required decorator on top.

127
00:08:21,120 --> 00:08:25,830
So here I would see and login underscore required.

128
00:08:25,830 --> 00:08:27,450
And that should do the trick.

129
00:08:28,080 --> 00:08:28,500
Okay.

130
00:08:28,500 --> 00:08:33,510
So now once we have the data from both the forms, which is the user form and the profile form.

131
00:08:33,539 --> 00:08:39,090
Now, once the user has edited both of these data, we now have to go ahead and check the validity of

132
00:08:39,090 --> 00:08:39,809
the data.

133
00:08:39,809 --> 00:08:44,820
And if we found these two forms to be valid, then we could go ahead and save that data.

134
00:08:44,970 --> 00:08:46,620
And doing that is quite simple.

135
00:08:46,620 --> 00:08:53,070
I would simply say if user form DOT is underscore valid.

136
00:08:53,280 --> 00:08:57,510
And I would also check the validity for profile form in the same line.

137
00:08:57,510 --> 00:09:06,240
So here I would say F profile form dot is underscore valid if both of them are valid.

138
00:09:06,240 --> 00:09:09,960
In that particular case, I simply have to save the data in the two forms.

139
00:09:10,110 --> 00:09:16,350
So I would say user form, dot save and then profile form, dot save as well.

140
00:09:17,310 --> 00:09:22,470
And once that thing is done, we are pretty much good to go and we are done handling the post part of

141
00:09:22,470 --> 00:09:22,950
this.

142
00:09:23,220 --> 00:09:27,900
Now let's write the LS, which means what happens when the request method is kept?

143
00:09:28,020 --> 00:09:32,880
So when the request method is get, we essentially want to go ahead and render out those particular

144
00:09:32,880 --> 00:09:33,510
forms.

145
00:09:33,510 --> 00:09:37,800
And while rendering out those forms, we are simply going to have these two forms.

146
00:09:37,800 --> 00:09:40,890
So let's copy this, paste it up over here.

147
00:09:40,890 --> 00:09:46,320
But when the request method is going to be post, we only have the data for the log then user, but

148
00:09:46,320 --> 00:09:48,360
we don't have this additional data here.

149
00:09:48,360 --> 00:09:54,240
So let's get rid of that data from here and let's also get rid of that data from here as well.

150
00:09:54,240 --> 00:10:00,390
And the reason why we are keeping this particular request or user and request or user load profile is

151
00:10:00,570 --> 00:10:05,190
because we get that particular data from the currently logged in users instance.

152
00:10:05,250 --> 00:10:06,690
That's why we keep it.

153
00:10:06,690 --> 00:10:10,380
And once that thing is done, we simply render out the templates.

154
00:10:10,380 --> 00:10:15,540
So here we say return, render, and now let's pass in the request.

155
00:10:15,660 --> 00:10:19,680
And we have not yet created any kind of template for these two particular forms.

156
00:10:19,680 --> 00:10:25,950
So let's create a template in users and let's call this particular template as a red dot HTML.

157
00:10:27,120 --> 00:10:29,460
And now let's pass in two forms.

158
00:10:29,460 --> 00:10:34,590
So in the previous case, we have rendered only one form inside a template like that.

159
00:10:34,590 --> 00:10:39,900
But in this particular case, as we have two forms we have to pass in these two forms here.

160
00:10:40,140 --> 00:10:47,280
So first of all, we'll pass in the first form, which is the user and the SCO form and then see user

161
00:10:47,280 --> 00:10:56,010
form and then give a comma right away and then see profile, underscore form, pass this thing as profile

162
00:10:56,010 --> 00:10:56,460
form.

163
00:10:57,060 --> 00:10:59,730
Once this thing is done, we are pretty much good to go.

164
00:10:59,730 --> 00:11:06,000
And what we are essentially doing here is that we are rendering the two forms both on one page, but

165
00:11:06,000 --> 00:11:11,190
it will look like they both are actually one single form, but in reality they are actually part of

166
00:11:11,220 --> 00:11:12,300
two different forms.

167
00:11:13,020 --> 00:11:19,020
So once this thing is done, let's go ahead and let's create the template which is users forward slash,

168
00:11:19,020 --> 00:11:20,430
edit, dot, HTML.

169
00:11:20,670 --> 00:11:27,810
So in here inside templates, let's create a new file that's going to be added dot HTML.

170
00:11:28,080 --> 00:11:30,930
This is going to contain the basic boilerplate code.

171
00:11:31,050 --> 00:11:33,420
So let's take some code from here.

172
00:11:34,710 --> 00:11:41,610
Oh, let's actually take the code from some other file which actually has a form like the password reset

173
00:11:41,610 --> 00:11:41,880
form.

174
00:11:41,880 --> 00:11:47,400
So let's use that so that we could save some time writing the basic boilerplate code.

175
00:11:47,400 --> 00:11:49,620
And here, let's remove the valid link.

176
00:11:50,310 --> 00:11:52,710
Let's remove the LS part as well.

177
00:11:52,860 --> 00:11:55,380
Let's remove the if.

178
00:11:55,890 --> 00:11:58,800
And in here we have the basic form.

179
00:11:58,830 --> 00:12:05,130
Let's add a heading which is something like edit row, file form.

180
00:12:05,640 --> 00:12:10,920
And instead of saying form as be the name of the forms which we have passed here, our user form and

181
00:12:10,920 --> 00:12:11,750
profile form.

182
00:12:11,760 --> 00:12:16,020
So here I would say form, dot, user, underscore form.

183
00:12:16,020 --> 00:12:21,980
And similarly, let's create one more form there that's going to be profile, underscore form.

184
00:12:21,990 --> 00:12:25,690
So I would replace this with profile.

185
00:12:25,800 --> 00:12:29,660
Once that thing is done, we are pretty much good to go.

186
00:12:29,670 --> 00:12:36,120
But now as one of the forms is actually submitting an image, what we need to do is we need to go ahead

187
00:12:36,120 --> 00:12:42,330
and whenever we are making a post, we have to add additional parameter, which is NC type and this

188
00:12:42,330 --> 00:12:45,040
is going to be multi multipart forward slash form data.

189
00:12:45,060 --> 00:12:47,790
Once that thing is done, we are pretty much good to go.

190
00:12:47,820 --> 00:12:50,610
We have the template in place, we have the forms in place.

191
00:12:50,610 --> 00:12:55,590
We also have the views and the view, which is going to render this template as the edit view, which

192
00:12:55,590 --> 00:12:59,700
means now we have to create a URL pattern for this particular edit view.

193
00:13:00,060 --> 00:13:01,820
So doing that is quite simple.

194
00:13:01,830 --> 00:13:08,730
Let's go to URL, start py file, give a comma, add the last URL, add a path.

195
00:13:08,730 --> 00:13:15,090
This path is going to say something like edit forward slash and this is going to render the view which

196
00:13:15,090 --> 00:13:15,660
is edit.

197
00:13:15,660 --> 00:13:16,620
So view start.

198
00:13:16,620 --> 00:13:20,280
Edit Let's name this as added as well.

199
00:13:20,670 --> 00:13:23,930
Give a comma at the end and we should be good.

200
00:13:23,940 --> 00:13:32,700
So now if I go back here, if I go to users forward slash edit, I get the edit profile form and for

201
00:13:32,700 --> 00:13:36,810
some reason we are not getting the forms here.

202
00:13:37,690 --> 00:13:39,580
Which is the user form and the profile form.

203
00:13:39,580 --> 00:13:41,500
So let's see why that is the case.

204
00:13:42,430 --> 00:13:44,140
So it seems.

205
00:13:45,660 --> 00:13:47,680
Okay, so I made a mistake here.

206
00:13:47,700 --> 00:13:49,920
This should be user form dot be.

207
00:13:50,840 --> 00:13:52,670
Not form dot this.

208
00:13:52,940 --> 00:13:56,300
So this should be dot as endoscopy.

209
00:13:57,410 --> 00:14:01,550
And this should be dot as underscore P as well.

210
00:14:02,430 --> 00:14:02,740
Okay.

211
00:14:02,750 --> 00:14:05,510
So also make sure that you have space here.

212
00:14:05,540 --> 00:14:05,890
Okay.

213
00:14:05,900 --> 00:14:07,130
So now let's set refresh.

214
00:14:07,130 --> 00:14:12,800
And as you can see, we also have this form here which allows us to edit all the details.

215
00:14:13,010 --> 00:14:17,840
So right now we are logged in as a super user and we could edit the first name, last name, and we

216
00:14:17,840 --> 00:14:20,510
could also upload another image for that user as well.

217
00:14:20,540 --> 00:14:22,960
So let's say I want to add the first name and last name.

218
00:14:22,970 --> 00:14:24,810
So I've added the first name and last name.

219
00:14:24,830 --> 00:14:29,120
Let me edit the email address and let me also choose a image.

220
00:14:29,120 --> 00:14:31,520
So let's say I choose a different image this time.

221
00:14:31,520 --> 00:14:35,240
And now if I now click on Submit, let's see what happens.

222
00:14:35,870 --> 00:14:37,340
So I've hit submit here.

223
00:14:37,730 --> 00:14:41,120
And now let's see if those changes are reflected in the admin.

224
00:14:41,120 --> 00:14:43,850
So I'll go to admin, I'll go to users.

225
00:14:43,850 --> 00:14:47,720
And as you can see now, we have the first name and last name added for the user.

226
00:14:47,990 --> 00:14:52,310
And also if I go to profiles, the profile for this user is created.

227
00:14:52,700 --> 00:14:58,790
And now, as you can see, the image for that particular user or the profile image has also been changed,

228
00:14:58,970 --> 00:15:00,980
which means that this is working well.

229
00:15:01,010 --> 00:15:06,140
Now, one more thing which we need to check here is that if we go ahead and register a new user, we

230
00:15:06,140 --> 00:15:10,070
want to see if the profile for that user is automatically created or not.

231
00:15:10,520 --> 00:15:12,190
So let's test that as well.

232
00:15:12,200 --> 00:15:16,340
So let's go to users forward slash register.

233
00:15:16,370 --> 00:15:18,040
Let's register a new user.

234
00:15:18,050 --> 00:15:19,580
So I would type in the user name.

235
00:15:19,580 --> 00:15:21,890
So I've added some details of a user here.

236
00:15:21,890 --> 00:15:25,670
And now if I click on register, it sees registration successful.

237
00:15:25,970 --> 00:15:27,650
Now let's go to admin.

238
00:15:27,800 --> 00:15:34,790
If I go to profiles now, as you can see, the profile for user pad has now already been created.

239
00:15:35,150 --> 00:15:42,680
And now if I want to add the profile picture for part, I simply have to go ahead and go to users forward

240
00:15:42,680 --> 00:15:43,760
slash edit here.

241
00:15:43,760 --> 00:15:45,440
I'm not able to edit the user.

242
00:15:45,440 --> 00:15:48,320
That's because I'm currently logged in as a super user.

243
00:15:48,380 --> 00:15:49,910
So I would log out.

244
00:15:51,020 --> 00:15:54,000
Let's log back in now.

245
00:15:54,020 --> 00:15:57,020
This time, let's log in as part submit.

246
00:15:57,650 --> 00:16:02,780
Now let's go to users forward slash edit and here, let's edit the last name.

247
00:16:02,900 --> 00:16:05,780
Let's use a profile picture for this user.

248
00:16:05,810 --> 00:16:07,130
Click on submit.

249
00:16:07,160 --> 00:16:09,450
Now the user profiles are saved.

250
00:16:09,470 --> 00:16:11,240
Now, if I go to admin.

251
00:16:11,480 --> 00:16:13,040
Let's see if that is saved.

252
00:16:13,040 --> 00:16:14,960
So I login as the super user.

253
00:16:16,070 --> 00:16:21,860
Now once I'm logged in here, if I go to profiles, if I go to the profile for part, as you can see,

254
00:16:21,860 --> 00:16:26,540
that particular image is uploaded and we also have the profile for Pat.

255
00:16:26,660 --> 00:16:32,480
If I go to users, I should also be able to have the last name added for part as well, which means

256
00:16:32,480 --> 00:16:38,510
that now the functionality for registering the user, creating the user, logging in the user, logging

257
00:16:38,510 --> 00:16:43,460
them out, even creating their own profile as well as profile picture is now complete.

258
00:16:43,460 --> 00:16:48,800
And from the next lecture onwards, let's work on creating the functionality so that these users could

259
00:16:48,800 --> 00:16:51,160
now create their very own posts.

260
00:16:51,170 --> 00:16:53,840
So let's learn how to do that in the next lecture.

