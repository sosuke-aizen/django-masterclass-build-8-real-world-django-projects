1
00:00:00,090 --> 00:00:05,730
So in this particular lecture, let's go ahead and let's work on generating the slug for this particular

2
00:00:05,730 --> 00:00:06,840
model, which we have.

3
00:00:07,080 --> 00:00:12,360
So whenever we want to generate a slug, we need to generate a slug out of a given string.

4
00:00:12,360 --> 00:00:18,310
And let's say we want to generate the slug from the title, which we have for the particular post.

5
00:00:18,330 --> 00:00:23,870
So in order to generate that particular slug, we will override the save method, which we have in Django.

6
00:00:23,880 --> 00:00:25,560
So let's go ahead and do that.

7
00:00:25,560 --> 00:00:31,740
So I'll type in def save here, and the save method from Django is going to accept self as a parameter.

8
00:00:31,740 --> 00:00:36,500
Then we have argument which is going to be an argument and then keyword arguments.

9
00:00:36,510 --> 00:00:40,920
So that's going to be KW args, which stands for keyword arguments.

10
00:00:40,920 --> 00:00:45,420
So these are the two parameters we need to pass to this particular function.

11
00:00:45,420 --> 00:00:50,810
And here what we do is we write in the code which we want to add to the overridden save method.

12
00:00:50,820 --> 00:00:53,230
So here we want to generate a new slug.

13
00:00:53,250 --> 00:00:56,400
So first we check if the particular post has a slug.

14
00:00:56,520 --> 00:01:02,320
So if the Post does not have a slug in that particular case, we generate a slug out of the title.

15
00:01:02,340 --> 00:01:10,050
So here we say if not self dot slug, which means that if the current object does not have a slug in

16
00:01:10,050 --> 00:01:14,430
that case, we have to create a slug and we want to create the slug out of the title.

17
00:01:14,430 --> 00:01:18,720
So in order to create a slug, we have a method called slug ify.

18
00:01:19,230 --> 00:01:20,820
So I would say slug ify.

19
00:01:20,820 --> 00:01:23,480
And to this we pass in this particular title.

20
00:01:23,490 --> 00:01:28,350
So to get access to the title of the current object, we say self dot title.

21
00:01:28,860 --> 00:01:31,470
And now let's see if the slug into something.

22
00:01:31,470 --> 00:01:35,670
So I would save this thing and do self dot slug itself.

23
00:01:37,170 --> 00:01:43,710
So I would say self dot slug equals this and also make sure that you indent it correctly.

24
00:01:43,710 --> 00:01:47,580
And once that thing is done now the slug is generated and it's saved as well.

25
00:01:47,580 --> 00:01:51,480
So once this thing is done, I would say super dot save.

26
00:01:51,780 --> 00:01:57,690
So we are essentially calling in the save method so that the object finally gets saved.

27
00:01:57,690 --> 00:02:05,850
And to this, we again passed in the missing parameters as arguments and keyword arguments and make

28
00:02:05,850 --> 00:02:07,170
sure that you have a dot.

29
00:02:07,170 --> 00:02:09,150
So you call the save on super.

30
00:02:09,270 --> 00:02:14,390
And here we are getting an error on slug ify and that's because we need to import slug ify.

31
00:02:14,400 --> 00:02:18,450
So slug ify is actually present in the utils dot text package.

32
00:02:18,510 --> 00:02:26,070
So here I would say from django dot utils dot text import slug if I.

33
00:02:27,070 --> 00:02:31,750
And also we are getting an error here because we need arcs here.

34
00:02:32,200 --> 00:02:33,970
And once this thing is done.

35
00:02:34,090 --> 00:02:38,440
Now, whenever you create a post, a new slug should be generated.

36
00:02:38,470 --> 00:02:44,440
So now let's go ahead and let's make migrations for this model and see how this thing behaves.

37
00:02:44,650 --> 00:02:49,630
So I would go ahead, save the code, go back to the terminal here.

38
00:02:49,630 --> 00:02:51,070
In order to make migrations.

39
00:02:51,070 --> 00:02:57,670
I would say python manage dot pie, make migrations, and then.

40
00:02:59,450 --> 00:03:05,420
Okay, So we have an error here which says this model has no attribute called last text fields.

41
00:03:05,420 --> 00:03:08,120
So let's take a look at what exactly went wrong here.

42
00:03:08,150 --> 00:03:08,390
Okay.

43
00:03:08,450 --> 00:03:09,560
This should be models.

44
00:03:09,560 --> 00:03:11,570
So we have a minor typo there.

45
00:03:12,290 --> 00:03:13,790
Let's clear up the screen.

46
00:03:13,910 --> 00:03:15,920
Let's run, make migrations again.

47
00:03:17,060 --> 00:03:17,420
Okay.

48
00:03:17,420 --> 00:03:19,400
We have plenty of typos here.

49
00:03:19,430 --> 00:03:20,660
This should be.

50
00:03:21,800 --> 00:03:22,610
Length.

51
00:03:22,880 --> 00:03:24,380
Let's go back again.

52
00:03:24,590 --> 00:03:25,950
Let's make migrations.

53
00:03:25,970 --> 00:03:27,950
Now, the migrations have been made.

54
00:03:28,040 --> 00:03:32,540
And now let's type in Python, managed by migrate.

55
00:03:33,570 --> 00:03:37,140
Okay, So now once the migrations are done, we are pretty much good to go.

56
00:03:37,170 --> 00:03:43,380
So the model has been created in the back end and now we simply have to register this particular model

57
00:03:43,380 --> 00:03:44,670
in the admin.

58
00:03:44,670 --> 00:03:48,110
And for that we go to the admin py file of the post sap.

59
00:03:48,270 --> 00:03:50,040
We first import that model.

60
00:03:50,040 --> 00:04:00,270
So I would say from DOT models I want to import the post model and then I would say admin, dot site,

61
00:04:00,270 --> 00:04:05,040
dot register and I want to register the post model.

62
00:04:05,550 --> 00:04:12,240
Once this thing is done, I could go back to the browser, run the server, go back to the browser here.

63
00:04:13,830 --> 00:04:19,500
Go to the admin panel and here we should be able to see the posts under the posts up here.

64
00:04:19,920 --> 00:04:23,700
So we have a post model and here we could go ahead and create a new post.

65
00:04:23,820 --> 00:04:25,900
So let's try creating a new post here.

66
00:04:25,920 --> 00:04:28,380
So I would select a particular user.

67
00:04:28,800 --> 00:04:31,350
So let's use an image which we want to post.

68
00:04:31,380 --> 00:04:33,390
So let me choose an image.

69
00:04:33,780 --> 00:04:35,010
Let's select this image.

70
00:04:35,010 --> 00:04:39,720
Let me add a caption like Nature is beautiful.

71
00:04:41,220 --> 00:04:43,530
Hopefully I don't make any type of that.

72
00:04:43,650 --> 00:04:45,300
Let's add a simple title here.

73
00:04:45,300 --> 00:04:47,310
Like let's say nature.

74
00:04:47,340 --> 00:04:50,220
Image Slack will be automatically generated.

75
00:04:50,220 --> 00:04:56,190
And now when I hit Save, as you can see, we have nature image created here, which is a post.

76
00:04:56,190 --> 00:05:01,170
And if you take a look at this, as you can see, the slug here is now automatically generated from

77
00:05:01,170 --> 00:05:03,060
the title which we have passed in here.

78
00:05:03,300 --> 00:05:08,310
And also we have that image uploaded up over here as well, which is nothing but the image for that

79
00:05:08,310 --> 00:05:09,300
particular post.

80
00:05:09,630 --> 00:05:15,570
So right now we are actually submitting the post via this admin panel and we actually need to go ahead

81
00:05:15,570 --> 00:05:20,790
and create a form which allows the users of our website to make these particular posts.

82
00:05:20,910 --> 00:05:26,790
So in order to do that, we now need to go ahead and create a form for letting the users make a post

83
00:05:26,790 --> 00:05:27,840
to our website.

84
00:05:27,870 --> 00:05:30,270
So let's go ahead and do that in the next one.

