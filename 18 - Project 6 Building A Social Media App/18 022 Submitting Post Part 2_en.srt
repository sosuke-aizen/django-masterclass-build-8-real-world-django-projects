1
00:00:00,090 --> 00:00:05,880
Okay, So before actually testing this particular functionality, we need to add one more thing to this

2
00:00:05,880 --> 00:00:07,170
particular view.

3
00:00:07,560 --> 00:00:13,320
So as we all know, as we are actually working with a form which also accepts an image field as well,

4
00:00:13,350 --> 00:00:18,660
we cannot just simply get the data, but we also get the files for this particular form as well.

5
00:00:18,750 --> 00:00:25,920
So along with the data, this data also has files, so I need to see the files field as request dot

6
00:00:25,980 --> 00:00:26,850
files.

7
00:00:27,240 --> 00:00:31,980
Once this thing is done now, let's test out the functionality and let's see if that works.

8
00:00:32,040 --> 00:00:37,950
So I'll go back to the browser here and in here I have to go to the root, which is forward, slash

9
00:00:38,520 --> 00:00:43,830
post forward, slash, create, because that's the root which we have associated with that particular

10
00:00:43,830 --> 00:00:44,200
view.

11
00:00:44,220 --> 00:00:48,760
So if you go there, you will be presented with this form which is working well.

12
00:00:48,780 --> 00:00:50,310
Now let's add the title.

13
00:00:50,490 --> 00:00:55,800
Let's say this is a title and I would say this is a caption.

14
00:00:57,150 --> 00:00:58,650
Let's choose an image as well.

15
00:00:58,650 --> 00:01:01,380
So let's say I select the same image as before.

16
00:01:02,410 --> 00:01:07,870
And now if I click on post, as you can see, it seems like the data has been submitted.

17
00:01:08,020 --> 00:01:11,500
So let's go ahead and check that by going into admin.

18
00:01:11,770 --> 00:01:17,860
And if I go to post here, as you can see, we have a new post which sees this, the title and this

19
00:01:17,860 --> 00:01:18,220
post.

20
00:01:18,220 --> 00:01:22,210
As you can see, the user is automatically associated with the post.

21
00:01:22,480 --> 00:01:25,330
We also have the image uploaded over here as well.

22
00:01:25,600 --> 00:01:32,050
We also have the caption the title as well as the slug, which is automatically generated, which means

23
00:01:32,050 --> 00:01:37,300
that the ability to make posts or create post by the user is working absolutely fine.

24
00:01:37,330 --> 00:01:42,820
Now, once we make these particular posts and once we have this functionalities for the user to be able

25
00:01:42,820 --> 00:01:49,000
to submit the post, let's say we want to display all the posts which are submitted by us on a particular

26
00:01:49,000 --> 00:01:49,840
home page.

27
00:01:49,960 --> 00:01:55,060
So let's create that functionality for displaying all the posts in one page in the next lecture.

