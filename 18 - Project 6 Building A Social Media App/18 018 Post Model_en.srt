1
00:00:00,090 --> 00:00:05,730
Now let's learn how we could add a functionality which would allow these users to go ahead and create

2
00:00:05,730 --> 00:00:06,560
posts.

3
00:00:06,570 --> 00:00:11,700
So in order to be able to create posts, what we will do is that we'll create a brand new app.

4
00:00:11,700 --> 00:00:18,510
So we already have the users happen there, and the users app is essentially used for handling the registration

5
00:00:18,510 --> 00:00:19,050
process.

6
00:00:19,050 --> 00:00:24,360
But whenever we want to work on the core functionality of our app, let's create a different app for

7
00:00:24,390 --> 00:00:24,780
that.

8
00:00:24,780 --> 00:00:27,090
So let's create an app called Last Posts.

9
00:00:27,090 --> 00:00:30,050
And for that, let's go back to the terminal.

10
00:00:30,060 --> 00:00:35,280
Let's clear up the terminal, make sure that you are actually present and do your social project or

11
00:00:35,280 --> 00:00:38,360
the project directory or whatever you would like to call it.

12
00:00:38,370 --> 00:00:38,610
Here.

13
00:00:38,610 --> 00:00:43,050
We should have the project name and the user's app directory as well.

14
00:00:43,050 --> 00:00:46,980
And here is where we are going to create a new app which is posts.

15
00:00:47,100 --> 00:00:53,010
So here I would say Django Dash admin start app, let's call this app as posts.

16
00:00:53,010 --> 00:00:57,960
And once that app is created, the very first thing which needs to be done is that you need to go to

17
00:00:57,960 --> 00:01:04,080
the settings dot API file of your main project and simply add that particular app name inside installed

18
00:01:04,080 --> 00:01:04,739
apps.

19
00:01:04,890 --> 00:01:11,250
So in here along with users, I would also add posts and once we do that, we should be good to go.

20
00:01:11,670 --> 00:01:18,660
Now, once we have this as now we want to be able to save posts, we will go inside the post app and

21
00:01:18,660 --> 00:01:22,050
in here let's first close all the tabs.

22
00:01:23,210 --> 00:01:25,880
And then let's go to posts.

23
00:01:25,880 --> 00:01:31,500
And in here we need to work on creating a post model which is going to save our posts.

24
00:01:31,520 --> 00:01:34,790
So in order to save post, let's create a post model.

25
00:01:34,790 --> 00:01:39,980
So class post and let's say this extends from models to model.

26
00:01:40,100 --> 00:01:43,410
And now let's create the different fields which we want for this post.

27
00:01:43,430 --> 00:01:46,250
So this is essentially a social media application.

28
00:01:46,250 --> 00:01:51,530
And inside a social media application, you should be able to add an image to a post.

29
00:01:51,530 --> 00:01:53,990
So let's create the first field as image.

30
00:01:54,020 --> 00:01:59,990
The image field is going to be nothing but models, dot image field.

31
00:02:00,380 --> 00:02:05,380
And then to this particular image field, we also need to upload to a particular folder.

32
00:02:05,390 --> 00:02:07,820
So we set the upload to parameter.

33
00:02:08,479 --> 00:02:15,740
So upload underscore two and we basically want to upload to images forward slash and then we want to

34
00:02:15,740 --> 00:02:18,170
name this thing as percent Y.

35
00:02:18,710 --> 00:02:20,270
Forward slash percent.

36
00:02:20,270 --> 00:02:20,670
M.

37
00:02:20,690 --> 00:02:22,460
Forward slash percent D.

38
00:02:23,360 --> 00:02:23,750
Okay.

39
00:02:23,750 --> 00:02:29,210
So now once we have added the image field, let's say we also want to add a caption for this image as

40
00:02:29,210 --> 00:02:29,570
well.

41
00:02:29,780 --> 00:02:33,940
So the caption is going to be nothing, but it's simply going to be a text field.

42
00:02:33,950 --> 00:02:39,110
So I would say model dot, text field, and you could actually leave the caption as blank.

43
00:02:39,110 --> 00:02:42,290
So I would set the blank of this thing to true.

44
00:02:42,320 --> 00:02:45,890
Okay, So once this thing is done, let's add a couple more fields here.

45
00:02:46,010 --> 00:02:48,410
So let's add a title for this particular post.

46
00:02:48,410 --> 00:02:52,910
So I would say title is going to be equal to the same thing, which is models.

47
00:02:52,910 --> 00:02:56,900
And instead of text field, let's say I want to say cow field this time.

48
00:02:56,900 --> 00:03:02,030
So cow field, let's say the max length I want for this thing is going to be 200.

49
00:03:02,510 --> 00:03:09,440
And now every post which you are going to make is obviously going to be associated with the user because

50
00:03:09,440 --> 00:03:13,190
you cannot make a post without being logged in and to application.

51
00:03:13,190 --> 00:03:17,540
And there is going to be no post which is going to not have any kind of a user.

52
00:03:17,750 --> 00:03:22,370
That means now here I need to go ahead and import the user model as well.

53
00:03:22,370 --> 00:03:25,460
And the user model is actually present in the settings.

54
00:03:25,460 --> 00:03:33,110
So here I would say from Django dot conf import settings first and then add the user field here and

55
00:03:33,110 --> 00:03:36,340
associated with the user model which we have in the settings.

56
00:03:36,350 --> 00:03:37,970
So I'll create a new field here.

57
00:03:37,970 --> 00:03:40,070
User This is going to be a foreign key.

58
00:03:40,070 --> 00:03:47,210
So I would say models dot foreign key and this is going to associate every post with a unique user.

59
00:03:47,210 --> 00:03:53,570
So here I would say settings dot and in order to associate it with the user model, I would say auth

60
00:03:53,570 --> 00:03:56,960
underscore user underscore model.

61
00:03:56,960 --> 00:04:02,900
And then I'm also going to set the own delete of this thing to let's say models dot cascade.

62
00:04:04,010 --> 00:04:04,340
Okay.

63
00:04:04,340 --> 00:04:07,150
So once this thing is done, we are pretty much good to go.

64
00:04:07,160 --> 00:04:11,300
Now let's see what other kind of fields do we have there or need to have there.

65
00:04:11,690 --> 00:04:14,360
So let's say we also want to create a slack for this.

66
00:04:14,360 --> 00:04:20,079
So slack is nothing, but it's basically some random set of characters which could be used.

67
00:04:20,089 --> 00:04:27,710
So here I would say slack equals models, dot slack field.

68
00:04:27,710 --> 00:04:33,290
And the slack field is essentially useful whenever we want to associate a URL with a specific post.

69
00:04:33,410 --> 00:04:37,790
So here I would say the max length I want for this thing is going to be 200.

70
00:04:37,970 --> 00:04:42,560
So Maxlength is going to be 200 and this field could also be blank.

71
00:04:42,560 --> 00:04:45,650
So I would set the blank of this thing to true.

72
00:04:45,650 --> 00:04:49,520
And now we also want to be able to set the date on which this post is created.

73
00:04:49,520 --> 00:04:51,890
So I would say created equals.

74
00:04:51,890 --> 00:04:54,860
That's going to be model start date field.

75
00:04:55,690 --> 00:05:01,420
And now I basically want to set the value of this thing by default to the current date and time.

76
00:05:01,420 --> 00:05:10,120
And therefore the set that I would set the auto now add field to true, which means that we automatically

77
00:05:10,120 --> 00:05:13,750
want to set the date fields to the current date, which we have.

78
00:05:14,020 --> 00:05:16,840
So once this thing is done, we are pretty much good to go.

79
00:05:16,870 --> 00:05:20,620
So now let's go ahead and let's add a string method for this.

80
00:05:20,620 --> 00:05:24,360
So def double underscore str double underscore.

81
00:05:24,400 --> 00:05:30,850
Let's pass in self and this thing needs to return the title of this particular image or this particular

82
00:05:30,850 --> 00:05:31,180
post.

83
00:05:31,180 --> 00:05:34,540
So I would make this thing return self taught.

84
00:05:34,540 --> 00:05:36,400
That's going to be title.

85
00:05:36,430 --> 00:05:36,730
Okay.

86
00:05:36,730 --> 00:05:39,450
So now once this thing is done, we are pretty much good to go.

87
00:05:39,460 --> 00:05:43,330
Now our next job is to go ahead and create a slug field for this one.

88
00:05:43,330 --> 00:05:46,360
So the value of slug needs to be automatically set.

89
00:05:46,360 --> 00:05:52,060
And in order to generate a slug out of this, what we will do is that we will overwrite the save method

90
00:05:52,060 --> 00:05:53,410
for this particular model.

91
00:05:53,650 --> 00:05:59,020
So what overwriting the save method means is that, let's say whenever you create an object of the type

92
00:05:59,020 --> 00:06:03,850
post, which means that whenever you are creating a post, we all know that an object is created.

93
00:06:03,850 --> 00:06:09,190
And whenever we save that particular object, a certain set of things or certain set of code is executed.

94
00:06:09,190 --> 00:06:14,860
And if you want to add another set of code to that particular save method, you could override the save

95
00:06:14,860 --> 00:06:17,320
method for this particular model here itself.

96
00:06:17,320 --> 00:06:23,680
So we are going to be using that particular save method to write code to generate this particular slug

97
00:06:23,680 --> 00:06:24,720
automatically.

98
00:06:24,730 --> 00:06:28,120
So let's go ahead and let's do that in the next lecture.

