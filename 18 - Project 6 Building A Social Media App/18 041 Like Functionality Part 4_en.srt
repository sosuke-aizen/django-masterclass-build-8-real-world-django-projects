1
00:00:00,090 --> 00:00:04,500
So in this particular lecture, we will fix the refresh issue, which we have.

2
00:00:04,590 --> 00:00:07,410
So the refresh issue is caused due to Ajax.

3
00:00:07,410 --> 00:00:13,710
That means whenever you're using Ajax and whenever you make some request using Ajax, what happens is

4
00:00:13,710 --> 00:00:16,400
the page does not get refreshed or reloaded.

5
00:00:16,410 --> 00:00:19,500
So Ajax basically prevents that refresh of page.

6
00:00:19,500 --> 00:00:26,100
But now the reason why we want to actually have a refresh there is because we don't want to change the

7
00:00:26,100 --> 00:00:30,530
state of just one icon there, but we instead want an entire refresh.

8
00:00:30,540 --> 00:00:35,160
So in order to do that, we need to go back to our Ajax code right here, which we have.

9
00:00:35,400 --> 00:00:41,610
And right after we execute that particular AJAX request, what we want to do is we want to refresh the

10
00:00:41,610 --> 00:00:44,450
page and basically just refresh the same page.

11
00:00:44,460 --> 00:00:51,810
So whenever you want to be redirected to a new page after an Ajax request, you see window, dot, location,

12
00:00:51,810 --> 00:00:54,150
dot each href.

13
00:00:55,380 --> 00:00:58,680
And then you basically pass in the location where we want to redirect you.

14
00:00:58,710 --> 00:01:03,660
So here in this case, we basically want to be redirected to the feed page, which is this page right

15
00:01:03,660 --> 00:01:04,050
here.

16
00:01:04,170 --> 00:01:06,900
So I'll simply copy this and paste it up over here.

17
00:01:07,410 --> 00:01:11,330
And once this thing is done, let's see what happens and how this works.

18
00:01:11,340 --> 00:01:13,120
So let's go back to the browser here.

19
00:01:13,140 --> 00:01:15,000
First of all, I'm going to hit refresh.

20
00:01:15,090 --> 00:01:21,180
And whenever I unlike this, as you can see, the page is now refreshed and we get the page reloaded

21
00:01:21,180 --> 00:01:22,950
and the like here is removed.

22
00:01:23,070 --> 00:01:27,480
So if I like this now, as you can see, it has this red shaped heart.

23
00:01:27,990 --> 00:01:32,510
Now, let's try and liking Pat's post here and let's see if that like is removed.

24
00:01:32,520 --> 00:01:37,680
So if I click this, if I go down here, as you can see, the like is now removed.

25
00:01:37,710 --> 00:01:39,690
If I again click on like.

26
00:01:40,590 --> 00:01:42,430
The like over here is present.

27
00:01:42,450 --> 00:01:47,190
Now, typically in any application, this is not the kind of experience you would want to get, like

28
00:01:47,190 --> 00:01:51,320
you're just hitting a like button and your entire feed is getting refreshed.

29
00:01:51,330 --> 00:01:52,950
That's not what you want.

30
00:01:53,100 --> 00:01:58,440
But in order to prevent that, what you have to do is you have to use Django in the back end and you

31
00:01:58,440 --> 00:02:02,690
have to use JavaScript in the front end and things become quite tricky over them.

32
00:02:02,700 --> 00:02:07,920
So that means you need react on the front end and react is going to handle the state in the front end

33
00:02:07,920 --> 00:02:11,710
and it's going to pass the response back to the server and the back end to Django.

34
00:02:11,730 --> 00:02:16,530
But as we are not learning react right now, this is what we need to settle with.

35
00:02:16,710 --> 00:02:22,800
So that completes the like functionality of app and the like functionality is working absolutely fine.

36
00:02:23,160 --> 00:02:27,810
So in the next lecture onwards, let's start working on the comment functionality, which basically

37
00:02:27,810 --> 00:02:32,770
allows us to go ahead and comment and allows any user to comment on a particular post.

38
00:02:32,790 --> 00:02:36,030
So let's design the common functionality in the next lecture.

