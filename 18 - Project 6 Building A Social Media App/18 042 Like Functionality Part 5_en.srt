1
00:00:00,090 --> 00:00:05,370
So even before we start learning how to create the common functionality, let's first go ahead and enhance

2
00:00:05,370 --> 00:00:07,060
this particular like functionality.

3
00:00:07,080 --> 00:00:11,830
So let's say you want to display the total number of people who have actually liked the post.

4
00:00:11,880 --> 00:00:14,010
So how can you do that?

5
00:00:14,190 --> 00:00:19,260
So in order to do that, what you could do is you could simply go to your views file and you could find

6
00:00:19,260 --> 00:00:21,810
out the number of people who have actually liked the post.

7
00:00:21,840 --> 00:00:27,480
Or else the other thing which could be done is that you simply could go ahead and count the number of

8
00:00:27,480 --> 00:00:29,260
likes in the templates which you have.

9
00:00:29,280 --> 00:00:34,980
So for example, if you go to the templates file for the feed page, which is this file right here,

10
00:00:35,190 --> 00:00:40,620
right up over here in this particular HTML code itself, you could actually take a look at the total

11
00:00:40,620 --> 00:00:42,470
number of people who have liked the post.

12
00:00:42,480 --> 00:00:44,610
So let's try doing that up over here.

13
00:00:44,640 --> 00:00:49,800
So in order to display the count here, what I would do is that I'll go right beneath the div which

14
00:00:49,800 --> 00:00:55,500
we have up over here, which is the div which is right above the title and the caption.

15
00:00:55,500 --> 00:00:57,240
So here I'll create another div.

16
00:00:57,330 --> 00:01:03,090
And in this particular day we are now actually going to go ahead and count the total number of posts

17
00:01:03,090 --> 00:01:04,440
which are liked by the user.

18
00:01:04,620 --> 00:01:11,100
So I'll go inside this div and we already actually have access to the post here.

19
00:01:11,310 --> 00:01:16,200
And now if I want to check the number of people who have actually liked the post, I could simply go

20
00:01:16,200 --> 00:01:21,360
ahead and make use of the template syntax and see if.

21
00:01:21,940 --> 00:01:23,260
Post start.

22
00:01:24,500 --> 00:01:27,710
Light underscored by dot count.

23
00:01:27,710 --> 00:01:31,350
And this is going to give me the number of people who have actually liked the post.

24
00:01:31,370 --> 00:01:36,950
So we say if post dot like by account, if this is less than one, that means if no person has actually

25
00:01:36,950 --> 00:01:42,770
liked the post and that particular case, we want to leave this entire thing empty or else what we want

26
00:01:42,770 --> 00:01:48,500
to do is that if we have one person who has liked the post, we want to display the name of that person.

27
00:01:48,500 --> 00:01:50,150
So here I will say.

28
00:01:51,270 --> 00:01:56,550
LL if that means if the post don't like by account.

29
00:01:56,550 --> 00:02:00,720
So instead of typing this thing one more time, I'll simply copy it from here.

30
00:02:01,080 --> 00:02:03,420
So, Elif, this is the case.

31
00:02:03,690 --> 00:02:09,660
That is, if this is equal to one in that particular case, we want to say that that particular person

32
00:02:09,660 --> 00:02:10,800
has liked it.

33
00:02:10,919 --> 00:02:15,300
So here, if one person has actually liked the post, we display the name of that person.

34
00:02:15,300 --> 00:02:22,500
So here we say post dot liked and does go by and we want to get the first person.

35
00:02:22,500 --> 00:02:32,850
So I would say dot first and I would say This person likes this or else if multiple people have actually

36
00:02:32,850 --> 00:02:35,270
liked this, we are going to make a check for this as well.

37
00:02:35,280 --> 00:02:41,370
So we will say Elif post dot lite by account of this is greater than one.

38
00:02:41,730 --> 00:02:47,370
If this is greater than one in that particular case, we want to say that the first person as well as

39
00:02:47,370 --> 00:02:49,410
the other people have actually liked it.

40
00:02:49,620 --> 00:02:56,310
That means here I need to say the first person has actually liked the post along with other number of

41
00:02:56,310 --> 00:02:57,330
people as well.

42
00:02:57,660 --> 00:03:02,760
So now in order to get the other number of people, we have to actually go ahead and count those people

43
00:03:02,760 --> 00:03:03,120
here.

44
00:03:03,600 --> 00:03:06,960
So here I would say this and.

45
00:03:08,840 --> 00:03:10,700
Other, which is going to be the count.

46
00:03:10,700 --> 00:03:17,720
So here I would say post dot light by dot count.

47
00:03:19,080 --> 00:03:20,070
Likes this.

48
00:03:20,190 --> 00:03:22,410
Now, let's see how this is going to work.

49
00:03:22,560 --> 00:03:24,050
So I'll simply save this.

50
00:03:24,060 --> 00:03:25,400
Go back to the browser.

51
00:03:25,410 --> 00:03:32,850
And if I had to refresh, as you can see now, it will say that could not pass the remainder from this

52
00:03:32,850 --> 00:03:33,300
one.

53
00:03:33,300 --> 00:03:37,980
So that's I guess because there's no space in between this one.

54
00:03:37,980 --> 00:03:40,620
So if I save this, go back here, I hit refresh.

55
00:03:41,700 --> 00:03:44,430
I think, again, we need to have some space here.

56
00:03:44,430 --> 00:03:49,260
So make sure that you have space between the number and the sign and right now.

57
00:03:49,470 --> 00:03:51,480
And we also are missing space from here.

58
00:03:51,480 --> 00:03:52,450
So let's fix that.

59
00:03:52,470 --> 00:03:57,390
Go back here and I think we forgot to load the end of tag as well.

60
00:03:57,390 --> 00:03:59,280
So let's add the end of tag here.

61
00:03:59,310 --> 00:04:00,830
Go back, hit refresh.

62
00:04:00,840 --> 00:04:03,990
And as you can see now, it's working absolutely fine.

63
00:04:04,350 --> 00:04:07,710
So right now, this thing says that one person has liked it.

64
00:04:07,740 --> 00:04:13,560
Now, let's log out and let's also like this thing as another person and let's see how that works.

65
00:04:13,890 --> 00:04:16,110
So right now, let me log in.

66
00:04:21,260 --> 00:04:23,300
So now I've actually logged in as Pat.

67
00:04:23,300 --> 00:04:25,700
So let me go to post slash feed.

68
00:04:26,510 --> 00:04:30,380
Currently this says one person likes it if I can click on this one.

69
00:04:31,200 --> 00:04:34,290
Now it sees this and two actually like this.

70
00:04:34,440 --> 00:04:37,770
If I click this, it sees one person like this.

71
00:04:37,770 --> 00:04:44,430
But technically, when I click on this button, it should say the first person as well as one other

72
00:04:44,430 --> 00:04:45,000
likes this.

73
00:04:45,000 --> 00:04:48,920
And that's because technically two people have liked it.

74
00:04:48,930 --> 00:04:53,250
But right now this count is showing up, the total number of people who have liked it.

75
00:04:53,520 --> 00:04:56,450
That means now we have to subtract one from this.

76
00:04:56,460 --> 00:05:03,390
However, subtraction is a problem if you actually try to go ahead and if you try to say minus one here,

77
00:05:03,390 --> 00:05:04,710
let's see what happens.

78
00:05:05,100 --> 00:05:07,110
So if I go back here, hit refresh.

79
00:05:07,110 --> 00:05:11,460
As you can see now, it will say could not pass this particular remainder.

80
00:05:11,850 --> 00:05:14,010
So let's try giving some space here.

81
00:05:14,490 --> 00:05:15,440
Hit refresh.

82
00:05:15,450 --> 00:05:17,520
But still, you are going to get an error.

83
00:05:17,520 --> 00:05:23,100
And that's because you cannot perform some addition, subtraction, multiplication or division operation

84
00:05:23,100 --> 00:05:23,550
here.

85
00:05:24,150 --> 00:05:29,220
You can actually perform comparison operation in the template syntax, but you cannot perform mathematical

86
00:05:29,220 --> 00:05:30,420
operations like that.

87
00:05:30,420 --> 00:05:35,820
And in order to perform mathematical operations here, what you need is you need a library which is

88
00:05:35,820 --> 00:05:37,410
called as math filters.

89
00:05:38,130 --> 00:05:40,230
So you have to go back to the terminal.

90
00:05:40,680 --> 00:05:42,090
Stop the server.

91
00:05:42,450 --> 00:05:44,310
Let me clear up the screen here.

92
00:05:45,620 --> 00:05:47,420
And then I would say.

93
00:05:48,650 --> 00:05:55,730
PIP install Django Dash math filters hit enter.

94
00:05:55,760 --> 00:05:57,940
This is going to install math filters for you.

95
00:05:57,950 --> 00:06:02,030
And now once this thing is done, let's rerun the server one more time.

96
00:06:02,270 --> 00:06:07,580
And then whenever you have to use math filters, you have to go back to the template, which is using

97
00:06:07,580 --> 00:06:08,600
matte filters.

98
00:06:08,600 --> 00:06:12,080
And you actually have to load math filters at the top.

99
00:06:12,260 --> 00:06:16,310
So right at the top here, I need to make use of templates, syntax.

100
00:06:16,310 --> 00:06:21,020
And as we are using the math filters library, I would say load math filters.

101
00:06:21,020 --> 00:06:26,240
And also as you're using that app, you also need to mention that in these settings, dot by file as

102
00:06:26,240 --> 00:06:26,520
well.

103
00:06:26,540 --> 00:06:29,060
So let's go inside the settings dot PY file.

104
00:06:29,150 --> 00:06:34,610
And then here I have to say I want to use the app, which is math.

105
00:06:35,280 --> 00:06:36,090
Filters.

106
00:06:36,180 --> 00:06:41,190
Okay, So once we have used the math filters, the next thing which we need to do is we need to go back

107
00:06:41,190 --> 00:06:42,800
to the feed dot HTML.

108
00:06:42,810 --> 00:06:48,390
And instead of performing this kind of subtraction here, the math filter actually allows us to go ahead

109
00:06:48,390 --> 00:06:52,670
and add filters which actually perform this abstraction operation for you.

110
00:06:52,680 --> 00:07:00,150
So in here, instead of saying this count minus one, I have to use math filters by using this horizontal

111
00:07:00,150 --> 00:07:07,470
line and then saying I want to perform a sub and I want to subtract the value of one from this.

112
00:07:07,500 --> 00:07:11,080
Leave some space here and as soon as you do that, this should work.

113
00:07:11,100 --> 00:07:13,230
So let's get back here to refresh.

114
00:07:13,650 --> 00:07:17,080
So right now it's these math filters must be a second tag.

115
00:07:17,100 --> 00:07:18,720
So let's again fix that.

116
00:07:18,720 --> 00:07:21,420
Let's go to v dot HTML.

117
00:07:21,960 --> 00:07:27,930
Take the math filters from here, place it right after the base, and then let's hit refresh.

118
00:07:28,200 --> 00:07:30,570
Now, as you can see, this is working well.

119
00:07:30,570 --> 00:07:33,940
And right now this says this person and one other likes this.

120
00:07:33,960 --> 00:07:37,690
If I dislike this, it sees a single person as like this.

121
00:07:37,710 --> 00:07:42,390
If I click on this now, instead of saying who likes this, this is saying one likes this.

122
00:07:42,630 --> 00:07:47,460
And instead of saying that, I would rather prefer saying and one other.

123
00:07:47,460 --> 00:07:48,810
So here I would say.

124
00:07:49,470 --> 00:07:52,260
And these many other.

125
00:07:53,410 --> 00:07:54,400
Like this.

126
00:07:54,490 --> 00:07:58,710
So now let's see, is this person and one other like this?

127
00:07:58,720 --> 00:08:01,840
That means now the like functionality is fully functional.

128
00:08:01,840 --> 00:08:06,910
So from the next lecture onwards, let's go ahead and let's start working on the common functionality.

129
00:08:07,450 --> 00:08:08,140
Thank you.

