1
00:00:00,090 --> 00:00:05,370
So in this particular lecture, let's style up this page, which is forward slash post forward, slash

2
00:00:05,370 --> 00:00:05,760
create.

3
00:00:05,760 --> 00:00:10,590
And that page is nothing, but it's actually this create dot HTML, which we have up over here.

4
00:00:10,710 --> 00:00:14,280
Now, styling this thing is pretty straightforward and simple.

5
00:00:14,280 --> 00:00:21,060
The only thing that needs to be done is we have to use the field names which we have inside the edit

6
00:00:21,060 --> 00:00:24,380
dot HTML or password change form dot HML.

7
00:00:24,390 --> 00:00:29,990
So you could do that by simply taking the entire form which you have or the entire code which you have.

8
00:00:30,000 --> 00:00:34,830
So let's get that real quick and let's simply go to create and.

9
00:00:35,820 --> 00:00:36,440
Yeah.

10
00:00:36,450 --> 00:00:40,200
Let's replace everything with the code which we currently have.

11
00:00:40,440 --> 00:00:42,840
And let's start making some modifications.

12
00:00:42,840 --> 00:00:48,510
So first and foremost modification is that I change the title of this thing to, let's say, create

13
00:00:48,510 --> 00:00:49,930
a post.

14
00:00:49,950 --> 00:00:52,080
You could also change the emoji here.

15
00:00:52,110 --> 00:00:59,790
Let's say I use this emoji for that particular post because we are uploading an image and here we don't

16
00:00:59,790 --> 00:01:04,050
want this particular error container because this form is not going to have any kind of errors.

17
00:01:04,050 --> 00:01:05,489
So let's get rid of that.

18
00:01:05,610 --> 00:01:11,490
And after that, if you actually want to get the fields which you have up over here, you could simply

19
00:01:11,490 --> 00:01:14,070
inspect them and you will be able to find the field names.

20
00:01:14,070 --> 00:01:16,580
So the field name for this thing is title.

21
00:01:16,920 --> 00:01:19,560
Then we have the image, then caption and post.

22
00:01:19,560 --> 00:01:23,880
So the names of these things are pretty straightforward and you don't have to go through them.

23
00:01:23,880 --> 00:01:26,240
So you simply have to change the labels here.

24
00:01:26,250 --> 00:01:28,740
So the first one is going to be for the title.

25
00:01:28,740 --> 00:01:31,410
So I would say add title.

26
00:01:31,920 --> 00:01:38,910
I will change the form to something like form dot title because this is accepting the post title.

27
00:01:39,030 --> 00:01:40,980
After this we want to accept an image.

28
00:01:40,980 --> 00:01:44,370
So instead of new password, I would change this thing to image.

29
00:01:44,730 --> 00:01:47,880
Let's change the label of this thing to.

30
00:01:49,590 --> 00:01:53,190
Upload image also.

31
00:01:53,220 --> 00:01:56,850
We have to change this thing to caption.

32
00:01:56,850 --> 00:01:59,400
So here I would say add caption.

33
00:02:02,930 --> 00:02:05,720
Let's change the name of the fields to caption.

34
00:02:07,960 --> 00:02:10,820
Now, once this thing is done, we are pretty much good to go.

35
00:02:10,840 --> 00:02:15,910
And one more thing that could be done here is that if you have this form, so let's first take a look

36
00:02:15,910 --> 00:02:17,460
at the form right up over here.

37
00:02:17,470 --> 00:02:20,110
So this is what the form looks like.

38
00:02:20,110 --> 00:02:26,290
And what could be done is that you could actually go ahead and add a kind of shadow and a little bit

39
00:02:26,290 --> 00:02:28,820
of background to this particular form which you have.

40
00:02:28,840 --> 00:02:34,810
So let's go ahead and let's add that extra styling here so you could add that styling right up to this

41
00:02:34,810 --> 00:02:35,560
form here.

42
00:02:35,710 --> 00:02:37,480
So let's add a class to this.

43
00:02:37,480 --> 00:02:40,540
So let's see, the BG of this thing is white.

44
00:02:40,840 --> 00:02:44,950
Let's add some shadow and the shadow is going to be of a medium size.

45
00:02:44,950 --> 00:02:51,580
So shadow, MD If I do that now, as you can see, I'm not sure if you are able to see, but you kind

46
00:02:51,580 --> 00:02:53,020
of have the shadow here.

47
00:02:53,020 --> 00:02:58,360
But you'll also notice that even after adding shadow, there is actually no margin for this form over

48
00:02:58,360 --> 00:03:01,020
here and the fields are kind of stuck to the left.

49
00:03:01,030 --> 00:03:04,390
So let's add a margin of ten as well.

50
00:03:04,390 --> 00:03:07,180
So M10 from all sides.

51
00:03:08,130 --> 00:03:11,280
And I guess you need to add some padding here instead of margin.

52
00:03:11,370 --> 00:03:15,150
So let's also add a padding of eight on the x axis.

53
00:03:15,180 --> 00:03:17,280
Now, it's not kind of cramped up.

54
00:03:17,280 --> 00:03:22,430
And now one more thing that could be done here is that you could actually make this foam as rounded,

55
00:03:22,440 --> 00:03:24,510
so I would say rounded as well.

56
00:03:24,630 --> 00:03:27,900
Let's add some padding at the top as well.

57
00:03:27,930 --> 00:03:30,570
Let's add some padding at the bottom as well.

58
00:03:30,600 --> 00:03:32,520
Let's add some margin at the bottom.

59
00:03:32,520 --> 00:03:34,830
So MB b for.

60
00:03:36,070 --> 00:03:39,540
Okay, so now if I had to refresh, this is what the form is going to look like.

61
00:03:39,550 --> 00:03:44,470
And if you feel that the width of this form is a little bit larger, you could actually also set the

62
00:03:44,470 --> 00:03:45,250
width as well.

63
00:03:45,250 --> 00:03:50,290
So I could make the width of this thing to 4/5 of the original size, which I have.

64
00:03:50,500 --> 00:03:55,420
And if I hit refresh, as you can see, the width of this particular form has decreased a bit.

65
00:03:55,990 --> 00:04:00,790
Now, one more thing which you could do here is that you also have this added user profile form.

66
00:04:00,790 --> 00:04:05,740
And in order to make this thing look better, you could also add the same class to that particular form

67
00:04:05,740 --> 00:04:06,400
as well.

68
00:04:06,460 --> 00:04:13,960
So I could simply go ahead, copy the entire class which I have here, go to edit dot hml and simply

69
00:04:13,960 --> 00:04:17,079
paste that particular class up over here inside this form.

70
00:04:17,350 --> 00:04:24,040
So as I do that, as you can see now, the edit user profile form is still in a much more better way,

71
00:04:24,040 --> 00:04:28,720
and the same could also be done with the other forms as well, which we have.

72
00:04:28,750 --> 00:04:37,900
So for example, if I go to user's forward slash change password or sorry, that should be password

73
00:04:37,900 --> 00:04:38,410
change.

74
00:04:38,410 --> 00:04:42,940
So if I go to password change, this is the form which we have.

75
00:04:42,940 --> 00:04:48,280
But now I could actually go ahead and add that particular styling up right to the form which I have

76
00:04:48,280 --> 00:04:51,130
by simply pasting that particular class.

77
00:04:51,280 --> 00:04:56,950
And if I had refresh, this is what the form is going to look like and it looks pretty neat and clean

78
00:04:56,950 --> 00:04:58,010
as compared to before.

79
00:04:58,030 --> 00:05:01,780
So this is how you could go ahead and style up the pages, which you have.

80
00:05:01,780 --> 00:05:06,060
And there are also a lot of other pages as well, which we are yet to style.

81
00:05:06,070 --> 00:05:10,630
But what you could do is that you could use the same logic which we have been using in the previous

82
00:05:10,630 --> 00:05:11,290
lectures.

83
00:05:11,290 --> 00:05:15,900
Simply go ahead, take these particular forms and convert them and style them up as well.

84
00:05:15,910 --> 00:05:20,650
So it's actually better if you do that on your own, because doing that would be quite repetitive.

85
00:05:20,680 --> 00:05:25,690
So in the next lecture, what we will be doing is that we will be actually creating a common user feed

86
00:05:25,690 --> 00:05:26,250
page.

87
00:05:26,260 --> 00:05:33,240
So currently if you go to users, as you can see, this will only show your very posts here.

88
00:05:33,250 --> 00:05:38,890
But let's say if you want to get the post for all the users which you have up over here registered on

89
00:05:38,890 --> 00:05:40,720
this particular social media website.

90
00:05:40,720 --> 00:05:46,180
So all the posts will be displayed in a single page and that page would be called as a common user feed

91
00:05:46,180 --> 00:05:46,690
page.

92
00:05:46,690 --> 00:05:51,970
So in the next lecture, let's go ahead and let's design a common user feed page which displays posts

93
00:05:51,970 --> 00:05:57,790
from all the users rather than displaying the post of a single user or rather than displaying our very

94
00:05:57,790 --> 00:05:58,680
own posts.

95
00:05:58,690 --> 00:06:00,790
So let's do that in the next lecture.

