1
00:00:00,430 --> 00:00:01,260
In this lecture.

2
00:00:01,260 --> 00:00:04,200
Let's go ahead and create a base template.

3
00:00:04,200 --> 00:00:10,470
And a base template is nothing but a template, which is going to basically be a mean template from

4
00:00:10,470 --> 00:00:13,540
which all the templates which we have are going to extend from.

5
00:00:13,560 --> 00:00:18,630
So, for example, let's say we want to create a navigation bar and we want to add that navigation bar

6
00:00:18,630 --> 00:00:20,460
to all the web pages which we have.

7
00:00:20,460 --> 00:00:25,110
In that particular case, we are going to place that navigation bar inside the base template.

8
00:00:25,200 --> 00:00:30,780
So in this case, we are designing the base template because we want to place the login and log out

9
00:00:30,780 --> 00:00:36,450
options or links there so that we don't have to manually type them in whenever we want to log in or

10
00:00:36,450 --> 00:00:37,380
log out a user.

11
00:00:37,410 --> 00:00:40,070
So let's go ahead and let's create those templates.

12
00:00:40,080 --> 00:00:44,490
So I'll go inside users and create a template called Last base dot HTML.

13
00:00:44,970 --> 00:00:50,770
Now, this template is obviously going to be an HTML template, which is also going to refer to the

14
00:00:50,790 --> 00:00:53,760
some CSS stylings as well in the near future.

15
00:00:53,790 --> 00:00:57,890
Therefore, it must have a load static tag to load all the static assets.

16
00:00:57,900 --> 00:01:04,340
So here I would start with load static and then let's start off by writing some HTML code in here.

17
00:01:04,349 --> 00:01:08,910
So first of all, I'll create an HTML tag here.

18
00:01:08,910 --> 00:01:12,720
So each HTML that's going to be a starting and ending tag.

19
00:01:12,870 --> 00:01:17,400
Let's add a head tag and then let's also add a body tag as well.

20
00:01:17,850 --> 00:01:18,090
Okay.

21
00:01:18,150 --> 00:01:20,970
So once this thing is done, we are pretty much good to go.

22
00:01:21,210 --> 00:01:26,610
And now this particular body tag is going to contain the body of that particular base template.

23
00:01:26,760 --> 00:01:31,860
And in here, what we want to do is we want to display the links for log in and log out.

24
00:01:31,890 --> 00:01:38,040
However, the way in which we want to do that is let's see if the user is logged in, then this template

25
00:01:38,040 --> 00:01:44,130
should only show the option to log out and if the user is already locked out, this particular template

26
00:01:44,130 --> 00:01:45,800
should show the option to log in.

27
00:01:45,810 --> 00:01:51,390
So in order to implement that functionality here, I have to first check if the user is logged in.

28
00:01:51,900 --> 00:01:55,200
And the question is how exactly are you going to check this?

29
00:01:55,380 --> 00:02:00,510
So whenever you render a template, that template has some context.

30
00:02:00,510 --> 00:02:06,000
So the context is nothing, but it has some information about the back end, like the logged in user,

31
00:02:06,030 --> 00:02:08,340
the request object, so on and so forth.

32
00:02:08,370 --> 00:02:14,100
So here we are going to be using the request object and that request object is going to contain the

33
00:02:14,100 --> 00:02:15,550
currently logged in user.

34
00:02:15,570 --> 00:02:19,530
And we could check if that currently logged in user as authenticated or not.

35
00:02:19,740 --> 00:02:22,680
So here I'll write template syntax to check that.

36
00:02:22,860 --> 00:02:25,110
So I'll write an F conditional statement here.

37
00:02:25,110 --> 00:02:31,350
So I would say if request so request, here is the object, which is the request object.

38
00:02:31,500 --> 00:02:37,410
So in this request object, we have user and we could check if that user is actually an authenticated

39
00:02:37,410 --> 00:02:38,190
user or not.

40
00:02:38,190 --> 00:02:43,050
So I would say DOT is underscore, authenticated.

41
00:02:43,740 --> 00:02:46,440
So it basically goes ahead and checks it for us.

42
00:02:46,800 --> 00:02:53,850
Now let's end the if statement here and if and if it finds that, okay, the user is authenticated,

43
00:02:53,850 --> 00:02:58,350
that means in that particular case we have to display a link for logging out the user.

44
00:02:58,350 --> 00:03:03,060
So I'll create an A ref tag which would say log out.

45
00:03:03,060 --> 00:03:06,090
And this is actually going to point to the URL for logout.

46
00:03:06,090 --> 00:03:09,390
And the URL for logout is nothing but this URL right here.

47
00:03:09,390 --> 00:03:15,780
And we have already named this URL as logout, which means we could make use of this name up over here.

48
00:03:16,020 --> 00:03:20,940
So here, in order to specify a URL, again, make use of the template syntax.

49
00:03:20,940 --> 00:03:26,850
And here I want to say that I want to mention a URL here and the URL is going to be nothing but log

50
00:03:26,850 --> 00:03:27,150
out.

51
00:03:27,150 --> 00:03:29,790
So I'll mention logout and single quotes here.

52
00:03:29,790 --> 00:03:33,420
And you cannot use double codes because you have already use them over here.

53
00:03:33,600 --> 00:03:40,650
So instead I would use single codes and say, log out and this log out is nothing, but it's simply

54
00:03:40,650 --> 00:03:45,330
the name of the URL pattern right up over here, which basically is going to render out this view.

55
00:03:46,110 --> 00:03:46,410
Okay.

56
00:03:46,410 --> 00:03:49,140
So once that thing is done, we are done with this.

57
00:03:49,560 --> 00:03:52,470
And we also need to add an else over here as well.

58
00:03:52,470 --> 00:03:58,590
And that's because what happens if the user is not authenticated in that particular case?

59
00:03:58,680 --> 00:04:00,780
We want to display the login button.

60
00:04:00,780 --> 00:04:10,050
So here I would say else I want to have an e ref link, which just like that.

61
00:04:10,710 --> 00:04:17,610
And I want to say that I want to log in if this happens and this should say login.

62
00:04:17,610 --> 00:04:24,600
And this login is nothing, but it's simply the name of the URL which we have here, which is this URL

63
00:04:24,600 --> 00:04:27,840
which renders this view, which is the user login view.

64
00:04:28,080 --> 00:04:34,260
Okay, So once this thing is done now I want to say that I want to have the block body and end block

65
00:04:34,260 --> 00:04:39,270
tags for this, which means that if this template is added to any other template, the first part is

66
00:04:39,270 --> 00:04:44,730
going to start from here, and the rest of the part which extends that template is going to start from

67
00:04:44,730 --> 00:04:45,180
here.

68
00:04:45,180 --> 00:04:52,650
So here I would see block body, which means that the body of the template is going to start from here

69
00:04:52,650 --> 00:04:53,820
and it's going to end here.

70
00:04:53,820 --> 00:04:54,660
So end.

71
00:04:54,780 --> 00:04:56,280
BLOCK Okay.

72
00:04:56,280 --> 00:04:59,810
So once this particular base template is ready, we are pretty much good.

73
00:05:00,510 --> 00:05:05,370
And in the upcoming lecture, we will actually go ahead and add this based templates to whatever templates

74
00:05:05,400 --> 00:05:07,320
we are going to create from here on.

75
00:05:07,800 --> 00:05:11,760
So thank you very much for watching and I'll see you guys in the next one.

76
00:05:11,880 --> 00:05:12,600
Thank you.

