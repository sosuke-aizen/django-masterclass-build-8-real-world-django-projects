1
00:00:00,120 --> 00:00:00,350
Okay.

2
00:00:00,450 --> 00:00:06,810
So in this particular lecture, let's go ahead and let's actually learn how we could trigger this particular

3
00:00:06,810 --> 00:00:12,930
view, which is the light post view whenever we click this particular button right here.

4
00:00:13,230 --> 00:00:19,080
Now, there are multiple ways to do this, but the way in which we are going to do it is by using jQuery

5
00:00:19,080 --> 00:00:20,570
and Ajax.

6
00:00:20,580 --> 00:00:26,370
So we are basically going to get access to this particular documents object, which is this button right

7
00:00:26,370 --> 00:00:26,580
here.

8
00:00:26,580 --> 00:00:31,560
And whenever a click event happens on this particular button, we want to trigger an action and that

9
00:00:31,560 --> 00:00:38,670
action is to basically go ahead and submit a post request to the URL, which is this URL right up over

10
00:00:38,670 --> 00:00:39,060
here.

11
00:00:39,210 --> 00:00:40,770
So let's learn how to do that.

12
00:00:40,770 --> 00:00:44,400
So first of all, let's inspect this particular element right here.

13
00:00:44,490 --> 00:00:48,260
So if we inspect this, this is what the button currently looks like.

14
00:00:48,270 --> 00:00:51,480
So the button is nothing, but it's simply an image.

15
00:00:51,480 --> 00:00:58,620
And right now what we want to do is we basically want to enclose this particular image inside of an

16
00:00:58,620 --> 00:00:59,730
A ref tag.

17
00:01:00,090 --> 00:01:03,160
And there's a specific reason to do that as well.

18
00:01:03,180 --> 00:01:08,390
So let's first go to Vsco and let's go to feed HTML.

19
00:01:08,430 --> 00:01:13,170
Let's actually go ahead and search for that like image, which we have, and we have it right up over

20
00:01:13,170 --> 00:01:13,560
here.

21
00:01:13,860 --> 00:01:16,080
So we'll go ahead and create an image of tag.

22
00:01:16,080 --> 00:01:21,450
And the each ref attribute for this thing is going to be empty, but I'm still going to take this like

23
00:01:21,450 --> 00:01:23,640
image and kind of paste it up over here.

24
00:01:23,670 --> 00:01:27,660
Now, the reason why we are doing this is because right now.

25
00:01:28,550 --> 00:01:29,840
If I hit refresh.

26
00:01:29,840 --> 00:01:35,540
And whenever we click this particular like button, what we essentially want to do is we want to get

27
00:01:35,540 --> 00:01:38,390
the ID of the post, which is associated with this.

28
00:01:38,540 --> 00:01:44,720
Now, the reason why we want to do that is because if you take a look at the view here, here we actually

29
00:01:44,720 --> 00:01:46,940
need that post underscore ID from here.

30
00:01:46,940 --> 00:01:52,340
So in order to get this particular post underscore ID, first of all, we should associate that ID with

31
00:01:52,340 --> 00:01:53,780
every post which we have.

32
00:01:54,020 --> 00:01:56,750
So here I'll go back to v dot HTML.

33
00:01:56,750 --> 00:02:02,660
And in order to associate post ID here, I will simply go ahead and add an ID to this ref.

34
00:02:02,660 --> 00:02:10,910
And here I would say the ID is going to be post dot ID and the class for this thing is going to be BTN.

35
00:02:11,990 --> 00:02:12,740
Like.

36
00:02:13,100 --> 00:02:18,650
And now the reason why we have associated this class is because we are now going to be using jQuery

37
00:02:18,650 --> 00:02:24,350
and jQuery is going to go ahead and get access to this particular element and it's going to look out

38
00:02:24,350 --> 00:02:30,410
or it's going to basically wait and watch for a click events to happen on this particular BTN underscore

39
00:02:30,410 --> 00:02:30,920
like.

40
00:02:31,130 --> 00:02:33,830
So now let's go ahead and let's add jQuery.

41
00:02:33,830 --> 00:02:39,650
So in order to add query, you could either download jQuery or other best thing which you could do is

42
00:02:39,650 --> 00:02:46,250
that you could simply go ahead and get G queries in, which is the content delivery network.

43
00:02:46,250 --> 00:02:52,700
So you could basically get the jQuery link and add it to any of the HTML files which you have here so

44
00:02:52,700 --> 00:02:54,950
that that HTML will be able to use jQuery.

45
00:02:54,950 --> 00:03:02,690
So let's go to Google, I'll search for jQuery and if I go to jQuery and here we actually want the CDN.

46
00:03:02,690 --> 00:03:07,910
So let's directly go on jQuery CDN and here you'll be able to get this CDN link for jQuery.

47
00:03:08,000 --> 00:03:14,150
So here you could actually go ahead and get the uncompressed jQuery link.

48
00:03:14,150 --> 00:03:15,440
So I'll copy this.

49
00:03:16,880 --> 00:03:23,570
And now so as to make Jake very functional across all the entire web pages, I'll actually paste this

50
00:03:23,570 --> 00:03:26,720
particular jQuery link in the based on the HTML file.

51
00:03:26,750 --> 00:03:30,800
So the base dot HTML file actually is present up over here.

52
00:03:30,800 --> 00:03:35,390
So I'll go right there and you actually have to paste this particular link in the head tag.

53
00:03:35,540 --> 00:03:41,690
So I'll go ahead, paste this jQuery link and the head tag which we have here and we should be good

54
00:03:41,690 --> 00:03:42,290
to go.

55
00:03:42,500 --> 00:03:47,930
So once this thing is done now, we could use jQuery in any of the HTML pages, which we have because

56
00:03:47,930 --> 00:03:52,280
all the HTML page anyways inheriting from the based or the HTML.

57
00:03:52,760 --> 00:03:53,090
Okay.

58
00:03:53,090 --> 00:03:57,590
So now once we have query, we could now write some JavaScript code in here.

59
00:03:57,620 --> 00:04:03,170
So right where the card ends, which is right up over here right after this div, I could go ahead and

60
00:04:03,170 --> 00:04:06,770
add a script tag which is going to contain the jQuery or JavaScript code.

61
00:04:06,800 --> 00:04:08,290
So I would say script.

62
00:04:08,300 --> 00:04:13,310
And the reason why we're adding a script tag here at the bottom is because we want to execute the script

63
00:04:13,310 --> 00:04:16,070
when all the content of this page is actually loaded.

64
00:04:16,339 --> 00:04:24,740
So here I would say script the type is equal to text forward slash JavaScript.

65
00:04:24,740 --> 00:04:30,260
And then what we do is in order to ensure if this script is actually working, I'll simply write a simple

66
00:04:30,260 --> 00:04:33,470
JavaScript statement like console.log.

67
00:04:33,530 --> 00:04:36,380
And I would say something like This is working.

68
00:04:37,440 --> 00:04:42,150
Now let's go back to that page, which is the feed page.

69
00:04:42,780 --> 00:04:44,170
I'll inspect this.

70
00:04:44,190 --> 00:04:46,170
Open up my console here.

71
00:04:47,310 --> 00:04:48,480
Let's hit refresh.

72
00:04:48,480 --> 00:04:51,540
And when we hit refresh, it would say, this is working.

73
00:04:52,600 --> 00:04:53,140
Okay.

74
00:04:53,200 --> 00:04:56,260
So once this thing is done, we are pretty much good to go.

75
00:04:56,290 --> 00:05:01,750
That means now we could go ahead and add any kind of JavaScript code in here so you could either comment

76
00:05:01,750 --> 00:05:03,470
this thing out or you could delete it.

77
00:05:03,490 --> 00:05:08,530
So comment this thing out, and then let's first get access to the like button, which we have.

78
00:05:08,650 --> 00:05:14,740
So the like button here is nothing, but it's actually this button right here or this image right here

79
00:05:14,740 --> 00:05:16,450
enclosed in the rev tag.

80
00:05:16,450 --> 00:05:22,620
So we could get access to this entire thing by making use of this BTN like class, which we have.

81
00:05:22,630 --> 00:05:26,740
So here, in order to get access to that, first of all, I'll get access to the document.

82
00:05:27,040 --> 00:05:29,110
So I would see a dollar.

83
00:05:30,610 --> 00:05:31,320
Document.

84
00:05:31,330 --> 00:05:33,970
So this is G query and you don't have to worry about this.

85
00:05:33,970 --> 00:05:35,500
So document on.

86
00:05:36,220 --> 00:05:41,800
And we are looking for the click event and we are looking for the click event on a class which is BTN

87
00:05:41,800 --> 00:05:42,460
like.

88
00:05:42,460 --> 00:05:51,730
So whenever we are using class I would see dot BTN dash like and then here I would say that whenever

89
00:05:51,730 --> 00:05:58,420
this click event happens on BTN, like in that particular case I would execute some kind of code and

90
00:05:58,420 --> 00:06:00,580
that code needs to be defined in the function.

91
00:06:00,580 --> 00:06:05,860
So I would say function here and then I would mention all the code for that particular thing up over

92
00:06:05,860 --> 00:06:06,190
here.

93
00:06:06,190 --> 00:06:09,700
So let's see what we want to do whenever the click event happens.

94
00:06:09,700 --> 00:06:15,790
So whenever the click event happens, we basically want to get the post ID and the post ID is nothing,

95
00:06:15,790 --> 00:06:21,400
but it's actually this ID which is associated with this one.

96
00:06:21,580 --> 00:06:28,630
So now in order to get access to this here, I simply have to say this dot ID and it will actually give

97
00:06:28,630 --> 00:06:29,980
me access to that ID.

98
00:06:30,010 --> 00:06:33,220
So if you don't believe that, I would console.log that.

99
00:06:33,220 --> 00:06:34,870
So I would say console.

100
00:06:35,860 --> 00:06:37,960
Dot log and let's see.

101
00:06:38,990 --> 00:06:41,880
This dot ID and let's see, what do we actually get there?

102
00:06:41,900 --> 00:06:43,410
So again, inspect this.

103
00:06:43,430 --> 00:06:45,080
Open up the JavaScript console.

104
00:06:45,080 --> 00:06:48,350
So whatever JavaScript results we have, they are going to show up over here.

105
00:06:48,620 --> 00:06:53,810
And right now, it's not actually showing up any kind of like button.

106
00:06:53,810 --> 00:06:55,730
So let me minimize this.

107
00:06:55,730 --> 00:07:01,130
So when I click on this button, as you can see, even though it's not visible because we have not made

108
00:07:01,130 --> 00:07:06,200
it responsive, when I click the like button of the first post, I'll get one here.

109
00:07:06,930 --> 00:07:09,210
So let me actually minimize this.

110
00:07:09,300 --> 00:07:09,900
Okay.

111
00:07:10,530 --> 00:07:16,090
And now, if I go to the second post here, if I click on that one, I'll get the idea of that post.

112
00:07:16,110 --> 00:07:18,690
So, again, if I go back here, I'll click on this one.

113
00:07:18,690 --> 00:07:20,280
I'll get the idea of this one.

114
00:07:20,640 --> 00:07:21,750
Let's click on this one.

115
00:07:21,750 --> 00:07:23,350
I would get the idea three.

116
00:07:23,370 --> 00:07:26,430
That means now we are successfully getting that post ID.

117
00:07:26,700 --> 00:07:29,990
So now let's save this post ID into some kind of variable.

118
00:07:30,000 --> 00:07:33,150
So I'll create a variable called us post underscore ID.

119
00:07:33,180 --> 00:07:39,570
And I would say that this is this dot ID, which means it gets the ID of the current post, which we

120
00:07:39,570 --> 00:07:39,950
have.

121
00:07:39,960 --> 00:07:48,210
And now here, once we have this post ID now we simply have to go ahead and submit a URL request to

122
00:07:48,210 --> 00:07:50,760
the URL pattern, which is like.

123
00:07:51,120 --> 00:07:55,710
So the URL pattern which we are looking for is forward slash.

124
00:07:56,670 --> 00:07:59,190
Posts forward slash.

125
00:08:00,150 --> 00:08:03,200
Like and we basically want to make a post request here.

126
00:08:03,210 --> 00:08:07,410
So now in order to make that particular request, let's write some AJAX code in here.

127
00:08:07,620 --> 00:08:12,090
So here, in order to write Ajax, I would say Dollar dot Ajax.

128
00:08:12,450 --> 00:08:14,300
And then I'll write some code in here.

129
00:08:14,310 --> 00:08:20,850
So first of all, as we want to make a post request on a specific URL, I would say method as post,

130
00:08:20,850 --> 00:08:26,430
which means that we want to make an AJAX post request and then you want to specify the URL where you

131
00:08:26,430 --> 00:08:27,450
want to make that request.

132
00:08:27,450 --> 00:08:28,800
So I would say URL.

133
00:08:28,950 --> 00:08:33,570
And I want to make a request on forward slash post forward slash like.

134
00:08:33,570 --> 00:08:39,030
And whenever we are making this post request on this URL, we also want to pass on some data as well.

135
00:08:39,059 --> 00:08:44,430
So for example, if you want to pass in the post ID here because that's what is actually needed up over

136
00:08:44,430 --> 00:08:50,010
here, which is post ID and that ID is ultimately extracted from the post request.

137
00:08:50,010 --> 00:08:53,150
So we also need to pass that post ID over here as well.

138
00:08:53,160 --> 00:08:59,190
So in order to pass that thing in, I'll add a data parameter here and I would say that the post underscore

139
00:08:59,220 --> 00:09:04,380
ID is going to be nothing but post ID, which is from here.

140
00:09:05,330 --> 00:09:11,660
So we have passed this, but also as we are making a post request, we also need to ensure that we pass

141
00:09:11,700 --> 00:09:13,320
a CSR token as well.

142
00:09:13,340 --> 00:09:15,860
That means we have to first create that token.

143
00:09:15,890 --> 00:09:21,270
So I would say window dot that's going to be see as a ref.

144
00:09:21,830 --> 00:09:27,260
And the school token is equal to that's going to be nothing but.

145
00:09:28,530 --> 00:09:29,380
See us.

146
00:09:29,670 --> 00:09:32,090
I have underscore token.

147
00:09:32,880 --> 00:09:33,180
Okay.

148
00:09:33,180 --> 00:09:36,330
So once this thing is done now, we could pass in that token here.

149
00:09:36,330 --> 00:09:40,050
So here I would see us RF middle.

150
00:09:41,100 --> 00:09:48,090
Were token and this is going to be nothing but window dots is RF token.

151
00:09:48,310 --> 00:09:51,390
Okay, So once this thing is done, we are pretty much good to go.

152
00:09:51,420 --> 00:09:56,490
Now what happens is whenever I click a particular button, this post request is going to be submitted

153
00:09:56,490 --> 00:10:00,810
and this view is going to be triggered, which is going to take the logged in user and it's actually

154
00:10:00,810 --> 00:10:05,580
going to take the post and add that particular user to the liked by of that post.

155
00:10:05,730 --> 00:10:07,530
So let's see if this thing works.

156
00:10:07,580 --> 00:10:09,180
Well, first of all, hit refresh.

157
00:10:09,300 --> 00:10:14,640
Now, we actually logged in as the super user and now let's try clicking this button right here.

158
00:10:15,090 --> 00:10:18,920
So I'll click here and we do get some error here.

159
00:10:18,930 --> 00:10:21,130
So let's see what kind of error do we have?

160
00:10:21,150 --> 00:10:26,700
So now the problem here is that in the view which we have, we have not returned anything.

161
00:10:27,840 --> 00:10:35,190
So if I go to The View, which is this, we have made this particular function and we are not returning

162
00:10:35,190 --> 00:10:37,110
anything up over here as of now.

163
00:10:37,320 --> 00:10:38,610
So we are going to do that.

164
00:10:38,610 --> 00:10:41,870
But let's see if the like is actually added.

165
00:10:41,880 --> 00:10:48,690
So I'll go to the admin here and I'll go to posts and I want to take a look at the post for Pat.

166
00:10:48,690 --> 00:10:51,330
And Pat has this post which is called a title.

167
00:10:51,720 --> 00:10:56,880
So if I go to title now, as you can see, this particular user is now highlighted.

168
00:10:56,880 --> 00:11:05,070
That means this particular user has liked Pat's post and now if I go back to the browser, let me go

169
00:11:05,070 --> 00:11:07,200
back hit refresh on this one.

170
00:11:07,500 --> 00:11:11,460
And let me click on this one one more time and let's see what happens.

171
00:11:11,460 --> 00:11:16,050
So when I click on this one more time, we do get an error because of that return statement.

172
00:11:16,200 --> 00:11:21,210
But now if I go back here and hit refresh, as you can see that like from here is removed.

173
00:11:21,660 --> 00:11:25,350
And if I again emulate that action one more time, let's see what happens.

174
00:11:25,350 --> 00:11:26,490
So I'll click here.

175
00:11:26,910 --> 00:11:32,220
Now we do get an error, but if I go back here, hit refresh that user again.

176
00:11:32,220 --> 00:11:34,290
Like that particular post by Pat.

177
00:11:34,560 --> 00:11:37,010
That means the like functionality is working well.

178
00:11:37,020 --> 00:11:42,030
Now the next thing which we need to do is in order to avoid that error, I simply have to add a return

179
00:11:42,030 --> 00:11:42,720
statement here.

180
00:11:42,720 --> 00:11:45,360
So I would want to return redirect.

181
00:11:46,840 --> 00:11:49,370
And I want to redirect to a specific URL.

182
00:11:49,390 --> 00:11:52,350
So let's say I want to return back to this feed page.

183
00:11:52,360 --> 00:11:54,460
So I would say return, redirect, feed.

184
00:11:54,550 --> 00:11:56,560
And we have not imported redirect.

185
00:11:56,560 --> 00:11:58,600
So let's do that real quick.

186
00:11:58,720 --> 00:12:05,170
So here from Django dot shortcuts, I would say import, redirect.

187
00:12:05,530 --> 00:12:09,040
Now that error should be fixed and we should no longer have any error here.

188
00:12:09,460 --> 00:12:12,040
So let's click on this icon right here.

189
00:12:12,070 --> 00:12:14,030
And as you can see, nothing happened.

190
00:12:14,050 --> 00:12:16,190
Let's see if the status here changed.

191
00:12:16,210 --> 00:12:20,710
Yeah, that actually changed to unlike let's click on this one.

192
00:12:21,160 --> 00:12:23,080
Now it will see light.

193
00:12:23,110 --> 00:12:26,560
Now let's go ahead and log out and then let's log in as Rob.

194
00:12:26,560 --> 00:12:27,220
One, two, three.

195
00:12:27,220 --> 00:12:29,950
And let's see if Rob is actually able to like this post.

196
00:12:30,250 --> 00:12:32,860
So here, I'll go back, log out.

197
00:12:33,730 --> 00:12:35,260
Uh, let's try logging in.

198
00:12:35,260 --> 00:12:36,700
So log in as Rob here.

199
00:12:36,730 --> 00:12:38,050
Rob One, two, three.

200
00:12:38,620 --> 00:12:40,990
Password is super user.

201
00:12:41,320 --> 00:12:42,370
Let's log in.

202
00:12:42,940 --> 00:12:44,650
Now let's go to feed.

203
00:12:45,590 --> 00:12:47,900
That is Post's forward slash feed.

204
00:12:50,330 --> 00:12:56,690
Okay, let's go down to this particular path post and I'll now click on this icon.

205
00:12:57,260 --> 00:12:59,960
And now the admin won't be accessible because we are logged out.

206
00:12:59,960 --> 00:13:02,260
So we again have to log back in as admin.

207
00:13:02,270 --> 00:13:03,520
So I'm logged back in.

208
00:13:03,530 --> 00:13:07,640
Let's go to posts, let's go to Patch post.

209
00:13:07,760 --> 00:13:13,790
And as you can see now inside liked by it's also showing up a Rob one, two three as well because now

210
00:13:13,790 --> 00:13:15,340
Rob has actually liked this post.

211
00:13:15,350 --> 00:13:18,680
That means the like functionality is working absolutely fine.

212
00:13:18,860 --> 00:13:25,760
But now the main problem here is that whenever a particular user clicks on this one or this particular

213
00:13:25,760 --> 00:13:31,730
icon right here, the user is not able to see if the post is liked or not because the icon is not changing

214
00:13:31,730 --> 00:13:32,420
any color.

215
00:13:32,570 --> 00:13:38,750
So in the next lecture, let's go ahead and let's see how to change the icons color to red when the

216
00:13:38,750 --> 00:13:42,140
post is liked and when to keep it white when the post is unliked.

217
00:13:42,140 --> 00:13:45,110
So let's implement that functionality in the next one.

