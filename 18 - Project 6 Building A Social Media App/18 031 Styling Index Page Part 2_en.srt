1
00:00:00,090 --> 00:00:03,660
And now let's finish styling up this particular web page, which we have set.

2
00:00:03,870 --> 00:00:11,370
So I'll start off by styling up this particular field, which is this particular block right here for

3
00:00:11,370 --> 00:00:12,750
the image and the user name.

4
00:00:12,750 --> 00:00:17,970
So for that particular thing, what we need to do is, first of all, as we want to align them to the

5
00:00:17,970 --> 00:00:19,850
side and not on top of each other.

6
00:00:19,860 --> 00:00:25,290
So there's one thing which needs to be fixed here, and that is this div which we have created for the

7
00:00:25,290 --> 00:00:25,920
image.

8
00:00:25,920 --> 00:00:30,810
This should actually also encapsulate the two divs which we have created right up over here.

9
00:00:30,810 --> 00:00:33,060
So first of all, let's go ahead and do that.

10
00:00:33,090 --> 00:00:35,580
So let's make this div the outermost div.

11
00:00:35,580 --> 00:00:42,840
So I'll cut the div which is this div from here, go right inside this major div pasted up over here

12
00:00:42,840 --> 00:00:50,070
such that we have this div first, which is this one, and then we have the image and the post user

13
00:00:50,070 --> 00:00:52,560
and then we have the image right up over here.

14
00:00:52,560 --> 00:00:57,960
So if I save this, this is it will look the same, but the structure is actually now changed.

15
00:00:58,440 --> 00:01:05,129
Okay, so now once we have this particular div now to align these things to the side of each other,

16
00:01:05,129 --> 00:01:07,650
I simply have to change the type of this thing to flex.

17
00:01:07,650 --> 00:01:13,110
So if I change this thing to flex, hit refresh, this is now going to be a line right next to each

18
00:01:13,110 --> 00:01:13,560
other.

19
00:01:13,890 --> 00:01:19,290
Now, after changing this, let's add some margin and padding from the x and Y axis.

20
00:01:19,290 --> 00:01:22,560
So let's add a margin and padding of PHI from both.

21
00:01:22,770 --> 00:01:28,110
Now, as you can see, it has some margin Now for this particular image, which we have, let's set

22
00:01:28,110 --> 00:01:30,330
a proper width and height for this image class.

23
00:01:30,330 --> 00:01:32,160
So here I would say image class.

24
00:01:32,160 --> 00:01:38,940
So let's set the height of this thing to ten and let's also set the width to ten as well.

25
00:01:38,940 --> 00:01:41,520
So it's going to make it sort of a square shape.

26
00:01:42,310 --> 00:01:45,560
So as you can see, this is what the image is going to look like.

27
00:01:45,580 --> 00:01:50,560
Now, the image which I had selected was already rounded, but in case if the image is not rounded,

28
00:01:50,560 --> 00:01:52,330
we have to manually make it rounded.

29
00:01:52,330 --> 00:01:57,640
And for that purpose I would set the rounded property of this thing as full.

30
00:01:57,640 --> 00:01:59,440
So I would say rounded dashboard.

31
00:02:00,460 --> 00:02:03,880
Now, once this thing is done, let's style up this user name right here.

32
00:02:04,150 --> 00:02:06,580
So first of all, I'll add some margin over here.

33
00:02:06,580 --> 00:02:11,770
So I would say margin on the x axis is two and margin on the Y axis is two as well.

34
00:02:12,040 --> 00:02:18,910
I want to make the font as bold and if I save this, I would hit refresh and this is what the margin

35
00:02:18,910 --> 00:02:19,780
is going to look like.

36
00:02:19,810 --> 00:02:25,210
Now, one more thing that could be done here is that let's say if you want to change the color of this

37
00:02:25,210 --> 00:02:28,050
thing, I could actually create a span tag here.

38
00:02:28,060 --> 00:02:34,100
So I would create a span tag and I could get the post dot user inside that particular span tag.

39
00:02:34,120 --> 00:02:40,180
So let's get the post user, which is nothing but the Post's username and the inside the span tag.

40
00:02:40,510 --> 00:02:43,780
And inside this particular span I could assign a class.

41
00:02:43,780 --> 00:02:46,320
So let's say I want the text to be indigo.

42
00:02:46,330 --> 00:02:50,980
So text indigo with a shade of 500.

43
00:02:51,400 --> 00:02:56,710
So if I go back and refresh, this is what the text is going to look like, which is exactly what we

44
00:02:56,710 --> 00:02:57,040
want.

45
00:02:57,070 --> 00:03:00,420
Now we have the post user, the profile picture of that user, the post.

46
00:03:00,430 --> 00:03:00,730
Okay.

47
00:03:00,730 --> 00:03:06,640
So now once we have this, let's actually make this entire post centered in a way that does not span

48
00:03:06,640 --> 00:03:08,120
the entire width of the page.

49
00:03:08,140 --> 00:03:12,070
So for that, we have already turn this thing into a grid of column one.

50
00:03:12,070 --> 00:03:17,170
And now the only thing that needs to be done is that we need to add some margin on the x axis.

51
00:03:17,170 --> 00:03:19,630
So let's say the margin over here is 60.

52
00:03:19,900 --> 00:03:25,900
And as soon as I do that and hit refresh, as you can see now, the post appears right in the center

53
00:03:26,110 --> 00:03:28,780
and we do not have any kind of space on the left hand.

54
00:03:28,780 --> 00:03:29,110
Right.

55
00:03:29,110 --> 00:03:30,910
And it looks pretty good.

56
00:03:31,810 --> 00:03:35,260
But now you'll also be able to see that this does not have any kind of padding.

57
00:03:35,260 --> 00:03:38,620
So let's also set a padding of ten to it.

58
00:03:38,920 --> 00:03:42,680
If I do that at refresh now, it looks even better.

59
00:03:42,700 --> 00:03:47,470
Now, once we have styled this thing up in the next lecture, let's actually go ahead and let's add

60
00:03:47,470 --> 00:03:49,720
some social icons to our post.

61
00:03:49,720 --> 00:03:55,030
Like the like button, the common button and the share button to every post which we have up over here.

62
00:03:55,270 --> 00:03:57,910
So let's go ahead and add that in the upcoming lecture.

