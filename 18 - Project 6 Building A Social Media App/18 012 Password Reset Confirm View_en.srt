1
00:00:00,090 --> 00:00:04,860
Okay, so in this lecture, let's work on fixing this password reset, confirm, view error, which

2
00:00:04,860 --> 00:00:05,490
we are getting.

3
00:00:05,490 --> 00:00:11,460
So basically what this error say is, is that whenever you want to reset a particular password, you

4
00:00:11,460 --> 00:00:14,040
actually have to pass on this particular URL right here.

5
00:00:14,040 --> 00:00:16,260
And you also have to use that view as well.

6
00:00:16,410 --> 00:00:23,250
And if you take a look at this, this URL actually contains the name of a protocol, which is HTTP and

7
00:00:23,250 --> 00:00:24,780
the domain name of our website.

8
00:00:24,780 --> 00:00:29,970
So currently we are using local host and then it actually accepts a URL pattern.

9
00:00:29,970 --> 00:00:35,940
And that URL pattern is nothing, but it's basically the link which is password reset confirm.

10
00:00:35,940 --> 00:00:42,060
That means we now need to set up a URL pattern for password reset, confirm, and then we also need

11
00:00:42,060 --> 00:00:49,560
to pass in a UID, which is having a base64 encoding and we also need to pass an ID token as well.

12
00:00:49,950 --> 00:00:53,130
So now the question is how exactly this could be done.

13
00:00:53,250 --> 00:00:58,560
So in order to do that, first of all, we need to add a password reset, confirm view.

14
00:00:58,680 --> 00:01:06,060
So in order to add that, let's actually go back to VTS code and here let's add another path.

15
00:01:06,060 --> 00:01:13,290
So I'll go back here, add a new path here and this time and this particular path, let's keep this

16
00:01:13,290 --> 00:01:14,100
path as empty.

17
00:01:14,110 --> 00:01:19,860
We are going to set it up later, but for now, let's use the art view and let's see if we have something

18
00:01:19,860 --> 00:01:21,930
like password reset, confirm view.

19
00:01:22,860 --> 00:01:24,630
So I would type in password.

20
00:01:25,500 --> 00:01:28,050
Reset, confirm, and we do have that.

21
00:01:28,050 --> 00:01:30,300
So I will use this thing as view.

22
00:01:31,170 --> 00:01:40,890
And once we have this thing as as view, let's name this thing as password reset, confirm.

23
00:01:40,890 --> 00:01:45,600
So password reset, confirm actually ends up being the name of that particular URL path.

24
00:01:45,690 --> 00:01:46,110
Okay.

25
00:01:46,110 --> 00:01:50,130
So now for this particular path, we need to set up a URL pattern.

26
00:01:50,310 --> 00:01:55,860
So as I earlier mentioned, this URL pattern needs to have it needs to have two parameters.

27
00:01:55,860 --> 00:02:02,160
So first parameter needs to be the UID, which is going to be of base64 encoding.

28
00:02:02,160 --> 00:02:04,320
And then we need to pass in the token as well.

29
00:02:04,560 --> 00:02:09,330
So in order to pass these particular parameters here, I'll first set up the path.

30
00:02:09,330 --> 00:02:14,370
So let's say this is reset forward slash, let's pass in the UID first.

31
00:02:14,370 --> 00:02:21,990
So UID base 64 and then the next parameter which we want to pass in here is going to be the token.

32
00:02:21,990 --> 00:02:27,330
Now, just as we have passed in the template for this view, we also need to pass in the template for

33
00:02:27,330 --> 00:02:28,350
this view as well.

34
00:02:28,440 --> 00:02:35,130
And this view is nothing, but it's simply a view which actually renders out a form which asks us to

35
00:02:35,130 --> 00:02:36,720
enter a new password.

36
00:02:36,720 --> 00:02:42,840
So for example, whenever we want to reset a password, we type in a new password and we also confirm

37
00:02:42,840 --> 00:02:44,100
that password as well.

38
00:02:44,100 --> 00:02:47,820
And that's exactly the form which this particular view renders.

39
00:02:47,820 --> 00:02:50,160
So here let's create a template for this one.

40
00:02:50,370 --> 00:02:56,400
And as usual, you know, creating a template is quite simple, but this template is going to be a little

41
00:02:56,400 --> 00:02:57,270
bit different.

42
00:02:57,510 --> 00:03:06,030
So let's create a template, and this template needs to be named as password reset, confirm, dot,

43
00:03:06,300 --> 00:03:06,900
HTML.

44
00:03:08,280 --> 00:03:13,230
And then in this particular template, as usual, you have the block body and n block, which you have.

45
00:03:13,230 --> 00:03:19,200
So let's copy the code from here, paste it up over here, and instead of this one, let's have a form.

46
00:03:19,470 --> 00:03:25,170
But even before presenting them with a form, we first need to check if the URL which is being passed

47
00:03:25,350 --> 00:03:27,090
is actually a valid URL.

48
00:03:27,090 --> 00:03:30,990
And in order to check that, we have to make an F statement here.

49
00:03:31,440 --> 00:03:34,680
So we say if the link is valid.

50
00:03:34,680 --> 00:03:41,160
So we say if a valid link in that particular case only then render this particular form.

51
00:03:41,400 --> 00:03:44,310
So we will also end this statement as well.

52
00:03:44,760 --> 00:03:45,870
So end.

53
00:03:47,540 --> 00:03:54,800
If and then let's render the form right here and the form method is going to be equal to post.

54
00:03:55,040 --> 00:03:56,990
So the method is post here.

55
00:03:57,620 --> 00:04:00,860
And then we, as usual, have the CSR of token.

56
00:04:00,860 --> 00:04:07,420
So see as our of underscore token and then we render the form as paragraph.

57
00:04:07,430 --> 00:04:11,240
So form dot as underscore p, which is as usual.

58
00:04:11,480 --> 00:04:17,060
And then once the form is rendered, we also need to have an input type submit so as to submit the new

59
00:04:17,060 --> 00:04:17,760
password.

60
00:04:17,779 --> 00:04:19,610
So this is going to be submit.

61
00:04:19,760 --> 00:04:26,060
And if this is not true, we also need to have an AE, which basically states that if your form is not

62
00:04:26,060 --> 00:04:31,110
being rendered, that means the password reset link which was sent to you is actually invalid.

63
00:04:31,130 --> 00:04:32,780
So here we say else.

64
00:04:34,100 --> 00:04:40,880
I want to render this thing which says password reset link is invalid.

65
00:04:41,840 --> 00:04:45,470
Okay, so once this thing is done, we are pretty much good to go.

66
00:04:45,500 --> 00:04:50,210
We now need to pass in the name of this template to this particular view.

67
00:04:50,390 --> 00:05:00,080
So here I would say a template name is going to be nothing but password reset, confirm dot HTML.

68
00:05:00,110 --> 00:05:04,040
So now let's go back here and let's see if we still get that error.

69
00:05:04,040 --> 00:05:07,160
And if we still get that error, we are going to fix that later.

70
00:05:07,670 --> 00:05:12,920
So for now, let's go back all the way back to password reset.

71
00:05:13,280 --> 00:05:14,850
Let's type in the email here.

72
00:05:14,870 --> 00:05:20,570
If I click on Submit, as you can see now, we do not get any error because we have now added the password

73
00:05:20,570 --> 00:05:27,140
reset view and we also get a message which sees we have mailed you the password reset link, but technically

74
00:05:27,140 --> 00:05:31,160
you won't get an email because we have not configured it to send an email.

75
00:05:31,160 --> 00:05:34,910
Instead, we have configured it to send an output in the terminal.

76
00:05:34,970 --> 00:05:38,990
So let's open up our terminal or command prompt whatever you are using.

77
00:05:38,990 --> 00:05:44,750
And here, as you can see, we get that particular reset password link in the terminal itself.

78
00:05:44,900 --> 00:05:51,560
So now let's copy the password and in case if we have forgotten the username, they will also display

79
00:05:51,560 --> 00:05:53,240
the username over here as well.

80
00:05:53,570 --> 00:05:56,540
So now let's copy and paste this particular link here.

81
00:05:56,900 --> 00:06:03,320
And as soon as I hit enter, as you can see now, it sees we do not have a template which is password

82
00:06:03,320 --> 00:06:07,490
reset confirmed HTML, but we do actually have that.

83
00:06:08,060 --> 00:06:08,420
Okay.

84
00:06:08,420 --> 00:06:12,860
So we are getting that error because we have not mentioned users here.

85
00:06:12,860 --> 00:06:18,980
So if we do that and if we hit refresh, as you can see, we actually get that password reset confirm

86
00:06:18,980 --> 00:06:19,430
page.

87
00:06:19,430 --> 00:06:21,910
And here we have to type in the new password.

88
00:06:21,920 --> 00:06:28,520
So let's say the new password I want to set is super user and I want to set the new password as super

89
00:06:28,520 --> 00:06:28,970
user.

90
00:06:28,970 --> 00:06:30,380
So I confirm it twice.

91
00:06:30,380 --> 00:06:33,290
And if I had submit the password is actually reset.

92
00:06:33,290 --> 00:06:38,960
But now it's again going to ask us for another view, which is password reset, complete view.

93
00:06:39,290 --> 00:06:44,360
And the password reset complete view is nothing, but it's simply a view which say something like,

94
00:06:44,360 --> 00:06:51,200
okay, your password is now being completely changed and the password reset process has been successful.

95
00:06:51,350 --> 00:06:53,300
So we simply have to create that view.

96
00:06:53,300 --> 00:06:57,830
So we are going to learn how to create the password reset complete view in the next lecture.

