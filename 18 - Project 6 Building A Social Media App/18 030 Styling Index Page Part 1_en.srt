1
00:00:00,090 --> 00:00:05,430
So in this particular lecture, let's style up this index page, which we have, which basically is

2
00:00:05,430 --> 00:00:07,810
the page to view our own posts.

3
00:00:07,830 --> 00:00:14,040
So currently, as you can see, this simply displays the title and the caption of a particular image

4
00:00:14,040 --> 00:00:17,700
which we have, but we want to style it in a much more better way.

5
00:00:17,870 --> 00:00:22,800
Then we could view all of the posts which we made all of the images along with their title and captions

6
00:00:22,800 --> 00:00:26,460
in a manner that this looks like a social media website.

7
00:00:26,460 --> 00:00:29,100
So let's go ahead and let's start sidling this thing up.

8
00:00:29,340 --> 00:00:35,100
So the main file, which controls the look and feel of this is actually the index dot HTML template.

9
00:00:35,100 --> 00:00:37,470
So here we are into that particular template.

10
00:00:37,470 --> 00:00:43,900
And currently we do have loaded the base template into it and we have also loaded the blog body and

11
00:00:43,900 --> 00:00:44,850
then blog as well.

12
00:00:45,180 --> 00:00:51,600
So for now, let's keep these things handy, which is the fields which we have used to access the image

13
00:00:51,600 --> 00:00:54,350
and the title and the user, so on and so forth.

14
00:00:54,360 --> 00:01:00,360
So let's actually cut it and let's paste it up over here for the time being, and let's start designing

15
00:01:00,360 --> 00:01:02,280
the page right away, up over here.

16
00:01:02,550 --> 00:01:08,310
So the first thing which we want to design here is that we want to design a certain heading over here.

17
00:01:08,310 --> 00:01:13,860
So we'll enclose that particular heading in a div and then we'll have an H two over here, which is

18
00:01:13,890 --> 00:01:14,840
a heading tag.

19
00:01:14,850 --> 00:01:18,690
So let's say this thing sees something like my posts.

20
00:01:19,140 --> 00:01:25,410
If I go ahead, do that, if I head back to the browser, you'll be able to see that we have my posts

21
00:01:25,410 --> 00:01:29,250
up over here and we kind of have it twice.

22
00:01:29,250 --> 00:01:32,040
And that's because I think we also have this for here.

23
00:01:32,040 --> 00:01:35,490
So let's get rid of this fall from here for the moment.

24
00:01:35,490 --> 00:01:36,960
Let's piece it up over here.

25
00:01:36,960 --> 00:01:42,600
Let's also cut the end for tag as well from here, and let's pace it up over here for the time being.

26
00:01:42,990 --> 00:01:43,380
Okay.

27
00:01:43,380 --> 00:01:49,580
So if I go back now, hit refresh now we have my post one time and let's get rid of this title, which

28
00:01:49,740 --> 00:01:51,870
is home page for users app.

29
00:01:51,900 --> 00:01:52,380
Okay?

30
00:01:52,380 --> 00:01:55,110
So once this thing is done, we are pretty much good to go.

31
00:01:55,320 --> 00:02:00,870
So now the next thing which we want to do is that let's say we want to center this particular element,

32
00:02:00,870 --> 00:02:02,340
which is my posts.

33
00:02:02,340 --> 00:02:07,440
So in order to center this thing up, I could actually send up the div which actually contains it.

34
00:02:07,440 --> 00:02:15,180
So here I would make this thing a flex and then I would say that I want to justify the items inside

35
00:02:15,180 --> 00:02:15,840
it to center.

36
00:02:15,840 --> 00:02:17,160
So justify.

37
00:02:18,360 --> 00:02:22,590
Dash center is the class I would be using if I do that.

38
00:02:22,710 --> 00:02:24,960
Now, as you can see, this thing is centered.

39
00:02:25,200 --> 00:02:31,170
Now you'll be able to notice that there's no margin for this particular title at the top.

40
00:02:31,200 --> 00:02:36,430
So if I want to add margin at the top, I would simply say empty dash.

41
00:02:36,450 --> 00:02:37,710
Let's say five.

42
00:02:38,400 --> 00:02:40,750
And now the margin at the top would be five.

43
00:02:40,770 --> 00:02:43,890
And also, let's say I want to add a padding from all the sides.

44
00:02:43,890 --> 00:02:45,480
So I would say padding ten.

45
00:02:46,180 --> 00:02:48,610
Now it will have a padding from all the sides.

46
00:02:48,940 --> 00:02:52,360
So now the heading of this thing is pretty much ready.

47
00:02:52,360 --> 00:02:56,070
And now let's go ahead and let's set up the actual heading here.

48
00:02:56,080 --> 00:03:00,550
So the heading, as you might notice, it looks pretty small in size.

49
00:03:00,550 --> 00:03:05,950
So if I want to increase the text size of this thing, I would simply set the class of this thing to

50
00:03:06,190 --> 00:03:11,350
text Dash Excel that will actually set the size of thing to extra large.

51
00:03:11,350 --> 00:03:16,000
But let's say if I want even bigger font, I could change it to three times excel.

52
00:03:16,180 --> 00:03:18,070
So it looks even bigger.

53
00:03:18,700 --> 00:03:19,360
Now.

54
00:03:19,360 --> 00:03:24,190
Let's say I want to make this font or I want to set the font weight to normal.

55
00:03:24,190 --> 00:03:25,630
So I would say font.

56
00:03:26,900 --> 00:03:27,770
Normal.

57
00:03:28,410 --> 00:03:33,990
And then let's also set some margin at the top or maybe at the bottom, because at the bottom you're

58
00:03:33,990 --> 00:03:34,920
going to have post.

59
00:03:34,920 --> 00:03:36,060
So me too.

60
00:03:36,240 --> 00:03:41,610
And let's change the text color to gray with a shade of 500.

61
00:03:42,600 --> 00:03:45,060
So now this is what it actually looks like.

62
00:03:45,120 --> 00:03:45,570
Okay.

63
00:03:45,570 --> 00:03:48,210
So the upper part of our page is styled.

64
00:03:48,270 --> 00:03:53,790
Now, let's go back and let's actually learn how we could place images over here.

65
00:03:53,820 --> 00:03:59,330
So the way in which we want to place the images over here is that we actually want to make a grid.

66
00:03:59,370 --> 00:04:04,820
And in that particular grid, we want to layout the images in terms of rows and columns.

67
00:04:04,830 --> 00:04:11,060
And basically what we want to do is we only want to have one column here in a particular grid.

68
00:04:11,070 --> 00:04:13,230
So we want to display one image here.

69
00:04:13,260 --> 00:04:17,459
Then if I scroll down, I should get the other image at the bottom of it.

70
00:04:17,550 --> 00:04:22,890
So in order to design that, first of all, I'll design a container which is going to start with div.

71
00:04:23,130 --> 00:04:27,690
And then for this container, I'm going to see that this is going to be a grid.

72
00:04:27,990 --> 00:04:31,530
So I would say div class as grid.

73
00:04:32,250 --> 00:04:38,220
And once we have this particular grid, I need to now say that in this particular grid I only want to

74
00:04:38,220 --> 00:04:39,180
have one column.

75
00:04:39,180 --> 00:04:42,570
So I would say grid dash calls, Dash one.

76
00:04:43,440 --> 00:04:49,790
Now once we have that particular column, instead of styling up this particular div with more classes,

77
00:04:49,800 --> 00:04:52,660
let's first go ahead and let's add an actual image.

78
00:04:52,680 --> 00:04:58,830
So whatever images which we have gone through over here using this loop, we simply have to repeat that

79
00:04:58,830 --> 00:04:59,490
over here.

80
00:04:59,730 --> 00:05:03,110
So in order to do that, I'll simply get the code from here.

81
00:05:03,120 --> 00:05:05,850
And this is why we have kept this code as of now.

82
00:05:06,180 --> 00:05:11,050
So here all the images which we are going to place here are going to be in a card.

83
00:05:11,070 --> 00:05:15,440
So I will actually give a comment here with say something like, okay, the card starts here.

84
00:05:15,450 --> 00:05:18,300
So I will say card starts here.

85
00:05:19,230 --> 00:05:23,290
And let's also see, card ends here.

86
00:05:23,310 --> 00:05:27,210
And within these two, we are actually going to loop through multiple cards.

87
00:05:27,540 --> 00:05:31,740
So here, first of all, I'll use a for loop.

88
00:05:32,190 --> 00:05:34,070
So the for loop is going to start.

89
00:05:34,080 --> 00:05:36,400
It's actually going to render out one card.

90
00:05:36,420 --> 00:05:37,830
The loop is going to end.

91
00:05:37,830 --> 00:05:39,360
The loop is going to start again.

92
00:05:39,360 --> 00:05:43,740
It's going to render out the second card with the second image, so on and so forth.

93
00:05:43,860 --> 00:05:52,080
So let's take the N4 out of here and let's actually start designing the card inside of these two tags.

94
00:05:52,410 --> 00:05:57,810
So first of all, whatever card you are going to create, this must be clickable or whatever image you're

95
00:05:57,810 --> 00:05:58,830
actually creating here.

96
00:05:58,830 --> 00:06:00,300
That image must be clickable.

97
00:06:00,300 --> 00:06:03,600
So I will enclose the entire thing into an H ref tag.

98
00:06:03,660 --> 00:06:06,230
For now, this is not going to point to anything.

99
00:06:06,240 --> 00:06:11,040
Okay, so once we have this particular of tag, we could actually go ahead and start adding the image

100
00:06:11,040 --> 00:06:11,340
here.

101
00:06:11,640 --> 00:06:14,570
So in order to add up an image here, it's actually quite simple.

102
00:06:14,580 --> 00:06:21,180
I simply have to say image and the source of this thing is going to be nothing but the post dot image,

103
00:06:21,180 --> 00:06:22,650
which we have up over here.

104
00:06:22,890 --> 00:06:27,030
So let's go ahead and let's take this particular post dot image, dot URL.

105
00:06:27,030 --> 00:06:32,190
Let's cut it from here and let's space it up right over here.

106
00:06:34,040 --> 00:06:36,540
So if I see this, we are pretty much good to go.

107
00:06:36,560 --> 00:06:41,390
So now, once we have added the image tag, if I go back here, you'll be able to see that if I had

108
00:06:41,400 --> 00:06:44,660
refresh, the image is now going to be added up over here.

109
00:06:44,780 --> 00:06:47,220
And this is what exactly we want.

110
00:06:47,240 --> 00:06:52,370
But now, at the same time, you'll also be able to notice that if you actually go back to the browser,

111
00:06:52,610 --> 00:06:57,770
the images which we have here, they do not properly fit over here and instead they span the entire

112
00:06:57,770 --> 00:06:59,360
page with which we have.

113
00:06:59,480 --> 00:07:04,790
So in order to limit this, what we need to do here is that we, first of all, need to enclose this

114
00:07:04,790 --> 00:07:07,650
entire thing into an image container of a sort.

115
00:07:07,670 --> 00:07:11,110
So in order to do that, I'll first of all, go ahead and create a div here.

116
00:07:11,120 --> 00:07:17,360
And inside this they will take the entire image tag, cut it from here, paste it right up over here,

117
00:07:17,360 --> 00:07:22,340
and we are going to style this particular div tag instead of styling up the image tag which we have

118
00:07:22,340 --> 00:07:22,610
here.

119
00:07:22,610 --> 00:07:26,600
So first of all, as I want to make this card rounded, I would say rounded.

120
00:07:26,870 --> 00:07:30,560
And then I want to set the property which is overflow hidden.

121
00:07:30,560 --> 00:07:34,640
So I would say overflow hidden.

122
00:07:34,640 --> 00:07:39,560
And then I could specifically set the width and height of this thing in terms of pixels.

123
00:07:39,560 --> 00:07:44,810
So until when whenever you want to set the width and height of a particular element in terms of exact

124
00:07:44,810 --> 00:07:51,020
pixel size as you would do in normal CSS, you say w dash and in square brackets you could specify the

125
00:07:51,020 --> 00:07:52,730
exact size in pixels.

126
00:07:52,730 --> 00:07:58,040
So here I could see for 70 pixels and same thing could be done with the height as well.

127
00:07:58,040 --> 00:08:02,930
So height I would say for 70 pixels as well.

128
00:08:02,930 --> 00:08:08,360
So we want a square type of an image and then I could set the background of this thing to white and

129
00:08:08,360 --> 00:08:13,580
I could also set a shadow LG to kind of give it a card look.

130
00:08:13,580 --> 00:08:17,630
So now if I head back here, hit refresh, you might not be able to see.

131
00:08:17,630 --> 00:08:23,270
But we have slightly rounded edges up over here and this is actually going to turn up in the way which

132
00:08:23,270 --> 00:08:24,890
we want eventually.

133
00:08:24,890 --> 00:08:27,710
But for now, let's start adding more properties here.

134
00:08:27,710 --> 00:08:32,870
So now once we have this image showing up over here, the next thing which we want to display over here

135
00:08:32,870 --> 00:08:36,740
is actually the user who has posted that particular image.

136
00:08:36,740 --> 00:08:41,809
So in order to add that we want to add the name of that particular user who has posted the image at

137
00:08:41,809 --> 00:08:47,440
the top and we want to display the name of that particular user along with their profile picture.

138
00:08:47,450 --> 00:08:53,690
So in order to display that, what I would do is that I would create a div tag and here I would name

139
00:08:53,690 --> 00:09:00,500
this thing as, let's say user container, because this is going to be used to display the profile picture

140
00:09:00,500 --> 00:09:02,360
of the user along with their name.

141
00:09:02,570 --> 00:09:08,510
So here once we have that, let's add the image which is going to be the image of the user.

142
00:09:08,750 --> 00:09:14,930
And in order to get the image of the user, what I would do is that I would actually need to go ahead

143
00:09:14,930 --> 00:09:18,860
and get access to the profile of a particular user who is currently logged in.

144
00:09:18,860 --> 00:09:24,980
But right now we only have access to the post, which we currently have and we do not have access to

145
00:09:24,980 --> 00:09:25,730
the user.

146
00:09:26,120 --> 00:09:30,950
So in order to get access to the user, first of all, I have to get back to the views.

147
00:09:30,950 --> 00:09:35,510
So let's go back to the views which we have, which is powering up this index page.

148
00:09:35,510 --> 00:09:37,070
So if I go back to view.

149
00:09:37,940 --> 00:09:40,970
I will be able to see the index V, which I have created.

150
00:09:40,970 --> 00:09:45,860
So currently here, in this particular view, we do have access to the current user.

151
00:09:45,980 --> 00:09:51,320
But in order to get the profile picture, let's first find out where the profile picture lies.

152
00:09:51,440 --> 00:09:53,630
So here if I go to admin.

153
00:09:54,800 --> 00:10:00,230
If I go to users here, you'll be able to see that we do not have any kind of a profile picture.

154
00:10:00,230 --> 00:10:04,040
But the profile picture actually lies in this profile model, which we have.

155
00:10:04,070 --> 00:10:09,050
So if I go to a particular user, as you can see, this right here is the profile picture of a current

156
00:10:09,050 --> 00:10:11,900
user, and that's exactly what we want to access.

157
00:10:12,080 --> 00:10:16,790
So right now inside this particular view, we already have access to the current user.

158
00:10:16,790 --> 00:10:23,810
But now if I want to get access to the profile here, what I would do is that I would say profile equals

159
00:10:23,810 --> 00:10:26,530
and I want to get the profile of the currently logged in user.

160
00:10:26,540 --> 00:10:31,730
So here I will get access to the profile by saying profile DOT objects and if I say all, it will give

161
00:10:31,730 --> 00:10:33,410
me access to all the profiles.

162
00:10:33,410 --> 00:10:39,260
But instead I want to filter this thing up and I want to say that I want the profile of the user, which

163
00:10:39,260 --> 00:10:40,670
is the currently logged in user.

164
00:10:40,670 --> 00:10:45,680
So I would say user equals the current user, and we are basically getting the current user from the

165
00:10:45,680 --> 00:10:46,850
request object.

166
00:10:47,030 --> 00:10:50,030
So here I would say current user.

167
00:10:50,240 --> 00:10:58,040
And then also I want the first element of this particular array because whenever you are doing profile

168
00:10:58,040 --> 00:11:00,680
dot objects, dot filter is going to give you an array.

169
00:11:00,680 --> 00:11:05,600
And from that array we want the first element of that array, which is going to be nothing but the profile.

170
00:11:05,750 --> 00:11:11,510
So now once we have this profile of that user, I now have to pass in the profile picture or the profile

171
00:11:11,510 --> 00:11:12,380
object here.

172
00:11:12,710 --> 00:11:17,630
So here I would pass in profile as profile.

173
00:11:18,170 --> 00:11:18,620
Okay.

174
00:11:18,620 --> 00:11:24,740
So once we have access to that particular profile, I could head back to index dot HML and here for

175
00:11:24,740 --> 00:11:30,440
the image source I could simply go ahead and access that particular profile object so I could instead

176
00:11:30,440 --> 00:11:34,970
see the profile, which is the object which we have just passed.

177
00:11:35,060 --> 00:11:39,800
And once we have the profile now, we simply have to access this photo field, which we have.

178
00:11:39,860 --> 00:11:43,910
So I could go back and I could say Profile dot for two.

179
00:11:43,910 --> 00:11:47,150
And as I want to get the URL, I could see a dot URL.

180
00:11:47,240 --> 00:11:52,030
So if I save this, if I go back here, let's see, what do we get?

181
00:11:52,040 --> 00:11:55,040
So I'll head back to the old URL head refresh.

182
00:11:57,020 --> 00:12:01,530
We are actually getting the profile photo and I've actually set the profile for two.

183
00:12:01,550 --> 00:12:03,750
Same as the logo picture, which we had.

184
00:12:03,770 --> 00:12:05,240
So that's a problem.

185
00:12:05,390 --> 00:12:06,910
So we could fix that.

186
00:12:06,920 --> 00:12:08,630
So let me go to admin.

187
00:12:09,380 --> 00:12:11,690
Let me go to Pro Files.

188
00:12:11,690 --> 00:12:14,270
Let me go to the profile for myself.

189
00:12:14,900 --> 00:12:17,660
Let's clear up this thing or let's choose up an image.

190
00:12:17,660 --> 00:12:20,340
So let's say I choose an avatar image for myself.

191
00:12:20,360 --> 00:12:21,740
If I click save.

192
00:12:22,690 --> 00:12:24,830
Okay, so let me.

193
00:12:24,850 --> 00:12:26,890
So I'll select an avatar image.

194
00:12:28,830 --> 00:12:30,000
Click on Save.

195
00:12:30,000 --> 00:12:32,020
And now the profile image is changed.

196
00:12:32,040 --> 00:12:35,280
I could go back to the page, hit refresh.

197
00:12:35,400 --> 00:12:40,230
And now, as you can see, we have this Avatar image along with the post image, which we have.

198
00:12:40,560 --> 00:12:46,080
So after that, the next thing which we need to do is we need to add the name of a particular user who's

199
00:12:46,080 --> 00:12:47,070
currently logged in.

200
00:12:47,220 --> 00:12:52,080
So now in order to do that, I would go ahead, go right after the image.

201
00:12:52,080 --> 00:12:58,320
And so the image actually ends here and I'll add a div so as to hold up the name of the user which we

202
00:12:58,320 --> 00:12:59,250
currently want.

203
00:12:59,460 --> 00:13:04,760
So here we currently want to get the log, then user or the user who's associated with the post.

204
00:13:04,770 --> 00:13:07,410
And henceforth here I could simply say.

205
00:13:09,220 --> 00:13:12,450
Post dot user if I do that.

206
00:13:12,460 --> 00:13:17,370
Now, as you can see, we have the profile image, we have the name of that particular user.

207
00:13:17,380 --> 00:13:23,110
We also have the image posted by that user and we have that for all the images for the currently logged

208
00:13:23,110 --> 00:13:23,770
in user.

209
00:13:24,010 --> 00:13:28,750
So now our next job is to go ahead and style up the different elements which we have here.

210
00:13:28,750 --> 00:13:30,340
So we need to have the image here.

211
00:13:30,340 --> 00:13:34,480
We need to have name at the right of that particular image, so on and so forth.

212
00:13:34,570 --> 00:13:37,420
So let's go ahead and let's do that in the next one.

