1
00:00:00,090 --> 00:00:00,920
In this lecture.

2
00:00:00,930 --> 00:00:05,310
Let's go ahead and let's learn how to extend the user model.

3
00:00:05,460 --> 00:00:11,710
So in the previous lecture, we had actually made use of the user model to register users into a website.

4
00:00:11,730 --> 00:00:17,010
But now let's see if we actually want to go ahead and let's say you want to add more information to

5
00:00:17,010 --> 00:00:17,490
the user.

6
00:00:17,490 --> 00:00:20,960
So by default, the user model does not have many fields.

7
00:00:20,970 --> 00:00:24,870
We only have the username password, email fields for the user.

8
00:00:24,900 --> 00:00:30,450
So let's say you also want to save some additional information for the user, like let's say the user's

9
00:00:30,450 --> 00:00:32,619
profile picture, so on and so forth.

10
00:00:32,640 --> 00:00:37,920
So for that very reason, what you could do is that you could create a new model and you could actually

11
00:00:37,920 --> 00:00:42,290
make that particular model extend the basic user model, which you had.

12
00:00:42,300 --> 00:00:46,290
So in order to do that, let's go ahead and let's create a brand new model here.

13
00:00:46,290 --> 00:00:49,770
So let's go to models dot p VI for users.

14
00:00:49,770 --> 00:00:52,530
So make sure that you are into your users app.

15
00:00:52,920 --> 00:00:58,410
So let's go to users and in here, let's go inside the model start p VI.

16
00:00:59,550 --> 00:01:01,550
So here is the file which we are looking for.

17
00:01:01,560 --> 00:01:06,910
And in here we create a new model and we call that particular model as, let's say, profile.

18
00:01:06,930 --> 00:01:12,690
So let's say this particular model saves information about the user, like the profile picture or the

19
00:01:12,690 --> 00:01:14,380
username, so on and so forth.

20
00:01:14,400 --> 00:01:16,070
So let's create that model.

21
00:01:16,080 --> 00:01:19,200
So I would say class profile to create a model.

22
00:01:19,230 --> 00:01:21,890
This is going to extend from model, start model.

23
00:01:21,900 --> 00:01:27,780
And here in order to associate the user model with this particular model here, we're actually going

24
00:01:27,780 --> 00:01:35,340
to add it a 1 to 1 field and we will be adding 1 to 1 field here because every user which we have registered

25
00:01:35,340 --> 00:01:42,150
on our site must have one profile and one user cannot have multiple profiles or a profile cannot belong

26
00:01:42,180 --> 00:01:43,320
to multiple users.

27
00:01:43,320 --> 00:01:46,330
Henceforth, we establish a 1 to 1 relationship here.

28
00:01:46,350 --> 00:01:51,220
So here I'll create a user field and this field is going to be the 1 to 1 field.

29
00:01:51,240 --> 00:01:54,200
So here I would say models dot.

30
00:01:54,210 --> 00:01:56,370
That's going to be 1 to 1 field.

31
00:01:56,490 --> 00:02:01,950
And then here, first of all, you need to associate this particular user with the user model, which

32
00:02:01,950 --> 00:02:02,640
you have.

33
00:02:02,700 --> 00:02:03,090
Now.

34
00:02:03,090 --> 00:02:08,669
The user model is actually present in the settings file as odd user model.

35
00:02:08,669 --> 00:02:11,060
And henceforth we first need to import that.

36
00:02:11,070 --> 00:02:19,110
So here I need to say from Django dot conf, which is Django dot config import D settings first.

37
00:02:19,110 --> 00:02:25,050
And in here once we have imported the settings, our next job is to actually go ahead and then use that

38
00:02:25,050 --> 00:02:29,210
model or associate that particular model with this 1 to 1 field.

39
00:02:29,220 --> 00:02:35,790
So here I would say settings DOT and the model which we are looking for is the auth underscore user

40
00:02:35,790 --> 00:02:37,380
underscore model.

41
00:02:38,010 --> 00:02:41,290
So once we have associated that model, we are good to go.

42
00:02:41,310 --> 00:02:46,950
And if you are using some model and if you want to associate that model with another model as 1 to 1

43
00:02:46,950 --> 00:02:50,550
field, you could simply directly use the model name here.

44
00:02:50,550 --> 00:02:56,100
But as we are working with the built in user model, we actually had to go ahead and use it from the

45
00:02:56,100 --> 00:02:57,300
settings package then.

46
00:02:57,420 --> 00:03:03,840
Okay, so once this thing is done, you also set the on delete option for this and the en delete specifies

47
00:03:03,840 --> 00:03:07,200
what happens when we go ahead and delete this particular field.

48
00:03:07,230 --> 00:03:14,220
So this should say model start cascade, which basically means that if a model is deleted, that means

49
00:03:14,220 --> 00:03:16,800
the profile of the user should also be deleted.

50
00:03:17,070 --> 00:03:21,770
Okay, So then let's go ahead and let's start adding different fields, which we want.

51
00:03:21,780 --> 00:03:28,110
So let's say for now, we want to save the user's profile picture so we could go ahead and create a

52
00:03:28,110 --> 00:03:29,490
profile field for this.

53
00:03:29,490 --> 00:03:34,590
So here, I'll name this thing as photo and then this is going to be an image field.

54
00:03:34,590 --> 00:03:37,440
So I would say model start, image field.

55
00:03:37,740 --> 00:03:43,920
And then whenever you're using or creating an image field in Django, you always need to set up a location

56
00:03:43,920 --> 00:03:45,620
where this needs to be uploaded.

57
00:03:45,630 --> 00:03:52,050
So here I would say I want to upload to a new directory which I want to create, which is the users.

58
00:03:52,500 --> 00:03:59,190
And then after that I actually want to pass in the year month and the date on which that particular

59
00:03:59,190 --> 00:04:02,130
image is uploaded as the name of that particular image.

60
00:04:02,280 --> 00:04:05,250
So here I would say forward slash percent.

61
00:04:05,250 --> 00:04:07,680
Y forward slash percent.

62
00:04:07,680 --> 00:04:10,320
M forward slash percent.

63
00:04:10,350 --> 00:04:18,329
D And then I would also set the blank to true, which means that this field could remain blank if we

64
00:04:18,329 --> 00:04:18,870
want to.

65
00:04:18,899 --> 00:04:24,810
That means if you go ahead and create a profile and if that profile does not have any photo, that particular

66
00:04:24,810 --> 00:04:27,870
profile object could actually exist in the database.

67
00:04:28,080 --> 00:04:32,640
And now once this thing is done, let's create a string representation method for this.

68
00:04:33,060 --> 00:04:40,830
So here I would say def str and this is nothing, but this is basically the thing which will act as

69
00:04:40,830 --> 00:04:45,900
a string representation of that object, which means when this object is actually saved and the Django

70
00:04:45,900 --> 00:04:51,930
admin, whenever we use this object, the object name should be displayed on that particular thing.

71
00:04:51,930 --> 00:04:53,880
So here I'll make this thing return.

72
00:04:53,880 --> 00:04:56,880
So here I'll make this thing return self dot user.

73
00:04:56,880 --> 00:05:01,680
So once we have added this particular image field, one more thing that needs to be done with this image

74
00:05:01,680 --> 00:05:07,770
field is that whenever we are uploading images or whenever we are working with images, we always need

75
00:05:07,770 --> 00:05:10,260
to use a package which is called as below.

76
00:05:10,620 --> 00:05:15,540
So in Django, whenever we are working with images you have to use below, so let's go ahead and install

77
00:05:15,540 --> 00:05:16,290
that package.

78
00:05:16,290 --> 00:05:23,250
So in order to install that package, you type in PIP, install below, hit enter and below should be

79
00:05:23,250 --> 00:05:24,300
installed for you.

80
00:05:24,630 --> 00:05:27,720
So once below is installed we are pretty much good to go.

81
00:05:27,990 --> 00:05:34,050
So one more thing that needs to be done is that as we are working with images, these images need to

82
00:05:34,050 --> 00:05:36,240
be uploaded to a certain folder location.

83
00:05:36,240 --> 00:05:42,120
So we actually have to go ahead and manually set the location where these images are actually saved.

84
00:05:42,420 --> 00:05:45,510
So here let's go to the settings dot py file.

85
00:05:45,510 --> 00:05:51,450
So I would go right back to the main project, go to settings dot p V and in here.

86
00:05:52,800 --> 00:05:54,890
First of all, you need to import us.

87
00:05:54,900 --> 00:05:57,960
So let's go to the top and import the US module.

88
00:05:57,960 --> 00:06:04,740
And this is because as the images are going to be stored on our computer, we basically need to find

89
00:06:04,740 --> 00:06:07,110
the exact path on our computer.

90
00:06:07,110 --> 00:06:12,030
And in order to get the exact path, first of all, we actually have to go ahead import the OS module

91
00:06:12,030 --> 00:06:16,170
because the OS module gives us the base path of our computer.

92
00:06:16,410 --> 00:06:20,040
So here, once OS is imported, first we set the media URL.

93
00:06:20,040 --> 00:06:26,130
So the media URL is going to be, let's say I want to upload it in the folder media, so I would specify

94
00:06:26,130 --> 00:06:27,490
the path as media.

95
00:06:27,510 --> 00:06:33,270
Now obviously this path cannot be directly set and said what we have to do is that we have to get the

96
00:06:33,270 --> 00:06:39,150
exact path on our computer and for that we need to get the route path, which is nothing but the entire

97
00:06:39,150 --> 00:06:39,510
path.

98
00:06:39,510 --> 00:06:42,990
So I would say media underscore route equals.

99
00:06:42,990 --> 00:06:48,450
And in order to get the route path, I would say OS dot path, which is going to give us access to the

100
00:06:48,450 --> 00:06:51,630
path and then we join that particular path with media.

101
00:06:51,720 --> 00:06:59,640
So here I would say Path dot join and I'm going to pass in the base directory, which is nothing but

102
00:06:59,640 --> 00:07:04,180
the base path and connected with media forward slash.

103
00:07:04,200 --> 00:07:09,570
Now the next thing which needs to be done is that as the images which are being uploaded here, they

104
00:07:09,570 --> 00:07:12,450
also have their very own URLs as well.

105
00:07:12,570 --> 00:07:17,600
That means whenever we click on those particular images, we should be able to get those images in our

106
00:07:17,600 --> 00:07:18,390
web browser.

107
00:07:18,600 --> 00:07:24,330
And in order to do that, we also need to set up the paths for them in the URL patterns as well.

108
00:07:24,630 --> 00:07:29,310
So here we need to go to the URLs dot py file of our main project.

109
00:07:29,490 --> 00:07:35,730
And just as we have the two URL patterns, one is for admin and the other one is for the users app.

110
00:07:35,970 --> 00:07:41,160
Over here we also need to set the URL patterns for the images as well.

111
00:07:41,340 --> 00:07:45,660
So here you see plus equals, which basically means append to this.

112
00:07:45,810 --> 00:07:48,660
And here you need to append static.

113
00:07:50,430 --> 00:07:55,830
Because that's actually a static path and you also need to import static as well.

114
00:07:55,860 --> 00:08:07,710
So here you would save from Django dot config dot you URLs import static and then to this particular

115
00:08:07,710 --> 00:08:11,610
static you basically have to pass in the media URL and the media route.

116
00:08:11,670 --> 00:08:14,820
So here I would say from settings.

117
00:08:14,820 --> 00:08:17,920
So that means you also need to import settings as well.

118
00:08:17,940 --> 00:08:23,640
So from django, dot conf or config import the settings.

119
00:08:24,030 --> 00:08:31,460
And in here we basically have to say settings start, get access to the media U URL.

120
00:08:31,470 --> 00:08:35,270
So first we pass in the media URL and then the document route.

121
00:08:35,280 --> 00:08:39,330
That means where the documents or the images are going to be actually uploaded.

122
00:08:39,330 --> 00:08:40,820
That is going to be media route.

123
00:08:40,830 --> 00:08:52,180
So here I would say document, underscore route that's going to be settings dot media route.

124
00:08:52,200 --> 00:08:55,230
Once that thing is done, we are pretty much good to go.

125
00:08:55,230 --> 00:08:57,120
So I could simply save this.

126
00:08:57,120 --> 00:09:04,620
And I think it's actually better if we say you URL patterns here plus equals this and that should work

127
00:09:04,620 --> 00:09:05,250
out well.

128
00:09:05,250 --> 00:09:05,670
Okay.

129
00:09:05,670 --> 00:09:09,600
So once this thing is done now we have pretty much set the path for everything.

130
00:09:09,750 --> 00:09:15,300
And now let's see if we are actually able to go ahead and save some sort of an image or create a profile

131
00:09:15,300 --> 00:09:18,240
and then add a particular image for that particular user.

132
00:09:18,240 --> 00:09:24,510
So in order to do that, as we have created a model, first of all, we have to go ahead and register

133
00:09:24,510 --> 00:09:25,770
that particular model.

134
00:09:25,920 --> 00:09:30,810
So I'll go to the admin side, which is admin by for the.

135
00:09:32,330 --> 00:09:33,020
App.

136
00:09:33,200 --> 00:09:41,450
And here I would see from DOT models, I want to import the profile model and then simply register that

137
00:09:41,450 --> 00:09:41,810
model.

138
00:09:41,810 --> 00:09:49,310
So admin, dot site, dot register the profile model.

139
00:09:49,580 --> 00:09:53,760
Once that thing is done, I could now open up the terminal, make migrations.

140
00:09:53,780 --> 00:10:01,340
So I would say python manage, start by make migrations, and then simply say migrate.

141
00:10:01,640 --> 00:10:05,600
But before that, I guess we do have some error here.

142
00:10:06,260 --> 00:10:09,920
So I guess in the URLs file, which we have set up right now.

143
00:10:10,820 --> 00:10:11,320
Okay.

144
00:10:11,330 --> 00:10:17,180
It has actually imported a couple of things from the locations, which we don't want to, and it has

145
00:10:17,180 --> 00:10:18,740
made a wrong import there.

146
00:10:19,190 --> 00:10:21,800
So I simply could delete that.

147
00:10:21,800 --> 00:10:25,040
And once that thing is done, we should be good to go.

148
00:10:25,550 --> 00:10:28,820
So let's execute, make migrations command once again.

149
00:10:29,000 --> 00:10:34,520
And again, we have an error which says module object is not callable.

150
00:10:34,790 --> 00:10:37,370
So let's see what exactly went wrong there.

151
00:10:37,580 --> 00:10:37,970
Okay.

152
00:10:37,970 --> 00:10:40,450
So here we have made a slight mistake.

153
00:10:40,460 --> 00:10:45,060
We actually had to import static from the URL start static.

154
00:10:45,080 --> 00:10:49,010
And once that thing is done, let's run this one more time.

155
00:10:49,950 --> 00:10:52,920
And this time, as you can see, the migrations were made.

156
00:10:52,920 --> 00:10:54,720
And now let's also migrate.

157
00:10:54,720 --> 00:10:59,400
So Python managed by migrate.

158
00:11:00,440 --> 00:11:03,100
And now, as you can see, the migrations have been done.

159
00:11:03,110 --> 00:11:06,110
That means now we should be able to access that particular model.

160
00:11:06,140 --> 00:11:07,680
So now let's run the server.

161
00:11:07,700 --> 00:11:14,780
So Python managed by run server server is up and running.

162
00:11:14,780 --> 00:11:16,170
Let's go to the admin.

163
00:11:16,190 --> 00:11:17,270
Let's log in.

164
00:11:17,420 --> 00:11:22,160
Now, here, as you can see for the users, we have something called us profiles.

165
00:11:22,220 --> 00:11:27,020
I could go to profiles and I could create a new profile for any user which we have.

166
00:11:27,080 --> 00:11:29,840
So let's create a profile for the super user.

167
00:11:29,840 --> 00:11:33,470
And here I could choose any profile picture for this particular user.

168
00:11:33,500 --> 00:11:37,310
So let me choose a profile picture and see if that really works.

169
00:11:37,310 --> 00:11:41,690
So I've actually chosen an avatar picture for this particular user.

170
00:11:41,690 --> 00:11:45,710
And now let's click on Save and let's see if that image would be uploaded.

171
00:11:45,920 --> 00:11:54,200
So if I click on Save here, we do have an error which says type error at user's profile ad, and it

172
00:11:54,200 --> 00:11:57,720
also will give us the location where exactly this went wrong.

173
00:11:57,740 --> 00:12:01,250
So let's handle this error and let's see what exactly went wrong.

174
00:12:01,640 --> 00:12:08,570
Okay, So if we take a look at the user, it says that str return non string type user.

175
00:12:08,570 --> 00:12:15,290
That means if I now go back here, it basically says that the STR method and the method we have actually

176
00:12:15,290 --> 00:12:17,350
set it and the model start by file.

177
00:12:17,360 --> 00:12:18,720
So let's go over there.

178
00:12:18,740 --> 00:12:20,960
So this is the method which we are talking about.

179
00:12:20,960 --> 00:12:27,200
And here it says that it actually returned a non string type and that is true and it says that we have

180
00:12:27,200 --> 00:12:30,760
returned the type as user, which is actually true.

181
00:12:30,770 --> 00:12:33,730
But here we actually have to return a string.

182
00:12:33,740 --> 00:12:36,670
So we don't have any kind of a string field here.

183
00:12:36,680 --> 00:12:43,550
So in order to return a string value here, what we could do is that in the user model we have something

184
00:12:43,550 --> 00:12:47,290
which is called as the username and username is actually off the type string.

185
00:12:47,300 --> 00:12:53,620
So here instead I could say dot user name and if I do that now, this error would be fixed.

186
00:12:53,630 --> 00:12:58,520
So let's head back here and now let's go to Profiles.

187
00:12:58,610 --> 00:13:01,600
Let's try adding the profile one more time for this user.

188
00:13:01,610 --> 00:13:02,840
Let's choose an image.

189
00:13:02,840 --> 00:13:04,830
So I have chosen an avatar image.

190
00:13:04,850 --> 00:13:10,850
If I click on Save this time, as you can see, the profile has been created without any issues and

191
00:13:10,850 --> 00:13:12,370
we don't get an error either.

192
00:13:12,380 --> 00:13:18,350
And now if I go to the profile here, as you can see, we also have the image uploaded at this particular

193
00:13:18,350 --> 00:13:23,040
location as well, along with the date format, which is specified up over here.

194
00:13:23,060 --> 00:13:26,180
So now if I open this file up, let's see, what do we get?

195
00:13:26,390 --> 00:13:32,930
So as you can see, this file is actually currently located at local host and it also has this address

196
00:13:32,930 --> 00:13:36,620
as well, which is forward slash media forward slash users.

197
00:13:36,620 --> 00:13:41,540
And if you actually go to VTS code, you should be able to see that that particular image will also

198
00:13:41,540 --> 00:13:43,310
be present up over here as well.

199
00:13:43,550 --> 00:13:47,900
So here, let's view the directory.

200
00:13:48,830 --> 00:13:54,470
If you take a look at the project, we have the media folder and inside the media folder we also have

201
00:13:54,470 --> 00:13:57,490
the users folder, then this folder right up over here.

202
00:13:57,500 --> 00:14:01,040
And then we also have the images which are currently uploaded.

203
00:14:01,370 --> 00:14:07,100
So that means the image upload functionality is working well and we are successfully able to extend

204
00:14:07,100 --> 00:14:13,070
the user model, create a profile for the user and upload the profile picture for a particular user.

205
00:14:13,100 --> 00:14:18,740
Now, in the next lecture, let's go ahead and let's actually create a form so that we could edit the

206
00:14:18,740 --> 00:14:19,850
user's profile.

207
00:14:19,940 --> 00:14:22,370
So let's do that in the next one.

208
00:14:22,460 --> 00:14:23,270
Thank you.

