1
00:00:00,120 --> 00:00:06,420
So now let's learn how we could add a functionality which allows users to reset their passwords.

2
00:00:06,420 --> 00:00:12,090
So we have already added the functionality to change the password where you enter the old password and

3
00:00:12,090 --> 00:00:13,540
then you set a new password.

4
00:00:13,560 --> 00:00:18,990
But let's say for some reason, if the user forgets their password, how exactly they could go ahead

5
00:00:18,990 --> 00:00:24,620
and reset their passwords from scratch by getting a password reset link in their email.

6
00:00:24,630 --> 00:00:27,810
So we are going to learn how to do that in this particular lecture.

7
00:00:28,050 --> 00:00:35,040
So just as we have some in-built views for changing passwords, we also have some views for resetting

8
00:00:35,040 --> 00:00:36,270
passwords as well.

9
00:00:36,270 --> 00:00:39,990
And we will be making use of those particular views and using them.

10
00:00:39,990 --> 00:00:43,160
We will implement this password reset functionality.

11
00:00:43,170 --> 00:00:49,890
So let's go to VTS code and in here, first of all, let me close every window which we have right here

12
00:00:50,040 --> 00:00:56,640
and right now we need to go to the URL start by file and we need to go here because obviously as we

13
00:00:56,640 --> 00:01:03,180
will be using the built in views, we need to make use of those built in views here and associate them

14
00:01:03,180 --> 00:01:04,800
with specific parts.

15
00:01:04,800 --> 00:01:10,470
So in Django there are some by default paths which are automatically preset, which you have to use.

16
00:01:10,470 --> 00:01:16,140
So for example, whenever you have to reset the password, you basically have to go ahead and you have

17
00:01:16,140 --> 00:01:19,610
to type in forward slash password and those go reset.

18
00:01:19,620 --> 00:01:22,080
So let's decide the URL pattern for this.

19
00:01:22,080 --> 00:01:30,930
So the URL pattern for resetting the password would be nothing, but that's going to be password underscore,

20
00:01:30,930 --> 00:01:31,740
reset.

21
00:01:32,460 --> 00:01:37,590
And now once we have this particular path, the view which we will be using is going to be called as

22
00:01:37,590 --> 00:01:39,480
the password reset view.

23
00:01:39,690 --> 00:01:45,810
So just as we have used some of the views like the password change view, the password change and then

24
00:01:45,810 --> 00:01:51,990
view similar to this, we have the password reset view, so let's use that.

25
00:01:51,990 --> 00:01:56,340
So here we need to use them or import them from the same auth view.

26
00:01:56,340 --> 00:02:01,530
So we have already imported and this particular package and we have called this thing as odd view or

27
00:02:01,530 --> 00:02:02,910
authentication views.

28
00:02:02,970 --> 00:02:10,710
So here I would say old view dot that's going to be password reset view.

29
00:02:10,740 --> 00:02:14,010
So as you can see the view pops up up over here right away.

30
00:02:14,100 --> 00:02:21,360
So let's use this as view and once we have this particular view, let's also name this particular URL

31
00:02:21,360 --> 00:02:21,900
as something.

32
00:02:21,900 --> 00:02:28,230
So let's see, the name of this thing is going to be password, underscore, reset, give a comma at

33
00:02:28,230 --> 00:02:30,060
the end and we should be good to go.

34
00:02:30,390 --> 00:02:36,720
So right now, whenever we go inside the URL patterns and whenever we type in this URL, which is password

35
00:02:36,720 --> 00:02:40,740
underscore, reset, So make sure that you add a forward slash here.

36
00:02:40,740 --> 00:02:46,410
So whenever we type in password reset or we should be able to get the password reset page.

37
00:02:46,410 --> 00:02:48,330
So let's see if that works out.

38
00:02:48,420 --> 00:02:55,260
So let's go back to the browser users forward slash, password, underscore, reset.

39
00:02:55,260 --> 00:03:00,960
So as soon as I do that, as you can see now, it actually opens up the password reset view.

40
00:03:00,960 --> 00:03:05,640
But as this is a built in view, it's going to display the page from Django administration.

41
00:03:05,640 --> 00:03:11,820
For us, however, we don't want to use this and instead we want to create our very own template and

42
00:03:11,820 --> 00:03:14,160
we want to display our very own form here.

43
00:03:14,160 --> 00:03:17,400
And we don't want to use the Django admins by default form.

44
00:03:17,400 --> 00:03:20,910
So let's go ahead and let's create our very own form here.

45
00:03:20,910 --> 00:03:33,000
So let's go inside the templates and let's create a template called as password underscore, reset,

46
00:03:33,930 --> 00:03:36,390
underscore, form dot HTML.

47
00:03:36,420 --> 00:03:36,810
Okay.

48
00:03:36,810 --> 00:03:43,590
So once we have this form, the only thing that needs to be done is that we simply have to extend from

49
00:03:43,590 --> 00:03:44,400
the base template.

50
00:03:44,400 --> 00:03:50,310
We have to add the block, body and block, and we simply have to render the basic form, which we already

51
00:03:50,310 --> 00:03:51,900
actually have up over here.

52
00:03:52,080 --> 00:03:54,900
So in order to do that, doing that is quite simple.

53
00:03:54,900 --> 00:04:00,630
And one shortcut which you could implement is that you could actually go to some of the views which

54
00:04:00,630 --> 00:04:02,310
you already have created.

55
00:04:02,310 --> 00:04:09,060
So for example, the password change underscore form which we have created here, you could simply copy

56
00:04:09,060 --> 00:04:13,680
the code from here, paste it up over here, because that's what I'm the same thing.

57
00:04:13,680 --> 00:04:20,940
So once you do that and now let's actually go ahead and fasten this particular password reset form to

58
00:04:20,940 --> 00:04:24,030
this particular URL, which we have just created.

59
00:04:24,030 --> 00:04:32,760
So here I would pass in the template name as nothing, but I would refer to the users app and then password

60
00:04:32,760 --> 00:04:36,480
underscore, reset, underscore, form dot HTML.

61
00:04:36,480 --> 00:04:37,890
Let's go back here to refresh.

62
00:04:37,890 --> 00:04:43,740
And as you can see now, we have our very own form right up over here, and we simply have to add the

63
00:04:43,740 --> 00:04:46,950
email of the user where we want to reset the password.

64
00:04:46,950 --> 00:04:52,860
So technically, what should happen is that whenever we submit the email and click on submit that particular

65
00:04:52,860 --> 00:04:59,010
password reset link should be sent to email because this is what happens whenever you try to reset passwords

66
00:04:59,010 --> 00:04:59,940
on any of your web.

67
00:05:00,020 --> 00:05:00,640
Sites.

68
00:05:00,800 --> 00:05:06,260
However, as we are not working with a real website, instead, what we are going to do is that we are

69
00:05:06,260 --> 00:05:10,310
going to get the password reset link in the terminal itself.

70
00:05:10,310 --> 00:05:13,940
So we are going to learn how we could configure that in the next lecture.

71
00:05:13,940 --> 00:05:17,660
So thank you very much for watching and I'll see you guys in the next one.

