1
00:00:00,120 --> 00:00:05,820
Okay, So in this lecture, let's go ahead and let's start working on the remaining part of our navigation

2
00:00:05,820 --> 00:00:06,210
bar.

3
00:00:06,360 --> 00:00:11,250
So if you take a look at the navigation bar, the navigation bar over here does not have any kind of

4
00:00:11,250 --> 00:00:12,390
space at the top here.

5
00:00:12,390 --> 00:00:18,330
So in order to add some space or sort of add some padding around this, what we could do is that right

6
00:00:18,330 --> 00:00:24,950
now we have a navigation bar inside a specific div, which is this div right here, which is a flex.

7
00:00:24,960 --> 00:00:29,690
So we'll keep this particular flex as it is, and this is the div where this flex ends.

8
00:00:29,700 --> 00:00:35,400
So in order to actually go ahead and add some more padding to it, we are actually going to take this

9
00:00:35,400 --> 00:00:41,580
entire thing, cut it and place it inside another div and paste it up over here.

10
00:00:41,820 --> 00:00:47,940
And the reason why we are doing this is because for this particular div which we have, we want to go

11
00:00:47,940 --> 00:00:49,350
ahead and add some padding.

12
00:00:49,350 --> 00:00:55,380
So here I would say class and I want to add a padding of four on the x axis.

13
00:00:55,950 --> 00:00:58,980
So if I do that and if I go back here, hit refresh.

14
00:00:58,980 --> 00:01:01,290
As you can see now, it has some padding on the left.

15
00:01:01,290 --> 00:01:05,430
And let's say we also want to add some margin on the Y axis for this.

16
00:01:05,430 --> 00:01:10,200
I could simply go ahead and add a property called las m x dash auto.

17
00:01:10,230 --> 00:01:15,530
So here I would say Amex auto and it will automatically set the margin for that.

18
00:01:15,540 --> 00:01:17,220
So once we are done with this.

19
00:01:17,460 --> 00:01:21,560
Now let's go ahead and let's start styling up the inner divs, which we have.

20
00:01:21,570 --> 00:01:25,190
But even before that, let's add some shadow to the navigation bar.

21
00:01:25,200 --> 00:01:33,450
So over here for this main nav, I will add a class and I would say Shadow Dash, LG and LG means a

22
00:01:33,450 --> 00:01:34,380
larger shadow.

23
00:01:34,710 --> 00:01:40,680
So if I do that now, as you can see, this navigation bar has a shadow and it kind of creates the separation

24
00:01:40,680 --> 00:01:44,190
between the navigation bar and the rest of the web page, which we have now.

25
00:01:44,190 --> 00:01:50,430
Let's go ahead and as we have these particular link items here, let's create many copies of this.

26
00:01:50,430 --> 00:01:54,540
So let's copy this, paste it up away here multiple number of times.

27
00:01:54,540 --> 00:01:56,610
So let's say we have three items in there.

28
00:01:57,420 --> 00:02:03,600
If I go back and hit refresh Now, as you can see, we have these three items, which are the NAV items.

29
00:02:03,690 --> 00:02:09,360
Now, let's say if you want to have some space in between them in order to do that or in order to add

30
00:02:09,360 --> 00:02:13,890
space between those items, I could go ahead and style up this particular div, which we have.

31
00:02:14,070 --> 00:02:23,100
So I could say class and I could say space, and I want the space on the x axis to be one If I had to

32
00:02:23,100 --> 00:02:23,630
refresh.

33
00:02:23,640 --> 00:02:26,970
Now it has a little bit of space in between the elements which we have.

34
00:02:26,970 --> 00:02:32,250
Okay, So once this thing is done, let's get back to styling top divs, which we have.

35
00:02:32,250 --> 00:02:37,710
And after setting up the flags for this div right here, let's say I want to justify the items, which

36
00:02:37,710 --> 00:02:47,370
is these two items or these two divs in center so I could say justify dash between if I add that now

37
00:02:47,370 --> 00:02:51,930
as you can see, this thing actually shifted to the right and we now have equal amount of space in between

38
00:02:51,930 --> 00:02:57,930
these two elements by adding justify between, okay, so now once this is ready, let's add the actual

39
00:02:57,930 --> 00:02:59,250
links which we want here.

40
00:02:59,550 --> 00:03:06,990
So here, instead of having link one and link two, I will actually copy the log out from here pasted

41
00:03:06,990 --> 00:03:08,220
instead of link one.

42
00:03:09,480 --> 00:03:12,570
I will copy login from here and paste it inside.

43
00:03:12,570 --> 00:03:17,370
Link two and the authentication logic which we had added here.

44
00:03:17,370 --> 00:03:19,380
I could simply cut this from here.

45
00:03:19,830 --> 00:03:21,540
I would add it up over here.

46
00:03:22,590 --> 00:03:24,740
I would add the spot there.

47
00:03:24,750 --> 00:03:26,970
So I would get the El spot here.

48
00:03:28,000 --> 00:03:34,780
I would cut the endive pot from here and I would pace it up right after the login.

49
00:03:35,560 --> 00:03:39,400
And also now you also need to add the ref for this.

50
00:03:39,640 --> 00:03:42,340
So let's copy the ref for log out.

51
00:03:42,730 --> 00:03:44,380
So I'll cut this from here.

52
00:03:44,920 --> 00:03:46,270
Paste it up over here.

53
00:03:46,840 --> 00:03:50,800
And let's do the same thing with the h ref of login as well.

54
00:03:50,830 --> 00:03:52,420
So let's cut it from here.

55
00:03:53,350 --> 00:03:55,060
Let's piece it up over here.

56
00:03:55,390 --> 00:03:58,030
Let's keep this extra item over there.

57
00:03:58,600 --> 00:04:01,390
Let's get rid of the login from here.

58
00:04:01,720 --> 00:04:03,400
And we should be good to go.

59
00:04:03,880 --> 00:04:06,670
So if I save this, if I go back here, hit refresh.

60
00:04:06,670 --> 00:04:09,940
As you can see now, we have logout and another item here.

61
00:04:10,090 --> 00:04:14,070
So if you want to add furthermore items, you could simply copy this and paste them here.

62
00:04:14,080 --> 00:04:16,149
So we are going to add that in the next lecture.

63
00:04:16,269 --> 00:04:20,860
But for now, let's simply work with this navigation bar which we have designed.

64
00:04:21,100 --> 00:04:22,840
So that's it for this lecture.

65
00:04:22,840 --> 00:04:27,880
And once we have designed the navigation bar in the next lecture, let's actually design the login page,

66
00:04:27,880 --> 00:04:28,500
which we have.

67
00:04:28,510 --> 00:04:31,320
So we are going to design this main home page later.

68
00:04:31,330 --> 00:04:36,010
But for now let's design the login page, which we have, which is this page right here.

69
00:04:36,190 --> 00:04:38,740
So let's learn how to do that in the next one.

70
00:04:38,950 --> 00:04:39,550
Thank you.

