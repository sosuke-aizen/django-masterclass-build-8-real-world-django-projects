1
00:00:00,180 --> 00:00:05,490
So in this particular lecture, let's go ahead and let's actually start designing the navigation bar

2
00:00:05,490 --> 00:00:06,760
for our website.

3
00:00:06,780 --> 00:00:12,330
So the first thing which needs to be done is you need to go to base dot HTML, and here is where we

4
00:00:12,330 --> 00:00:13,830
are going to set up the NAF bar.

5
00:00:13,980 --> 00:00:19,050
And once we have the NAF bar, then we are going to have these login and log out buttons in there.

6
00:00:19,080 --> 00:00:24,900
So for now, let's go ahead and let's go inside the body tag and let's actually create enough bar so

7
00:00:24,900 --> 00:00:28,740
we'll create enough tag here, which is going to be our entire navigation bar.

8
00:00:28,740 --> 00:00:32,030
And in this particular nav bar, let's create a main div.

9
00:00:32,040 --> 00:00:35,790
So this div is going to be the container for all the elements which we have.

10
00:00:35,880 --> 00:00:41,430
So the way in which we are going to design this NAV bias that we are actually going to go ahead.

11
00:00:41,430 --> 00:00:47,130
And on the left hand side we want the logo and on the right hand side we want the items of the menu.

12
00:00:47,220 --> 00:00:51,180
So this is going to be the outer container, which is this div.

13
00:00:51,180 --> 00:00:53,730
And inside this div we are going to have two divs.

14
00:00:53,730 --> 00:00:59,460
So one def is going to be for the website logo and the other there is going to be for the navigation

15
00:00:59,460 --> 00:00:59,850
items.

16
00:00:59,850 --> 00:01:01,190
So let's create that.

17
00:01:01,200 --> 00:01:05,290
So a lot of comment here which would say something like website logo.

18
00:01:05,310 --> 00:01:08,310
So here I would say website logo.

19
00:01:08,310 --> 00:01:15,120
And then similarly I would add another comment here which is going to say something like website items.

20
00:01:16,110 --> 00:01:19,400
And here let's create a div for the website logo.

21
00:01:19,410 --> 00:01:22,650
So this div is going to be a container for the website logo.

22
00:01:22,650 --> 00:01:28,380
And here this entire website logo is going to be wrapped up in a ref tag.

23
00:01:28,410 --> 00:01:30,690
So I'll create an e tag here.

24
00:01:30,690 --> 00:01:36,030
And in that particular image ref tag, first of all, we are going to have an image and this image is

25
00:01:36,030 --> 00:01:38,340
going to be something which we are going to add later.

26
00:01:38,340 --> 00:01:42,920
But for now we also need to add the name of that particular website as well.

27
00:01:42,930 --> 00:01:49,590
So here, in order to add a name, as we want to add the name or right next to this particular image,

28
00:01:49,680 --> 00:01:54,600
I would use a span tag and this span tag is going to have our website name.

29
00:01:55,050 --> 00:02:00,980
Let's say we name this thing as photo app, which is the name of our app.

30
00:02:00,990 --> 00:02:05,280
If I go back here to the browser, let's see, what do we get?

31
00:02:05,310 --> 00:02:11,310
So if I had to refresh, so as you can see, if I had refresh, I get photo app right up over here.

32
00:02:11,310 --> 00:02:13,250
So that means we have added this thing.

33
00:02:13,270 --> 00:02:16,870
Now let's also add a logo as well, which is the actual image.

34
00:02:16,890 --> 00:02:22,590
Now, in order to get a logo for your website or what you could do is you could simply go to a website

35
00:02:22,590 --> 00:02:23,730
called As I Can Find.

36
00:02:24,060 --> 00:02:29,400
So you could simply go to Google, search for Icon Finder, click on the first link which pops up.

37
00:02:29,400 --> 00:02:35,100
And this website gives you free images which you could use for adding into your website so you could

38
00:02:35,100 --> 00:02:36,330
search for icons here.

39
00:02:36,330 --> 00:02:42,900
So let's say I want an icon for a social media website so I could search for an icon like Instagram.

40
00:02:43,740 --> 00:02:49,170
We won't be using the Instagram logo here, but we will be using a similar looking icon like this one.

41
00:02:49,170 --> 00:02:50,620
So this is free to use.

42
00:02:50,640 --> 00:02:56,190
You could select that and then you could download this particular icon as a PNG image.

43
00:02:56,190 --> 00:02:57,780
So I'll simply click here.

44
00:02:58,260 --> 00:03:04,770
And once this particular icon is downloaded, you could simply drag it and drop it at whatever location

45
00:03:04,770 --> 00:03:05,550
which you want.

46
00:03:05,700 --> 00:03:11,280
So here, right inside the static folder, we have the user's folder right up over here.

47
00:03:11,460 --> 00:03:16,890
But in this user's folder, let's say I want to create another folder for our images.

48
00:03:16,890 --> 00:03:20,880
So I'll create a new folder in the class images.

49
00:03:20,880 --> 00:03:24,960
And let's say I want to use this or paste this image in there.

50
00:03:25,050 --> 00:03:28,020
I will simply drag it and drop it inside this folder.

51
00:03:28,620 --> 00:03:33,050
And now let's change this particular name to something else.

52
00:03:33,060 --> 00:03:35,730
So let's say I rename this thing to.

53
00:03:36,680 --> 00:03:38,600
Social dot org.

54
00:03:39,140 --> 00:03:42,080
And as you can have it, we now have an app over there.

55
00:03:42,620 --> 00:03:50,990
So if I go back to Vsco now in order to add this icon right up over here to this particular image tag,

56
00:03:51,170 --> 00:03:53,870
I simply have to edit the source attribute here.

57
00:03:54,140 --> 00:04:00,290
So inside the source, as we are using an image from static, I would add a static tag and then the

58
00:04:00,290 --> 00:04:09,020
path for the images users forward slash images forward slash social dot P and G, which is the name

59
00:04:09,020 --> 00:04:09,860
of the image.

60
00:04:10,130 --> 00:04:13,580
Now, as soon as we add that and if I go back here, I hit refresh.

61
00:04:13,580 --> 00:04:18,200
As you can see now, the icon is added up right up over here.

62
00:04:18,260 --> 00:04:21,950
But if you take a look at this, the icon is much bigger.

63
00:04:21,950 --> 00:04:25,220
And let's say if you want to reduce the size of this particular icon.

64
00:04:25,520 --> 00:04:29,660
So for that very reason, what you could do is that you could add CSS.

65
00:04:29,660 --> 00:04:35,960
But now, as we are using Tailwind, instead of writing CSS to set up the size of this image, instead

66
00:04:35,960 --> 00:04:37,890
you could use the built in classes.

67
00:04:37,910 --> 00:04:44,960
So here I would say class, and then I could say I want the height of this thing to be certain number

68
00:04:44,960 --> 00:04:45,910
of pixels.

69
00:04:45,920 --> 00:04:49,130
So here we have an H eight class.

70
00:04:49,310 --> 00:04:55,700
So if I say H eight, it's actually going to set up this particular image to be of a certain size.

71
00:04:55,700 --> 00:05:01,040
And in order to know the exact size for this tag, it could actually visit the official website.

72
00:05:01,040 --> 00:05:04,640
So you could go to even the official website by searching for tailwind.

73
00:05:04,640 --> 00:05:06,990
And here you could search for anything which you want.

74
00:05:07,010 --> 00:05:12,680
So let's say if I want to add width and height for a certain HTML element I could search for.

75
00:05:13,580 --> 00:05:14,390
Height.

76
00:05:14,390 --> 00:05:19,940
And if I go ahead search for height now here, it will show me the different kinds of classes which

77
00:05:19,940 --> 00:05:22,250
I need to use to set this particular height.

78
00:05:22,400 --> 00:05:28,670
So h eight over here basically means that you use a height which is 32 pixels or two rams.

79
00:05:29,060 --> 00:05:33,720
So here we have set up H eight and similar class is used for width as well.

80
00:05:33,740 --> 00:05:38,180
So if I want to set the weight to 32 pixels, I would say w-8.

81
00:05:38,930 --> 00:05:45,800
Now as soon as I do that and if I go back to the browser, if I go to local host head refresh, as you

82
00:05:45,800 --> 00:05:51,590
can see now, this particular image I can now has a size of 32 pixels.

83
00:05:52,190 --> 00:05:57,590
Now, similar to setting up the width and height of a particular HML element, you could also set up

84
00:05:57,590 --> 00:06:00,500
the other properties of a particular element as well.

85
00:06:00,500 --> 00:06:05,660
Like you could set certain items to be flexed, you could center certain items, so on and so forth,

86
00:06:05,840 --> 00:06:13,280
and also make sure that you are into version 2.2.19 while making these searches and you could search

87
00:06:13,280 --> 00:06:14,510
for any property here.

88
00:06:14,540 --> 00:06:16,970
So we are not going to search each and every property.

89
00:06:16,980 --> 00:06:20,150
Instead, I am directly going to add them in order to save time.

90
00:06:20,150 --> 00:06:23,180
But if you want to explore tail, then you could do that over here.

91
00:06:23,780 --> 00:06:26,870
Okay, so now let's get back styling our image here.

92
00:06:26,900 --> 00:06:32,570
So now, once we have added the styling to this, now you'll be able to see that we have these two things

93
00:06:32,570 --> 00:06:35,500
and they are right on top of each other.

94
00:06:35,510 --> 00:06:39,740
But let's say if I want to make them to the side, I could actually go ahead.

95
00:06:39,740 --> 00:06:45,470
And for this image, which I have, I could actually add a class and I could make it flex.

96
00:06:45,470 --> 00:06:51,650
So as soon as I make it flex and hit refresh, now these things are on the side of each other and this

97
00:06:51,650 --> 00:06:52,880
is what exactly we want.

98
00:06:52,880 --> 00:06:57,580
And also let's go ahead and let's increase the font of this thing a little bit.

99
00:06:57,590 --> 00:07:00,620
So let's make this font as a little bit bolder.

100
00:07:00,620 --> 00:07:05,360
And in order to make that font bold, you simply have to add a couple of classes to the span.

101
00:07:05,660 --> 00:07:13,190
So if I want to make the font bold, you could simply again go back to Telvin and you could see font

102
00:07:13,190 --> 00:07:19,490
bold or you could search for font weight more specifically, and it will give you the different classes

103
00:07:19,490 --> 00:07:21,480
which you could use to set up the font.

104
00:07:21,500 --> 00:07:27,860
So if you want to set the font weight to 600, you could use to one class, which is font dash semi

105
00:07:27,860 --> 00:07:28,400
bold.

106
00:07:28,520 --> 00:07:33,260
So here I would say font slash semi bold.

107
00:07:33,260 --> 00:07:35,510
Save this if I go back here.

108
00:07:35,930 --> 00:07:38,960
Now, as you can see, the font is now a little bit darker.

109
00:07:39,350 --> 00:07:44,120
Now you'll be able to see that the text over here is a little bit smaller.

110
00:07:44,300 --> 00:07:49,190
So to increase the size of the text, there's a class which is text dash LLG.

111
00:07:49,640 --> 00:07:55,240
If I add that now the text is a little bit larger and let's say if you want to set the text color.

112
00:07:55,250 --> 00:07:58,790
So right now the color is about black.

113
00:07:58,790 --> 00:08:02,950
And let's say you want to set it to some gray or shade, you could do that as well.

114
00:08:02,960 --> 00:08:11,840
So for that, you simply have to add a class text gray with a shade of 500.

115
00:08:12,350 --> 00:08:16,910
If you do that and hit refresh, as you can see now, this has a much lighter shade.

116
00:08:16,910 --> 00:08:20,630
And now let's add a couple of more properties to this logo.

117
00:08:20,810 --> 00:08:23,900
So as you can see, the logo is not separated from this text.

118
00:08:23,900 --> 00:08:26,120
So we want some margin on the right.

119
00:08:26,330 --> 00:08:30,830
So I could go to Tailwind, I could search for margin.

120
00:08:32,450 --> 00:08:33,350
Right.

121
00:08:35,110 --> 00:08:37,330
Or I could simply search for margin.

122
00:08:38,740 --> 00:08:43,450
And here, as you can see, it will also give you the different options if you only want to set the

123
00:08:43,450 --> 00:08:44,700
margin for different sites.

124
00:08:44,710 --> 00:08:51,460
So, for example, if I search for right on this page, if you want to add margin on the x axis or margin

125
00:08:51,460 --> 00:08:55,520
on the right, you say x dash the margin which you want to add.

126
00:08:55,540 --> 00:09:01,840
So here I would say I want to add a margin of two to this particular image on the right.

127
00:09:01,840 --> 00:09:06,850
So here inside the class, I would say mr-2.

128
00:09:07,600 --> 00:09:12,130
If I go back to refresh now, as you can see, we have margin on the right hand side.

129
00:09:12,460 --> 00:09:14,580
So this is how tailbone works.

130
00:09:14,590 --> 00:09:16,940
Now we have the left side of this NAV ready.

131
00:09:16,960 --> 00:09:20,270
Now let's set up the nav items inside another div.

132
00:09:20,290 --> 00:09:25,080
So I'll create another div here and this is going to be for website items.

133
00:09:25,090 --> 00:09:29,830
Now for the website items right up away here we are going to require links.

134
00:09:29,830 --> 00:09:32,050
So here I would create certain links.

135
00:09:32,410 --> 00:09:34,570
Let's say this is going to be link one.

136
00:09:35,050 --> 00:09:38,440
Let's also add another link like link two.

137
00:09:38,710 --> 00:09:45,250
So once I add them again, we have the same thing that is, these two things are aligned on top of each

138
00:09:45,250 --> 00:09:46,860
other and not next to each other.

139
00:09:46,870 --> 00:09:54,070
That means the container for these two divs is this main div right here and we need to set its class

140
00:09:54,070 --> 00:09:55,870
to flex.

141
00:09:56,350 --> 00:09:58,370
As soon as I do that and hit refresh.

142
00:09:58,390 --> 00:10:02,070
Now, as you can see, it's now set right next to each other.

143
00:10:02,080 --> 00:10:07,850
So now we have added a couple of elements here, but we also need to add styling to this stuff as well.

144
00:10:07,870 --> 00:10:12,970
So first of all, let's add some padding for this particular element, which is the link element.

145
00:10:12,970 --> 00:10:17,710
So here I'll add a class and we could start adding some tailbone classes in here.

146
00:10:18,100 --> 00:10:20,650
So I want the padding on the y axis to be full.

147
00:10:20,650 --> 00:10:22,180
So I'll say pv4.

148
00:10:22,210 --> 00:10:24,540
I want the padding on the x axis to be two.

149
00:10:24,550 --> 00:10:30,340
So px2 and I want the text of this thing to be gray with a shade of 500.

150
00:10:30,340 --> 00:10:33,280
So I would say text gray.

151
00:10:34,310 --> 00:10:36,290
With a shade of 500.

152
00:10:37,290 --> 00:10:42,000
I want the font to be semi bold so font is semi bold.

153
00:10:42,570 --> 00:10:47,730
I could go back, hit refresh and now as you can see, we have that write up over here.

154
00:10:47,760 --> 00:10:52,680
Now we will continue styling this particular nav bar in the upcoming lecture where we are going to add

155
00:10:52,680 --> 00:10:56,160
a couple of more CSS classes to all the elements which we have.

156
00:10:56,190 --> 00:10:59,700
So let's go ahead and let's learn how to do that in the next one.

