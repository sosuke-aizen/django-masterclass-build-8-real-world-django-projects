1
00:00:00,090 --> 00:00:04,350
So in this lecture, let's finally work on the password reset complete view.

2
00:00:04,350 --> 00:00:07,740
And keep in mind that these are not errors which we are getting.

3
00:00:07,740 --> 00:00:12,990
I mean, obviously they are errors, but I'm showing you these errors on purpose so that you understand

4
00:00:12,990 --> 00:00:19,320
what basically is the next step and how Jango actually proceeds to the next step and how these steps

5
00:00:19,320 --> 00:00:21,060
are actually connected with each other.

6
00:00:21,060 --> 00:00:26,100
So you need to understand that all the views which we are creating for password reset are interlinked

7
00:00:26,100 --> 00:00:32,189
and the view which is before to a particular view, won't work unless and until we have the next view.

8
00:00:32,189 --> 00:00:38,310
And that's why it is so important to have all the views in place so that all the password reset mechanism

9
00:00:38,310 --> 00:00:40,110
completes without any issues.

10
00:00:40,110 --> 00:00:44,010
Okay, so now let's work on creating this password reset complete view.

11
00:00:44,250 --> 00:00:51,600
So in order to do that, let's go back to VTS code and in here the password reset complete view is also

12
00:00:51,600 --> 00:00:52,590
a built in view.

13
00:00:52,620 --> 00:00:59,490
We could use it from all views, so I'll create a path here and this path is going to be something like

14
00:00:59,490 --> 00:01:06,210
reset forward, slash done and we won't be passing in any ID or anything like that over here because

15
00:01:06,210 --> 00:01:07,320
we don't need that.

16
00:01:07,320 --> 00:01:14,190
This is going to be art view dot that's going to be password reset complete and it will auto complete

17
00:01:14,190 --> 00:01:16,020
that particular thing for us.

18
00:01:16,530 --> 00:01:22,050
And this needs to be as view and as usual we need a template for this as well.

19
00:01:22,350 --> 00:01:26,970
And let's name this thing as password reset.

20
00:01:28,250 --> 00:01:29,360
Complete.

21
00:01:29,900 --> 00:01:32,780
Give a comma at the end and we should be good to go.

22
00:01:33,080 --> 00:01:35,480
So let's now create a simple template for this.

23
00:01:35,480 --> 00:01:38,000
So we don't need any fancy template for this.

24
00:01:38,300 --> 00:01:48,500
We just need to go ahead, create a new template, and call this thing as password reset, complete

25
00:01:48,500 --> 00:01:53,990
dot HTML, and let me copy the name as well so that we don't have to type that thing again.

26
00:01:54,260 --> 00:01:57,130
And let me right away add the template name here.

27
00:01:57,140 --> 00:02:04,130
So the template name here is going to be password reset complete.

28
00:02:04,130 --> 00:02:07,730
And also don't forget to add users before to that.

29
00:02:08,280 --> 00:02:08,590
Okay.

30
00:02:08,600 --> 00:02:14,060
So once this thing is added now, we could actually go back to the password reset complete view.

31
00:02:14,060 --> 00:02:19,610
And in here we could just borrow the code from one of these templates which we have.

32
00:02:19,640 --> 00:02:25,210
So let's copy the code for extending from the base template and block, body and block.

33
00:02:25,220 --> 00:02:26,630
And here, let's see.

34
00:02:26,630 --> 00:02:36,350
Something like your password has been successfully reset.

35
00:02:37,640 --> 00:02:39,110
Okay, let's save this.

36
00:02:39,110 --> 00:02:41,030
And if I now go back here.

37
00:02:41,030 --> 00:02:43,760
So now let's try resetting the password from scratch.

38
00:02:43,760 --> 00:02:48,770
So I would say users forward, slash, password, underscore, reset.

39
00:02:48,770 --> 00:02:50,900
And here, let's type in the email.

40
00:02:51,080 --> 00:02:52,340
Click on submit.

41
00:02:53,360 --> 00:02:56,070
It sees we have mail you the password reset link.

42
00:02:56,090 --> 00:02:57,950
Let's go back to the terminal.

43
00:02:58,700 --> 00:02:59,840
We have the link here.

44
00:02:59,840 --> 00:03:01,970
So let's copy this link.

45
00:03:03,170 --> 00:03:05,920
Let's go back here, paste this link.

46
00:03:05,930 --> 00:03:08,480
Now it says you have to enter a new password.

47
00:03:08,690 --> 00:03:13,250
So I'll enter the new password as new super user.

48
00:03:14,270 --> 00:03:18,210
New super user.

49
00:03:18,230 --> 00:03:20,300
Let's try submitting this.

50
00:03:21,190 --> 00:03:23,050
Okay, so the two fields did not match.

51
00:03:23,080 --> 00:03:25,120
Okay, so let's type in a new password here.

52
00:03:25,120 --> 00:03:31,300
So let's say the new password is super user 3 to 1.

53
00:03:31,840 --> 00:03:33,940
Let's type that thing in one more time.

54
00:03:34,570 --> 00:03:39,730
Super user 3 to 1, click on submit.

55
00:03:39,850 --> 00:03:44,110
And here, as you can see, it sees your password has been successfully reset.

56
00:03:44,110 --> 00:03:46,780
So let's try logging in with that particular password.

57
00:03:47,110 --> 00:03:50,680
So here, let's go ahead and let's see, log in.

58
00:03:51,580 --> 00:03:53,350
Let's type in the username.

59
00:03:54,420 --> 00:03:58,130
Let me type in the password as super user.

60
00:03:58,140 --> 00:03:59,220
Three, two, one.

61
00:03:59,910 --> 00:04:01,030
If I click on submit.

62
00:04:01,050 --> 00:04:07,260
It says user authenticated and logged in, which means now the password reset mechanism is working absolutely

63
00:04:07,260 --> 00:04:07,680
fine.

64
00:04:08,100 --> 00:04:13,590
And one more thing which you could do here is that you could add a link to reset password here so you

65
00:04:13,590 --> 00:04:16,140
can ask the user if you have forgotten the password.

66
00:04:16,140 --> 00:04:21,740
And if the user has forgotten their password, they could actually be shown up the password reset link.

67
00:04:21,750 --> 00:04:28,050
So in order to add that, I could simply go to the login HTML page and in here right where the form

68
00:04:28,050 --> 00:04:32,000
ends, I could simply go ahead and create a paragraph tag.

69
00:04:32,010 --> 00:04:37,230
I would add a link and say forgot password.

70
00:04:38,220 --> 00:04:40,800
Click here to reset.

71
00:04:41,250 --> 00:04:47,460
And this particular link tag should actually point out to the URL pattern, which is nothing but password

72
00:04:47,460 --> 00:04:47,970
reset.

73
00:04:47,970 --> 00:04:51,780
So this particular path actually has a name which is password reset.

74
00:04:51,900 --> 00:04:53,700
So I could simply copy that.

75
00:04:53,700 --> 00:04:59,140
Go back here and I need to add a template tag in order to be able to add a URL.

76
00:04:59,160 --> 00:05:04,590
So here I would say URL, use single codes and simply paste the name of that URL here.

77
00:05:04,590 --> 00:05:09,560
So that means whenever now I visit the login page, I should have this option here.

78
00:05:09,570 --> 00:05:15,030
And when I click on this, it will actually take me to this particular password reset form here.

79
00:05:15,030 --> 00:05:20,760
And one more thing which you could do here is that in order to actually identify that this is a password

80
00:05:20,760 --> 00:05:24,180
reset form, you could add a simple heading here on the top.

81
00:05:24,570 --> 00:05:26,670
So that's it for this particular lecture.

82
00:05:26,670 --> 00:05:31,310
And the password reset mechanism or the functionality has been completed.

83
00:05:31,320 --> 00:05:36,630
So in the next lecture, we will go ahead and we will start learning how to add the functionality to

84
00:05:36,630 --> 00:05:38,780
register users to our site.

85
00:05:38,790 --> 00:05:41,640
So let's learn how to do that in the next lecture.

