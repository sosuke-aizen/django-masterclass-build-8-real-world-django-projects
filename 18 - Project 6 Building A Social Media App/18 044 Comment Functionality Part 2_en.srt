1
00:00:00,090 --> 00:00:05,520
In this lecture, let's learn how we could display the comments related to a particular post underneath

2
00:00:05,520 --> 00:00:06,570
that particular post.

3
00:00:06,570 --> 00:00:12,300
So let's say right after the name of the post and the caption down right here, we want to display some

4
00:00:12,300 --> 00:00:12,990
comments.

5
00:00:12,990 --> 00:00:15,240
So how exactly would we do that?

6
00:00:15,240 --> 00:00:17,940
So in order to do that, I'll go back to VAR code.

7
00:00:17,940 --> 00:00:20,460
I'll go back to the page, which is v dot HTML.

8
00:00:20,460 --> 00:00:25,770
And right now up over here, in order to display the comments, every comment is actually associated

9
00:00:25,770 --> 00:00:26,640
with a post.

10
00:00:26,730 --> 00:00:31,590
That means if we could get access to the post, we could also get access to the comment as well.

11
00:00:31,860 --> 00:00:37,260
So that means we have to loop through all the posts and once we have looped through the posts, we could

12
00:00:37,260 --> 00:00:38,820
loop through all the comments as well.

13
00:00:38,970 --> 00:00:42,480
So we are already looping through the posts here and inside the post.

14
00:00:42,480 --> 00:00:47,970
We could loop through comments so we'll go right below where the title and the caption ends, which

15
00:00:47,970 --> 00:00:54,000
is this div right here and we'll create our own div and let's set the padding and margin for this one.

16
00:00:54,000 --> 00:00:58,290
So I would say px5 and p Y as five as well.

17
00:00:58,290 --> 00:01:03,030
So once we have this up over here, we are going to loop through all the comments which we have.

18
00:01:03,240 --> 00:01:09,030
So the problem here is that you might think that, okay, if you go to the views, we have not passed

19
00:01:09,030 --> 00:01:11,640
any kind of context here for comments.

20
00:01:11,640 --> 00:01:15,900
So if you go to views here, let me show you what I exactly mean to say.

21
00:01:16,110 --> 00:01:22,320
So the view which powers up the feed page is this particular view right here and we are simply passing

22
00:01:22,320 --> 00:01:23,250
in posts.

23
00:01:23,250 --> 00:01:29,280
But now with posts you could actually get access to comments because post is actually a foreign key

24
00:01:29,280 --> 00:01:30,150
for comments.

25
00:01:30,240 --> 00:01:34,980
So that means you don't have to query all the comments here because if you query all the comments,

26
00:01:34,980 --> 00:01:38,820
it's actually going to give you all the comments related to all the posts.

27
00:01:38,910 --> 00:01:45,630
But what we want here is that we only want to get the comments of that specific post itself.

28
00:01:45,630 --> 00:01:49,560
And for that very reason, we are going to make a different kind of loop here.

29
00:01:49,710 --> 00:01:51,480
So here let's create a for loop.

30
00:01:51,480 --> 00:01:53,580
So I would say for a single comment.

31
00:01:53,580 --> 00:02:00,270
N And instead of saying comments here and pass comments from The View, instead, I would say post dot

32
00:02:00,420 --> 00:02:03,810
comments, dot all and then I will end my for here.

33
00:02:03,810 --> 00:02:11,220
So NFO and once we have this, we are simply going to display a single comment here by saying comment

34
00:02:11,220 --> 00:02:14,520
and the extract of a particular comment lies in its body.

35
00:02:14,520 --> 00:02:16,650
So comment, dot body.

36
00:02:17,220 --> 00:02:20,220
And then I'm simply going to have a break tag here.

37
00:02:20,220 --> 00:02:26,550
So this is because we want to create a space between multiple comments if we have for a post.

38
00:02:26,790 --> 00:02:30,150
So once we have added that, let's see if we are able to display the comment.

39
00:02:30,150 --> 00:02:36,000
So if I had to refresh here, as you can see now, the comment for this particular post is being displayed.

40
00:02:36,330 --> 00:02:42,000
Now, if I have to create another comment, let's say for another post, so I'll go ahead and try creating

41
00:02:42,000 --> 00:02:44,010
that and let's see if that thing shows up.

42
00:02:44,220 --> 00:02:47,850
So let's say I want to create a comment for this one.

43
00:02:48,600 --> 00:02:51,210
I would say nice title post.

44
00:02:51,510 --> 00:02:53,160
Let's create that comment.

45
00:02:53,430 --> 00:03:00,060
And once that comment is created, I would hit refresh and as you can see, it is nice title post here.

46
00:03:00,090 --> 00:03:06,540
So by making use of this logic, we ensure that a comment actually shows up after a particular post

47
00:03:06,540 --> 00:03:09,520
and we do not have all the comments displayed on a single post.

48
00:03:09,540 --> 00:03:11,250
So this is the logic to do that.

49
00:03:11,490 --> 00:03:16,400
Now there's one thing missing in a comment here, like we don't know who exactly made the comment,

50
00:03:16,410 --> 00:03:20,830
and that's because we have not associated a comment with any kind of user.

51
00:03:20,850 --> 00:03:28,020
So if you go to the models here, we have created a comment model, but now a comment is no longer associated

52
00:03:28,020 --> 00:03:32,190
with the user and also even before associating a comment with a user.

53
00:03:32,370 --> 00:03:39,180
Now, you also need to have a way so that users could actually comment from here because right now we

54
00:03:39,180 --> 00:03:42,090
are actually creating comments making use of the admin panel.

55
00:03:42,150 --> 00:03:47,190
So in the next lecture, let's go ahead and let's actually create a form so that users of our website

56
00:03:47,190 --> 00:03:48,760
could actually post a comment.

57
00:03:48,810 --> 00:03:50,670
So let's do that in the next one.

