1
00:00:00,090 --> 00:00:05,910
So now that we have this particular registration form ready now our job is to go ahead and render this

2
00:00:05,910 --> 00:00:06,350
form.

3
00:00:06,360 --> 00:00:12,150
So to render this form, first of all, you need to create a view that actually renders out a template.

4
00:00:12,150 --> 00:00:16,110
And to that particular template you need to pass in this user registration form.

5
00:00:16,110 --> 00:00:21,890
And also in that particular view, you also need to handle what happens when the form gets submitted.

6
00:00:21,900 --> 00:00:26,850
So let's go ahead and let's go into the view start UI file to create a view for this form.

7
00:00:27,060 --> 00:00:32,670
So first of all, you need to remember that you need to import that particular form from user registration

8
00:00:32,670 --> 00:00:33,150
form.

9
00:00:33,150 --> 00:00:39,300
So here I would say from Django Forms, we have already imported the login form and let's import the

10
00:00:39,300 --> 00:00:41,040
user registration form as well.

11
00:00:41,610 --> 00:00:47,910
Now let's create a view down right here, which basically is going to say register.

12
00:00:47,910 --> 00:00:49,860
So this is a view for register.

13
00:00:49,870 --> 00:00:51,930
So I will name this thing as a register.

14
00:00:52,320 --> 00:00:58,260
This is going to accept a request and then it's going to return and render a particular template.

15
00:00:58,260 --> 00:01:04,400
But first of all, we are going to just go ahead and manage the post method for this particular form.

16
00:01:04,410 --> 00:01:10,290
That is what happens if the method is post, which means that what should happen when the user tries

17
00:01:10,290 --> 00:01:11,450
to submit the form.

18
00:01:11,460 --> 00:01:16,350
So whenever the user tries to submit that form, we want to register the user.

19
00:01:16,350 --> 00:01:22,860
And by registering the user, we simply mean we actually need to go ahead and create a user object and

20
00:01:22,860 --> 00:01:25,830
save that particular user object in the user model.

21
00:01:25,830 --> 00:01:27,510
So let's go ahead and do that.

22
00:01:27,510 --> 00:01:32,730
So here, first of all, I would say if the request method is post, so if request start method, if

23
00:01:32,730 --> 00:01:40,170
this is equal, equal to post, in that particular case, what we do is we basically go ahead and get

24
00:01:40,170 --> 00:01:41,370
the data from the form.

25
00:01:41,370 --> 00:01:47,580
So here, in order to get the data from the form, I would say user registration form, and then I would

26
00:01:47,580 --> 00:01:50,520
say request dot post.

27
00:01:50,520 --> 00:01:56,430
So this will fetch me the data here and now in order to actually get that particular data, I would

28
00:01:56,430 --> 00:01:59,280
actually save that data and user underscore form.

29
00:01:59,280 --> 00:02:02,640
So I would say user underscore form is going to be this form.

30
00:02:02,640 --> 00:02:04,680
That means we get all the data.

31
00:02:04,740 --> 00:02:07,020
Now let's validate this data.

32
00:02:07,020 --> 00:02:12,330
And if the data is validated, that means if the data is well and good, we could then go ahead and

33
00:02:12,330 --> 00:02:13,530
create a new user.

34
00:02:13,620 --> 00:02:22,620
So here I would say if user form DOT is valid, which means that if all the values which are submitted

35
00:02:22,620 --> 00:02:27,900
to this particular form, if they are actually valid, then we go ahead and then we create a new user.

36
00:02:27,900 --> 00:02:33,630
So now let's create a new user here or a new user object from the form data which we have here.

37
00:02:34,020 --> 00:02:41,310
So here we see new user is going to be equal to user form dot save.

38
00:02:41,310 --> 00:02:45,210
And now when we save this, we are going to set the commit to false.

39
00:02:45,210 --> 00:02:50,610
And that's because while creating this particular new user object right now, we are not saving the

40
00:02:50,610 --> 00:02:51,270
password.

41
00:02:51,270 --> 00:02:54,330
And apart from the password, we want to save everything else.

42
00:02:54,330 --> 00:02:58,230
So here I would pass in the parameter commit as false.

43
00:02:58,440 --> 00:03:01,260
And now here let's set the password.

44
00:03:01,260 --> 00:03:06,450
And after the password has been set only then let's go ahead and save that particular form.

45
00:03:06,450 --> 00:03:13,950
So here I would say new underscore user dot and the method is set, underscore password.

46
00:03:14,340 --> 00:03:16,890
And let's say here we want to get the clean password.

47
00:03:16,890 --> 00:03:18,720
So we get that from the user form.

48
00:03:18,720 --> 00:03:25,230
So we see user form dot cleaned data and then simply pass in password here.

49
00:03:25,710 --> 00:03:29,850
Once this thing is done, we now simply save the new user object.

50
00:03:30,000 --> 00:03:33,810
So new user save and we are pretty much good to go.

51
00:03:34,170 --> 00:03:39,840
So once this thing has happened, we want to display a message like the new user has been registered

52
00:03:39,840 --> 00:03:42,990
and for that we return and render a template.

53
00:03:43,200 --> 00:03:46,800
And to this return we obviously need to pass in the request object.

54
00:03:46,980 --> 00:03:51,930
We also need to pass in the template which is going to say something like the registration is completed,

55
00:03:51,930 --> 00:03:54,030
so we are going to create that template soon.

56
00:03:54,030 --> 00:03:57,990
But for now, let's go ahead and let's handle the else part of this.

57
00:03:57,990 --> 00:04:03,330
So that means whenever the request method is kept in that particular case, we actually have to render

58
00:04:03,330 --> 00:04:03,900
the form.

59
00:04:04,170 --> 00:04:11,640
So here I would say user form equals first let's get the form, which is the user registration form.

60
00:04:12,060 --> 00:04:21,570
Once we get that particular form, we make this thing return render request, and then this form which

61
00:04:21,570 --> 00:04:25,980
we get here, we need to now pass this form to a template so that it could be rendered.

62
00:04:26,250 --> 00:04:30,300
So here I would say I want to render a particular template.

63
00:04:30,300 --> 00:04:34,950
And along with this I also want to pass in user form as context.

64
00:04:35,670 --> 00:04:38,130
So let's copy that and quotations.

65
00:04:38,130 --> 00:04:40,740
Let's paste it up like that.

66
00:04:40,740 --> 00:04:44,100
Now it is being passed to whatever template which we pass in here.

67
00:04:44,100 --> 00:04:46,500
So now we need to create two templates.

68
00:04:46,510 --> 00:04:52,920
One template is actually going to be the template which is going to render up when the form is submitted

69
00:04:52,920 --> 00:04:55,830
successfully and another template when we load up the form.

70
00:04:55,830 --> 00:04:58,950
So let's first create a template for loading up the form.

71
00:04:58,950 --> 00:04:59,890
So let's go.

72
00:05:00,320 --> 00:05:08,060
Let's go to the users templates here and let's create a new template and let's call it as register dot

73
00:05:08,300 --> 00:05:08,870
HTML.

74
00:05:09,440 --> 00:05:15,740
And this particular template, as usual as we know it actually could use all the parameters which we

75
00:05:15,740 --> 00:05:17,550
have in this particular form right here.

76
00:05:17,570 --> 00:05:24,380
So what we could do is we could simply copy this, go back here, go to register dot HTML, simply paste

77
00:05:24,380 --> 00:05:25,610
this form up over here.

78
00:05:25,850 --> 00:05:28,400
We could remove this forgot password from here.

79
00:05:28,670 --> 00:05:34,010
And also, let's borrow something from the other files as well, like the index.

80
00:05:34,010 --> 00:05:38,240
So let's borrow the code to extend from the base template from here.

81
00:05:38,690 --> 00:05:41,000
So let me paste that above.

82
00:05:41,720 --> 00:05:45,230
And now let's paste this particular form in here.

83
00:05:45,830 --> 00:05:49,580
Okay, So once this thing is done, we are pretty much good to go.

84
00:05:49,580 --> 00:05:54,830
And one more thing which you need to do here is that as we are passing user form as the context here

85
00:05:55,220 --> 00:06:01,490
over here, instead of rendering out the form, I now need to say that I want to render user underscore

86
00:06:01,490 --> 00:06:02,020
form.

87
00:06:02,030 --> 00:06:05,600
So here I would say user underscore form dot ASP.

88
00:06:05,840 --> 00:06:08,330
And also we could change this input a little bit.

89
00:06:08,330 --> 00:06:14,780
So instead of input, let's change it to button type is going to be submit and let's name this thing

90
00:06:14,780 --> 00:06:16,160
as register.

91
00:06:16,670 --> 00:06:16,970
Okay?

92
00:06:16,970 --> 00:06:19,940
So once this thing is done, we are pretty much good to go.

93
00:06:20,000 --> 00:06:21,560
Now the view is ready.

94
00:06:21,560 --> 00:06:23,570
The view renders this particular form.

95
00:06:23,600 --> 00:06:26,300
So let's pass in that particular template here.

96
00:06:26,300 --> 00:06:31,710
So users forward slash that's going to be registered dot HTML.

97
00:06:31,730 --> 00:06:36,200
Now, once that thing is done, now the final thing which is remaining is to go ahead and connect this

98
00:06:36,200 --> 00:06:38,720
particular view with a URL pattern.

99
00:06:38,720 --> 00:06:42,770
But even before that, we also need to add a template over here as well.

100
00:06:42,950 --> 00:06:50,330
So here let's create a template which is users forward slash, let's say just done dot HTML.

101
00:06:50,360 --> 00:06:55,660
Let's say that's the name of the template which displays the message like the registration is successful.

102
00:06:55,670 --> 00:06:58,800
So let's create that template up over here as well.

103
00:06:58,820 --> 00:07:04,070
So that's going to be just underscore done dot HTML.

104
00:07:04,370 --> 00:07:04,940
Okay.

105
00:07:05,540 --> 00:07:10,220
So now let's go ahead, let's copy the code from here, paste it up over here.

106
00:07:10,610 --> 00:07:17,390
And I would simply say something like registration successful.

107
00:07:17,840 --> 00:07:22,640
Okay, So now the view is ready, the form is ready, the templates are ready.

108
00:07:22,640 --> 00:07:26,540
Now we just need to associate this particular view with a URL pattern.

109
00:07:26,720 --> 00:07:29,270
So let's go to a URL start by file.

110
00:07:29,270 --> 00:07:37,730
And in here let's create a URL for just so here I would say something like Path, and this path is going

111
00:07:37,730 --> 00:07:40,280
to be register forward slash.

112
00:07:40,730 --> 00:07:46,310
This is going to say view, start, register, and let's name this thing as a register as well.

113
00:07:47,060 --> 00:07:47,450
Okay?

114
00:07:47,450 --> 00:07:52,100
So once this thing is done now let's see if the registration functionality works well.

115
00:07:52,400 --> 00:07:56,900
So I'll save this and let's test it out so the server is up and running.

116
00:07:57,440 --> 00:07:58,520
Let's go ahead.

117
00:07:58,520 --> 00:08:02,780
Let's go to forward slash users forward slash register.

118
00:08:03,290 --> 00:08:08,540
Now, here we get the registration form, which means at least one part of this is working well.

119
00:08:08,540 --> 00:08:13,000
So let's try adding some data here and let's see how that works.

120
00:08:13,010 --> 00:08:15,170
Okay, So I've added a bunch of data here.

121
00:08:15,170 --> 00:08:20,040
I've added the first name username, email, password, and I've also confirmed the password.

122
00:08:20,060 --> 00:08:24,730
So let's click on register and if I do that, it sees registration successful.

123
00:08:24,740 --> 00:08:30,860
So in order to check if that's actually true, if the user is actually registered, I could go to admin

124
00:08:31,870 --> 00:08:33,020
and let's log in.

125
00:08:33,020 --> 00:08:39,740
And now if I go to users here, I should be able to see another user added up over here, which is Rob.

126
00:08:39,799 --> 00:08:45,620
So that means the registration functionality is working absolutely fine and we don't have any sort of

127
00:08:45,620 --> 00:08:46,400
error over there.

128
00:08:46,400 --> 00:08:52,160
So now once we are successfully able to register the user now our next job is to go ahead and extend

129
00:08:52,160 --> 00:08:54,550
this particular user model, which we have.

130
00:08:54,560 --> 00:08:57,620
So this user model is nothing, but it's an in-built model.

131
00:08:57,620 --> 00:09:03,200
And let's say if you have to add a couple of more fields to this particular user model, we could do

132
00:09:03,200 --> 00:09:03,830
that as well.

133
00:09:03,830 --> 00:09:08,870
So let's say if you want to set a profile picture for this user, you could actually extend this particular

134
00:09:08,870 --> 00:09:09,670
user model.

135
00:09:09,680 --> 00:09:15,230
So in the next lecture, let's learn how to extend the user model by creating another model for the

136
00:09:15,230 --> 00:09:15,740
user.

137
00:09:15,860 --> 00:09:19,730
So thank you very much for watching and I'll see you guys in the next one.

138
00:09:19,850 --> 00:09:20,510
Thank you.

