1
00:00:00,090 --> 00:00:04,019
So now let's try associating a comment with a user.

2
00:00:04,170 --> 00:00:09,300
So in order to associate a comment with the user, you actually have to go ahead and make changes to

3
00:00:09,300 --> 00:00:10,710
the models itself.

4
00:00:10,980 --> 00:00:17,580
So every comment which you have posted, you also need to say the user who has actually posted that

5
00:00:17,580 --> 00:00:18,220
comment.

6
00:00:18,240 --> 00:00:24,150
And in order to do that, we are going to be adding a posted by Fields to the comment which we have.

7
00:00:24,360 --> 00:00:29,830
And that particular posted by simply going to save the username of the user who's currently logged in.

8
00:00:29,850 --> 00:00:35,760
So here I'll add another feel like poster and just go by and the posted by is going to be nothing,

9
00:00:35,760 --> 00:00:40,190
but it's going to be a character field of maximum length, let's say 100.

10
00:00:40,200 --> 00:00:43,710
So I could simply copy this, paste it up over here.

11
00:00:44,760 --> 00:00:45,890
I have made a typo.

12
00:00:45,900 --> 00:00:51,090
This should be posted by and once we have made these changes, let's go back to the terminal.

13
00:00:51,090 --> 00:00:52,410
Let's stop the server.

14
00:00:52,440 --> 00:00:56,550
Let's clear up the terminal first and now let's make migrations.

15
00:00:56,550 --> 00:01:00,120
So python manage dot be we make migrations.

16
00:01:01,270 --> 00:01:07,000
And now it's actually going to ask you for this particular thing wherein you actually need to have a

17
00:01:07,000 --> 00:01:11,500
one off default like by which particular user has the comment has been made.

18
00:01:11,590 --> 00:01:18,460
So here I will provide a one off default now by saying one, and here I'll actually type in the name

19
00:01:18,460 --> 00:01:21,850
as the super user name username.

20
00:01:22,210 --> 00:01:27,370
So that means whatever comments which we have up over here, they are all going to be associated with

21
00:01:27,370 --> 00:01:28,390
the super user.

22
00:01:28,990 --> 00:01:32,890
And I'll type enter and the migrations are made.

23
00:01:32,890 --> 00:01:36,800
And then I would finally say python minus dot py migrate.

24
00:01:36,940 --> 00:01:37,510
Okay.

25
00:01:37,900 --> 00:01:43,510
So now every comment which you have that particular comment is going to be associated with a super user.

26
00:01:43,510 --> 00:01:49,300
And now if you actually make a comment with some other user log, then that particular comment is associated

27
00:01:49,300 --> 00:01:50,230
with that user.

28
00:01:50,560 --> 00:01:56,530
So now once we have done this, there's one more thing which we need to do and that is if you go to

29
00:01:56,530 --> 00:01:57,040
the.

30
00:01:58,080 --> 00:01:59,190
From here.

31
00:01:59,490 --> 00:02:04,880
We also now want to say that we also want to add a posted by Fields to the form as well.

32
00:02:04,890 --> 00:02:07,440
So here I would say posted underscore by.

33
00:02:08,460 --> 00:02:13,800
And that's because when we submit a comment we also want the posted by field submitted as well.

34
00:02:14,370 --> 00:02:18,990
And now once we do that now I could go back go to the feed dot HTML.

35
00:02:18,990 --> 00:02:23,280
And here I also now need to submit the currently logged in user as well.

36
00:02:23,280 --> 00:02:29,700
So here, just as we have this particular hidden input field, I could just copy this and here paste

37
00:02:29,700 --> 00:02:30,690
it up over here.

38
00:02:30,690 --> 00:02:38,850
And instead of saying post dot ID, I would see posted underscore by the ID is going to be the same

39
00:02:38,850 --> 00:02:40,200
which is posted by.

40
00:02:40,290 --> 00:02:43,740
And the value of this thing is going to be the currently logged in user.

41
00:02:43,740 --> 00:02:45,480
So let's type in user here.

42
00:02:45,900 --> 00:02:47,760
Or we could also see the logged user.

43
00:02:47,760 --> 00:02:50,250
I guess logged user would be more appropriate.

44
00:02:50,700 --> 00:02:53,440
So let's see which one actually suits the best.

45
00:02:53,460 --> 00:02:57,200
So let's try with logged user and let's see if that thing works.

46
00:02:57,210 --> 00:03:00,060
So let's save this and let's see if it actually works.

47
00:03:00,360 --> 00:03:04,770
But even before actually running this, we go to the feed page.

48
00:03:05,340 --> 00:03:08,280
This is not running because I need to start the server.

49
00:03:08,700 --> 00:03:14,880
And now if I go to the feed page here, if I actually take a look at the comment here, as you can see,

50
00:03:14,880 --> 00:03:20,370
the username is not displayed because we also have to make changes to the way in which the comments

51
00:03:20,370 --> 00:03:21,510
are actually displayed.

52
00:03:21,870 --> 00:03:28,140
So in order to make those particular changes, what I need to do here is that I also need to associate

53
00:03:28,140 --> 00:03:30,480
the user who's associated with that comment.

54
00:03:30,480 --> 00:03:35,640
So here I'll go to the feed page, go to the code which actually displays the comments.

55
00:03:35,640 --> 00:03:36,990
So that's the code here.

56
00:03:37,230 --> 00:03:43,200
So right before the comment body, I also want to display the name of that user and let's say bold letters.

57
00:03:43,200 --> 00:03:46,170
So I would add a B tag here, which is for bold.

58
00:03:46,410 --> 00:03:48,900
And in here I'll make use of template syntax.

59
00:03:48,900 --> 00:03:53,670
And I would say comment dot posted underscore by.

60
00:03:53,880 --> 00:03:56,700
So if I do that, go back here, hit refresh.

61
00:03:56,820 --> 00:04:00,870
Now, as you can see, you know that the super user has actually made these comments.

62
00:04:01,170 --> 00:04:04,800
Now, let's try making another comment and let's see if that is successful.

63
00:04:04,800 --> 00:04:10,170
So here I would say new comment and if I click on ADD.

64
00:04:11,540 --> 00:04:17,180
As you can see right now, it's seeing anonymous user because maybe we are not currently logged in.

65
00:04:17,959 --> 00:04:21,740
And yeah, we are not logged in because it's showing up the login sign.

66
00:04:21,740 --> 00:04:24,200
So let me log in now as some user.

67
00:04:24,230 --> 00:04:25,820
Let's log in as Matt.

68
00:04:25,850 --> 00:04:27,500
So I login as Pat.

69
00:04:27,830 --> 00:04:32,490
And now once I'm logged in, I would go to post forward slash feed.

70
00:04:32,510 --> 00:04:34,400
Let's try commenting this.

71
00:04:36,130 --> 00:04:38,770
New comment by Pat.

72
00:04:38,770 --> 00:04:43,630
Lets say that's the comment and if I click on ADD, as you can see now we have the comment added by

73
00:04:43,630 --> 00:04:43,930
Pat.

74
00:04:43,930 --> 00:04:47,270
We'll see his new comment by Pat, which is all fine and good.

75
00:04:47,290 --> 00:04:52,690
Now this means that the comment functionality is working absolutely fine without any issues.

76
00:04:52,960 --> 00:04:58,720
Now the only problem is that this form does not actually looks like it has merged well with the post,

77
00:04:58,720 --> 00:04:59,680
which we have.

78
00:04:59,710 --> 00:05:03,100
So let's make a couple of modifications to that app over here.

79
00:05:03,280 --> 00:05:08,350
So I'll go right away inside the code and kind of chip away some of the classes which we have added

80
00:05:08,350 --> 00:05:09,130
to the form.

81
00:05:09,400 --> 00:05:10,540
So let's go over.

82
00:05:10,540 --> 00:05:12,100
The form actually starts.

83
00:05:13,420 --> 00:05:16,780
And for now, let's remove the shadow from here.

84
00:05:17,880 --> 00:05:23,890
Let's also remove the width, I guess, because the width is actually already limited by the post.

85
00:05:23,910 --> 00:05:25,710
Let's also remove the margin.

86
00:05:26,310 --> 00:05:32,880
Let's only keep the background and the rounded field over here and get rid of everything else from here.

87
00:05:34,280 --> 00:05:35,060
For this one.

88
00:05:35,060 --> 00:05:38,260
This is okay for the label which we have up over here.

89
00:05:38,270 --> 00:05:39,950
I don't think we need a label.

90
00:05:39,950 --> 00:05:43,610
So let's also get rid of this entire div along with the label as well.

91
00:05:43,880 --> 00:05:47,570
And for this one that is the common body.

92
00:05:48,290 --> 00:05:49,730
Let's remove the shadow.

93
00:05:50,090 --> 00:05:51,530
Let's remove appearance.

94
00:05:51,530 --> 00:05:52,520
None as well.

95
00:05:53,420 --> 00:05:56,450
Let's keep the margin right.

96
00:05:57,940 --> 00:05:59,470
Let's remove the padding.

97
00:05:59,470 --> 00:06:04,360
And also let's get rid of these divs here because we don't want them.

98
00:06:04,360 --> 00:06:05,890
So I'll remove this.

99
00:06:06,070 --> 00:06:07,310
And there we go.

100
00:06:07,330 --> 00:06:09,730
Let's see how this is going to look like right now.

101
00:06:09,850 --> 00:06:11,260
So I'll go back here.

102
00:06:11,900 --> 00:06:15,820
And as you can see, this is what the comment form looks like.

103
00:06:15,830 --> 00:06:19,250
This is still not a line to the right, but that's absolutely fine.

104
00:06:19,640 --> 00:06:21,480
We could actually add a flex here.

105
00:06:21,500 --> 00:06:27,590
So now, in order to align these two things together, I'll create another div and I'll add a class

106
00:06:27,590 --> 00:06:28,490
of flex to it.

107
00:06:28,490 --> 00:06:35,060
And once we have this flex inside this div, I'm actually going to take this div as well as the button

108
00:06:35,060 --> 00:06:40,250
input field which we have pasted in there so that these are actually going to be present on the same

109
00:06:40,250 --> 00:06:40,730
line.

110
00:06:41,090 --> 00:06:45,350
So now if I had to refresh, as you can see, this is what exactly what we wanted.

111
00:06:45,380 --> 00:06:48,530
We have this common feel here as well as the button up over here.

112
00:06:48,950 --> 00:06:54,890
So this actually completes the entire section of creating this particular social media app.

113
00:06:54,890 --> 00:06:57,490
And I hope you guys enjoyed creating this.

114
00:06:57,500 --> 00:07:02,690
So this has the functionality of being able to like being able to comment.

115
00:07:02,720 --> 00:07:07,160
Now, the only functionality which is not being covered here is the share functionality, and that's

116
00:07:07,160 --> 00:07:11,030
because this share functionality could be designed in multiple ways.

117
00:07:11,120 --> 00:07:16,160
One way to implement this is that you could actually go ahead and you could simply share this to other

118
00:07:16,160 --> 00:07:18,710
websites so you could add social widgets here.

119
00:07:18,980 --> 00:07:25,640
And also you could actually create a share page for every profile where the user could actually see

120
00:07:25,640 --> 00:07:27,770
the post shared by those users.

121
00:07:27,770 --> 00:07:30,410
So you could try implementing this on your own.

122
00:07:30,410 --> 00:07:33,550
And also there are a couple of great changes which you could make.

123
00:07:33,560 --> 00:07:39,650
So for example, let's say only when I click this particular comment box, this particular field or

124
00:07:39,650 --> 00:07:41,300
this particular form should appear.

125
00:07:41,750 --> 00:07:47,750
And the reason why that is not implemented here is because that's a part of JavaScript, and there's

126
00:07:47,750 --> 00:07:49,550
not much of Django in that.

127
00:07:49,880 --> 00:07:51,830
Therefore, that part has not been covered.

128
00:07:51,830 --> 00:07:58,100
But I highly recommend that you go ahead and kind of enhance or kind of improve this particular app

129
00:07:58,100 --> 00:08:03,920
by adding your own features to it, because that's going to really enhance your learning experience.

130
00:08:04,070 --> 00:08:06,730
So I hope you enjoyed making this project.

131
00:08:06,740 --> 00:08:08,540
So thank you very much for watching.

