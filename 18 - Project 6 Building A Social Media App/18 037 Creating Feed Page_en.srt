1
00:00:00,090 --> 00:00:05,460
So in this particular lecture, let's go ahead and let's design a page, which is the feed page which

2
00:00:05,460 --> 00:00:07,510
displays posts from all the users.

3
00:00:07,530 --> 00:00:13,590
So we already have this particular page which displays our very own post, but in a social media website

4
00:00:13,590 --> 00:00:18,000
you also need a page which will display posts from all the users of our website.

5
00:00:18,030 --> 00:00:20,820
Now, designing this particular page is pretty simple.

6
00:00:20,850 --> 00:00:25,590
Basically, it has the same structure or HTML structure as the my post page.

7
00:00:25,620 --> 00:00:30,840
The only change that's going to happen here is that instead of getting our very own posts, we are going

8
00:00:30,840 --> 00:00:35,050
to get the posts of each and every user who is actually present on our site.

9
00:00:35,070 --> 00:00:38,500
So let's go ahead and let's work on creating that particular feed page.

10
00:00:38,520 --> 00:00:41,430
So first of all, let's start off with creating a view.

11
00:00:41,460 --> 00:00:44,790
So in order to create a view, I'll go to the view start py file.

12
00:00:44,910 --> 00:00:46,800
So let me just go back here.

13
00:00:47,100 --> 00:00:51,360
Let's go to the posts and here let's go inside the views.

14
00:00:51,660 --> 00:00:57,930
Now, in here I have to create a view and let's call that particular view as something like feed.

15
00:00:57,960 --> 00:01:04,830
So I'll go ahead and I will see def feed because it's going to power up the user feed.

16
00:01:04,890 --> 00:01:07,650
This is going to accept a request as usual.

17
00:01:07,650 --> 00:01:12,920
And then first of all, we just have to go ahead and get all the posts from all the users.

18
00:01:12,930 --> 00:01:18,060
So in order to get all the posts from all the users, I simply have to go ahead and make use of the

19
00:01:18,060 --> 00:01:19,780
post model, which we have.

20
00:01:19,800 --> 00:01:22,440
So we have to first import that model.

21
00:01:23,220 --> 00:01:24,660
So I would say from.

22
00:01:25,600 --> 00:01:29,530
Dot models import the post model.

23
00:01:29,920 --> 00:01:35,070
Now, once we have this model, we are free to get all the objects out of this particular model.

24
00:01:35,080 --> 00:01:41,500
So in order to get all the objects here, I would simply go ahead and say posts to get all the post

25
00:01:41,500 --> 00:01:45,220
equals post, dot objects, dot all.

26
00:01:45,220 --> 00:01:49,000
And this is actually going to give us access to all the posts which we have.

27
00:01:49,180 --> 00:01:53,210
And now we simply have to pass these particular posts to a certain template.

28
00:01:53,230 --> 00:01:58,990
So here I would say return and render a template, but first pass in a request.

29
00:01:58,990 --> 00:02:05,500
So I would say request and then I would say post forward, slash and let's say we want to create a template

30
00:02:05,500 --> 00:02:07,780
which is called last feed dot HTML.

31
00:02:08,020 --> 00:02:13,090
And to this particular template we have to pass in the post which we have gotten from here.

32
00:02:13,390 --> 00:02:19,960
So in order to pass them, I would say posts and person posts here.

33
00:02:21,010 --> 00:02:25,840
Okay, so now our job is to go ahead and create this feed dot HTML template.

34
00:02:25,840 --> 00:02:27,790
So let's go ahead and do that right away.

35
00:02:27,790 --> 00:02:32,800
So right inside the template, inside posts, we simply have the create dot HTML.

36
00:02:32,860 --> 00:02:37,840
So I'll also create another one like feed dot HTML.

37
00:02:37,840 --> 00:02:43,300
And now the best thing is you don't have to write all the code which you have over here and the feed

38
00:02:43,300 --> 00:02:44,110
dot HTML.

39
00:02:44,110 --> 00:02:49,360
You simply have to go ahead and copy the code, which you already have powering up this page, which

40
00:02:49,360 --> 00:02:50,470
is my post page.

41
00:02:50,470 --> 00:02:56,890
So this particular my post page, which we have up over here, is powered by the page which is indexed

42
00:02:56,900 --> 00:02:57,700
on HTML.

43
00:02:57,700 --> 00:03:03,910
So if you go to the app, which is users app, if you go to templates inside of it, and if you actually

44
00:03:03,910 --> 00:03:10,230
take a look at this indexed HTML, this page or this HTML code is the one which powers up that page.

45
00:03:10,240 --> 00:03:16,450
Now you simply have to go ahead, copy the entire thing from here and paste it up into feed dot HTML.

46
00:03:16,450 --> 00:03:22,000
And here we have already looked through all the posts and we have basically displayed the title caption

47
00:03:22,000 --> 00:03:23,200
and everything up over here.

48
00:03:23,200 --> 00:03:28,120
So once this thing is done now, we have basically added the code for free HTML.

49
00:03:28,390 --> 00:03:31,150
So that means this feed dot HTML page is ready.

50
00:03:31,150 --> 00:03:32,920
We also have this view created.

51
00:03:32,920 --> 00:03:37,480
Now we simply have to associate this particular view with a URL pattern.

52
00:03:37,480 --> 00:03:44,050
So in order to do that, I'll actually go back and go to the URLs dot py file, which is this file right

53
00:03:44,050 --> 00:03:47,920
here and associate the current view with a new URL pattern.

54
00:03:47,920 --> 00:03:54,310
So for the feed page, I will say let's say the path is going to be feed and the view which is going

55
00:03:54,310 --> 00:03:56,880
to be associated with this is going to be the feed view.

56
00:03:56,890 --> 00:04:02,500
So view start feed and the name of this thing is going to be feed.

57
00:04:02,710 --> 00:04:07,120
So now once this thing is done, let's see if this feed URL works.

58
00:04:07,300 --> 00:04:12,670
So here, I'll go back, I'll go to forward slash posts, forward slash feed.

59
00:04:13,480 --> 00:04:18,250
And if I go back here, as you can see now, we have the post showing up over here.

60
00:04:18,279 --> 00:04:23,740
Now, the problem with this is that this is actually working, but we are not sure if it's actually

61
00:04:23,740 --> 00:04:26,200
able to retrieve all the posts from all the users.

62
00:04:26,380 --> 00:04:30,700
So in order to check that, let's actually go to admin.

63
00:04:31,060 --> 00:04:34,590
So I'll open up another tab here and I would go to admin.

64
00:04:34,600 --> 00:04:41,200
So currently if I go to posts, as you can see, we only have two posts here, so let's create another

65
00:04:41,200 --> 00:04:42,700
post by some other user.

66
00:04:42,700 --> 00:04:46,300
So here I'll select a user like PAD.

67
00:04:46,330 --> 00:04:49,420
I'll choose a file which is image file.

68
00:04:49,420 --> 00:04:51,220
So I've chosen an image file.

69
00:04:51,220 --> 00:04:59,440
I would say this is the caption and then I will select a title like title.

70
00:04:59,440 --> 00:05:03,190
The Slug is going to be automatically generated if I save this.

71
00:05:03,610 --> 00:05:07,590
Now, as you can see, we have this post with a title as title.

72
00:05:07,600 --> 00:05:13,450
Now, if I go back to the older tab, which we have here, if I had refresh now, as you can see, you'll

73
00:05:13,450 --> 00:05:16,720
also be able to see this particular post by pad.

74
00:05:16,960 --> 00:05:22,150
But now the problem is that we are actually able to go ahead and retrieve the username and everything,

75
00:05:22,150 --> 00:05:24,790
but we are not able to retrieve the user image.

76
00:05:24,790 --> 00:05:27,190
And there's a specific reason for that.

77
00:05:27,190 --> 00:05:32,890
And that reason is if you actually take a look at the feed dot HTML, as you can see, the way in which

78
00:05:32,890 --> 00:05:37,210
we get the profile picture is that we see profile .42. URL.

79
00:05:37,210 --> 00:05:42,700
But now this isn't going to work because right now we are not passing in any kind of profile there.

80
00:05:42,790 --> 00:05:48,160
So if you actually take a look at the view which actually runs the index page, So in order to access

81
00:05:48,160 --> 00:05:51,820
that view, you actually have to go to the users app.

82
00:05:51,820 --> 00:05:53,590
So let's go to users.

83
00:05:53,590 --> 00:05:57,490
If you go to The View, which runs the index template.

84
00:05:57,910 --> 00:06:00,010
So let's take a look at this view.

85
00:06:00,760 --> 00:06:03,850
Let's take a look at the index view, which we have.

86
00:06:04,510 --> 00:06:05,850
So this is a login.

87
00:06:05,860 --> 00:06:07,030
This is edit.

88
00:06:07,120 --> 00:06:09,580
Okay, so here we have the indexed view right here.

89
00:06:09,580 --> 00:06:15,280
And to this indexed view, we were actually passing in this profile object as context.

90
00:06:15,280 --> 00:06:19,990
But right now if you go to this view, we are not passing that anymore and that's because we don't want

91
00:06:19,990 --> 00:06:26,170
to access an individual's profile, but instead we actually want to go ahead and access the profile

92
00:06:26,170 --> 00:06:30,160
associated with the person who's associated with a specific post.

93
00:06:30,370 --> 00:06:35,860
So in that particular case, we have to make some modifications to our free HTML page.

94
00:06:35,860 --> 00:06:41,200
And the first modification, which you have to make here is that instead of getting access to the profile

95
00:06:41,230 --> 00:06:46,840
here, instead what I would say is that I would say, first get the post because we are getting access

96
00:06:46,840 --> 00:06:48,940
to the post here from all the posts.

97
00:06:49,060 --> 00:06:52,900
So every post is going to be associated with a user.

98
00:06:52,900 --> 00:06:57,520
So I would say post dot user and then I will access the user's profile.

99
00:06:57,520 --> 00:07:01,390
So dot profile and then finally I will access the profile photo.

100
00:07:01,390 --> 00:07:04,390
So I would say photo, but we cannot just stop there.

101
00:07:04,390 --> 00:07:10,240
We also have to access the URL of that photo because for image source you have to pass in a URL.

102
00:07:10,300 --> 00:07:12,130
So here I will see a URL.

103
00:07:12,130 --> 00:07:17,110
And now finally, this thing would be functional, so let's see if that thing works.

104
00:07:17,110 --> 00:07:20,470
So I'll go back here to the browser.

105
00:07:20,920 --> 00:07:22,360
If I had to refresh this time.

106
00:07:22,360 --> 00:07:29,890
As you can see now we have the username, the profile picture, the entire post displayed up over here

107
00:07:29,890 --> 00:07:31,390
without any issues.

108
00:07:31,390 --> 00:07:34,780
So as you can see now, the feed page is fully functional.

109
00:07:34,780 --> 00:07:40,120
So now we have created this particular feed functionality, which kind of gives you an overview of all

110
00:07:40,120 --> 00:07:43,770
the posts which are uploaded by all the users present on our website.

111
00:07:43,780 --> 00:07:50,820
Now, from the next lecture onwards, let's start designing the functionality to like a particular image.

112
00:07:50,830 --> 00:07:56,380
So for example, when I click this particular heart icon, it should actually go ahead and like that

113
00:07:56,380 --> 00:07:57,130
particular image.

114
00:07:57,130 --> 00:08:00,130
So let's learn how exactly we could do that in the next one.

