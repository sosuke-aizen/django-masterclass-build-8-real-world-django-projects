1
00:00:00,120 --> 00:00:05,280
So now let's actually create a super user so that we could test are log in functionality.

2
00:00:05,280 --> 00:00:11,430
So in order to create a super user, I have to go back to the terminal, stop the server which is currently

3
00:00:11,430 --> 00:00:11,970
running.

4
00:00:11,970 --> 00:00:14,180
And in here, let's clear up the terminal.

5
00:00:14,190 --> 00:00:19,920
And in order to create a super user, I simply have to use a command which is python, manage dot b

6
00:00:19,920 --> 00:00:24,630
y, create super user ID enter.

7
00:00:24,630 --> 00:00:27,890
And now it's actually going to ask us for the username.

8
00:00:27,900 --> 00:00:30,900
I'm going to use the same username as displayed here.

9
00:00:30,900 --> 00:00:32,380
So let's hit enter.

10
00:00:32,400 --> 00:00:34,200
Let's type in some dummy email.

11
00:00:34,200 --> 00:00:39,510
Like let's type in some dummy email like demo at gmail dot com.

12
00:00:40,530 --> 00:00:49,620
Let's type in the password as super user, type in the password again as super user, and now the super

13
00:00:49,620 --> 00:00:50,760
user is created.

14
00:00:51,090 --> 00:00:56,820
Now, once the super user is created, let's see if we could actually log in using the super user credentials

15
00:00:56,820 --> 00:00:59,280
and the login form which we have created.

16
00:00:59,280 --> 00:01:04,230
And that's actually going to be a test to see if the login form works.

17
00:01:04,440 --> 00:01:08,160
So here, first of all, let's run the project back.

18
00:01:08,160 --> 00:01:10,980
So Python managed not be run server.

19
00:01:11,010 --> 00:01:12,780
Now the project is up and running.

20
00:01:13,170 --> 00:01:17,190
Let's go back to our login form, hit refresh.

21
00:01:18,060 --> 00:01:20,790
Let's try and bring some invalid credentials.

22
00:01:20,790 --> 00:01:26,010
So here I have typed in the wrong username here and let's type in some wrong password as well.

23
00:01:26,100 --> 00:01:29,160
So if I had submit it says invalid credentials.

24
00:01:29,160 --> 00:01:34,140
Let's go back here and this time let's try and bring the correct credentials.

25
00:01:34,140 --> 00:01:39,030
So let me type in the password as super user.

26
00:01:39,060 --> 00:01:45,450
So if I type in the correct credentials and hit submit, as you can see now, it says user authenticated

27
00:01:45,450 --> 00:01:52,020
and logged in, which means now we are currently logged in into the system using the super user credentials.

28
00:01:52,020 --> 00:01:57,450
That means our login form is working absolutely fine and we do not have any issues in there.

29
00:01:57,750 --> 00:02:03,420
And if you are facing any issues in the login functionality, make sure that your indentations are proper

30
00:02:03,420 --> 00:02:08,490
here and you understand which if l statement actually match up with each other.

31
00:02:08,880 --> 00:02:12,600
So once that thing is done, you should be pretty much good to go.

32
00:02:12,600 --> 00:02:15,780
And our login functionality is now up and running.

33
00:02:15,990 --> 00:02:21,240
Now, once we have designed the login functionality, let's also design the log out functionality as

34
00:02:21,240 --> 00:02:21,690
well.

35
00:02:21,720 --> 00:02:27,270
So in the next lecture, let's go ahead and let's create the log out functionality in Django.

36
00:02:27,390 --> 00:02:31,530
So thank you very much for watching and I'll see you guys in the next one.

37
00:02:31,560 --> 00:02:32,370
Thank you.

