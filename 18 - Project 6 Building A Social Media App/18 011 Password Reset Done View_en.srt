1
00:00:00,120 --> 00:00:05,190
Okay, So now once we have this particular phone with us, let's see how we could reset the password.

2
00:00:05,490 --> 00:00:07,680
So let's first go to the admin panel.

3
00:00:10,490 --> 00:00:16,370
So let's log in in the admin panel here and if I go to users, you'll be able to find that if you click

4
00:00:16,370 --> 00:00:21,400
on the user, you will actually have the email address associated with that user.

5
00:00:21,410 --> 00:00:26,360
So whenever you have to reset the password for this user, you have to make use of this email address

6
00:00:26,360 --> 00:00:26,860
right here.

7
00:00:26,870 --> 00:00:29,470
So let's try doing that and let's see what happens.

8
00:00:29,480 --> 00:00:35,630
So I have typed in the email address up over here and now if I click on Submit, as you can see, it

9
00:00:35,630 --> 00:00:37,820
sees this sort of token as missing.

10
00:00:37,820 --> 00:00:39,920
So let's see what exactly went wrong.

11
00:00:40,370 --> 00:00:43,550
So I'll go back to VTS code, I'll go back here.

12
00:00:43,580 --> 00:00:47,330
We do have a token here, but for some reason the form isn't working.

13
00:00:47,330 --> 00:00:52,280
Okay, So when we submit this particular form, what technically happen is that we should be able to

14
00:00:52,280 --> 00:00:55,460
get a specific message in the back end.

15
00:00:55,460 --> 00:01:00,620
So when we type this email over here and when we click on submit, we should actually get a password

16
00:01:00,620 --> 00:01:01,880
reset link in the email.

17
00:01:01,880 --> 00:01:07,610
But as I earlier mentioned, as we are not running a real web app here in Django, what we need to do

18
00:01:07,610 --> 00:01:14,510
is that we need to log the password reset link in the console itself, and in order to do that, you

19
00:01:14,510 --> 00:01:20,450
need to go to VTS code and then here you need to go to the settings dot py file of your main app.

20
00:01:20,450 --> 00:01:23,990
And here you actually need to set up the email back end.

21
00:01:23,990 --> 00:01:31,160
So here you need to say email back and equals and that should actually point to the console here.

22
00:01:31,340 --> 00:01:41,600
So here you need to say django echo, dot mail, dot backends, dot console, dot, email, backend.

23
00:01:41,600 --> 00:01:48,020
And what this does is that it basically is going to log the password reset link into the console.

24
00:01:48,020 --> 00:01:50,300
So let's see if that actually works.

25
00:01:50,510 --> 00:01:51,950
So let's go back here.

26
00:01:51,980 --> 00:01:56,630
Let's visit this URL, let's type in the email and when I click submit.

27
00:01:57,560 --> 00:02:01,610
It sees no reverse match found at password reset conform.

28
00:02:01,610 --> 00:02:05,930
And we are getting this error because we are yet to set a couple of more templates.

29
00:02:05,930 --> 00:02:10,240
So this particular password reset process is not a simple process.

30
00:02:10,250 --> 00:02:15,680
So when you click on the submit button, technically you should actually get a message which says that

31
00:02:15,680 --> 00:02:21,740
the password reset link has been sent and in order to set that thing up, what you need to do is that

32
00:02:21,740 --> 00:02:26,930
you need to set up another view, which is called as the password reset done view, which basically

33
00:02:26,930 --> 00:02:33,110
tells you that, okay, the password reset process has been started and you have been mailed or you

34
00:02:33,110 --> 00:02:36,500
have been console logged the password reset link.

35
00:02:36,500 --> 00:02:43,730
So in order to add that, first of all, what you have to do is you have to go to Vsco and go to the

36
00:02:43,730 --> 00:02:45,320
URL start by file.

37
00:02:45,590 --> 00:02:47,930
And here you need to add a path.

38
00:02:47,930 --> 00:02:57,050
And this path is actually going to be something like password reset, forward, slash, done.

39
00:02:57,710 --> 00:03:01,610
And this is also going to be an art view, which is a built in art view.

40
00:03:01,610 --> 00:03:05,570
And here you need to type in password reset, done view.

41
00:03:06,380 --> 00:03:08,840
And this needs to have an as view.

42
00:03:08,840 --> 00:03:12,440
And we are also going to pass in a template to this as well.

43
00:03:12,980 --> 00:03:19,190
So the template and the school name is going to be empty for now because we have not yet created the

44
00:03:19,190 --> 00:03:19,820
template.

45
00:03:19,820 --> 00:03:26,720
But then again, the name of this view is going to be nothing, but it's going to be password reset

46
00:03:26,720 --> 00:03:31,590
done, give a comma at the end and we should be good to go.

47
00:03:31,610 --> 00:03:36,590
Now let's create the template, which is nothing but password reset done template.

48
00:03:36,680 --> 00:03:39,050
So let's go to the templates file here.

49
00:03:40,920 --> 00:03:49,500
And let's create a new template, let's call it as password reset than dot HTML.

50
00:03:49,500 --> 00:03:52,230
And this is going to say something like, let's say.

51
00:03:54,080 --> 00:04:03,080
We have mailed you the password reset link and we won't be adding anything fancy here so as to keep

52
00:04:03,080 --> 00:04:03,890
things simple.

53
00:04:04,400 --> 00:04:08,240
But still, let's go ahead and let's extend from the base dot HTML.

54
00:04:08,390 --> 00:04:11,720
So let's make use of some code which we have here.

55
00:04:12,500 --> 00:04:14,420
So let's use that.

56
00:04:16,579 --> 00:04:19,100
Let's also use the end block from here.

57
00:04:20,570 --> 00:04:20,870
Okay.

58
00:04:20,870 --> 00:04:25,370
So once we have added this template, let's go to the URL start p file.

59
00:04:26,540 --> 00:04:36,350
And in here, let's add the template name as password, reset, turn, dot, HTML, and make sure that

60
00:04:36,350 --> 00:04:40,250
you actually refer to this thing from the user's app.

61
00:04:40,520 --> 00:04:42,770
So hopefully that thing is correct.

62
00:04:44,330 --> 00:04:46,220
Okay, so now let's get back.

63
00:04:46,220 --> 00:04:49,340
Let's try adding some email and let's see, what do we get?

64
00:04:49,820 --> 00:04:53,090
Okay, so when I click submit it again gives us the same error.

65
00:04:53,090 --> 00:04:58,430
And that's because now when we actually need to get the password reset link, we must also configure

66
00:04:58,430 --> 00:05:04,550
the view, which is password reset, confirm view and associate it with password reset, confirm you

67
00:05:04,550 --> 00:05:05,030
all.

68
00:05:05,330 --> 00:05:10,190
But the problem now we are getting is that we have not yet configured the password reset.

69
00:05:10,190 --> 00:05:13,400
Confirm you are and we actually have to do that very soon.

70
00:05:13,610 --> 00:05:19,760
So in the next lecture, let's go ahead and let's set up this particular password reset, confirm view,

71
00:05:19,760 --> 00:05:22,730
and also set up the template which is associated with it.

72
00:05:23,060 --> 00:05:27,140
So thank you very much for watching and I'll see you guys in the next one.

73
00:05:27,260 --> 00:05:27,890
Thank you.

