1
00:00:00,090 --> 00:00:05,520
In this particular lecture, let's work on creating the common functionality for our application.

2
00:00:05,520 --> 00:00:10,860
So in order to create the common functionality, first of all, we actually have to go ahead and create

3
00:00:10,860 --> 00:00:15,060
a model for saving all of the comments which are related to a post.

4
00:00:15,300 --> 00:00:19,860
So here let's go to the model start py file of the app which is post.

5
00:00:19,860 --> 00:00:26,850
So make sure that you are into your post app and go inside the model, start py file and write up over

6
00:00:26,850 --> 00:00:27,070
here.

7
00:00:27,110 --> 00:00:32,400
You already have this particular post model and right now we are going to create a comment which is

8
00:00:32,400 --> 00:00:37,860
going to be associated with the post because obviously when you create a comment, a comment cannot

9
00:00:37,860 --> 00:00:39,060
exist on its own.

10
00:00:39,060 --> 00:00:40,750
It needs to have a post.

11
00:00:40,770 --> 00:00:42,450
So let's create the comment model.

12
00:00:42,450 --> 00:00:48,840
So here I would say class comment and then let's pass an model start model here.

13
00:00:48,840 --> 00:00:51,550
And now let's add the first field of a comment.

14
00:00:51,570 --> 00:00:56,700
Now, every comment which you create that needs to be associated with a post and also you need to remember

15
00:00:56,700 --> 00:01:00,240
that a comment could only be associated with one post.

16
00:01:00,240 --> 00:01:07,140
A comment cannot be associated with two posts and therefore we'll add a post field here which is actually

17
00:01:07,140 --> 00:01:09,430
going to be a foreign key for the comment.

18
00:01:09,450 --> 00:01:15,150
So here I will say models dot foreign key and here the foreign key is going to be post.

19
00:01:15,150 --> 00:01:17,640
So I'll pass in post here and on delete.

20
00:01:17,640 --> 00:01:24,480
I would say models dot cascade and I'll also set the related name of this one.

21
00:01:24,480 --> 00:01:28,410
And the related name is going to be nothing but let's say comments.

22
00:01:28,620 --> 00:01:28,980
Okay?

23
00:01:28,980 --> 00:01:31,620
So once this thing is done, we are pretty much good to go.

24
00:01:31,860 --> 00:01:35,790
Now, you could add as many number of things in this particular comment.

25
00:01:35,800 --> 00:01:41,370
So, for example, a comment could have the posted by date, the updated date, so on and so forth.

26
00:01:41,370 --> 00:01:45,500
But we are going to keep things simple and we are simply going to have a common body.

27
00:01:45,510 --> 00:01:49,650
So here I will say body equals and this is going to be simply text.

28
00:01:49,650 --> 00:01:52,230
So models start text field.

29
00:01:52,230 --> 00:01:57,660
And once we have this, let's also add another feel like created and this created field is actually

30
00:01:57,660 --> 00:02:01,830
going to be the date and time when the comment is actually created.

31
00:02:01,830 --> 00:02:10,410
So here we'll see models dot date time field and then I'll also set the auto now to true, which means

32
00:02:10,410 --> 00:02:15,840
that whenever a comment is created, the created field is automatically going to be set to the current

33
00:02:15,840 --> 00:02:16,650
date and time.

34
00:02:16,950 --> 00:02:24,210
And also when the comments are actually loop through, let's say we want to order them by this particular

35
00:02:24,210 --> 00:02:25,370
created field.

36
00:02:25,380 --> 00:02:28,410
So here I'll add a class stating the same.

37
00:02:28,590 --> 00:02:37,950
So here I will say class matter and I would set the ordering of this thing to nothing but created give

38
00:02:37,950 --> 00:02:41,550
a comma at the end so that Shango understands that this is a typo.

39
00:02:42,000 --> 00:02:45,510
And then I'll also add a string method for this one.

40
00:02:45,690 --> 00:02:50,220
So I would say def double underscore is the double underscore.

41
00:02:50,220 --> 00:02:56,840
Let's pass in self and let's return self dot body, which means the body of the comment itself.

42
00:02:56,850 --> 00:03:00,050
So once this thing is done, we are pretty much good to go.

43
00:03:00,060 --> 00:03:03,530
So now once this model is created, let's make migrations.

44
00:03:03,540 --> 00:03:06,180
So I'll go back to the terminal.

45
00:03:06,300 --> 00:03:07,590
Stop the server.

46
00:03:08,940 --> 00:03:19,710
I would say Python managed by make migrations and then I would say Python and start by migrate.

47
00:03:19,860 --> 00:03:22,020
Let's run the server back up again.

48
00:03:23,080 --> 00:03:28,120
And once the server is up and running, let's go back to the browser.

49
00:03:28,960 --> 00:03:32,050
If I go up over here, let's log in.

50
00:03:32,050 --> 00:03:36,190
And now, as you can see, there's no model right up over here which these comments.

51
00:03:36,190 --> 00:03:40,270
And that's because whenever we create a model, we also have to register that.

52
00:03:40,270 --> 00:03:49,690
So let's go to the admin API file up over here and I would say admin dot site, dot register and register

53
00:03:49,690 --> 00:03:51,610
the model which is comment.

54
00:03:51,700 --> 00:03:52,070
Okay.

55
00:03:52,090 --> 00:03:54,730
And let's also import comments as well over here.

56
00:03:54,730 --> 00:03:56,530
So comment is going to be here.

57
00:03:56,980 --> 00:04:03,640
If I go back, hit refresh as you can see, if I go to comments now, we have an option to add a comment

58
00:04:03,640 --> 00:04:09,520
with the post, which is any of the post which we have in our application and we also have the comment

59
00:04:09,520 --> 00:04:10,390
body as well.

60
00:04:10,810 --> 00:04:15,100
So let's try adding a comment here for this nature image post.

61
00:04:15,430 --> 00:04:21,790
And I would say this is a great nature picture.

62
00:04:21,910 --> 00:04:24,340
Nice job clicking it.

63
00:04:24,340 --> 00:04:28,810
So it's a comment and the created field is automatically going to be added.

64
00:04:28,810 --> 00:04:34,960
So if I add that now that comment is created and it is now going to be associated with the post, which

65
00:04:34,960 --> 00:04:35,710
is this one.

66
00:04:36,100 --> 00:04:41,740
Now our next job is to actually go ahead and display this particular comment out up on our homepage,

67
00:04:42,370 --> 00:04:44,680
which is this particular post page right here.

68
00:04:44,890 --> 00:04:48,160
So we are going to learn how to do that in the next lecture.

69
00:04:48,160 --> 00:04:52,360
So thank you very much for watching and I'll see you guys in the next one.

70
00:04:52,390 --> 00:04:53,140
Thank you.

