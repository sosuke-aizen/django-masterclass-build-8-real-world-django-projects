1
00:00:00,090 --> 00:00:03,630
So in this particular lecture, let's style up the form which we have.

2
00:00:03,660 --> 00:00:07,980
So let's start off by styling up the outermost element, which is the form itself.

3
00:00:08,010 --> 00:00:12,810
So for this form which we have, I want to add a class, I want to set the background of this thing

4
00:00:12,810 --> 00:00:14,230
to white.

5
00:00:14,250 --> 00:00:18,920
I want to add a shadow to this form which is going to be of the medium size.

6
00:00:18,930 --> 00:00:21,360
I want the margin of this thing to be ten.

7
00:00:21,480 --> 00:00:24,380
I want the width of this thing to be 4/5.

8
00:00:24,390 --> 00:00:27,570
And we are going to modify this if we found this to be weird.

9
00:00:27,810 --> 00:00:29,850
Let's make it rounded.

10
00:00:31,150 --> 00:00:33,820
Let's set the padding on the x axis to be eight.

11
00:00:33,850 --> 00:00:37,810
Let's set the padding at the top as Let's see six.

12
00:00:38,020 --> 00:00:38,620
Let's see.

13
00:00:38,620 --> 00:00:41,500
The padding at the bottom is going to be eight.

14
00:00:41,500 --> 00:00:42,190
And let's see.

15
00:00:42,190 --> 00:00:44,620
The margin at the bottom is going to be four.

16
00:00:45,310 --> 00:00:47,460
So let's see how the form looks like.

17
00:00:47,470 --> 00:00:49,090
So if I had to refresh.

18
00:00:50,130 --> 00:00:54,930
So let's revisit the speech and this is what the form looks like.

19
00:00:55,050 --> 00:01:01,800
It kind of is looking weird because right now we have actually zoomed in quite a lot.

20
00:01:01,890 --> 00:01:04,110
But let's see how this is going to turn out.

21
00:01:04,110 --> 00:01:05,489
So I'll go back again.

22
00:01:05,790 --> 00:01:11,670
And right now, let's start styling up this particular div, which we have, which is the div for the

23
00:01:11,670 --> 00:01:12,110
label.

24
00:01:12,120 --> 00:01:18,810
So for the label, let's say I want to add a margin bottom to two and for the label itself, I want

25
00:01:18,810 --> 00:01:22,650
to add a class and I want to make this thing to be of a block.

26
00:01:22,800 --> 00:01:28,380
I want the text color of this thing to be gray with the shade of 700.

27
00:01:28,410 --> 00:01:30,840
I want the text to be smaller.

28
00:01:30,870 --> 00:01:33,840
I want the font to be bolder.

29
00:01:35,800 --> 00:01:38,410
And I want the margin bottom to be, too.

30
00:01:38,440 --> 00:01:40,780
Now, let's pile up this debt, which we have up over here.

31
00:01:40,810 --> 00:01:43,030
So first of all, I'll add a class here.

32
00:01:43,360 --> 00:01:45,430
I would add a shadow to this one.

33
00:01:45,640 --> 00:01:48,730
I would say appearance of this thing is going to be none.

34
00:01:48,760 --> 00:01:52,250
I want to add a border because right now we do not have any border.

35
00:01:52,270 --> 00:01:53,920
I want to make it rounded.

36
00:01:54,040 --> 00:01:59,500
I want the padding on the y axis to be two padding on the x axis, to be three margin on the right to

37
00:01:59,500 --> 00:02:00,220
be ten.

38
00:02:00,730 --> 00:02:04,990
I want the text to be gray with the shade of 700.

39
00:02:05,650 --> 00:02:06,220
Okay.

40
00:02:06,550 --> 00:02:12,010
So once we are done with this, let's go ahead and let's style up the next which we have.

41
00:02:12,190 --> 00:02:15,040
So I guess for this particular div, let's keep it simple.

42
00:02:15,190 --> 00:02:21,370
And for the final one, which we have the button, let's remove the div because we want this input field

43
00:02:21,370 --> 00:02:23,200
and this one to be on the same line.

44
00:02:23,200 --> 00:02:27,880
So I'll actually remove this from here and let's style up the button individually.

45
00:02:27,880 --> 00:02:32,980
So I would say class is going to be background green with a shade of 500.

46
00:02:33,220 --> 00:02:35,780
Obviously then the text needs to be white.

47
00:02:35,800 --> 00:02:40,340
I want to add a padding X of five and padding y of two.

48
00:02:40,360 --> 00:02:43,070
So let's get back here and let's see how it looks like.

49
00:02:43,090 --> 00:02:45,730
So as you can see, this is what the form looks like right now.

50
00:02:45,730 --> 00:02:47,350
And still it kind of looks weird.

51
00:02:47,350 --> 00:02:52,750
And that's because if you take a look at this form, this particular field, which we have here, we

52
00:02:52,750 --> 00:02:58,460
have actually made this particular field into I guess we have actually turned it into a text field.

53
00:02:58,480 --> 00:03:01,460
So let me check the model here for comments.

54
00:03:01,480 --> 00:03:06,700
So if you go to models dot p file, as you can see, we have actually used the text field here.

55
00:03:06,910 --> 00:03:12,190
So I would rather go ahead and change it to cow field because right now we don't want an entire text

56
00:03:12,190 --> 00:03:12,740
area here.

57
00:03:12,760 --> 00:03:17,030
Let's say we want to post smaller comments and not an entire paragraph.

58
00:03:17,050 --> 00:03:22,030
And also, as we have added calf field here, I also need to specify the maxlength as, let's say,

59
00:03:22,030 --> 00:03:22,780
100.

60
00:03:22,810 --> 00:03:26,860
Now, once this thing is done, I have to stop the server and make migrations again.

61
00:03:27,040 --> 00:03:31,540
So python managed by make migrations.

62
00:03:32,430 --> 00:03:36,120
And then Python managed by migrate.

63
00:03:36,270 --> 00:03:39,360
Now, once the migrations are made, let's run the server again.

64
00:03:39,360 --> 00:03:40,240
Let's go back here.

65
00:03:40,260 --> 00:03:41,190
Hit refresh.

66
00:03:41,310 --> 00:03:45,130
And now, as you can see, the common form actually looks a lot better.

67
00:03:45,150 --> 00:03:49,290
So now the comment form here is ready, and I could add any comment here.

68
00:03:49,290 --> 00:03:52,380
And that comment should be added up over here without any issues.

69
00:03:52,380 --> 00:03:54,170
And we have that comment added here.

70
00:03:54,180 --> 00:03:57,450
So now the comment functionality is working absolutely fine.

71
00:03:57,450 --> 00:04:02,850
But now the only problem which we need to solve is that all the comments which we have added on a particular

72
00:04:02,850 --> 00:04:07,040
post, these comments are not associated with any kind of user.

73
00:04:07,050 --> 00:04:12,510
And what we want to do here is that we actually want to associate every comment with the person who's

74
00:04:12,510 --> 00:04:13,430
currently logged in.

75
00:04:13,440 --> 00:04:19,649
So for example, right now if I'm logged in as a super user in that particular case, I need to be able

76
00:04:19,649 --> 00:04:21,450
to go ahead and make a comment.

77
00:04:21,450 --> 00:04:27,420
And when I make a comment, my user name should appear right before the comment itself over here.

78
00:04:27,420 --> 00:04:29,910
So let's add that functionality in the next one.

