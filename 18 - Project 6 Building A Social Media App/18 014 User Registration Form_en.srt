1
00:00:00,090 --> 00:00:05,430
Now let's go ahead and design the functionality to register users on our website.

2
00:00:05,700 --> 00:00:10,680
So in order to create a registration functionality, first of all, we have to create a registration

3
00:00:10,680 --> 00:00:11,240
form.

4
00:00:11,250 --> 00:00:12,830
And we all know that forever.

5
00:00:12,840 --> 00:00:14,100
We have to create forms.

6
00:00:14,100 --> 00:00:19,890
We already have a file which is called last form start P up over here, which basically goes ahead and

7
00:00:19,890 --> 00:00:22,740
allows us to create any kind of forms which we want.

8
00:00:22,770 --> 00:00:29,220
So we have already created a login form and in a similar fashion we will also be creating a user registration

9
00:00:29,220 --> 00:00:30,120
form as well.

10
00:00:30,240 --> 00:00:36,060
And we are going to then render that particular form by attaching it to a view and then passing that

11
00:00:36,060 --> 00:00:40,620
particular form from a view to finally a HTML template.

12
00:00:40,650 --> 00:00:42,300
So let's go ahead and do that.

13
00:00:42,300 --> 00:00:44,130
So let's first create a form here.

14
00:00:44,310 --> 00:00:47,730
So this is actually going to be a user registration form.

15
00:00:47,730 --> 00:00:52,500
So I would say class user registration form.

16
00:00:52,500 --> 00:00:57,320
And this is going to inherit from form, start, model form.

17
00:00:57,330 --> 00:01:02,790
And here, as you can see, this particular form is not a simple form, it's actually a model form,

18
00:01:02,790 --> 00:01:06,480
which means that this particular form actually works on a particular model.

19
00:01:07,110 --> 00:01:09,150
So I have misspelled this.

20
00:01:09,150 --> 00:01:11,100
This should be form, start, model, form.

21
00:01:11,100 --> 00:01:15,920
And now this particular form, as it's a model form, we have to pass in a specific model.

22
00:01:15,930 --> 00:01:19,680
So this particular form needs to work on a user model.

23
00:01:19,680 --> 00:01:25,200
So essentially what we will be doing is that we will be using the in-built user model, which Django

24
00:01:25,200 --> 00:01:27,540
has, and then making use of this form.

25
00:01:27,540 --> 00:01:31,820
We will actually go ahead and set up the fields inside that particular model.

26
00:01:31,830 --> 00:01:34,530
So let's first import that particular model here.

27
00:01:34,560 --> 00:01:39,000
So that particular model is actually present in Django Report.

28
00:01:39,000 --> 00:01:48,300
Auth So I would say from Django dot contrib dot auth dot models and from this I want to import the user

29
00:01:48,300 --> 00:01:48,870
model.

30
00:01:48,870 --> 00:01:53,790
So once we have this particular user model, we could pass that particular model to this particular

31
00:01:53,790 --> 00:01:59,740
form which basically states that this form is going to be built for getting the data for the user model.

32
00:01:59,760 --> 00:02:06,510
So here in order to pass the information about the form itself, we will have a method class which allows

33
00:02:06,510 --> 00:02:09,660
us to add the information about the form itself.

34
00:02:09,690 --> 00:02:16,440
Now here we say that we want to target the user model, and we'll also add the fields which we want

35
00:02:16,440 --> 00:02:18,320
to use in this particular form.

36
00:02:18,330 --> 00:02:25,230
So we want to use the user name, we want to use the email, and we also want to use the first name

37
00:02:25,230 --> 00:02:25,650
as well.

38
00:02:25,650 --> 00:02:27,690
So I would say first underscore name.

39
00:02:27,930 --> 00:02:28,350
Okay.

40
00:02:28,350 --> 00:02:35,490
So once this thing is done, now the next thing which we do is that we also go ahead and we set the

41
00:02:35,490 --> 00:02:36,870
password fields for this.

42
00:02:36,900 --> 00:02:42,060
Now, the reason why we are doing this is because by default, these particular password fields are

43
00:02:42,060 --> 00:02:47,580
called less password and password two, and we want to change the labels of those particular fields.

44
00:02:47,580 --> 00:02:50,640
So in here I would create a field called Last password.

45
00:02:50,640 --> 00:02:52,680
This is going to be a farm field.

46
00:02:52,710 --> 00:03:00,330
So from start, this is going to be a character field and the label here is going to be nothing, but

47
00:03:00,840 --> 00:03:07,740
it's going to say something like password, and then the widget here is going to be nothing, but it's

48
00:03:07,740 --> 00:03:11,130
going to be the widget which is used to enter the password.

49
00:03:11,130 --> 00:03:13,260
So that's going to be password input.

50
00:03:13,260 --> 00:03:16,680
So I would say from start password input.

51
00:03:17,790 --> 00:03:18,180
Okay.

52
00:03:18,180 --> 00:03:23,970
So once this thing is done, let's also set up the password to field as well, which is basically a

53
00:03:23,970 --> 00:03:26,130
field to confirm the password of the user.

54
00:03:26,880 --> 00:03:35,070
So here I would say form start Garfield and let's set the label of this thing to, let's say.

55
00:03:36,750 --> 00:03:43,230
Confirm password or retype password, whatever you would like to call it, I will call it as confirm

56
00:03:43,230 --> 00:03:43,890
password.

57
00:03:43,890 --> 00:03:48,600
And for this as well, let's make use of the widget, which is the password input.

58
00:03:48,870 --> 00:03:53,670
So I'll copy this, give a comma and add this thing up over here.

59
00:03:54,300 --> 00:03:54,690
Okay.

60
00:03:54,690 --> 00:03:56,820
Now our form pretty much looks ready.

61
00:03:56,820 --> 00:04:00,240
And there's also a method which we want to add here.

62
00:04:00,240 --> 00:04:06,690
And that method basically is going to check if the password one matches up with password two and if

63
00:04:06,690 --> 00:04:08,100
those two passwords match.

64
00:04:08,100 --> 00:04:12,440
In that particular case, we actually want to display a particular message.

65
00:04:12,450 --> 00:04:14,030
So we are going to add that.

66
00:04:14,040 --> 00:04:22,260
So here I'll create a method and this method is going to be check, underscore password and this is

67
00:04:22,260 --> 00:04:24,150
going to accept a parameter self.

68
00:04:24,150 --> 00:04:28,920
And here we are basically going to check if the two passwords like the password one and password to

69
00:04:28,920 --> 00:04:29,590
match.

70
00:04:29,610 --> 00:04:33,900
So here we check if that is if.

71
00:04:34,900 --> 00:04:40,450
Self dot clean data and clean data basically gives us access to the password.

72
00:04:40,450 --> 00:04:42,760
So we save self clean data.

73
00:04:42,770 --> 00:04:46,060
And from the data we are looking for the field, which is password.

74
00:04:46,660 --> 00:04:57,610
If this is not equal to self dot clean data, which is going to be password two if that happens.

75
00:04:57,610 --> 00:05:02,950
In that particular case, we want to raise a validation error which says that the password does not

76
00:05:02,950 --> 00:05:03,550
match.

77
00:05:03,670 --> 00:05:11,620
So I would raise a form stored validation error and this error should say something like passwords do

78
00:05:11,620 --> 00:05:13,300
not match.

79
00:05:13,300 --> 00:05:19,310
And here after this we finally make this method return the password to.

80
00:05:19,330 --> 00:05:26,110
So here I would say self dot cleaned data and just pass in password to.

81
00:05:26,110 --> 00:05:26,410
Okay.

82
00:05:26,410 --> 00:05:28,270
So now this form is pretty much ready.

83
00:05:28,270 --> 00:05:30,250
So let's go through this form one more time.

84
00:05:30,250 --> 00:05:35,800
So here we are basically making use of the user model and as we want a form to store the data inside

85
00:05:35,800 --> 00:05:38,230
the user model, we are using a model form.

86
00:05:38,230 --> 00:05:44,170
And whenever you create a model form, we pass in a metadata information here, which means that this

87
00:05:44,170 --> 00:05:47,380
particular form is targeting the user model.

88
00:05:47,380 --> 00:05:52,720
And these are the fields which it's actually looking for, which is the username, email and first name.

89
00:05:52,900 --> 00:05:59,320
And then here to basically modify the default behavior of the password and password two fields.

90
00:05:59,320 --> 00:06:04,300
We have actually changed the labels of these particular fields here to password and confirm password.

91
00:06:04,300 --> 00:06:09,550
And also we have set the widgets to be the password fields, which basically hide the text which we

92
00:06:09,550 --> 00:06:10,420
are typing in.

93
00:06:10,570 --> 00:06:15,940
And also we have created a check password method which actually checks if the two passwords are correct,

94
00:06:15,940 --> 00:06:19,740
and if the two passwords do not match, then there is a validation error.

95
00:06:19,750 --> 00:06:25,630
And this is specifically important because whenever we are registering users, the user must type in

96
00:06:25,630 --> 00:06:31,120
the accurate password because if they don't do so, they won't be able to log in into our website.

97
00:06:31,360 --> 00:06:31,810
Okay.

98
00:06:31,810 --> 00:06:36,040
So once this is set up, our next job is to render up this particular form.

99
00:06:37,100 --> 00:06:38,310
Using a view.

100
00:06:38,330 --> 00:06:42,500
So in the next lecture, let's create a view for registering the user.

101
00:06:42,650 --> 00:06:46,550
So thank you very much for watching and I'll see you guys in the next one.

102
00:06:46,580 --> 00:06:47,180
Thank you.

