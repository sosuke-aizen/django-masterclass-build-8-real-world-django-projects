1
00:00:00,120 --> 00:00:05,700
So in this particular lecture, let's go ahead and let's learn how to create the like functionality

2
00:00:05,700 --> 00:00:11,730
or design the like functionality in a way that whenever we click on this particular icon, the post

3
00:00:11,730 --> 00:00:15,000
should not only be like, but this icon should turn to red.

4
00:00:15,000 --> 00:00:19,490
And whenever we like that particular thing, it should actually turn to white.

5
00:00:19,500 --> 00:00:24,480
So we already have this white icon right here and now we need to find a red icon.

6
00:00:24,480 --> 00:00:29,340
So in order to do that, I would go ahead, go to Icon Finder and I would search for heart.

7
00:00:29,550 --> 00:00:34,560
And here, as you can see, we have a free button, which is the heart button right up over here.

8
00:00:34,590 --> 00:00:39,300
You could use any one of these buttons here or I can see a but I'm going to choose this one.

9
00:00:39,480 --> 00:00:43,800
So let's select a smaller size and let's download it.

10
00:00:45,410 --> 00:00:46,670
And PNG format.

11
00:00:46,670 --> 00:00:52,700
And once we have it downloaded now our job is to just go ahead and paste it up in the static folder,

12
00:00:52,700 --> 00:00:53,410
which we have.

13
00:00:53,420 --> 00:00:55,550
So let's go ahead and let's do that.

14
00:00:55,730 --> 00:01:00,110
So let's go inside the app, which is users.

15
00:01:00,740 --> 00:01:03,170
Here, if you take a look at the static folder.

16
00:01:04,530 --> 00:01:06,780
We have the images folder right up over here.

17
00:01:06,780 --> 00:01:11,920
We have this like dot PNG and this is nothing but this heart shaped icon.

18
00:01:11,940 --> 00:01:16,190
So let's actually replace this with something called as white like.

19
00:01:16,200 --> 00:01:23,180
So I'll rename this thing to white like and I'll name this icon as red like.

20
00:01:23,190 --> 00:01:26,790
So I'll take this up over here and let's rename it to red.

21
00:01:26,790 --> 00:01:28,940
Like so red like.

22
00:01:28,950 --> 00:01:34,500
So whenever a post is liked we are going to be using the red like and whenever the post is unliked we

23
00:01:34,500 --> 00:01:36,900
are going to use the white like as simple as that.

24
00:01:37,260 --> 00:01:39,240
Okay, so now let's start working on that.

25
00:01:39,570 --> 00:01:44,310
So right now, if you take a look at the code here which actually displays the like, so if you actually

26
00:01:44,310 --> 00:01:45,900
go to feed dot HTML.

27
00:01:47,050 --> 00:01:50,180
As you can see, this right here is the like button, which we have.

28
00:01:50,200 --> 00:01:52,750
So currently it's showing us like dot PNG.

29
00:01:52,810 --> 00:01:59,080
So you want to go ahead and first of all, check if a particular post is liked by the user and if the

30
00:01:59,080 --> 00:02:00,940
post is liked by the user only.

31
00:02:00,940 --> 00:02:04,070
In that particular case we want to show the red like.

32
00:02:04,090 --> 00:02:08,320
And on the other hand, when the person has not actually liked it, we want to display the white light.

33
00:02:08,350 --> 00:02:11,960
So here, first of all, let's check if the user has actually liked the post.

34
00:02:11,980 --> 00:02:14,410
So for that, I'll add a statement.

35
00:02:14,500 --> 00:02:21,100
So here I would say if and we want to check if the logged in user has actually liked that particular

36
00:02:21,100 --> 00:02:21,730
post.

37
00:02:21,730 --> 00:02:23,290
So we say if.

38
00:02:24,590 --> 00:02:26,600
Logged underscore user.

39
00:02:26,630 --> 00:02:37,190
So if this logged in user is actually present in post, dot liked underscore by dot all, which means

40
00:02:37,190 --> 00:02:44,000
that if we go through the list of the post of liked by users in that particular case, if we find that

41
00:02:44,000 --> 00:02:46,280
the logged in user is actually present there.

42
00:02:46,280 --> 00:02:51,220
In that particular case, we want to display the red icon, which is the red like.

43
00:02:51,230 --> 00:02:56,180
So here I'll simply copy this and I'll say then display this.

44
00:02:56,180 --> 00:03:02,750
And here we want to say that we want to display the red like or else if that's not the case, in that

45
00:03:02,750 --> 00:03:04,730
case, I want to display the white like.

46
00:03:04,730 --> 00:03:06,830
So here I'll have an L statement.

47
00:03:07,340 --> 00:03:09,020
So I would say else.

48
00:03:09,020 --> 00:03:12,700
And then finally I will end my F block right after this image.

49
00:03:12,710 --> 00:03:20,600
So here I would say and if okay, so now let's go back here, let's refresh.

50
00:03:20,600 --> 00:03:26,390
And right now you'll be able to see that if I hit refresh still, that particular white light will be

51
00:03:26,390 --> 00:03:27,710
displayed up over here.

52
00:03:27,710 --> 00:03:30,890
And that's not because we have white like over here.

53
00:03:30,890 --> 00:03:32,150
We still have like here.

54
00:03:32,150 --> 00:03:38,060
So let's change that to white, like and now if I go back here at Refresh, nothing happens.

55
00:03:38,060 --> 00:03:43,220
And this is because whenever you make a change to the images and whenever you alter the name of the

56
00:03:43,220 --> 00:03:47,360
images here, you have to go back to the terminal, stop the server.

57
00:03:48,400 --> 00:03:50,470
And then restart the server one more time.

58
00:03:51,340 --> 00:03:54,040
And now if you do that, if you hit refresh.

59
00:03:55,550 --> 00:04:00,770
Right now, you'll be able to see that still, there is no change here, so let's try liking a particular

60
00:04:00,770 --> 00:04:01,340
post.

61
00:04:01,340 --> 00:04:05,330
So if I click on there, as you can see, nothing is happening.

62
00:04:05,330 --> 00:04:08,810
And that's because there's one more thing which needs to be done here.

63
00:04:08,810 --> 00:04:13,460
So whenever you make changes to the static file directory, you have to execute a command which is collect

64
00:04:13,460 --> 00:04:13,910
static.

65
00:04:13,910 --> 00:04:18,019
So you have to say python manage dot by.

66
00:04:19,570 --> 00:04:21,910
Collect static.

67
00:04:24,220 --> 00:04:24,640
Typing.

68
00:04:24,640 --> 00:04:25,450
Yes.

69
00:04:26,660 --> 00:04:27,620
And then.

70
00:04:28,500 --> 00:04:30,520
Let's run the server one more time.

71
00:04:30,540 --> 00:04:32,430
Let's go back to the browser.

72
00:04:33,490 --> 00:04:36,290
This isn't working, so let's fix that.

73
00:04:36,310 --> 00:04:40,630
So for now, let's get rid of all the code from here, and let's see if that image is removed.

74
00:04:40,630 --> 00:04:42,550
So let's get this from here.

75
00:04:43,030 --> 00:04:46,510
If I cut this, if I refresh, that image is gone from here.

76
00:04:46,660 --> 00:04:49,540
And if I paste this back up again, let's see what happens.

77
00:04:49,540 --> 00:04:52,120
So when I hit refresh, you get this button.

78
00:04:52,390 --> 00:04:55,360
If I click on this, nothing's happening here.

79
00:04:56,630 --> 00:04:58,760
So let's see why that is the case.

80
00:04:58,790 --> 00:05:02,600
So let me go to the app, which is users.

81
00:05:03,050 --> 00:05:10,670
And if I go to static users images, we have the red like and white like, which is all fine and good.

82
00:05:11,030 --> 00:05:13,660
So let's see and test this one more time.

83
00:05:13,670 --> 00:05:16,010
So right now I log out.

84
00:05:16,790 --> 00:05:19,380
I'll actually log in as the super user.

85
00:05:19,400 --> 00:05:21,110
So let me log in first.

86
00:05:21,140 --> 00:05:23,390
Now I'm logged in as the super user.

87
00:05:23,870 --> 00:05:29,540
Right now, if I take a look at the post, which is this post right here, which is nature image, let's

88
00:05:29,540 --> 00:05:30,620
go to post here.

89
00:05:30,650 --> 00:05:32,990
So here is the nature image post right now.

90
00:05:32,990 --> 00:05:34,730
Nobody has liked that post.

91
00:05:35,060 --> 00:05:39,500
If I had to refresh and let's click on this particular button right here.

92
00:05:40,910 --> 00:05:48,350
As you can see, the user has now liked this one, but still the icon image here is not changing to

93
00:05:48,350 --> 00:05:49,070
something else.

94
00:05:49,070 --> 00:05:54,200
And this thing actually is happening because right now we have this logged user here, but we have not

95
00:05:54,200 --> 00:05:57,080
actually passed in logged users context from The View.

96
00:05:57,320 --> 00:06:03,620
So let's go to the views dot p v file and up over here we have to actually get the logged in user and

97
00:06:03,620 --> 00:06:05,250
then pass it as logged user.

98
00:06:05,270 --> 00:06:10,010
So here the view which powers up the feed page is this particular page right up over here.

99
00:06:10,010 --> 00:06:12,710
So first of all, I'll get access to the user.

100
00:06:12,710 --> 00:06:20,600
So here I will say logged underscore user equals request, dot user.

101
00:06:21,670 --> 00:06:22,750
Let's save this.

102
00:06:22,750 --> 00:06:25,630
And now let's pass this thing as context here.

103
00:06:25,660 --> 00:06:32,260
So I would say logged user as logged user.

104
00:06:32,410 --> 00:06:32,670
Okay.

105
00:06:32,680 --> 00:06:36,670
So once this thing is done, I could now head back here, hit refresh.

106
00:06:36,670 --> 00:06:42,210
And this time, as you can see, as this post is liked, we have the red icon right up over here.

107
00:06:42,220 --> 00:06:45,510
If I click on this one more time, hit refresh.

108
00:06:45,520 --> 00:06:47,230
As you can see, this is now gone.

109
00:06:47,740 --> 00:06:50,080
If I click it one more time, hit refresh.

110
00:06:50,200 --> 00:06:52,660
The red icon reappears right up over here.

111
00:06:52,750 --> 00:06:55,630
So now, as you can see, the like functionality is working.

112
00:06:55,630 --> 00:07:01,120
But now the problem is every time you actually make a change to this one, you have to refresh this

113
00:07:01,120 --> 00:07:01,510
page.

114
00:07:01,510 --> 00:07:06,330
And unless and until you refresh this page, you are not actually able to get the change to icon here.

115
00:07:06,340 --> 00:07:09,970
So let's fix this particular refresh issue in the next lecture.

