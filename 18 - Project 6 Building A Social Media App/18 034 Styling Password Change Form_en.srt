1
00:00:00,090 --> 00:00:05,040
So in this particular lecture, let's go ahead and let's style up this particular password change from

2
00:00:05,040 --> 00:00:06,310
which we have up over here.

3
00:00:06,330 --> 00:00:11,280
And in order to style up this particular form, we will be using the same code which we have actually

4
00:00:11,280 --> 00:00:13,830
used in this particular edit dot HTML.

5
00:00:13,830 --> 00:00:16,890
So let's go ahead, let's close all the files.

6
00:00:17,130 --> 00:00:23,520
Uh, other than this particular file and let's also open up the password change form, which we have.

7
00:00:23,520 --> 00:00:26,900
So the password change form is nothing but this form right here.

8
00:00:26,910 --> 00:00:32,430
So here, as you can see, this is what the form currently looks like and we simply have a form rendered

9
00:00:32,430 --> 00:00:33,120
as P.

10
00:00:33,120 --> 00:00:38,190
So in order to change that real quick, the very first thing that needs to be done is that you need

11
00:00:38,190 --> 00:00:43,210
to access the label as well as the input field as we have done up over here.

12
00:00:43,230 --> 00:00:46,530
So that means I now simply have to copy one of these divs here.

13
00:00:46,530 --> 00:00:53,580
So without having to write any new kind of code, you simply go ahead and you copy this content or this

14
00:00:53,580 --> 00:01:00,510
particular, you could say a format which you have, and you could even copy the entire thing from right

15
00:01:00,510 --> 00:01:08,430
from the H to tag copy the whole entire thing until the div which you have here, go back here and right

16
00:01:08,430 --> 00:01:15,420
now take this particular thing and then before pasting it right up over here, what you could do is

17
00:01:15,420 --> 00:01:17,970
that you could have form dot SB at the bottom.

18
00:01:17,970 --> 00:01:23,070
So you could take this, put it up over here right before the end.

19
00:01:23,070 --> 00:01:25,950
BLOCK And I'll tell you the reason why you need to do this.

20
00:01:26,040 --> 00:01:31,140
We need to do this because we currently don't know the names of the fields for that particular form.

21
00:01:31,140 --> 00:01:36,960
And we are going to take the name of the form fields from this form right here In the meanwhile, let's

22
00:01:36,960 --> 00:01:44,850
copy this, paste it up over here so I'll get rid of the form from here and paste it up over here like

23
00:01:44,850 --> 00:01:45,270
that.

24
00:01:45,630 --> 00:01:48,150
Now, if I go back here, hit refresh.

25
00:01:48,150 --> 00:01:51,030
As you can see, this is what the older form looks like.

26
00:01:51,030 --> 00:01:55,740
Now, I could simply go ahead, inspect this, get the name of this particular field.

27
00:01:55,740 --> 00:01:59,160
So the name of this field is old underscore password.

28
00:01:59,160 --> 00:02:02,540
So as you can see, this is what the name of that particular field is.

29
00:02:02,550 --> 00:02:12,840
So I could go ahead and go inside my first label, change it from first name to old password and change

30
00:02:12,840 --> 00:02:17,280
this thing to let's first change this thing from user form to form.

31
00:02:17,400 --> 00:02:21,330
And here let's type an old underscore password.

32
00:02:21,960 --> 00:02:26,790
If I do that now, the first field which we are going to have up over here is going to be for the old

33
00:02:26,790 --> 00:02:27,480
password.

34
00:02:27,570 --> 00:02:31,680
Now, in a similar fashion, let's also get the names of the other fields over here.

35
00:02:31,680 --> 00:02:33,030
So let's inspect this.

36
00:02:33,030 --> 00:02:36,420
So this thing is actually called as a new password one.

37
00:02:36,420 --> 00:02:39,390
So remember that that's the name of that particular field.

38
00:02:39,570 --> 00:02:41,040
So let's go ahead and add that.

39
00:02:41,040 --> 00:02:45,180
So this is going to be label for new password.

40
00:02:45,300 --> 00:02:49,920
And then the name of this particular field is new password one.

41
00:02:49,920 --> 00:02:53,340
So let's say form dot new password one.

42
00:02:54,400 --> 00:02:56,110
So that's going to be new.

43
00:02:56,920 --> 00:02:58,870
And the scope password.

44
00:03:00,040 --> 00:03:00,700
One.

45
00:03:01,120 --> 00:03:04,300
Okay, so now let's analyze the next field.

46
00:03:04,330 --> 00:03:07,450
So the next field is the password confirmation field.

47
00:03:07,450 --> 00:03:10,750
And I guess this is going to be called as new password, too.

48
00:03:10,780 --> 00:03:11,110
Yeah.

49
00:03:11,110 --> 00:03:12,130
So That's correct.

50
00:03:12,130 --> 00:03:12,910
So.

51
00:03:14,220 --> 00:03:23,430
Here, I would see confirm new password as the name of this particular label.

52
00:03:23,430 --> 00:03:31,430
And let's change this thing to form dot new password two.

53
00:03:31,620 --> 00:03:34,710
Once this thing is done, let's go ahead, hit refresh.

54
00:03:34,710 --> 00:03:37,920
So we have old password, new password, confirm password.

55
00:03:38,100 --> 00:03:41,460
And let's get rid of the extra fields which we have like the photo.

56
00:03:42,460 --> 00:03:45,760
So I'll simply go ahead, delete this field from here.

57
00:03:47,490 --> 00:03:49,040
And we are now good to go.

58
00:03:49,050 --> 00:03:52,170
So if I had refresh, this is what the form looks like.

59
00:03:52,320 --> 00:03:54,570
And now we could finally get rid of this form.

60
00:03:54,570 --> 00:03:59,040
But you'll also be able to see that there are certain warnings which will be showing up over here if

61
00:03:59,040 --> 00:04:00,960
you type an incorrect passwords here.

62
00:04:00,960 --> 00:04:03,710
And we are going to learn how to get those warnings later.

63
00:04:03,720 --> 00:04:06,030
But for now, let's actually get rid of this form.

64
00:04:06,330 --> 00:04:11,280
So let's get rid of this form from here by deleting form dot SB.

65
00:04:11,310 --> 00:04:13,050
We need to have the end block here.

66
00:04:13,620 --> 00:04:17,970
And now, as you can see, the password form is pretty much ready.

67
00:04:18,120 --> 00:04:22,740
Let's change the heading to change password.

68
00:04:23,250 --> 00:04:25,440
Let's change the emoji as well.

69
00:04:25,470 --> 00:04:28,710
Let's say we use something like a key.

70
00:04:28,710 --> 00:04:29,830
And there you have it.

71
00:04:29,850 --> 00:04:33,380
We now have the password change form fully functional and complete.

72
00:04:33,390 --> 00:04:40,470
But now the problem is that let's say if you type in an incorrect old password and an incorrect new

73
00:04:40,470 --> 00:04:45,180
password and confirm password here, and if you try to submit, this will actually simply load up the

74
00:04:45,180 --> 00:04:48,360
page and you would have no idea what actually happened.

75
00:04:48,390 --> 00:04:53,880
That means this form should be capable to display the errors which are actually present in that particular

76
00:04:53,880 --> 00:04:54,440
form.

77
00:04:54,450 --> 00:04:59,970
And in order to display those particular errors, what you could do is that the form object which you

78
00:04:59,970 --> 00:05:04,440
have passed here, this actually contains something which is called less errors, and you could actually

79
00:05:04,440 --> 00:05:09,090
loop through these errors which you have and display those particular errors right at the top of the

80
00:05:09,090 --> 00:05:09,610
form.

81
00:05:09,630 --> 00:05:15,960
So here, right where our form is, let's go at the top of this form, which is right after this div.

82
00:05:15,960 --> 00:05:18,930
And here I'll create a container for errors.

83
00:05:18,930 --> 00:05:20,370
So I would create a div.

84
00:05:20,400 --> 00:05:29,010
Let's name this thing as a row container, or let's rather name this class as a row container.

85
00:05:29,190 --> 00:05:32,100
And I'm actually going to make the text of this thing to read.

86
00:05:32,100 --> 00:05:38,130
So text read with the sheet of 500 and I would add a padding of ten on the x axis.

87
00:05:38,310 --> 00:05:44,130
So once we have this div in order to actually get the errors which actually arise here, first you need

88
00:05:44,130 --> 00:05:45,990
to check if the form has any errors.

89
00:05:45,990 --> 00:05:50,370
So we say if the form has any errors.

90
00:05:50,370 --> 00:05:55,620
So if form errors in that particular case, do something and then end the if statement here.

91
00:05:55,620 --> 00:05:57,330
So I would add end of here.

92
00:05:58,380 --> 00:06:02,790
So that means if the form has the errors, we have to loop through all the errors in the form and then

93
00:06:02,790 --> 00:06:03,780
display those errors.

94
00:06:03,780 --> 00:06:11,280
So in order to loop through the errors which we have in the form, you need to say for fill in form,

95
00:06:11,280 --> 00:06:17,700
which means we loop through all the fields which we have up over here and we get each and every error

96
00:06:17,700 --> 00:06:19,080
from that particular field.

97
00:06:19,080 --> 00:06:26,840
So we also need to add a N for here and then let's actually loop through each error for each field.

98
00:06:26,850 --> 00:06:30,360
So here I would say field dot errors.

99
00:06:30,660 --> 00:06:33,750
Okay, Once this thing is done, let's see if that's functional.

100
00:06:33,990 --> 00:06:40,140
So right now, if I hit refresh, as you can see, it says you are old password was incorrectly entered.

101
00:06:40,170 --> 00:06:41,340
Please enter it again.

102
00:06:41,340 --> 00:06:44,220
And also it says that your passwords feel do not match.

103
00:06:44,220 --> 00:06:49,680
And now let's say if the old password is incorrect, but let's say the new password and old password

104
00:06:49,680 --> 00:06:50,880
are same.

105
00:06:50,880 --> 00:06:56,730
So now in this case, if I had submit, as you can see, it will actually show all the errors which

106
00:06:56,730 --> 00:06:57,870
we have in a password.

107
00:06:57,870 --> 00:07:02,700
So this time, as you can see, it says that the password is too common because I have simply typed

108
00:07:02,700 --> 00:07:04,620
in numbers from 1 to 5.

109
00:07:04,650 --> 00:07:07,410
It also says that the password is entirely numeric.

110
00:07:07,410 --> 00:07:12,540
It also says the password is too short, but this time we do not get the password, do not match error.

111
00:07:12,570 --> 00:07:17,040
That means now the errors and warnings are displayed along with this form as well.

112
00:07:17,190 --> 00:07:21,120
That means this particular password change form is now fully functional.

113
00:07:21,120 --> 00:07:26,250
But still there's one issue which we have in the previous form as well, where we have styled it up,

114
00:07:26,250 --> 00:07:31,980
and that issue is this blue coloured border which we have for the inner input field which we have,

115
00:07:31,980 --> 00:07:35,010
and this particular input field is customised by us.

116
00:07:35,010 --> 00:07:40,530
So in the next lecture let's go ahead and let's learn how we could get rid of this particular blue border

117
00:07:40,530 --> 00:07:43,460
on this input field from the text field which we have.

118
00:07:43,470 --> 00:07:46,920
So thank you very much for watching and I'll see you guys in the next one.

