1
00:00:00,060 --> 00:00:00,830
In this lecture.

2
00:00:00,840 --> 00:00:06,480
Let's go ahead and let's start working on creating this like functionality so that whenever we like

3
00:00:06,480 --> 00:00:10,650
this particular or click this particular icon right here, it should turn to red.

4
00:00:10,650 --> 00:00:15,030
And we should also be able to know what particular users actually like this post.

5
00:00:15,030 --> 00:00:20,400
So even before starting to create this like functionality, let me explain how this is going to work.

6
00:00:20,580 --> 00:00:27,240
So every social media post which you have, so if you actually click on that particular post, you will

7
00:00:27,240 --> 00:00:31,140
be able to see the list of people who actually liked that particular post.

8
00:00:31,170 --> 00:00:38,100
Now, the way in which this should work architecturally is that you take the post and you basically

9
00:00:38,100 --> 00:00:41,040
save all the users who like that particular post.

10
00:00:41,040 --> 00:00:48,210
That means now this post model should also incorporate the people who have actually liked the post.

11
00:00:48,210 --> 00:00:53,040
That means it should actually maintain a list of people who actually have liked that particular post.

12
00:00:53,040 --> 00:00:57,960
So in order to do that, what we are going to do is that we are going to make modifications to the post

13
00:00:57,960 --> 00:00:58,890
model, which we have.

14
00:00:58,890 --> 00:01:02,030
So the post model is actually present in the Posts app.

15
00:01:02,040 --> 00:01:08,310
So let's go right in there and the posts and if you take a look at the models here, as you can see,

16
00:01:08,310 --> 00:01:09,960
this is what the model looks like.

17
00:01:09,960 --> 00:01:15,480
So right now we don't have any provision for saving the users who have liked this post, so let's create

18
00:01:15,480 --> 00:01:16,160
that right away.

19
00:01:16,170 --> 00:01:22,050
So I would say liked by and this field is going to hold all the people who actually have liked that

20
00:01:22,050 --> 00:01:23,010
particular post.

21
00:01:23,220 --> 00:01:29,040
Now, the problem is we don't know what kind of type of this field should be.

22
00:01:29,130 --> 00:01:34,260
So, for example, if we should make this a character field or whether we should make it some other

23
00:01:34,260 --> 00:01:40,350
field, but now, as we have to save the entire user here, we are going to change it to many to many

24
00:01:40,350 --> 00:01:40,950
fields.

25
00:01:40,950 --> 00:01:45,960
So just as we have this foreign key here, we cannot associate another foreign key with this post.

26
00:01:45,960 --> 00:01:52,080
So let's actually go ahead and as we want many users to be able to like many posts.

27
00:01:52,080 --> 00:01:58,620
So for example, post could be liked by many users or many users could like a single post or a single

28
00:01:58,620 --> 00:02:00,150
user could like many posts.

29
00:02:00,150 --> 00:02:06,090
So that's why many too many relationships would be the best one to establish between the light bar and

30
00:02:06,090 --> 00:02:06,780
the post.

31
00:02:06,780 --> 00:02:14,910
So here I would say models dot many to many field and the field which we want to associate with this

32
00:02:14,910 --> 00:02:16,440
is actually the user model.

33
00:02:16,440 --> 00:02:19,830
So the user model which we have is the inbuilt Django's user model.

34
00:02:19,830 --> 00:02:31,230
So we will say settings dot that's going to be auth and the school user underscore model.

35
00:02:32,050 --> 00:02:35,920
And then I would see the related name is going to be nothing.

36
00:02:35,920 --> 00:02:42,850
But let's say we want to call this particular relationship, as posts underscore, liked.

37
00:02:43,720 --> 00:02:49,630
And then also we could say that the blank of this thing could be true, meaning that if a post is created,

38
00:02:49,630 --> 00:02:53,710
it could be possible that no user has currently like that particular post.

39
00:02:53,740 --> 00:02:57,250
That's why we also have to set the blank of this one to true.

40
00:02:57,790 --> 00:03:03,580
Now, once this thing is done, we have to make migrations because we have made modifications to the

41
00:03:03,580 --> 00:03:03,940
model.

42
00:03:03,940 --> 00:03:05,290
So let's go back here.

43
00:03:05,980 --> 00:03:15,280
Let's actually clear the screen up and I would say Python manage, start by make migrations and then

44
00:03:15,280 --> 00:03:19,630
I would say Python manage, start by migrate.

45
00:03:19,950 --> 00:03:20,210
Okay.

46
00:03:20,230 --> 00:03:22,930
So once this thing is done, we are pretty much good to go.

47
00:03:22,960 --> 00:03:29,500
Now, once this thing is created now our model should actually have the light bi attribute.

48
00:03:29,500 --> 00:03:31,660
So now let's run the server back.

49
00:03:33,070 --> 00:03:37,330
And now let's go to the admin and let's see the kind of field which is being added there.

50
00:03:37,480 --> 00:03:43,660
So here if I go to admin and if I go to posts and if I go to one particular post here, as you can see,

51
00:03:43,660 --> 00:03:47,280
we have this light by field over here, which is one too many field.

52
00:03:47,290 --> 00:03:50,680
And right now it's showing a couple of options here.

53
00:03:50,680 --> 00:03:54,430
And we could add who actually liked that particular post from the admin.

54
00:03:54,430 --> 00:03:59,860
But technically we have to create a functionality that means whenever we have clicked this particular

55
00:03:59,860 --> 00:04:05,170
button right here, the user who is currently logged in should be added to the light bar of that particular

56
00:04:05,170 --> 00:04:05,730
post.

57
00:04:05,740 --> 00:04:08,500
So for example, let's take an example of this post.

58
00:04:08,500 --> 00:04:13,840
So right now I'm logged in as the admin user and this post belongs to some other user like Pat.

59
00:04:13,870 --> 00:04:20,980
Now if I click a like on Pat's post, the like should actually be saved or my name username should be

60
00:04:20,980 --> 00:04:22,870
saved in the like by of this one.

61
00:04:23,110 --> 00:04:25,770
So the question is how to create that functionality.

62
00:04:25,780 --> 00:04:33,430
So in order to create that functionality we have to go inside the views dot py file of the posts app

63
00:04:33,430 --> 00:04:34,480
right up over here.

64
00:04:34,660 --> 00:04:39,580
And then I have to create another view which is like and this like view should be triggered whenever

65
00:04:39,580 --> 00:04:42,340
I click this particular like I can right here.

66
00:04:42,340 --> 00:04:43,630
So I would say def.

67
00:04:44,640 --> 00:04:46,380
Like underscore post.

68
00:04:46,950 --> 00:04:52,920
I'll name this thing as it is because this looks intuitive and this is going to accept a request as

69
00:04:52,920 --> 00:04:53,550
usual.

70
00:04:53,640 --> 00:04:59,070
And then the next thing which we do is that whenever a user likes that particular post, we want to

71
00:04:59,070 --> 00:05:00,780
see which post the user has liked.

72
00:05:00,780 --> 00:05:04,860
And we could identify the post being liked by making use of the post ID.

73
00:05:04,890 --> 00:05:08,880
Now the question is from where exactly are you going to get the post ID?

74
00:05:09,540 --> 00:05:13,600
So we are going to get the post ID when a particular user clicks this.

75
00:05:13,620 --> 00:05:18,780
So we want to submit a post request whenever a user is going to click this particular button.

76
00:05:19,410 --> 00:05:24,830
So here I would say post underscore ID is going to be equal to from the request dot.

77
00:05:24,840 --> 00:05:28,410
So as we say, request dot get dot something here.

78
00:05:28,410 --> 00:05:33,570
I will see request dot post dot get and get the post underscore ID.

79
00:05:33,600 --> 00:05:37,900
Now, once we have gotten the post ID from this, we get the actual post.

80
00:05:37,920 --> 00:05:45,090
So in order to get the actual post, I would say post equals get underscore object, which means get

81
00:05:45,090 --> 00:05:49,500
that specific object or return a for or for error.

82
00:05:50,040 --> 00:05:52,650
And then I want to get a post.

83
00:05:52,860 --> 00:05:59,010
So I will mention post here and I want to get a post where the ID is going to be equal to the post ID,

84
00:05:59,010 --> 00:06:00,540
which we have just extracted.

85
00:06:00,870 --> 00:06:02,910
So then we get the post as well.

86
00:06:03,360 --> 00:06:09,270
Now then the next thing we need to do is we need to check if the post, which we are currently liking,

87
00:06:09,270 --> 00:06:12,000
if that post is already being liked by us.

88
00:06:12,750 --> 00:06:18,120
So if the post is already being liked by us and that particular case, we have to go ahead and do the

89
00:06:18,120 --> 00:06:19,160
exact opposite.

90
00:06:19,170 --> 00:06:23,970
That is, we have to remove ourselves from the liked by field of that particular post.

91
00:06:24,120 --> 00:06:30,870
And if the post was not previously liked, then we have to go ahead and add ourselves into the like

92
00:06:30,870 --> 00:06:31,960
bar of that post.

93
00:06:31,980 --> 00:06:33,270
So here we see.

94
00:06:33,300 --> 00:06:36,500
Let's first check if the post is already being liked.

95
00:06:36,510 --> 00:06:43,910
So we say if post dot liked by and this post dot like by is nothing, but it's actually a list of objects.

96
00:06:43,920 --> 00:06:46,500
As you can see, it's actually a list of objects.

97
00:06:46,500 --> 00:06:52,980
So out of these objects we have to filter out and check if the ID of the current user is equal to that

98
00:06:52,980 --> 00:06:53,370
ID.

99
00:06:53,370 --> 00:07:00,480
So we say filter and we want to find out if it contains the ID of the currently logged in user.

100
00:07:00,480 --> 00:07:08,520
So we check if ID is equal to request dot user dot ID, which means that if this contains an idea of

101
00:07:08,520 --> 00:07:12,360
the user who's currently logged in, that means the user has already liked the post.

102
00:07:12,510 --> 00:07:18,930
So if this exists in that particular case, we have to remove ourselves from here.

103
00:07:18,930 --> 00:07:22,380
So what I'll write in pass here and we are going to add that code later.

104
00:07:22,380 --> 00:07:23,040
Else.

105
00:07:23,220 --> 00:07:28,980
That means if the post is not already liked, then we basically take the currently logged in user and

106
00:07:28,980 --> 00:07:30,720
add it to the liked by over here.

107
00:07:30,720 --> 00:07:34,980
So I would say post dot lite by.

108
00:07:36,440 --> 00:07:41,150
Dot add and we want to add the currently logged in user and in order to get the currently logged in

109
00:07:41,150 --> 00:07:43,580
user, I would say request dot user.

110
00:07:44,210 --> 00:07:45,020
That's it.

111
00:07:45,590 --> 00:07:51,290
Now let's write the logic of what happens when the post has already been liked, so when the post is

112
00:07:51,290 --> 00:07:51,950
already liked.

113
00:07:51,950 --> 00:07:56,720
So typically what happens is that if you have already like the post, you again click this particular

114
00:07:56,720 --> 00:07:58,760
heart icon right here you should.

115
00:07:58,760 --> 00:08:03,580
Unlike the post, that means you have to remove yourself from the light bar of that post.

116
00:08:03,590 --> 00:08:11,180
So here I would say post dot light pie, dot remove and we want to remove the currently logged in user.

117
00:08:11,180 --> 00:08:13,430
So I'll say request dot user.

118
00:08:14,280 --> 00:08:15,350
All right, that's fine.

119
00:08:15,360 --> 00:08:17,820
Now, once this thing is done, the view is created.

120
00:08:17,820 --> 00:08:22,470
But we have an error over here because we have not imported this cat object or four or four.

121
00:08:22,650 --> 00:08:24,090
So let's import this.

122
00:08:24,090 --> 00:08:25,260
So I would say.

123
00:08:26,140 --> 00:08:32,809
From django dot shortcuts import cat object or four.

124
00:08:33,100 --> 00:08:36,100
So once this thing is done, the warning is now removed.

125
00:08:36,520 --> 00:08:39,390
Okay, so now let's get to the next part of this one.

126
00:08:39,490 --> 00:08:39,700
Okay.

127
00:08:39,700 --> 00:08:44,380
So once this view is created, our next job is to go ahead and associate this particular view with a

128
00:08:44,410 --> 00:08:45,430
URL pattern.

129
00:08:45,460 --> 00:08:48,910
So let's go to the URL, start p file off the post snap.

130
00:08:48,910 --> 00:08:55,480
And in here, let's also associate a URL for like so this is going to be like and I'll associate it

131
00:08:55,480 --> 00:08:56,170
with view.

132
00:08:56,170 --> 00:09:06,010
Start like post and I'll name this thing as let's see, like, okay, so now what we have to do is that

133
00:09:06,010 --> 00:09:12,880
in some of the other way we have to trigger this particular like you URL, whenever we click this particular

134
00:09:12,880 --> 00:09:15,220
icon right here, which is the like icon.

135
00:09:15,580 --> 00:09:18,490
So the question now is how exactly would you do that?

136
00:09:18,670 --> 00:09:24,790
So in order to do that, what we need to do is we need to actually make use of some JavaScript and making

137
00:09:24,790 --> 00:09:25,720
use of JavaScript.

138
00:09:25,720 --> 00:09:29,430
We have to basically go ahead and trigger that particular URL.

139
00:09:29,440 --> 00:09:32,050
So we are going to learn how to do that in the next one.

