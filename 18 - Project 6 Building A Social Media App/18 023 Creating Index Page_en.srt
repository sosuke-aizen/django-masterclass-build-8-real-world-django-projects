1
00:00:00,240 --> 00:00:06,330
So in this particular lecture, let's go ahead and let's create a page which displays all the posts

2
00:00:06,330 --> 00:00:12,660
which are posted by the current user or basically posted by the user who is currently logged in.

3
00:00:12,750 --> 00:00:18,000
So in order to create such kind of a page, that page obviously needs to be powered up by a view.

4
00:00:18,030 --> 00:00:20,970
So first of all, we'll start working on creating a view.

5
00:00:21,150 --> 00:00:24,980
So essentially we here want to design an index page for our app.

6
00:00:24,990 --> 00:00:33,120
So for example, if we go to our slash localhost 8000, as you can see, if I go to forward slash users,

7
00:00:33,120 --> 00:00:38,970
which is our users app here, you will be able to see that we currently have a blank page which says

8
00:00:38,970 --> 00:00:39,850
home page.

9
00:00:39,870 --> 00:00:45,530
But let's say we want to create a page here, which is basically going to show up all the posts.

10
00:00:45,540 --> 00:00:51,420
So in order to display or in order to create that kind of a view here, what we do is that we go ahead

11
00:00:51,540 --> 00:00:54,630
and I guess we already have the users app.

12
00:00:54,630 --> 00:00:57,900
So let's go to The View start P VI file of the users app.

13
00:00:58,230 --> 00:01:00,770
And you could also do that and this as well.

14
00:01:00,780 --> 00:01:07,740
But as I want to display the post on the user's home page, I probably would better go inside the user's

15
00:01:07,740 --> 00:01:13,560
app, go to the views dot p v file, which we have up over there, and here is where we will create

16
00:01:13,560 --> 00:01:14,550
that particular view.

17
00:01:14,550 --> 00:01:17,880
And we already have created this particular view called as index.

18
00:01:17,880 --> 00:01:23,730
And what we are going to do is that we are going to power this particular index page by making changes

19
00:01:23,730 --> 00:01:25,980
to this view right here and right now.

20
00:01:25,980 --> 00:01:29,340
We also know that it renders the index dot HTML page.

21
00:01:29,340 --> 00:01:33,560
So if we go to index dot HTML, let's see what do we have there?

22
00:01:33,570 --> 00:01:37,500
So currently, as you can see, it sees this is the home page for users app.

23
00:01:37,500 --> 00:01:43,050
So let's close everything else apart from this, so close others and let's simply open up the view start

24
00:01:43,050 --> 00:01:49,620
p file and here in the index as we want to get all the posts from the currently logged in user.

25
00:01:49,620 --> 00:01:52,830
First of all, we need to get access to the currently logged in user.

26
00:01:53,100 --> 00:01:56,700
Now the question is how would you get access to the currently logged in user?

27
00:01:56,850 --> 00:02:02,370
So whenever you have to access the logged in user, you simply would get that from the request object,

28
00:02:02,370 --> 00:02:03,670
which we already have.

29
00:02:03,690 --> 00:02:11,160
So here I would say request dot and if I see user, this will give me the currently logged in user.

30
00:02:11,160 --> 00:02:15,090
So let's see if this thing into a variable called last current.

31
00:02:16,060 --> 00:02:17,550
Underscore user.

32
00:02:17,560 --> 00:02:19,870
So current user is request, not user.

33
00:02:20,110 --> 00:02:25,330
Now, once we have this user, the reason why we need this particular user is because we don't want

34
00:02:25,330 --> 00:02:30,820
to get all the posts, but we want to get only the post which are associated with the current user.

35
00:02:31,090 --> 00:02:38,560
So here, if I want to get all the post, I could simply do post dot objects dot all and this will give

36
00:02:38,560 --> 00:02:40,690
me access to all the posts which I have.

37
00:02:40,840 --> 00:02:44,230
And we are going to get those posts from the post model.

38
00:02:44,260 --> 00:02:47,720
So here, first of all, you need to import the post model.

39
00:02:47,740 --> 00:02:54,130
So here, in order to import the post model, you would say from post because post is a completely different

40
00:02:54,130 --> 00:02:54,620
tab.

41
00:02:54,640 --> 00:03:01,600
So you need to say from posts dot model and I want to be able to import the post model from here.

42
00:03:01,600 --> 00:03:06,160
So once you have imported that particular model, we are pretty much good to go.

43
00:03:06,280 --> 00:03:09,640
And it says it cannot be resolved for some reason.

44
00:03:09,700 --> 00:03:11,860
And let's see why that's the case.

45
00:03:12,350 --> 00:03:12,640
Okay.

46
00:03:12,640 --> 00:03:15,400
So if we take a look at the name of our app.

47
00:03:16,330 --> 00:03:19,060
I think the name of our app is.

48
00:03:20,610 --> 00:03:21,540
Posts.

49
00:03:21,870 --> 00:03:26,280
So this should technically work, but for some reason it's not working.

50
00:03:26,280 --> 00:03:30,240
So let's try running this and let's see if this causes any issues.

51
00:03:30,240 --> 00:03:31,950
But for now, let's keep it as it is.

52
00:03:31,950 --> 00:03:37,260
So we have this posts app and from this we have the model and we are importing the model, which is

53
00:03:37,260 --> 00:03:38,040
the post model.

54
00:03:38,040 --> 00:03:40,770
So it seems accurate for me at least.

55
00:03:40,800 --> 00:03:42,890
And now let's see if we could use it.

56
00:03:42,900 --> 00:03:46,950
So if I do post dot objects, dot, all this is going to give me all the posts.

57
00:03:46,950 --> 00:03:51,090
But now let's say I want the post for the currently logged in user.

58
00:03:51,090 --> 00:03:56,460
So instead of getting all the post, I would say post dot objects, dot filter.

59
00:03:56,460 --> 00:04:01,780
And I want to filter and I want to say where the user is going to be the current user.

60
00:04:01,800 --> 00:04:05,840
So here I would say current user, once that thing is done.

61
00:04:05,850 --> 00:04:08,820
Now let's see if this particular post into something.

62
00:04:08,820 --> 00:04:16,920
So I would say posts equals this and now let's pass in this posts as context to this particular template

63
00:04:16,920 --> 00:04:17,579
which we have.

64
00:04:17,579 --> 00:04:24,720
So here I would say percent posts as posts.

65
00:04:24,720 --> 00:04:30,240
So once we have passed the post there, let's go ahead, go inside this page and let's loop through

66
00:04:30,240 --> 00:04:31,710
all the posts which we have.

67
00:04:31,950 --> 00:04:35,010
So in order to loop through them, I am going to make use of a for loop.

68
00:04:35,010 --> 00:04:41,820
So I would say for a single post and the posts object list which we have passed and I will end the for

69
00:04:41,820 --> 00:04:42,270
loop here.

70
00:04:42,270 --> 00:04:48,930
So end for and in there I'm actually going to go ahead and simply print out the posts.

71
00:04:48,930 --> 00:04:52,200
So I would say post user post title and the post image.

72
00:04:52,200 --> 00:04:57,000
So let's first simply print out the title of the post and see if that works.

73
00:04:57,000 --> 00:05:03,090
So I would say post dot title, Save this, go back here at refresh.

74
00:05:03,090 --> 00:05:05,520
And as you can see, we have an error.

75
00:05:05,850 --> 00:05:08,280
So let's see what exactly went wrong here.

76
00:05:08,310 --> 00:05:08,720
Okay.

77
00:05:08,730 --> 00:05:11,070
So we were getting this error before.

78
00:05:11,070 --> 00:05:18,000
And the reason for that is because in the models I have to actually import from post dot models instead

79
00:05:18,000 --> 00:05:22,500
of saying model because the name of the file is models and not model.

80
00:05:22,500 --> 00:05:29,250
So once we fix that, go back here head refresh, it sees invalid block tag on line six.

81
00:05:29,250 --> 00:05:29,630
Okay.

82
00:05:29,640 --> 00:05:32,130
So I think we do have another error.

83
00:05:33,110 --> 00:05:36,050
Okay, so there should be no space in between the.

84
00:05:36,050 --> 00:05:38,600
And for once, we fix that.

85
00:05:38,600 --> 00:05:41,390
As you can see now, we are getting the post title on here.

86
00:05:41,570 --> 00:05:46,910
So just as we have gotten the post title, let's also try getting the post user as well.

87
00:05:47,030 --> 00:05:53,200
So here I would type an H2 tag and I would say something like post dot user.

88
00:05:53,210 --> 00:05:58,520
So this is going to give me access to the user and also let's get the post image as well.

89
00:05:58,520 --> 00:06:05,150
So let's add an image tag and the source for this is going to be post dot image dot URL.

90
00:06:05,150 --> 00:06:10,700
So once I do that, hit refresh, as you can see now we actually get the name of the post.

91
00:06:10,700 --> 00:06:16,100
The person who actually posted that particular post and the image which is associated with that as well.

92
00:06:16,160 --> 00:06:21,800
And here we have pretty large images and we need to crop them and set appropriate sizes for them.

93
00:06:21,800 --> 00:06:23,480
And we are going to do that soon.

94
00:06:23,480 --> 00:06:29,480
But right now we know that whenever we now visit the home page, we are now able to get the users posts

95
00:06:29,480 --> 00:06:29,780
here.

96
00:06:29,780 --> 00:06:35,420
This page is able to retrieve the post of the lockdown user, but the post are not formatted really

97
00:06:35,420 --> 00:06:35,900
well.

98
00:06:35,900 --> 00:06:41,000
So in the upcoming lecture, let's go ahead and let's add some styling to our web pages so as to make

99
00:06:41,000 --> 00:06:42,580
them look more appropriate.

100
00:06:42,590 --> 00:06:46,550
So thank you very much for watching and I'll see you guys in the next one.

101
00:06:46,640 --> 00:06:47,300
Thank you.

