1
00:00:00,120 --> 00:00:04,080
Now let's work on actually creating that particular common form here.

2
00:00:04,080 --> 00:00:09,720
So we all know that once we have actually passed in the comment form here as context, you could simply

3
00:00:09,720 --> 00:00:16,830
go ahead and dissect this particular form and use each node field inside this particular feed dot HTML,

4
00:00:16,830 --> 00:00:17,610
which we have.

5
00:00:17,760 --> 00:00:22,020
So even before doing that, I'll actually go ahead and create a couple of divs.

6
00:00:22,020 --> 00:00:25,350
And inside those divs V are going to render out all the fields here.

7
00:00:25,620 --> 00:00:28,920
So right up over here, inside the form, I'll create another div.

8
00:00:28,950 --> 00:00:33,420
So this step, we are going to style it later, but this is actually going to contain a label.

9
00:00:33,480 --> 00:00:36,980
So that label is simply going to say something like add comment.

10
00:00:36,990 --> 00:00:39,300
So I would say add comment.

11
00:00:39,300 --> 00:00:44,280
And once we have this label, let's create another div and this div is going to actually render the

12
00:00:44,280 --> 00:00:46,680
field which is going to accept the form body.

13
00:00:46,680 --> 00:00:51,870
So the context which we have passed here is actually comment, underscore form.

14
00:00:51,870 --> 00:00:54,410
So this right here is the context for the form.

15
00:00:54,420 --> 00:00:57,330
So I could render that particular field by saying.

16
00:00:58,240 --> 00:01:03,700
Comment and the school form dot body and body is actually the name of our field.

17
00:01:04,150 --> 00:01:10,240
So once this thing is done, there's one more important thing which we need to get here, and that is

18
00:01:10,240 --> 00:01:14,680
if you actually remember, you also need a post ID as well.

19
00:01:14,680 --> 00:01:19,270
And we have actually gotten the post ID here from the post request, which we have.

20
00:01:19,360 --> 00:01:24,810
That means this form which we are submitting that form should also be able to have that particular field.

21
00:01:24,820 --> 00:01:30,790
So right now, in order to get access to that field and again add another div and this div is going

22
00:01:30,790 --> 00:01:32,640
to contain an input field.

23
00:01:32,650 --> 00:01:38,500
So I would say input and this input type is going to be hidden because we don't want to show that particular

24
00:01:38,500 --> 00:01:39,520
post ID here.

25
00:01:40,120 --> 00:01:47,770
But the name of this thing is going to be post ID, so I'm going to say name as post underscore ID as

26
00:01:47,770 --> 00:01:51,610
well as the ID is going to be post underscore ID.

27
00:01:52,150 --> 00:01:57,160
And now we also need to set the value of this thing and the value is going to be nothing, but it's

28
00:01:57,160 --> 00:01:58,840
actually going to be the post ID.

29
00:01:59,080 --> 00:02:04,150
Now, we already looping through all the posts here and now in order to actually get the idea of the

30
00:02:04,150 --> 00:02:09,729
post, I could simply say post dot ID and it's actually going to get our fetch that ID for us.

31
00:02:10,120 --> 00:02:15,160
And after that, finally we actually need another div to actually have a submit button.

32
00:02:15,430 --> 00:02:20,680
So here I would add a button and the type of this thing is going to be submit.

33
00:02:20,890 --> 00:02:26,980
So I would say type is submit and this is going to say add comment or let's say simply add.

34
00:02:27,700 --> 00:02:27,990
Okay.

35
00:02:28,000 --> 00:02:31,630
So once we have this form, let's see if this actually even works.

36
00:02:31,630 --> 00:02:33,820
So I'll go back to the browser.

37
00:02:34,530 --> 00:02:35,910
Let's hit refresh.

38
00:02:37,320 --> 00:02:41,490
Okay, So we actually have this huge looking text field here.

39
00:02:41,490 --> 00:02:48,420
I could add a comment like this comment is added using a form.

40
00:02:48,840 --> 00:02:51,420
And now if I click on ADD, let's see what happens.

41
00:02:51,420 --> 00:02:57,330
So as soon as I add that, as you can see, we have this particular comment added up over here to this

42
00:02:57,330 --> 00:03:00,300
post, which is this comment is added using a form.

43
00:03:00,570 --> 00:03:05,550
So if I scroll down, I guess the text over here is not being cleared.

44
00:03:05,580 --> 00:03:07,350
So we are going to fix that later.

45
00:03:07,350 --> 00:03:09,900
But now let's try adding a comment on another post.

46
00:03:09,900 --> 00:03:16,530
So if I try to add that the form field is not getting cleared for some reason, but let's say I post

47
00:03:16,530 --> 00:03:23,720
some comment here like nice post, but if I add this comment, let's see what happens.

48
00:03:23,730 --> 00:03:32,580
So if I add this, if I go up over here, as you can see, we actually get a comment added up over here,

49
00:03:32,580 --> 00:03:34,500
which is a nice post bat.

50
00:03:34,500 --> 00:03:37,110
And remember that this right here is not a comment.

51
00:03:37,110 --> 00:03:40,200
It's actually just a text field showing up over here.

52
00:03:40,200 --> 00:03:42,350
So we are going to fix that later as well.

53
00:03:42,360 --> 00:03:47,700
But for now, we know that the comment functionality is working absolutely fine without any issues.

54
00:03:48,090 --> 00:03:52,800
So in the next lecture, let's actually go ahead and let's start styling up this particular form, which

55
00:03:52,800 --> 00:03:53,460
we have.

56
00:03:53,760 --> 00:03:57,810
So thank you very much for watching and I'll see you guys in the next one.

57
00:03:57,960 --> 00:03:58,680
Thank you.

