1
00:00:00,060 --> 00:00:06,750
So now let's understand templates in a little bit more depth, so why exactly do we need templates?

2
00:00:07,260 --> 00:00:14,100
So in the previous example, we had a database and we have extracted the data from the database and

3
00:00:14,100 --> 00:00:18,670
we have displayed that data onto the Web page using HTP response.

4
00:00:19,080 --> 00:00:25,740
So this is the kind of result which we get in case of using a response to display the data from the

5
00:00:25,740 --> 00:00:26,430
database.

6
00:00:26,430 --> 00:00:30,710
That is, we have these items displayed over here onto the Web page.

7
00:00:31,230 --> 00:00:38,700
But as you could see, the format of this item is not good at all, which means that the items are just

8
00:00:38,700 --> 00:00:45,450
displayed one after the other and there is no proper formatting or there is no proper structure to the

9
00:00:45,450 --> 00:00:48,690
Web page in which these items are actually laid out.

10
00:00:49,110 --> 00:00:53,670
So what if you actually want to display those items in a structured manner like this?

11
00:00:54,090 --> 00:00:57,100
So how exactly can you go ahead and do that?

12
00:00:57,780 --> 00:01:00,660
So here's where templates actually come into picture.

13
00:01:00,780 --> 00:01:04,319
So what exactly are the templates to so let's see.

14
00:01:04,319 --> 00:01:09,340
We have this database right here and from the database we extract the data.

15
00:01:10,770 --> 00:01:16,860
So now in order to structure the page or in order to lay out the elements in a proper format, in a

16
00:01:16,860 --> 00:01:21,330
structured way, we need to have some sort of HTML to it.

17
00:01:21,360 --> 00:01:29,010
So what HTML does is that HTML gives a structure to our Web page so that the elements which are present

18
00:01:29,010 --> 00:01:32,180
on our Web page are actually laid out in a specific manner.

19
00:01:32,670 --> 00:01:39,990
So the HTML is responsible for the layout and the structure of our web page, whereas the database is

20
00:01:39,990 --> 00:01:42,150
responsible for providing the data.

21
00:01:42,660 --> 00:01:44,520
And what templates allow you to do?

22
00:01:44,520 --> 00:01:50,790
Is that template allow you to combine these two things together so that your Web page not only has the

23
00:01:50,790 --> 00:01:54,680
data, but it also has a proper structure or a proper layout.

24
00:01:55,650 --> 00:02:02,730
So this HTML part of your Web page is called as the static part, and that's because it's actually going

25
00:02:02,730 --> 00:02:03,810
to remain the same.

26
00:02:04,410 --> 00:02:10,740
And the database part of your Web page is called as the dynamic part, and that's because the data is

27
00:02:10,740 --> 00:02:12,210
going to constantly change.

28
00:02:13,230 --> 00:02:19,850
So now when you combine the static part and the dynamic part together, you can do that using a template.

29
00:02:19,860 --> 00:02:22,680
And that's exactly why we need to use a template.

30
00:02:22,980 --> 00:02:29,070
So our template allows us to do is that they allow us to combine static part with the dynamic part and

31
00:02:29,070 --> 00:02:31,590
it renders it out as a single Web page.

32
00:02:31,830 --> 00:02:34,540
And this is exactly why we use a template.

33
00:02:35,670 --> 00:02:41,400
So now let's go ahead and create a template by taking an example in the upcoming lecture.

34
00:02:41,670 --> 00:02:43,680
So thank you very much for watching.

35
00:02:43,680 --> 00:02:45,690
And I'll see you guys next time.

36
00:02:46,230 --> 00:02:46,770
Thank you.

