1
00:00:00,180 --> 00:00:04,710
OK, so in this lecture, let's continue with the detailed view functionality.

2
00:00:05,070 --> 00:00:13,530
So our job is now to go ahead and just display the details whenever someone actually visits, would

3
00:00:13,530 --> 00:00:14,760
slash one over here.

4
00:00:15,420 --> 00:00:21,360
So now, in order to display those database details, we first need to go ahead and obtain the data

5
00:00:21,360 --> 00:00:23,340
from our database or the backend.

6
00:00:23,700 --> 00:00:25,630
So how exactly are we going to do that?

7
00:00:25,830 --> 00:00:28,490
So for that, let's move on to the detailed view right here.

8
00:00:28,650 --> 00:00:33,780
And just as we have queried the items over here, we are also going to go ahead and do the same over

9
00:00:33,780 --> 00:00:34,410
here as well.

10
00:00:35,040 --> 00:00:39,760
So now what we do is that we don't want to query all the items.

11
00:00:39,780 --> 00:00:44,610
Instead, we want to query the item whose ID is equal to the item, Edem.

12
00:00:46,110 --> 00:00:51,020
So if someone enters food, slash one the item, it becomes one.

13
00:00:51,030 --> 00:00:54,420
And we actually want to query the object whose item idea is one.

14
00:00:55,140 --> 00:00:58,290
So let's go ahead and do that so I can type an item.

15
00:00:59,430 --> 00:01:05,430
And now instead of getting all the objects, we want to get the object who has a specific ID.

16
00:01:05,459 --> 00:01:12,950
So we type in objects dot get and in here I'll type in Pecky, which is primary.

17
00:01:12,950 --> 00:01:14,780
He is equal to item ID.

18
00:01:15,450 --> 00:01:23,160
So what this means is that we only want to get that particular object whose ID or pecky or primary key

19
00:01:23,160 --> 00:01:26,490
is equal to item ID, which is this item idea right here.

20
00:01:27,210 --> 00:01:32,700
Now let's go ahead and store this thing into some sort of a variable, so I'll just go ahead and see

21
00:01:32,710 --> 00:01:34,470
that and let's see item.

22
00:01:34,500 --> 00:01:40,710
So now once we have this data into the item, let's go ahead and create a context out of it so I can

23
00:01:40,710 --> 00:01:50,010
type in context equals and then just simply type an item and then give a colon that's going to be item

24
00:01:50,550 --> 00:01:52,240
and then give a comma at the end.

25
00:01:52,770 --> 00:01:58,230
So now, as you can see, we have created the context in the same fashion as we had created here.

26
00:01:58,980 --> 00:02:04,470
So now once you have the context in here, now we all know that you need to pass in the context to a

27
00:02:04,470 --> 00:02:06,750
template along with the request.

28
00:02:07,080 --> 00:02:10,050
So right now, we don't have any template for detail.

29
00:02:10,050 --> 00:02:13,260
So let's go ahead and create that template right now.

30
00:02:14,220 --> 00:02:16,790
So let's go to Templates Directory.

31
00:02:16,800 --> 00:02:18,360
Let's go to the food directory.

32
00:02:18,960 --> 00:02:19,200
Right.

33
00:02:19,200 --> 00:02:21,930
Click over here and create a new file.

34
00:02:21,960 --> 00:02:24,690
And that's going to be called as detailed on each year.

35
00:02:25,890 --> 00:02:30,530
So now, once we have this detailed template, let's go ahead and write in some code in here.

36
00:02:31,840 --> 00:02:38,920
So, you know, once we have the context in this particular item, we can now go ahead and easy to access

37
00:02:38,920 --> 00:02:39,760
the properties.

38
00:02:40,180 --> 00:02:42,720
So let me just type in each one as a heading.

39
00:02:42,730 --> 00:02:49,930
And in here, let's use the template syntax and we already have the context as item so we can type in

40
00:02:49,930 --> 00:02:56,700
item dot and let's say I want to access the item name so I can simply type an item and a school name.

41
00:02:57,850 --> 00:03:05,860
And now let's say I want to display the item description in each to I simply type in H2 and then I can

42
00:03:05,860 --> 00:03:07,150
type in something like.

43
00:03:09,000 --> 00:03:16,020
Item, not item on the school description, item on the school description.

44
00:03:16,200 --> 00:03:25,230
And now let's see, see, I want to display the price so I can type in item, dot item and the school

45
00:03:25,810 --> 00:03:26,400
price.

46
00:03:27,370 --> 00:03:32,310
OK, so once we have extracted all those properties over here, let's see if this thing works fine.

47
00:03:33,070 --> 00:03:38,050
So we have passed in the context, we have created the template and now we need to render the template

48
00:03:38,050 --> 00:03:39,280
along with the context.

49
00:03:39,670 --> 00:03:41,620
So, again, we will use a shortcut here.

50
00:03:41,920 --> 00:03:45,010
That is, will you surrender and then pass and request.

51
00:03:45,820 --> 00:03:49,120
And then you also need to pass in the path for the template.

52
00:03:49,150 --> 00:03:58,600
So this is going to be forward slash detail dot H.M. And then you also pass in the context, which is

53
00:03:58,600 --> 00:03:59,830
this context right here.

54
00:04:00,180 --> 00:04:02,040
So I can put this in context in here.

55
00:04:03,750 --> 00:04:09,390
And now let's go ahead, save the code and let's see if this thing works out so I can go back over here

56
00:04:10,230 --> 00:04:14,700
and now instead of just food, when I type in food, slash one.

57
00:04:14,700 --> 00:04:18,200
As you can see, we get the details for pizza over here.

58
00:04:18,390 --> 00:04:20,880
And then I go ahead open food slash.

59
00:04:21,839 --> 00:04:27,870
Do we don't get anything because food with an I.D. two does not exist, when I go ahead and type in

60
00:04:27,870 --> 00:04:35,220
food, slash three, I get the details of a burrito when I go ahead and type in food slash for I get

61
00:04:35,220 --> 00:04:36,370
a cheeseburger here.

62
00:04:36,930 --> 00:04:40,890
That means we have successfully added the functionality for detail.

63
00:04:41,340 --> 00:04:47,610
But there is one single problem over here that is we don't want to actually go ahead and manually add

64
00:04:47,610 --> 00:04:49,650
and those IDs instead.

65
00:04:49,650 --> 00:04:53,770
What we want to do is that we simply want to click on these IDs over here.

66
00:04:55,110 --> 00:04:58,740
So let's go ahead and implement that feature as well.

67
00:04:59,490 --> 00:05:06,300
So now we all know that this view actually comes from the index, not HTML page, which is this page

68
00:05:06,300 --> 00:05:06,860
right here.

69
00:05:07,410 --> 00:05:14,880
And in order to add that functionality, you simply need to add a EAF tag over here so I can type in

70
00:05:14,880 --> 00:05:15,690
HSF.

71
00:05:16,200 --> 00:05:21,630
And now in here in this particular attack, you can simply just get that piece set up over here.

72
00:05:22,170 --> 00:05:30,300
And instead of having the link as blank, you can type in the usual slash food slash and then you actually

73
00:05:30,300 --> 00:05:34,890
want the item ID in here, which is nothing but this item ID.

74
00:05:34,920 --> 00:05:36,420
So I can simply go ahead.

75
00:05:36,900 --> 00:05:37,740
Copy that.

76
00:05:38,670 --> 00:05:39,840
We sit over here.

77
00:05:40,230 --> 00:05:45,190
So what this does is that for each item, it's going to create a dynamic mural.

78
00:05:45,240 --> 00:05:51,120
So, for example, for the first item, the wall is going to be slash food, slash one so that whenever

79
00:05:51,120 --> 00:05:57,420
you click on the first item, it it's going to redirect to slash food, slash one or the second item.

80
00:05:57,420 --> 00:05:59,760
It's going to do slash food, slash two.

81
00:06:00,660 --> 00:06:02,490
Now let's see this thing in action.

82
00:06:02,680 --> 00:06:05,050
So now if I go ahead, hit refresh.

83
00:06:05,070 --> 00:06:07,620
Now, as you can see, all of those things are links.

84
00:06:08,010 --> 00:06:13,950
And now if you hover over here, as you can see at the bottom, make sure that you have your eye over

85
00:06:13,950 --> 00:06:14,250
here.

86
00:06:14,730 --> 00:06:21,360
So when I go over this, as you can see, the wall is slash food, slash one when I go down, this thing

87
00:06:21,360 --> 00:06:27,810
has you are like slash food, slash three and this has a real -- slash food slash full.

88
00:06:28,500 --> 00:06:32,940
So now when I click on the first thing, I get pizza and it's details.

89
00:06:33,060 --> 00:06:36,600
When I click on Burrito, I get a burrito and it's details.

90
00:06:36,900 --> 00:06:42,360
And when I go ahead and click on Cheeseburger, I get the details for a cheeseburger as well as its

91
00:06:42,360 --> 00:06:42,960
details.

92
00:06:43,260 --> 00:06:45,620
Now, sorry for the typo here.

93
00:06:46,060 --> 00:06:49,430
That's a mistake, but still our app is going to work just fine.

94
00:06:50,850 --> 00:06:54,510
So that said, we have implemented the detailed view functionality.

95
00:06:54,510 --> 00:06:59,890
That is, we now can get details of any individual items on which we click.

96
00:06:59,910 --> 00:07:01,240
So that's it for this lecture.

97
00:07:01,260 --> 00:07:06,780
Hopefully you guys were able to understand this detail functionality, but in case if you have some

98
00:07:06,780 --> 00:07:12,390
doubts regarding this, then feel free to post your query in the Q&A section and I will be there to

99
00:07:12,390 --> 00:07:13,200
help you out.

100
00:07:13,730 --> 00:07:15,660
So thank you very much for watching.

101
00:07:15,660 --> 00:07:17,790
And I'll see you guys next time.

102
00:07:18,090 --> 00:07:18,690
Thank you.

