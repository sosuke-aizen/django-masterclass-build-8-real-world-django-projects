1
00:00:00,300 --> 00:00:06,870
Hello and welcome to this lecture and in this lecture, we will go ahead and talk about name speccing

2
00:00:06,870 --> 00:00:07,740
Yarl's.

3
00:00:08,670 --> 00:00:11,370
So what exactly do I mean by name speccing?

4
00:00:11,490 --> 00:00:11,910
You are.

5
00:00:12,630 --> 00:00:13,950
So now consider this.

6
00:00:14,490 --> 00:00:18,150
Let's go to the all star profile of your food app.

7
00:00:18,300 --> 00:00:24,840
So if you just go ahead and go to the all star profile, as you can see, we have a bunch of parts or

8
00:00:24,840 --> 00:00:27,030
you are old patterns to find right over here.

9
00:00:27,300 --> 00:00:29,820
And each one of them also have a name.

10
00:00:29,850 --> 00:00:33,030
So, for example, this part actually has a name index.

11
00:00:33,330 --> 00:00:38,700
This part actually has a name as detail, and this path actually has a name as Eitam.

12
00:00:38,790 --> 00:00:41,110
So that's all fine and good.

13
00:00:41,700 --> 00:00:47,220
Now you need to understand that these you are actually present inside our food app.

14
00:00:47,490 --> 00:00:53,460
So if you have a look at this, you Alstott profile, they are actually present in our food app and

15
00:00:53,460 --> 00:01:01,350
the food app is actually just a part of our bigger project, which is my site, which means that just

16
00:01:01,350 --> 00:01:05,800
like the food up, there could be multiple apps present in our project as well.

17
00:01:06,720 --> 00:01:12,450
So while you're working in a company and you become an Shango developer, what might happen is that

18
00:01:12,480 --> 00:01:19,380
you might be working on the food app for the myside project while your friend or co-worker might be

19
00:01:19,380 --> 00:01:21,960
working on some other app in the same project.

20
00:01:22,770 --> 00:01:31,320
So now what would happen if he actually creates you are patterns in his own app and he also names those

21
00:01:31,470 --> 00:01:34,150
parts as a detail and item.

22
00:01:34,170 --> 00:01:41,200
So in that case, if you actually go to the index card, H.M., as you can see, we have mentioned a

23
00:01:41,220 --> 00:01:47,100
little over here now, your friend also has an app and he has also the final.

24
00:01:47,100 --> 00:01:50,280
You are all partnered with the name detail in his own app.

25
00:01:50,850 --> 00:01:53,370
So how exactly will Django understand?

26
00:01:53,670 --> 00:01:57,930
To which detail does this detail refer to?

27
00:01:59,080 --> 00:02:05,650
So now Django has two choices, either it has to follow the detailed path which is created by you over

28
00:02:05,650 --> 00:02:12,490
here and this particular app, which is the Alstott profile of your food app, or it can refer to the

29
00:02:12,610 --> 00:02:16,960
detail you are URL, which is created by your friend and some other app.

30
00:02:17,830 --> 00:02:24,040
So in order to avoid such confusions, when we are working with multiple apps, what we do is that we

31
00:02:24,040 --> 00:02:26,470
go ahead and we namespace that.

32
00:02:26,470 --> 00:02:27,180
You are rules.

33
00:02:27,760 --> 00:02:29,460
So how exactly do we do that?

34
00:02:29,620 --> 00:02:36,250
So in name spacing, what you simply do is that you simply go ahead and add app and the school name

35
00:02:36,250 --> 00:02:40,640
in the URL Stoppie file and just add in the app name over here.

36
00:02:41,140 --> 00:02:44,920
So our current app is food soil, simply type in food here.

37
00:02:45,490 --> 00:02:49,600
And once we have typed this thing in, you now have to simply go ahead.

38
00:02:49,990 --> 00:02:55,560
And wherever you have used these, you are all these parts right here.

39
00:02:55,600 --> 00:02:57,130
You can simply go over there.

40
00:02:57,580 --> 00:03:06,640
And now, instead of just specifying detail, I will now mention that my particular detailed path actually

41
00:03:06,640 --> 00:03:12,580
belongs from the app, which is food, soil type food, colon and then detail.

42
00:03:13,180 --> 00:03:20,050
So now Django actually understands that, OK, this detail you are actually belongs to the food app.

43
00:03:20,350 --> 00:03:26,410
So that means it is only going to look at the you are stored by a file of the food app and it's not

44
00:03:26,410 --> 00:03:29,710
going to look at the URL profile of your friends up.

45
00:03:29,710 --> 00:03:35,920
And when your friend is going to write in some good, he also has to go ahead and use name spacing and

46
00:03:35,920 --> 00:03:41,220
he has to explicitly mention the name of the app before specifying any.

47
00:03:41,230 --> 00:03:42,290
You are right here.

48
00:03:42,400 --> 00:03:48,390
So that's how namespace works and that's why name spacing is actually important in Django.

49
00:03:48,910 --> 00:03:54,440
So hopefully you guys be able to understand what is name spacing and how exactly it is used.

50
00:03:54,730 --> 00:03:58,930
So thank you very much for watching and I'll see you guys next time.

51
00:03:59,470 --> 00:04:00,070
Thank you.

