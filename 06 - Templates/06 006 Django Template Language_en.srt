1
00:00:00,060 --> 00:00:05,400
So in the previous lecture, we have created a Shango template and we have also passed some context

2
00:00:05,590 --> 00:00:06,100
to do it.

3
00:00:06,840 --> 00:00:11,460
We have also written some code in the template using a very different Shango syntax.

4
00:00:11,670 --> 00:00:16,620
And I'm sure that you might be confused looking at the syntax, seeing what exactly was that.

5
00:00:17,310 --> 00:00:22,940
So the syntax which we had used in that particular lecture was the Shango template language.

6
00:00:22,950 --> 00:00:27,960
And this is what exactly we are going to learn a little bit more about in this particular lecture.

7
00:00:28,080 --> 00:00:33,840
So let's go ahead and learn about the Django templating language, which is nothing but the syntax which

8
00:00:33,840 --> 00:00:35,400
we had used in the previous case.

9
00:00:35,940 --> 00:00:38,790
So this language was not actually Python.

10
00:00:38,940 --> 00:00:40,910
Neither was it H.M..

11
00:00:41,310 --> 00:00:47,940
This is a specific language used by Django, which is called as the DTL or Django template language.

12
00:00:48,660 --> 00:00:54,480
So in this lecture, let's learn about DTL in a little bit much more, by the way, so as to understand

13
00:00:55,140 --> 00:00:58,260
what it exactly is and how it actually works.

14
00:00:59,130 --> 00:01:04,860
So to understand, DTL, the first and foremost thing which you need to understand is you need to understand

15
00:01:04,860 --> 00:01:11,190
what exactly is a templating engine now to be able to render templates in Django app.

16
00:01:11,430 --> 00:01:14,100
We need something which is called as a templating engine.

17
00:01:14,610 --> 00:01:21,540
Now, with Django, you can use multiple templating engines, uh, like, uh, the Django templating

18
00:01:21,540 --> 00:01:27,210
engine or another popular engine, which is called as the Ginge, too, which is also templating engine,

19
00:01:27,210 --> 00:01:29,050
which you can use with Django.

20
00:01:29,160 --> 00:01:36,180
Now, apart from that, Django also provides us with its own default templating engine, which is the

21
00:01:36,420 --> 00:01:39,210
DTL, which stands for Django template language.

22
00:01:39,870 --> 00:01:46,500
Now, the Django template language actually has its own syntax, the one which we have used in the previous

23
00:01:46,500 --> 00:01:46,920
case.

24
00:01:47,070 --> 00:01:52,980
And hence it's sort of like having its own programming language, which defines its own syntax.

25
00:01:53,340 --> 00:02:00,000
Now, as you can see, this was the syntax from that example, and we won't jump into much depth about

26
00:02:00,000 --> 00:02:04,270
that, but we will surely learn a few things to understand the syntax.

27
00:02:04,830 --> 00:02:09,430
So, first of all, we will learn how to exactly define a variable in DTL.

28
00:02:09,780 --> 00:02:15,510
So as you can see, this thing right here, which is the item ID here, is nothing, but it's actually

29
00:02:15,510 --> 00:02:16,220
a variable.

30
00:02:16,710 --> 00:02:22,950
So whenever you want to define a variable in DTL, you just need to make sure that you define a variable

31
00:02:22,950 --> 00:02:25,800
in between these two calibrations right here.

32
00:02:25,950 --> 00:02:32,120
So you need to have the opening curly braces and then you need to have to close in curly braces whenever

33
00:02:32,130 --> 00:02:34,500
you actually want to define a variable.

34
00:02:35,250 --> 00:02:41,220
Now, another important thing which we will learn about DTL is how to define tags.

35
00:02:41,550 --> 00:02:48,030
So whenever you want to write some kind of a programming construct or whenever you want to write in

36
00:02:48,420 --> 00:02:55,020
some control flow logic, you always go ahead and make sure to include that in the form of these tags

37
00:02:55,020 --> 00:02:55,780
right over here.

38
00:02:56,280 --> 00:03:01,980
So whenever you want to use tags, you define those tags by using your opening curly bracket or person

39
00:03:01,980 --> 00:03:05,140
sign, then a person sign and a closing curly bracket.

40
00:03:05,850 --> 00:03:08,570
So this is how you can go ahead and define tags.

41
00:03:08,580 --> 00:03:14,130
So we will be using the Django templating language a lot in the upcoming lectures.

42
00:03:14,130 --> 00:03:16,640
When will we go ahead and design these templates?

43
00:03:16,980 --> 00:03:23,190
So these are the only two things which you need to remember for now in case of Django template language

44
00:03:23,190 --> 00:03:29,010
that is nothing but how to define the variables and how to define the control flow using these tags

45
00:03:29,010 --> 00:03:29,730
right over here.

46
00:03:29,760 --> 00:03:31,290
So that's it for this lecture.

47
00:03:31,680 --> 00:03:38,430
And this lecture was just to give you guys a brief overview about what the syntax actually was so that

48
00:03:38,430 --> 00:03:43,060
you guys don't get confused about which exact language did we use here.

49
00:03:43,920 --> 00:03:45,600
So that's it for this lecture.

50
00:03:45,720 --> 00:03:47,610
And I'll see you guys in the next one.

