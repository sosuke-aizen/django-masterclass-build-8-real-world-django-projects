1
00:00:00,180 --> 00:00:06,600
Hello and welcome to this lecture and in this lecture, we will go ahead and learn about removing hardcoded

2
00:00:06,630 --> 00:00:07,140
you are.

3
00:00:08,130 --> 00:00:14,540
So what exactly do I mean by hardcoded you are allowed and why do we actually need to remove them?

4
00:00:15,210 --> 00:00:21,360
So in the previous lecture, if you remember in the index taught each HTML file, we have set up the

5
00:00:21,360 --> 00:00:28,350
URL for this particular each rectangle here that is, we have said the nature of property of this thing

6
00:00:28,350 --> 00:00:32,700
to slash food and then we have added item right over here.

7
00:00:34,260 --> 00:00:41,400
So out of this particular, you are as you can see, this item in part is actually a dynamic and that

8
00:00:41,400 --> 00:00:42,420
is it can change.

9
00:00:42,720 --> 00:00:45,060
But we have actually hardcoded this part.

10
00:00:45,060 --> 00:00:47,000
Right, which is the food part.

11
00:00:47,640 --> 00:00:54,570
So such kind of for you all is called as a hardcoded you are, which means that we have manually added

12
00:00:54,570 --> 00:00:56,930
something over here to the you are now.

13
00:00:57,360 --> 00:01:01,710
How exactly can we go ahead and fix this particular hardcoded?

14
00:01:01,710 --> 00:01:02,150
You are.

15
00:01:02,220 --> 00:01:08,940
That is, how can we make it not hardcoded, but instead a dynamic you are now if you actually go to

16
00:01:08,940 --> 00:01:17,310
the you Alstott profile right here, as you can see for this particular part right here of the detail,

17
00:01:17,310 --> 00:01:20,960
but we have already named this particular path.

18
00:01:21,150 --> 00:01:24,410
So the name of this particular part is and detail.

19
00:01:24,840 --> 00:01:32,610
So what you can essentially do over here is that if you go to the next Tmall, you can essentially replace

20
00:01:32,820 --> 00:01:34,320
this particular hardcoded.

21
00:01:34,320 --> 00:01:37,460
You are with that particular name of the path.

22
00:01:37,650 --> 00:01:43,040
So instead of having the hardcoded you are here, you can simply get rid of that.

23
00:01:43,050 --> 00:01:48,240
And in here we'll use the template syntax, which is the syntax right here.

24
00:01:48,570 --> 00:01:54,180
And then you just type in your order, meaning that you want to mention that you are Allaway here and

25
00:01:54,180 --> 00:02:00,420
now instead of mentioning the entire hardcoded, you are, as we all know, that we want to refer to

26
00:02:00,420 --> 00:02:03,510
this particular path or this particular you are right here.

27
00:02:03,870 --> 00:02:11,310
You can now simply go ahead and add its name so you can type in the name as detail so you can just go

28
00:02:11,310 --> 00:02:14,310
ahead and type in detail and.

29
00:02:15,810 --> 00:02:22,020
So now it understands that, OK, it needs to go ahead and look for this path right here.

30
00:02:22,470 --> 00:02:26,270
And this path is nothing but slash food, slash idee.

31
00:02:26,280 --> 00:02:31,830
And after having the name over here, you can actually go ahead and passing the item ideas.

32
00:02:31,830 --> 00:02:36,030
Well, which is nothing but this idea right here.

33
00:02:36,710 --> 00:02:43,110
So you can go ahead, get out of these single cuts and in here you can just simply mention the item

34
00:02:43,530 --> 00:02:48,120
so you can type an item dot idea here and you should be good to go.

35
00:02:49,020 --> 00:02:54,450
So now once you go ahead and save the code, this code should actually work just fine and you should

36
00:02:54,450 --> 00:02:55,440
get your result.

37
00:02:55,620 --> 00:02:59,730
So now let's go to the app and see if this thing works.

38
00:02:59,940 --> 00:03:06,180
So as you can see, if you go ahead, hit refresh, the app is still going to work just fine.

39
00:03:06,210 --> 00:03:12,780
So now once we know that the app works fine, we confirm that this particular jewel is no longer hardcoded

40
00:03:12,780 --> 00:03:15,720
and it does not affect the functionality of the app.

41
00:03:16,320 --> 00:03:19,280
Now, why exactly do we need to remove hardcoded?

42
00:03:19,290 --> 00:03:22,950
You are the reason why we need to remove a hardcoded.

43
00:03:22,950 --> 00:03:30,720
You are is because using the hardcoded you, Earl, makes it a tightly Koppell approach and it makes

44
00:03:30,900 --> 00:03:33,810
things less dynamic or less flexible.

45
00:03:34,260 --> 00:03:40,200
And if we keep on adding or keep on using hardcoded, you also hear an entire application.

46
00:03:40,530 --> 00:03:47,190
Then changing the users can become quite challenging if we have multiple templates in our project right

47
00:03:47,190 --> 00:03:47,430
here.

48
00:03:47,610 --> 00:03:53,060
So as you can see right now, you only have two templates in here and it was easy enough to change the

49
00:03:53,160 --> 00:03:53,490
world.

50
00:03:53,490 --> 00:04:00,120
But when we have a huge project having a hardcoded, you are or here is not actually a good practice.

51
00:04:01,110 --> 00:04:02,740
So that's it for this lecture.

52
00:04:02,760 --> 00:04:06,910
Hopefully you guys be able to understand how to remove the hardcoded you are.

53
00:04:07,290 --> 00:04:14,340
So the only thing which we have done here is that instead of having slash food slash over here, we

54
00:04:14,340 --> 00:04:15,930
have simply replaced that.

55
00:04:16,050 --> 00:04:23,010
You are all with the actual name of the you are, which is detail and the item, it actually stays right

56
00:04:23,010 --> 00:04:23,510
over here.

57
00:04:24,600 --> 00:04:26,190
So that's it for this lecture.

58
00:04:26,220 --> 00:04:28,330
And I'll see you guys in the next one.

59
00:04:28,710 --> 00:04:29,310
Thank you.

