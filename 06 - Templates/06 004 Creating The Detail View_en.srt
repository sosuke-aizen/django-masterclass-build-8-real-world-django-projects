1
00:00:00,550 --> 00:00:07,270
Hello and welcome to this lecture and in this lecture, we will go ahead and implement the delete functionality.

2
00:00:07,930 --> 00:00:13,780
So right now we already know that we have implemented this functionality where you pass on some data

3
00:00:13,780 --> 00:00:17,410
to the template and that data gets displayed over here.

4
00:00:18,040 --> 00:00:24,670
Now, what we want to do is that we want to make sure that wherever we click one of these items right

5
00:00:24,670 --> 00:00:31,750
here, we go into the detail page of those items where you get a detailed information about these food

6
00:00:31,750 --> 00:00:32,299
items.

7
00:00:32,320 --> 00:00:38,470
So, for example, when I go ahead and click on pizza, I should get all the details for this food item,

8
00:00:38,470 --> 00:00:42,310
pizza, that is, I should get its name, its price.

9
00:00:42,460 --> 00:00:45,380
I should also get its description as well.

10
00:00:45,820 --> 00:00:51,790
So how exactly can we do that or how exactly can we implement that particular functionality?

11
00:00:53,160 --> 00:01:00,450
Now, we already know that whenever you want to display a new view or a new page, you need to go ahead

12
00:01:00,570 --> 00:01:02,030
and design a view.

13
00:01:02,490 --> 00:01:04,920
So we are going to do exactly the same thing.

14
00:01:05,580 --> 00:01:10,650
So what we wish to do here is that we need to create a new view, which is called Ask the Detailed View.

15
00:01:11,280 --> 00:01:13,210
So let's go ahead and do that over here.

16
00:01:13,560 --> 00:01:18,850
So I'll just go down here and we'll create a view which is called less detail.

17
00:01:19,260 --> 00:01:22,770
So let me just go ahead and type in depth detail.

18
00:01:23,310 --> 00:01:29,880
And this thing or this view is going to accept two things, as usually it is going to accept a request.

19
00:01:30,510 --> 00:01:37,530
And one additional thing which this view is going to accept is that it's going to accept the ID, which

20
00:01:37,530 --> 00:01:41,820
is the item ID and what do I exactly mean by accepting item ID?

21
00:01:42,540 --> 00:01:49,590
So whenever you click one of these items, you are expected to get the details of that item on the next

22
00:01:49,590 --> 00:01:50,010
page.

23
00:01:50,040 --> 00:01:55,370
So, for example, if you click pizza, you should not get the details of a burrito on the next page.

24
00:01:55,710 --> 00:01:58,740
Instead, you should get details of pizza only.

25
00:01:59,340 --> 00:02:05,810
So how exactly do we make sure that Django knows that we have clicked on this item or pizza and not

26
00:02:05,820 --> 00:02:09,870
burrito and we can do that by identifying its ID?

27
00:02:10,080 --> 00:02:16,290
So when we click on pizza, Django knows that the idea for pizza is one and we actually need to pass

28
00:02:16,290 --> 00:02:22,080
that ID to the next page so that the next page is going to only display the details for pizza.

29
00:02:23,250 --> 00:02:26,130
And hence we passing the idea over here.

30
00:02:26,760 --> 00:02:29,880
And let's call that IDS item and the school ID.

31
00:02:31,940 --> 00:02:38,650
OK, so for now, what we will do is that we will make this view return the particular idea only.

32
00:02:39,080 --> 00:02:46,400
So, for example, if the user types in the ID as one, we want to return some response, like, OK,

33
00:02:46,580 --> 00:02:53,960
you have visited the page for ID one, or we can say something like this is item number and we can specify

34
00:02:53,960 --> 00:02:59,900
the item video here so I can type and return HTP response.

35
00:03:00,290 --> 00:03:10,610
And here I can say something like this is item number or let's say ID and then I can type in %s and

36
00:03:10,610 --> 00:03:16,520
we are going to pass in the item ID here by typing in the person sign item underscore ID.

37
00:03:16,970 --> 00:03:20,730
So we simply take this item ID and display it over here.

38
00:03:21,950 --> 00:03:24,820
So this is not our final little view.

39
00:03:24,860 --> 00:03:30,590
This is just for the sake of an understanding and we are going to modify this when we go ahead in this

40
00:03:30,590 --> 00:03:31,580
particular lecture.

41
00:03:32,210 --> 00:03:38,240
OK, so now once a detailed view is ready, you know what we do when we go ahead and create a view?

42
00:03:38,450 --> 00:03:41,120
You need to attach the view with the particular you are.

43
00:03:41,840 --> 00:03:46,670
So let's go into the you are all start by file, which is this file right here.

44
00:03:47,060 --> 00:03:52,100
And now let's go ahead and create the you are alberton for that particular view.

45
00:03:53,040 --> 00:03:59,610
So even before we do that, you first need to understand how this view works, so this view works when

46
00:03:59,610 --> 00:04:03,090
we go ahead and type in slash food slash.

47
00:04:04,380 --> 00:04:11,190
And now for the detailed view, we want to make it work when we type in slash food slash and the idea.

48
00:04:11,610 --> 00:04:13,620
So the idea of the food might be one.

49
00:04:13,620 --> 00:04:15,750
It might be too or it might be three.

50
00:04:15,870 --> 00:04:21,779
So we want to design a general pattern which handles a request which looks something like this.

51
00:04:22,380 --> 00:04:24,150
So how exactly can we do that?

52
00:04:24,900 --> 00:04:31,260
So instead of accepting something empty over here, we need to create a path which is going to accept

53
00:04:31,260 --> 00:04:32,140
an integer.

54
00:04:32,460 --> 00:04:36,330
So we need to specify that we need to accept an integer.

55
00:04:36,450 --> 00:04:40,250
And that integer is nothing but the item ID.

56
00:04:40,380 --> 00:04:48,660
So I'll pass an item ID in here because this number right here is nothing but the item ID and then we

57
00:04:48,660 --> 00:04:51,330
give a slide show you a comma.

58
00:04:51,510 --> 00:04:54,690
And the view which we are talking about is the details for you.

59
00:04:54,870 --> 00:04:57,330
So we type in view start detail.

60
00:04:57,330 --> 00:04:59,610
Comma name.

61
00:05:00,890 --> 00:05:01,790
Equals.

62
00:05:03,450 --> 00:05:08,850
Detail, just make sure to have a comma at the end and you are now good to go.

63
00:05:09,690 --> 00:05:16,050
So just make sure that the subway is up and running and now what you can do is that in order to test

64
00:05:16,050 --> 00:05:22,710
this, if this thing works, you can type in food, slash one, and you should get this is the item

65
00:05:22,710 --> 00:05:24,040
number one.

66
00:05:24,840 --> 00:05:32,520
Now, if you go ahead and type in food, slash two, you should get this as item number or ID two.

67
00:05:32,790 --> 00:05:37,950
When you go ahead and type this thing as three or 23, you should get that.

68
00:05:37,950 --> 00:05:40,190
This item number is 23.

69
00:05:40,200 --> 00:05:43,360
So that means a part of our detailed view is working.

70
00:05:43,890 --> 00:05:50,400
So what we wish to do now is that instead of getting these random numbers in here, we actually want

71
00:05:50,400 --> 00:05:53,760
to display the details of those specific items.

72
00:05:53,760 --> 00:06:00,810
So, for example, when I go ahead and type in food one, I don't want to see this output right here.

73
00:06:01,110 --> 00:06:07,650
But instead, what I want to see is that I want to see the details of the item whose ID is one.

74
00:06:08,760 --> 00:06:13,370
So we are going to implement the rest of the functionality in the upcoming lecture.

75
00:06:13,530 --> 00:06:19,950
So hopefully you guys be able to understand how we have created this view right here, which is the

76
00:06:20,130 --> 00:06:25,570
detailed view and how we have configured that you are for that particular view in the you are.

77
00:06:25,580 --> 00:06:26,300
Let's start by.

78
00:06:27,540 --> 00:06:29,340
So thank you very much for watching.

79
00:06:29,340 --> 00:06:31,350
And I'll see you guys next time.

80
00:06:31,710 --> 00:06:32,280
Thank you.

