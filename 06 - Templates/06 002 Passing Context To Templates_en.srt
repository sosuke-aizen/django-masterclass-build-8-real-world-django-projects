1
00:00:00,300 --> 00:00:06,630
Hello and welcome to this lecture and in this lecture, we will go ahead and learn how to pass database

2
00:00:06,630 --> 00:00:08,400
data to templates.

3
00:00:08,730 --> 00:00:12,750
So in the previous lecture, we have already rendered this template right here.

4
00:00:13,080 --> 00:00:18,450
And right now we are going to pass in the database data, which is the item list to this particular

5
00:00:18,450 --> 00:00:18,900
template.

6
00:00:18,900 --> 00:00:21,150
So let's learn how exactly can we do that.

7
00:00:21,840 --> 00:00:27,840
So the first thing which you need to do here is that you need to go ahead and pass in the context.

8
00:00:27,840 --> 00:00:35,010
And the context is nothing but this particular object right here, which we have extracted from the

9
00:00:35,010 --> 00:00:35,850
item model.

10
00:00:36,120 --> 00:00:40,740
So in order to pass that as context, you simply type an item list.

11
00:00:42,480 --> 00:00:50,460
And you need to actually include it in single quotations and a colon and again, then just type an item

12
00:00:50,460 --> 00:00:50,880
list.

13
00:00:52,590 --> 00:00:57,670
So this basically means that we are passing item list as item list.

14
00:00:57,920 --> 00:01:03,440
Now make sure that you have the comma at the end or else this context is not going to work fine.

15
00:01:04,230 --> 00:01:08,120
And now you just need to go ahead and pass in the context over here.

16
00:01:08,130 --> 00:01:11,760
And we have already passed in the context here in the previous case.

17
00:01:12,720 --> 00:01:18,300
So now once the context is passed in here, the next thing which you need to do is that you need to

18
00:01:18,300 --> 00:01:22,740
go ahead and make use of that past context right over here.

19
00:01:23,160 --> 00:01:29,820
So what I will do here is that I will just go ahead and get rid of all this code right here just to

20
00:01:29,820 --> 00:01:30,900
avoid confusion.

21
00:01:31,110 --> 00:01:36,780
And we are only going to have the template code in here and instead of the entire HTML file.

22
00:01:37,140 --> 00:01:43,740
So now let's go ahead and make use of this context so that we can display items right over here.

23
00:01:44,040 --> 00:01:50,580
Now, if you have a look at this particular item list here, this item list is nothing but the list

24
00:01:50,580 --> 00:01:55,300
of all the items which we have over here or the list of all the item objects.

25
00:01:55,770 --> 00:02:01,510
Now, in order to loop through a particular list in Python, we use a for loop.

26
00:02:01,650 --> 00:02:05,570
So we are going to follow the same exact approach in Django as well.

27
00:02:05,820 --> 00:02:08,430
That is, we are going to use a for loop over here.

28
00:02:08,729 --> 00:02:13,590
But in Django, you just cannot directly go ahead and write a for loop.

29
00:02:13,800 --> 00:02:18,090
Instead, you have to write a for loop using the template syntax.

30
00:02:18,480 --> 00:02:24,210
So in template syntax, whenever you want to write a for loop, first of all, you have these curly

31
00:02:24,210 --> 00:02:30,150
braces and within these curly braces you have the person saying, make sure that you have a space in

32
00:02:30,150 --> 00:02:31,380
here and here.

33
00:02:31,380 --> 00:02:35,670
You can start writing your for loop so you can write something like four item.

34
00:02:35,850 --> 00:02:39,060
And we actually want the item from the item list.

35
00:02:39,060 --> 00:02:42,880
So we type in in item on the school list.

36
00:02:42,900 --> 00:02:47,530
So now once we have this loop, we also need to make sure that we end this loop.

37
00:02:47,550 --> 00:02:53,050
So we again use the same syntax over here and we end the loop by typing in and form.

38
00:02:53,100 --> 00:02:57,240
And now once we have this loop, we are basically good to go now in here.

39
00:02:57,300 --> 00:03:03,180
The way in which this loop works is that it loops through each and every item in this particular item

40
00:03:03,180 --> 00:03:03,570
list.

41
00:03:03,870 --> 00:03:10,230
And that item is going to be stored in the item variable right here and now in order to display the

42
00:03:10,230 --> 00:03:17,670
contents of that item, we go ahead and type an item over here so I can type in something like item.

43
00:03:18,090 --> 00:03:25,500
And now in order to access the attributes of that particular object, I type in DOT and then let's say

44
00:03:25,500 --> 00:03:27,810
we want to access the idea of the item.

45
00:03:27,820 --> 00:03:29,760
I'll simply type an idea over here.

46
00:03:29,760 --> 00:03:33,030
And now let's say we also want to display the item name as well.

47
00:03:33,590 --> 00:03:40,740
Again, go ahead and type in item, dot item, underscore name and let's make sure that we have some

48
00:03:40,740 --> 00:03:44,020
dashes over here so that these two attributes are separated.

49
00:03:44,610 --> 00:03:50,130
So now once we go ahead, save the code, let's go ahead, run the server and see how this thing is

50
00:03:50,130 --> 00:03:50,840
going to work.

51
00:03:51,000 --> 00:03:54,620
So I'll just go ahead, open up the browser.

52
00:03:55,290 --> 00:03:56,460
Let's go to the board.

53
00:03:56,460 --> 00:03:57,840
No slash food.

54
00:03:57,840 --> 00:04:02,350
And now, as you can see, we have all of our items displayed over here.

55
00:04:02,400 --> 00:04:04,820
That is the item ID and the item name.

56
00:04:04,830 --> 00:04:06,510
So we have item 81.

57
00:04:06,510 --> 00:04:08,960
Then we have the item name as pizza.

58
00:04:08,970 --> 00:04:10,440
We have item 83.

59
00:04:10,440 --> 00:04:15,070
We have item as burrito and we have item in full and we have cheeseburger.

60
00:04:15,810 --> 00:04:22,560
Now, that means we have successfully passed the data or the database data to the templates and we have

61
00:04:22,560 --> 00:04:23,760
displayed them over here.

62
00:04:24,540 --> 00:04:28,340
Now let's go ahead and format this thing in a little bit better way.

63
00:04:28,920 --> 00:04:35,880
So what we will do here is that now we will use HTML and we will format this entire content in a much

64
00:04:35,880 --> 00:04:36,610
more better way.

65
00:04:37,200 --> 00:04:41,840
So what I can do here is that I can use an unordered list, so I'll type in you.

66
00:04:41,850 --> 00:04:47,730
Well, then I'll type an allow for the list element and we are actually going to include this entire

67
00:04:47,730 --> 00:04:49,410
thing inside here.

68
00:04:50,490 --> 00:04:55,620
So now each one of those items and the item name are going to appear as a list item.

69
00:04:56,220 --> 00:05:05,460
So now if I go back and if I hit refresh, as you can see now, these items and their respective IDs

70
00:05:05,460 --> 00:05:10,610
are actually represented as a list items and this representation looks much more better.

71
00:05:11,820 --> 00:05:16,800
Now, in the upcoming lectures, we are going to style them in a much more better way, but I guess

72
00:05:16,800 --> 00:05:19,740
this format is good enough for now.

73
00:05:20,700 --> 00:05:26,940
So let's actually go ahead and learn how exactly we have passed those items and the template and use

74
00:05:26,940 --> 00:05:27,220
them.

75
00:05:27,660 --> 00:05:32,580
So the first thing which you need to do is that you need to create a context and the context is nothing

76
00:05:32,580 --> 00:05:36,010
but whatever things you have obtained from the database.

77
00:05:36,030 --> 00:05:41,540
So in this case, we have actually query the database using item, not objects at all.

78
00:05:41,550 --> 00:05:44,640
And we have stored those objects in the item list.

79
00:05:45,180 --> 00:05:49,020
Now we have passed the same item list as the context over here.

80
00:05:49,350 --> 00:05:53,820
And whenever we want to render the template, we always pass in the context.

81
00:05:54,180 --> 00:06:00,090
So that means that this context is passed into the template, which is indexed each year.

82
00:06:00,750 --> 00:06:04,280
And here we simply go ahead and make use of this context.

83
00:06:04,620 --> 00:06:07,300
So we already have the context as item list.

84
00:06:07,320 --> 00:06:13,740
So we simply go ahead, loop through all the objects in the item list and we use the template syntax

85
00:06:13,740 --> 00:06:14,520
for doing that.

86
00:06:14,940 --> 00:06:19,440
And hence we get access to each individual item over here.

87
00:06:19,530 --> 00:06:26,310
Now, when we have access to each individual item, we simply go ahead and extract its ID and its name.

88
00:06:26,730 --> 00:06:30,080
Now you can also go ahead and extract the other properties as well.

89
00:06:30,090 --> 00:06:33,360
But for now we are just extracting the ID and the item name.

90
00:06:33,510 --> 00:06:40,830
And finally, we actually put this thing up in an altered HDMI list so as to make it look better and

91
00:06:40,830 --> 00:06:42,370
hence we get the final result.

92
00:06:43,120 --> 00:06:50,130
And now one more interesting thing which you can do here is that instead of returning the HTP response,

93
00:06:50,280 --> 00:06:53,760
you can also directly go ahead and render this template.

94
00:06:54,690 --> 00:07:01,290
So, for example, right now, as you can see, we have written the HDP response and inside that we

95
00:07:01,290 --> 00:07:02,290
render this template.

96
00:07:02,700 --> 00:07:06,180
Now we can simply go ahead and get rid of this entire thing.

97
00:07:06,930 --> 00:07:10,100
And instead what we can do is that we can type and render.

98
00:07:11,220 --> 00:07:15,630
And then in this particular render, you can pass in the request.

99
00:07:16,440 --> 00:07:22,560
So you just pass and request and then you need to type in the path for the template.

100
00:07:22,800 --> 00:07:28,890
So the template path is actually defined over here, which is huge slash and text on each jemal.

101
00:07:29,310 --> 00:07:37,650
So you can simply just copy that piece up over here and then you finally need to pass in context over

102
00:07:37,650 --> 00:07:37,980
here.

103
00:07:38,340 --> 00:07:39,900
So you type in context.

104
00:07:40,970 --> 00:07:42,590
And you should be good to go.

105
00:07:42,890 --> 00:07:45,690
So now this thing is going to just work fine.

106
00:07:46,460 --> 00:07:52,010
So with this, you no longer need to use this template right here so you can simply go ahead and get

107
00:07:52,010 --> 00:07:52,700
rid of that.

108
00:07:52,700 --> 00:07:54,040
So it'll simply delete that.

109
00:07:54,440 --> 00:07:59,890
But the only thing which you need to make sure while using render is that you need to import render.

110
00:08:00,410 --> 00:08:07,380
So here render is actually imported already, so it saves from Zango dot shortcuts, import render.

111
00:08:08,060 --> 00:08:13,430
And now if you go back, as you can see, this thing is going to still work fine.

112
00:08:13,760 --> 00:08:18,840
So using the render function directly is an efficient way to render your templates.

113
00:08:18,920 --> 00:08:23,960
You don't have to go ahead and use HTP response and then pass and render to it.

114
00:08:25,220 --> 00:08:26,760
So that's it for this lecture.

115
00:08:26,870 --> 00:08:32,840
Hopefully you guys be able to understand how to pass context to a template so that we can display the

116
00:08:32,840 --> 00:08:35,809
database data into the template.

117
00:08:36,200 --> 00:08:40,220
So thank you very much for watching and I'll see you guys next time.

118
00:08:40,580 --> 00:08:41,210
Thank you.

