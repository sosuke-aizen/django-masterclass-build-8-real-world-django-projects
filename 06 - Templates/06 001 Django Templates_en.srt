1
00:00:00,360 --> 00:00:06,130
Hello and welcome to this lecture and in this lecture, we are going to talk about templates in general.

2
00:00:06,570 --> 00:00:11,730
So even before we learn what our templates and how to create them, first of all, we need to learn

3
00:00:11,730 --> 00:00:14,280
why exactly do we need templates?

4
00:00:14,970 --> 00:00:22,080
So templates and Zango allows us to create HTML dynamically and what exactly do I mean by that?

5
00:00:22,290 --> 00:00:28,800
So you might have noticed here is that we have passed this e-mail over here to the HTP response and

6
00:00:28,800 --> 00:00:30,380
this thing actually works fine.

7
00:00:30,390 --> 00:00:34,520
But what happens if you have a bunch of HTML code in here?

8
00:00:34,530 --> 00:00:41,250
So let's say, for example, instead of simply having a simple H1 tag over here, let's say you have

9
00:00:41,250 --> 00:00:44,030
an entire huge HTML page.

10
00:00:44,040 --> 00:00:49,710
So it actually becomes quite tedious to write your entire e-mail code in here within this parenthesis

11
00:00:49,710 --> 00:00:50,270
right here.

12
00:00:50,460 --> 00:00:53,480
And also the code does not look clean at all.

13
00:00:54,150 --> 00:00:58,980
So instead, what Django allows you to do is that they will allow you to create a template.

14
00:00:58,980 --> 00:01:01,830
And a template is nothing but a simple e-mail file.

15
00:01:02,190 --> 00:01:09,690
And you can have all this HTML code into that particular e-mail file and then you can use that e-mail

16
00:01:09,690 --> 00:01:15,070
file as a template and parse that template over here instead of passing an entire HTML.

17
00:01:15,750 --> 00:01:20,850
So this is one advantage of using template or one benefit of using template.

18
00:01:21,000 --> 00:01:27,420
And the next thing which you can do with templates is that you can also pass in the database entries

19
00:01:27,420 --> 00:01:28,200
to the template.

20
00:01:28,680 --> 00:01:35,250
So right now, in this particular view, right here, we have the database entries in this particular

21
00:01:35,250 --> 00:01:36,820
variable, which is item lists.

22
00:01:36,840 --> 00:01:43,980
So the template, what you can do is that you can take the HTML code, take the database content, combine

23
00:01:43,980 --> 00:01:47,940
them together and render an HTML page dynamically.

24
00:01:48,790 --> 00:01:52,820
So this is the magical template and this is what templates allow you to do.

25
00:01:53,600 --> 00:01:59,020
OK, so now once we understand why exactly do we need templates, let's actually go ahead and learn

26
00:01:59,020 --> 00:02:00,000
how to create them.

27
00:02:00,340 --> 00:02:07,030
Now, Chango actually comes with its very own templating engine called the Django Template Engine.

28
00:02:07,510 --> 00:02:15,310
So if you go to the settings or buy a file and if you actually scroll up a bit, as you can see, you

29
00:02:15,310 --> 00:02:16,690
will have the list templates.

30
00:02:17,020 --> 00:02:23,110
And in here you will find that the template engine, which the Django actually uses is the Django template

31
00:02:23,110 --> 00:02:23,550
engine.

32
00:02:23,830 --> 00:02:30,190
As you can see, it is configured over here that this particular app is going to use a template engine,

33
00:02:30,190 --> 00:02:32,050
which is the Django template back.

34
00:02:32,050 --> 00:02:36,640
And so now once we know that, let's go back to our view right over here.

35
00:02:36,970 --> 00:02:40,290
And now let's understand how exactly can we create a template.

36
00:02:40,630 --> 00:02:43,860
So we want to create the template for the food up over here.

37
00:02:44,050 --> 00:02:49,990
And whenever you want to create a template for a particular app, you first need to go ahead and create

38
00:02:49,990 --> 00:02:52,740
a template directly into that particular app.

39
00:02:53,230 --> 00:02:57,100
So you need to go ahead, create a directory over here, name it as.

40
00:02:58,590 --> 00:03:05,240
Templates hit enter, and now once you have those templates directly, you again need to go ahead,

41
00:03:05,260 --> 00:03:10,820
create a direct viewer here, which is called as Food Itself, which is the app name itself.

42
00:03:11,610 --> 00:03:16,710
And now in this particular food directory, you can now go ahead and create a template.

43
00:03:16,980 --> 00:03:22,020
So now let's create a file in here and let's name this template as and like start each team up.

44
00:03:22,320 --> 00:03:28,980
And the reason why we need to follow this method of having a template is because Shango first needs

45
00:03:28,980 --> 00:03:34,620
to understand what language first needs to know where exactly the template is located.

46
00:03:35,580 --> 00:03:41,100
So by following this convention now, Jianguo actually knows that all the template related to the food

47
00:03:41,100 --> 00:03:43,420
app are actually stored in the templates folder.

48
00:03:43,740 --> 00:03:45,690
And again, back in the food folder.

49
00:03:45,750 --> 00:03:51,120
OK, so now once we have created this template right over here, let's have a bunch of HTML code or

50
00:03:51,120 --> 00:03:53,580
some sample dummy e-mail code in here.

51
00:03:53,610 --> 00:03:59,820
So if you're using visual studio code, you can simply type in the exclamation mark and hit enter and

52
00:03:59,820 --> 00:04:02,190
you'll automatically have a bunch of code in here.

53
00:04:02,910 --> 00:04:08,010
So for the moment, we are only going to include a heading tag over here and we are going to say something

54
00:04:08,010 --> 00:04:08,910
like this.

55
00:04:10,240 --> 00:04:19,480
This is just a template and simply save the code and now we will learn how exactly can we render this

56
00:04:19,480 --> 00:04:23,980
particular template for, let's say, this particular view right here.

57
00:04:24,010 --> 00:04:30,090
So now once we have this template, we first need to go ahead and load this template up.

58
00:04:30,490 --> 00:04:33,340
And what exactly do I mean by loading a template?

59
00:04:33,880 --> 00:04:39,340
So Django first needs to know from what location does it need to load the template?

60
00:04:39,790 --> 00:04:43,530
And for that very purpose, we actually need to import LoDo.

61
00:04:43,840 --> 00:04:50,470
So we go ahead and do that by typing in from Django, taht template import load.

62
00:04:50,660 --> 00:04:54,310
So now once we have this LoDo, we can actually load a template.

63
00:04:54,580 --> 00:05:01,840
So in order to load a template, you simply dipen the dot and the method to get template is the template

64
00:05:01,840 --> 00:05:02,340
method.

65
00:05:02,530 --> 00:05:06,490
And in here you simply pass in the path where the template is located.

66
00:05:06,670 --> 00:05:09,640
So the template is located in in the food directly.

67
00:05:09,640 --> 00:05:11,710
And the name of the file is index.

68
00:05:12,820 --> 00:05:13,540
Dorte.

69
00:05:14,510 --> 00:05:20,900
Each now, once we have that, let's save that template into some variable class template.

70
00:05:22,900 --> 00:05:28,000
OK, once we have that, we are good to go, and this means that we have successfully loaded up this

71
00:05:28,000 --> 00:05:28,970
particular template.

72
00:05:29,350 --> 00:05:31,660
Now we can go ahead and render this template.

73
00:05:31,960 --> 00:05:37,030
But even before entering a template, a template always needs a context.

74
00:05:37,360 --> 00:05:39,850
Now, what exactly do I mean by context?

75
00:05:40,150 --> 00:05:47,950
And context is nothing but the data which we obtained from the database and Django takes the template

76
00:05:47,950 --> 00:05:53,920
and combines it with the data obtained from the database and then together and does a final output.

77
00:05:54,790 --> 00:05:59,950
So right now we already have this data which is extracted from the database, which we can pass as a

78
00:05:59,950 --> 00:06:00,640
context.

79
00:06:00,880 --> 00:06:04,060
But for now we are just going to keep the context as empty.

80
00:06:04,400 --> 00:06:06,580
So I'll type in context equals.

81
00:06:07,480 --> 00:06:13,760
And for now, this context is going to remain empty and we are simply going to pass in an empty context

82
00:06:13,760 --> 00:06:14,080
in here.

83
00:06:14,560 --> 00:06:21,130
So now what we will do is that instead of returning item list in the HDB response, we are going to

84
00:06:21,130 --> 00:06:22,240
render the template.

85
00:06:22,240 --> 00:06:26,560
So in order to render the template, you type in template, not render.

86
00:06:27,690 --> 00:06:31,920
And in here, you need to pass in the context as well as the request.

87
00:06:33,000 --> 00:06:37,890
So now once you go ahead and do that, your template will be successfully rendered.

88
00:06:38,520 --> 00:06:40,980
So now just make sure that the subway is up and running.

89
00:06:41,500 --> 00:06:48,330
And now if you actually go over here to the browser and now if you go ahead and hit enter, as you can

90
00:06:48,330 --> 00:06:51,480
see now, your template is successfully rendered.

91
00:06:51,480 --> 00:06:54,680
And upon visiting this, you are well, you'll get that.

92
00:06:54,690 --> 00:07:00,360
This is just a template and we get that result because we have rendered a template over here that is

93
00:07:00,360 --> 00:07:04,730
an entire HTML file instead of putting in some static content in here.

94
00:07:05,660 --> 00:07:11,420
So that's how you can go ahead and use templates and Shango and we have not yet passed the database

95
00:07:11,420 --> 00:07:17,120
contents over here as a context of this particular template, but in the upcoming lecture, we will

96
00:07:17,120 --> 00:07:23,210
go ahead and learn how to pass the database content or the database data to this particular template

97
00:07:23,480 --> 00:07:27,680
so that that database data is also rendered along with the HTML.

98
00:07:28,580 --> 00:07:32,600
So thank you very much for watching and I'll see you guys next time.

99
00:07:32,960 --> 00:07:33,560
Thank you.

